<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberPromo.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode    		= $HTTP_GET_VARS['mode'];
$act    		= $HTTP_GET_VARS['act'];
$submode 		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'blank';

$Promo	=	new Promo(); 	

switch($mode){
//MENAMPILKAN KOLOM ISIAN BLANK==========================================================================================================
case 'blank':

	$opt_layer_promo="
		<option selected value='1'>1</option>
		<option value='2'>2</option>
		<option value='3'>3</option>
		<option value='4'>4</option>
		<option value='5'>5</option>
		<option value='6'>6</option>
		<option value='7'>7</option>
		<option value='8'>8</option>
		<option value='9'>9</option>
		<option value='10'>10</option>";
	
	$pesan="Penambahan";
	
	$template->set_filenames(array('body' => 'member_promo_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  					=>$userdata['username'],
	   	'BCRUMP'    					=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_promo.'.$phpEx).'">Promo</a>',
			'PESAN'								=>$pesan,
			'LAYER_PROMO'					=>$opt_layer_promo,
		));
break;

//TAMBAH MEMBER BARU ==========================================================================================================
case 'tambah_promo':
	
	//ambil isi dari parameter
	
	$layer_promo					=$HTTP_GET_VARS['layer_promo'];
	$kode_jadwal					=$HTTP_GET_VARS['kode_jadwal'];
	$masa_berlaku_mula		=$HTTP_GET_VARS['masa_berlaku_mula'];
	$masa_berlaku_akhir		=$HTTP_GET_VARS['masa_berlaku_akhir'];
	$point								=$HTTP_GET_VARS['point'];
	$discount_jumlah			=$HTTP_GET_VARS['discount_jumlah'];
	$discount_persentase	=$HTTP_GET_VARS['discount_persentase'];
	
	$return = $Promo->tambah(
		$layer_promo, $kode_jadwal, $masa_berlaku_mula, $masa_berlaku_akhir, 
		$point, $discount_jumlah, $discount_persentase);
	
	$return	= ($return)?"berhasil":"gagal";
	
	echo $return;
	
exit;

//UBAH MEMBER BARU ==========================================================================================================
case 'ubah_promo':
	
	//ambil isi dari inputan
	$id_promo							=$HTTP_GET_VARS['id_promo'];
	$layer_promo					=$HTTP_GET_VARS['layer_promo'];
	$kode_jadwal					=$HTTP_GET_VARS['kode_jadwal'];
	$masa_berlaku_mula		=$HTTP_GET_VARS['masa_berlaku_mula'];
	$masa_berlaku_akhir		=$HTTP_GET_VARS['masa_berlaku_akhir'];
	$point								=$HTTP_GET_VARS['point'];
	$discount_jumlah			=$HTTP_GET_VARS['discount_jumlah'];
	$discount_persentase	=$HTTP_GET_VARS['discount_persentase'];
	
	
	$return = $Promo->ubah(
		$id_promo,$layer_promo, $kode_jadwal, $masa_berlaku_mula, $masa_berlaku_akhir, 
		$point, $discount_jumlah, $discount_persentase);
	
	$return	= ($return)?"berhasil":"gagal";
	
	echo $return;
	
exit;

//AMBIL DATA MEMBER==========================================================================================================
case 'ambil_data_promo':
	
	$id_promo    = $HTTP_GET_VARS['id_promo'];
	
	$row	= $Promo->ambilDataDetail($id_promo);
	
	$id_promo							=trim($row['id_promo']);
	$layer_promo					=trim($row['layer_promo']);
	$kode_jadwal					=trim($row['kode_jadwal']);
	$masa_berlaku_mula		=trim($row['masa_berlaku_mula']);
	$masa_berlaku_akhir		=trim($row['masa_berlaku_akhir']);
	$point								=trim($row['point']);
	$discount_jumlah			=trim($row['discount_jumlah']);
	$discount_persentase	=trim($row['discount_persentase']);
	$dibuat_oleh					=trim($row['dibuat_oleh']);
	$waktu_dibuat					=trim($row['waktu_dibuat']);
	$diubah_oleh					=trim($row['diubah_oleh']);
	$waktu_diubah					=trim($row['waktu_diubah']);
	
	//memeriksa apakah data member dengan id_member ditemukan dalam database
	if($kode_jadwal==""){
		//jika tidak ditemukan, akan langsung didirect ke halaman member
		redirect(append_sid('member_promo.'.$phpEx)."&pesan=$pesan",true); 
	}
	
	$pesan="Pengubahan";
	
	//option layer promo
	$temp_var="selected_layer_promo".$layer_promo;
	$$temp_var="selected";
	
	$opt_layer_promo="
		<option $selected_layer_promo1 value='1'>1</option>
		<option $selected_layer_promo2 value='2'>2</option>
		<option $selected_layer_promo3 value='3'>3</option>
		<option $selected_layer_promo4 value='4'>4</option>
		<option $selected_layer_promo5 value='5'>5</option>
		<option $selected_layer_promo6 value='6'>6</option>
		<option $selected_layer_promo7 value='7'>7</option>
		<option $selected_layer_promo8 value='8'>8</option>
		<option $selected_layer_promo9 value='9'>9</option>
		<option $selected_layer_promo10 value='10'>10</option>";
	
	$template->set_filenames(array('body' => 'member_promo_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  					=>$userdata['username'],
	   	'BCRUMP'    					=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_promo.'.$phpEx).'">Promo</a>',
			'ID_PROMO'						=>$id_promo,
			'LAYER_PROMO'					=>$opt_layer_promo,
			'KODE_JADWAL'					=>$kode_jadwal,
			'MASA_BERLAKU_MULA'		=>$masa_berlaku_mula,
			'MASA_BERLAKU_AKHIR'	=>$masa_berlaku_akhir,
			'POINT'								=>$point,
			'DISCOUNT_JUMLAH'			=>$discount_jumlah,
			'DISCOUNT_PERSENTASE'	=>$discount_persentase,
			'DIBUAT_OLEH'					=>$dibuat_oleh,					
			'WAKTU_DIBUAT'				=>$waktu_dibuat,
			'DIUBAH_OLEH'					=>$diubah_oleh,					
			'WAKTU_DIUBAH'				=>$waktu_diubah,
			'PESAN'								=>$pesan
  ));
	
break;
} //switch mode

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>