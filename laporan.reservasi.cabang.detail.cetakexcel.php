<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['tglmulai'])? $HTTP_GET_VARS['tglmulai'] : $HTTP_POST_VARS['tglmulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$kode_cabang		= isset($HTTP_GET_VARS['kodecabang'])? $HTTP_GET_VARS['kodecabang'] : $HTTP_POST_VARS['kodecabang'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cabang	= $userdata['user_level']!=$USER_LEVEL_INDEX["SUPERVISOR"]?"":" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";

//QUERY TIKET
$sql=
	"SELECT 
		WaktuCetakTiket,NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,HargaTiket,0),SubTotal) AS HargaTiket,SubTotal,
		IF(JenisPembayaran!=3,Discount,0) AS Discount,IF(JenisPembayaran!=3,Total,0) AS Total,
		IF(JenisPenumpang!='R',JenisDiscount,'RETURN') AS JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM 
		tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' $kondisi_cabang";	

//CONTENT TIKET

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Err: $sql ".__LINE__);exit;
}
	
//QUERY PAKET
$sql_paket=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,NamaPengirim,
		NamaPenerima,HargaPaket,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO
	FROM 
		tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' $kondisi_cabang";	
	
$tgl_cetak	=	date("d-m-Y");

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);

//HEADER
 $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap Tiket periode '.dateparse(FormatMySQLDateToTgl($tanggal_mulai_mysql)).' s/d '.dateparse(FormatMySQLDateToTgl($tanggal_akhir_mysql)));
$objPHPExcel->getActiveSheet()->setCellValue('C2', 'Tgl.Cetak:'.$tgl_cetak);
$objPHPExcel->getActiveSheet()->setCellValue('A2', '[-TIKET-]');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Waktu Cetak');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', '#Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', '#Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', '#Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Nama Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Telepon');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Harga Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Tipe Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', '#Manifest');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', '#Unit');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'CSO');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);

while ($row = $db->sql_fetchrow($result)){
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
		$sum_tiket_batal++;
		$sum_uang_tiket_batal	+= $row['HargaTiket'];
		$sum_discount_batal		+= $row['Discount'];
		$sum_total_batal			+= $row['Total'];
	}
	
	$sum_tiket_all++;
	$sum_uang_tiket_all	+= $row['HargaTiket'];
	$sum_discount_all		+= $row['Discount'];
	$sum_total_all			+= $row['Total'];
	
	//PLOT DATA
	$idx++;
	$idx_row=$idx+3;
		
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])));
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Telp']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NomorKursi']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['HargaTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['Discount']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Total']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['JenisDiscount']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $row['NoSPJ']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['NoPolisi']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $row['NamaCSO']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $status);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $keterangan);
}

$idx_row++;		
	
//SUMMARY TIKET
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Tiket");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$idx);$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Tiket Batal");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$sum_tiket_batal);$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Omzet");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$sum_uang_tiket_all-$sum_uang_tiket_batal);$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Discount");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$sum_discount_all-$sum_discount_batal);$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Nett");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$sum_total_all-$sum_total_batal);$idx_row++;

$idx_row++;
//HEADER PAKET
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,'[-PAKET-]');$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, 'Waktu Cetak');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, '#Resi');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '#Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, 'Dari');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, 'Untuk');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, 'Harga Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, 'CSO');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

//CONTENT PAKET

$sum_paket_batal			= 0;
$sum_uang_paket_batal	= 0;
$sum_paket_all				= 0;
$sum_uang_paket_all		= 0;


if (!$result = $db->sql_query($sql_paket)){
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

$idx_paket=0;

while ($row = $db->sql_fetchrow($result)){
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
		$sum_paket_batal++;
		$sum_uang_paket_batal	+= $row['HargaPaket'];
	}
	
	$sum_paket_all++;
	$sum_uang_paket_all	+= $row['HargaPaket'];
	
	//PLOT DATA
	$idx_paket++;
	$idx_row++;
		
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx_paket);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NamaPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NamaPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['HargaPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NamaCSO']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $status);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $keterangan);
	
}
$idx_row++;

//SUMMARY TIKET
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Paket");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$idx);$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Paket Batal");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$sum_paket_batal);$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row,"Total Omzet");$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row,$sum_uang_paket_all-$sum_uang_paket_batal);$idx_row++;

	  
if($idx>0 || $idx_paket>0){
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Laporan Rekap Tiket Periode '.dateparse(FormatMySQLDateToTgl($tanggal_mulai_mysql)).' s/d '.dateparse(FormatMySQLDateToTgl($tanggal_akhir_mysql)).'.xls"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output'); 
}						
else{
	echo("Tidak ada data");
}
?>