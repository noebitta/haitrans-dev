<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CCARE))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$btn_submit  = isset($HTTP_GET_VARS['btn_submit'])? $HTTP_GET_VARS['btn_submit'] : $HTTP_POST_VARS['btn_submit'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$btn_submit			= ($tanggal_mulai=='')?"cari":$btn_submit;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Member	= new Member();
	
		// LIST
		$template->set_filenames(array('body' => 'member/member_calon_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";
		
		//mengambil data member untuk pencocokkan data
		$sql	= 
			"SELECT 
				Handphone,Telp
			FROM tbl_md_member";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err:".__LINE__);exit;
		}
		
		$data_no_hp		= array();
		$data_no_telp	= array();
		
		while ($row = $db->sql_fetchrow($result)){
			if($row['Handphone']!=''){ $data_no_hp[]	= "'".$row['Handphone']."'";}
			if($row['Telp']!=''){ $data_no_telp[]	= "'".$row['Telp']."'";}
		}
		
		$list_no_hp		= implode(",",$data_no_hp);
		$list_no_telp	= implode(",",$data_no_telp);
		
		$kondisi_hp		= ($list_no_hp!='')?" AND Telp NOT IN($list_no_hp) ":"";
		$kondisi_telp	= ($list_no_telp!='')?" AND Telp NOT IN($list_no_telp) ":"";
		
		$kondisi	=($cari=="")?"":
			" AND (Nama LIKE '%$cari%' 
				OR Telp LIKE '%$cari%')";
		
		if($btn_submit=="cari"){
			
			//create view
			$sql = 
				"CREATE OR REPLACE VIEW view_calon_member_frekwensi AS 
				SELECT Nama,Telp,COUNT(1) AS Frekwensi 
				FROM tbl_reservasi_olap
				WHERE 
					FlagBatal!=1 
					AND CetakTiket=1 
					AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
					$kondisi
					$kondisi_hp 
					$kondisi_telp
				GROUP BY Telp";
			
			if (!$result = $db->sql_query($sql)){
				echo("Error :".__LINE__);exit;
			}
		}
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"1","view_calon_member_frekwensi","&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order","WHERE Frekwensi>=$MINIMUM_KEBERANGKATAN_JADI_MEMBER","pengaturan_member_calon_member.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		//mengambil data dari view
		$sql = 
			"SELECT * 
			FROM view_calon_member_frekwensi 
			WHERE Frekwensi>=$MINIMUM_KEBERANGKATAN_JADI_MEMBER
			$kondisi_sort LIMIT $idx_awal_record,$VIEW_PER_PAGE";
			
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama'=>$row['Nama'],
							'hp'=>$row['Telp'],
							'frekwensi'=>number_format($row['Frekwensi'],0,",","."),
							'aktif'	=>$status
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=10 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			echo("Error :".__LINE__);exit;
		} 
		
		$order_invert	= ($order=='asc' || $order=='')?'desc':'asc';
		
		$parameter_sorting	= 'pengaturan_member_calon_member.'.$phpEx.'?cari='.$cari."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir;
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&cari=".$cari."&sort_by=".$sort_by."&order=".$order;
			
		//$script_cetak_pdf="Start('pengaturan_member_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
														
		$script_cetak_excel="Start('pengaturan_member_calon_member_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		
		//--END KOMPONEN UNTUK EXPORT
		
		$page_title	= "Penumpang Potensial Jadi Member";
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('menu_pengelolaan_member.'.$phpEx.'?top_menu_dipilih=top_menu_pengelolaan_member') .'">Home</a> | <a href="'.append_sid('pengaturan_member_calon_member.'.$phpEx).'">Pelanggan Potensial Jadi Member</a>',
			'ACTION_CARI'		=> append_sid('pengaturan_member_calon_member.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'TGL_AWAL'			=> $tanggal_mulai,
			'TGL_AKHIR'			=> $tanggal_akhir,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging,
			'A_SORT_BY_NAMA'			=>  append_sid($parameter_sorting.'&sort_by=Nama&order='.$order_invert),
			'A_SORT_BY_TELP'			=>  append_sid($parameter_sorting.'&sort_by=Telp&order='.$order_invert),
			'A_SORT_BY_FREKWENSI'	=>  append_sid($parameter_sorting.'&sort_by=Frekwensi&order='.$order_invert),
			'CETAK_XL'						=> $script_cetak_excel
			)
		);
		
	  

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>