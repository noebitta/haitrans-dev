<?php
//
// RESERVASI
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){  
  if(!isset($HTTP_GET_VARS['mode'])){
		redirect(append_sid('index.'.$phpEx),true); 
	}
	else{
		echo("Silahkan Login Ulang!");exit;
	}
}

include($adp_root_path . 'ClassPaketCargo.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Cargo	= new Cargo();

//METHODS ==========================================================================================================
function setComboKurir($kode_user_dipilih){
	//SET COMBO SOPIR
	global $db;
	global $userdata;
	global $USER_LEVEL_INDEX;
	
	$sql = 
		"SELECT *
		FROM tbl_user
		WHERE user_level=".$USER_LEVEL_INDEX["PICKER"].";";
				
	if (!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	
	$opt="<option value=''>- silahkan pilih kurir  -</option>";
		
	while ($row = $db->sql_fetchrow($result)){
		$selected	=($kode_user_dipilih!=$row['user_id'])?"":"selected";
		$opt .="<option value='$row[user_id]' $selected>$row[nama]</option>";
	}
			
	return $opt;
	//END SET COMBO USER
}
//PEMILIHAN PROSES
switch ($mode){

//POINT PICKUP  ===========================================================================================================================================================================================
case "setpointpickup":
    // membuat nilai awal..
	$kota	 = $HTTP_GET_VARS['kotapickup'];
	include($adp_root_path . 'ClassCabang.php');
	
	$Cabang	= new Cabang();
	
	$result	= $Cabang->setComboCabang($kota);
	
  $opt = "";
	
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
      $opt .= "<option value='$row[0]'>$row[1]</option>";
    }    
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
	
	echo("
	  <table width='100%'> 
		  <tr><td>POINT PICK UP<br></td></tr>
	    <tr>
				<td><select id='pointpickup' name='pointpickup' onchange='getUpdatePaket();'>$opt</select><span id='progress_asal' style='display:none;'><img src='./templates/images/loading.gif' /></span>
				</td>
			</tr>
		</table>");

exit;

//SET KOTA  ASAL===========================================================================================================================================================================================
case "setkotaasal":
   
	$provinsi	 = $HTTP_GET_VARS['prov'];
	
  $result	= $Cargo->ambilDataKota("KodeProvinsi='$provinsi'");
	
	$opt = "<option value=''>-pilih kota-</option>";
		
	while ($row = $db->sql_fetchrow($result)){
		$opt .= "<option value='".$row['KodeKota']."'>".$row['NamaKota']."</option>";
  }    
	
	echo("
		<select id='kotaasal' name='kotaasal' onfocus=\"this.style.background='';\" onchange=\"hargaperkg.value='';leadtime.innerHTML='';setHarga();\">$opt</select>
	");

exit;

//SET KOTA  TUJUAN===========================================================================================================================================================================================
case "setkotatujuan":
   
	$provinsi	 = $HTTP_GET_VARS['prov'];
	
  $result	= $Cargo->ambilDataKota("KodeProvinsi='$provinsi'");
	
	$opt = "<option value=''>-pilih kota-</option>";
		
	while ($row = $db->sql_fetchrow($result)){
		$opt .= "<option value='".$row['KodeKota']."'>".$row['NamaKota']."</option>";
  }    
	
	echo("
		<select id='kotatujuan' name='kotatujuan' onfocus=\"this.style.background='';\" onchange=\"setHarga();\">$opt</select>
	");

exit;

	
// PERIKSA NO TELP ===========================================================================================================================================================================================
case "periksanotelp":
	$telp 				= $HTTP_GET_VARS['telp'];
	$nama_elemen 	= $HTTP_GET_VARS['obj'];

	include($adp_root_path . 'ClassPelanggan.php');
	
	$Pelanggan = new Pelanggan();
	
	$data_pelanggan=$Pelanggan->ambilDataDetail($telp);
  
	if(count($data_pelanggan)>1){	
		if($nama_elemen=="telppengirim"){
			echo("
				prosesok=true;
				namapengirim.value='".trim($data_pelanggan['Nama'])."';
				alamatpengirim.value='".trim($data_pelanggan['Alamat'])."';
			");
		}
		else{
			echo("
				prosesok=true;
				namapenerima.value='".trim($data_pelanggan['Nama'])."';
				alamatpenerima.value='".trim($data_pelanggan['Alamat'])."';
			");
		}
	}
	else{
		echo("prosesok=true;");
	}
	
  exit;
	
//SET HARGA PAKET===========================================================================================================================================================================================
case "setharga":
   
	$kota_asal	 = $HTTP_GET_VARS['kotaasal'];
	$kota_tujuan = $HTTP_GET_VARS['kotatujuan'];
	
  $row	= $Cargo->ambilHarga($kota_asal,$kota_tujuan);
	
	echo("
		prosesok=true;
		hargaperkg.value='$row[HargaPerKg]';
		leadtime.innerHTML='$row[LeadTime]';
		hitungHargaPaket();
	");

exit;

// PENGUMUMAN LAYOUT===========================================================================================================================================================================================
case "pengumuman":
  // menampilkan pengumuman
	include_once($adp_root_path . 'ClassPengumuman.php');
	
	$Pengumuman	= new Pengumuman();
	
  if ($userdata['user_level'] != $LEVEL_SCHEDULER){	
		//HANYA BISA DIAKSES OLEH SELAIN AGEN
		
	  $jumlah_pengumuman	= $Pengumuman->ambilJumlahPengumumanBaru($userdata['user_id']);
			
		if($jumlah_pengumuman>0){
			$jumlah_pengumuman	= "<blink>".$jumlah_pengumuman."</blink>";
			$blink_Pengumuman		= "<blink>Pesan</blink>";
		}
		else{
			$jumlah_pengumuman	= "";
			$blink_Pengumuman		= "Pesan";
		}
		
		//$isi_pengumuman
		
		$baca_pengumuman	= "Start('".append_sid('pengumuman_inbox.'.$phpEx)."');getPengumuman();return false;";
		
		echo("
			<a class='menu' href='#' onClick=$baca_pengumuman>&nbsp;&nbsp;&nbsp;&nbsp;<font color='red' size=5><b>$jumlah_pengumuman</b></font>&nbsp;&nbsp;&nbsp;&nbsp;
			<br><br><br>
			$blink_Pengumuman</a>
		");

	}
		
  exit;
break;

//==PAKET==
// PAKET LAYOUT===========================================================================================================================================================================================
case "paketlayout":
  // menampilkan paket yang tersedia sesuai dengan tanggal, rute dan jam yang dimasukan
		
	$TglDiterima  		= $HTTP_GET_VARS['tglditerima']; 
	$PoinPickedUp			= $HTTP_GET_VARS['poinpickedup'];
	$NamaPoinPickedUp	= $HTTP_GET_VARS['namapoinpickedup'];
	$mode_mutasi_on		= $HTTP_GET_VARS['modemutasion'];
	
	//DATA MANIFEST
	$data_manifest	= $Cargo->ambilDataManifestByTglPoin($TglDiterima,$PoinPickedUp);
	
	if($data_manifest["Id"]==""){
		if($mode_mutasi_on==1){
			$template->assign_block_vars('TOMBOL_MUTASIKAN',array());
		}
		
		$template->assign_block_vars('BELUM_PICKUP',array());
		$ket_tombol_manifest	= "CETAK MANIFEST";
	}
	else{
		if($mode_mutasi_on==1 && $userdata["user_level"]<=$USER_LEVEL_INDEX["SPV_RESERVASI"]){
			$template->assign_block_vars('TOMBOL_MUTASIKAN',array());
		}
		
		//manifest sudah dicetak
		$template->assign_block_vars(
			'DATA_MANIFEST',array(
				'no_spj'					=> $data_manifest['NoSPJ'],
				'waktu_cetak'	=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_manifest['WaktuCetak'])),
				'petugas_pencetak'	=> $data_manifest['NamaPetugasCetak']
			)
		);
		
		$ket_tombol_manifest	= "PICKED UP";
		
		if($data_manifest["WaktuPickedUp"]){
			//SUDAH DI PICEKD UP
			$template->assign_block_vars(
				'DATA_PICKUP',array(
					'waktu_pickedup'	=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_manifest['WaktuPickedUp'])),
					'petugas_pemberi'	=> $data_manifest['NamaPetugasPemberi'],
					'petugas_penerima'=> $data_manifest['NamaPetugasPickedUp']
				)
			);
		}
		else{
			$template->assign_block_vars("BELUM_PICKUP",array());
		}
		
		$no_spj		= $data_manifest['NoSPJ'];
	}
	
	//MENGAMBIL DATA BARANG
	$result	= $Cargo->ambilListCargo($TglDiterima,$PoinPickedUp);
	
	$i=0;
	while($row=$db->sql_fetchrow($result)){
		$i++;
		
		$odd	= $i%2==0?"even":"odd";
		
		$template->assign_block_vars(
			'DAFTAR_CARGO',array(
				'no'						=> $i,
				'odd'						=> $odd,
				'id'						=> $row['Id'],
				'awb'						=> $row['NoAWB'],
				'jenis_barang'	=> $row['JenisBarang']=="PRC"?"PARCEL":"DOKUMEN",
				'layanan'				=> $row['Layanan']=="REG"?"REGULER":"URGENT",
				'rute'					=> $row['Asal']."-".$row["Tujuan"],
				'nama_pengirim'	=> substr($row['NamaPengirim'],0,15),
				'nama_penerima'	=> substr($row['NamaPenerima'],0,15),
				'koli'					=> number_format($row['Koli'],0,",","."),
				'berat'					=> number_format($row['Berat'],0,",",".")
			)
		);
		
		$total_pax		+= $row["Koli"];
		$total_omzet	+= $row["TotalBiaya"];
	}
	
	if($i<=0){
		$template->assign_block_vars('NO_DATA',array());
	}
	else{
		$template->assign_block_vars('TOMBOL_CETAK_MANIFEST',array('value'=>$ket_tombol_manifest));
	}
	
	$template->assign_vars(array(
		'NO_SPJ'										=> $no_spj,
		'TANGGAL_BERANGKAT_DIPILIH' => dateparse(FormatMySQLDateToTgl($TglDiterima)),
		'POIN_PICKEDUP_DIPILIH'			=> $NamaPoinPickedUp,
		'TOTAL_TRANSAKSI'						=> number_format($i,0,",","."),
		'TOTAL_PAX'									=> number_format($total_pax,0,",","."),
		'TOTAL_OMZET'								=> number_format($total_omzet,0,",",".")
  ));
	
	$template->set_filenames(array('body' => 'reservasi.cargo/layout.tpl')); 
	$template->pparse('body');
	
  exit;
break;

//PAKET TAMBAH ===========================================================================================================================================================================================
case "pakettambah":
	
	$NamaPengirim		= $HTTP_POST_VARS['namapengirim'];
	$TelpPengirim		= $HTTP_POST_VARS['telppengirim'];
	$AlamatPengirim	= $HTTP_POST_VARS['alamatpengirim'];
	$NamaPenerima		= $HTTP_POST_VARS['namapenerima'];
	$TelpPenerima		= $HTTP_POST_VARS['telppenerima'];
	$AlamatPenerima	= $HTTP_POST_VARS['alamatpenerima'];
	$TglDiterima		= $HTTP_POST_VARS['tglberangkat'];
	$KodeAsal				= $HTTP_POST_VARS['kodeasal'];
	$Asal						= $HTTP_POST_VARS['asal'];
	$KodeTujuan			= $HTTP_POST_VARS['kodetujuan'];
	$Tujuan					= $HTTP_POST_VARS['tujuan'];
	$JenisBarang		= $HTTP_POST_VARS['jenisbarang'];
	$Via						= $HTTP_POST_VARS['via'];
	$Layanan				= $HTTP_POST_VARS['layanan'];
	$Koli						= $HTTP_POST_VARS['koli'];
	$IsVolumetric		= $HTTP_POST_VARS['isvolumetric'];
	$DimensiPanjang	= $HTTP_POST_VARS['dimensipanjang'];
	$DimensiLebar		= $HTTP_POST_VARS['dimensilebar'];
	$DimensiTinggi	= $HTTP_POST_VARS['dimensitinggi'];
	$Berat					= $HTTP_POST_VARS['berat'];
	$BiayaTambahan	= $HTTP_POST_VARS['biayatambahan'];
	$BiayaPacking		= $HTTP_POST_VARS['biayapacking'];
	$IsAsuransi			= $HTTP_POST_VARS['isasuransi'];
	$HargaDiakui		= $HTTP_POST_VARS['hargadiakui'];
	$IsTunai				= $HTTP_POST_VARS['istunai'];
	$DeskripsiBarang= $HTTP_POST_VARS['deskripsibarang'];
	$PoinPickedUp 	= $HTTP_POST_VARS['poinpickedup'];
	$NamaPoinPickedUp	= $HTTP_POST_VARS['namapoinpickedup']; 
	$no_spj					= $HTTP_POST_VARS['nospj'];
	
	//VERIFIKASI DATA
  
	if($TelpPengirim==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	if($NamaPengirim==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	if($AlamatPengirim==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	if($TelpPenerima==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	if($AlamatPenerima==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	if($KodeAsal==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	if($KodeTujuan==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	if($IsVolumetric==0){
		if($Berat<=0){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	}
	else{
		if($DimensiPanjang<=0 || $DimensiLebar<=0 || $DimensiTinggi<=0){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	}
	if($IsAsuransi==1){
		if($HargaDiakui==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}
	}
	if($DeskripsiBarang==''){echo("prosesok=false;alert('Err:".__LINE__."')");exit;}

	//PROSES
	
	$NoAWB					= $Cargo->getNoAWB();
	
	$data_layanan_paket	= $Cargo->ambilHarga($KodeAsal,$KodeTujuan);
	
	$LeadTime				= $data_layanan_paket['LeadTime'];
	
	if(!$IsVolumetric) {
		$BiayaKirim		= $data_layanan_paket['HargaPerKg'] * $Berat;
	}
	else{
		$Berat					= ceil(($DimensiPanjang*$DimensiLebar*$DimensiTinggi)/4000);
		$BiayaKirim			= $data_layanan_paket['HargaPerKg'] * $Berat;
	}
	
	$BiayaAsuransi	= !$IsAsuransi?0:0.002*$HargaDiakui;
	
	$TotalBiaya			= $BiayaKirim+$BiayaTambahan+$BiayaPacking+$BiayaAsuransi;
	
	$DicatatOleh		= $userdata['user_id'];
	$NamaPencatat		= $userdata['nama'];
	
	if($Cargo->tambah(
		$NoAWB,
		$NamaPengirim, $TelpPengirim,$AlamatPengirim,
		$NamaPenerima, $TelpPenerima, $AlamatPenerima,
		$TglDiterima, $KodeAsal, $Asal,
		$KodeTujuan, $Tujuan, $JenisBarang,
		$Via, $Layanan, $Koli,
		$IsVolumetric,$DimensiPanjang,$DimensiLebar,$DimensiTinggi,
		$Berat, $LeadTime,$BiayaKirim,
		$BiayaTambahan, $BiayaPacking, $IsAsuransi,
		$HargaDiakui,$BiayaAsuransi,$TotalBiaya,
		$IsTunai,$DeskripsiBarang,$DicatatOleh,
		$NamaPencatat,$DicatatOleh,$NamaPencatat,
		$PoinPickedUp,$NamaPoinPickedUp)){
		
		echo("prosesok=true;cetakAWB('$NoAWB');");	
	}
	else{
		echo("prosesok=false;");	
	}
	

exit;

//PAKET BATAL ===========================================================================================================================================================================================
case "paketbatal":
	
	$id	= $HTTP_GET_VARS['id'];// nourut
	
	//periksa wewenang
	if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI']))){
		$Cargo->batal($id, $userdata['user_id'],$userdata['nama']);
		echo("prosesok=true;");
	}
	else{
		echo("prosesok=true;alert('Anda tidak memiliki wewenang untuk membatalkan data ini!')");
	}

exit;

// PAKET DETAIL  ===========================================================================================================================================================================================
case "paketdetail":
	$id	= $HTTP_GET_VARS['id'];
	
	$data_cargo	=  $Cargo->ambilDataDetailById($id);
	
	$list_via	= array("DRT"=>"DARAT","UDR"=>"UDARA","LUT"=>"LAUT");
	
	if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI']))){
		$action_batal= "batalPaket($data_cargo[Id]);";
	}
	
	$template->assign_vars(array(
		'AWB'							=> $data_cargo['NoAWB'],
		'ID'							=> $data_cargo['Id'],
		'TELP_PENGIRIM'		=> $data_cargo['TelpPengirim'],
		'NAMA_PENGIRIM'		=> $data_cargo['NamaPengirim'],
		'ALAMAT_PENGIRIM'	=> $data_cargo['AlamatPengirim'],
		'TELP_PENERIMA'		=> $data_cargo['TelpPenerima'],
		'NAMA_PENERIMA'		=> $data_cargo['NamaPenerima'],
		'ALAMAT_PENERIMA'	=> $data_cargo['AlamatPenerima'],
		'KOTA_ASAL'				=> $data_cargo['Asal'],
		'KOTA_TUJUAN'			=> $data_cargo['Tujuan'],
		'JENIS_BARANG'		=> ($data_cargo['JenisBarang']=="PRC"?"PARCEL":"DOKUMEN"),
		'VIA'							=> $list_via[$data_cargo['Via']],
		'LAYANAN'					=> ($data_cargo['Layanan']=="REG"?"REGULER":"URGENT"),
		'KOLI'						=> number_format($data_cargo['Koli'],0,",","."),
		'PERHITUNGAN'			=> ($data_cargo['IsVolumetric']==0?"BERAT":"VOLUMETRIC"),
		'SHOW_DIMENSI'		=> ($data_cargo['IsVolumetric']==0?"none":"block"),
		'DIMENSI_PANJANG'	=> number_format($data_cargo['DimensiPanjang'],0,",","."),
		'DIMENSI_LEBAR'		=> number_format($data_cargo['DimensiLebar'],0,",","."),
		'DIMENSI_TINGGI'	=> number_format($data_cargo['DimensiTinggi'],0,",","."),
		'BERAT'						=> number_format($data_cargo['Berat'],0,",","."),
		'LEAD_TIME'				=> $data_cargo['LeadTime'],
		'BIAYA_KIRIM'			=> number_format($data_cargo['BiayaKirim'],0,",","."),
		'BIAYA_TAMBAHAN'	=> number_format($data_cargo['BiayaTambahan'],0,",","."),
		'BIAYA_PACKING'		=> number_format($data_cargo['BiayaPacking'],0,",","."),
		'IS_ASURANSI'			=> ($data_cargo['IsAsuransi']==0?"TIDAK":"YA"),
		'SHOW_ASURANSI'		=> ($data_cargo['IsAsuransi']==0?"none":"block"),
		'HARGA_DIAKUI'		=> number_format($data_cargo['HargaDiakui'],0,",","."),
		'BIAYA_ASURANSI'	=> number_format($data_cargo['BiayaAsuransi'],0,",","."),
		'TOTAL_BIAYA'			=> number_format($data_cargo['TotalBiaya'],0,",","."),
		'CARA_BAYAR'			=> ($data_cargo['IsAsuransi']==1?"TUNAI":"KREDIT"),
		'DESKRIPSI_BARANG'=> $data_cargo['DeskripsiBarang'],
		'CSO'							=> $data_cargo['NamaPencatat'],
		'WAKTU_TERIMA'		=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_cargo['WaktuCatat'])),
		'ACTION_BATAL'		=> $action_batal
  ));
	
	if($data_cargo['IsMutasi']==1){
		$template->assign_block_vars('INFO_MUTASI',array(
			'cso'			=> $data_cargo['NamaPetugasMutasi'],
			'waktu'		=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_cargo['WaktuMutasi'])),
			'tgl'			=> dateparse(FormatMySQLDateToTgl($data_cargo['TglDiterimaLama'])),
			'point'		=> $data_cargo['NamaPoinPickedUpLama']
		));
	}
	
	if($data_cargo['NoSPJ']=='' || $userdata["user_level"]<=$USER_LEVEL_INDEX["SPV_RESERVASI"]){
		$template->assign_block_vars('TOMBOL_MUTASI',array());
	}
	
	$template->set_filenames(array('body' => 'reservasi.cargo/paketdetail.tpl')); 
	$template->pparse('body');
	exit;
	
break;

//MUTASI PAKET=============================================================================================================================================
case "mutasipaket":
	$id									= $HTTP_GET_VARS['id'];
	$tanggal						= $HTTP_GET_VARS['tanggal'];
	$point_pickedup			= $HTTP_GET_VARS['pointpickedup'];
	$nama_point_pickup	= $HTTP_GET_VARS['namapoinpickedup'];
	
	$Cargo->mutasi($id,$tanggal,$point_pickedup,$nama_point_pickup,$userdata['user_id'],$userdata['nama']);
	
	echo("prosesok=true;");
exit;

//PAKET AMBIL ===========================================================================================================================================================================================
case "paketambil":
	include($adp_root_path . 'ClassPaketEkspedisi.php');
	$Paket			= new Paket();
	
	$no_tiket					= $HTTP_GET_VARS['no_tiket'];
	$nama_pengambil		= $HTTP_GET_VARS['nama_pengambil'];
	$no_ktp_pengambil	= $HTTP_GET_VARS['no_ktp_pengambil'];
	
	if($Paket->updatePaketDiambil($no_tiket,$nama_pengambil,$no_ktp_pengambil,$userdata['user_id'],$userdata['KodeCabang'])){
		echo(1);
	}
	else{
		echo(0);
	}
	
exit;

//CARI PAKET  ===========================================================================================================================================================================================
case 'caripaket':
	$Reservasi	= new Reservasi();
	
	$no_resi	= $HTTP_GET_VARS['noresi'];
	
	$result	= $Cargo->cariPaket($no_resi);
	
	if ($result){
	
		$return	= "
			<table>
				<tr>
					<th width=100>Tgl.Pergi</th>
					<th width=150>Asal</th>
					<th width=150>Tujuan</th>
					<th width=70>Jam</th>
					<th width=100>Id Kendaraan</th>
					<th width=100>Sopir</th>
					<th width=100>No.Resi</th>
					<th width=150>Pengirim</th>
					<th width=150>Penerima</th>
					<th width=150>Pengambil</th>
				</tr>";
		
		$jum_data	= 0;
		
		while ($row = $db->sql_fetchrow($result)){
			
			if($row['StatusDiambil']==1){
				$output_pengambilan	="
				<div valign='top'>
					Nama	: $row[NamaPengambil]<br>
					KTP		: $row[NoKTPPengambil]<br>
					Waktu	: ".FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])."
					CSO		: $row[NamaPetugasPemberi]
				</div>
				";
			}
			else{
				$output_pengambilan	="
					<div align='center'><a href='' onClick='ambilDataPaket(\"$row[NoTiket]\");return false;'>Ambil Paket</a></div>
				";
			}
			
			$return	.="
				<tr bgcolor='dfdfdf'>
					<td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
					<td>$row[Asal]</td>
					<td>$row[Tujuan]</td>
					<td align='center'>$row[JamBerangkat]</td>
					<td align='left'>$row[KodeKendaraan] ($row[NoPolisi])</td>
					<td align='left'>$row[NamaSopir]</td>
					<td>$row[NoTiket]</td>
					<td valign='top'>
						Nama: $row[NamaPengirim]<br>
						Alamat: $row[AlamatPengirim]<br>
						Telp: $row[TelpPengirim]
					</td>
					<td valign='top'>
						Nama: $row[NamaPenerima]<br>
						Alamat: $row[AlamatPenerima]<br>
						Telp: $row[TelpPenerima]
					</td>
					<td>
						$output_pengambilan
					</td>
				</tr>";
			
			$jum_data++;
	  } 
	} 
	else{ 
		echo("Err :".__LINE__);exit;
	}
	
	if($jum_data==0){
		$return .="<tr><td colspan=11 class='banner' align='center'><h2>Data paket tidak ditemukan!</h2></td></tr>";
	}
	
	echo($return."</table>");

exit;

//AMBIL LIST HARGA PAKET======================================================================================================================	
case 'ambil_list_harga_paket':
	//Mengambil daftar harga paket
	include($adp_root_path .'/ClassPaketEkspedisi.php');
	$id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];
	
	$Paket= new Paket();
	
	$result= $Paket->ambilDaftarHarga($id_jurusan);
	
	if ($result){
	  $row = $db->sql_fetchrow($result);
		
		//return eval
		echo("
			document.getElementById('harga_kg_pertama_1').value 		= $row[HargaPaket1KiloPertama];
			document.getElementById('harga_kg_pertama_2').value 		= $row[HargaPaket2KiloPertama];
			document.getElementById('harga_kg_pertama_3').value 		= $row[HargaPaket3KiloPertama];
			document.getElementById('harga_kg_pertama_4').value 		= $row[HargaPaket4KiloPertama];
			document.getElementById('harga_kg_pertama_5').value 		= $row[HargaPaket5KiloPertama];
			document.getElementById('harga_kg_pertama_6').value 		= $row[HargaPaket6KiloPertama];
			document.getElementById('harga_kg_berikutnya_1').value 	= $row[HargaPaket1KiloBerikut];
			document.getElementById('harga_kg_berikutnya_2').value 	= $row[HargaPaket2KiloBerikut];
			document.getElementById('harga_kg_berikutnya_3').value 	= $row[HargaPaket3KiloBerikut];
			document.getElementById('harga_kg_berikutnya_4').value 	= $row[HargaPaket4KiloBerikut];
			document.getElementById('harga_kg_berikutnya_5').value 	= $row[HargaPaket5KiloBerikut];
			document.getElementById('harga_kg_berikutnya_6').value 	= $row[HargaPaket6KiloBerikut];
		");
	 
	} 
	else{      
		echo("Error ".__LINE__);
	}
	
exit;

//SPJ OPERATION===============================================
case "setdialogpembawa":
	
	$no_spj	= $HTTP_GET_VARS["nospj"];
	
	$data_spj	= $Cargo->ambilDataManifestByNo($no_spj);
	
	echo("<select id='optkurir' onclick='this.style.background=\"white\";'>".setComboKurir($data_spj['PetugasPickedUp'])."</select>");
exit;
}

//SET COMBO PROVINSI
$result=$Cargo->ambilDataProvinsi("1 ORDER BY NamaProvinsi");
while($data_provinsi=$db->sql_fetchrow($result)){
	$template->
		assign_block_vars(
			'PROV',
			array(
				'value'					=> $data_provinsi["Id"],
				'nama'					=> $data_provinsi["NamaProvinsi"],
			)
	);
}

include($adp_root_path . 'ClassKota.php');

$Kota = new Kota();

$template->assign_vars (
	array(
		'BCRUMP'    					=> '<a href="'.append_sid('main.'.$phpEx) .'">Home',
		'TGL_SEKARANG'				=> dateY_M_D(),
		'OPT_KOTA'						=> $Kota->setComboKota("BANDUNG"),
		'U_CHECKIN_PAKET'			=> "window.open('".append_sid('paket.checkin.'.$phpEx.'')."','_blank');",
		'U_LAPORAN_UANG'			=> "window.open('".append_sid('laporan.rekap.setoran.'.$phpEx.'')."','_blank');",
		/*'U_LAPORAN_UANG'			=> "window.open('".append_sid('laporan_rekap_uang_user.'.$phpEx.'')."','_blank');",*/
		'U_LAPORAN_PENJUALAN'	=> "Start('".append_sid('laporan_penjualan_paket_user.'.$phpEx.'')."');",
		'U_UBAH_PASSWORD'			=> append_sid('ubah_password.'.$phpEx.'')
		)
	);
	

$template->set_filenames(array('body' => 'reservasi.cargo/index.tpl')); 

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>