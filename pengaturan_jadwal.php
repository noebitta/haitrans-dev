<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');
		
// SESSION
$userdata = session_pagestart($user_ip,"master jadwal");
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['SPV_OPERASIONAL']))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

//PAGING
$idx_page 	= ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:$HTTP_POST_VARS['page'];
$idx_page 	= ($idx_page!='')?$idx_page:0;
$cari				= $HTTP_POST_VARS["txt_cari"]!=""?$HTTP_POST_VARS["txt_cari"]:$HTTP_GET_VARS["cari"];

$Jurusan= new Jurusan();
$Cabang	= new Cabang();
$Jadwal	= new Jadwal();
$Mobil	= new Mobil();

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;

  $kondisi_tambahan = "FlagOperasionalJurusan IN(0,1,2)";
	$result=$Jurusan->ambilDataByKodeCabangAsal($cabang_asal,$kondisi_tambahan);
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboJadwal($id_jurusan,$jadwal_dipilih){
	//SET COMBO jurusan
	global $db;
	global $Jadwal;
			
	$result=$Jadwal->setComboJadwal("",$id_jurusan,"AND FlagSubJadwal!=1");

  $opt_jadwal="";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($jadwal_dipilih!=$row['KodeJadwal'])?"":"selected";
			$opt_jadwal .="<option value='$row[KodeJadwal]' $selected>$row[KodeJadwal] $row[JamBerangkat]</option>";
		}
	}
	else{
		echo("silahkan pilih jurusan");exit;
	}
	
	return $opt_jadwal;
	//END SET COMBO JURUSAN
}

function setComboJam($jam_dipilih){
	$opt_jam="";
		
		for($jam=0;$jam<24;$jam++){
			$str_jam=substr("0".$jam,-2);
			
			$selected=($jam_dipilih!=$str_jam)?"":"selected";
			$opt_jam .="<option $selected value=$str_jam>$str_jam</option>";
		}
		
		return $opt_jam;
}

function setComboMenit($menit_dipilih){
	$opt_menit="";
	
	for($menit=0;$menit<60;$menit +=15){
		$str_menit	= substr("0".$menit,-2);
		$selected=($menit_dipilih!=$str_menit)?"":"selected";
		$opt_menit .="<option $selected value=$str_menit>$str_menit</option>";
	}
	
	return $opt_menit;
}

//BODY ==================================
$mode = $mode==""?"exp":$mode;
$data_layout	= $Mobil->getArrayLayout();

switch($mode){
  case "exp":
    // LIST
    $template->set_filenames(array('body' => 'jadwal/jadwal_body.tpl'));

    $temp_cari=str_replace("asal=","",$cari);
    if($temp_cari==$cari){
      $kondisi_asal = "";
    }
    else{
      $kondisi_asal = "AND f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) LIKE '%$temp_cari%' ";
      $cari=$temp_cari;
    }

    $temp_cari=str_replace("tujuan=","",$cari);
    if($temp_cari==$cari){
      $kondisi_tujuan = "";
    }
    else{
      $kondisi_tujuan = "AND f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) LIKE '%$temp_cari%' ";
      $cari=$temp_cari;
    }

    $kondisi	=($cari=="")?"":
      " WHERE (KodeJadwal LIKE '%$cari%'
			OR f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) LIKE '%$cari%'
			OR f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) LIKE '%$cari%')
			$kondisi_asal
			$kondisi_tujuan ";

    //PAGING======================================================
    $paging		= pagingData($idx_page,"KodeJadwal","tbl_md_jadwal","&cari=$cari",$kondisi,"pengaturan_jadwal.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
    //END PAGING======================================================

    $sql =
      "SELECT KodeJadwal,IdJurusan,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) as asal,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) as tujuan,
			JamBerangkat,JumlahKursi,FlagSubJadwal,KodeJadwalUtama,FlagAktif,IdLayout
		FROM tbl_md_jadwal $kondisi
		ORDER BY KodeJadwal LIMIT $idx_awal_record,$VIEW_PER_PAGE";

    $idx_check=0;


    if ($result = $db->sql_query($sql)){
      $i = $idx_page*$VIEW_PER_PAGE+1;
      while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
          $odd = 'even';
        }

        if($row['FlagAktif']){
          $status="<a href='' onClick='return ubahStatus(\"$row[KodeJadwal]\")'>Aktif</a>";
        }
        else{
          $odd	= "red";
          $status="<a href='' onClick='return ubahStatus(\"$row[KodeJadwal]\")'>Tidak Aktif</a>";
        }

        $idx_check++;

        $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"".strCleanJS($row[0])."\"/>";

        $kode_jadwal_utama="";
        if($row['FlagSubJadwal']==1){
          $kode_jadwal_utama	= " <font color='".($odd!='red'?'red':'white')."'>($row[KodeJadwalUtama])</font>";
        }

        $act 	="<a href=\"".append_sid("pengaturan_jadwal.php?mode=edit&cari=".$cari."&page=".$idx_page."&id=".$row[0])."\">Edit</a> +";
        $act .="<a  href='' onclick=\"return hapusData('$row[0]');\">Delete</a>";

        $template->
          assign_block_vars(
            'ROW',
            array(
              'odd'=>$odd,
              'check'=>$check,
              'no'=>$i,
              'kode'=>"<b>".$row['KodeJadwal']."</b>".$kode_jadwal_utama,
              'jurusan'=>$row['asal']."-".$row['tujuan'],
              'jam'=>$row['JamBerangkat'],
              'kursi'=>$row['IdLayout'] ." (".$row['JumlahKursi']." Kursi)",
              'aktif'=>$status,
              'action'=>$act
            )
          );

        $i++;
      }

      if($i-1<=0){
        $no_data	=	"<tr><td colspan=8 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
      }
    }
    else{
      //die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
      echo("Error :".__LINE__);exit;
    }

    $page_title	= "Pengaturan Jadwal";

    $template->assign_vars(array(
        'BCRUMP'    		=> '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx).'">Jadwal</a>',
        'U_JADWAL_ADD'		=> append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add&cari='.$cari.'&page='.$idx_page),
        'ACTION_CARI'		=> append_sid('pengaturan_jadwal.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'NO_DATA'				=> $no_data,
        'PAGING'				=> $paging
      )
    );
    break;

  case "add":
    // add

    $pesan = $HTTP_GET_VARS['pesan'];

    if($pesan==1){
      $pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
      $bgcolor_pesan="98e46f";
    }

    $page_title	= "Tambah Jadwal";

    $template->set_filenames(array('body' => 'jadwal/add_body.tpl'));
    $template->assign_vars(array(
      'BCRUMP'	=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?page='.$idx_page.'&cari='.$cari).'">Jadwal</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add&page='.$idx_page.'&cari='.$cari).'">Ubah Jadwal</a> ',
      'JUDUL'	=>'Tambah Data Jadwal',
      'MODE'   => 'save',
      'SUB'    => '0',
      'OPT_ASAL'=> $Cabang->setInterfaceComboCabang(),
      'OPT_JAM' => setComboJam("00"),
      'OPT_MENIT'=> setComboMenit("00"),
      'OPT_KURSI'=> $Mobil->setComboLayoutKursi($data_layout,""),
      'OPT_SUB_ASAL'=> $Cabang->setInterfaceComboCabang(),
      'SUB_JADWAL'=> 'none',
      'PESAN'						=> $pesan,
      'BGCOLOR_PESAN'		=> $bgcolor_pesan,
      'U_JADWAL_ADD_ACT'=> append_sid('pengaturan_jadwal.'.$phpEx),
      'CARI'					=> $cari,
      'PAGE'					=> $idx_page
     )
    );
    break;

  case "save":
    // aksi menambah jadwal
    $kode_jadwal  			= str_replace(" ","",strtoupper($HTTP_POST_VARS['kode_jadwal']));
    $kode_jadwal_old		= str_replace(" ","",$HTTP_POST_VARS['kode_jadwal_old']);
    $id_jurusan					= $HTTP_POST_VARS['opt_tujuan'];
    $asal								= $HTTP_POST_VARS['opt_asal'];
    $jam_berangkat			= $HTTP_POST_VARS['opt_jam'];
    $menit_berangkat  	= $HTTP_POST_VARS['opt_menit'];
    $kursi   						= $HTTP_POST_VARS['opt_kursi'];
    $flag_sub_jadwal		= $HTTP_POST_VARS['flag_sub_jadwal'];
    $kode_jadwal_utama	= $HTTP_POST_VARS['kode_jadwal_utama'];
    $sub_id_jurusan			= $HTTP_POST_VARS['opt_tujuan_asal'];
    $sub_asal						= $HTTP_POST_VARS['opt_sub_asal'];
    $biaya_sopir1				= $HTTP_POST_VARS['biayasopir1'];
    $biaya_sopir2				= $HTTP_POST_VARS['biayasopir2'];
    $biaya_sopir3				= $HTTP_POST_VARS['biayasopir3'];
    $is_biaya_sopir_kumulatif	= $HTTP_POST_VARS['isbiayasopirkumulatif']!='on'?0:1;
    /*$biaya_tol					= $HTTP_POST_VARS['biayatol'];
    $biaya_parkir				= $HTTP_POST_VARS['biayaparkir'];
    $biaya_bbm					= $HTTP_POST_VARS['biayabbm'];
    $is_bbm_voucher			= $HTTP_POST_VARS['isbbmvoucher']!='on'?0:1;*/
    $status_aktif   		= $HTTP_POST_VARS['aktif'];

    $terjadi_error=false;

    $waktu_berangkat=$jam_berangkat.":".$menit_berangkat;

    if($Jadwal->periksaDuplikasi($kode_jadwal) && $kode_jadwal!=$kode_jadwal_old){
      $pesan="<font color='white' size=3>Kode jadwal yang dimasukkan sudah terdaftar dalam sistem!</font>";
      $bgcolor_pesan="red";
      $terjadi_error=true;
    }
    else if($flag_sub_jadwal==1 && $kode_jadwal_utama==""){
      $pesan="<font color='white' size=3>Anda belum memilih jadwal utama!</font>";
      $bgcolor_pesan="red";
      $terjadi_error=true;
    }
    else{
      $data_layout  = $Mobil->getArrayLayout();
      $data_jurusan = $Jurusan->ambilDataDetail($id_jurusan);

      if($submode==0){
        $judul="Tambah Data Jadwal";
        $path	='<a href="'.append_sid('pengaturan_jadwal.'.$phpEx."?mode=add&cari=".$cari."&page=".$idx_page).'">Tambah Jadwal</a> ';

        if($Jadwal->tambah(
          $kode_jadwal,$id_jurusan,$data_jurusan['KodeCabangAsal'],
          $data_jurusan['KodeCabangTujuan'],$waktu_berangkat,$kursi,
          $data_layout[$kursi],$flag_sub_jadwal,$kode_jadwal_utama,
          $biaya_sopir1,$biaya_sopir2,$biaya_sopir3,
          $is_biaya_sopir_kumulatif,$biaya_tol,$biaya_parkir,
          $biaya_bbm,$is_bbm_voucher,$status_aktif)){

          //JIKA JADWAL YANG DIUBAH ADALAH JADWAL TRANSIT, MAKA AKAN MENGUPDATE KOLOM "VIA" DI JADWAL UTAMANYA
          if($flag_sub_jadwal==1){
            $Jadwal->updateViaJadwalUtama($kode_jadwal_utama);
          }

          redirect(append_sid("pengaturan_jadwal.php?mode=add&pesan=1&cari=$cari&page=$idx_page",true));
          //die_message('<h2>Data jadwal Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
        }
      }
      else{

        $judul="Ubah Data Jadwal";
        $path	='<a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=edit&id='.$kode_jadwal_old.'&cari='.$cari.'&page='.$idx_page).'">Ubah Jadwal</a> ';
        $kode_jadwal_utama  = $flag_sub_jadwal!=1?"":$kode_jadwal_utama;

        $data_jadwal_lama = $Jadwal->ambilDataDetail($kode_jadwal_old);

        if($Jadwal->ubah(
          $kode_jadwal_old,
          $kode_jadwal, $id_jurusan,$data_jurusan['KodeCabangAsal'],
          $data_jurusan['KodeCabangTujuan'],$waktu_berangkat,$kursi,
          $data_layout[$kursi],$flag_sub_jadwal,$kode_jadwal_utama,
          $biaya_sopir1,$biaya_sopir2,$biaya_sopir3,
          $is_biaya_sopir_kumulatif,$biaya_tol,$biaya_parkir,
          $biaya_bbm,$is_bbm_voucher,$status_aktif)){

          //JIKA JADWAL YANG DIUBAH ADALAH JADWAL TRANSIT, MAKA AKAN MENGUPDATE KOLOM "VIA" DI JADWAL UTAMANYA
          if($flag_sub_jadwal==1){
            $Jadwal->updateViaJadwalUtama($kode_jadwal_utama);
          }
          else if($data_jadwal_lama["FlagSubJadwal"]==1){
            $Jadwal->updateViaJadwalUtama($data_jadwal_lama["KodeJadwalUtama"]);
          }

          redirect(append_sid("pengaturan_jadwal.php?mode=edit&pesan=1&cari=$cari&page=$idx_page&id=$kode_jadwal",true));
          //die_message('<h2>Data jadwal Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=edit&id='.$kode_jadwal).'">Sini</a> Untuk Melanjutkan','');
        }
      }

      //exit;

    }
	  break;

  case "edit":
    // edit

    $id = $HTTP_GET_VARS['id'];

    $row	= $Jadwal->ambilDataDetail($id);

    if($row['FlagSubJadwal']==1){
      $row_sub				= $Jadwal->ambilDataDetail($row['KodeJadwalUtama']);
      $opt_sub_jadwal	= setComboJadwal($row_sub['IdJurusan'],$row['KodeJadwalUtama']);
    }

    $temp_var_aktif="status_aktif_".$row['FlagAktif'];
    $$temp_var_aktif="selected";

    $temp_var_sub_jadwal	="sub_jadwal_".$row['FlagSubJadwal'];
    $$temp_var_sub_jadwal	="selected";

    $page_title	= "Ubah Jadwal";

    if($HTTP_GET_VARS["pesan"]==1){
      $pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
      $bgcolor_pesan="98e46f";
    }

    $template->set_filenames(array('body' => 'jadwal/add_body.tpl'));
    $template->assign_vars(array(
        'BCRUMP'	=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?page='.$idx_page.'&cari='.$cari).'">Jadwal</a> | <a href="'.append_sid('pengaturan_jadwal.'.$phpEx.'?mode=edit&id='.$id.'&page='.$idx_page.'&cari='.$cari).'">Ubah Jadwal</a> ',
        'JUDUL'	=>'Ubah Data Jadwal',
        'MODE'   => 'save',
        'SUB'    => '1',
        'KODE_JADWAL_OLD'	=> $row['KodeJadwal'],
        'KODE_JADWAL'   	=> $row['KodeJadwal'],
        'ASAL'						=> $row['Asal'],
        'OPT_ASAL'				=> $Cabang->setInterfaceComboCabang($row['Asal']),
        'ID_JURUSAN'			=> $row['IdJurusan'],
        'OPT_JAM' 				=> setComboJam(substr($row['JamBerangkat'],0,2)),
        'OPT_MENIT'				=> setComboMenit(substr($row['JamBerangkat'],-2)),
        'OPT_KURSI'				=> $Mobil->setComboLayoutKursi($data_layout,$row['IdLayout']),
        'SUB_JADWAL_0'		=> $sub_jadwal_0,
        'SUB_JADWAL_1'		=> $sub_jadwal_1,
        'SUB_JADWAL'			=> ($row['FlagSubJadwal']!=1)?'none':'yes',
        'OPT_SUB_ASAL'		=> $Cabang->setInterfaceComboCabang($row_sub['Asal']),
        'SUB_ASAL'				=> $row_sub['Asal'],
        'SUB_ID_JURUSAN'	=> $row_sub['IdJurusan'],
        'KODE_SUB_JADWAL'	=> $row['KodeJadwalUtama'],
        'OPT_SUB_JADWAL'	=> $opt_sub_jadwal,
        'BIAYA_SOPIR1'		=> $row['BiayaSopir1'],
        'BIAYA_SOPIR2'		=> $row['BiayaSopir2'],
        'BIAYA_SOPIR3'		=> $row['BiayaSopir3'],
        'BIAYA_TOL'				=> $row['BiayaTol'],
        'BIAYA_PARKIR'		=> $row['BiayaParkir'],
        'BIAYA_BBM'				=> $row['BiayaBBM'],
        'IS_BIAYA_SOPIR_KUMULATIF'	=> $row['IsBiayaSopirKumulatif']!=1?"":"checked",
        'IS_VOUCHER_BBM'	=> $row['IsBBMVoucher']!=1?"":"checked",
        'AKTIF_1'					=> $status_aktif_1,
        'AKTIF_0'					=> $status_aktif_0,
        'U_JADWAL_ADD_ACT'=>append_sid('pengaturan_jadwal.'.$phpEx),
        'CARI'						=> $cari,
        'PAGE'						=> $idx_page,
        'PESAN'						=> $pesan,
        'BGCOLOR_PESAN'		=> $bgcolor_pesan,
       )
    );
    break;

  case "delete":
    // aksi hapus jadwal
    $list_jadwal = str_replace("\'","'",$HTTP_GET_VARS['list_jadwal']);

    $list_jadwal  = str_replace("'","",$list_jadwal);

    $kode_jadwals = explode(",",$list_jadwal);

    for($idx=0;$idx<count($kode_jadwals);$idx++){
      $data_jadwal  = $Jadwal->ambilDataDetail($kode_jadwals[$idx]);

      if($Jadwal->hapus("'$kode_jadwals[$idx]'")){
        if($data_jadwal["FlagSubJadwal"]==0){
          //Reset flag sub jadwal untuk jadwal transit ketika induknya dihapus
          $Jadwal->resetFlagSubJadwal($kode_jadwals[$idx]);
         }
        else{
          $Jadwal->updateViaJadwalUtama($data_jadwal["KodeJadwalUtama"]);
        }
      }
    }

    exit;

  case "ubahstatus":
    // aksi hapus jadwal
    $kode_jadwal = str_replace("\'","'",$HTTP_GET_VARS['kode_jadwal']);

    $Jadwal->ubahStatusAktif($kode_jadwal);

    exit;

  case "get_tujuan":
    $cabang_asal		= $HTTP_GET_VARS['asal'];
    $id_jurusan			= $HTTP_GET_VARS['jurusan'];

    /*if($cabang_asal==''){
      $result	= $Cabang->ambilData("","Nama,Kota","ASC LIMIT 0,1");

      if($result){
        while ($row = $db->sql_fetchrow($result)){
          $cabang_asal = $row['KodeCabang'];
        }
      }
      else{
        echo("Error :".__LINE__);exit;
      }

    }
    else{

    }*/

    $opt_cabang_tujuan  ="<select class='form-control' id='opt_tujuan' name='opt_tujuan'>";
    $opt_cabang_tujuan .= $cabang_asal==""?"<option value=''>silahkan pilih asal</option>":setComboCabangTujuan($cabang_asal,$id_jurusan);
    $opt_cabang_tujuan .="</select>";

    echo($opt_cabang_tujuan);

    exit;

  case "get_tujuan_utama":
    $cabang_asal		= $HTTP_GET_VARS['asal'];
    $id_jurusan			= $HTTP_GET_VARS['jurusan'];

    /*if($cabang_asal==''){
      $result	= $Cabang->ambilData("","Nama,Kota","ASC LIMIT 0,1");

      if($result){
        while ($row = $db->sql_fetchrow($result)){
          $cabang_asal = $row['KodeCabang'];
        }
      }
      else{
        echo("Error :".__LINE__);exit;
      }
    }*/

    $opt_cabang_tujuan  ="<select class='form-control' id='opt_tujuan_asal' name='opt_tujuan_asal' onChange='getUpdateJadwal(this.value)'>";
    $opt_cabang_tujuan .= $cabang_asal==""?"<option value=''>silahkan pilih asal</option>":setComboCabangTujuan($cabang_asal,$id_jurusan);
    $opt_cabang_tujuan .="</select>";

    echo($opt_cabang_tujuan);

    exit;

  case "get_jadwal":
    $kode_jadwal		= $HTTP_GET_VARS['kode_jadwal'];
    $id_jurusan			= $HTTP_GET_VARS['jurusan'];

    $opt_jadwal=
      "
      <select class='form-control' id='kode_jadwal_utama' name='kode_jadwal_utama' onChange='setDetailRute();'>".
      setComboJadwal($id_jurusan,$kode_jadwal)
      ."</select>";

    echo($opt_jadwal);

    exit;

  case "detailrute":
    $kode_jadwal	      = $HTTP_POST_VARS['kodejadwal'];
    $kode_jadwal_utama	= $HTTP_POST_VARS['kodejadwalutama'];

    $result = $Jadwal->ambilDetailRute($kode_jadwal,$kode_jadwal_utama);

    if($db->sql_numrows($result)<=0){
      exit;
    }

    $i=0;

    while($row=$db->sql_fetchrow(($result))){
      $odd =$i%2==0?"even":"odd";
      $i = 1-$i;

      $template->
        assign_block_vars(
          'ROW',
          array(
            'odd'     =>$odd,
            'jam'     =>$row["GroupJamBerangkat"],
            'outlet'  =>$row["NamaCabangAsal"]." (".$row["KodeCabangAsal"].")",
            'jadwal'  =>$row["GroupKodeJadwal"]
          )
        );
    }

    $template->set_filenames(array('body' => 'jadwal/detailrute.tpl'));
    $template->pparse('body');
    exit;
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>