<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,305);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

$page_title	= "Laporan Biaya Sopir";
$interface_menu_utama=false;

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kota_dipilih 	= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$start	= $tanggal_mulai==''?true:false;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$kota_dipilih	= ($kota_dipilih!='')?$kota_dipilih:"JAKARTA";
		
$kondisi_cari	=($cari=="")?
	"":
	" AND (f_sopir_get_nama_by_id(tbo.KodeSopir) LIKE '$cari%'
					OR f_kendaraan_ambil_nopol_by_kode(tbo.NoPolisi) LIKE '%$cari%'
					OR tbo.NoSPJ LIKE '%$cari%'
					OR f_user_get_nama_by_userid(IdPetugas) LIKE '%$cari%'
					OR CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) LIKE  '%$cari%'
				)";
		
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tbo.TglTransaksi":$sort_by;

// LIST
$template->set_filenames(array('body' => 'laporan_voucher_bbm/laporan_voucher_bbm_body.tpl')); 

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tms.KodeSopir","tbl_md_sopir tms",
"&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota_dipilih."&cari=".$cari."&sort_by=".$sort_by."&order=".$order,
$kondisi_cari,"laporan_voucher_bbm.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//mengambil query voucher BBM

$sql	=
	"SELECT
		tbo.TglTransaksi,
		CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) AS Jurusan,
		ts.JamBerangkat,
		tbo.NoSPJ,
		f_kendaraan_ambil_nopol_by_kode(tbo.NoPolisi) AS NoKendaraan,
		f_sopir_get_nama_by_id(tbo.KodeSopir) AS NamaSopir,
		Jumlah,
		f_user_get_nama_by_userid(IdPetugas) AS Pencetak
	FROM (tbl_biaya_op tbo INNER JOIN tbl_spj ts ON tbo.NoSPJ=ts.NoSPJ)
	WHERE FlagJenisBiaya=$FLAG_BIAYA_VOUCHER_BBM
		AND (tbo.TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan))='$kota_dipilih'
		$kondisi_cari
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'tanggal'=>dateparseD_Y_M(FormatMySQLDateToTgl($row['TglTransaksi'])),
					'jurusan'=>$row['Jurusan'],
					'jam'=>substr($row['JamBerangkat'],0,-3),
					'no_spj'=>$row['NoSPJ'],
					'kendaraan'=>$row['NoKendaraan'],
					'sopir'=>$row['NamaSopir'],
					'jumlah'=>number_format($row['Jumlah'],0,",","."),
					'pencetak'=>$row['Pencetak']
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_sopir',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota_dipilih.
										"&cari=".$cari."&sort_by=".$sort_by."&order=".$order."";
	
$script_cetak_pdf="Start('laporan_voucher_bbm_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_voucher_bbm_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//BEGIN KOMPONEN-KOMPONEN SORTING
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= 
	"&page=".$idx_page."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota_dipilih.
	"&cari=".$cari."&order=".$order_invert."";
//END KOMPONEN-KOMPONEN SORTING

$page_title	= "Voucher BBM";

//Ambil total tagihan BBM

$sql	=
	"SELECT SUM(Jumlah)
	FROM (tbl_biaya_op tbo INNER JOIN tbl_spj ts ON tbo.NoSPJ=ts.NoSPJ)
	WHERE FlagJenisBiaya=$FLAG_BIAYA_VOUCHER_BBM
		AND (tbo.TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan))='$kota_dipilih'
		$kondisi_cari";
	
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

include($adp_root_path . 'ClassKota.php');
$Kota = new Kota();

$summary	=
	"<b>Total Tagihan	= Rp. ".number_format($row[0],0,",",".")."</b>";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('menu_lap_keuangan.'.$phpEx.'?top_menu_dipilih=top_menu_lap_keuangan') .'">Home</a> | <a href="'.append_sid('laporan_voucher_bbm.'.$phpEx).'">Laporan Pembayaran Sopir</a>',
	'ACTION_CARI'		=> append_sid('laporan_voucher_bbm.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'OPT_KOTA'			=> $Kota->setComboKota($kota_dipilih),
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'OPT_SORT'			=> $opt_sort_by,
	'OPT_ORDER'			=> $opt_order,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=tgltransaksi'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Tgl Transaksi ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=jurusan'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Jurusan ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=jamberangkat'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan Jam Berangkat ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=nospj'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan No. SPJ ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=nokendaraan'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan No. Kendaraan ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=namasopir'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Nama Sopir ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=jumlah'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Jumlah ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=pencetak'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Pencetak ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>