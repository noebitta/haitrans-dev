<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraInsentifSopir.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassJurusan.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$idba 			= isset($HTTP_GET_VARS['idba'])? $HTTP_GET_VARS['idba'] : $HTTP_POST_VARS['idba'];

// LIST
$BeritaAcaraInsentifSopir= new BeritaAcaraInsentifSopir();
$Jurusan = new Jurusan();

$data_ba	= $BeritaAcaraInsentifSopir->ambilDetail($idba);

if($data_ba["Id"]==""){
	echo("Data tidak ditemukan!");
	exit;
}

$array_jurusan	= $Jurusan->ambilNamaJurusanByIdJurusan($data_ba['IdJurusan']);

$asal							= $array_jurusan['Asal'];
$tujuan						= $array_jurusan['Tujuan'];
$waktu_cetak_tiket= dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket']));

$template->set_filenames(array('body' => 'beritaacara.insentifsopir/release.cetak.tpl')); 

$template->
	assign_block_vars(
		'ROW',
		array(
			'NAMA_PERUSAHAAN'	=>$config["nama_perusahaan"],
			'NAMA_SOPIR'			=>$data_ba["NamaSopir"],
			'NIK'							=>$data_ba["KodeSopir"],
			'NOMINAL_INSENTIF'=>"Rp.".number_format($data_ba["NominalInsentif"],0,",","."),
			'CSO'							=>$data_ba["NamaReleaser"],
			'WAKTU_BAYAR'			=>dateparseWithTime(FormatMySQLDateToTglWithTime($data_ba['WaktuRelease'])),
			'APPROVER'				=>$data_ba["NamaApprover"],
		)
	);

$template->pparse('body');	
?>