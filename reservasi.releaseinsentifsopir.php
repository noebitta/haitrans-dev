<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraInsentifSopir.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassKota.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$expired_time = 120; //menit

// LIST
$template->set_filenames(array('body' => 'beritaacara.insentifsopir/release.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

switch($mode){
	case 'getasal':
		$Cabang	= new Cabang();
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		$Cabang	= new Cabang();
		
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;
	
	case 'bayar':
		$BeritaAcaraInsentifSopir	= new BeritaAcaraInsentifSopir();
		$BiayaOperasional = new BiayaOperasional();
		
		//MENYIMPAN BERITA ACARA
		$id_berita_acara	= $HTTP_POST_VARS["idba"];
		
		if($BeritaAcaraInsentifSopir->bayar($id_berita_acara,$userdata['user_id'],$userdata['nama'])){
			$data_ba = $BeritaAcaraInsentifSopir->ambilDetail($id_berita_acara);
			
			if($data_ba["IsRelease"]==1){
				$BiayaOperasional->tambah("","",$FLAG_BIAYA_SOPIR_INSENTIF,"",$data_ba["KodeSopir"],$data_ba["NominalInsentif"],$userdata["user_id"],$data_ba["KodeJadwal"],"");
			}
			echo(0);
		}
		else{
			echo(9);
		}
		
	exit;
}

$kondisi =	$cari==""?"":
	" AND (tpk.KodeJadwal LIKE '$cari%'
	  OR tbis.KodeSopir LIKE '$cari%' 
		OR tbis.NamaSopir LIKE '%$cari%'
		OR tbis.Keterangan LIKE '%$cari%' 
		OR tbis.NamaPembuat LIKE '%$cari%'
		OR tbis.NamaApprover LIKE '%$cari%'
		OR tbis.NamaReleaser LIKE '%$cari%')";

$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan))='$kota'":"";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan)='$asal'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tpk.IdJurusan)='$tujuan'":"";

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"TglBerangkat,JamBerangkat":$sort_by;


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tbis.Id","tbl_biaya_insentif_sopir tbis INNER JOIN  tbl_penjadwalan_kendaraan tpk ON tpk.IdPenjadwalan=tbis.IdPenjadwalan",
"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
"WHERE (tpk.TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"reservasi.releaseinsentifsopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	=
	"SELECT tpk.*,tbis.Id AS IdBA,tbis.KodeSopir,tbis.NamaSopir,Keterangan,
		NamaPembuat,WaktuBuat,NamaApprover,
		WaktuApprove,NominalInsentif,NamaReleaser,
		WaktuRelease,IsRelease
	FROM tbl_biaya_insentif_sopir tbis INNER JOIN  tbl_penjadwalan_kendaraan tpk ON tpk.IdPenjadwalan=tbis.IdPenjadwalan
	WHERE (tpk.TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND (TIME_TO_SEC(TIMEDIFF(WaktuBuat,NOW()))/60>=-$expired_time OR IsRelease)
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$show_header	= ($i%$config['repeatshowheader']!=0 && $i!=1)?"none":"";
	
	if($row['IsRelease']==0){
		$act= "<a href='#' onClick=\"false;bayarInsentif('".$row['IdBA']."');\">Bayar</a>";
	}
	else{
		$odd="green";
		$act= "<a href='#' onClick=\"false;Start('".append_sid("reservasi.releaseinsentifsopir.cetak.php?idba=".$row['IdBA'])."');\">Cetak</a>";
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'showheader'=>$show_header,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'tglberangkat'=>dateparse(FormatMySQLDateToTglWithTime($row['TglBerangkat'])),
				'kodejadwal'=>$row['KodeJadwal'],
				'jamberangkat'=>substr($row['JamBerangkat'],0,5),
				'status'=>$status,
				'dibuatoleh'=>$row['NamaPembuat'],
				'waktubuat'=>$row['WaktuBuat']==""?"":dateparse(FormatMySQLDateToTglWithTime($row['WaktuBuat'])),
				'sopir'=>$row['NamaSopir'],
				'jumlah'=>$row['NominalInsentif']==""?"":"Rp.".number_format($row['NominalInsentif'],0,",","."),
				'approver'=>$row['NamaApprover'],
				'waktuapproved'=>$row['WaktuApprove']==""?"":dateparse(FormatMySQLDateToTglWithTime($row['WaktuBuat'])),
				'remark'=>$row['Remark'],
				'keterangan'=>$row['Keterangan'],
				'releaser'=>$row['NamaReleaser'],
				'waktureleased'=>dateparse(FormatMySQLDateToTglWithTime($row['WaktuRelease'])),
				'act'=>$act,
			)
		);
	$i++;
}

if($i-1<=0){
	$template->assign_block_vars('ROW',array('showheader'=>''));
	$no_data	=	"<div style='width:100%;' class='yellow' align='center'><font size=3><b>data tidak ditemukan</b></font></div>";
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&sort_by=$sort_by&order=$order&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=$kota&asal=$asal&tujuan=$tujuan";								
$script_cetak_excel="Start('daftar_manifest_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&kota=$kota&asal=$asal&tujuan=$tujuan&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$array_sort	= 
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=Nama'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=KodeCabang'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=Alamat'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalOpenTrip'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalBerangkat'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangPerTrip'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenjualanTiket'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPaket'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenjualanPaket'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalDiscount'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalBiaya'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=Total'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangB'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangU'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangM'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangK'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangKK'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangG'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangR'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangV'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalPenumpangO'.$parameter_sorting)."',".
	"'".append_sid('reservasi.releaseinsentifsopir.php?sort_by=TotalTiket'.$parameter_sorting)."'";

$Kota = new Kota();

$page_title	= "Berita Acara Insentif Sopir";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('reservasi.releaseinsentifsopir.'.$phpEx).'">Berita Acara Insentif Sopir</a>',
	'ACTION_CARI'		=> append_sid('reservasi.releaseinsentifsopir.'.$phpEx),
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_KOTA'			=> $Kota->setComboKota($kota),
	'TXT_CARI'			=> $cari,
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'ARRAY_SORT'		=> $array_sort,
	'NO_DATA'				=> $no_data
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>