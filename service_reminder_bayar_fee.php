<?php

define('FRAMEWORK', true);
chdir("/var/www/html/dayax/");

$adp_root_path = './';

include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
$token	= md5("hanyauntukmuindonesiaku");
include($adp_root_path . 'ClassRekon.php');
include($adp_root_path . 'sendmail.php');

$pass   = $HTTP_GET_VARS['pass'];
$ip			= $_SERVER['REMOTE_ADDR'];

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if($ip!="" && $pass!="hanyauntukmuindonesiaku"){ 
  echo("DENIED");
	exit;
}
//#############################################################################

//INISIALISASI
$Rekon					= new Rekon();
$PengaturanUmum	= new PengaturanUmum();

$date = strtotime(date("d-m-Y"));
$date = strtotime("-1 day", $date);

$tgl_rekon	= date("Y-m-d",$date);
//$tgl_rekon	= date("Y-m-d");

$tgl_today	= date("d-m-Y");

$total_rekon_terhutang = $Rekon->ambilTotalRekonTerhutang();

echo("Total hutang:".$total_rekon_terhutang);

$content	=
		"Kepada Yth,<br>
		Divisi Keuangan Bimotrans<br>
		<br>
		Ini adalah email yang dikirim secara otomatis dari sistem TIKETUX.<br><br>
		Menurut data kami, total fee terhutang anda telah melebihi batas yang ditentukan, jika hingga pukul 14.00 siang ini anda tidak melakukan pembayaran,<br>
		maka sistem akan dinonaktifkan secara otomatis.<br>
		Demikian e-mail pemberitahuan ini, harap dimaklumi.<br>
		Terima kasih.<br>
		<br>
		Hormat kami,<br>
		TIKETUX<br>";

$isi_pesan	= 
	"Dear BIMO,total fee terhutang anda sudah melebihi limit,bila hingga pukul 14.00 anda tidak melakukan pembayaran,maka sistem akan dinonaktifkan secara otomatis.";
	
		
if($Rekon->isRekonClosedByTgl($tgl_rekon)){
	if($total_rekon_terhutang>$config['limit_utang_rekon'] && date('N')!=6){
		//KIRIM EMAIL WARNING KE BIMO
		echo (sendEmail($config['email_host'],$config['email_smtp_debug'],$config['email_smtp_auth'],$config['email_smtp_secure'],
			$config['email_port'],$config['email_username'],$config['email_password'],$config['email_display_name'],
			$config['email_to_rekon'],$config['email_to_name'],"PEMBERITAHUAN ".dateparse($tgl_today),$content));
		
		//CC KE BARTON
		echo (sendEmail($config['email_host'],$config['email_smtp_debug'],$config['email_smtp_auth'],$config['email_smtp_secure'],
			$config['email_port'],$config['email_username'],$config['email_password'],$config['email_display_name'],
			"barton@tiketux.com","Barton Yan Fari","PEMBERITAHUAN ".dateparse($tgl_today),$content));
		
		//MENGIRIM SMS KE BIMO
		$parameter= "username=".$sms_config['user']."&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=".$config['sms_to_bimo']."&message=".$isi_pesan;	
		$response	= sendHttpPost($sms_config['url'],$parameter);	
			
		$list_response	.= $config['sms_to_bimo']." | ".$response." <br>";
		
		//mengirim sms ke tiketux
		$parameter= "username=".$sms_config['user']."&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=".$config['sms_to_tiketux']."&message=".$isi_pesan;	
		$response	= sendHttpPost($sms_config['url'],$parameter);	
			
		$list_response	.= $config['sms_to_tiketux']." | ".$response." <br>";
			
	}
	exit;
}

$fee_transaksi	= $PengaturanUmum->ambilFeeTransaksi();

$total_fee = $Rekon->ambilTotalBayarFee($tgl_rekon,$fee_transaksi['FeeTiket'],$fee_transaksi['FeePaket'],$fee_transaksi['FeeSMS']);

if($total_rekon_terhutang+$total_fee>$config['limit_utang_rekon'] && date('N')!=6){
	//jika hari sabtu tidak ada pemblokiran
	//KIRIM EMAIL WARNING KE BIMO
	echo (sendEmail($config['email_host'],$config['email_smtp_debug'],$config['email_smtp_auth'],$config['email_smtp_secure'],
		$config['email_port'],$config['email_username'],$config['email_password'],$config['email_display_name'],
		$config['email_to_rekon'],$config['email_to_name'],"PEMBERITAHUAN ".dateparse($tgl_today),$content));
		
	//CC KE BARTON
	echo (sendEmail($config['email_host'],$config['email_smtp_debug'],$config['email_smtp_auth'],$config['email_smtp_secure'],
		$config['email_port'],$config['email_username'],$config['email_password'],$config['email_display_name'],
		"barton@tiketux.com","Barton Yan Fari","PEMBERITAHUAN  ".dateparse($tgl_today),$content));
	
	//MENGIRIM SMS KE BIMO
	$parameter= "username=".$sms_config['user']."&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=".$config['sms_to_bimo']."&message=".$isi_pesan;	
	$response	= sendHttpPost($sms_config['url'],$parameter);	
		
	$list_response	.= $config['sms_to_bimo']." | ".$response." <br>";
	
	//mengirim sms ke tiketux
	$parameter= "username=".$sms_config['user']."&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=".$config['sms_to_tiketux']."&message=".$isi_pesan;	
	$response	= sendHttpPost($sms_config['url'],$parameter);	
		
	$list_response	.= $config['sms_to_tiketux']." | ".$response." <br>";
}

echo($list_response);

?>