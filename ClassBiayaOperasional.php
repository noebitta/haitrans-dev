<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit; 
}
//#############################################################################

class BiayaOperasional{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function BiayaOperasional(){
		$this->ID_FILE="C-BOP";
	}
	
	//BODY
	
	function tambah(
		$no_spj,$kode_akun,$flag_jenis_biaya,
		$kode_kendaraan,$kode_sopir,$jumlah,
		$id_petugas,$kode_jadwal,$kode_cabang,
		$keterangan=""){
	  
		/*
		ID		: 001
		DESC	: menambahkan data biaya operasional 
		*/
		
		//kamus
		global $db;
			
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql	=
			"INSERT INTO tbl_biaya_op (
				NoSPJ, KodeAkun, FlagJenisBiaya,
				TglTransaksi, NoPolisi, KodeSopir,
				Jumlah, IdPetugas, IdJurusan,
				WaktuCatat,KodeCabang,Keterangan)
			VALUES (
				'$no_spj', '$kode_akun', '$flag_jenis_biaya',
				NOW(),'$kode_kendaraan','$kode_sopir',
				'$jumlah', '$id_petugas', f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'),
				NOW(),'$kode_cabang','$keterangan');";
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilBiayaOpByJurusan($id_jurusan){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				KodeAkunBiayaSopir, BiayaSopir, 
				KodeAkunBiayaTol, BiayaTol, 
				KodeAkunBiayaParkir, BiayaParkir, 
				KodeAkunBiayaBBM, LiterBBM,
				KodeAkunKomisiPenumpangSopir,KomisiPenumpangSopir,
				IsBiayaSopirKumulatif,IsVoucherBBM
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaOpByJurusan
	
	/*function ambilBiayaOpByKodeJadwal($kode_jadwal){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		By Jurusan
		*/
		
		//kamus
		/*global $db;
		
		$sql = 
			"SELECT 
				KodeAkunBiayaSopir, BiayaSopir, 
				KodeAkunBiayaTol, BiayaTol, 
				KodeAkunBiayaParkir, BiayaParkir, 
				KodeAkunBiayaBBM, BiayaBBM,
				KodeAkunKomisiPenumpangSopir,KomisiPenumpangSopir,
				KodeAkunKomisiPaketSopir,KomisiPaketSopir,
				IsBiayaSopirKumulatif,IsVoucherBBM
			FROM tbl_md_jurusan
			WHERE IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal');";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaOpByKodeJadwal
	*/
	
	function ambilBiayaOpByKodeJadwal($tgl_berangkat,$kode_jadwal){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		By Jadwal
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				BiayaSopir1,BiayaSopir2,BiayaSopir3,
				BiayaTol,BiayaParkir,BiayaBBM,
				IsBiayaSopirKumulatif,IsBBMVoucher,WEEKDAY('$tgl_berangkat') AS Hari
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal';";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		
		$data_biaya['BiayaTol']								= $row['BiayaTol'];
		$data_biaya['BiayaParkir']						= $row['BiayaParkir'];
		$data_biaya['BiayaBBM']								= $row['BiayaBBM'];
		$data_biaya['IsBiayaSopirKumulatif']	= $row['IsBiayaSopirKumulatif'];
		$data_biaya['IsVoucherBBM']						= $row['IsBBMVoucher'];
		$hari_berangkat												= $row['Hari'];
		
		$sql = 
			"SELECT COUNT(1)
			FROM tbl_md_libur
			WHERE TglLibur='$tgl_berangkat';";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row_libur=$db->sql_fetchrow($result);
				
		$data_biaya['BiayaSopir']	= $row_libur[0]==0 || $row_libur[0]==''?($hari_berangkat==0?$row['BiayaSopir1']:($hari_berangkat>=1 && $hari_berangkat<=3?$row['BiayaSopir2']:$row['BiayaSopir3'])):$row['BiayaSopir3']; 
		
		return $data_biaya;
		
	}//  END ambilBiayaOpByKodeJadwal
	
	function ambilBiayaByNoSPJ($no_spj){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj';";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		while($row=$db->sql_fetchrow($result)){
			$data_biaya["$row[FlagJenisBiaya]"]	+=$row["Jumlah"];
		}
		
		return $data_biaya;
		
	}//  END ambilBiayaByNoSPJ
	
	function ambilBiayaBBMByNoSPJ($no_spj){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $FLAG_BIAYA_BBM;
		
		$sql = 
			"SELECT Jumlah
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj' AND FlagJenisBiaya = '$FLAG_BIAYA_BBM';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaBBMByNoSPJ
	
	function ambilBiayaVoucherBBMByNoSPJ($no_spj){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $FLAG_BIAYA_VOUCHER_BBM;
		
		$sql = 
			"SELECT Jumlah
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj' AND FlagJenisBiaya = '$FLAG_BIAYA_VOUCHER_BBM';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaBBMByNoSPJ
	
	function isSudahCetakVoucherBBM($kode_kendaraan){
		
		/*
		Desc	:Mengembalikan true jika voucher BBM sudah dicetak di keberangkatan sebelumnya
		*/
		
		//kamus
		global $db;

		$sql = 
			"SELECT IsCetakVoucherBBM
			FROM tbl_spj
			WHERE NoPolisi = '$kode_kendaraan'
			ORDER BY TglBerangkat DESC,JamBerangkat DESC
			LIMIT 0,1";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		
		if($row[0]=="" || $row[0]==0){
			return false;
		}
		else{
			return true;
		}
		
	}//  END isSudahCetakVoucherBBM
	
	function ambilTotalBiayaByNoSPJTunai($no_spj){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $FLAG_BIAYA_BBM;
		
		$sql = 
			"SELECT SUM(Jumlah) AS JumlahBiaya
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj' AND FlagJenisBiaya NOT IN(5,6);";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilTotalBiayaByNoSPJ
	
	function tambahLogCetakUlangVoucher(
		$no_spj, $kode_jadwal, $tgl_berangkat, 
		$jam_berangkat, $mobil_dipilih, $sopir_dipilih,
		$nama_sopir,$jumlah_rupiah, $user_cetak,
		$nama_user_cetak,$user_approval, $nama_user_approval){
		
		//kamus
		global $db;
		
		//MENGHITUNG CETAKAN KE BERAPA
		
		$sql = 
			"SELECT IS_NULL(COUNT(1),0) AS jumlah
			FROM tbl_log_cetakulang_voucher_bbm
			WHERE NoSPJ='$no_spj';";
				
		if (!$result = $db->sql_query($sql)){
			//$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ );
		}
		
		$row=$db->sql_fetchrow($result);
		$cetakan_ke	= ($row[0]==""?0:$row[0])+1;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		$sql=
			"INSERT INTO tbl_log_cetakulang_voucher_bbm
				(NoSPJ,KodeJadwal,TglBerangkat,
				JamBerangkat,NoPolisi,KodeDriver,
				Driver,JumlahRupiah,WaktuCetak,
				UserCetak,NamaUserPencetak,UserApproval,
				NamaUserApproval,CetakanKe)
			VALUES(
				'$no_spj','$kode_jadwal','$tgl_berangkat',
				'$jam_berangkat','$mobil_dipilih','$sopir_dipilih',
				'$nama_sopir','$jumlah_rupiah',NOW(),
				'$user_cetak','$nama_user_cetak','$user_approval',
				'$nama_user_approval','$cetakan_ke');
		";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
}
?>