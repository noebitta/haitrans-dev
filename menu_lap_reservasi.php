<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,200); 
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################
// HEADER

$page_title	= "Menu Laporan Reservasi";
$interface_menu_utama=true;

// TEMPLATE
switch($userdata['user_level']){
	case $USER_LEVEL_INDEX["CSO"]:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX["CSO_PAKET"]:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX["ADMIN"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_lap_reservasi_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		
		//Menu Laporan Reservasi
		$template->assign_block_vars('menu_laporan_reservasi',array());
		//sub menu
		$template->assign_block_vars('menu_lap_reservasi_cso',array());
		$template->assign_block_vars('menu_lap_reservasi_cabang',array());
		$template->assign_block_vars('menu_lap_reservasi_jurusan',array());
		$template->assign_block_vars('menu_lap_reservasi_jam',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_reservasi_seluruh',array());
		$template->assign_block_vars('menu_lap_reservasi_kendaraan',array());
		$template->assign_block_vars('menu_lap_reservasi_sopir',array());
		$template->assign_block_vars('menu_lap_voucher',array());
		//END Laporan Reservasi
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
		
	break;
	
	case $USER_LEVEL_INDEX["MANAJEMEN"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_lap_reservasi_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		
		//Menu Laporan Reservasi
		$template->assign_block_vars('menu_laporan_reservasi',array());
		//sub menu
		$template->assign_block_vars('menu_lap_reservasi_cso',array());
		$template->assign_block_vars('menu_lap_reservasi_cabang',array());
		$template->assign_block_vars('menu_lap_reservasi_jurusan',array());
		$template->assign_block_vars('menu_lap_reservasi_jam',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_reservasi_seluruh',array());
		$template->assign_block_vars('menu_lap_reservasi_kendaraan',array());
		$template->assign_block_vars('menu_lap_reservasi_sopir',array());
		$template->assign_block_vars('menu_lap_voucher',array());
		//END Laporan Reservasi
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX["MANAJER"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_lap_reservasi_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		
		//Menu Laporan Reservasi
		$template->assign_block_vars('menu_laporan_reservasi',array());
		//sub menu
		$template->assign_block_vars('menu_lap_reservasi_cso',array());
		$template->assign_block_vars('menu_lap_reservasi_cabang',array());
		$template->assign_block_vars('menu_lap_reservasi_jurusan',array());
		$template->assign_block_vars('menu_lap_reservasi_jam',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_reservasi_seluruh',array());
		$template->assign_block_vars('menu_lap_reservasi_kendaraan',array());
		$template->assign_block_vars('menu_lap_reservasi_sopir',array());
		$template->assign_block_vars('menu_lap_voucher',array());
		//END Laporan Reservasi
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX["SPV_RESERVASI"] :
		$height_wrapper	= 100;
		
		$template->set_filenames(array('body' => 'menu_lap_reservasi_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		
		//Menu Laporan Reservasi
		$template->assign_block_vars('menu_laporan_reservasi',array());
		//sub menu
		$template->assign_block_vars('menu_lap_reservasi_cabang',array());
		//END Laporan Reservasi
		
		$template->assign_block_vars('menu_laporan_keuangan',array());
		
	break;
	
	case $USER_LEVEL_INDEX["SPV_OPERASIONAL"] :
		$height_wrapper	= 100;
		
		$template->set_filenames(array('body' => 'menu_lap_reservasi_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		
		//Menu Laporan Reservasi
		$template->assign_block_vars('menu_laporan_reservasi',array());
		//sub menu
		$template->assign_block_vars('menu_lap_reservasi_cabang',array());
		//END Laporan Reservasi
		$template->assign_block_vars('menu_laporan_paket',array());
		//MENU UTAMA
		$template->assign_block_vars('menu_pengaturan',array());
		
	break;

	case $USER_LEVEL_INDEX["KEUANGAN"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_lap_reservasi_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		
		//Menu Laporan Reservasi
		$template->assign_block_vars('menu_laporan_reservasi',array());
		//sub menu
		$template->assign_block_vars('menu_lap_reservasi_cso',array());
		$template->assign_block_vars('menu_lap_reservasi_cabang',array());
		$template->assign_block_vars('menu_lap_reservasi_jurusan',array());
		$template->assign_block_vars('menu_lap_reservasi_jam',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_reservasi_seluruh',array());
		$template->assign_block_vars('menu_lap_reservasi_kendaraan',array());
		$template->assign_block_vars('menu_lap_reservasi_sopir',array());
		$template->assign_block_vars('menu_lap_voucher',array());
		//END Laporan Reservasi
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_tiketux',array());
	break;
	
}

$template->assign_vars(array
  ( 'BCRUMP'    				=> '<a href="'.append_sid('menu_lap_reservasi.'.$phpEx.'?top_menu_dipilih=top_menu_lap_reservasi') .'">Home',
    'U_LAPORAN_CSO'			=>append_sid('laporan_penjualan_cso.'.$phpEx),
		'U_LAPORAN_CABANG'	=>append_sid('laporan_omzet_cabang.'.$phpEx),
		'U_LAPORAN_JURUSAN'	=>append_sid('laporan_omzet_jurusan.'.$phpEx),
		'U_LAPORAN_JADWAL'	=>append_sid('laporan_omzet_jadwal.'.$phpEx),
		'U_LAPORAN_OMZET'		=>append_sid('laporan_omzet.'.$phpEx),
		'U_LAPORAN_KENDARAAN'=>append_sid('laporan_omzet_kendaraan.'.$phpEx),
		'U_LAPORAN_SOPIR'		=>append_sid('laporan_omzet_sopir.'.$phpEx),
		'U_LAPORAN_VOUCHER'	=>append_sid('laporan_voucher.'.$phpEx),
		'HEIGT_WRAPPER'			=>$height_wrapper
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>