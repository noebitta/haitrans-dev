<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,400); 
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################
// HEADER

$page_title	= "Menu Tiketux";
$interface_menu_utama=true;
$template->set_filenames(array('body' => 'menu_tiketux_body.tpl'));

// TEMPLATE
switch($userdata['user_level']){
	case $USER_LEVEL_INDEX["ADMIN"]: 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
		
		//Menu tiketux
		$template->assign_block_vars('menu_tiketux',array());
	break;
	
	case $USER_LEVEL_INDEX["MANAJEMEN"]:
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
		
		//Menu tiketux
		$template->assign_block_vars('menu_tiketux',array());
	break;
	
	case $USER_LEVEL_INDEX["KEUANGAN"]:
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());

		//Menu tiketux
		$template->assign_block_vars('menu_tiketux',array());
	break;
	
}

$template->assign_vars(array
  ( 'BCRUMP'    						=> '<a href="'.append_sid('menu_tiketux.'.$phpEx.'?top_menu_dipilih=top_menu_tiketux') .'">Home',
    'U_LAPORAN_TIKETUX'			=>append_sid('laporan_keuangan_tiketux.'.$phpEx),
		'U_DEPOSIT_TIKETUX'			=>append_sid('deposit_tiketux.'.$phpEx),
		'U_LAPORAN_TRX_DEPOSIT'	=>append_sid('deposit_tiketux_lap_trx.'.$phpEx)
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>