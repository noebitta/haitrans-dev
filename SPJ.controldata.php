<?php
//
// SPJ
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassJadwal.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

//MENGAMBIL DATA SOPIR
$Sopir = new Sopir();

if($userdata['user_level']<=$USER_LEVEL_INDEX['SPV_RESERVASI']){
  $result=$Sopir->ambilData("","Nama,Alamat","ASC");
}
else{
  $sql =
    "SELECT tms.*
		 FROM tbl_md_sopir tms
		 WHERE FlagAktif=1 AND (tms.KodeSopir NOT IN(SELECT KodeDriver FROM tbl_spj WHERE TIMEDIFF(NOW(),TglSPJ)<'02:00:00') OR tms.KodeSopir='$kode_sopir_dipilih')
		 ORDER BY tms.Nama;";

  if (!$result = $db->sql_query($sql)){
    echo("Err $this->ID_FILE".__LINE__);exit;
  }
}

$sopir_dipilih=$HTTP_GET_VARS["sopirdipilih"];
$sopirs   = "";
$sopirs_id="";
$idx=0;

while ($row = $db->sql_fetchrow($result)){
  $sopirs     .="sopirs[$idx]=\"".strtoupper("$row[Nama] ($row[KodeSopir])")."\";";
  $sopirs_id  .="sopirsid[$idx]=\"".strtoupper($row["KodeSopir"])."\";";

  if($sopir_dipilih==$row["KodeSopir"]){
    $script_sopir_dipilih="manflistsopir.value=\"".strtoupper("$row[Nama] ($row[KodeSopir])")."\";";
  }

  $idx++;
}

//MENGAMBIL DATA MOBIL
$Mobil = new Mobil();

if($userdata['user_level']<=$USER_LEVEL_INDEX['SPV_RESERVASI']){
  $result=$Mobil->ambilDataForComboBox();
}
else{
  $sql =
    "SELECT tmk.KodeKendaraan, tmk.Jenis, tmk.Merek,tmk.NoPolisi,tmk.JumlahKursi
		 FROM tbl_md_kendaraan tmk
		WHERE FlagAktif=1 AND (tmk.KodeKendaraan NOT IN(SELECT NoPolisi FROM tbl_spj WHERE TIMEDIFF(NOW(),TglSPJ)<'02:00:00') OR tmk.KodeKendaraan='$kode_kendaraan')
		ORDER BY KodeKendaraan";

  if (!$result = $db->sql_query($sql)){
    echo("Err: $this->ID_FILE".__LINE__);exit;
  }
}

$mobil_dipilih=$HTTP_GET_VARS["mobildipilih"];
$mobils= "";
$mobils_id="";
$idx=0;

while ($row = $db->sql_fetchrow($result)){
  $mobils     .="mobils[$idx]=\"".strtoupper("$row[KodeKendaraan] ($row[JumlahKursi] Kursi) $row[Merek] $row[Jenis]")."\";";
  $mobils_id  .="mobilsid[$idx]=\"".strtoupper($row["KodeKendaraan"])."\";";

  if($mobil_dipilih==strtoupper($row["KodeKendaraan"])){
    $script_mobil_dipilih="manflistmobil.value=\"".strtoupper("$row[KodeKendaraan] ($row[JumlahKursi] Kursi) $row[Merek] $row[Jenis]")."\";";
    }

  $idx++;
}
//MEMERIKSA JIKA JADWAL ADALAH JADWAL TRANSIT, TIDAK DIPERBOLEHKAN MENGUBAH KENDARAAN
$Jadwal = new Jadwal();

$kode_jadwal  = $HTTP_GET_VARS["kodejadwal"];

$data_jadwal  = $Jadwal->ambilDataDetail($kode_jadwal);

$script_disabled  = $data_jadwal["FlagSubJadwal"]?"manflistmobil.disabled=true;manflistsopir.disabled=true;manflistmobil.setAttribute('class','manifestdisabled');manflistsopir.setAttribute('class','manifestdisabled');":"manflistmobil.disabled=false;manflistsopir.disabled=false;manflistmobil.setAttribute('class','');manflistsopir.setAttribute('class','');";

echo($sopirs.$sopirs_id.$mobils.$mobils_id.$script_sopir_dipilih.$script_mobil_dipilih.$script_disabled);
?>