<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassMemberTransaksi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode	= $HTTP_GET_VARS['mode'];
$mode	=($mode=='')?"blank":$mode;

$Member			= new Member();

$body="";

switch ($mode){
	case 'blank':
		
		if($HTTP_GET_VARS['error']==1){
			$pesan	= "<font color='red'>KARTU TIDAK TERDAFTAR</font>";
		}
		else if($HTTP_GET_VARS['error']==2){
			$pesan	= "<font color='red'>KARTU SUDAH TERDAFTAR PADA MEMBER LAIN</font>";
		}		
		
		$body=
			"<table width='80%'>
				<tr>
					<td colspan=3 align='center'>$pesan</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>Silahkan gesekkan kartu member ke alat pembaca</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action='".append_sid('member_registrasi_member_baru.'.$phpEx)."&mode=input_data&sub_mode=0' method='post'>
							<input type='password' id='id_kartu' name='id_kartu' />
						</form>
					</td>
				</tr><tr>
					<td colspan='3' align='center'> ATAU <BR> Silahkan masukkan nomor seri kartu member lalu kemudian tekan enter pada kolom isian dibawah ini</td>
				</tr>
				<tr>
					<td colspan='3' align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action=\"".append_sid('member_registrasi_member_baru.'.$phpEx)."&mode=input_data&sub_mode=1' method='post'>
							<input type='text' id='no_seri_kartu' name='no_seri_kartu' />
						</form>
					</td>
				</tr>
			</table>";
		
	break;
	
	//MEMINTA NOMINAL UANG =============================================================================================
	case 'input_data':	
		$sub_mode			= $HTTP_GET_VARS['sub_mode'];		
		$id_kartu			= $HTTP_POST_VARS['id_kartu'];		
		$id_kartu_md5	= md5($id_kartu);
		$no_seri_kartu= trim($HTTP_POST_VARS['no_seri_kartu']);
		
		//MENGAMBIL DATA MEMBER BY ID KARTU
		if($sub_mode==0){
			//jika pencarian data dilakukan berdasarkan ID KARTU
			$data_member=$Member->ambilDataByIdKartu($id_kartu_md5);
		}
		else{
			//jika pencarian data dilakukan berdasarkan NO SERI KARTU
			$data_member=$Member->ambilDataByNoSeriKartu($no_seri_kartu);
			$id_kartu=$data_member['id_kartu'];
		}
		
		if($data_member['status_member'] || $data_member['id_member']==''){
			if($data_member['id_member']==''){
				//JIKA ID KARTU TIDAK TERDAFTAR
				$error	=1;
			}
			else{
				//JIKA KARTU SUDAH DIAKTIFKAN
				$error	=2;
			}
			redirect(append_sid('member_registrasi_member_baru.'.$phpEx)."&error=".$error,true); 
		}
						
		$body 	= 		
		"<form id='f_data_anggota' name='f_data_anggota' method='post'>		
		<table bgcolor='FFFFFF' width='100%'>
			<tr>
				<td valign='top'align='center'>
					<table>
						<tr>
						  <td width='30%' class='kolomwajib'>ID Member</td>
							<td width='5%'>:</td>
							<td>$data_member[id_member]<input type='hidden' id='hdn_id_member' value='$data_member[id_member]'></td>
						</tr>
						<tr>
						  <td class='kolomwajib'>Tgl Registrasi</td>
							<td>:</td>
							<td>
								<input type='text' id='tgl_register' value='".dateNow()."' maxlength='10'/> (dd/mm/yyyy)
							</td>
						</tr>
						<tr>
						  <td class='kolomwajib'>Nama</td>
							<td>:</td>
							<td><input  name='nama' id='nama' type='text' maxlength='25'></td>
						</tr>
						<tr>
						  <td>Jenis kelamin</td>
							<td>:</td>
						  <td>
								<select name='jenis_kelamin' id='jenis_kelamin'>
									<option selected value='L'>Lelaki</option>
									<option value='P'>Perempuan</option>
								</select>
							</td>
						</tr>
						<tr>
						  <td>Tempat lahir</td>
							<td>:</td>
							<td><input name='tempat_lahir' id='tempat_lahir' type='text' maxlength='20'></td>
						</tr>
						<tr>
						  <td>Tgl Lahir</td>
						  <td>:</td>
						  <td>
								<input type='text' id='tgl_lahir' maxlength='10'/> (dd/mm/yyyy)
							</td>
						</tr>
						<tr>
							<td>Kategori Member</td>
							<td>:</td>
							<td>
								<select id='opt_kategori_member'>
									<option value='KARYAWAN'>KARYAWAN</option>
									<option value='UMUM'>UMUM</option>
									<option selected value='MAHASISWA'>MAHASISWA</option>
									<option value='MANULA'>MANULA</option>
								</select>
							</td>
						</tr>
						<tr>
						  <td colspan='3' height='20'></td>
						</tr>
						
					</table>
				</td>
				<td bgcolor='D0D0D0'></td>
				<td valign='top' align='center'>
					<table> 
						<tr>
						  <td><strong><u>No. KTP</u></strong></td>
							<td>:</td>
							<td><input name='no_ktp' id='no_ktp' type='text' maxlength='40'></td>
						</tr>
						<tr>
						  <td>Alamat</td>
							<td>:</td>
							<td><textarea name='alamat' id='alamat' cols='30' rows='2' ></textarea></td>
						</tr>
						<tr>
						  <td>Kota</td>
							<td>:</td>
							<td><input name='kota' id='kota' type='text' maxlength='20'></td>
						</tr>
						<tr>
						  <td>Kodepos</td>
							<td>:</td>
							<td><input name='kodepos' id='kodepos' type='text' maxlength='6' ></td>
						</tr>
						<tr>
						  <td>Telp rumah</td>
							<td>:</td>
							<td><input name='telp_rumah' id='telp_rumah' type='text' maxlength='20'></td>
						</tr>
						<tr>
						  <td>Handphone</td>
							<td>:</td>
							<td><input name='handphone' id='handphone' type='text' maxlength='20'></td>
						</tr>
						<tr>
						  <td>Email</td>
							<td>:</td>
							<td><input name='email' id='email' type='text' maxlength='50'></td>
						</tr>
						<tr>
						  <td colspan='3' height='20'></td>
						</tr>
						<tr>
						  <td class='red'>Password</td>
							<td>:</td>
							<td><input name='password' id='password' type='password' maxlength='6'></td>
						</tr>
						<tr>
						  <td class='red'>Konfirm Password</td>
							<td>:</td>
							<td><input name='konf_password' id='konf_password' type='password' maxlength='6'></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align='center' colspan='3' bgcolor='C0C0C0' valign='middle' height='40'>
					<input type='button' id='DataMemberBtnSimpan' onClick='showKonfirmasi();' value='&nbsp;Simpan&nbsp'>
					<input type='button' onClick=\"javascript:document.location='member.php?sid={SID}';\" id='DataMemberBtnNo' value='&nbsp;Batal&nbsp;'>
				</td>
			</tr>
		</table>
		</form>";
		
	break;
	
	//PROSES TOPUP =============================================================================================
	case 'simpan':	
		$id_member			= $HTTP_GET_VARS['id_member'];
		$nama						= $HTTP_GET_VARS['nama'];
		$jenis_kelamin	= $HTTP_GET_VARS['jenis_kelamin'];
		$tempat_lahir		= $HTTP_GET_VARS['tempat_lahir'];
		$tgl_lahir			= $HTTP_GET_VARS['tgl_lahir'];
		$kategori_member= $HTTP_GET_VARS['kategori_member'];
		$no_ktp					= $HTTP_GET_VARS['no_ktp'];
		$tgl_registrasi	= $HTTP_GET_VARS['tgl_register'];
		$alamat					= $HTTP_GET_VARS['alamat'];
		$kota						= $HTTP_GET_VARS['kota'];
		$kode_pos				= $HTTP_GET_VARS['kode_pos'];
		$telp_rumah			= $HTTP_GET_VARS['telp_rumah'];
		$handphone			= $HTTP_GET_VARS['handphone'];	
		$email					= $HTTP_GET_VARS['email'];	
		$password							=$HTTP_GET_VARS['password'];
		$konf_password				=$HTTP_GET_VARS['konf_password'];
		
		//melakukan pendaftaran member baru
		$TransaksiMember	= new TransaksiMember();
		
		$body=$TransaksiMember->pendaftaranMember(
			$id_member, $nama, $jenis_kelamin, $tempat_lahir, 
			$tgl_lahir, $no_ktp, $tgl_registrasi, $alamat, 
			$kota, $kode_pos, $telp_rumah, $handphone,$email, $kategori_member,$password);
		
		redirect(append_sid('member_registrasi_member_baru.'.$phpEx)."&mode=hasil&id_member=$id_member",true); 
	
	break;
	
	//HASIL==================================================================================================
	case 'hasil':		
		$id_member		= $HTTP_GET_VARS['id_member'];		
				
		//MENGAMBIL DATA MEMBER BY ID KARTU
		$data_member	= $Member->ambilDataDetail($id_member);
								
		$body 	= 
			"<input type='hidden' id='id_topup' value='$id_topup'/>
			<table width='80%'>
				<tr>
					<td width='20%'>ID MEMBER</td><td width='5%'>:</td><td width='75%'>$data_member[id_member]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td><td>$data_member[nama]</td>
				</tr>
				<tr>
					<td>Alamat</td><td>:</td><td>$data_member[alamat] $data_member[kota]</td>
				</tr>
					<tr>
					<td>Telp</td><td>:</td><td>$data_member[telp_rumah] / $data_member[handphone]</td>
				</tr>
				<tr>
					<td>Email</td><td>:</td><td>$data_member[email]</td>
				</tr>
				<tr>
					<td>Deposit</td><td>:</td><td>Rp. ".number_format($data_member['saldo'],0,",",".")."</td>
				</tr>
				<tr>
					<td>Point</td><td>:</td><td>".number_format($data_member['point'],0,",",".")."</td>
				</tr>
				<tr><td colspan=3 bgcolor='d0d0d0'></td></tr>
				<tr>
					<td colspan=3 align='center'>
						<h2><font color='green'>PENDAFTARAN BERHASIL!</font></h2>
					</td>
				</tr>
			</table>";
	break;
}

include($adp_root_path . 'includes/page_header.php');
$template->set_filenames(array('body' => 'member_registrasi_member_baru.tpl')); 
$template->assign_vars (
	array(
		'USERNAME'  	=>$userdata['username'],
	  'BCRUMP'    	=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_registrasi_member_baru.'.$phpEx).'">Pendaftaran member baru</a>',
	  'TOP_UP_AWAL' =>number_format($TOP_UP_AWAL,0,",","."),
		'BODY'				=> $body
	)
);
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>