<?php
/**
 * Kelimutu PHP 5 Framework
 *
 * Last updated: June 05, 2010, 11:16 PM
 *
 * @package   io
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0
 * @copyright Copyright (c) 2010 Lorensius W. L. T
 */

/**
 * KelimutuException
 */
require_once CLASS_DIR . '/kelimutu/exception/class.KelimutuException.php';

/**
 * Input output exception class
 *
 * @package   IO
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0
 * @copyright Copyright (c) 2010 Lorensius W. L. T
 *
 */
class IOException extends KelimutuException
{
    /**
     * Constructor.
     * Create new instance of this class
     *
     * @param string $message Error message
     * @param int $code Error code
     *
     * @return void
     */
    public function __construct($message, $code = 0)
    {
        $message = "IO [$code]: $message";

        parent::__construct($message, $code);
    }
}