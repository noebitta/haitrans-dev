<?php
/**
 * Kelimutu API Framework
 *
 * Last updated: May 29, 2012, 15:40
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 */

/**
 * Registry class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.Registry.php');

/**
 * Registry class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.Loader.php');

/**
 * Exception class
 */
require_once(CLASS_DIR . '/kelimutu/system/class.SystemException.php');

/**
 * Request timeout xception class
 */
require_once(CLASS_DIR . '/kelimutu/transport/class.RequestTimeoutException.php');

/**
 * Connection xception class
 */
require_once(CLASS_DIR . '/kelimutu/transport/class.ConnectionException.php');

/**
 * Array to XML
 */
require_once(CLASS_DIR . '/kelimutu/utils/class.ArrayXML.php');

/**
 * Authentication class
 */
require_once(CLASS_DIR . '/kelimutu/auth/class.Auth.php');


/**
 * Controller class
 *
 * @package   controller
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2011-2012 Lorensius W. L. T
 *
 */
class Controller
{
    /**
     * Content type
     *
     * @var string
     */
    private $_contentType;

	/**
	 * Enable debug
	 */
	private $_enableDebug;

    /**
	 * Authentication object
	 */
	private $_authObj;

	/**
     * Database object
     */
    protected $dbObj;

	/**
	 * User id
	 */
	private $_userId;

	/**
	 * User object
	 */
	private $_user;

	/**
	 * Log params
	 */
	protected $logParam = array();


    /**
     * Constructor.
     *
     * Create a new instance of this class
     */
    public function __construct()
    {
		global $cfg;

        $this->params = array();
        $this->dbObj  = Registry::get('db');

		if ($cfg['sys']['enableAuth']) {
			$this->_authObj	= new Auth();
		}

		$this->_parseLog();
    }

    /**
     * Set content type
     *
     * @param string $type Content type (xml, json, etc)
     *
     * @return void
     */
    public function setContentType($type)
    {
        $this->_contentType = $type;
    }

	/**
	 * Get content type
	 *
	 * @return string Content type
	 */
	public function getContentType()
	{
		return $this->_contentType;
	}

	/**
	 * Enable debugging
	 *
	 * @param boolean $enable Enable debug
	 *
	 * @return void
	 */
	public function enableDebug($enable)
	{
		$this->_enableDebug = $enable;
	}

	/**
	 * Load model
	 *
	 * @param string $model Model
	 *
	 * @return object Model object
	 */
	protected function loadModel($model)
    {
        try {
            $className = $model . 'Model';

            Loader::loadClass($className, ROOT_DIR . '/models');

            if (class_exists($className)) {
                $modelObj  = new $className;

                return $modelObj;
            } else {
                app_shutdown("Model class <i>$className</i> does not exists!");
            }
        } catch (SystemException $e) {
            app_shutdown("Model file <i>$className</i> does not exists!");
        }
    }

	/**
	 * Send response
	 *
	 * @param mixed $data Data (response body)
	 * @param int $status HTTP status code
	 * $param string $contentType Content type ()
	 *
	 * @return void
	 */
	public function sendResponse($data, $status=200, $message='', $contentType='')
	{
		global $cfg;

		$this->_contentType = (empty($contentType)) ? $this->_contentType : $contentType;
		$this->_contentType = strtolower($this->_contentType);

		if (!$this->_getContentType($this->_contentType)) {
			$status = 400;
			$data   = $this->_getStatusCodeMessage($status);
		}

		$statusHeader       = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);

		if ($status == '401') {
			header('WWW-Authenticate: OAuth realm="users"');
		}

		header($statusHeader);
		header('Content-type:' . $this->_getContentType($this->_contentType));

		if ($this->_contentType == 'json' || $this->_contentType == 'xml') {
			$data = (!$data) ? array() : $data;

			if ($status != 200) {
				$data = array('status' => 'ERROR', 'error' => $message);

				if($message == "Access denied, invalid access token"){
					$data = array('status' => 'INVALIDTOKEN', 'error' => $message);
				}
			}

			$data   = array_merge($data, array('url'  => HTTP::getCurrentRequestURL()));
			$data   = array_merge($data, array('time' => date('Y-m-d H:i:s')));
		} else if ($this->_contentType == 'html') {
			$site = $cfg['sys']['siteName'];
			$data = "<!DOCTYPE html><html><head><title>$site</title></head><body>$data</body></html>";
		}

		$errors = Error::getAll();

		if (sizeof($errors) && $this->_enableDebug && is_array($data)) {
			$data = array_merge($data, array('debug' => $errors));
		}

		if ($this->_contentType == 'xml') {
			$result = ArrayToXML::toXML($data, $cfg['sys']['responsePrefix']);
		} elseif ($this->_contentType == 'json') {
			$result = json_encode(array($cfg['sys']['responsePrefix'] => $data));
		} else {
			$result = $data;
		}

		$this->_log($result);

		$this->dbObj->disconnect();

		die($result);
	}

	/**
	 * Get log parameter.
	 *
	 * @return array Log params
	 */
	public function getLogParams()
	{
		return $this->logParams;
	}

	/**
	 * Set http request method (GET, POST, PUT, DELETE)
	 *
	 * @param string $request Request type
	 *
	 * @return void
	 */
	protected function setRequestMethod($request)
	{
		$request		= strtoupper($request);
		$requestMethod	= $_SERVER['REQUEST_METHOD'];

		if ($requestMethod != $request) {
			$this->sendResponse('', 405, 'The requested method is not allowed');
		}
	}

	/**
	 * Authenticate incoming request
	 *
	 * @param int $level Authentication type. Where 1=authenticate request for non user protected resource,
	 *                   2 = authenticate for user protected source
	 *
	 * @return void
	 */
	protected function authenticate($type)
	{
		global $cfg;

		if (!$cfg['sys']['enableAuth']) return;

		if ($this->_authObj) {
			$status = $this->_authObj->authenticate($type);

			if ($status != 1) {
				$this->sendResponse('', $this->_authObj->getHttpStatusCode($status),
									$this->_authObj->getStatusMessage($status));
			} else {
				$this->_userId = $this->_authObj->getUserId();

				if (!empty($this->_userId)) {
					$userModel 		= $this->loadModel('User');

					$this->_user	= $userModel->getDetail($this->_userId);

					if (empty($this->_user)) {
						$this->sendResponse('', '404', 'User tidak terdaftar');
					} else if ($this->_user->user_active == 0) {
						$this->sendResponse('', '403', 'Akses user diblok, silahkan kontak support/admin');
					}
				}
			}
		}
	}

	/**
	 * Authenticate
	 */
	protected function authenticateAll()
	{
		if (isset($_GET['auth_access_token'])) {
			$this->authenticate(2);
		} else {
			$this->authenticate(1);
		}
	}

	/**
	 * Get user id
	 *
	 * @return string User id
	 */
	protected function getUserId()
	{
		return $this->_userId;
	}

	/**
	 * Get use
	 *
	 * @return object User
	 */
	protected function getUser()
	{
		return $this->_user;
	}

	/**
	 * Fetch remote url using curl
	 *
	 * @param string $method Request method: GET or POST
	 * @param string $url Remote URL
	 * @param string $params Parameters for POST request
	 *
	 * @return string Response
	 */
	protected function callRemote($method, $url, $params='')
	{
		global $cfg;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		if ($method == 'POST') {
            $sentdata = $params;

            if (is_array($params) && sizeof($params)) {
                $sentdata = '';

                foreach($params as $name => $value) {
                    $sentdata .= $name .'=' . $value . '&';
                }

                $sentdata = rtrim($sentdata, '&');
            }

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $sentdata);
		}

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $cfg['sys']['connectTimeout']);
		curl_setopt($ch, CURLOPT_TIMEOUT, $cfg['sys']['timeOut']);

		$response 	= curl_exec($ch);
		$error		= curl_errno($ch);

		if ($error == CURLE_OPERATION_TIMEOUTED) {
			throw new RequestTimeoutException('');
		}

		if ($response === FALSE) {
			throw new ConnectionException('');
		}

		curl_close($ch);

		return $response;
	}

	/**
	 * Send request time out error
	 *
	 * @return void
	 */
	protected function requestTimeOut()
	{
		$response = array('status' => 'ERROR', 'error' => 'Request time out');

		$this->sendResponse($response);
	}

	/**
	 * Send internal transaction error (eg. database error etc)
	 *
	 * @return void
	 */
	protected function internalError()
	{
		$response = array('status' => 'ERROR', 'error' => 'Internal transaction error');

		$this->sendResponse($response);
	}

	/**
	 * Show error with custom message
	 *
	 * @param string $error Error message
	 *
	 * @return void
	 */
	protected function error($error)
	{
		$response = array('status' => 'ERROR', 'error' => $error);

		$this->sendResponse($response);
	}

	/**
	 * Get http status message
	 *
	 * @param string $status Status code
	 *
	 * @return string Status message
	 */
	private function _getStatusCodeMessage($status)
    {
        $codes = array( 200 => 'OK',
                        400 => 'Bad Request',
                        401 => 'Unauthorized',
                        402 => 'Payment Required',
                        403 => 'Forbidden',
                        404 => 'Not Found',
						405 => 'Method Not Allowed',
                        500 => 'Internal Server Error',
                        501 => 'Not Implemented');

        return (isset($codes[$status])) ? $codes[$status] : '';
    }

	/**
	 * Get http content type
	 *
	 * @param string $type Content type
	 *
	 * @return string Http content type
	 */
	private function _getContentType($type)
	{
		$types = array('xml' 	=> 'text/xml',
					   'json' 	=> 'application/json',
					   'html' 	=> 'text/html',
					   'text' 	=> 'text');

		return (isset($types[$type])) ? $types[$type] : '';
	}

	/**
	 * Get HTTP POST value
	 *
	 * @param string $key Key / parameter name
	 *
	 * @return mixed Parameter value
	 */
	protected function postParam($key)
    {
        return $this->_cleanXSS($_POST[$key]);
    }

	/**
	 * Get HTTP GET value
	 *
	 * @param string $key Key / parameter name
	 *
	 * @return mixed Parameter value
	 */
    protected function getParam($key)
    {
        return $this->_cleanXSS($_GET[$key]);
    }

	/**
	 * Clean xss attack
	 *
	 * @param mixed $val Value
	 *
	 * @return mixed Value
	 */
    private function _cleanXSS($val)
    {
        if (empty($val)) return '';

        # source from http://quickwired.com/kallahar/smallprojects/php_xss_filter_function.php
        $val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
        $search = 'abcdefghijklmnopqrstuvwxyz';
        $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $search .= '1234567890!@#$%^&*()';
        $search .= '~`";:?+/={}[]-_|\'\\';
        for ($i = 0; $i < strlen($search); $i++) {
            $val = preg_replace('/(&#[x|X]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val);
            $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val);
        }
        $ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
        $ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
        $ra = array_merge($ra1, $ra2);
        $found = true;
        while ($found == true) {
            $val_before = $val;
            for ($i = 0; $i < sizeof($ra); $i++) {
                $pattern = '/';
                for ($j = 0; $j < strlen($ra[$i]); $j++) {
                    if ($j > 0) {
                        $pattern .= '(';
                        $pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
                        $pattern .= '|(&#0{0,8}([9][10][13]);?)?';
                        $pattern .= ')?';
                    }
                    $pattern .= $ra[$i][$j];
                }
                $pattern .= '/i';
                $replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2);
                $val = preg_replace($pattern, $replacement, $val);
                if ($val_before == $val) {$found = false;}
            }
        }
        $allowedtags = "<strong><em><ul><li><pre><hr><blockquote><span>";
        $cstring = strip_tags($val, $allowedtags);
        $cstring = nl2br($cstring);
        return $cstring;
    }

	protected function formatArray($array)
    {
        if ($this->_contentType == 'xml') {
            return (sizeof($array)) ? $array : '';
        }

        return $array;
    }

	protected function logApiOut($agen, $method, $url, $params, $response)
	{
		global $cfg;

		try {
            $sentdata = $params;

            if (is_array($params) && sizeof($params)) {
                $sentdata = '';

                foreach($params as $name => $value) {
                    $sentdata .= $name .'=' . $value . '&';
                }

                $sentdata = rtrim($sentdata, '&');
            }

			// $response	= pg_escape_string($response);
			// $params		= pg_escape_string($sentdata);

			$response	= mysql_escape_string($response);
			$params		= mysql_escape_string($sentdata);

			$values 	= array();

			$values[] 	= "agen_kode  	= '$agen'";
			$values[] 	= "out_url  	= '$url'";
			$values[] 	= "out_params  	= E'$sentdata'";
			$values[] 	= "out_method  	= '$method'";
			$values[] 	= "out_response = E'$response'";
			$values[] 	= "out_time  	= '" . date('Y-m-d H:i:s') . "'";

			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_api_log_out', $values);
		} catch (DbException $e) {

		}
	}

	protected function logApiIn($response)
	{
		$this->_log($response);
	}
	
	private function _log($response)
	{
		global $cfg;

		// $response		= pg_escape_string($response);
		// $reqUri         = pg_escape_string(HTTP::getCurrentRequestURL());
		$response		= mysql_real_escape_string($response);
		$reqUri         = mysql_real_escape_string(HTTP::getCurrentRequestURL());
		$reqMethod      = $_SERVER['REQUEST_METHOD'];
		// $queryString    = pg_escape_string($this->_getQueryString());
		$queryString    = mysql_real_escape_string($this->_getQueryString());
		$clientId       = (HTTP::getVar('auth_client_id')) ? HTTP::getVar('auth_client_id') : '';
		$accessToken    = (HTTP::getVar('auth_access_token')) ? HTTP::getVar('auth_access_token') : '';
		$ipAddress      = $_SERVER['REMOTE_ADDR'];
		$userAgent      = $_SERVER['HTTP_USER_AGENT'];

		if (empty($this->_userId) && !empty($accessToken)) {
			if ($this->_authObj == null) {
				$this->_authObj = new Auth();
			}

			$tokenDetail = $this->_authObj->getUserToken($accessToken);

			if (!empty($tokenDetail)) {
				$this->_userId = $tokenDetail->user_id;
			}
		}

		$values          = array();

		$values[]        = "in_request_uri       = '$reqUri'";
		$values[]        = "in_request_query     = '$queryString'";
		$values[]        = "in_request_method    = '$reqMethod'";
		$values[]        = "in_client_id         = '$clientId'";
		$values[]        = "in_user_id           = '" . $this->_userId . "'";
		$values[]        = "in_access_token      = '$accessToken'";

		if (preg_match("/\*3trust\*/i", $userAgent)) {
			$splits      = preg_split("/\*3trust\*/i", $userAgent);
			$temps       = explode(',', $splits[1]);

			$values[]    = "in_client_type       = '$temps[0]'";
			$values[]    = "in_hh_manufacturer   = '$temps[1]'";
			$values[]    = "in_hh_model          = '$temps[2]'";
			$values[]    = "in_hh_os             = '$temps[3]'";
			$values[]    = "in_hh_sdk            = " . (int) $temps[4];
			$values[]    = "in_hh_screen         = '$temps[7]'";
			$values[]    = "in_app_version_code  = " . (int) $temps[5];
			$values[]    = "in_app_version_name  = '$temps[6]'";

			$values[]    = "in_hh_imei  		 = '$temps[8]'";
			$values[]    = "in_network_operator  = '$temps[9]'";
			$values[]    = "in_msisdn 			 = '$temps[10]'";
			$values[]    = "in_subscriber_id     = '$temps[11]'";

		} else {
			$values[]    = "in_client_type       = 'WEB/UNKNOWN'";
		}

		$values[]        = "in_ip_address        = '$ipAddress'";
		$values[]        = "in_user_agent        = '$userAgent'";
		$values[]        = "in_time              = '" . date('Y-m-d H:i:s') . "'";
		$values[]        = "in_response          = '$response'";

		try {
			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_api_log_in', $values);
		} catch (DbException $e) {
			Error::store('Controller', $e->getMessage());
		}
	}

	private function _getQueryString()
	{
		$string = ($_SERVER['REQUEST_METHOD'] == 'GET') ? $_GET : $_POST;

		$query 	= '';
		foreach($string as $name => $value) {
			if ($name == 'url') continue;

			$query .= $name.'='.htmlspecialchars(strip_tags(trim($value))).'&';
		}

		return $query;
	}

	private function _parseLog()
	{
		$userAgent      = $_SERVER['HTTP_USER_AGENT'];

		if (preg_match("/\*3trust\*/i", $userAgent)) {
			$splits      = preg_split("/\*3trust\*/i", $userAgent);
			$temps       = explode(',', $splits[1]);

			$values['type']    		= $temps[0];
			$values['manufacturer']	= $temps[1];
			$values['model']    	= $temps[2];
			$values['os']    		= $temps[3];
			$values['sdk']    		= $temps[4];
			$values['screen']    	= $temps[7];
			$values['versioncode']	= (int) $temps[5];
			$values['versionname']	= $temps[6];

			$values['imei']			= $temps[8];
			$values['networkop']	= $temps[9];
			$values['msisdn']		= $temps[10];
			$values['imsi']			= $temps[11];

			$this->logParam = $values;
		}
	}
}
?>