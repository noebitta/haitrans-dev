<?php
/**
 * Kelimutu API Framework
 *
 * Last updated: Feb 10, 2012, 12:00
 *
 * @package   auth
 * @author    Lorensius W. L. T <lorenz@londatiga.net>
 * @version   1.0.0
 * @copyright Copyright (c) 2012 Lorensius W. L. T
 */

/**
 * Loader class
 */
require_once CLASS_DIR . '/kelimutu/system/class.Registry.php';

/**
 * Request signature class
 */
require_once CLASS_DIR . '/kelimutu/auth/class.RequestSignature.php';


/**
 * Request authentication class.
 *
 * @package    auth
 * @author     Lorensius W. L. T <lorenz@londatiga.net>
 * @version    1.0.0
 * @copyright  Copyright (c) 2012 Lorensius W. L. T
 */
class Auth
{
    /**
     * Database object
     */
    private $_dbObj;

    /**
     * User id (save resolved user id from access token)
     */
    private $_userId;

    private $_signature;
    private $_serverSignature;


    public function __construct()
    {
        $this->_dbObj = Registry::get('db');
    }

    /**
     * Authenticate incoming request.
     *
     * @param int $type Authentication type.
     *                  Where:
     *                  1 = Authenticate incoming request without access token (standart).
     *                      Only check for signature and client id.
     *                  2 = Authentication incoming request with access token.
     *                      Check for signature, access token and client id.
     *
     * @return int Authentication status code:
     *             Status codes:
     *              1 = ok (valid)
     *              2 = missing parameters
     *              3 = invalid client id
     *              4 = invalid access token
     *              5 = invalid signature
     *              6 = duplicate request
     */
    public function authenticate($type)
    {
        global $cfg;

        $requestMethod = $_SERVER['REQUEST_METHOD'];
        $baseUrl       = $cfg['sys']['baseUrl'] . HTTP::getCurrentRequestURL();
        $params        = ($requestMethod == 'GET') ? $_GET : $_POST;
        $reqSignature  = new RequestSignature();

        if (is_array(($params))) {
            if (!array_key_exists('auth_signature', $params) || !array_key_exists('auth_client_id', $params) ||
                ((!array_key_exists('auth_nonce', $params) || !array_key_exists('auth_timestamp', $params)) &&
                 $cfg['sys']['authLevel'] == 1) ||
                (!array_key_exists('auth_access_token', $params) && $type == 2)) {

                return 2;
            } else {
                $clientId  = $params['auth_client_id'];
                $signature = $params['auth_signature'];

                unset($params['auth_signature']);
                unset($params['url']);

                //check client id
                $clientDetail = $this->_getClientDetail($clientId);
                $secret       = $clientDetail->client_secret;

                if (!$clientDetail) {
                    return 3;
                }

                //check access token
                if ($type == 2) {
                    $token         = $params['auth_access_token'];
                    $tokenDetail   = $this->getUserToken($token);
                    $this->_userId = $tokenDetail->user_id;
                    $secret        = $tokenDetail->access_token_secret;
                    $key           = $params['auth_access_token'];

                    if (!$tokenDetail) {
                        return 4;
                    }
                } else {
                    $token = sha1($clientId);
                    $key   = $clientId;
                }

                if ($cfg['sys']['authLevel'] == 1) {
                    $timestamp = $params['auth_timestamp'];
                    $nonce     = $params['auth_nonce'];

                    try {
                        $this->_checkServerNonce($clientId, $token, $timestamp, $nonce);
                    } catch (Exception $ex) {
                        return 6;
                    }
                }

                $signatureBase   = $reqSignature->createSignatureBase($requestMethod, $baseUrl, $params);

                $serverSignature = $reqSignature->createSignature($signatureBase, $key, $secret);
                $serverSignature = $reqSignature->urldecode($serverSignature);
                $signature       = $reqSignature->urldecode($signature);

                $signature     = str_replace(" ", "+", $signature);

                // if($signature != $serverSignature){
                //     $this->_sendSMS('08982879064', trim($signature) ."|". trim($serverSignature));
                // }

                $this->_signature = $signature;
                $this->_serverSignature = $serverSignature;

                return ($signature == $serverSignature) ? 1 : 5;
            }
        } else {
            return 2;
        }
    }

    /**
     * Get user id
     *
     * @return string User id
     */
    public function getUserId()
    {
        return $this->_userId;
    }

    /**
     * Get authentication status message
     *
     * @param int $status Authentication status code
     *
     * @return string Status message
     */
    public function getStatusMessage($status)
    {
        $message = '';

        switch ($status) {
            case 1:
                $message = 'Ok';
                break;

            case 2:
                $message = 'Access denied, incomplete authentication parameters';
                break;

            case 3:
                $message = 'Access denied, invalid client id';
                break;

            case 4:
                $message = 'Access denied, invalid access token';
                break;

            case 5:
                $message = 'Access denied, invalid signature'. $this->_signature .'#'. $this->_serverSignature;
                break;

            case 6:
                $message = 'Access denied, duplicate request';
                break;
        }

        return $message;
    }

    /**
     * Get http status code associated with authentication status code.
     *
     * @param int Authentication status code
     *
     * @return int Http status code
     */
    public function getHttpStatusCode($status)
    {
        if ($status == 1) {
            return 200;
        } else if ($status == 3 || $status == 4 || $status == 5) {
            return 401;
        } else {
            return 400;
        }
    }

    /**
     * Get client detail
     *
     * @param string $id Client id
     *
     * @return object Client detail
     */
    private function _getClientDetail($id)
    {
        global $cfg;

        $sql = "SELECT
                        *
                FROM
                        " . $cfg['sys']['tblPrefix'] . "_auth_client
                WHERE
                        client_id = '$id'";

        $res = null;

        try {
            $this->_dbObj->query($sql);

            $res = $this->_dbObj->fetch();
        } catch (DbException $ex) {
            Error::store('Auth', "Query error $sql");
        }

        return $res;
    }

    /**
     * Get user token
     *
     * @param string $token Access token
     *
     * @return object User token
     */
    public function getUserToken($token)
    {
        global $cfg;

        $sql = "SELECT
                        *
                FROM
                        " . $cfg['sys']['tblPrefix'] . "_auth_user_token
                WHERE
                        access_token = '$token'";

        $res = null;

        try {
            $this->_dbObj->query($sql);

            $res = $this->_dbObj->fetch();
        } catch (DbException $ex) {
            Error::store('Auth', "Query error $sql");
        }

        return $res;
    }

    /**
     * Check server nonce
     *
     * @param string $clientId Client id
     * @param string $token Access token
     * @param string $nonce Nonce
     *
     * @throws Exception when error occured
     */
    private function _checkServerNonce($clientId, $token, $timestamp, $nonce)
    {
        global $cfg;

        $skew = $cfg['sys']['timestampSkew'];

        $sql  = "SELECT
                        MAX(asn_timestamp), MAX(asn_timestamp) > $timestamp + $skew AS mt
                 FROM
                        " . $cfg['sys']['tblPrefix'] . "_auth_server_nonce
                 WHERE
                        asn_client_id = '$clientId'
                        AND
                        asn_token = '$token'";

        try {
            $this->_dbObj->query($sql);

            $data = $this->_dbObj->fetch();

            if ($data && $data->mt === 't') {
                throw new Exception('Timestamp is out of sequence. Request rejected');
            }

            $value = array("asn_client_id = '$clientId'",
                           "asn_token = '$token'",
                           "asn_timestamp = '$timestamp'",
                           "asn_nonce = '$nonce'");

            $this->_dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_auth_server_nonce', $value);
        } catch (DbException $ex) {
            throw new Exception('Duplicate timestamp/nonce combination, possible replay attack.  Request rejected.');
        }
    }

    private function _sendSMS($telp, $text){

        $userKey    = 'o23tc4';
        $passKey    = 'Or10n';
        $pesan      = $text;
        $url        = 'https://reguler.zenziva.net/apps/smsapi.php';
        $parameter  = 'userkey='. $userKey .'&passkey='. $passKey .'&nohp='. $telp .'&pesan='. $pesan;

        $length     = strlen($parameter);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        //curl_setopt($ch, CURLOPT_GET, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("application/x-www-form-urlencoded", "Content-length: $length"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        
        // return $response;

    }
}
?>