<?php

function curlPost($url, $postdata, $timeout=0) 
{
	$sentdata = '';
	foreach($postdata as $name=>$value){
		$sentdata .= $name.'='.$value.'&';
	}
	$sentdata = rtrim($sentdata,'&');
	$ssl_active = false;
	if(strtolower(substr($url,0,5))=="https"){
		$ssl_active = true;
	}
	$channel = curl_init($url);
	curl_setopt ($channel, CURLOPT_HEADER, false);
	curl_setopt ($channel, CURLINFO_HEADER_OUT, false);
	curl_setopt	($channel, CURLOPT_POST, 1);
	curl_setopt	($channel, CURLOPT_POSTFIELDS, $sentdata);
	curl_setopt	($channel, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt ($channel, CURLOPT_ENCODING, "");
    curl_setopt ($channel, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($channel, CURLOPT_AUTOREFERER, 1);
	curl_setopt ($channel, CURLOPT_URL, $url);
	if($ssl_active==true){
		//curl_setopt ($channel, CURLOPT_PORT , 443);
		curl_setopt ($channel, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($channel, CURLOPT_SSL_VERIFYHOST, 0);
	}
	if($timeout>0){
		curl_setopt ($channel, CURLOPT_CONNECTTIMEOUT, $timeout );
		curl_setopt ($channel, CURLOPT_TIMEOUT, $timeout );
	}
    curl_setopt ($channel, CURLOPT_MAXREDIRS, 10);
    curl_setopt ($channel, CURLOPT_VERBOSE, 1);
	$output = curl_exec($channel);
	curl_close 	($channel);
	return $output;
}

function curlGet($url, $timeout=0) 
{
	$ssl_active = false;

	if(strtolower(substr($url,0,5))=="https"){
		$ssl_active = true;
	}

	$channel = curl_init($url);

	curl_setopt ($channel, CURLOPT_HEADER, false);
	curl_setopt ($channel, CURLINFO_HEADER_OUT, false);
	curl_setopt	($channel, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt ($channel, CURLOPT_ENCODING, "");
    curl_setopt ($channel, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($channel, CURLOPT_AUTOREFERER, 1);
	curl_setopt ($channel, CURLOPT_URL, $url);

	if($timeout>0){
		curl_setopt ($channel, CURLOPT_CONNECTTIMEOUT, $timeout );
		curl_setopt ($channel, CURLOPT_TIMEOUT, $timeout );
	}

	if($ssl_active==true){
		//curl_setopt ($channel, CURLOPT_PORT , 443);
		curl_setopt ($channel, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($channel, CURLOPT_SSL_VERIFYHOST, 0);
	}
	
	curl_setopt ($channel, CURLOPT_MAXREDIRS, 10);
    curl_setopt ($channel, CURLOPT_VERBOSE, 1);
	$output = curl_exec($channel);
	curl_close 	($channel);
	return $output;
}

function sendSms($kodeBooking, $telpon, $pesan)
{
	global $cfg;

	$response = '';

	$token	= md5($cfg['sms']['password'].$cfg['sms']['username'].$cfg['sms']['keytoken']);

	$param	= array('username' 		=> $cfg['sms']['username'],
					'token' 		=> $token,
					'destination'	=> $telpon,
					'message'		=> $pesan);

	$response  	= curlPost($cfg['sms']['url'], $param, 0);

	logSms($kodeBooking, $telpon, $pesan, $response);

	return $response;
}

function logSms($kodeBooking, $telpon, $pesan, $response)
{
	global $cfg, $dbObj;

	try {
		$values 	= array();

		$values[]	= "res_kode_booking = '$kodeBooking'";
		$values[]   = "sl_phone 		= '$telpon'";
		$values[]	= "sl_message 		= '$pesan'";
		$values[]	= "sl_response 		= '$response'";
		$values[]	= "sl_time 			= '" . date('Y-m-d H:i:s') . "'";

		$dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_sms_log', $values);
	} catch (DbException $e) {}
}

function logApiOut($agen, $method, $url, $params, $response)
{
	global $cfg, $dbObj;

	try {
		$params		= flatParam($params);
		$response	= pg_escape_string($response);
		$params		= pg_escape_string($params);

		$values 	= array();

		$values[] 	= "agen_kode  	= '$agen'";
		$values[] 	= "out_url  	= '$url'";
		$values[] 	= "out_params  	= E'$params'";
		$values[] 	= "out_method  	= '$method'";
		$values[] 	= "out_response = E'$response'";
		$values[] 	= "out_time  	= '" . date('Y-m-d H:i:s') . "'";

		$dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_api_log_out', $values);
	} catch (DbException $e) {}
}

function flatParam($params)
{
	$str = '';

	foreach ($params as $k => $v) {
		$str .= "$k=$v&";
	}

	return $str;
}

function emailSupport($invoice, $type=0)
{
	global $cfg;

	if ($type == 1) {
		sendmail($cfg['sys']['emailSupport'], 'Support Tiketux', "[ALERT] Payment Failed $invoice", 
					"Pembayaran gagal untuk kode booking $invoice", '');
	} else {
		sendmail($cfg['sys']['emailSupport'], 'Support Tiketux', "[ALERT] Payment Update Problem $invoice", 
					"Terjadi gangguan saat melakukan update status pembayaran untuk kode booking $invoice", '');
	}
}


?>