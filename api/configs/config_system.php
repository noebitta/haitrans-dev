<?php
/**
 * System config file.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 *
 */

$cfg['sys']['dbDriver']       = 'MySQL';
$cfg['sys']['authType']       = 'db';
$cfg['sys']['tblPrefix']      = 'tbl';
$cfg['sys']['debug']          = false;
$cfg['sys']['theme']          = 'default';
$cfg['sys']['itemPerPage']    = 30;
$cfg['sys']['responsePrefix'] = 'tiketux';
$cfg['sys']['enableAuth']     = true;
$cfg['sys']['authLevel']      = 0;
$cfg['sys']['timestampSkew']  = 600;
 $cfg['sys']['baseUrl']        = 'http://localhost/haitrans/api'; //Localhost
//$cfg['sys']['baseUrl']        = 'http://192.168.1.102/tiketux-cso-api-baraya'; //Localhost
// $cfg['sys']['baseUrl']        = 'http://day-api.tiketux.com';
$cfg['sys']['webdir']         = '';
$cfg['sys']['weburl']         = '';
$cfg['sys']['tokenExpire']    = 365; //days
$cfg['sys']['connectTimeout'] = 30; //seconds
$cfg['sys']['timeOut']        = 30; //seconds
$cfg['sys']['phpexec']        = '/usr/local/bin/php';
$cfg['sys']['apiUrl']         = 'https://api.tiketux.com/dev1/'; //server dev
$cfg['sys']['client_id']	  = "DEVELOPER02"; //server dev
$cfg['sys']['client_secret']  = "79a5b997e737d1ed2d2c2ccd5809b963054004ff3"; // server dev

// $cfg['sys']['client_id']			= "WEB01"; //LIVE
// $cfg['sys']['client_secret']		= "cc6e66f3491d9395ce2fc869fc5d34580512838c6"; // LIVE

$cfg['sys']['emailFrom']      = 'noreply@tiketux.com';
$cfg['sys']['emailName']      = 'Tiketux';

?>