<?php
/**
 * Global varible.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */

$cfg['bulan']   = array(1 => 'Januari',
                        2 => 'Februari',
                        3 => 'Maret',
                        4 => 'April',
                        5 => 'Mei',
                        6 => 'Juni',
                        7 => 'Juli',
                        8 => 'Agustus',
                        9 => 'September',
                        10=> 'Oktober',
                        11=> 'November',
                        12=> 'Desember');

$cfg['hari']   =  array('Minggu',
                        'Senin',
                        'Selasa',
                        'Rabu',
                        'Kamis',
                        'Jumat',
                        'Sabtu');

$cfg['hariIn'] =  array(
                        1 => 'Senin',
                        2 => 'Selasa',
                        3 => 'Rabu',
                        4 => 'Kamis',
                        5 => 'Jumat',
                        6 => 'Sabtu',
                        7 => 'Minggu');

$cfg['haris']   =  array(
                        'Senin',
                        'Selasa',
                        'Rabu',
                        'Kamis',
                        'Jumat',
                        'Sabtu',
                        'Minggu');

$cfg['payment'] = array( 
                        0 => 'Tunai',
                        1 => 'Debit',
                        2 => 'Kredit',
                        3 => 'Voucher');

$cfg['paket']  = array( 
                        '1' => 'Dokumen',
                        '2' => 'Barang',
                        '3' => 'Bagasi',
                        '4' => 'Elekronik');

$cfg['ukuran_paket']  = array( 
                           '1' => 'Kecil',
                           '2' => 'Sedang',
                           '3' => 'Besar');

$cfg['payment_paket'] = array( 
                           0 => 'Tunai',
                           1 => 'Langganan',
                           2 => 'Bayar di Tujuan');

$cfg['layanan_paket']  = array( 
                           'P2P' => 'Port to Port',
                           'P2D' => 'Port to Door');

$cfg['userLevel']   = array(
                        '0.0'=>"Admin",
                        '1.0'=>"Manajemen",
                        '1.2'=>"Manajer",
                        '1.3'=>"Spv.Reservasi",
                        '1.4'=>"Spv.Operasional",
                        '2.0'=>"CSO",
                        '2.1'=>"CSO Paket",
                        '2.2'=>"CSO2",
                        '3.0'=>"Scheduler",
                        '4.0'=>"Kasir",
                        '5.0'=>"Keuangan",
                        '6.0'=>"Customer Care",
                        '7.0'=>"Mekanik"
                    );

$cfg['biaya']  = array(
                     0  => 'Jasa',
                     1  => 'Tol',
                     2  => 'Supir',
                     3  => 'BBM',
                     4  => 'Parkir',
                     5  => 'Insentif Supir',
                     6  => 'Voucher BBM',
                     7  => 'Tambahan BBM',
                     8  => 'Tambahan Tol',
                     9  => 'Lainya'
               );

$cfg['kursi'][12]   = array('rows'  => 4,
                            'cols'  => 4,
                            'kursi' => array(array('row' => 1, 'col' => 1, 'no' => '1', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '2', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 1, 'col' => 4, 'no' => '',  'tipe' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '3', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '4', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 2, 'col' => 4, 'no' => '5', 'tipe' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '6',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '7',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 3, 'col' => 4, 'no' => '8',  'tipe' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '9',  'tipe' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '10', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '11', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 4, 'no' => '12', 'tipe' => 'P'),
                                             )
                            );

$cfg['kursi'][13]   = array('rows'  => 6,
                            'cols'  => 3,
                            'kursi' => array(array('row' => 1, 'col' => 1, 'no' => '1', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'tipe' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 2, 'col' => 2, 'no' => '3', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '2', 'tipe' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '6',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '5',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '4',  'tipe' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 4, 'col' => 2, 'no' => '8',  'tipe' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '7', 'tipe' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 5, 'col' => 2, 'no' => '10',  'tipe' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '9', 'tipe' => 'P'),

                                             array('row' => 6, 'col' => 1, 'no' => '13',  'tipe' => 'P'),
                                             array('row' => 6, 'col' => 2, 'no' => '12', 'tipe' => 'P'),
                                             array('row' => 6, 'col' => 3, 'no' => '11', 'tipe' => 'P'),
                                             )
                            );

$cfg['kursi'][14]   = array('rows'  => 5,
                            'cols'  => 3,
                            'kursi' => array(array('row' => 1, 'col' => 1, 'no' => '1', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '2', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'tipe' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '3', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '4', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '5', 'tipe' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '6',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '7',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '8',  'tipe' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '9',  'tipe' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '10', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '11', 'tipe' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '12',  'tipe' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '13', 'tipe' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '14', 'tipe' => 'P'),
                                             )
                            );

$cfg['kursi'][16]   = array('rows'  => 6,
                            'cols'  => 3,
                            'kursi' => array(array('row' => 1, 'col' => 1, 'no' => '1', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '2', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'tipe' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '3',  'tipe' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '4',  'tipe' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '5',  'tipe' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '6',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '7',   'tipe' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '8',  'tipe' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '9',  'tipe' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '10',   'tipe' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '11',  'tipe' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 5, 'col' => 2, 'no' => '12',   'tipe' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '13',   'tipe' => 'P'),

                                             array('row' => 6, 'col' => 1, 'no' => '14',   'tipe' => 'P'),
                                             array('row' => 6, 'col' => 2, 'no' => '15',   'tipe' => 'P'),
                                             array('row' => 6, 'col' => 3, 'no' => '16',   'tipe' => 'P'),
                                            )
                            );


$cfg['kursi'][29]   = array('rows'  => 7,
                            'cols'  => 5,
                            'kursi' => array(array('row' => 1, 'col' => 1, 'no' => '1', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 2, 'no' => '2', 'tipe' => 'P'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 1, 'col' => 4, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 1, 'col' => 5, 'no' => '', 'tipe' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '5', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '6', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 2, 'col' => 4, 'no' => '3', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 5, 'no' => '4', 'tipe' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '9',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '10', 'tipe' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 3, 'col' => 4, 'no' => '7', 'tipe' => 'P'),
                                             array('row' => 3, 'col' => 5, 'no' => '8', 'tipe' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '13', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '14', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 4, 'col' => 4, 'no' => '11', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 5, 'no' => '12', 'tipe' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '17', 'tipe' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '18', 'tipe' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 5, 'col' => 4, 'no' => '15', 'tipe' => 'P'),
                                             array('row' => 5, 'col' => 5, 'no' => '16', 'tipe' => 'P'),

                                             array('row' => 6, 'col' => 1, 'no' => '21', 'tipe' => 'P'),
                                             array('row' => 6, 'col' => 2, 'no' => '22', 'tipe' => 'P'),
                                             array('row' => 6, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 6, 'col' => 4, 'no' => '19', 'tipe' => 'P'),
                                             array('row' => 6, 'col' => 5, 'no' => '20', 'tipe' => 'P'),

                                             array('row' => 7, 'col' => 1, 'no' => '23', 'tipe' => 'P'),
                                             array('row' => 7, 'col' => 2, 'no' => '24', 'tipe' => 'P'),
                                             array('row' => 7, 'col' => 3, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 7, 'col' => 4, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 7, 'col' => 5, 'no' => '', 'tipe' => 'K'),

                                             array('row' => 8, 'col' => 1, 'no' => '29', 'tipe' => 'P'),
                                             array('row' => 8, 'col' => 2, 'no' => '28', 'tipe' => 'P'),
                                             array('row' => 8, 'col' => 3, 'no' => '27', 'tipe' => 'P'),
                                             array('row' => 8, 'col' => 4, 'no' => '26', 'tipe' => 'P'),
                                             array('row' => 8, 'col' => 5, 'no' => '25', 'tipe' => 'P'),
                                            )
                            );

$cfg['kursi'][47]   = array('rows'  => 12,
                            'cols'  => 5,
                            'kursi' => array(array('row' => 1, 'col' => 1, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 1, 'col' => 2, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 1, 'col' => 3, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 1, 'col' => 4, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 1, 'col' => 5, 'no' => '', 'tipe' => 'S'),

                                             array('row' => 2, 'col' => 1, 'no' => '1', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 2, 'no' => '2', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 3, 'no' => '',  'tipe' => 'K'),
                                             array('row' => 2, 'col' => 4, 'no' => '3', 'tipe' => 'P'),
                                             array('row' => 2, 'col' => 5, 'no' => '4', 'tipe' => 'P'),

                                             array('row' => 3, 'col' => 1, 'no' => '5',  'tipe' => 'P'),
                                             array('row' => 3, 'col' => 2, 'no' => '6', 'tipe' => 'P'),
                                             array('row' => 3, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 3, 'col' => 4, 'no' => '7', 'tipe' => 'P'),
                                             array('row' => 3, 'col' => 5, 'no' => '8', 'tipe' => 'P'),

                                             array('row' => 4, 'col' => 1, 'no' => '9', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 2, 'no' => '10', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 4, 'col' => 4, 'no' => '11', 'tipe' => 'P'),
                                             array('row' => 4, 'col' => 5, 'no' => '12', 'tipe' => 'P'),

                                             array('row' => 5, 'col' => 1, 'no' => '13', 'tipe' => 'P'),
                                             array('row' => 5, 'col' => 2, 'no' => '14', 'tipe' => 'P'),
                                             array('row' => 5, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 5, 'col' => 4, 'no' => '15', 'tipe' => 'P'),
                                             array('row' => 5, 'col' => 5, 'no' => '16', 'tipe' => 'P'),

                                             array('row' => 6, 'col' => 1, 'no' => '17', 'tipe' => 'P'),
                                             array('row' => 6, 'col' => 2, 'no' => '18', 'tipe' => 'P'),
                                             array('row' => 6, 'col' => 3, 'no' => '',   'tipe' => 'K'),
                                             array('row' => 6, 'col' => 4, 'no' => '19', 'tipe' => 'P'),
                                             array('row' => 6, 'col' => 5, 'no' => '20', 'tipe' => 'P'),

                                             array('row' => 7, 'col' => 1, 'no' => '21', 'tipe' => 'P'),
                                             array('row' => 7, 'col' => 2, 'no' => '22', 'tipe' => 'P'),
                                             array('row' => 7, 'col' => 3, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 7, 'col' => 4, 'no' => '23', 'tipe' => 'P'),
                                             array('row' => 7, 'col' => 5, 'no' => '24', 'tipe' => 'P'),

                                             array('row' => 8, 'col' => 1, 'no' => '25', 'tipe' => 'P'),
                                             array('row' => 8, 'col' => 2, 'no' => '26', 'tipe' => 'P'),
                                             array('row' => 8, 'col' => 3, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 8, 'col' => 4, 'no' => '27', 'tipe' => 'P'),
                                             array('row' => 8, 'col' => 5, 'no' => '28', 'tipe' => 'P'),

                                             array('row' => 9, 'col' => 1, 'no' => '29', 'tipe' => 'P'),
                                             array('row' => 9, 'col' => 2, 'no' => '30', 'tipe' => 'P'),
                                             array('row' => 9, 'col' => 3, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 9, 'col' => 4, 'no' => '31', 'tipe' => 'P'),
                                             array('row' => 9, 'col' => 5, 'no' => '32', 'tipe' => 'P'),

                                             array('row' => 10, 'col' => 1, 'no' => '33', 'tipe' => 'P'),
                                             array('row' => 10, 'col' => 2, 'no' => '34', 'tipe' => 'P'),
                                             array('row' => 10, 'col' => 3, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 10, 'col' => 4, 'no' => '35', 'tipe' => 'P'),
                                             array('row' => 10, 'col' => 5, 'no' => '36', 'tipe' => 'P'),

                                             array('row' => 11, 'col' => 1, 'no' => '37', 'tipe' => 'P'),
                                             array('row' => 11, 'col' => 2, 'no' => '38', 'tipe' => 'P'),
                                             array('row' => 11, 'col' => 3, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 11, 'col' => 4, 'no' => '39', 'tipe' => 'P'),
                                             array('row' => 11, 'col' => 5, 'no' => '40', 'tipe' => 'P'),                                             

                                             array('row' => 12, 'col' => 1, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 12, 'col' => 2, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 12, 'col' => 3, 'no' => '', 'tipe' => 'K'),
                                             array('row' => 12, 'col' => 4, 'no' => '41', 'tipe' => 'P'),
                                             array('row' => 12, 'col' => 5, 'no' => '42', 'tipe' => 'P'),

                                             array('row' => 13, 'col' => 1, 'no' => '47', 'tipe' => 'P'),
                                             array('row' => 13, 'col' => 2, 'no' => '46', 'tipe' => 'P'),
                                             array('row' => 13, 'col' => 3, 'no' => '45', 'tipe' => 'P'),
                                             array('row' => 13, 'col' => 4, 'no' => '44', 'tipe' => 'P'),
                                             array('row' => 13, 'col' => 5, 'no' => '43', 'tipe' => 'P'),
                                            )
                            );


?>