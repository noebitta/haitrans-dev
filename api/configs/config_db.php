<?php
/**
 * Database config file
 *
 * @author Yudi Setiadi Permana
 */

//LOCAL DEV
$cfg['db']['host']     = 'localhost';
$cfg['db']['name']     = 'haitrans';
$cfg['db']['user']     = 'root';
$cfg['db']['password'] = '';

//ONLINE DEV
// $cfg['db']['host']     = 'localhost';
// $cfg['db']['name']     = 'tiketux_daytrans_dev';
// $cfg['db']['user']     = 'dev';
// $cfg['db']['password'] = 'TiketuxD3v';
