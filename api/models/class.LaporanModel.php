<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class LaporanModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= '';
		$this->_id 		= '';
	}
	
	public function getSetoranReservasiUser($user) {
		
		global $cfg;

		$sql = "SELECT 
					COUNT(NoTiket) as qty, IS_NULL(SUM(Discount), 0) as diskon, IS_NULL(SUM(Total), 0) as total
  				FROM 
  					tbl_reservasi
  				WHERE 
  					CetakTiket 			= '1' AND 
  					FlagBatal 		   != '1' AND 
  					PetugasCetakTiket	= '$user' AND 
  					(IdSetoran = '' OR IdSetoran IS NULL)
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranPaketUser($user) {
		
		global $cfg;

		$sql = "SELECT 
					COUNT(NoTiket) as qty, IS_NULL(SUM(TotalBayar), 0) as total
  				FROM 
  					tbl_paket
  				WHERE 
  					CetakTiket 		= '1' AND 
  					FlagBatal 	   != '1' AND 
  					PetugasPenjual 	= '$user' AND 
  					(IdSetoran = '' OR IdSetoran IS NULL)
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranBiayaUser($user) {
		
		global $cfg;

		$sql = "SELECT 
					IS_NULL(SUM(Jumlah), 0) as total
  				FROM 
  					tbl_biaya_op
  				WHERE 
  					IdPetugas 		= '$user' AND 
  					FlagJenisBiaya != '6' AND 
  					(IdSetoran = '' OR IdSetoran IS NULL)
				";

				// FlagJenisBiaya = 6 -> kecuali Flag voucher BBM

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getRekapSetoranUser($user, $tgl_awal, $tgl_akhir) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_user_setoran	
  				WHERE 
  					IdUser  			= '$user' AND 
  					date(WaktuSetoran) >= '$tgl_awal' AND 
  					date(WaktuSetoran) <= '$tgl_akhir'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranReservasiList($user) {
		
		global $cfg;

		$sql = "SELECT 
					* 
  				FROM 
  					tbl_reservasi
  				WHERE 
  					CetakTiket 			= '1' AND 
  					FlagBatal 		   != '1' AND 
  					PetugasCetakTiket	= '$user' AND 
  					(IdSetoran = '' OR IdSetoran IS NULL)
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranPaketList($user) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_paket
  				WHERE 
  					CetakTiket 		= '1' AND 
  					FlagBatal 	   != '1' AND 
  					PetugasPenjual 	= '$user' AND 
  					(IdSetoran = '' OR IdSetoran IS NULL)
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranBiayaList($user) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_biaya_op
  				WHERE 
  					IdPetugas 		= '$user' AND 
  					FlagJenisBiaya != '6' AND 
  					(IdSetoran = '' OR IdSetoran IS NULL)
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranReservasiResi($id) {
		
		global $cfg;

		$sql = "SELECT 
					* 
  				FROM 
  					tbl_reservasi
  				WHERE 
  					IdSetoran = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranPaketResi($id) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_paket
  				WHERE 
  					IdSetoran = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranBiayaResi($id) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_biaya_op
  				WHERE 
  					IdSetoran = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

	public function getSetoranDetail($id) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_user_setoran
  				WHERE 
  					IdSetoran = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Laporan', $e->getMessage()); }

		return $res;
	}

}