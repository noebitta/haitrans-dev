<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class UserModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= $this->_tblPrefix .'_user';
		$this->_id 		= 'user_id';
	}
	
	public function userLogin($username, $password) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_user
				WHERE 
					username 		= '$username' AND 
					user_password 	= '$password'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('User', $e->getMessage()); }

		return $res;
	}

	public function getDetail($id){

        return $this->find(array('filter' => array("user_id = '$id'")));
    }

	public function emailExist($email){

		return $this->find(array('filter' => array("email = '$email'")));
	}

}