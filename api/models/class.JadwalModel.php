<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class JadwalModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= $this->_tblPrefix .'_md_jadwal';
		$this->_id 		= '';
	}
	
	public function getList($id, $TglBerangkat) {
		
		global $cfg;

		$sql = "SELECT 
					j.IdJurusan as id_jurusan, j.KodeJadwal as kode_jadwal, j.JamBerangkat as jam_berangkat, j.JumlahKursi as jumlah_kursi, IS_NULL(pk.LayoutKursi, 0) as pen_jumlah_kursi,
					j.FlagSubJadwal as flag_sub, j.KodeJadwalUtama as jadwal_utama, IS_NULL(pk.StatusAktif, 2) AS status_penjadwalan, j.FlagAktif AS flag_aktif, IS_NULL(spj.IdLayout, 0) as spj_layout
  				FROM 
  					tbl_md_jadwal as j 
  				LEFT JOIN
  					tbl_penjadwalan_kendaraan as pk
  				ON
  					pk.KodeJadwal 			= j.KodeJadwal AND 
  					pk.TglBerangkat 		= '$tgl' 
  				LEFT JOIN 	
  					tbl_spj as spj
  				ON 
  					spj.KodeJadwal 			= j.KodeJadwalUtama AND 
  					date(spj.TglBerangkat) 	= '$tgl' 
  				WHERE 
  					j.IdJurusan 			= '$id' 	
				";

		$res = null;

		//on pk.StatusAktif 		= '1'
		//where j.FlagAktif 		= '1'

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Jadwal', $e->getMessage()); }

		return $res;
	}

	public function getPromo($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					j.IdJurusan as id_jurusan, j.KodeJadwal as kode_jadwal, j.JamBrangkat as jam_berangkat, j.JumlahKursi as jumlah_kursi
  				FROM 
  					tbl_md_jadwal as j 
  				LEFT JOIN
  					tbl_penjadwalan_kendaraan as pk
  				ON
  					pk.KodeJadwal 		= j.KodeJadwal AND 
  					pk.TanggalBerangkat = '$tgl'
  				WHERE 
  					IdJurusan = '$id' AND 
  					FlagAktif = '1'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Jadwal', $e->getMessage()); }

		return $res;
	}

	public function getPromoPP($tgl, $hari, $jam) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_promo2
				WHERE 	
					BerlakuMula 	<= '$tgl' AND 
					BerlakuAkhir 	>= '$tgl' AND 
					JamMulai 		<= '$jam' AND 
					JamAkhir 		>= '$jam' AND 
					BerlakuHari 	 = '$hari' AND 
					FlagAktif 		 = '1' AND 
					IsPromoPP 		 = '1'  
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jadwal', $e->getMessage()); }

		return $res;
	}

	public function getPromoPPByHari($tgl, $hari) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_promo2
				WHERE 
					BerlakuMula 	<= '$tgl' AND 
					BerlakuAkhir 	>= '$tgl' AND 	 
					BerlakuHari 	 = '$hari' AND 
					FlagAktif 		 = '1' AND 
					IsPromoPP 		 = '1'  
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jadwal', $e->getMessage()); }

		return $res;
	}

	public function getDetail($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					j.IdJurusan as IdJurusan, j.KodeJadwal as KodeJadwal, j.JamBerangkat as JamBerangkat, j.JumlahKursi as JumlahKursi, IS_NULL(pk.LayoutKursi, 0) as PenJumlahKursi,
					j.FlagSubJadwal as FlagSubJadwal, j.KodeJadwalUtama as KodeJadwalUtama, IS_NULL(pk.StatusAktif, 2) AS StatusPenjadwalan, j.FlagAktif AS FlagAktif,
					j.BiayaSopir1 as BiayaSopir1, j.BiayaSopir2 as BiayaSopir2, j.BiayaSopir3 as BiayaSopir3, j.KodeCabangAsal, j.KodeCabangTujuan
  				FROM 
  					tbl_md_jadwal as j, tbl_md_jurusan as j2
  				LEFT JOIN
  					tbl_penjadwalan_kendaraan as pk
  				ON
  					pk.KodeJadwal 	= '$id' AND 
  					pk.TglBerangkat = '$tgl'
  				WHERE 
  					j.KodeJadwal 	= '$id' AND 
  					j2.IdJurusan 	= j.IdJurusan
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jadwal', $e->getMessage()); }

		return $res;
	}

	public function getCekJadwal($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					j.IdJurusan as id_jurusan, j.KodeJadwal as kode_jadwal, j.JamBerangkat as jam_berangkat, j.JumlahKursi as jumlah_kursi,
					j.FlagSubJadwal as flag_sub, j.KodeJadwalUtama as jadwal_utama, IS_NULL(pk.StatusAktif, 2) AS status_penjadwalan, j.FlagAktif AS flag_aktif
  				FROM 
  					tbl_md_jadwal as j 
  				LEFT JOIN
  					tbl_penjadwalan_kendaraan as pk
  				ON
  					pk.KodeJadwal 		= j.KodeJadwal AND 
  					pk.TglBerangkat 	= '$tgl' 
  				WHERE 
  					j.KodeJadwal 		= '$id' 	
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jadwal', $e->getMessage()); }

		return $res;
	}

	public function getPenjadwalan($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_penjadwalan_kendaraan
  				WHERE 
  					KodeJadwal 		= '$id' AND 
  					TglBerangkat 	= '$tgl'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jadwal', $e->getMessage()); }

		return $res;
	}

}