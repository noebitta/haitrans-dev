<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class JurusanModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= $this->_tblPrefix .'_md_jurusan';
		$this->_id 		= '';
	}
	
	public function getList($id) {
		
		global $cfg;

		$sql = "SELECT 
					j.IdJurusan as id_jurusan, j.KodeJurusan as kode_jurusan, j.HargaTiket as harga_tiket,
					j.HargaPaket1KiloPertama as HargaPaket1KiloPertama, j.HargaPaket1KiloBerikut as HargaPaket1KiloBerikut, 
					j.HargaPaket2KiloPertama as HargaPaket2KiloPertama, j.HargaPaket2KiloBerikut as HargaPaket2KiloBerikut,
					j.HargaPaket3KiloPertama as HargaPaket3KiloPertama, j.HargaPaket3KiloBerikut as HargaPaket3KiloBerikut,
					c1.Nama as cabang_asal, c1.Kota as kota_asal,
					c2.Nama as cabang_tujuan, c2.Kota as kota_tujuan
  				FROM 
  					tbl_md_jurusan as j
  				JOIN 
  					tbl_md_cabang as c1 ON j.KodeCabangAsal  = c1.KodeCabang
  				JOIN 
  					tbl_md_cabang as c2 ON j.KodeCabangTujuan = c2.KodeCabang
  				WHERE 
  					j.KodeCabangAsal = '$id'
  				ORDER BY 
  					c2.Nama ASC
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Jurusan', $e->getMessage()); }

		return $res;
	}

	public function getDetail($id) {
		
		global $cfg;

		$sql = "SELECT 
					*,
					c1.Nama as cabang_asal, c1.Kota as kota_asal,
					c2.Nama as cabang_tujuan, c2.Kota as kota_tujuan
  				FROM 
  					tbl_md_jurusan as j
  				JOIN 
  					tbl_md_cabang as c1 ON j.KodeCabangAsal  = c1.KodeCabang
  				JOIN 
  					tbl_md_cabang as c2 ON j.KodeCabangTujuan = c2.KodeCabang
  				WHERE 
  					j.IdJurusan = '$id'
  				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jurusan', $e->getMessage()); }

		return $res;
	}

	public function getDetailByCabang($asal, $tujuan) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_md_jurusan 
  				WHERE 
  					KodeCabangAsal 		= '$asal' AND 
  					KodeCabangTujuan	= '$tujuan'
  				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jurusan', $e->getMessage()); }

		return $res;
	}

	public function getCabang($id) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_md_cabang
  				WHERE 
  					KodeCabang = '$id'
  				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jurusan', $e->getMessage()); }

		return $res;
	}

	public function getDetailById($id) {
		
		global $cfg;

		$sql = "SELECT 
					j.IdJurusan as id_jurusan, j.KodeJurusan as kode_jurusan, j.HargaTiket as harga_tiket,
					c1.Nama as cabang_asal, c1.Kota as kota_asal,
					c2.Nama as cabang_tujuan, c2.Kota as kota_tujuan
  				FROM 
  					tbl_md_jurusan as j
  				JOIN 
  					tbl_md_cabang as c1 ON j.KodeCabangAsal  = c1.KodeCabang
  				JOIN 
  					tbl_md_cabang as c2 ON j.KodeCabangTujuan = c2.KodeCabang
  				WHERE 
  					j.IdJurusan = '$id'
  				ORDER BY 
  					c2.Nama ASC
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Jurusan', $e->getMessage()); }

		return $res;
	}

}