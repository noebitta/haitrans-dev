<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class PaketModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= $this->_tblPrefix .'_md_jurusan';
		$this->_id 		= '';
	}
	
	public function getList($kode_jadwal_aktual, $kode_jadwal, $tanggal) {
		
		global $cfg;

		$sql = "SELECT 
					* 
  				FROM 
  					tbl_paket
  				WHERE 
  					(KodeJadwal 	= '$kode_jadwal' OR KodeJadwal = '$kode_jadwal_aktual') AND 
  					TglBerangkat	= '$tanggal' AND 
  					FlagBatal 	   != 1 
  				ORDER BY 
  					WaktuPesan
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Paket', $e->getMessage()); }

		return $res;
	}

	public function getListByTanggal($tgl_awal, $tgl_akhir) {
		
		global $cfg;

		$sql = "SELECT 
					* 
  				FROM 
  					tbl_paket
  				WHERE 
  					TglBerangkat	>= '$tgl_awal' AND 
  					TglBerangkat	<= '$tgl_akhir' AND 
  					FlagBatal 	   != 1 
  				ORDER BY 
  					WaktuPesan
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Paket', $e->getMessage()); }

		return $res;
	}

	public function getDetail($id) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_paket 
  				WHERE 
  					NoTiket = '$id'
  				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Paket', $e->getMessage()); }

		return $res;
	}

	public function getCheckinList($kota, $asal, $tujuan, $tgl_awal, $tgl_akhir) {
		
		global $cfg;

		$cond .= $kota != "" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) = '$kota'" : "";
		$cond .= $tujuan != "" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) = '$tujuan'" : "";
		$cond .= $asal != "" && $tujuan != "" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) = '$asal'" : "";

		$sql = "SELECT 
					tms.Nama as NamaSopir, tp.KodeSopir as KodeDriver,
					tp.NoSPJ as NoManifest, TglCetakSPJ as TglSPJ,
					KodeJadwal, TglBerangkat, JamBerangkat,
					COUNT(1) as JumlahPaket, KodeKendaraan as NoPolisi,
					TIMEDIFF(TglCetakSPJ, CONCAT(DATE(TglBerangkat),' ', JamBerangkat)) as Keterlambatan,
					IF(TIMEDIFF(TglCetakSPJ, CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) > '00:15:00',1,0) IsTerlambat
				FROM 
					tbl_paket as tp 
				LEFT JOIN 
					tbl_md_sopir as tms 
				ON 
					tp.KodeSopir = tms.KodeSopir
				WHERE 
					(TglBerangkat BETWEEN '$tgl_awal' AND '$tgl_akhir') AND 
					FlagBatal  != 1 AND 
					CetakTiket	= 1 AND 
					CetakSPJ 	= 1
					$cond
				GROUP BY 
					TglBerangkat, KodeJadwal
				ORDER BY 
					TglBerangkat DESC
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Paket', $e->getMessage()); }

		return $res;
	}

}