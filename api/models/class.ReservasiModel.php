<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class ReservasiModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= $this->_tblPrefix .'_reservasi';
		$this->_id 		= '';
	}

	public function getReservasi($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi
  				WHERE 
  					(KodeJadwal     ='$id' OR f_jadwal_ambil_kodeutama_by_kodejadwal(KodeJadwal) = '$id') AND 
  					TglBerangkat 	= '$tgl' AND 
  					FlagBatal 	   != '1' AND 
  					CetakTIket 		= '1'
  				ORDER BY 
  					NomorKursi
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getReservasiTidakDatang($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi
  				WHERE 
  					(KodeJadwal     ='$id' OR f_jadwal_ambil_kodeutama_by_kodejadwal(KodeJadwal) = '$id') AND 
  					TglBerangkat 	= '$tgl' AND 
  					FlagBatal 	   != '1' AND 
  					CetakTIket 		= '1' AND 
  					(JenisPenumpang = 'T' OR JenisPenumpang = 'PP') AND 
  					OTPUsed 	   != '1'
  				ORDER BY 
  					NomorKursi
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getReservasiUser($id, $tgl_awal, $tgl_akhir){

		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi
  				WHERE 
  					(PetugasPenjual   = '$id' OR PetugasCetakTiket = '$id') AND
  					DATE(WaktuPesan) >= '$tgl_awal' AND 
  					DATE(WaktuPesan) <= '$tgl_akhir' AND 
  					FlagBatal 	     != '1' 
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;

	}

	public function getReservasiMember($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi
  				WHERE 
  					IdMember 		= '$id' AND
  					TglBerangkat 	= '$tgl' AND 
  					JenisPenumpang 	= 'M' AND
  					FlagBatal 	   != '1' AND 
  					OTPUsed 		= '1'
  				ORDER BY 
  					NomorKursi
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getReservasiMemberAll($id, $tgl, $tiket) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi
  				WHERE 
  					IdMember 		= '$id' AND
  					TglBerangkat 	= '$tgl' AND 
  					JenisPenumpang 	= 'M' AND
  					FlagBatal 	   != '1' AND
  					NoTiket 	   != '$tiket'
  				ORDER BY 
  					NomorKursi
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getReservasiReguler($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					*, COUNT(NoTiket) as jumcetak
  				FROM 
  					tbl_reservasi
  				WHERE 
  					IdMember 		= '$id' AND
  					TglBerangkat 	= '$tgl' AND 
  					JenisPenumpang 	= 'K' AND
  					FlagBatal 	   != '1' AND 
  					CetakTiket 		= '1'
  				ORDER BY 
  					NomorKursi
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getDetailByTiketOtp($no_tiket, $otp) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi as res, tbl_posisi_detail as pod
  				WHERE 
  					res.NoTiket = '$tiket' AND
  					res.OTP 	= '$otp' AND 
  					pod.NoTiket = res.NoTiket
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getDetailbyOtp($otp) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi
  				WHERE 
  					OTP = '$otp'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getDetailbyTiket($tiket) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi as res, tbl_posisi_detail as pod
  				WHERE 
  					res.NoTiket = '$tiket' AND 
  					pod.NoTiket = res.NoTiket
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getJenisDiskon($type) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_jenis_discount
  				WHERE 
  					FlagAktif 		= '1' AND 
  					FlagLuarKota 	= '1' AND
  					KodeDiscount	= '$type'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getDetailJenisDiskon($id) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_jenis_discount
  				WHERE 
  					IdDiscount = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getDetailJenisDiskonByNama($id) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_jenis_discount
  				WHERE 
  					NamaDiscount = '$id' AND 
  					FlagAktif 	 = '1' AND 
  					FlagLuarKota = '1'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getPosisi($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_posisi
  				WHERE 
  					KodeJadwal 	 = '$id' AND 
  					TglBerangkat = '$tgl'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getPosisiDetail($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					pod.NomorKursi as no_kursi, pod.StatusKursi as status_kursi, pod.StatusBayar as status_bayar, 
					pod.NoTiket as no_tiket,pod.Session as session, pod.Nama as nama,
					res.JenisPenumpang as jenis_penumpang,  pod.SessionTime as session_time
  				FROM 
  					tbl_posisi_detail as pod
  				LEFT JOIN
  					tbl_reservasi as res
  				ON
  					res.KodeBooking  = pod.KodeBooking
  				WHERE 
  					pod.KodeJadwal 	 = '$id' AND 
  					pod.TglBerangkat = '$tgl'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getFlagKursi($id, $kursi, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_posisi_detail
  				WHERE 
  					KodeJadwal 	 = '$id' AND 
  					TglBerangkat = '$tgl' AND 
  					NomorKursi 	 = '$kursi'	
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getFlagByUser($id, $tgl, $user) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_posisi_detail
  				WHERE 
  					KodeJadwal 	 = '$id' AND 
  					TglBerangkat = '$tgl' AND 
  					Session 	 = '$user'	AND 
  					StatusKursi  = '1' AND 
  					StatusBayar  = '0' AND
  					NoTiket IS NULL AND
  					(HOUR(TIMEDIFF(SessionTime, NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW()))) <= 600 
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getFlagByUserAll($id, $tgl, $user) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_posisi_detail
  				WHERE 
  					KodeJadwal 	 = '$id' AND 
  					TglBerangkat = '$tgl' AND 
  					Session 	 = '$user'	AND 
  					StatusKursi  = '1' AND 
  					StatusBayar  = '0' AND
  					NoTiket IS NULL AND
  					(HOUR(TIMEDIFF(SessionTime, NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW()))) <= 600 
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getKursiBooked($id, $tgl) {
		
		global $cfg;

		$sql = "SELECT 
					count(NomorKursi) as jumlah_booked
  				FROM 
  					tbl_posisi_detail
  				WHERE 
  					KodeJadwal 	 = '$id' AND 
  					TglBerangkat = '$tgl' AND 
  					StatusKursi  = '1' AND 
 					(KodeBooking != null OR KodeBooking != '') AND 
 					(NoTiket != null OR NoTiket != '')
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getLogCetakTiket($no_tiket) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_log_cetak_tiket
  				WHERE 
  					NoTiket = '$no_tiket'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function getVoucherDetail($kode) {
		
		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_voucher
  				WHERE 
  					KodeVoucher = '$kode' AND 
  					NoTiket IS NULL
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Reservasi', $e->getMessage()); }

		return $res;
	}

	public function tambahPosisi($kode_jadwal, $tgl_berangkat, $jam_berangkat ,$jumlah_kursi, $kode_kendaraan, $kode_supir) {

		$sql = "CALL sp_posisi_tambah('$kode_jadwal', '$tgl_berangkat', '$jam_berangkat',$jumlah_kursi, '$kode_kendaraan', '$kode_supir')";

		try {
            $this->_dbObj->query($sql);

            $res = true;
        } catch (DbException $e) {
            Error::store('Reservasi', $e->getMessage());
        }

        return $res;
	}

	public function generateKodeBooking(){

		$temp	= array("-",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		= $temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		
		
		do{
			$rnd1	= $temp[rand(1,61)];
			$rnd2	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			
			$kode_booking	= "B".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
		
			$sql = "SELECT COUNT(1) as Jml FROM tbl_reservasi WHERE KodeBooking = '$kode_booking'";
						
			try {
	            $this->_dbObj->query($sql);

	            $res = $this->_dbObj->fetch();
	        } catch (DbException $e) {
	            Error::store('Reservasi', $e->getMessage());
	        }
		
		}while($res->Jml > 0);
		
		return $kode_booking;
	}

	public function generateNoTiket(){

		$temp	= array("-",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		= $temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		
		return "T".$y.$m.$d.$j.$mn.$s.$rnd1.$rnd2;
	}

}