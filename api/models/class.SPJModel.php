<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class SPJModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= '';
		$this->_id 		= '';
	}
	
	public function getListSupir() {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_sopir
				WHERE 
					FlagAktif = '1'
				ORDER BY 
					Nama
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getListKendaraan() {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_kendaraan
				WHERE 
					FlagAktif = '1'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getListPaket($id,$tgl) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_paket
				WHERE 
					(KodeJadwal     ='$id' OR f_jadwal_ambil_kodeutama_by_kodejadwal(KodeJadwal) = '$id') AND 
  					TglBerangkat 	= '$tgl' AND 
  					FlagBatal 	   != '1' AND 
  					CetakTIket 		= '1'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}


	public function getDetailSupir($id) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_sopir
				WHERE 
					KodeSopir = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getDetailLibur($tgl) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_libur
				WHERE 
					TglLibur = '$tgl'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getDetailKendaraan($id) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_kendaraan
				WHERE 
					KodeKendaraan = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getSPJUser($id, $tgl_awal, $tgl_akhir){

		global $cfg;

		$sql = "SELECT 
					*
  				FROM 
  					tbl_reservasi
  				WHERE 
  					PetugasCetakSPJ  	= '$id' AND
  					DATE(TglCetakSPJ)  >= '$tgl_awal' AND 
  					DATE(TglCetakSPJ)  <= '$tgl_akhir' AND 
  					FlagBatal 	       != '1' 
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;

	}

	public function getDetailOtpManifest($otp, $kode_jadwal, $tanggal) {
		
		global $cfg;

		$tanggal = $tanggal . ' 00:00:00';

		$sql = "SELECT 
					* 
				FROM 
					tbl_otp_manifest
				WHERE 
					KodeJadwal 		= '$kode_jadwal' AND 
					TglBerangkat 	= '$tanggal' AND 
					OTP 			= '$otp'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getDetailOtpManifestJadwal($kode_jadwal, $tanggal) {
		
		global $cfg;

		$tanggal = $tanggal . ' 00:00:00';

		$sql = "SELECT 
					* 
				FROM 
					tbl_otp_manifest
				WHERE 
					KodeJadwal 		= '$kode_jadwal' AND 
					TglBerangkat 	= '$tanggal' 
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getDetailLogManifest($no_spj) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_log_cetak_manifest
				WHERE 
					NoSPJ = '$no_spj'
				ORDER BY 
					Id Desc
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getLogManifestByJadwal($kode_jadwal, $tanggal) {
		
		global $cfg;

		$tanggal = $tanggal .' 00:00:00';

		$sql = "SELECT 
					* 
				FROM 
					tbl_log_cetak_manifest
				WHERE 
					KodeJadwal 		= '$kode_jadwal' AND 
					TglBerangkat 	= '$tanggal'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getDetailSPJ($id) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_spj
				WHERE 
					NoSPJ = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

	public function getDetailSPJByJadwal($kode_jadwal, $tanggal) {
		
		global $cfg;

		$tanggal = $tanggal .' 00:00:00';

		$sql = "SELECT 
					* 
				FROM 
					tbl_spj
				WHERE 
					KodeJadwal 		= '$kode_jadwal' AND 
					TglBerangkat 	= '$tanggal'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('SPJ', $e->getMessage()); }

		return $res;
	}

}