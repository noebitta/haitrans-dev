<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class SpjController extends Controller{

	private $_spjModel;
	private $_reservasiModel;
	private $_jurusanModel;
	private $_jadwalModel;
	private $_userModel;

	public function __construct(){

		parent::__construct();

		$this->_spjModel 		= $this->loadModel('SPJ');
		$this->_reservasiModel 	= $this->loadModel('Reservasi');
		$this->_jurusanModel 	= $this->loadModel('Jurusan');
		$this->_jadwalModel 	= $this->loadModel('Jadwal');
		$this->_userModel 		= $this->loadModel('User');

	}

	public function index(){

		
	}

	public function cek(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$kode_jadwal  	= $this->getParam('kode_jadwal');
		$tgl_berangkat 	= $this->getParam('tgl_berangkat');

		$penjadwalan	= $this->_jadwalModel->getPenjadwalan($kode_jadwal, $tgl_berangkat);
		$reservasi 		= $this->_reservasiModel->getReservasi($kode_jadwal, $tgl_berangkat);
		$manifest 		= $this->_spjModel->getDetailSPJByJadwal($kode_jadwal, $tgl_berangkat);
		$paket 			= $this->_spjModel->getListPaket($kode_jadwal, $tgl_berangkat);

		$jadwal 		= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$jurusan 		= $this->_jurusanModel->getDetail($jadwal->IdJurusan);

		if(!$jadwal){
			$this->error("Jadwal tidak tersedia");
		}

		$no_spj 		= "";
		$kode_supir 	= "";
		$kode_kendaraan	= "";
		$status_spj		= "BELUM CETAK";

		/* Biaya */
		$biaya_tol 		= $jurusan->BiayaTol;
		$biaya_parkir 	= $jurusan->BiayaParkir;
		$biaya_bbm 		= $jurusan->BiayaBBM;

		$hari 			= date('N', strtotime($tgl_berangkat));
		$libur 			= $this->_spjModel->getDetailLibur($tgl_berangkat);

		if($hari <= 4){
			$biaya_supir 	= $jadwal->BiayaSopir2;

			if($hari == 1 || $libur){
                $biaya_supir    = $jadwal->BiayaSopir1;
			}
		}
		else{
			$biaya_supir 	= $jadwal->BiayaSopir3;
		}

		if($penjadwalan){

			$kode_supir 	= $penjadwalan->KodeDriver;
			$kode_kendaraan	= $penjadwalan->KodeKendaraan;

		}

		if($manifest){

			$no_spj 		= $manifest->NoSPJ;
			$kode_supir 	= $manifest->KodeDriver;
			$kode_kendaraan = $manifest->NoPolisi;

			$status_spj	= "SUDAH CETAK";

		}

		// die(var_dump($logmanifest));

		$supir 			= $this->_spjModel->getDetailSupir($kode_supir);
		$kendaraan		= $this->_spjModel->getDetailKendaraan($kode_kendaraan);

		/*
		 * Data supir
		 */
		$nama_supir 	= "";
		$no_sim 		= "";

		if($supir){

			$kode_supir = $supir->KodeSopir;
			$nama_supir = $supir->Nama;
			$no_sim 	= $supir->NoSIM;

		}

		/*
		 * Data kendaraan
		 */
		$no_polisi 		= "";
		$jenis 			= "";
		$merek 			= "";
		$jumlah_kursi	= "";

		if($kendaraan){

			$kode_kendaraan = $kendaraan->KodeKendaraan;
			$no_polisi 		= $kendaraan->NoPolisi;
			$jenis 			= $kendaraan->Jenis;
			$merek 			= $kendaraan->Merek;
			$jumlah_kursi	= $kendaraan->JumlahKursi;

		}

		$status = "OK";
		$result = array(
					'status'	=> $status_spj,
					'no_spj'	=> $no_spj,
					'supir'		=> array(
									'kode_supir'	=> $kode_supir,
									'nama' 			=> $nama_supir,
									'no_sim' 		=> $no_sim
								),
					'kendaraan' => array(
									'kode_kendaraan'=> $kode_kendaraan,
									'no_polisi' 	=> $no_polisi,
									'jenis' 		=> $jenis,
									'merek' 		=> $merek,
									'jumlah_kursi' 	=> $jumlah_kursi
								),
					'biaya' 	=> array(
									'supir' 		=> $biaya_supir,
									'tol' 			=> $biaya_tol,
									'parkir' 		=> $biaya_parkir,
									'bbm' 			=> $biaya_bbm,
									'total' 		=> $biaya_supir + $biaya_tol + $biaya_parkir +  $biaya_bbm
								),
					'reservasi' => sizeof($reservasi), 
					'paket' 	=> sizeof($paket)

				);

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);

	}

	public function supir(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$supir  	= $this->_spjModel->getListSupir();

		if($supir){

			$status	= "OK";

			for ($i = 0; $i < sizeof($supir); $i++) {

				$result[]	= array(
								'kode_supir'	=> $supir[$i]->KodeSopir,
								'nama'			=> $supir[$i]->Nama,
								'telp'			=> $supir[$i]->HP,
								'alamat' 		=> $supir[$i]->Alamat,
								'no_sim'	 	=> $supir[$i]->NoSIM
							);
			}
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function request_otp(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$kode_jadwal  	= $this->getParam('kode_jadwal');
		$tgl_berangkat 	= $this->getParam('tgl_berangkat');
		$user 			= $this->getUserId();

		$jadwal  		= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$getotp 	 	= $this->_spjModel->getDetailOtpManifestJadwal($kode_jadwal, $tgl_berangkat);

		if(!$jadwal){
			$this->error("Jadwal tidak tersedia");
		}

		if($getotp){
			$this->error("OTP sudah tersedia");
		}

		try {

			$otp 		= $this->_generateOTP();

			$values 	= array();

			$values[] 	= "IdJurusan 		= '". $jadwal->IdJurusan ."'";
			$values[] 	= "KodeJadwal 		= '$kode_jadwal'";
			$values[] 	= "TglBerangkat 	= '$tgl_berangkat 00:00:00'";
			$values[] 	= "JamBerangkat 	= '". $jadwal->JamBerangkat ."'";
			$values[] 	= "OTP 				= '$otp'";
			$values[] 	= "OTPUsed 			= '0'";
			$values[] 	= "PetugasRequest 	= '$user'";
			$values[] 	= "WaktuRequest 	= now()";

			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_otp_manifest', $values);

			$status		= "OK";
			$result		= array(
							'pesan'	=> "Request OTP berhasil"
						);
			
		} catch (DbException $e) {
			$this->error($e->getMessage());
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function kendaraan(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$kendaraan  = $this->_spjModel->getListKendaraan();

		if($kendaraan){

			$status	= "OK";

			for ($i = 0; $i < sizeof($kendaraan); $i++) {

				$result[]	= array(
								'kode_kendaraan'=> $kendaraan[$i]->KodeKendaraan,
								'kode_cabang'	=> $kendaraan[$i]->KodeCabang,
								'no_polisi'		=> $kendaraan[$i]->NoPolisi,
								'jenis' 		=> $kendaraan[$i]->Jenis,
								'merek'	 		=> $kendaraan[$i]->Merek,
								'tahun'	 		=> $kendaraan[$i]->Tahun,
								'warna'	 		=> $kendaraan[$i]->Warna,
								'jumlah_kursi'	=> $kendaraan[$i]->JumlahKursi,
								'no_stnk'	 	=> $kendaraan[$i]->NoSTNK,
								'no_bpkb'	 	=> $kendaraan[$i]->NoBPKB,
								'no_rangka'	 	=> $kendaraan[$i]->NoRangka,
								'no_mesin'	 	=> $kendaraan[$i]->NoMesin,
							);
			}
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function cetak(){

		global $cfg;

		$this->setRequestMethod('POST');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status    	 	= "ZERO_RESULTS";

		$id_jurusan  	= $this->postParam('id_jurusan');
		$kode_jadwal  	= $this->postParam('kode_jadwal');
		$tgl_berangkat 	= $this->postParam('tgl_berangkat');
		$kode_supir  	= $this->postParam('kode_supir');
		$kode_kendaraan = $this->postParam('kode_kendaraan');
		$otp 			= $this->postParam('otp');
		$no_spj			= $this->postParam('no_spj');
		$cetak_ulang	= $this->postParam('cetak_ulang');
		$user 			= $this->getUserId();

		$jurusan  		= $this->_jurusanModel->getDetail($id_jurusan);
		$jadwal  		= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$supir  		= $this->_spjModel->getDetailSupir($kode_supir);
		$kendaraan  	= $this->_spjModel->getDetailKendaraan($kode_kendaraan);

		$logmanifest 	= $this->_spjModel->getDetailLogManifest($no_spj);
		$manifest 		= $this->_spjModel->getDetailSPJ($no_spj);
		$manifestjadwal	= $this->_spjModel->getDetailSPJByJadwal($kode_jadwal, $tgl_berangkat);

		if($jurusan == FALSE){
			$this->error("Jurusan tidak tersedia");
		}

		if($jadwal == FALSE){
			$this->error("Jadwal tidak tersedia");
		}

		if($supir == FALSE){
			$this->error("Supir tidak tersedia");
		}

		if($kendaraan == FALSE){
			$this->error("Kendaraan tidak tersedia");
		}

		/*
		 * Data detail jurusan
		 */
		$biaya_tol 			= $jurusan->BiayaTol;
		$biaya_parkir 		= $jurusan->BiayaParkir;
		$biaya_bbm 			= $jurusan->BiayaBBM;

		$hari 			= date('N', strtotime($tgl_berangkat));
		$libur 			= $this->_spjModel->getDetailLibur($tgl_berangkat);

		if($hari <= 4){
			$biaya_supir 	= $jadwal->BiayaSopir2;

			if($hari == 1 || $libur){
                $biaya_supir    = $jadwal->BiayaSopir1;
			}
		}
		else{
			$biaya_supir 	= $jadwal->BiayaSopir3;
		}


		/*
		 * Data detail jadwal
		 */
		$flagsub 			= $jadwal->FlagSubJadwal;
		$kode_jadwal_aktual = $flagsub == '1' ? $jadwal->KodeJadwalUtama : $kode_jadwal;
		$jam_berangkat 	 	= $jadwal->JamBerangkat;

		if($manifest || $manifestjadwal){
			$no_spj 		= $manifest->NoSPJ;
		}
		else{
			$no_spj 		= $this->_generateSPJ($kode_jadwal, $id_jurusan);	
		}

		if($no_spj == '' || empty($no_spj)){
			$this->error("Terjadi kesalahan dalam proses cetak manifest");
		}
		
		$no_polisi 			= $kendaraan->NoPolisi;
		$nama_supir 		= $supir->Nama;

		$userdata 			= $this->_userModel->getDetail($user);
		$kode_cabang 		= $userdata->KodeCabang;
		$nama_petugas 		= $userdata->nama;

		$reservasi 			= $this->_reservasiModel->getReservasi($kode_jadwal, $tgl_berangkat);
		$tdkdatang 			= $this->_reservasiModel->getReservasiTidakDatang($kode_jadwal, $tgl_berangkat);
		$paket 				= $this->_spjModel->getListPaket($kode_jadwal, $tgl_berangkat);
		//$cekotp 			= $this->_spjModel->getDetailOtpManifest($otp, $kode_jadwal, $tgl_berangkat);
		$now 				= date('Y-m-d H:i:s');
		$total 				= 0;
		$total_paket		= 0;
		
		// Tolak Cetak Manifest
		/*if (sizeof($reservasi) == 0 AND $otp == '') {
			$this->error("Manifest tidak bisa dicetak");
		}*/

		/*if($otp != ''){

			if(!$cekotp){
				$this->error("OTP salah");
			}

			if($cekotp->OTPUsed == '1'){
				$this->error("OTP telah digunakan");
			}

		}*/

		//if(sizeof($reservasi) > 0 || $cekotp){

			$this->dbObj->beginTrans();

			/* Data Reservasi */
			$penumpang 	= array();

			for ($i = 0; $i < sizeof($reservasi); $i++) {

				$no_kursi 	 		= $reservasi[$i]->NomorKursi;
				$no_kursi 	 		= strlen($no_kursi) == '1' ? '0'. $no_kursi : $no_kursi;

				$kode_jad_jurusan 	= $reservasi[$i]->KodeJadwal;
				$jurusanjadwal 		= $this->_jurusanModel->getDetailById($reservasi[$i]->IdJurusan);

				$cabang_asal 		= $jurusanjadwal->cabang_asal;
				$cabang_tujuan 		= $jurusanjadwal->cabang_tujuan;

				$penumpang[] = array(
									'no_kursi'			=> $no_kursi,
									'no_tiket'			=> $reservasi[$i]->NoTiket,
									'nama'				=> $reservasi[$i]->Nama,
									'jenis_penumpang'	=> $reservasi[$i]->JenisPenumpang,
									'cabang_asal' 		=> $cabang_asal,
									'cabang_tujuan' 	=> $cabang_tujuan
								);

				$values 	= array();

				$values[] 	= "KodeKendaraan 	= '$kode_kendaraan'";
				$values[] 	= "KodeSopir 		= '$kode_supir'";
				$values[] 	= "NoSPJ 			= '$no_spj'";
				$values[] 	= "TglCetakSPJ 		= '$now'";
				$values[] 	= "PetugasCetakSPJ 	= '$user'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values, array("NoTiket = '". $reservasi[$i]->NoTiket ."'"));

				$total 		= $total + $reservasi[$i]->Total;
			}

			$values 	= array();

			$values[] 	= "NoSPJ 			= '$no_spj'";	
			$values[] 	= "KodeKendaraan 	= '$kode_kendaraan'";
			$values[] 	= "KodeSopir 		= '$kode_supir'";

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi', $values, array("KodeJadwal = '$kode_jadwal_aktual' AND TglBerangkat = '$tgl_berangkat'"));	

			/* Data SPJ */
			$omzet 		 	= $total;
			$omzet_paket 	= $total_paket;
			$jum_penumpang 	= sizeof($reservasi);
			$jum_kursi 		= $jadwal->JumlahKursi;
			$jum_tidakdatang= sizeof($tdkdatang);
			$jum_paket 		= sizeof($paket);

			$values 	= array();

			$values[] 	= "NoSPJ 		 				= '$no_spj'";
			$values[] 	= "IdJurusan 		 			= '$id_jurusan'";
			$values[] 	= "TglSpj 		 				= now()";
			$values[] 	= "KodeJadwal 		 			= '$kode_jadwal'";
			$values[] 	= "TglBerangkat 		 		= '$tgl_berangkat 00:00:00'";
			$values[] 	= "JamBerangkat 		 		= '$jam_berangkat'";
			$values[] 	= "JumlahKursiDisediakan 		= '$jum_kursi'";
			$values[] 	= "JumlahPenumpang 		 		= '$jum_penumpang'";
			//$values[] 	= "JumlahPenumpangTidakDatang  	= '$jum_tidakdatang'";
			$values[] 	= "JumlahPaket 		 			= '$jum_paket'";
			$values[] 	= "JumlahPaxPaket 		 		= '0'";
			$values[] 	= "NoPolisi 		 			= '$kode_kendaraan'";
			$values[] 	= "KodeDriver 		 			= '$kode_supir'";
			$values[] 	= "CSO 		 					= '$user'";
			$values[] 	= "CSOPaket 		 			= '0'";
			$values[] 	= "Driver 		 				= '$nama_supir'";
			$values[] 	= "TotalOmzet 		 			= '$omzet'";
			$values[] 	= "FlagAmbilBiayaOP 		 	= '0'";
			$values[] 	= "InsentifSopir 		 		= '0'";
			$values[] 	= "IsEkspedisi 		 			= '1'";
			$values[] 	= "IsCetakVoucherBBM 		 	= '0'";
			$values[] 	= "IsSubJadwal 		 			= '$flagsub'";
			//$values[] 	= "IsCheck 		 				= '0'";

			if($manifest || $manifestjadwal){
				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_spj', $values, array("NoSPJ = '$no_spj'"));
			}
			else{
				$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_spj', $values);
			}

			/* Data paket */
			// for ($i = 0; $i < sizeof($paket); $i++) { 
				
			// 	$values = array();

			// 	$values = "KodeKendaraan  	= '$kode_kendaraan'";
			// 	$values = "KodeSopir  		= '$kode_supir'";
			// 	$values = "NoSPJ  			= '$no_spj'";
			// 	$values = "TglCetakSPJ  	= '$now'";
			// 	$values = "CetakSPJ  		= '1'";

			// 	$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_paket', $values, array("NoTiket = '". $paket[$i]->NoTiket ."'"));

			// 	$total_paket = $total_paket + $paket[$i]->TotalBayar;

			// }

			/* Update data OTP manifest */
			/*if($cekotp){
				$values 	= array();

				$values[]  	= "OTPUsed 			= '1'";
				$values[]  	= "UsedBy 			= '$user'";
				$values[]  	= "WaktuDigunakan	= now()";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_otp_manifest', $values, array("OTP = '$otp'"));
			}*/

			/* Log Manifest */
			$values 	= array();

			$values[] 	= "NoSPJ 						= '$no_spj'";	
			$values[] 	= "KodeJadwal 					= '$kode_jadwal'";	
			$values[] 	= "TglBerangkat 				= '$tgl_berangkat 00:00:00'";	
			$values[] 	= "JamBerangkat 				= '$jam_berangkat'";	
			$values[] 	= "JumlahKursiDisediakan		= '$jum_kursi'";	
			$values[] 	= "JumlahPenumpang 				= '$jum_penumpang'";	
			//$values[] 	= "JumlahPenumpangTidakDatang 	= '$jum_tidakdatang'";
			$values[] 	= "JumlahPaket 					= '$jum_paket'";	
			$values[] 	= "NoPolisi 					= '$kode_kendaraan'";	
			$values[] 	= "KodeDriver 					= '$kode_supir'";
			$values[] 	= "Driver 						= '$nama_supir'";
			$values[] 	= "TotalOmzet 					= '$omzet'";
			$values[] 	= "TotalOmzetPaket 				= '$omzet_paket'";
			$values[] 	= "IsSubJadwal 					= '$flagsub'";
			$values[] 	= "WaktuCetak 					= now()";
			$values[] 	= "UserCetak 					= '$user'";
			$values[] 	= "NamaUserPencetak 			= '$nama_petugas'";

			if($logmanifest){
				$cetakanke 	= $logmanifest->CetakanKe + 1;

				$values[] 	= "CetakanKe 				= '$cetakanke'";

				// $this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_log_cetak_manifest', $values, array("NoSPJ = '$no_spj'"));
			}
			else{
				$values[] 	= "CetakanKe 				= '1'";
				
			}
			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_log_cetak_manifest', $values);

			if($cetak_ulang != '1' || !$manifestjadwal || !$manifest){
				/* Biaya Supir */
				if($biaya_supir > 0){
					$values 	= array();

					$values[] 	= "NoSPJ 						= '$no_spj'";	
					$values[] 	= "FlagJenisBiaya 				= '2'";	
					$values[] 	= "TglTransaksi 				= '". date('Y-m-d') ."'";
					$values[] 	= "NoPolisi 					= '$kode_kendaraan'";
					$values[] 	= "KodeSopir 					= '$kode_supir'";
					$values[] 	= "Jumlah 						= '$biaya_supir'";
					$values[] 	= "IdPetugas					= '$user'";
					$values[] 	= "IdJurusan					= '$id_jurusan'";
					$values[] 	= "KodeCabang					= '$kode_cabang'";
					$values[] 	= "WaktuCatat					= now()";

					if($manifest){
						$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values, array("NoSPJ = '$no_spj'"));
					}
					else{
						$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values);
					}
				}
				
				/* Biaya Parkir */
				if($biaya_parkir > 0){
					$values 	= array();

					$values[] 	= "NoSPJ 						= '$no_spj'";	
					$values[] 	= "FlagJenisBiaya 				= '4'";	
					$values[] 	= "TglTransaksi 				= '". date('Y-m-d') ."'";
					$values[] 	= "NoPolisi 					= '$kode_kendaraan'";
					$values[] 	= "KodeSopir 					= '$kode_supir'";
					$values[] 	= "Jumlah 						= '$biaya_parkir'";
					$values[] 	= "IdPetugas					= '$user'";
					$values[] 	= "IdJurusan					= '$id_jurusan'";
					$values[] 	= "KodeCabang					= '$kode_cabang'";
					$values[] 	= "WaktuCatat					= now()";

					if($manifest){
						$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values, array("NoSPJ = '$no_spj'"));
					}
					else{
						$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values);
					}
				}

				/* Biaya BBM */
				if($biaya_bbm){
					$values 	= array();

					$values[] 	= "NoSPJ 						= '$no_spj'";	
					$values[] 	= "FlagJenisBiaya 				= '3'";	
					$values[] 	= "TglTransaksi 				= '". date('Y-m-d') ."'";
					$values[] 	= "NoPolisi 					= '$kode_kendaraan'";
					$values[] 	= "KodeSopir 					= '$kode_supir'";
					$values[] 	= "Jumlah 						= '$biaya_bbm'";
					$values[] 	= "IdPetugas					= '$user'";
					$values[] 	= "IdJurusan					= '$id_jurusan'";
					$values[] 	= "KodeCabang					= '$kode_cabang'";
					$values[] 	= "WaktuCatat					= now()";

					if($manifest){
						$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values, array("NoSPJ = '$no_spj'"));
					}
					else{
						$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values);
					}
				}

				/* Biaya Tol */
				if($biaya_tol > 0){
					$values 	= array();

					$values[] 	= "NoSPJ 						= '$no_spj'";	
					$values[] 	= "FlagJenisBiaya 				= '1'";	
					$values[] 	= "TglTransaksi 				= '". date('Y-m-d') ."'";
					$values[] 	= "NoPolisi 					= '$kode_kendaraan'";
					$values[] 	= "KodeSopir 					= '$kode_supir'";
					$values[] 	= "Jumlah 						= '$biaya_tol'";
					$values[] 	= "IdPetugas					= '$user'";
					$values[] 	= "IdJurusan					= '$id_jurusan'";
					$values[] 	= "KodeCabang					= '$kode_cabang'";
					$values[] 	= "WaktuCatat					= now()";

					if($manifest){
						$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values, array("NoSPJ = '$no_spj'"));
					}
					else{
						$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values);
					}
				}
			}

			$this->dbObj->commitTrans();

			$status	= "OK";
			$result	= array(
						'no_spj'			=> $no_spj,
						'kode_jadwal'		=> $kode_jadwal,
						'tgl_berangkat'		=> $tgl_berangkat,
						'jam_berangkat'		=> $jam_berangkat,
						'mobil' 			=> $kode_kendaraan .' ('. $no_polisi .')',
						'supir'	 			=> $nama_supir .' ('. $kode_supir .')',
						'penumpang'	 		=> $penumpang,
						'jumlah_penumpang' 	=> sizeof($reservasi),
						'jum_tidak_datang' 	=> sizeof($tdkdatang),
						'omzet_penumpang' 	=> $total,
						'nama_petugas'		=> $nama_petugas,
						'biaya' 			=> array(
												'supir' 	=> $biaya_supir,
												'parkir' 	=> $biaya_parkir,
												'tol' 		=> $biaya_tol,
												'bbm' 		=> $biaya_bbm,
												'total' 	=> $biaya_supir + $biaya_parkir + $biaya_tol+ $biaya_bbm
 											),
						'cetak_ulang' 		=> $cetak_ulang
					);
		//}
		


		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	private function _generateSPJ($kode_jadwal, $id_jurusan){

		$nourut = rand(1000,9999);

		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		= $temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];

		// $no_spj = "MNF". substr($kode_jadwal,0,3).$y.$m.$d.$j.$mn.$s.$nourut;
		$no_spj= "MNF".substr(date('Ymd'), 2, 6).substr($kode_jadwal,0,3).$id_jurusan.rand(1000,9999);

		return $no_spj;

	}

	private function _generateOTP(){

		$digit 	= 6 ;
    	$otp 	= rand(pow(10, $digit-1), pow(10, $digit)-1);

    	return $otp;

	}

}
?>