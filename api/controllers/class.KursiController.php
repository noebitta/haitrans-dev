<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class KursiController extends Controller{

	private $_jadwalModel;
	private $_jurusanModel;
	private $_reservasiModel;

	public function __construct(){

		parent::__construct();

		$this->_jadwalModel 	= $this->loadModel('Jadwal');
		$this->_jurusanModel 	= $this->loadModel('Jurusan');
		$this->_reservasiModel 	= $this->loadModel('Reservasi');

	}

	public function index(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$kode_jadwal	= $this->getParam('kode_jadwal');
		$tgl_berangkat	= $this->getParam('tgl_berangkat');
		$pulang 		= $this->getParam('pulang');

		$jadwal  		= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$cekjadwal 		= $this->_jadwalModel->getCekJadwal($kode_jadwal, $tgl_berangkat);

		if($cekjadwal == FALSE){
			
			$this->error("Tidak ada keberangkatan");

		}
		else{

			if($cekjadwal->status_penjadwalan == '1' OR ($cekjadwal->status_penjadwalan == '2' AND $cekjadwal->flag_aktif == '1')){

				//Jadwal tersedia

			}
			else{

				$status 		= "CLOSING";

				$data['status']	= $status;	
				$data['error']	= "Jadwal tidak dioperasikan atau ditutup";	
				$data['result']	= $result;

				$this->sendResponse($data); 

			}

		}

		$kode_jadwal 	= $jadwal->FlagSubJadwal == '1' ? $jadwal->KodeJadwalUtama : $jadwal->KodeJadwal;
		// $jumlah_kursi 	= $jadwal->JumlahKursi;
		// $layout_kursi 	= $jadwal->JumlahKursi;
		$layout_kursi 	= $jadwal->PenJumlahKursi == '0' ? $jadwal->JumlahKursi : $jadwal->PenJumlahKursi;
		$jumlah_kursi 	= $jadwal->PenJumlahKursi == '0' ? $jadwal->JumlahKursi : $jadwal->PenJumlahKursi;
		
		$jam_berangkat 	= $jadwal->JamBerangkat .":00";

		/**
 		 * Cek Jadwal Promo
 		 * Promo PP 
		 */
		/*$harino  		= date('N', strtotime($tgl_berangkat));
		$hari  			= $cfg['hariIn'][$harino];
		$jam 			= substr($jadwal->JamBerangkat, 0, 2);
		$getpp 			= $this->_jadwalModel->getPromoPP($tgl_berangkat, $hari, $jam);

		$kursisale 		= array();
		$kursipp 		= $getpp->NomorKursi;
		$kursipps 		= explode(',', $kursipp);

		if($kursipp != ''){
			for ($i = 0; $i < sizeof($kursipps); $i++) { 
				$kursisale[] = $kursipps[$i];
			}
		}*/

		/**
		 * Jurusan untuk pulang
		 * Promo PP
		 */	
		$id_jurusan_pulang = "";

		/*if($pulang != '1'){
			
			$id_jurusan 	= $jadwal->IdJurusan;

			$getjurusan 	= $this->_jurusanModel->getDetail($id_jurusan);
			$asal 			= $getjurusan->KodeCabangAsal;
			$tujuan 		= $getjurusan->KodeCabangTujuan;

			$getbalik 		= $this->_jurusanModel->getDetailByCabang($tujuan, $asal);

			if($getbalik){
				$id_jurusan_pulang = $getbalik->IdJurusan;
			}

		}*/

		/*
 		 * Cek Posisi
 		 * Jika belum ada, tambah baru
		 */

		$posisi  		= $this->_reservasiModel->getPosisi($kode_jadwal, $tgl_berangkat);
		
		if($posisi == FALSE){

			$kode_kendaraan = "";
			$kode_supir 	= "";

			$tambahposisi 	= $this->_reservasiModel->tambahPosisi($kode_jadwal, $tgl_berangkat, $jam_berangkat, $jumlah_kursi, $kode_kendaraan, $kode_supir);

		}

		/*
 		 * Posisi Detail
		 */

		$data_posisi 	= array();
		$posisi_detail	= $this->_reservasiModel->getPosisiDetail($kode_jadwal, $tgl_berangkat);

		if(sizeof($posisi_detail) > 0){

			for ($i = 0; $i < sizeof($posisi_detail); $i++) {

				$no_kursi 		= $posisi_detail[$i]->no_kursi;
				$status_kursi 	= $posisi_detail[$i]->status_kursi;
				$status_bayar 	= $posisi_detail[$i]->status_bayar;
				$no_tiket 		= $posisi_detail[$i]->no_tiket;
				$user 			= $posisi_detail[$i]->session;
				$nama 			= $posisi_detail[$i]->nama;

				$penumpang 		= $posisi_detail[$i]->jenis_penumpang;
				$online 		= 0;

				$now 			= strtotime(date('Y-m-d H:i:s'));
				$session 		= strtotime($posisi_detail[$i]->session_time);
				$expired 		= $now - $session;

				if($posisi_detail[$i]->jenis_penumpang == 'T'){
					$online 	= 1;
				}

				if($status_kursi == '1'){

					// if($no_tiket == "" AND $expired < 900) $status = "ONFLAG";
					// else if($no_tiket != "" AND $no_tiket != null AND $status_bayar == '0') $status = "BOOKED";
					// else if($no_tiket != "" AND $no_tiket != null AND $status_bayar == '1') $status = "SOLD";
					// else $status = "AVAILABLE";

					if($no_tiket != "" AND $no_tiket != null){
						$status = $status_bayar == '1' ? "SOLD" : "BOOKED";
					}
					else{
						$status = $expired < 900 ? "ONFLAG" : "AVAILABLE";
					}

					$data_posisi[] = array(
										'no_kursi' 	=> $no_kursi,
										'nama' 		=> $nama,
										'status' 	=> $status,
										'session'	=> $expired,
										'no_tiket' 	=> $no_tiket,
										'user_id'	=> $user,
										'tiketux' 	=> $online	
									);
				}

			}

		}

		/* Buat ulang layout */
		$layouts 	= $cfg['kursi'][$layout_kursi];
		$rows 		= $layouts['rows'];
		$cols 		= $layouts['cols'];
		$kursis		= $layouts['kursi'];
		$kursi  	= array();
		// $kursisale 	= array('9', '10', '11', '12');

		for ($i = 0; $i < sizeof($kursis); $i++) { 

			$col 	= $kursis[$i]['col'];
			$row 	= $kursis[$i]['row'];
			$no 	= $kursis[$i]['no'];
			$tipe 	= $kursis[$i]['tipe'];

			/*if(in_array($no, $kursisale)){
				$tipe = "D";
			}*/

			/* Jika kursi pulang selain
			if(!in_array($no, $kursisale) && $no != "" && $pulang == '1'){
				$tipe = "B";
			}*/

			$kursi[] = array(
							'col' 	=> $col,
							'row' 	=> $row,
							'no' 	=> $no,
							'tipe' 	=> $tipe
						);

		}

		$layout 	= array(
						'rows'	=> $rows,
						'cols' 	=> $cols,
						'kursi' => $kursi
					);

		$status		= "OK";
		$result[]	= array(
						'jurusan_pulang'=> $id_jurusan_pulang,
						'layout_kursi'	=> $layout_kursi,
						'jumlah_kursi'	=> $jumlah_kursi,
						'layout' 		=> $layout,
						'data_posisi' 	=> $data_posisi
					);

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);
	}

	public function flag(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$kode_jadwal	= $this->getParam('kode_jadwal');
		$no_kursi		= $this->getParam('no_kursi');
		$tgl_berangkat	= $this->getParam('tgl_berangkat');
		$flag			= $this->getParam('flag');
		$user 			= $this->getUserId();
		$now 			= date('Y-m-d H:i:s');

		$jadwal  		= $this->_jadwalModel->getDetail($kode_jadwal);

		if($jadwal == FALSE){
			$this->error("Tidak ada keberangkatan");
		}

		$kode_jadwal 	= $jadwal->FlagSubJadwal == '1' ? $jadwal->KodeJadwalUtama : $jadwal->KodeJadwal;
		$flag_kursi		= $this->_reservasiModel->getFlagKursi($kode_jadwal, $no_kursi, $tgl_berangkat);

		/*
	 	 * Cek flag paramater
	 	 * jika flag = 1, flag kursi
	 	 * jika flag = 0, unfalg kursi
		 */
		if($flag == '1'){

			if($flag_kursi == FALSE){

				$this->dbObj->beginTrans();

				$values		= array();

				$values[]	= "NomorKursi 		= '$no_kursi'";
				$values[]	= "KodeJadwal		= '$kode_jadwal'";
				$values[]	= "TglBerangkat 	= '$tgl_berangkat'";
				$values[]	= "JamBerangkat 	= '$jadwal->JamBerangkat'";
				$values[]	= "KodeJadwalUtama 	= '$kode_jadwal'";
				$values[]	= "KodeCabangAsal 	= '$jadwal->KodeCabangAsal'";
				$values[]	= "KodeCabangTujuan	= '$jadwal->KodeCabangTujuan'";
				$values[]	= "StatusKursi 		= '1'";
				$values[]	= "Session 			= '$user'";
				$values[]	= "SessionTime 		= '$now'";

				$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values);

				$this->dbObj->commitTrans();

				$msg 		= "Berhasil membuat flag";

			}
			else{

				$this->dbObj->beginTrans();

				$values		= array();

				$values[]	= "StatusKursi	= '1'";
				$values[]	= "Session 		= '$user'";
				$values[]	= "SessionTime 	= '$now'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values, array("NomorKursi = '$no_kursi' AND KodeJadwal = '$kode_jadwal' AND TglBerangkat = '$tgl_berangkat'"));

				$this->dbObj->commitTrans();

				$msg 		= "Berhasil mengupdate flag";
			}

		}
		else{

			$this->dbObj->beginTrans();

			$values		= array();

			$values[]	= "StatusKursi	= '0'";
			$values[]	= "Session 		= '$user'";

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values, array("NomorKursi = '$no_kursi' AND KodeJadwal = '$kode_jadwal' AND TglBerangkat = '$tgl_berangkat'"));

			$this->dbObj->commitTrans();

			$msg 		= "Berhasil mengupdate unflag";
		}

		$status		= "OK";
		$result[]	= array(
						'pesan'	=> $msg
					);

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);
	}

}
?>