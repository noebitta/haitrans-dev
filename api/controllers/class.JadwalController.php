<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class JadwalController extends Controller{

	private $_jadwalModel;
	private $_reservasiModel;

	public function __construct(){

		parent::__construct();


		$this->_jadwalModel 	= $this->loadModel('Jadwal');
		$this->_reservasiModel 	= $this->loadModel('Reservasi');

	}

	public function index(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$id_jurusan		= $this->getParam('id_jurusan');
		$tgl_berangkat	= $this->getParam('tgl_berangkat');
		$pulang 		= $this->getParam('pulang');
		$kode_jadwal 	= $this->getParam('kode_jadwal_pergi');
		$tgl_pergi 		= $this->getParam('tgl_berangkat_pergi');
		$jadwal  		= $this->_jadwalModel->getList($id_jurusan, $tgl_berangkat);
		$now 			= date('Y-m-d H:i:s');

		if($jadwal){

			for ($i = 0; $i < sizeof($jadwal); $i++) {

				if($jadwal[$i]->status_penjadwalan == '1' OR ($jadwal[$i]->status_penjadwalan == '2' AND $jadwal[$i]->flag_aktif == '1')){

					$jadwal_jam 	= $tgl_berangkat .' '. $jadwal[$i]->jam_berangkat .':00';
					$jam_akses 		= date('Y-m-d H:i:s', strtotime('- 3 hours', strtotime($now)));

					// die($jam_akses);

					if($pulang == '1'){

						$jadwal_pergi 	= $this->_jadwalModel->getDetail($kode_jadwal);
						$jam_pergi 	 	= $tgl_pergi .' '. $jadwal_pergi->JamBerangkat .':00';

						if(strtotime($jadwal_jam) > strtotime($jam_pergi)){

							$flagsub 		= $jadwal[$i]->flag_sub;
							$jadwal_utama 	= $flagsub == '1' ? $jadwal[$i]->jadwal_utama : "";
							$jadwal_aktual 	= $flagsub == '1' ? $jadwal[$i]->jadwal_utama : $jadwal[$i]->kode_jadwal;
							$jumlah_kursi 	= $jadwal[$i]->pen_jumlah_kursi == '0' ? $jadwal[$i]->jumlah_kursi : $jadwal[$i]->pen_jumlah_kursi;
							$jumlah_kursi 	= $jadwal[$i]->spj_layout == '0' ? $jumlah_kursi : $jadwal[$i]->spj_layout;

							$kursi_booked 	= $this->_reservasiModel->getKursiBooked($kode_jadwal, $tgl_berangkat);
							$jumlah_booked 	= $kursi_booked->jumlah_booked;

							$status			= "OK";
							$result[]		= array(
												'id_jurusan'		=> $jadwal[$i]->id_jurusan,
												'kode_jadwal'		=> $jadwal[$i]->kode_jadwal,
												'jam_berangkat'		=> $jadwal[$i]->jam_berangkat,
												'jumlah_kursi' 		=> $jumlah_kursi,
												'sisa_kursi' 		=> $jumlah_kursi - $jumlah_booked,
												'kode_jadwal_utama' => $jadwal_utama
											);
						}

					}
					else{

						if(strtotime($jadwal_jam) > strtotime($jam_akses)){

							$flagsub 		= $jadwal[$i]->flag_sub;
							$jadwal_utama 	= $flagsub == '1' ? $jadwal[$i]->jadwal_utama : "";
							$jadwal_aktual 	= $flagsub == '1' ? $jadwal[$i]->jadwal_utama : $jadwal[$i]->kode_jadwal;
							$jumlah_kursi 	= $jadwal[$i]->pen_jumlah_kursi == '0' ? $jadwal[$i]->jumlah_kursi : $jadwal[$i]->pen_jumlah_kursi;
							$jumlah_kursi 	= $jadwal[$i]->spj_layout == '0' ? $jumlah_kursi : $jadwal[$i]->spj_layout;

							$kursi_booked 	= $this->_reservasiModel->getKursiBooked($jadwal_aktual, $tgl_berangkat);
							$jumlah_booked 	= $kursi_booked->jumlah_booked;

							$status			= "OK";
							$result[]		= array(
												'id_jurusan'		=> $jadwal[$i]->id_jurusan,
												'kode_jadwal'		=> $jadwal[$i]->kode_jadwal,
												'jam_berangkat'		=> $jadwal[$i]->jam_berangkat,
												'jumlah_kursi' 		=> $jumlah_kursi,
												'sisa_kursi' 		=> $jumlah_kursi - $jumlah_booked,
												'kode_jadwal_utama' => $jadwal_utama
											);
						}

					}

					

				}
			}
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

}
?>