<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class PaketController extends Controller{

	private $_jadwalModel;
	private $_jurusanModel;
	private $_paketModel;
	private $_spjModel;
	private $_userModel;

	public function __construct(){

		parent::__construct();

		$this->_jadwalModel 	= $this->loadModel('Jadwal');
		$this->_jurusanModel 	= $this->loadModel('Jurusan');
		$this->_paketModel 		= $this->loadModel('Paket');
		$this->_spjModel 		= $this->loadModel('SPJ');
		$this->_userModel 		= $this->loadModel('User');

	}

	public function index(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$kode_jadwal	= $this->getParam('kode_jadwal');
		$tgl_berangkat	= $this->getParam('tgl_berangkat');

		$jadwal  		= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$cekjadwal 		= $this->_jadwalModel->getCekJadwal($kode_jadwal, $tgl_berangkat);

		if($cekjadwal == FALSE){
			
			$this->error("Tidak ada keberangkatan");

		}
		else{

			if($cekjadwal->status_penjadwalan == '1' OR ($cekjadwal->status_penjadwalan == '2' AND $cekjadwal->flag_aktif == '1')){

				//Jadwal tersedia

			}
			else{

				$status 		= "CLOSING";

				$data['status']	= $status;	
				$data['error']	= "Jadwal tidak dioperasikan atau ditutup";	
				$data['result']	= $result;

				$this->sendResponse($data); 

			}

		}

		$kode_jadwal_aktual	= $jadwal->FlagSubJadwal == '1' ? $jadwal->KodeJadwalUtama : $jadwal->KodeJadwal;
		$jam_berangkat 		= $jadwal->JamBerangkat .":00";

		$paket 				= $this->_paketModel->getList($kode_jadwal_aktual, $kode_jadwal, $tgl_berangkat);

		$jurusan 		= $this->_jurusanModel->getDetail($jadwal->IdJurusan);
		$cabang_asal 	= $jurusan->cabang_asal;
		$cabang_tujuan 	= $jurusan->cabang_tujuan;

		if($paket){

			$status = "OK";
			$pakets = array();
			$omzet 	= 0;
			$koli 	= 0;

			for ($i = 0; $i < sizeof($paket); $i++) { 

				$dimensi 	= array();
				$loket 		= $paket[$i]->KodeJadwal == $kode_jadwal ? 'Di Loket Ini' : '';

				if($paket[$i]->Dimensi == '3'){

					$label 		= "UKURAN";
					
				}
				else{

					$label 		= "BERAT";

					if($paket[$i]->IsVolumetrik == '1'){

						$label 		= "VOLMETRIK";
						$dimensi 	= array(
										'panjang'	=> (int) $paket[$i]->Panjang,
										'lebar'		=> (int) $paket[$i]->Lebar,
										'tinggi'	=> (int) $paket[$i]->Tinggi
									);

					}

				}

				$omzet 		= $omzet + $paket[$i]->TotalBayar;
				$koli 		= $koli + $paket[$i]->JumlahKoli;
				
				$pakets[]	= array(
								'no_resi'		=> $paket[$i]->NoTiket,
								'jenis_barang'	=> $cfg['paket'][$paket[$i]->Dimensi],
								'jenis_layanan'	=> $cfg['layanan_paket'][$paket[$i]->JenisLayanan],
								'pengirim'		=> array(
														'nama' 		=> $paket[$i]->NamaPengirim,
														'telpon' 	=> $paket[$i]->TelpPengirim,
														'alamat' 	=> $paket[$i]->AlamatPengirim,
													),
								'penerima'		=> array(
														'nama' 		=> $paket[$i]->NamaPenerima,
														'telpon' 	=> $paket[$i]->TelpPenerima,
														'alamat' 	=> $paket[$i]->AlamatPenerima,
													),
								'label'			=> $label,
								'dimensi'		=> $dimensi,
								'koli'			=> (int) $paket[$i]->JumlahKoli,
								'berat'			=> (int) $paket[$i]->Berat,
								'biaya'			=> (int) $paket[$i]->TotalBayar,
								'keterangan' 	=> $paket[$i]->KeteranganPaket,
								'loket'			=> $loket
							);

			}

			$manifest 		= $this->_spjModel->getDetailSPJByJadwal($kode_jadwal, $tgl_berangkat);
			$manifests 		= array();
			$cetak_manifest	= 0;

			if($manifest){
				$cetak_manifest	= 1;
				$manifests 		= array(
									'no_spj'		=> $manifest->NoSPJ,
									'kode_jadwal' 	=> $kode_jadwal,
									'jurusan'		=> $cabang_asal .'-'. $cabang_tujuan,
									'tanggal' 		=> dateNum($manifest->TglSPJ),
									'jam' 			=> substr($manifest->TglSPJ, 12,5),
									'kendaraan' 	=> $manifest->NoPolisi,
									'supir' 		=> $manifest->Driver .' ('. $manifest->KodeDriver .')'
								);
			}

			$result = array(
						'transaksi' 	=> sizeof($paket),
						'koli'			=> (int) $koli,
						'omzet'			=> (int) $omzet,
						'cetak_manifest'=> $cetak_manifest,
						'manifest' 		=> $manifests,
						'jml_paket'		=> sizeof($pakets),
						'paket'			=> $pakets
					);

		}

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);
	}

	public function jenis(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$jenis 		= $cfg['paket'];
		$ukuran		= $cfg['ukuran_paket'];
		$layanan	= $cfg['layanan_paket'];

		$jeniss 	= array();
		$ukurans 	= array();
		$layanans 	= array();

		for ($i = 0; $i < sizeof($jenis); $i++) { 
		 	
			$status 	= "OK";
			$jeniss[]	= array(
							'kode'			=> key($jenis),
							'keterangan'	=> $jenis[key($jenis)]
						);

			next($jenis);

		} 

		for ($i = 0; $i < sizeof($ukuran); $i++) { 
		 	
			$status 	= "OK";
			$ukurans[]	= array(
							'kode'			=> key($ukuran),
							'keterangan'	=> $ukuran[key($ukuran)]
						);

			next($ukuran);

		} 

		for ($i = 0; $i < sizeof($layanan); $i++) { 
		 	
			$status 	= "OK";
			$layanans[]	= array(
							'kode'			=> key($layanan),
							'keterangan'	=> $layanan[key($layanan)]
						);

			next($layanan);

		}

		$result = array(
					'jenis' 	=> $jeniss,
					'ukuran'	=> $ukurans,
					'layanan'	=> $layanans
				);

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);
	}

	public function harga(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$id_jurusan	= $this->getParam('id_jurusan');
		$jenis 		= $this->getParam('kode_jenis');
		$koli 		= $this->getParam('koli');
		$volume		= $this->getParam('volume');
		$berat		= $this->getParam('berat');
		$panjang	= $this->getParam('panjang');
		$lebar		= $this->getParam('lebar');
		$tinggi		= $this->getParam('tinggi');
		$ukuran		= $this->getParam('ukuran'); /* 1 = kecil, 2 = sedang, 3 = besar */

		$data		= array();
		$result    	= array();
		$status     = "ERROR";

		$jurusan 	= $this->_jurusanModel->getDetail($id_jurusan);

		if(!$jurusan){
			$this->error("Jurusan tidak tersedia");
		}

		switch ($jenis) {

			case '1': // Dokumen

				$harga	= $jurusan->HargaPaket1KiloPertama;
				$harga2	= $jurusan->HargaPaket1KiloBerikut;

			break;

			case '2': // Barang

				$harga	= $jurusan->HargaPaket2KiloPertama;
				$harga2	= $jurusan->HargaPaket2KiloBerikut;

			break;

			case '3': // Bagasi

				$harga	= $jurusan->HargaPaket3KiloPertama;
				$harga2	= $jurusan->HargaPaket3KiloBerikut;

				$berat 	= $ukuran;

			break;

			case '4': // Elektronik

				$harga	= $jurusan->harga_tiket;
				$harga2	= 0;

			break;
		}

		if($volume == '1'){
			$berat 		= ceil($panjang * $lebar * $tinggi / 4000);
		}

		$harga_paket 	= $harga + ($harga2 * ($berat - 1));

		$status 		= "OK";
		$result 		= array('biaya' => (int) $harga_paket, 'berat' => (int) $berat);

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);
	}

	public function reservasi(){

		global $cfg;

		$this->setRequestMethod('POST');
		$this->authenticate(2);

		$kode_jadwal 	= $this->postParam('kode_jadwal');
		$id_jurusan		= $this->postParam('id_jurusan');
		$tgl_berangkat	= $this->postParam('tgl_berangkat');
		$jenis 			= $this->postParam('kode_jenis');
		$koli 			= $this->postParam('koli');
		$volume			= $this->postParam('volume');
		$berat			= $this->postParam('berat');
		$panjang		= $this->postParam('panjang');
		$lebar			= $this->postParam('lebar');
		$tinggi			= $this->postParam('tinggi');
		$ukuran			= $this->postParam('ukuran'); /* 1 = kecil, 2 = sedang, 3 = besar */
		$layanan		= $this->postParam('jenis_layanan');
		$keterangan		= $this->postParam('keterangan');
		$pembayaran		= $this->postParam('jenis_pembayaran');
		$no_spj			= $this->postParam('no_spj');

		$nama_pengirim 	= $this->postParam('nama_pengirim');
		$telp_pengirim 	= $this->postParam('telp_pengirim');
		$alamat_pengirim= $this->postParam('alamat_pengirim');

		$nama_penerima 	= $this->postParam('nama_penerima');
		$telp_penerima 	= $this->postParam('telp_penerima');
		$alamat_penerima= $this->postParam('alamat_penerima');

		$user 		= $this->getUserId();
		$userdata 	= $this->_userModel->getDetail($user);
		$petugas 	= $userdata->nama;

		$data		= array();
		$result    	= array();
		$status     = "ERROR";

		$jadwal 	= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$jurusan 	= $this->_jurusanModel->getDetail($id_jurusan);

		if(!$jurusan){
			$this->error("Jurusan tidak tersedia");
		}

		if(!$jadwal){
			$this->error("Tidak ada keberangkatan");
		}

		/* Data Jurusan */
		$ka_pendapatan 	= $jurusan->KodeAkunPendapatanPaket;
		$kp_supir 		= $jurusan->KomisiPaketSopir;
		$kakp_supir 	= $jurusan->KodeAkunKomisiPaketSopir;
		$kp_cso 		= $jurusan->KomisiPaketCSO;
		$kakp_cso 		= $jurusan->KodeAkunKomisiPaketCSO;

		$kode_cabang_asal	= $jurusan->KodeCabangAsal;
		$cabang_asal 		= $jurusan->cabang_asal;
		$cabang_tujuan 		= $jurusan->cabang_tujuan;

		/* Hitung harga */
		switch ($jenis) {

			case '1': // Dokumen

				$harga	= $jurusan->HargaPaket1KiloPertama;
				$harga2	= $jurusan->HargaPaket1KiloBerikut;

			break;

			case '2': // Barang

				$harga	= $jurusan->HargaPaket2KiloPertama;
				$harga2	= $jurusan->HargaPaket2KiloBerikut;

			break;

			case '3': // Bagasi

				$harga	= $jurusan->HargaPaket3KiloPertama;
				$harga2	= $jurusan->HargaPaket3KiloBerikut;

				$berat 	= $ukuran;

			break;

			case '4': // Elektronik

				$harga	= $jurusan->harga_tiket;
				$harga2	= 0;

			break;
		}

		if($volume == '1'){
			$berat 		= ceil($panjang * $lebar * $tinggi / 4000);
		}

		//$harga_paket 	= $harga + ($harga2 * ($berat - 1));
		$diskon 		= 0;

		/* Data Jadwal */
		$jam_berangkat 	= $jadwal->JamBerangkat;


		/* Manifest */
		$manifest 		= $this->_spjModel->getDetailSPJ($no_spj);
		$kode_supir 	= "";
		$kode_kendaraan = "";
		$tgl_spj 		= "";


		if($manifest){
			$kode_supir 	= $manifest->KodeDriver;
			$kode_kendaraan = $manifest->NoPolisi;
			$tgl_spj 		= $manifest->TglSPJ;
		}

		/* Label & Dimensi */
		$dimensi 		= array();

		if($jenis == '3'){

			$label 		= "UKURAN";
					
		}
		else{

			$label 		= "BERAT";

			if($volume == '1'){

				$label 		= "VOLMETRIK";
				$dimensi 	= array(
								'panjang'	=> (int) $panjang,
								'lebar'		=> (int) $lebar,
								'tinggi'	=> (int) $tinggi
							);

			}

		}

		$harga_paket			= $this->postParam('biaya');

		try {

			$no_resi 	= $this->_generateNoTiketPaket();
			$otp 		= $this->_generateOTP();
			$intruksi 	= "";

			$values		= array();

			$values[]	= "NoTiket 					= '$no_resi'";
			$values[]	= "KodeCabang				= '$kode_cabang_asal'";
			$values[]	= "KodeJadwal				= '$kode_jadwal'";
			$values[]	= "IdJurusan				= '$id_jurusan'";
			$values[]	= "KodeKendaraan			= '$kode_kendaraan'";
			$values[]	= "kodeSopir				= '$kode_supir'";
			$values[]	= "TglBerangkat				= '$tgl_berangkat'";
			$values[]	= "JamBerangkat				= '$jam_berangkat'";
			$values[]	= "NamaPengirim				= '$nama_pengirim'";
			$values[]	= "AlamatPengirim			= '$alamat_pengirim'";
			$values[]	= "TelpPengirim				= '$telp_pengirim'";
			$values[]	= "WaktuPesan				= now()";
			$values[]	= "NamaPenerima				= '$nama_penerima'";
			$values[]	= "AlamatPenerima			= '$alamat_penerima'";
			$values[]	= "TelpPenerima				= '$telp_penerima'";
			$values[]	= "HargaPaket				= '$harga_paket'";
			$values[]	= "Diskon					= '$diskon'";
			$values[]	= "TotalBayar				= '$harga_paket'";
			$values[]	= "Dimensi					= '$jenis'";
			$values[]	= "JumlahKoli				= '$koli'";
			$values[]	= "Berat					= '$berat'";
			$values[]	= "IsVolumetrik				= '$volume'";
			$values[]	= "Panjang					= '$panjang'";
			$values[]	= "Lebar					= '$lebar'";
			$values[]	= "Tinggi					= '$tinggi'";
			$values[]	= "KeteranganPaket			= '$keterangan'";
			$values[]	= "InstruksiKhusus			= '$intruksi'";
			$values[]	= "KodeAkunPendapatan		= '$ka_pendapatan'";
			$values[]	= "PetugasPenjual			= '$user'";
			$values[]	= "KomisiPaketSopir			= '$kp_supir'";
			$values[]	= "KodeAkunKomisiPaketSopir	= '$kakp_supir'";
			$values[]	= "KomisiPaketCSO			= '$kp_cso'";
			$values[]	= "KodeAkunKomisiPaketCSO	= '$kskp_cso'";
			$values[]	= "NoSPJ					= '$no_spj'";
			$values[]	= "TglCetakSPJ				= '$tgl_spj'";
			$values[]	= "FlagBatal				= '0'";
			$values[]	= "JenisPembayaran			= '$pembayaran'";
			$values[]	= "CaraPembayaran			= '0'";
			$values[]	= "CetakTiket				= '1'";
			$values[]	= "WaktuCetakTiket			= now()";
			$values[]	= "StatusDiambil			= '0'";
			$values[]	= "JenisBarang				= '$jenis'";
			$values[]	= "JenisLayanan				= '$layanan'";
			$values[]	= "OTP						= '$otp'";

			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_paket', $values);

			$status 	= "OK";
			$result 	= array(
							'no_resi'		=> $no_resi,
							'cabang'		=> $cabang_tujuan,
							'carier'		=> $cabang_asal .'-'. $cabang_tujuan,
							'tanggal'		=> dateNum($tgl_berangkat),
							'jam'			=> $jam_berangkat,
							'jenis'			=> $cfg['paket'][$jenis],
							'layanan'		=> $cfg['layanan_paket'][$layanan],
							'pengirim'		=> array(
													'nama'		=> $nama_pengirim,
													'telpon'	=> $telp_pengirim,
													'alamat'	=> $alamat_pengirim
											),
							'penerima'		=> array(
													'nama'		=> $nama_penerima,
													'telpon'	=> $telp_penerima,
													'alamat'	=> $alamat_penerima
											),
							'keterangan'	=> $keterangan,
							'pax'			=> (int) $koli,
							'perhitungan'	=> $label,
							'dimensi'		=> $dimensi,
							'berat'			=> (int) $berat,
							'biaya'			=> (int) $harga_paket,
							'cso'			=> $petugas,
							'pembayaran'	=> $cfg['payment'][$pembayaran],
							'waktu_cetak'	=> dateNumFull(date('Y-m-d H:i:s'))
						);
			
		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Paket', $e->getMessage());

			$this->internalError();
		}

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);

	}

	public function detail(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$no_resi 	= $this->getParam('no_resi');
		$user 		= $this->getUserId();
		
		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$detail 	= $this->_paketModel->getDetail($no_resi);

		if($detail){

			$userdata		= $this->_userModel->getDetail($detail->PetugasPenjual);
			$petugas 		= $userdata->nama;

			$jurusan 		= $this->_jurusanModel->getDetail($detail->IdJurusan);
			$cabang_asal 	= $jurusan->cabang_asal;
			$cabang_tujuan 	= $jurusan->cabang_tujuan;

			/* Label & Dimensi */
			$dimensi 		= array();

			if($detail->Dimensi == '3'){

				$label 		= "UKURAN";
						
			}
			else{

				$label 		= "BERAT";

				if($detail->IsVolumetrik == '1'){

					$label 		= "VOLMETRIK";
					$dimensi 	= array(
									'panjang'	=> (int) $detail->Panjang,
									'lebar'		=> (int) $detail->Lebar,
									'tinggi'	=> (int) $detail->Tinggi
								);

				}

			}

			$status = "OK";
			$result = array(
						'no_resi'		=> $detail->NoTiket,
						'jadwal'		=> $detail->NoTiket,
						'asal'			=> $cabang_asal,
						'tujuan'		=> $cabang_tujuan,
						'tanggal'		=> dateNum($detail->TglBerangkat),
						'jam'			=> $detail->JamBerangkat,
						'jenis'			=> $cfg['paket'][$detail->Dimensi],
						'layanan'		=> $cfg['layanan_paket'][$detail->JenisLayanan],
						'pengirim'		=> array(
												'nama'		=> $detail->NamaPengirim,
												'telpon'	=> $detail->TelpPengirim,
												'alamat'	=> $detail->AlamatPengirim
											),
						'penerima'		=> array(
												'nama'		=> $detail->NamaPenerima,
												'telpon'	=> $detail->TelpPenerima,
												'alamat'	=> $detail->AlamatPenerima
										),
						'keterangan'	=> $detail->KeteranganPaket,
						'koli'			=> (int) $detail->JumlahKoli,
						'berat'			=> (int) $detail->Berat,
						'perhitungan'	=> $label,
						'dimensi'		=> $dimensi,
						'biaya'			=> (int) $detail->TotalBayar,
						'cso'			=> $petugas,
						'waktu_terima'	=> dateNumFull($detail->WaktuPesan)
					);

		}

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);

	}

	public function batal(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$no_resi 	= $this->getParam('no_resi');
		$user 		= $this->getUserId();
		
		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$detail 	= $this->_paketModel->getDetail($no_resi);

		if(!$detail){
			$this->error("Nomor tiket salah");
		}

		try {
			
			$values		= array();

			$values[]	= "FlagBatal 			= '1'";
			$values[]	= "WaktuPembatalan 		= now()";
			$values[]	= "PetugasPembatalan 	= '$user'";

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_paket', $values, array("NoTiket = '$no_resi'"));

			$status 	= "OK";
			$result 	= array('pesan' => 'Pembatalan paket berhasil');


		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Paket', $e->getMessage());

			$this->internalError();
		}

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);

	}

	public function cetak_resi(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$no_resi 	= $this->getParam('no_resi');
		$user 		= $this->getUserId();
		
		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$detail 	= $this->_paketModel->getDetail($no_resi);

		if($detail){

			$userdata		= $this->_userModel->getDetail($detail->PetugasPenjual);
			$petugas 		= $userdata->nama;

			$jurusan 		= $this->_jurusanModel->getDetail($detail->IdJurusan);
			$cabang_asal 	= $jurusan->cabang_asal;
			$cabang_tujuan 	= $jurusan->cabang_tujuan;

			/* Label & Dimensi */
			$dimensi 		= array();

			if($detail->Dimensi == '3'){

				$label 		= "UKURAN";
						
			}
			else{

				$label 		= "BERAT";

				if($detail->IsVolumetrik == '1'){

					$label 		= "VOLMETRIK";
					$dimensi 	= array(
									'panjang'	=> (int) $detail->Panjang,
									'lebar'		=> (int) $detail->Lebar,
									'tinggi'	=> (int) $detail->Tinggi
								);

				}

			}

			/* Update Data Paket */
			// $values		= array();

			// $values[]	= "CetakTiket		= '1'";
			// $values[]	= "WaktuCetakTiket	= now()";

			// $this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_paket', $values, array("NoTiket = '$no_resi'"));

			$status = "OK";
			$result = array(
						'no_resi'		=> $detail->NoTiket,
						'cabang'		=> $cabang_tujuan,
						'carier'		=> $cabang_asal .'-'. $cabang_tujuan,
						'tanggal'		=> dateNum($detail->TglBerangkat),
						'jam'			=> $detail->JamBerangkat,
						'jenis'			=> $cfg['paket'][$detail->Dimensi],
						'layanan'		=> $cfg['layanan_paket'][$detail->JenisLayanan],
						'pengirim'		=> array(
												'nama'		=> $detail->NamaPengirim,
												'telpon'	=> $detail->TelpPengirim,
												'alamat'	=> $detail->AlamatPengirim
											),
						'penerima'		=> array(
												'nama'		=> $detail->NamaPenerima,
												'telpon'	=> $detail->TelpPenerima,
												'alamat'	=> $detail->AlamatPenerima
										),
						'keterangan'	=> $detail->KeteranganPaket,
						'pax'			=> (int) $detail->JumlahKoli,
						'perhitungan'	=> $label,
						'dimensi'		=> $dimensi,
						'biaya'			=> (int) $detail->TotalBayar,
						'pembayaran'	=> $cfg['payment'][$detail->JenisPembayaran],
						'cso'			=> $petugas,
						'waktu_terima'	=> dateNumFull($detail->WaktuPesan)
					);

		}

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);

	}

	public function manifest(){

		global $cfg;

		$this->setRequestMethod('POST');
		$this->authenticate(2);

		$kode_jadwal		= $this->postParam('kode_jadwal');
		$tgl_berangkat		= $this->postParam('tgl_berangkat');
		$no_spj				= $this->postParam('no_spj');

		$data				= array();
		$result    			= array();
		$status     		= "ZERO_RESULTS";

		$user 				= $this->getUserId();
		$userdata 		 	= $this->_userModel->getDetail($user);
		$petugas 			= $userdata->nama;

		/* Manifest */
		$manifest 			= $this->_spjModel->getDetailSPJ($no_spj);

		if(!$manifest){
			$this->error("No manifest salah");
		}

		$kode_supir 		= $manifest->KodeDriver;
		$nama_supir 		= $manifest->Driver;
		$kode_kendaraan 	= $manifest->NoPolisi;
		$tgl_spj 			= $manifest->TglSPJ;

		/* Jadwal */
		$jadwal 			= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);

		if(!$jadwal){
			$this->error("Jadwal tidak tersedia");
		}

		$kode_jadwal_aktual	= $jadwal->FlagSubJadwal == '1' ? $jadwal->KodeJadwalUtama : $jadwal->KodeJadwal;
		$jam_berangkat 		= $jadwal->JamBerangkat .":00";

		$paket 				= $this->_paketModel->getList($kode_jadwal_aktual, $kode_jadwal, $tgl_berangkat);
		$pakets 			= array();

		try {

			$jumlah_koli 	= 0;

			for ($i = 0; $i < sizeof($paket); $i++) { 

				$jurusan 		= $this->_jurusanModel->getDetail($paket[$i]->IdJurusan);
				$cabang_asal 	= $jurusan->cabang_asal;
				$cabang_tujuan 	= $jurusan->cabang_tujuan;
				
				$pakets[] 		= array(
									'resi'		=> $paket[$i]->NoTiket,
									'tujuan'	=> $cabang_tujuan,
									'pengirim'	=> array(
														'nama'		=> $paket[$i]->NamaPengirim,
														'telpon'	=> $paket[$i]->TelpPengirim
													),
									'penerima'	=> array(
														'nama'		=> $paket[$i]->NamaPenerima,
														'telpon'	=> $paket[$i]->TelpPenerima
													),
									'koli'		=> $paket[$i]->JumlahKoli,

								);

				$jumlah_koli 	= $jumlah_koli + $paket[$i]->JumlahKoli;

				/* Udpate data paket */
				$values 		= array();

				$values[]		= "KodeKendaraan 	= '$kode_kendaraan'";
				$values[]		= "KodeSopir 		= '$kode_supir'";
				$values[]		= "NoSPJ 			= '$no_spj'";
				$values[]		= "TglCetakSPJ 		= '$tgl_spj'";
				$values[]		= "CetakSPJ 		= '1'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_paket', $values, array("NoTiket = '". $paket[$i]->NoTiket ."'"));

			}

			$status = "OK";
			$result = array(
						'no_spj'		=> $no_spj,
						'jadwal'		=> $kode_jadwal,
						'tanggal'		=> dateNum($tgl_berangkat),
						'kendaraan'		=> $kode_kendaraan,
						'supir'			=> $nama_supir .'('. $kode_supir .')',
						'paket'			=> $pakets,
						'tanggal_cetak'	=> dateNumFull(date('Y-m-d H:i:s')),
						'jumlah_koli'	=> $jumlah_koli,
						'transaksi'		=> sizeof($paket),
						'cso'			=> $petugas
					);
			
		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Paket', $e->getMessage());

			$this->internalError();
			
		}

		$data['status']	= $status;	
		$data['result']	= $result; 

		$this->sendResponse($data);
	}

	public function list_checkin(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$kota 			= $this->getParam('kota');
		$tujuan 		= $this->getParam('tujuan');
		$asal 			= $this->getParam('asal');
		$tgl_awal 		= $this->getParam('tgl_awal');
		$tgl_akhir 		= $this->getParam('tgl_akhir');

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$user 			= $this->getUserId();

		$paket 			= $this->_paketModel->getCheckinList($kota, $asal, $tujuan, $tgl_awal, $tgl_akhir);

		if($paket){

			for ($i = 0; $i < sizeof($paket); $i++) { 
					
				$status 	= "OK";
				$result[] 	= array(
								'tanggal'		=> dateNum($paket[$i]->TglBerangkat),
								'jam' 			=> $paket[$i]->JamBerangkat,
								'kode_jadwal' 	=> $paket[$i]->KodeJadwal,
								'berangkat' 	=> dateNumFull($paket[$i]->TglSPJ),
								'terlambat' 	=> (int) $paket[$i]->IsTerlambat,
								'menifest' 		=> $paket[$i]->NoManifest,
								'supir' 		=> $paket[$i]->NamaSopir .' ('. $paket[$i]->KodeDriver .')',
								'kendaraan' 	=> $paket[$i]->NoPolisi,
								'jumlah_paket' 	=> (int) $paket[$i]->JumlahPaket
							);

			}

		}

		$data['status']	= $status;	
		$data['result']	= $result; 
		
		$this->sendResponse($data);

	}

	public function checkin(){

		global $cfg;

		$this->setRequestMethod('POST');
		$this->authenticate(2);

		$no_resi 		= $this->postParam('no_resi'); // dipisahkan dengan koma

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$user 			= $this->getUserId();

		if($no_resi == ''){
			$this->error('Nomor resi salah');
		}

		$resis 			= explode(',', $no_resi);

		try {

			for ($i = 0; $i < sizeof($resis); $i++) { 
			
				$values 	= array();

				$values[] 	= "IsSampai 	= '1'";
				$values[] 	= "WaktuSampai 	= now()";
				$values[] 	= "UserCheckIn 	= '$user'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_paket', $values, array("NoTiket = '". $resis[$i] ."'"));

			}

			$status 	= "OK";
			$result 	= array('pesan' => 'Checkin paket berhasil');
			
		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Paket', $e->getMessage());

			$this->internalError();
			
		}

		$data['status']	= $status;	
		$data['result']	= $result; 
		
		$this->sendResponse($data);

	}

	public function laporan(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$tgl_awal 	= $this->getParam('tgl_awal');
		$tgl_akhir 	= $this->getParam('tgl_akhir');

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$user 		= $this->getUserId();

		$paket 	 	= $this->_paketModel->getListByTanggal($tgl_awal, $tgl_akhir);

		for ($i = 0; $i < sizeof($paket); $i++) { 

			$dimensi 	= array();
			$loket 		= $paket[$i]->KodeJadwal == $kode_jadwal ? 'Di Loket Ini' : '';
			$kirim 		= $paket[$i]->IsSampai == '1' ? 'SAMPAI' : 'DIKIRIM';
			$pembayaran = $cfg['payment_paket'][$paket[$i]->JenisPembayaran] != null ? $cfg['payment_paket'][$paket[$i]->JenisPembayaran] : ' Tunai';

			if($paket[$i]->Dimensi == '3'){

				$label 		= "UKURAN";
					
			}
			else{

				$label 		= "BERAT";

				if($paket[$i]->IsVolumetrik == '1'){

					$label 		= "VOLMETRIK";
					$dimensi 	= array(
									'panjang'	=> (int) $paket[$i]->Panjang,
									'lebar'		=> (int) $paket[$i]->Lebar,
									'tinggi'	=> (int) $paket[$i]->Tinggi
								);
				}
			
			}

			$status 	= "OK";
			$result[] 	= array(
							'no_resi'		=> $paket[$i]->NoTiket,
							'waktu' 		=> dateNum($paket[$i]->TglBerangkat) .' '. $paket[$i]->JamBerangkat,
							'jenis_barang'	=> $cfg['paket'][$paket[$i]->Dimensi],
							'jenis_layanan'	=> $cfg['layanan_paket'][$paket[$i]->JenisLayanan],
							'pengirim'		=> array(
													'nama' 		=> $paket[$i]->NamaPengirim,
													'telpon' 	=> $paket[$i]->TelpPengirim,
													'alamat' 	=> $paket[$i]->AlamatPengirim,
												),
							'penerima'		=> array(
													'nama' 		=> $paket[$i]->NamaPenerima,
													'telpon' 	=> $paket[$i]->TelpPenerima,
													'alamat' 	=> $paket[$i]->AlamatPenerima,
												),
							'label'			=> $label,
							'dimensi'		=> $dimensi,
							'koli'			=> (int) $paket[$i]->JumlahKoli,
							'berat'			=> (int) $paket[$i]->Berat,
							'biaya'			=> (int) $paket[$i]->TotalBayar,
							'keterangan' 	=> $paket[$i]->KeteranganPaket,
							'pembayaran' 	=> $pembayaran,
							'status' 		=> $kirim
						);

		}

		$data['status']	= $status;	
		$data['result']	= $result; 
		
		$this->sendResponse($data);
	}

	private function _generateNoTiketPaket(){
	
		$temp	= array("-",
						"1","2","3","4","5","6","7","8","9",
						"A","B","C","D","E","F","G","H","I","J",
						"K","L","M","N","O","P","Q","R","S","T",
						"U","V","W","X","Y","Z",
						"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
						"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
						"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		= $temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		
		return "P".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
	}

	private function _generateOTP(){

		$digit 	= 6 ;
    	$otp 	= rand(pow(10, $digit-1), pow(10, $digit)-1);

    	return $otp;

	}

}
?>