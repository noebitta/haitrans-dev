<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class LaporanController extends Controller{

	private $_jurusanModel;
	private $_jadwalModel;
	private $_reservasiModel;
	private $_spjModel;
	private $_laporanModel;
	private $_userModel;

	public function __construct(){

		parent::__construct();


		$this->_jurusanModel 	= $this->loadModel('Jurusan');
		$this->_jadwalModel 	= $this->loadModel('Jadwal');
		$this->_reservasiModel 	= $this->loadModel('Reservasi');
		$this->_spjModel 		= $this->loadModel('SPJ');
		$this->_laporanModel	= $this->loadModel('Laporan');
		$this->_userModel		= $this->loadModel('User');

	}

	public function index(){

		
	}

	public function keuangan(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$tgl_awal 	= $this->getParam('tgl_awal');
		$tgl_akhir	= $this->getParam('tgl_akhir');
		$user 		= $this->getUserId();

		if(strtotime($tgl_akhir) < strtotime($tgl_awal)){
			$this->error("Sortir tanggal bermasalah");
		}

		$reservasi 	= $this->_reservasiModel->getReservasiUser($user, $tgl_awal, $tgl_akhir);
		$spj 		= $this->_spjModel->getSPJUser($user, $tgl_awal, $tgl_akhir);

		$tiket		= 0;
		$tunai 		= 0;
		$debit 		= 0;
		$kredit		= 0;
		$manifest	= 0;
		$omzet		= 0;
		$diskon		= 0;
		$setoran	= 0;
		$tempspj 	= array();

		if(sizeof($reservasi) > 0){

			for ($i = 0; $i < sizeof($reservasi); $i++) { 
				
				if($reservasi[$i]->PetugasCetakTiket == $user){

					$tiket 		= $tiket + 1;
					$diskon 	= $diskon + $reservasi[$i]->Discount;
					$omzet 		= $omzet + $reservasi[$i]->HargaTiket;
					$setoran 	= $setoran + $reservasi[$i]->Total;

					/*
					 * Data jenis pembayaran
					 */
					switch ($reservasi[$i]->JenisPembayaran) {

						case '0': $tunai 	= $tunai + 1; 	break;
						case '1': $debit 	= $debit + 1; 	break;
						case '2': $kredit 	= $kredit + 1; 	break;

					}

				}

			}

			for ($i = 0; $i < sizeof($spj); $i++) { 
				
				if(in_array($spj[$i]->NoSPJ, $tempspj)){

					$tempspj[] = $spj[$i]->NoSPJ;

				}

				$manifest 	= sizeof($tempspj);

			}

		}

		$status 	= "OK";
		$result 	= array(
						'total_cetak_tiket'  	=> $tiket,
						'total_tunai' 			=> $tunai,
						'total_debit' 			=> $debit,
						'total_kredit' 			=> $kredit,
						'total_cetak_manifest' 	=> $manifest,
						'total_omzet' 			=> $omzet,
						'total_diskon' 			=> $diskon,
						'total_setoran' 		=> $setoran,
					);
		
		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);

	}

	public function penjualan(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$tgl_awal 	= $this->getParam('tgl_awal');
		$tgl_akhir	= $this->getParam('tgl_akhir');
		$user 		= $this->getUserId();

		if(strtotime($tgl_akhir) < strtotime($tgl_awal)){
			$this->error("Sortir tanggal bermasalah");
		}

		$reservasi 	= $this->_reservasiModel->getReservasiUser($user, $tgl_awal, $tgl_akhir);

		$total 		= 0;
		$booking	= 0;
		$tiket 		= 0;

		if(sizeof($reservasi) > 0){

			$total 	= sizeof($reservasi);

			for ($i = 0; $i < sizeof($reservasi); $i++) { 
				
				if($reservasi[$i]->PetugasPenjual == $user) $booking = $booking + 1;
				if($reservasi[$i]->PetugasCetakTiket == $user AND $reservasi[$i]->PetugasPenjual == $user) $tiket = $tiket + 1;

			}

		}

		$status 	= "OK";
		$result 	= array(
						'total_penjualan'	=> $booking + $tiket,
						'total_booking' 	=> $booking,
						'total_tiket' 		=> $tiket
					);
		
		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);

	}

	public function setoran(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$tgl_awal 	= $this->getParam('tgl_awal');
		$tgl_akhir	= $this->getParam('tgl_akhir');
		$user 		= $this->getUserId();

		$getreservasi 		= $this->_laporanModel->getSetoranReservasiUser($user);
		$getpaket 			= $this->_laporanModel->getSetoranPaketUser($user);
		$getbiaya 			= $this->_laporanModel->getSetoranBiayaUser($user);
		$getrekap 			= $this->_laporanModel->getRekapSetoranUser($user, $tgl_awal, $tgl_akhir);

		$setoran 			= array();
		$rekap 				= array();
		
		$penumpang_qty 		= 0;
		$penumpang_diskon	= 0;
		$penumpang_setoran 	= 0;
		$paket_qty 			= 0;
		$paket_setoran 		= 0;
		$biaya 				= 0;
		$total 				= 0;

		if($getreservasi){
			$penumpang_qty 		= $getreservasi->qty;
			$penumpang_diskon	= $getreservasi->diskon;
			$penumpang_setoran 	= $getreservasi->total;
		}

		if($getpaket){
			$paket_qty 			= $getpaket->qty;
			$paket_setoran 		= $getpaket->total;
		}

		if($getbiaya){
			$biaya 				= $getbiaya->total;
		}

		$total 		= $penumpang_setoran + $paket_setoran - $biaya;

		$setoran 	= array(
						'penumpang' => array(
										'qty' 		=> $penumpang_qty,
										'diskon'	=> $penumpang_diskon,
										'setoran' 	=> $penumpang_setoran
									),
						'paket' 	=> array(
										'qty' 		=> $paket_qty,
										'setoran'	=> $paket_setoran
									),
						'biaya' 	=> $biaya,
						'total'		=> $total
					);

		if($getrekap){

			for ($i = 0; $i < sizeof($getrekap); $i++) { 
				
				$rekap[] 	= array(
								'no_resi' 					=> $getrekap[$i]->IdSetoran,
								'waktu_setor' 				=> $getrekap[$i]->WaktuSetoran,
								'jumlah_tiket_umum' 		=> $getrekap[$i]->JumlahTiketUmum,
								'jumlah_tiket_diskon' 		=> $getrekap[$i]->JumlahTiketDiskon,
								'omzet_penumpang_tunai' 	=> $getrekap[$i]->OmzetPenumpangTunai,
								'omzet_penumpang_debit' 	=> $getrekap[$i]->OmzetPenumpangDebit,
								'omzet_penumpang_kredit' 	=> $getrekap[$i]->OmzetPenumpangKredit,
								'total_diskon' 				=> $getrekap[$i]->TotalDiskon,
								'omzet_paket' 				=> $getrekap[$i]->OmzetPaket,
								'total_biaya' 				=> $getrekap[$i]->TotalBiaya
							);

			}

		}

		$status 	= "OK";
		$result 	= array(
						'setor'	=> $setoran,
						'rekap' => $rekap
					);
		
		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);

	}

	public function rekap(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$user 		= $this->getUserId();
		$userdata 	= $this->_userModel->getDetail($user);

		$reservasi 	= $this->_laporanModel->getSetoranReservasiList($user);
		$paket 		= $this->_laporanModel->getSetoranPaketList($user);
		$biaya 		= $this->_laporanModel->getSetoranBiayaList($user);

		$penjualan 		= array();
		$data_penjualan = array();
		$data_tanggal 	= array();
		$data_jadwal	= array();

		try {

			$no_resi			= $this->_generateResi();
			$jumlah_tiket_umum 	= 0;
			$omzet_penumpang  	= 0;
			$omzet_tunai  		= 0;
			$omzet_debit  		= 0;
			$omzet_kredit  		= 0;
			$omzet_paket 		= 0;
			$total_diskon 		= 0;
			$total_biaya 		= 0;
			$total_paket 		= 0;
			$tunai 				= 0;

			$omzet_tiket_manifest 	= 0;
			$omzet_paket_manifest 	= 0;

			$index 	= 0;
			$index2 = 0;

			/* Data reservasi */
			for ($i = 0; $i < sizeof($reservasi); $i++) { 

				$kode_jadwal 	= $reservasi[$i]->KodeJadwal;
				$tgl_berangkat 	= $reservasi[$i]->TglBerangkat;

				if($reservasi[$i]->JenisPenumpang == 'U'){
					$jumlah_tiket_umum = $jumlah_tiket_umum + 1;
				}

				// Tunai
				if($reservasi[$i]->JenisPembayaran == '0'){
					$omzet_tunai 		 = $omzet_tunai + $reservasi[$i]->Total;
					$tunai[$kode_jadwal] = $omzet_tunai + $reservasi[$i]->Total;
				}

				// Debit
				if($reservasi[$i]->JenisPembayaran == '1'){
					$omzet_debit = $omzet_debit + $reservasi[$i]->Total;
				}

				// Kredit
				if($reservasi[$i]->JenisPembayaran == '2'){
					$omzet_kredit = $omzet_kredit + $reservasi[$i]->Total;
				}

				$omzet_penumpang 	= $omzet_penumpang + $reservasi[$i]->Total;
				$total_diskon 		= $total_diskon + $reservasi[$i]->Discount;

				/* Daftar penjualan pertanggal berangkat */
				$penumpang[$kode_jadwal] = 	$penumpang[$kode_jadwal] + 1;

				if($reservasi[$i]->JenisPenumpang == 'U'){
					$umum[$kode_jadwal]	= $umum[$kode_jadwal] + 1; 
				}

				// Grouping per jadwal
				if(!in_array($kode_jadwal, $data_jadwal)){

					$index2 = $index2 + 1;
					$data_jadwal[] 	= $reservasi[$i]->KodeJadwal;
				}

					
				$jadwal 		= $this->_jadwalModel->getDetail($kode_jadwal);
				$jurusan 		= $this->_jurusanModel->getDetailById($jadwal->IdJurusan);
				$manifest 		= $this->_spjModel->getDetailSPJByJadwal($kode_jadwal, $tgl_berangkat);

				if($manifest){
					$cetak_manifest = "Sudah Cetak";
				}
				else{
					$cetak_manifest = "Belum Cetak";
				}

				$data_penjualan[$tgl_berangkat][$index2]	= array(
																'jurusan'	=> $jurusan->cabang_asal .'-'. $jurusan->cabang_tujuan,
																'jam' 		=> $jadwal->JamBerangkat,
																'manifest' 	=> $cetak_manifest,
																'penumpang'	=> $penumpang[$kode_jadwal],
																'umum' 		=> $umum[$kode_jadwal],
																'tunai'		=> $tunai[$kode_jadwal]
															);
				

				// Grouping per jadwal
				if(!in_array($tgl_berangkat, $data_tanggal)){
					$index = $index + 1;
					$data_tanggal[]	= $tgl_berangkat;
				}

				$penjualan[$index] 	= array(
										'tanggal'	=> dateIn($tgl_berangkat),
										'data' 		=> array_values($data_penjualan[$tgl_berangkat])
									);


			}

			/* Data paket */
			$total_paket = sizeof($paket);
			
			for ($i = 0; $i < sizeof($paket); $i++) { 
				
				$omzet_paket 	= $omzet_paket + $paket->TotalBayar;

			}

			/* Data Biaya */
			$biaya_supir 	= 0;
			$biaya_parkir	= 0;
			$biaya_ins_supir= 0;
			$biaya_tol 		= 0;

			for ($i = 0 ; $i < sizeof($biaya); $i++) { 

				// Biaya Supir
				if($biaya[$i]->FlagJenisBiaya == '2'){
					$biaya_supir = $biaya_supir + $biaya[$i]->Jumlah;
				}

				// Biaya Insentif Supir
				if($biaya[$i]->FlagJenisBiaya == '5'){
					$biaya_ins_supir = $biaya_ins_supir + $biaya[$i]->Jumlah;
				}

				// Biaya Tol
				if($biaya[$i]->FlagJenisBiaya == '1'){
					$biaya_tol = $biaya_tol + $biaya[$i]->Jumlah;
				}

				// Biaya Parkir
				if($biaya[$i]->FlagJenisBiaya == '4'){
					$biaya_parkir = $biaya_parkir + $biaya[$i]->Jumlah;
				}
				
				$total_biaya = $total_biaya + $biaya[$i]->Jumlah;

			}

			$values 	= array();

			$values[] 	= "IdSetoran 			= '$no_resi'";
			$values[] 	= "WaktuSetoran 		= now()";
			$values[] 	= "IdUser 				= '$user'";
			$values[] 	= "NamaUser 			= '". $userdata->nama ."'";
			$values[] 	= "JumlahTiketUmum 		= '$jumlah_tiket_umum'";
			$values[] 	= "OmzetPenumpangTunai 	= '$omzet_tunai'";
			$values[] 	= "OmzetPenumpangDebit 	= '$omzet_debit'";
			$values[] 	= "OmzetPenumpangKredit = '$omzet_kredit'";
			$values[] 	= "TotalDiskon 			= '$total_diskon'";
			$values[] 	= "TotalPaket 			= '$total_paket'";
			$values[] 	= "OmzetPaket 			= '$omzet_paket'";
			$values[] 	= "TotalBiaya 			= '$total_biaya'";

			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_user_setoran', $values);

			/* Update no resi ke reservasi */
			for ($i = 0; $i < sizeof($reservasi); $i++) { 
				
				$values 	= array();

				$values[] 	= "WaktuSetor 		= now()";
				$values[] 	= "PenerimaSetoran	= ''";
				$values[] 	= "IdSetoran 		= '$no_resi'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values, array("NoTiket = '". $reservasi[$i]->NoTiket ."'"));

			}

			/* Update no resi ke paket */
			for ($i = 0; $i < sizeof($paket); $i++) { 
				
				$values 	= array();

				$values[] 	= "WaktuSetor 		= now()";
				$values[] 	= "PenerimaSetoran	= ''";
				$values[] 	= "IdSetoran 		= '$no_resi'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_paket', $values, array("NoTiket = '". $paket[$i]->NoTiket ."'"));

			}

			/* Update no resi ke biaya */
			for ($i = 0; $i < sizeof($biaya); $i++) { 
				
				$values 	= array();

				$values[] 	= "IdSetoran 		= '$no_resi'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_biaya_op', $values, array("IDBiayaOP = '". $biaya[$i]->IDBiayaOP ."'"));

			}

			$status		= "OK";
			$result		= array(
							'no_resi' 			=> $no_resi,
							'cso' 				=> $userdata->nama,
							'tanggal' 			=> date('d-m-Y'),
							'penjualan'	 		=> array_values($penjualan),
							'penjualan_tunai'	=> $omzet_tunai,
							'penjualan_tiket' 	=> sizeof($reservasi),
							'biaya' 			=> array(
													'supir' 		=> $biaya_supir,
													'ins_supir' 	=> $biaya_ins_supir,
													'parkir' 		=> $biaya_parkir,
													'tol' 			=> $biaya_tol,
													'total' 		=> $total_biaya
												),
							'manifest' 			=> array(
													'omzet_tiket' 	=> $omzet_tiket_manifest,
													'omzet_paket' 	=> $omzet_paket_manifest,
													'total_omzet' 	=> $omzet_tiket_manifest + $omzet_paket_manifest
												),
							'setoran_tunai'		=> $omzet_tunai - $total_biaya,
 							'waktu_cetak' 		=> dateNumFull(date('Y-m-d H:i:s'))
						);
			
		} catch (DbException $e) {
			$this->error($e->getMessage());
		}
		
		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function rekap_detail(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$no_resi 	= $this->getParam('no_resi');

		$user 		= $this->getUserId();
		$userdata 	= $this->_userModel->getDetail($user);

		$tikets 	= array();
		$pakets 	= array();
		$cargos 	= array();
		$biayas 	= array();

		$reservasi 	= $this->_laporanModel->getSetoranReservasiList($user);
		$paket 		= $this->_laporanModel->getSetoranPaketList($user);
		$biaya 		= $this->_laporanModel->getSetoranBiayaList($user);

		$supir 		= $this->_spjModel->getListSupir();
		$kendaraan	= $this->_spjModel->getListKendaraan();

		$supirs 	= array();
		$kendaraans	= array();

		/* Data supir */
		for ($i = 0; $i < sizeof($supir); $i++) { 
			
			$supirs[$supir->KodeSopir] = array(
											'kode' => $supir->KodeSopir,
											'nama' => $supir->Nama
										);

		}

		/* Data kendaraan */
		for ($i = 0; $i < sizeof($kendaraan); $i++) { 
			
			$kendaraans[$kendaraan->KodeKendaraan] = array(
														'kode' 		=> $kendaraan->KodeKendaraan,
														'no_polisi' => $kendaraan->NoPolisi,
														'jenis' 	=> $kendaraan->Jenis,
														'merek' 	=> $kendaraan->Merek
													);

		}

		/* Data sudah di setor */
		if($no_resi != ""){
			$reservasi 	= $this->_laporanModel->getSetoranReservasiResi($no_resi);
			$paket 		= $this->_laporanModel->getSetoranPaketResi($no_resi);
			$biaya 		= $this->_laporanModel->getSetoranBiayaResi($no_resi);
		}

		/* Data reservasi atau tiket */
		for ($i = 0; $i < sizeof($reservasi); $i++) { 
			
			$tikets[] 	= array(
							'waktu_cetak'		=> dateNumFull($reservasi[$i]->WaktuCetakTiket),
							'no_tiket' 			=> $reservasi[$i]->NoTiket,
							'waktu_berangkat'	=> dateNum($reservasi[$i]->TglBerangkat) .' '. $reservasi[$i]->JamBerangkat,
							'kode_jadwal' 		=> $reservasi[$i]->KodeJadwal,
							'nama' 				=> $reservasi[$i]->Nama,
							'telp' 				=> $reservasi[$i]->Telp,
							'kursi' 			=> $reservasi[$i]->NomorKursi,
							'harga_tiket' 		=> $reservasi[$i]->HargaTiket,
							'diskon'	 		=> $reservasi[$i]->Discount,
							'total' 			=> $reservasi[$i]->Total,
							'jenis_pembayaran'	=> $cfg['payment'][$reservasi[$i]->JenisPembayaran],
							'cso' 				=> $userdata->nama,
							'status' 			=> 'Tiket',
							'keterangan' 		=> ''
						);

		}

		/* Data paket */
		for ($i = 0; $i < sizeof($paket); $i++) { 
			
			$pakets[] 	= array(
							'waktu_berangkat' 	=> dateNum($paket[$i]->TglBerangkat) .' '. $paket[$i]->JamBerangkat,
							'no_tiket' 			=> $paket[$i]->NoTiket,
							'kode_jadwal' 		=> $paket[$i]->KodeJadwal,
							'dari' 				=> $paket[$i]->NamaPengirim,
							'untuk' 			=> $paket[$i]->NamaPenerima,
							'harga_paket' 		=> $paket[$i]->HargaPaket,
							'cso' 				=> $userdata->nama,
							'status' 			=> '',
							'keterangan' 		=> ''
						);

		}

		/* Data biaya */
		for ($i = 0; $i < sizeof($biaya); $i++) { 

			$manifest 	= $this->_spjModel->getDetailSPJ($biaya[$i]->NoSPJ);
			$tgl 		= substr($manifest->TglBerangkat, 0, 10);
			
			$biayas[] 	= array(
							'waktu_transaksi' 	=> dateNum($biaya[$i]->Tgltransaksi),
							'jadwal' 			=> $manifest->KodeJadwal,
							'waktu_berangkat' 	=> dateNum($tgl) .' '. $manifest->JamBerangkat,
							'manifest' 			=> $biaya[$i]->NoSPJ,
							'jenis_biaya' 		=> $cfg['biaya'][$biaya[$i]->FlagJenisBiaya],
							'supir' 			=> $supir[$biaya[$i]->KodeSopir]['nama'],
							'unit' 				=> $biaya[$i]->NoPolisi,
							'jumlah' 			=> $biaya[$i]->Jumlah,
							'keterangan' 		=> '',
						);

		}

		$status 	= "OK";
		$result 	= array(
						'tiket' => $tikets,
						'paket'	=> $pakets,
						'cargo' => $cargos,
						'biaya' => $biayas,
					);

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	private function _generateResi(){

		global $cfg;
		
		$temp	= array("0",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		= $temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		
		do{
			$rnd1	= $temp[rand(1,61)];
			$rnd2	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			
			$id_setoran	= "RKS".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s.$rnd3;
		
			$res 	= $this->_laporanModel->getSetoranDetail($id_setoran);
		
		} while($res);
		
		return $id_setoran;

	}

}
?>