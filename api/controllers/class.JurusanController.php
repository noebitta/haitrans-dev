<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class JurusanController extends Controller{

	private $_jurusanModel;

	public function __construct(){

		parent::__construct();

		$this->_jurusanModel = $this->loadModel('Jurusan');

	}

	public function index(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$asal 		= $this->getParam('asal');
		$jurusan  	= $this->_jurusanModel->getList($asal);

		if($jurusan){

			$status	= "OK";

			for ($i = 0; $i < sizeof($jurusan); $i++) {

				$result[]	= array(
								'id_jurusan'	=> $jurusan[$i]->id_jurusan,
								'kode_jurusan'	=> $jurusan[$i]->kode_jurusan,
								'cabang_asal'	=> ucwords(strtolower($jurusan[$i]->cabang_asal)),
								'kota_asal' 	=> ucwords(strtolower($jurusan[$i]->kota_asal)),
								'cabang_tujuan' => ucwords(strtolower($jurusan[$i]->cabang_tujuan)),
								'kota_tujuan'	=> ucwords(strtolower($jurusan[$i]->kota_tujuan)),
								'harga_tiket'	=> $jurusan[$i]->harga_tiket
							);
			}
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

}
?>