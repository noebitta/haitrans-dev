<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class ReservasiController extends Controller{

	private $_reservasiModel;
	private $_jurusanModel;
	private $_jadwalModel;
	private $_userModel;
	private $_pelangganModel;

	public function __construct(){

		parent::__construct();

		$this->_reservasiModel 	= $this->loadModel('Reservasi');
		$this->_jurusanModel 	= $this->loadModel('Jurusan');
		$this->_jadwalModel 	= $this->loadModel('Jadwal');
		$this->_userModel 		= $this->loadModel('User');
		$this->_pelangganModel 	= $this->loadModel('Pelanggan');

	}

	public function index(){

	}

	public function jenis_diskon(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data				= array();
		$result    			= array();
		$status     		= "ZERO_RESULTS";

		$id_jurusan			= $this->getParam('id_jurusan');
		$jenis_penumpang 	= $this->getParam('jenis_penumpang'); // M = Mahasiswa | PP = pp | K = Reguler
		$id_member 			= $this->getParam('id_member');
		$tgl_berangkat 		= $this->getParam('tgl_berangkat');

		$jurusan 			= $this->_jurusanModel->getDetail($id_jurusan);
		$diskon 			= $this->_reservasiModel->getJenisDiskon($jenis_penumpang);

		if($jurusan == FALSE){
			$this->error('Jurusan tidak tersedia');
		}

		$harga 		= $jurusan->HargaTiket;

		//Penumpang Umum
		if ($jenis_penumpang == "M") {

			$member 	=  $this->_pelangganModel->getMemberDetail($id_member);

			if(!$member){
				$this->error("Member tidak terdaftar");
			}

			$reservasi 	= $this->_reservasiModel->getReservasiMember($member->IdMember, $tgl_berangkat);

			$potongan 	= $diskon[0]->JumlahDiscount;

			if($potongan < 1){
				$potongan 	= $harga * $potongan;
			}
			
			if(sizeof($reservasi) == 0){
				
				$result[] 	= array(
								'id_diskon'		=> $diskon[0]->IdDiscount,
								'harga'			=> max($harga - $potongan, 0),
								'keterangan'	=> $diskon[0]->NamaDiscount
							);
			}	

			$status  	= "OK";
			$result[] = array(
							'id_diskon'		=> "",
							'harga'			=> max($harga, 0),
							'keterangan'	=> "Umum"
						);
			
			
			
		}
		elseif($jenis_penumpang == "PP") {
			
			$harino  		= date('N', strtotime($tgl_berangkat));
			$hari  			= $cfg['hari'][$harino];

			$getpp 			= $this->_jadwalModel->getPromoPPByHari($tgl_berangkat, $hari);
			$potongan 		= $getpp->JumlahDiscount * 2;

			if(!$getpp){
				$this->error("Promo pp tidak tersedia");
			}

			$status  	= "OK";
			$result[] 	= array(
							'id_diskon'		=> "pp",
							'harga'			=> max(($harga * 2) - $potongan, 0),
							'keterangan'	=> "Promo PP"
						);


		}
		else {

			$status  	= "OK";
			$result[] 	= array(
							'id_diskon'		=> "",
							'harga'			=> max($harga, 0),
							'keterangan'	=> "Umum"
						);
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);

	}

	public function booking(){

		global $cfg;

		$this->setRequestMethod('POST');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$id_jurusan		= $this->postParam('id_jurusan');
		$kode_jadwal	= $this->postParam('kode_jadwal');
		$tgl_berangkat	= $this->postParam('tgl_berangkat');

		/* Promo PP */
		$id_jurusan2	= $this->postParam('id_jurusan_pulang');
		$kode_jadwal2	= $this->postParam('kode_jadwal_pulang');
		$tgl_berangkat2 = $this->postParam('tgl_berangkat_pulang');

		$no_telepon		= $this->postParam('no_telepon');
		$nama			= $this->postParam('nama');
		$keterangan		= $this->postParam('keterangan');
		$jenis_diskon	= $this->postParam('jenis_diskon');
		$pembayaran		= $this->postParam('jenis_pembayaran');
		$cetak_tiket	= $this->postParam('cetak_tiket');
		$id_member 		= $this->postParam('id_member');
		$kode_voucher 	= $this->postParam('kode_voucher');
		$user 			= $this->getUserId();

		$jurusan 		= $this->_jurusanModel->getDetail($id_jurusan);
		$jadwal  		= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$member 		= $this->_pelangganModel->getMemberDetail($id_member);
		
		if($jurusan == FALSE){
			$this->error('Jurusan tidak tersedia');
		}

		if($jadwal == FALSE){
			$this->error("Tidak ada keberangkatan");
		}

		if(!$member && $id_member != '' && $id_member != 'null'){
			$this->error("Member tidak terdaftar");
		}

		$kode_jadwal_aktual = $jadwal->FlagSubJadwal == '1' ? $jadwal->KodeJadwalUtama : $jadwal->KodeJadwal;
		$jam_berangkat 		= $jadwal->JamBerangkat;		

		/*
		 * Data detail jurusan
		 */
		$harga_tiket 		= $jurusan->HargaTiket;
		$komisi_p_CSO 		= $jurusan->KomisiPenumpangCSO;
		$kda_komisi_CSO 	= $jurusan->KodeAkunKomisiPenumpangCSO;
		$kda_pendapatan 	= $jurusan->KodeAkunPendapatanPenumpang;

		$asal 				= $this->_jurusanModel->getCabang($jurusan->KodeCabangAsal);
		$tujuan 			= $this->_jurusanModel->getCabang($jurusan->KodeCabangTujuan);
		$cabang_asal 		= $asal->Nama;
		$cabang_tujuan 		= $tujuan->Nama;

		/*
		 * Data detail posisi 
		 */
		$posisi  			= $this->_reservasiModel->getPosisi($kode_jadwal_aktual, $tgl_berangkat);
		$sisa_kursi 		= $posisi->SisaKursi;
		$no_spj 			= $posisi->NoSPJ;
		$kode_kendaraan 	= $posisi->KodeKendaraan;
		$kode_supir 		= $posisi->KodeSopir;
		$tgl_cetak_spj 		= $posisi->TglCetakSpj;
		$petugas_cetak_spj 	= $posisi->PetugasCetakSpj;

		$flag_kursi 		= $this->_reservasiModel->getFlagByUserAll($kode_jadwal_aktual, $tgl_berangkat, $user);

		/**
		 * Data detail jurusan & jadwal pulang
		 * Promo PP
		 */
		if($jenis_diskon == 'pp'){

			$jurusan_pulang	= $this->_jurusanModel->getDetail($id_jurusan2);
			$jadwal_pulang	= $this->_jadwalModel->getDetail($kode_jadwal2, $tgl_berangkat2);

			if(!$jurusan_pulang){
				$this->error("Jurusan pulang tidak tersedia");
			}

			if(!$jadwal_pulang){
				$this->error("Tidak ada keberangkatan pulang");
			}

			$kode_jadwal_aktual2 = $jadwal_pulang->FlagSubJadwal == '1' ? $jadwal_pulang->KodeJadwalUtama : $jadwal_pulang->KodeJadwal;
			$jam_berangkat2 	= $jadwal_pulang->JamBerangkat;

			/*
			 * Data detail jurusan
			 */
			$komisi_p_CSO2 		= $jurusan->KomisiPenumpangCSO;
			$kda_komisi_CSO2 	= $jurusan->KodeAkunKomisiPenumpangCSO;
			$kda_pendapatan2 	= $jurusan->KodeAkunPendapatanPenumpang;

			$asal2 				= $this->_jurusanModel->getCabang($jurusan->KodeCabangAsal);
			$tujuan2 			= $this->_jurusanModel->getCabang($jurusan->KodeCabangTujuan);
			$cabang_asal2 		= $asal2->Nama;
			$cabang_tujuan2 	= $tujuan2->Nama;

			/*
			 * Data detail posisi pulang
			 */
			$posisi2 			= $this->_reservasiModel->getPosisi($kode_jadwal_aktual2, $tgl_berangkat2);
			$sisa_kursi2 		= $posisi2->SisaKursi;
			$no_spj2 			= $posisi2->NoSPJ;
			$kode_kendaraan2 	= $posisi2->KodeKendaraan;
			$kode_supir2 		= $posisi2->KodeSopir;
			$tgl_cetak_spj2 	= $posisi2->TglCetakSpj;
			$petugas_cetak_spj2 = $posisi2->PetugasCetakSpj;

			$flag_kursi2 		= $this->_reservasiModel->getFlagByUserAll($kode_jadwal_aktual2, $tgl_berangkat2, $user);

			if(sizeof($flag_kursi2) == 0){
				$this->error("Data kursi belum tersedia");
			}
		}		

		/*
		 * Data booking
		 */
		$kode_booking 		= $this->_reservasiModel->generateKodeBooking();
		$now 				= date('Y-m-d H:i:s');
		$kode_payment 		= "";
		
		$pembayaran 		= $pembayaran == '' ? '0' : $pembayaran;
		$charge 			= 0;
		$PPN 				= 0;
		
		$userdata 			= $this->_userModel->getDetail($user);
		$kode_cabang 		= $userdata->KodeCabang;
		$nama_petugas 		= $userdata->nama;

		/* Get reservasi member */
		if($member){
			$reservasi 	= $this->_reservasiModel->getReservasiMember($member->IdMember, $tgl_berangkat);
		}

		try {

			$this->dbObj->beginTrans();

			$pemesanan 		= array();
			$total_biaya 	= 0;
			$pulang 	 	= array();

			/*
			 * Looping flag kursi yang tersedia
			 */
			for ($i = 0; $i < sizeof($flag_kursi); $i++) { 

				// Generate OTP
				$otp 		= $this->_generateOTP();
				$no_tiket 	= $this->_reservasiModel->generateNoTiket();

				/*
				 * Data detail diskon 
				 */
				if($jenis_diskon != ""){

					switch($jenis_diskon){

						case "1" :

							if(sizeof($reservasi) == 0){
								// Diskon mahasiswa
								$data_diskon	= $this->_reservasiModel->getDetailJenisDiskon($jenis_diskon);

								$potongan 		= $data_diskon->JumlahDiscount;
								$kode_diskon 	= $data_diskon->KodeDiscount;
								$nama_diskon 	= $data_diskon->NamaDiscount;
								$jenis_penumpang= $kode_diskon;

								if($potongan < 1){
									$potongan 	= $harga_tiket * $potongan;
								}

								// kirim sms member mahasiswa
								$this->_sendSMS($member->Nama, $member->Handphone, $otp, $no_tiket, $kode_booking, '1');
							}
							else{
								// Mahasiswa lebih dari 1 kembali ke harga umum
								$potongan 		= 0;
								$kode_diskon 	= "";
								$nama_diskon 	= "";
								$jenis_penumpang= "U";
							}

						break;

						case "pp" :
							
							// Promo PP
							$harino  		= date('N', strtotime($tgl_berangkat));
							$hari  			= $cfg['hariIn'][$harino];

							$getpp 			= $this->_jadwalModel->getPromoPPByHari($tgl_berangkat, $hari);
							$potongan 		= $getpp->JumlahDiscount;
							
							// $potongan 		= $potongan / 2;
							$kode_diskon 	= $getpp->KodePromo;
							$nama_diskon 	= $getpp->NamaPromo;
							$jenis_penumpang= "PP";

						break; 

						default :

							$data_diskon	= $this->_reservasiModel->getDetailJenisDiskon($jenis_diskon);

							$potongan 		= $data_diskon->JumlahDiscount;
							$kode_diskon 	= $data_diskon->KodeDiscount;
							$nama_diskon 	= $data_diskon->NamaDiscount;
							$jenis_penumpang= $kode_diskon;

							if($potongan < 1){
								$potongan 	= $harga_tiket * $potongan;
							}

						break;

					}

				}
				else{
					$potongan 		= 0;
					$kode_diskon 	= "";
					$nama_diskon 	= "";
					$jenis_penumpang= "U";

					// kirim smsm member reguler
					if($member && $member->KategoriMember == '0'){
						
						$this->_sendSMS($member->Nama, $member->Handphone, $otp, $no_tiket, $kode_booking, '1');

						$jenis_penumpang= "K";
					}
				}

				if($kode_voucher != ''){

					$voucher 		= $this->_reservasiModel->getVoucherDetail($kode_voucher);
					$tanggal 		= date('Y-m-d');
					$hari 			= date('N');

					if(!$voucher){
						$this->error("Kode voucher salah");
					}

					if(strtotime($tanggal) > strtotime($voucher->ExpiredDate)){
						$this->error("Voucher telah kadaluarsa");
					}

					if(($hari == '1' || $hari == '6' || $hari == '7') && $voucher->IsBolehWeekEnd == '0'){
						$this->error("Voucher hanya bisa digunakan weekday");
					}

					if($voucher->IdJurusan != NULL && $voucher->IdJurusan != $jadwal->IdJurusan){
						$this->error("Voucher tidak bisa digunakan di jurusan ini");
					}

					if($voucher->KodeJadwal !=  NULL && $voucher->KodeJadwal != $kode_jadwal){
						$this->error("Voucher tidak bisa digunakan di jadwal ini");
					}

					if($voucher->IsReturn == '1'){
						$this->error("Voucher hanya bisa digunakan untuk return jurusan ini");
					}

					$values 	= array();

					$values[]	= "IdJurusan		= '". $jurusan->IdJurusan ."'";
					$values[]	= "KodeJadwal		= '". $kode_jadwal ."'";
					$values[]	= "Cabangberangkat 	= '". $jurusan->KodeCabangAsal ."'";
					$values[]	= "CabangTujuan		= '". $jurusan->KodeCabangTujuan ."'";
					$values[]	= "WaktuDigunakan	= now()";
					$values[]	= "PetugasPengguna	= '$user'"; 

					$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_voucher', $values, array("KodeVoucher = '$kode'"));

					$potongan 	= $harga_tiket;
					$nama_diskon= $kode_voucher;

				}
				
				$potongan  		= $potongan > $harga_tiket ? $harga_tiket : $potongan;
				$subtotal 		= $harga_tiket;
				$total 			= $subtotal + $charge + $PPN - $potongan;
				
				$kursi 			= $flag_kursi[$i]->NomorKursi;

				$pemesanan[]	= array(
									'no_tiket' 		=> $no_tiket,
									'no_kursi' 		=> $kursi,
									'harga_tiket' 	=> (int) $harga_tiket,
									'diskon'		=> (int) $potongan,
									'total_bayar' 	=> (int) $total
								);
				$total_biaya 	= $total_biaya + $total;

				/*
				 * Tambah reservasi
				 */
				$values			= array();

				$values[]		= "NoTiket 						= '$no_tiket'";
				$values[]		= "KodeCabang					= '$kode_cabang'";
				$values[]		= "KodeJadwal 					= '$kode_jadwal'";
				$values[]		= "IdJurusan 					= '$id_jurusan'";
				$values[]		= "KodeKendaraan				= '$kode_kendaraan'";
				$values[]		= "KodeSopir					= '$kode_supir'";
				$values[]		= "TglBerangkat					= '$tgl_berangkat'";
				$values[]		= "JamBerangkat					= '$jam_berangkat'";
				$values[]		= "KodeBooking					= '$kode_booking'";
				$values[]		= "IdMember						= '$id_member'";
				$values[]		= "Nama							= '$nama'";
				$values[]		= "Telp							= '$no_telepon'";
				$values[]		= "WaktuPesan					= '$now'";
				$values[]		= "NomorKursi					= '$kursi'";
				$values[]		= "HargaTiket					= '$harga_tiket'";
				$values[]		= "Charge						= '$charge'";
				$values[]		= "Subtotal						= '$subtotal'";
				$values[]		= "Discount						= '$potongan'";
				$values[]		= "PPN							= '$PPN'";
				$values[]		= "Total						= '$total'";
				$values[]		= "PetugasPenjual				= '$user'";
				$values[]		= "NoSPJ						= '$no_spj'";
				$values[]		= "TglCetakSpj					= '$tgl_cetak_spj'";
				$values[]		= "CetakSPJ						= '0'";
				$values[]		= "KomisiPenumpangCSO			= '$komisi_p_CSO'";
				$values[]		= "PetugasCetakSpj				= '$petugas_cetak_spj'";
				$values[]		= "Keterangan					= '$keterangan'";
				$values[]		= "JenisPembayaran				= '$pembayaran'";
				$values[]		= "JenisDiscount				= '$nama_diskon'";
				$values[]		= "KodeAkunPendapatan			= '$kda_pendapatan'";
				$values[]		= "JenisPenumpang				= '$jenis_penumpang'";
				$values[]		= "KodeAkunKomisiPenumpangCSO	= '$kda_komisi_CSO'";
				$values[]		= "OTP							= '$otp'";
				//$values[]		= "NeedPaymentCode				= '0'";

				//jika member mahasiswa atau member reguler
				if(($jenis_diskon == '1' && sizeof($reservasi) == 0 && $i == 0) || ($member && $member->KategoriMember == '0')){

				$values[]		= "FlagSmsOTP					= '1'";
				
				}

				//jika langsung cetak tiket atau go show
				if($cetak_tiket == '1'){
				
				$values[]		= "CetakTiket					= '1'"; //cetak tiket
				$values[]		= "PetugasCetakTiket			= '$user'"; //cetak tiket
				$values[]		= "WaktuCetakTiket				= '$now'"; //cetak tiket

				}

				//jika go show pp
				if($cetak_tiket == '1' && $jenis_penumpang == 'PP'){

				$values[]		= "OTPUsed					= '1'";

				}

				$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values);

				/*
				 * Update posisi sisa kursi
				 */
				$sisa_kursi 	= $sisa_kursi - 1;
				$values			= array();

				$values[]		= "SisaKursi					= '$sisa_kursi'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi', $values, array("KodeJadwal = '$kode_jadwal_aktual' AND TglBerangkat = '$tgl_berangkat'"));

				/*
				 * Update posisi detail
				 */
				$values			= array();

				$values[]		= "NoTiket						= '$no_tiket'";
				$values[]		= "KodeBooking					= '$kode_booking'";
				$values[]		= "Nama							= '$nama'";

				//jika langsung cetak tiket atau go show
				if($cetak_tiket == '1'){

				$values[]		= "StatusBayar					= '1'";
				
				}

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values, array("KodeJadwal = '$kode_jadwal_aktual' AND TglBerangkat = '$tgl_berangkat' AND NomorKursi = '$kursi'"));

				//jika langsung cetak tiket atau go show dan jika pembarayan menggunakan voucher
				if($cetak_tiket == '1' && $pembayaran == '3' && $kode_voucher != ''){

				$values			= array();

				$values[]		= "NoTiket					= '$no_tiket'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_voucher', $values, array("KodeVoucher = '$kode_voucher'"));

				}

			}

			if($jenis_diskon == 'pp'){

				/**
				 * Looping flag kursi yang tersedia pulang
				 * Promo PP
				 */
				for ($i = 0; $i < sizeof($flag_kursi2); $i++) { 

					// Generate OTP
					$otp = $this->_generateOTP();

					/*
					 * Data detail diskon 
					 */
					$harino  		= date('N', strtotime($tgl_berangkat2));
					$hari  			= $cfg['hariIn'][$harino];

					$getpp 			= $this->_jadwalModel->getPromoPPByHari($tgl_berangkat2, $hari);
					$potongan 		= $getpp->JumlahDiscount;
								
					// $potongan 		= $potongan / 2;
					$kode_diskon 	= $getpp->KodePromo;
					$nama_diskon 	= $getpp->NamaPromo;
					$jenis_penumpang= "PP";
					
					$potongan  		= $potongan > $harga_tiket ? $harga_tiket : $potongan;
					$subtotal 		= $harga_tiket;
					$total 			= $subtotal + $charge + $PPN - $potongan;
					
					$kursi 			= $flag_kursi2[$i]->NomorKursi;
					$no_tiket 		= $this->_reservasiModel->generateNoTiket();

					$pemesanan2[]	= array(
										'no_tiket' 		=> $no_tiket,
										'no_kursi' 		=> $kursi,
										'harga_tiket' 	=> (int) $harga_tiket,
										'diskon'		=> (int) $potongan,
										'total_bayar' 	=> (int) $total
									);

					/*
					 * Tambah reservasi
					 */
					$values			= array();

					$values[]		= "NoTiket 						= '$no_tiket'";
					$values[]		= "KodeCabang					= '$kode_cabang'";
					$values[]		= "KodeJadwal 					= '$kode_jadwal2'";
					$values[]		= "IdJurusan 					= '$id_jurusan2'";
					$values[]		= "KodeKendaraan				= '$kode_kendaraan'";
					$values[]		= "KodeSopir					= '$kode_supir'";
					$values[]		= "TglBerangkat					= '$tgl_berangkat2'";
					$values[]		= "JamBerangkat					= '$jam_berangkat2'";
					$values[]		= "KodeBooking					= '$kode_booking'";
					$values[]		= "IdMember						= '$id_member'";
					$values[]		= "Nama							= '$nama'";
					$values[]		= "Telp							= '$no_telepon'";
					$values[]		= "WaktuPesan					= '$now'";
					$values[]		= "NomorKursi					= '$kursi'";
					$values[]		= "HargaTiket					= '$harga_tiket'";
					$values[]		= "Charge						= '$charge'";
					$values[]		= "Subtotal						= '$subtotal'";
					$values[]		= "Discount						= '$potongan'";
					$values[]		= "PPN							= '$PPN'";
					$values[]		= "Total						= '$total'";
					$values[]		= "PetugasPenjual				= '$user'";
					$values[]		= "NoSPJ						= '$no_spj2'";
					$values[]		= "TglCetakSpj					= '$tgl_cetak_spj2'";
					$values[]		= "CetakSPJ						= '0'";
					$values[]		= "KomisiPenumpangCSO			= '$komisi_p_CSO2'";
					$values[]		= "PetugasCetakSpj				= '$petugas_cetak_spj2'";
					$values[]		= "Keterangan					= '$keterangan'";
					$values[]		= "JenisPembayaran				= '$pembayaran'";
					$values[]		= "JenisDiscount				= '$nama_diskon'";
					$values[]		= "KodeAkunPendapatan			= '$kda_pendapatan2'";
					$values[]		= "JenisPenumpang				= '$jenis_penumpang'";
					$values[]		= "KodeAkunKomisiPenumpangCSO	= '$kda_komisi_CSO2'";
					$values[]		= "OTP							= '$otp'";
					$values[]		= "FlagSmsOTP					= '1'";
					$values[]		= "NeedPaymentCode				= '0'";

					//jika langsung cetak tiket atau go show
					if($cetak_tiket == '1'){
					
					$values[]		= "CetakTiket					= '1'"; //cetak tiket
					$values[]		= "PetugasCetakTiket			= '$user'"; //cetak tiket
					$values[]		= "WaktuCetakTiket				= '$now'"; //cetak tiket

					}

					$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values);

					/*
					 * Update posisi sisa kursi
					 */
					$sisa_kursi 	= $sisa_kursi - 1;
					$values			= array();

					$values[]		= "SisaKursi					= '$sisa_kursi'";

					$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi', $values, array("KodeJadwal = '$kode_jadwal_aktual2' AND TglBerangkat = '$tgl_berangkat2'"));

					/*
					 * Update posisi detail
					 */
					$values			= array();

					$values[]		= "NoTiket						= '$no_tiket'";
					$values[]		= "KodeBooking					= '$kode_booking'";
					$values[]		= "Nama							= '$nama'";

					//jika langsung cetak tiket atau go show
					if($cetak_tiket == '1'){

					$values[]		= "StatusBayar					= '1'";
					
					}

					$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values, array("KodeJadwal = '$kode_jadwal_aktual2' AND TglBerangkat = '$tgl_berangkat2' AND NomorKursi = '$kursi'"));

					//kirim sms
					$this->_sendSMSPP($nama, $no_telepon, $otp, $no_tiket, $kode_booking, '1', $tgl_berangkat2, $cabang_asal2 .'-'. $cabang_tujuan2, $jam_berangkat2);

				}

				$pulang = array(
							'kode_jadwal'	=> $kode_jadwal2,
							'cabang_asal'	=> $cabang_asal2,
							'cabang_tujuan'	=> $cabang_tujuan2,
							'tgl_berangkat'	=> $tgl_berangkat2,
							'jam_berangkat' => $jam_berangkat2,
							'pemesanan'		=> $pemesanan2
						);

			}

			/*
			 * Insert atau update data pelanggan
			 */
			$pelanggan 		= $this->_pelangganModel->getDetailByTelp($no_telepon);

			if($pelanggan == FALSE){

				$values		= array();
				$date 		= date('Y-m-d');

				$values[]	= "NoHP					= '$no_telepon'";
				$values[]	= "NoTelp				= '$no_telepon'"; 
				$values[]	= "Nama					= '$nama'"; 
				$values[]	= "TglPertamaTransaksi	= '$date'"; 
				$values[]	= "TglTerakhirTransaksi	= '$date'"; 
				$values[]	= "CSOTerakhir			= '$user'"; 
				$values[]	= "KodeJurusanTerakhir	= '$kode_jadwal'"; 
				$values[]	= "FrekwensiPergi		= '1'"; 

				$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_pelanggan', $values);

			}
			else{
				$values		= array();
				$date 		= date('Y-m-d');
				$frek 		= $pelanggan->FrekwensiPergi + 1;

				$values[]	= "Nama					= '$nama'"; 
				$values[]	= "TglTerakhirTransaksi	= '$date'"; 
				$values[]	= "CSOTerakhir			= '$user'"; 
				$values[]	= "KodeJurusanTerakhir	= '$kode_jadwal'"; 
				$values[]	= "FrekwensiPergi		= '$frek'"; 

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_pelanggan', $values, array("NoTelp = '$no_telepon'"));
			}

			$this->dbObj->commitTrans();

			$status	= "OK";

			$result	= array(
							'kode_jadwal'	=> $kode_jadwal,
							'cabang_asal'	=> $cabang_asal,
							'cabang_tujuan'	=> $cabang_tujuan,
							'tgl_berangkat'	=> $tgl_berangkat,
							'jam_berangkat' => $jam_berangkat,
							'kode_booking' 	=> $kode_booking,
							'nama' 			=> $nama,
							'no_telepon' 	=> $no_telepon,
							'pemesanan'		=> $pemesanan,
							'total_biaya' 	=> (int) $total_biaya,
							'petugas_cetak' => $nama_petugas,
							'waktu_cetak' 	=> dateNumFull($now),
							'pulang' 		=> $pulang,
							'kode_voucher' 	=> $kode_voucher,
							'keterangan' 	=> ""
						);

		} catch (DbException $e) {
			$this->dbObj->rollbackTrans();

			Error::store('Reservasi', $e->getMessage());

			$this->internalError();
		}


		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function cetak_tiket(){

		global $cfg;

		$this->setRequestMethod('POST');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status 		= "ZERO_RESULTS";

		$no_tiket		= $this->postParam('no_tiket');
		$otp			= $this->postParam('otp');
		$cetak_ulang	= $this->postParam('cetak_ulang');
		$nama			= $this->postParam('nama');
		$no_telepon		= $this->postParam('no_telepon');
		$jenis_diskon	= $this->postParam('jenis_diskon');
		$pembayaran		= $this->postParam('jenis_pembayaran');
		$jenis_penumpang= $this->postParam('jenis_penumpang');
		$kode_voucher 	= $this->postParam('kode_voucher');
		$username 		= $this->postParam('username');
		$password 		= $this->postParam('password');
		$user 			= $this->getUserId();
		$now 			= date('Y-m-d H:i:s');

		$getdata 		= $this->_reservasiModel->getDetailbyTiket($no_tiket);

		$userdata 		= $this->_userModel->getDetail($user);
		$kode_cabang 	= $userdata->KodeCabang;
		$nama_petugas 	= $userdata->nama;
		$keterangan 	= "";

		if($getdata){

			if($cetak_ulang == '1'){

				$leader = $this->_userModel->userLogin($username, md5($password));

				if(!$leader || empty($leader)){
					$this->error('Username atau password salah !');
				}

				if($leader->user_active == 0) {
					$this->error('Akses user diblok, silahkan kontak support/admin');
				}

				//cek masa berlaku
				$berlaku 	= $leader->berlaku;
				$berlaku 	= strtotime($berlaku);
				$now 		= strtotime($now);

				if($now > $berlaku) {
					$this->error('Masa berlaku habis, silahkan kontak support/admin');
				}

				$level 		= $leader->user_level;
				$level 		= substr($level, 0, 1);

				if($level != '0' && $level != '1'){
					$this->error('Hak akses ditolak');
				}

			}

			if($getdata->FlagBatal == '1'){
				$this->error("Pemesanan telah dibatalkan");
			}

			if($jenis_penumpang != "U"){
				if($getdata->OTP != $otp){
					$this->error("Kode OTP yang dimasukan salah !");
				}
			}

			$member 		= $this->_pelangganModel->getMemberDetail($getdata->IdMember);

			if(!$member && $jenis_penumpang != "U" && $jenis_penumpang == "T" && $jenis_penumpang != "PP"){
				$this->error("Member tidak terdaftar");
			}

			$jadwal 		= $this->_jadwalModel->getDetail($getdata->KodeJadwal, $getdata->TglBerangkat);
			$kode_jadwal 	= $getdata->KodeJadwal;

			$jurusan 		= $this->_jurusanModel->getDetail($getdata->IdJurusan);
			$harga_tiket 	= $jurusan->HargaTiket;

			$asal 			= $this->_jurusanModel->getCabang($jurusan->KodeCabangAsal);
			$tujuan 		= $this->_jurusanModel->getCabang($jurusan->KodeCabangTujuan);
			$cabang_asal 	= $asal->Nama;
			$cabang_tujuan 	= $tujuan->Nama;

			/*
			 * Data detail diskon 
			 */
			if($jenis_diskon != ""){

				switch($jenis_diskon){

					case "1" :

						// Diskon mahasiswa
						$data_diskon	= $this->_reservasiModel->getDetailJenisDiskon($jenis_diskon);
						$res_member 	= $this->_reservasiModel->getReservasiMemberAll($member->IdMember, $getdata->TglBerangkat, $getdata->NoTiket);

						$potongan 		= $data_diskon->JumlahDiscount;
						$kode_diskon 	= $data_diskon->KodeDiscount;
						$nama_diskon 	= $data_diskon->NamaDiscount;
						$jenis_penumpang= $kode_diskon;

						if($potongan < 1){
							$potongan 	= $harga_tiket * $potongan;
						}

						if($cetak_ulang != '1'){

							// Update reservasi mahasiswa di hari yang sama
							if(sizeof($res_member) > 0){

								// die(var_dump($res_member));

								for ($i = 0; $i < sizeof($res_member); $i++) { 

									$res_harga 	= $res_member[$i]->HargaTiket; 
									// $res_harga 	= (int) $res_harga + (int) $potongan;
									
									$values		= array();

									$values[]	= "HargaTiket		= '$res_harga'";
									$values[]	= "SubTotal			= '$res_harga'";
									$values[]	= "Discount			= '0'";
									$values[]	= "Total			= '$res_harga'";
									$values[]	= "JenisDiscount	= ''";
									$values[]	= "JenisPenumpang 	= 'U'";

									$res = $this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values, array("NoTiket = '". $res_member[$i]->NoTiket ."'"));
		
								}

							}

						}

					break;

					case "pp" :
						
						// Promo PP
						$harino  		= date('N', strtotime($getdata->TglBerangkat));
						$hari  			= $cfg['hariIn'][$harino];

						$getpp 			= $this->_jadwalModel->getPromoPPByHari($getdata->TglBerangkat, $hari);
						$potongan 		= $getpp->JumlahDiscount;
							
						// $potongan 		= $potongan / 2;
						$kode_diskon 	= $getpp->KodePromo;
						$nama_diskon 	= $getpp->NamaPromo;
						$jenis_penumpang= "PP";

					break; 

					default :

						$data_diskon	= $this->_reservasiModel->getDetailJenisDiskon($jenis_diskon);

						$potongan 		= $data_diskon->JumlahDiscount;
						$kode_diskon 	= $data_diskon->KodeDiscount;
						$nama_diskon 	= $data_diskon->NamaDiscount;
						$jenis_penumpang= $kode_diskon;

						if($potongan < 1){
							$potongan 	= $harga_tiket * $potongan;
						}

					break;

				}

			}
			else{
				$potongan 		= 0;
				$kode_diskon 	= "";
				$nama_diskon 	= "";

				if($jenis_penumpang != 'T'){
					$jenis_penumpang= "U";
				}

				// kirim smsm member reguler
				if($member && $member->KategoriMember == '0'){

					$jenis_penumpang= "K";

				}
			}

			if($kode_voucher != ''){

				$voucher 		= $this->_reservasiModel->getVoucherDetail($kode_voucher);
				$tanggal 		= date('Y-m-d');
				$hari 			= date('N');

				if(!$voucher){
					$this->error("Kode voucher salah");
				}

				if(strtotime($tanggal) > strtotime($voucher->ExpiredDate)){
					$this->error("Voucher telah kadaluarsa");
				}

				if(($hari == '1' || $hari == '6' || $hari == '7') && $voucher->IsBolehWeekEnd == '0'){
					$this->error("Voucher hanya bisa digunakan weekday");
				}

				if($voucher->IdJurusan != NULL && $voucher->IdJurusan != $jadwal->IdJurusan){
					$this->error("Voucher tidak bisa digunakan di jurusan ini");
				}

				if($voucher->KodeJadwal !=  NULL && $voucher->KodeJadwal != $kode_jadwal){
					$this->error("Voucher tidak bisa digunakan di jadwal ini");
				}

				if($voucher->IsReturn == '1'){
					$this->error("Voucher hanya bisa digunakan untuk return jurusan ini");
				}


				$values 	= array();

				$values[]	= "IdJurusan		= '". $jurusan->IdJurusan ."'";
				$values[]	= "KodeJadwal		= '". $kode_jadwal ."'";
				$values[]	= "Cabangberangkat 	= '". $jurusan->KodeCabangAsal ."'";
				$values[]	= "CabangTujuan		= '". $jurusan->KodeCabangTujuan ."'";
				$values[]	= "WaktuDigunakan	= now()";
				$values[]	= "PetugasPengguna	= '$user'"; 

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_voucher', $values, array("KodeVoucher = '$kode'"));

				$potongan 	= $harga_tiket;
				$nama_diskon= $kode_voucher;

			}

			$potongan  			= $potongan > $harga_tiket ? $harga_tiket : $potongan;
			$pembayaran 		= $pembayaran == '' ? '0' : $pembayaran;
			$charge 			= 0;
			$PPN 				= 0;
			$subtotal 			= $harga_tiket;
			$total 				= $subtotal + $charge + $PPN - $potongan;

			$this->dbObj->beginTrans();

			/*
			 * Update data reservasi
			 */

			$values 		= array();

			if($cetak_ulang != '1'){

			$values[]		= "Nama					= '$nama'";
			$values[]		= "Telp					= '$no_telepon'";
			$values[]		= "HargaTiket			= '$harga_tiket'";
			$values[]		= "Charge				= '$charge'";
			$values[]		= "Subtotal				= '$subtotal'";
			$values[]		= "Discount				= '$potongan'";
			$values[]		= "PPN					= '$PPN'";
			$values[]		= "Total				= '$total'";
			$values[]		= "JenisPembayaran		= '$pembayaran'";
			$values[]		= "JenisDiscount		= '$nama_diskon'";

			}

			$values[]		= "CetakTiket			= '1'"; //cetak tiket
			$values[]		= "PetugasCetakTiket	= '$user'"; //cetak tiket
			$values[]		= "WaktuCetakTiket		= '$now'"; //cetak tiket

			/* Cetak menggunakan OTP */
			if($jenis_penumpang != "U"){
			$values[]		= "OTPUsed				= '1'";
			}

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values, array("NoTiket = '". $getdata->NoTiket ."'"));

			/*
			 * Update posisi detail
			 */
			$values			= array();

			$values[]		= "StatusBayar			= '1'";

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values, array("NoTiket = '". $getdata->NoTiket ."'"));

			$this->dbObj->commitTrans();

			/*
			 * Data jika cetak ulang
			 */
			if($cetak_ulang == '1'){
			
				$nama 			= $getdata->Nama;
				$no_telepon		= $getdata->Telp;
				$total 			= $getdata->Total;
				$jenis_diskon	= $getdata->JenisDiscount;
				$potongan 		= $getdata->Discount;
				$pembayaran		= $getdata->JenisPembayaran;
				$harga_tiket 	= $getdata->HargaTiket;
				$charge 		= $getdata->Charge;
				$PPN 			= $getdata->PPN;
				$subtotal 		= $getdata->Subtotal;
				$total 			= $getdata->Total;
			}

			/**
			 * Frekuensi member reguler
			 */
			$res_reguler  		= $this->_reservasiModel->getReservasiReguler($member->IdMember, $getdata->TglBerangkat);
			$jumcetak 			= $res_reguler->jumcetak;

			if($jumcetak < 2 AND $cetak_ulang != '1'){

				// $getfrekwensi 	= $this->_pelangganModel->getFrekwensiMember($member->IdMember, $getdata->TglBerangkat);
				$getfrekwensi 	= $this->_pelangganModel->getFrekwensiMember2($member->IdMember);

				if($getfrekwensi){

					$frekwensi 	= $getfrekwensi->FrekwensiBerangkat;
					$frekwensi 	= $frekwensi + 1;

					$values 	= array();

					$values[]		= "FrekwensiBerangkat	= '$frekwensi'"; 

					$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_member_frekwensi_berangkat', $values, array("IdMember = '". $member->IdMember ."'"));

				}
				else{

					$values 	= array();

					$values[]		= "IdMember				= '". $member->IdMember ."'"; 
					$values[]		= "TglBerangkat			= '". $getdata->TglBerangkat ."'"; 
					$values[]		= "FrekwensiBerangkat	= '1'"; 

					$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_member_frekwensi_berangkat', $values);
				}

			}

			/* Log cetak tiket */
			$getlog = $this->_reservasiModel->getLogCetakTiket($getdata->NoTiket);

			$values = array();

			$values = "NoTiket 			= '". $getdata->NoTiket ."'";
			$values = "NamaPenumpang 	= '$nama'";
			$values = "TelpPenumpang 	= '$no_telepon'";
			$values = "TglBerangkat 	= '". $getdata->TglBerangkat ."'";
			$values = "JamBerangkat 	= '". $getdata->JamBerangkat ."'";
			$values = "KodeJadwal 		= '". $getdata->KodeJadwal ."'";
			$values = "IdJurusan 		= '". $getdata->IdJurusan ."'";
			$values = "NomorKursi 		= '". $getdata->NomorKursi ."'";
			$values = "UserPencatak 	= '$user'";
			$values = "NamaUserPencetak = '$nama_petugas'";
			$values = "WaktuCetak 		= now()";

			// $values = "UserOtoritasi 	= ''";
			// $values = "NamaUserOtoritasi = ''";

			if($getlog){

				$cetakke= $getlog->CetakanKe + 1;

				$values = "CetakanKe 		= '$cetakke'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_log_cetak_tiket', $values, array("NoTiket = '". $getdata->NoTiket ."'"));
			}
			else{
				$values = "CetakanKe 		= '1'";

				$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_log_cetak_tiket', $values);
			}

			//jika langsung cetak tiket atau go show dan jika pembarayan menggunakan voucher
			if($cetak_ulang != '1' && $pembayaran == '3' && $kode_voucher != ''){

				$values			= array();

				$values[]		= "NoTiket					= '$no_tiket'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_voucher', $values, array("KodeVoucher = '$kode_voucher'"));

			}

			if($jenis_penumpang == 'M'){
				$keterangan 	= "Member mahasiswa";
			}

			if($jenis_penumpang == 'K'){
				$getfrekwensi 	= $this->_pelangganModel->getFrekwensiMember2($member->IdMember);
				$jumberangkat 	= $getfrekwensi->FrekwensiBerangkat % 10;

				if($getfrekwensi > 0 && $jumberangkat == 0){
					$jumberangkat = 10;
				}

				$keterangan 	= "Frekuensi keberangkatan ke ". $jumberangkat;
			}
			
			$status	= "OK";

			$result	= array(
						'kode_jadwal'	=> $getdata->KodeJadwal,
						'cabang_asal'	=> $cabang_asal,
						'cabang_tujuan'	=> $cabang_tujuan,
						'tgl_berangkat'	=> $getdata->TglBerangkat,
						'jam_berangkat' => $getdata->JamBerangkat,
						'kode_booking' 	=> $getdata->KodeBooking,
						'no_tiket' 		=> $getdata->NoTiket,
						'no_kursi' 		=> $getdata->NomorKursi,
						'nama' 			=> $nama,
						'no_telepon' 	=> $no_telepon,
						'harga_tiket' 	=> (int) $harga_tiket,
						'diskon'		=> (int) $potongan,
						'total_bayar' 	=> (int) $total,
						'petugas_cetak' => $nama_petugas,
						'waktu_cetak' 	=> dateNumFull($now),
						'kode_voucher ' => $kode_voucher,
						'keterangan' 	=> $keterangan
					);

		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function penumpang(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status 		= "ZERO_RESULTS";

		$no_tiket 		= $this->getParam('no_tiket');
		$getdata 		= $this->_reservasiModel->getDetailbyTiket($no_tiket);

		if($getdata){

			/*
			 * Data penjual
			 */
			$penjual			= $this->_userModel->getDetail($getdata->PetugasPenjual);
			$nama_penjual 		= $penjual->nama;

			/*
			 * Data cetak tiket
			 */
			$petugas_cetak 		= "";

			if($getdata->CetakTiket == '1'){

				$cetak 			= $this->_userModel->getDetail($getdata->PetugasCetakTiket);
				$petugas_cetak	= $cetak->nama;

			}

			$jenis_penumpang	= $getdata->JenisDiscount;
			$jenis_penumpang	= $jenis_penumpang == '' ? 'Umum' : $jenis_penumpang;

			/*
			 * Data Posisi Detail
			 */


			if(($getdata->JenisPembayaran != 'NULL' OR $getdata->JenisPembayaran != '') AND ($getdata->StatusBayar == 1)){

				
				$status_bayar 	= "SUDAH DIBAYAR";
				$waktu_bayar 	= $getdata->WaktuCetakTiket;


			}
			else{

				$status_bayar 	= "BELUM DIBAYAR";
				$waktu_bayar 	= "";

			}

			$status 			= "OK";
			$result 			= array(
									'kode_jadwal' 	=> $getdata->KodeJadwal,
									'kode_booking' 	=> $getdata->KodeBooking,
									'no_tiket' 		=> $getdata->NoTiket,
									'no_kursi' 		=> (int) $getdata->NomorKursi,
									'harga_tiket'	=> (int) $getdata->HargaTiket,
									'diskon'		=> (int) $getdata->Discount,
									'total'			=> (int) $getdata->Total,
									'otp_used' 		=> $getdata->OTPUsed,
									'penumpang' 	=> array(
															'nama' 			=> $getdata->Nama,
															'no_telepon' 	=> $getdata->Telp,
															'kode_jenis' 	=> $getdata->JenisDiscount,
															'jenis' 		=> $jenis_penumpang,
															'tipe' 			=> $getdata->JenisPenumpang,
															'id_member' 	=> $getdata->IdMember
													),
									'pemesanan' 	=> array(
															'status' 		=> $status_bayar,
															'waktu_pesan' 	=> dateNumFull($getdata->WaktuPesan),
															'cso_pemesan' 	=> $nama_penjual,
															'waktu_bayar' 	=> dateNumFull($waktu_bayar)
													),
									'cso_cetak' 	=> $petugas_cetak
								);
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function batal(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status 	= "ZERO_RESULTS";

		$no_tiket 	= $this->getParam('no_tiket');
		$user 		= $this->getUserId();
		$now 		= date('Y-m-d H:i:s');

		$getdata 	= $this->_reservasiModel->getDetailbyTiket($no_tiket);

		if($getdata == FALSE){
			$this->error("Pemesanan tidak tersedia");
		}

		try {

			$this->dbObj->beginTrans();

			/*
			 * Update data reservasi
			 */

			$values		= array();

			$values[]	= "FlagBatal			= '1'";
			$values[]	= "PetugasPembatalan	= '$user'";
			$values[]	= "WaktuPembatalan		= '$now'";

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values, array("NoTiket = '$no_tiket'"));

			/*
			 * Hapus posisi detail
			 */
			$this->dbObj->deleteRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', array("NoTiket = '$no_tiket'"));

			$this->dbObj->commitTrans();

			$status	= "OK";
			$result	= array(
							'pesan' => 'Berhasil membatalkan pesanan'
						);

		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Reservasi', $e->getMessage());

			$this->internalError();
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);

	}

	public function mutasi(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status 		= "ZERO_RESULTS";

		$kode_jadwal	= $this->getParam('kode_jadwal');
		$no_tiket		= $this->getParam('no_tiket');
		$no_kursi		= $this->getParam('no_kursi');
		$tgl_berangkat 	= $this->getParam('tgl_berangkat');
		$user 			= $this->getUserId();
		$pembayaran 	= "";
		$now 			= date('Y-m-d H:i:s');

		$getdata 		= $this->_reservasiModel->getDetailbyTiket($no_tiket);

		if($getdata == FALSE){
			$this->error("Nomor tiket invalid");
		}

		try{

			/*
			 * Data lama
			 */
			$kode_jadwal_L 		= $getdata->KodeJadwal;
			$no_kursi_L 		= $getdata->NomorKursi;
			$id_jurusan_L 		= $getdata->id_jurusan;
			$tgl_berangkat_L 	= $getdata->TglBerangkat;
			$jenis_diskon 		= $getdata->JenisDiscount;

			$flag_kursi_L 		= $this->_reservasiModel->getFlagKursi($kode_jadwal_L, $no_kursi_L, $tgl_berangkat_L);
			$kode_booking 		= $flag_kursi_L[0]->KodeBooking;
			$status_bayar 		= $flag_kursi_L[0]->StatusBayar;
			$nama 				= $flag_kursi_L[0]->Nama; 				
			// $no_tiket 			= $posisi_detail_L->NoTiket;

			/* Update posisi detail lama */
			$this->dbObj->beginTrans();

			$values 	= array();

			$values[]	= "StatusKursi		= '0'"; 
			$values[]	= "NoTiket			= NULL"; 
			$values[]	= "StatusBayar		= '0'";
			$values[]	= "Nama				= NULL";
			$values[]	= "KodeBooking		= NULL";

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values, array("NoTiket = '$no_tiket'"));

			/*
			 * Data jadwal baru
			 */
			$jadwal  			= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);

			if($jadwal == FALSE){
				$this->error("Tidak ada keberangkatan");
			}

			$kode_jadwal 		= $jadwal->FlagSubJadwal == '1' ? $jadwal->KodeJadwalUtama : $jadwal->KodeJadwal;
			$id_jurusan 		= $jadwal->IdJurusan;
			$layout_kursi 		= $jadwal->PenJumlahKursi == '0' ? $jadwal->JumlahKursi : $jadwal->PenJumlahKursi;
			$jumlah_kursi 		= $jadwal->PenJumlahKursi == '0' ? $jadwal->JumlahKursi : $jadwal->PenJumlahKursi;
			$jam_berangkat 		= $jadwal->JamBerangkat .":00";
			$cabang_asal 		= $jadwal->KodeCabangAsal;
			$cabang_tujuan 		= $jadwal->KodeCabangTujuan;
			$harga_tiket 		= $jadwal->HargaTiket;

			/*
	 		 * Cek Posisi
	 		 * Jika belum ada, tambah baru
			 */

			$posisi  			= $this->_reservasiModel->getPosisi($kode_jadwal, $tgl_berangkat);
			
			if($posisi == FALSE){

				$kode_kendaraan = "";
				$kode_supir 	= "";

				$tambahposisi 	= $this->_reservasiModel->tambahPosisi($kode_jadwal, $tgl_berangkat, $jam_berangkat, $jumlah_kursi, $kode_kendaraan, $kode_supir);

			}

			/*
			 * Data detail diskon 
			 */
			/*
			if($jenis_diskon != ""){

				$data_diskon	= $this->_reservasiModel->getDetailJenisDiskonByNama($jenis_diskon);

				$potongan 		= $data_diskon->JumlahDiscount;
				$kode_diskon 	= $data_diskon->KodeDiscount;
				$nama_diskon 	= $data_diskon->NamaDiscount;
				$jenis_penumpang= $kode_diskon;

				if($potongan < 1){
					$potongan 	= $harga_tiket * $potongan;
				}
			}
			else{
				$potongan 		= 0;
				$kode_diskon 	= "";
				$nama_diskon 	= "";
				$jenis_penumpang= "U";
			}

			$potongan  			= $potongan > $harga_tiket ? $harga_tiket : $potongan;
			$pembayaran 		= $pembayaran == '' ? '0' : $pembayaran;
			$charge 			= 0;
			$PPN 				= 0;
			$subtotal 			= $harga_tiket;
			$total 				= $subtotal + $charge + $PPN - $potongan;
			*/

			/* Update data reservasi */
			$this->dbObj->beginTrans();

			$values 	= array();

			$values[]	= "KodeJadwal		= '$kode_jadwal'"; 
			$values[]	= "IdJurusan		= '$id_jurusan'"; 
			$values[]	= "JamBerangkat		= '$jam_berangkat'"; 
			$values[]	= "TglBerangkat		= '$tgl_berangkat'"; 
			/*
			$values[]	= "HargaTiket		= '$harga_tiket'";
			$values[]	= "Charge			= '$charge'";
			$values[]	= "Subtotal			= '$subtotal'";
			$values[]	= "Discount			= '$potongan'";
			$values[]	= "PPN				= '$PPN'";
			$values[]	= "Total			= '$total'";
			$values[]	= "JenisPembayaran	= '$pembayaran'";
			$values[]	= "JenisDiscount	= '$nama_diskon'";
			*/
			$values[]	= "NomorKursi		= '$no_kursi'"; 
			$values[]	= "WaktuMutasi		= '$now'"; 
			$values[]	= "Pemutasi			= '$user'";
			$values[]	= "MutasiDari		= '$kode_jadwal_L'"; 

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_reservasi', $values, array("NoTiket = '$no_tiket'"));

			/*
			 * Update posisi detail
			 */
			$flag_kursi	= $this->_reservasiModel->getFlagKursi($kode_jadwal, $no_kursi, $tgl_berangkat);

			if($flag_kursi){

				$values		= array();

				$values[]	= "StatusKursi	= '1'";
				$values[]	= "Session 		= '$user'";
				$values[]	= "SessionTime 	= '$now'";
				$values[]	= "NoTiket 		= '$no_tiket'";
				$values[]	= "Nama 		= '$nama '";
				$values[]	= "StatusBayar	= '$status_bayar'";
				$values[]	= "KodeBooking	= '$kode_booking'";
				$values[]	= "JamBerangkat 	= '$jam_berangkat'";
				$values[]	= "KodeJadwalUtama 	= '$kode_jadwal'";
				$values[]	= "KodeCabangAsal 	= '$cabang_asal'";
				$values[]	= "KodeCabangTujuan	= '$cabang_tujuan'";

				$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values, array("NomorKursi = '$no_kursi' AND KodeJadwal = '$kode_jadwal' AND TglBerangkat = '$tgl_berangkat'"));

			}
			else{

				$values		= array();

				$values[]	= "NomorKursi 	= '$no_kursi'";
				$values[]	= "KodeJadwal	= '$kode_jadwal'";
				$values[]	= "TglBerangkat = '$tgl_berangkat'";
				$values[]	= "JamBerangkat 	= '$jam_berangkat'";
				$values[]	= "KodeJadwalUtama 	= '$kode_jadwal'";
				$values[]	= "KodeCabangAsal 	= '$cabang_asal'";
				$values[]	= "KodeCabangTujuan	= '$cabang_tujuan'";
				$values[]	= "StatusKursi 	= '1'";
				$values[]	= "Session 		= '$user'";
				$values[]	= "SessionTime 	= '$now'";
				$values[]	= "NoTiket 		= '$no_tiket'";
				$values[]	= "Nama 		= '$nama '";
				$values[]	= "StatusBayar	= '$status_bayar'";
				$values[]	= "KodeBooking	= '$kode_booking'";

				$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_posisi_detail', $values);

			}

			$this->dbObj->commitTrans();
			
			$status	= "OK";
			$msg 	= "Berhasil mutasi";

			$result	= array(
						'pesan'	=> $msg
					);

		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Reservasi', $e->getMessage());

			$this->internalError('Internal Error'  . $e->getMessage());
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function jenis_pembayaran(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$payment  	= $cfg['payment'];

		if(sizeof($payment) > 0){

			$status	= "OK";

			for ($i = 0; $i < sizeof($payment); $i++) {

				$result[]	= array(
								'kode'	=> $i,
								'jenis'	=> $payment[$i]
							);
			}
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function cek_voucher(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$kode  			= $this->getParam('kode_voucher');
		$kode_jadwal 	= $this->getParam('kode_jadwal');
		$tgl_berangkat 	= $this->getParam('tgl_berangkat');

		$tanggal 		= date('Y-m-d');
		$hari 			= date('N');
		$user 			= $this->getUserId();

		$voucher 		= $this->_reservasiModel->getVoucherDetail($kode);
		$jadwal 		= $this->_jadwalModel->getDetail($kode_jadwal, $tgl_berangkat);
		$jurusan 		= $this->_jurusanModel->getDetail($jadwal->IdJurusan);

		if(!$jadwal){
			$this->error("Jadwal tidak tersedia");
		}

		if(!$voucher){
			$this->error("Kode voucher salah");
		}

		if(strtotime($tanggal) > strtotime($voucher->ExpiredDate)){
			$this->error("Voucher telah kadaluarsa");
		}

		if(($hari == '1' || $hari == '6' || $hari == '7') && $voucher->IsBolehWeekEnd == '0'){
			$this->error("Voucher hanya bisa digunakan weekday");
		}

		if($voucher->IdJurusan != NULL && $voucher->IdJurusan != $jadwal->IdJurusan){
			$this->error("Voucher tidak bisa digunakan di jurusan ini");
		}

		if($voucher->KodeJadwal !=  NULL && $voucher->KodeJadwal != $kode_jadwal){
			$this->error("Voucher tidak bisa digunakan di jadwal ini");
		}

		if($voucher->IsReturn == '1'){
			$this->error("Voucher hanya bisa digunakan untuk return jurusan ini");
		}

		// try {

			// $values 	= array();

			// $values[]	= "IdJurusan		= '". $jurusan->IdJurusan ."'";
			// $values[]	= "KodeJadwal		= '". $kode_jadwal ."'";
			// $values[]	= "Cabangberangkat 	= '". $jurusan->KodeCabangAsal ."'";
			// $values[]	= "CabangTujuan		= '". $jurusan->KodeCabangTujuan ."'";
			// $values[]	= "WaktuDigunakan	= now()";
			// $values[]	= "PetugasPengguna	= '$user'"; 

			// $this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_voucher', $values, array("KodeVoucher = '$kode'"));

			$status	= "OK";
			$result = array(
						'pesan'	=> "Voucher berhasil digunakan"
					);
			
		// } catch (DbException $e) {

		// 	$this->dbObj->rollbackTrans();

		// 	Error::store('Reservasi', $e->getMessage());

		// 	$this->internalError('Internal Error'  . $e->getMessage());
		// }

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function kirim_otp(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$no_tiket 	= $this->getParam('no_tiket');
		$getdata 	= $this->_reservasiModel->getDetailbyTiket($no_tiket);

		if(!$getdata){
			$this->error("Reservasi tidak tesedia");
		}

		try {

			$kirim 	= $this->_sendSMS($getdata->Nama, $getdata->Telp, $getdata->OTP, $getdata->NoTiket, $getdata->KodeBooking, '1');

			$status	= "OK";
			$result = array(
						'pesan'	=> "Otp telah dikirim ulang"
					);
			
		} catch (Exception $e) {
			
			Error::store('Reservasi', $e->getMessage());

			$this->internalError('Internal Error'  . $e->getMessage());

		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);

	}

	private function _generateOTP(){

		$digit 	= 6 ;
    	$otp 	= rand(pow(10, $digit-1), pow(10, $digit)-1);

    	return $otp;

	}

	private function _sendSMS($nama, $telp, $otp, $no_tiket, $kode_booking, $jum_pesan, $flag =  NULL){

		global $cfg;

		$userKey 	= 'o23tc4';
    	$passKey 	= 'orionaja';
    	$pesan   	= 'BARAYA : Terima Kasih Tn/Ny '. $nama .', Kode OTP : '. $otp .' , Kode Boooking : '. $kode_booking .'. Gunakan utk mendapatkan tiket.';
    	$url 		= 'http://reguler.sms-notifikasi.com/apps/smsapi.php';
    	$parameter 	= 'userkey='. $userKey .'&passkey='. $passKey .'&nohp='. $telp .'&pesan='. $pesan;

    	$length 	= strlen($parameter);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("application/x-www-form-urlencoded", "Content-length: $length"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);

		//Log Sms
		try{

			$values = array();

			$values[] 	= "NoTujuan 			= '$telp'";
			$values[] 	= "NamaPenerima 		= '$nama'";
			$values[] 	= "WaktuKirim 			= '". date('Y-m-d H:i:s') ."'";
			$values[] 	= "JumlahPesan 			= '$jum_pesan'";
			$values[] 	= "FlagTipePengiriman 	= '$flag'";
			$values[] 	= "KodeReferensi 		= '$kode_booking'";
			$values[] 	= "Pesan 				= '$pesan'";
			$values[] 	= "Responses 			= '$response'";
			$values[] 	= "OTP 					= '$otp'";

			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_log_sms', $values);

		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Reservasi', $e->getMessage());

			$this->internalError('Internal Error'  . $e->getMessage());
		}

		return $response;

	}

	private function _sendSMSPP($nama, $telp, $otp, $no_tiket, $kode_booking, $jum_pesan, $tanggal, $jurusan, $jam, $flag =  NULL){

		global $cfg;

		$userKey 	= 'o23tc4';
    	$passKey 	= 'orionaja';
    	$pesan   	= 'BARAYA : Terima Kasih Tn/Ny '. $nama .', Kode OTP : '. $otp .', Tiket pulang '. $tanggal .' '. $jurusan .' '. $jam;
    	$url 		= 'http://reguler.sms-notifikasi.com/apps/smsapi.php';
    	$parameter 	= 'userkey='. $userKey .'&passkey='. $passKey .'&nohp='. $telp .'&pesan='. $pesan;

    	$length 	= strlen($parameter);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("application/x-www-form-urlencoded", "Content-length: $length"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);

		//Log Sms
		try{

			$values = array();

			$values[] 	= "NoTujuan 			= '$telp'";
			$values[] 	= "NamaPenerima 		= '$nama'";
			$values[] 	= "WaktuKirim 			= '". date('Y-m-d H:i:s') ."'";
			$values[] 	= "JumlahPesan 			= '$jum_pesan'";
			$values[] 	= "FlagTipePengiriman 	= '$flag'";
			$values[] 	= "KodeReferensi 		= '$kode_booking'";
			$values[] 	= "Pesan 				= '$pesan'";
			$values[] 	= "Responses 			= '$response'";
			$values[] 	= "OTP 					= '$otp'";

			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_log_sms', $values);

		} catch (DbException $e) {

			$this->dbObj->rollbackTrans();

			Error::store('Reservasi', $e->getMessage());

			$this->internalError('Internal Error'  . $e->getMessage());
		}

		return $response;

	}

}
?>