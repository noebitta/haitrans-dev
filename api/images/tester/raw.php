<?php
include 'class.RequestSignature.php';

$clientID		= $_POST['client_id'];
$clientSecret	= $_POST['client_secret'];
$accessToken	= $_POST['access_token'];
$accessTokenSecret	= $_POST['access_token_secret'];
$requestType	= $_POST['request_type'];
$requestURI		= $_POST['request_uri'];
$requestQuery	= $_POST['request_query'];
$buildQuery		= $_POST['build_query'];

$reqSignature	= new RequestSignature();
$params 		= array();

if (!empty($requestQuery)) {
	$query  = explode('&', $requestQuery);
	if (is_array($query)) {
		for ($i = 0; $i < sizeof($query); $i++) {
			$namevalue = explode('=', $query[$i]);

			$params[$namevalue[0]] = $namevalue[1];
		}
	}
}

$params['auth_nonce']	  = $reqSignature->createNonce(true);
$params['auth_timestamp'] = time();
$params['auth_client_id'] = $clientID;

if (!empty($accessToken)) {
	$params['auth_access_token'] = $accessToken;
	$key 	= $accessToken;
	$secret = $accessTokenSecret;
} else {
	$key 	= $clientID;
	$secret = $clientSecret;
}

if ($buildQuery) {
	$baseSignature = $reqSignature->createSignatureBase($requestType, $requestURI, $params);
	$signature     = $reqSignature->createSignature($baseSignature, $key, $secret);

	if ($requestType == 'GET') {
		$url   = $requestURI . '?' . $reqSignature->normalizeParams($params) . '&auth_signature=' . $signature;
		$curl  = "curl --get '$url' --verbose";
	} else {
		$curl  = "curl -d '" . $reqSignature->normalizeParams($params) . '&auth_signature=' . $signature . "' $requestURI --verbose";
	}
}

$clientID 		= (empty($clientID)) ? 'TEST' : $clientID;
$clientSecret 	= (empty($clientSecret)) ? 'b014dcc2a3a984ebaea090dd96e6d0fc' : $clientSecret;
$requestType 	= (empty($requestType)) ? 'GET' : $requestType;
$requestURI		= (empty($requestURI)) ? 'http://api.3trust.com/tiketux/v1/' : $requestURI;
$requestQuery	= (empty($requestQuery)) ? '' : $requestQuery;

 ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="en">
<head>
<meta name="keywords" content="aviation, trip report, aviation trip, airlines, airport, aircraft"/>
<meta name="description" content="Log and share your aviation trip" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>.: TIKETUX :.</title>
<link type="text/css" media="screen" rel="stylesheet" href="main.css" />
<link type="text/css" media="screen" rel="stylesheet" href="form.css"/>
</head>
<body >
<div id="content"><strong>TIKETUX</strong> <br/><br/>
	<div id="FormContainer" style="width:800px">
		<form action="index.php" name="role" id="role"method="post" enctype="MULTIPART/FORM-DATA">
			<input type="hidden" name="submitted" value="1">
			<?php
			if ($buildQuery) {
				?>
				<fieldset >
					<legend >API Request</legend><br/>
						<label>Signature Base String</label> <br />
						<textarea name="" class="text" cols="100" rows="3"><?=$baseSignature?></textarea>

						<label>Signature</label><br />
						<input type="text" class="text"  value="<?=$signature?>" size="50"/> <br />

						<?php
						if ($requestType == 'GET') {
						?>
						<label>URL</label> <br />
						<textarea name="" class="text" cols="100" rows="3"><?=$url?></textarea> <br/>
						<?php
						} else {
							?>
							<label>Post Data</label> <br />
							<textarea name="" class="text" cols="100" rows="3"><?=$reqSignature->normalizeParams($params) . '&auth_signature=' . $signature?></textarea> <br/>

							<?
						}
						?>

						<label>Curl</label> <br />
						<textarea name="" class="text" cols="100" rows="3"><?=$curl?></textarea> <br/>

					</fieldset>

				<fieldset >
					<legend >Builld Request</legend><br/>
			<?php
			}
			?>
			<label>Client ID</label><br />
			<input type="text" class="text" name="client_id" id="client_id" value="<?=$clientID?>" size="50"/> <br />

			<label>Client Secret</label><br />
			<input type="text" class="text" name="client_secret" id="client_secret" value="<?=$clientSecret?>" size="50"/> <br />

			<label>Access Token</label><br />
			<input type="text" class="text" name="access_token" id="access_token" value="<?=$accessToken?>" size="50"/> <br />

			<label>Access Token Secret</label><br />
			<input type="text" class="text" name="access_token_secret" id="access_token_secret" value="<?=$accessTokenSecret?>" size="50"/> <br />

			 <label>Request Type</label><br />
			 <select name="request_type" id="request_type" class="select">
			   <option value="GET" <?php if ($requestType == 'GET') echo 'selected'; ?>>GET</option>
			   <option value="POST" <?php if ($requestType == 'POST') echo 'selected'; ?>>POST</option>
			</select>
			<br/>

			<label>Request URI</label><br />
			<input type="text" class="text" name="request_uri" id="request_uri" value="<?=$requestURI?>" size="90"/> <br />

			<label>Request Query</label><br />
			<input type="text" class="text" name="request_query" id="request_query" value="<?=$requestQuery?>" size="90"/> <br />

			<br />
			<input type="submit" class="button" name="build_query" id="build_query" value=" Build query "/>
			<?php
			if ($buildRequest) {
				echo "</fieldset>";
			}
			?>
		</form>
	</div>
</div>
</body>
</html>