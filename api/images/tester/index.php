<?php
//API Console.
//Lorensius W. L. T <lorenz@londatiga.net>

include 'class.RequestSignature.php';
include 'endpoint.php';

$clientID		= $_POST['client_id'];
$clientSecret	= $_POST['client_secret'];
$accessToken	= $_POST['access_token'];
$accessTokenSecret	= $_POST['access_token_secret'];
$requestType	= $_POST['request_type'];
$requestURI		= $_POST['request_uri'];
$requestQuery	= $_POST['request_query'];
$baseUrl        = $_POST['api_url'];
$endPoint 		= $_POST['end_point'];
$responseFormat = $_POST['response_format'];
$buildQuery		= $_POST['build_query'];
$reqParam		= $_POST['req_param'];
$reqValue		= $_POST['req_value'];

$reqSignature	= new RequestSignature();
$params 		= array();

if (is_array($reqParam)) {
	for ($i = 0; $i < sizeof($reqParam); $i++) {
		$params[$reqParam[$i]] = $reqValue[$i];
	}
}

$params['auth_nonce']	  = $reqSignature->createNonce(true);
$params['auth_timestamp'] = time();
$params['auth_client_id'] = $clientID;

if (!empty($accessToken)) {
	$params['auth_access_token'] = $accessToken;
	$key 	= $accessToken;
	$secret = $accessTokenSecret;
} else {
	$key 	= $clientID;
	$secret = $clientSecret;
}

if ($buildQuery) {
	$baseSignature = $reqSignature->createSignatureBase($requestType, $requestURI, $params);
	$signature     = $reqSignature->createSignature($baseSignature, $key, $secret);

	if ($requestType == 'GET') {
		$url   = $requestURI . '?' . $reqSignature->normalizeParams($params) . '&auth_signature=' . $signature;
		$curl  = "curl --get '$url' --verbose";
	} else {
		$curl  = "curl -d '" . $reqSignature->normalizeParams($params) . '&auth_signature=' . $signature . "' $requestURI --verbose";
	}
}

$clientID 		= (empty($clientID)) ? 'TESTER' : $clientID;
$clientSecret 	= (empty($clientSecret)) ? '6e962e3ca23ee44d83f38496dfa82c1b05182f035' : $clientSecret;
$requestType 	= (empty($requestType)) ? 'GET' : $requestType;
$requestURI		= (empty($requestURI)) ? $baseUrl : $requestURI;
$requestQuery	= (empty($requestQuery)) ? '' : $requestQuery;
$baseUrl		= (empty($baseUrl)) ? 'https://api.tiketux.com/v1' : $baseUrl;

$strEndpoints	= '';
$jsEndpoints	= "var endpoints = new Array();\n";

foreach ($endpoints as $k => $v) {
	$strEndpoints .= "<option value='$k' " . (($endPoint == $k) ? 'selected' : '') .">$v[method] $k</option>";

	$jsEndpoints .= "endpoints['$k'] = new Array();\n";
	$jsEndpoints .= "endpoints['$k']['method'] = '$v[method]';\n";
	$jsEndpoints .= "endpoints['$k']['auth']   = $v[auth];\n";
	$jsEndpoints .= "endpoints['$k']['params'] = new Array();\n";

	for ($i = 0; $i < sizeof($v['params']); $i++) {
		$jsEndpoints .= "endpoints['$k']['params'][$i] = '" . $v['params'][$i] . "';\n";
	}

	$jsEndpoints .= "\n";
}

$reqParamStr = '';

if (is_array($reqParam)) {
	for ($i = 0; $i < sizeof($reqParam); $i++) {
		$reqParamStr .= '<br /><input type="text" class="text" name="req_param[]" id="req_param[]" value="' . $reqParam[$i]
					 . '" size="20"/> = <input type="text" class="text" name="req_value[]" id="req_value[]" '
					 . 'value="' . $reqValue[$i]. '" size="30"/>';
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="en">
<head>
<meta name="keywords" content=""/>
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>.: TIKETUX API CONSOLE :.</title>
<link type="text/css" media="screen" rel="stylesheet" href="main.css" />
<link type="text/css" media="screen" rel="stylesheet" href="form.css"/>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
<?=$jsEndpoints?>

$(document).ready(function() {
    $('#new_req').click(function() {
        $("#request_query_span").append('<br /><input type="text" class="text" name="req_param[]" id="req_param[]" value="" size="20"/> = <input type="text" class="text" name="req_value[]" id="req_value[]" value="" size="30"/>');
    });

    $('#end_point').change(function() {
		if ($('#end_point').find(':selected').index() == 0) { //reset
			$("#request_query_span").html('');
			$("#request_type").val('GET').attr('selected',true);
			$("#request_uri").val($("#api_url").val());
		}

        var val = $('#end_point').find(':selected').val();

		$("#request_type").val(endpoints[val]['method']).attr('selected',true);

		var params 		= endpoints[val]['params'];
		var strParams 	= '';

		$("#request_query_span").html('');

		for (i = 0; i < params.length; i++) {
			strParams += '<br /><input type="text" class="text" name="req_param[]" id="req_param[]" value="' + params[i]
					  + '" size="20"/> = <input type="text" class="text" name="req_value[]" id="req_value[]" '
					  + 'value="" size="30"/>';
		}

		$("#request_query_span").append(strParams);

		var uri = $("#api_url").val() + val + '.' + $('#response_format').find(':selected').val();

		$("#request_uri").val(uri);

		var accessToken 	  = $("#access_token").val();
		var accessTokenSecret = $("#access_token_secret").val();

		$("#ac_warn").html('');
		$("#acs_warn").html('');

		if (endpoints[val]['auth'] == 2) {
			if (accessToken == '' || accessTokenSecret == '') {
				$("#ac_warn").html('*** required for auth type 2 ***');
				$("#acs_warn").html('*** required for auth type 2 ***');

				alert('This API Endpoint requires authentication type 2, please fill the Access Token and Access Token Secret');
			}
		}
    });

	$('#response_format').change(function() {
		var reqUri  = $("#request_uri").val();
		var arr		= reqUri.split(".");

		if (arr.length > 0) {
			arr[arr.length-1] = $('#response_format').find(':selected').val();
			reqUri = arr.join('.');

			$("#request_uri").val(reqUri);
		}
	});
})
</script>
</head>
<body >
<div id="content"><strong>TIKETUX API CONSOLE - KELIMUTU FRAMEWORK</strong> <br/><br/>
	<div id="FormContainer" style="width:800px">
		<form action="index.php" name="role" id="role"method="post" enctype="MULTIPART/FORM-DATA">
			<input type="hidden" name="submitted" value="1">
			<?php
			if ($buildQuery) {
				?>
				<fieldset >
					<legend >API Request</legend><br/>
						<label>Signature Base String</label> <br />
						<textarea name="" class="text" cols="100" rows="3"><?=$baseSignature?></textarea>

						<label>Signature</label><br />
						<input type="text" class="text"  value="<?=$signature?>" size="50"/> <br />

						<?php
						if ($requestType == 'GET') {
						?>
						<label>URL</label> <br />
						<textarea name="" class="text" cols="100" rows="3"><?=$url?></textarea> <br/>
						<?php
						} else {
							?>
							<label>Post Data</label> <br />
							<textarea name="" class="text" cols="100" rows="3"><?=$reqSignature->normalizeParams($params) . '&auth_signature=' . $signature?></textarea> <br/>

							<?
						}
						?>

						<label>Curl</label> <br />
						<textarea name="" class="text" cols="100" rows="3"><?=$curl?></textarea> <br/>

					</fieldset>

				<fieldset >
					<legend >Builld Request</legend><br/>
			<?php
			}
			?>
			<fieldset><legend>Authentication</legend>
			<label>Client ID</label><br />
			<input type="text" class="text" name="client_id" id="client_id" value="<?=$clientID?>" size="50"/> <br />

			<label>Client Secret</label><br />
			<input type="text" class="text" name="client_secret" id="client_secret" value="<?=$clientSecret?>" size="50"/> <br />

			<label>Access Token</label><br />
			<input type="text" class="text" name="access_token" id="access_token" value="<?=$accessToken?>" size="50"/>
			<span class="red" id="ac_warn"></span><br />

			<label>Access Token Secret</label><br />
			<input type="text" class="text" name="access_token_secret" id="access_token_secret" value="<?=$accessTokenSecret?>" size="50"/>
			<span class="red" id="acs_warn"></span><br />
			</fieldset>

			<fieldset><legend>Request</legend>
			<label>Base URL</label><br />
			<input type="text" class="text" name="api_url" id="api_url" value="<?=$baseUrl?>" size="50"/> <br />

			<label>End Point</label><br/>
			<select name="end_point" id="end_point" class="select">
			   <option value=''>--- choose ---</option>
			   <?=$strEndpoints?>
			</select>
			<br/>

			<label>Request Type</label><br />
			<select name="request_type" id="request_type" class="select">
			   <option value="GET" <?php if ($requestType == 'GET') echo 'selected'; ?>>GET</option>
			   <option value="POST" <?php if ($requestType == 'POST') echo 'selected'; ?>>POST</option>
			</select>
			<br/>

			<label>Response Format</label><br />
			<select name="response_format" id="response_format" class="select">
			   <option value="json" <?php if ($responseFormat == 'json') echo 'selected'; ?>>JSON</option>
			   <option value="xml" <?php if ($responseFormat == 'xml') echo 'selected'; ?>>XML</option>
			</select>
			<br/>

			<label>Request URI</label><br />
			<input type="text" class="text" name="request_uri" id="request_uri" value="<?=$requestURI?>" size="90"/> <br />

			<label>Request Query</label><br />
			<a href='#' id="new_req">Add New</a>
			<span id="request_query_span"><?=$reqParamStr?></span>

			</fieldset>
			<br />
			<input type="submit" class="button" name="build_query" id="build_query" value=" Build query "/>
			<br/><br/>Please read the <a href='Tiketux_API_1.0.0.pdf'>API Documentation</a> for more detail | <a href='raw.php'>Raw Version</a>
			<?php
			if ($buildRequest) {
				echo "</fieldset>";
			}
			?>
		</form>
	</div>

	<div id="footer">Created by <a href='http://www.londatiga.net'>Lorensius W. L. T</a> / lorenz@londatiga.net</div>
</div>
</body>
</html>