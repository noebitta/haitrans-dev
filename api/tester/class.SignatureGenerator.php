<?php
class SignatureGenerator
{
	public function genKeyId($authkey)
	{
		return strtoupper(bin2hex($this->str2bin($authkey)));
	}

	public function genSignature($merchant, $kodeBooking, $waktuPesan, $jumlahBayar, $telp, $keyId)
	{
		$tempKey 	= $merchant . $kodeBooking . $waktuPesan . $jumlahBayar . $telp . $keyId;
		$hashKey 	= $this->getHash($tempKey);

		$signature 	= abs($hashKey);

		return $signature;
	}

	private function hex2bin($data)
	{
		$len = strlen($data);
		return pack("H" . $len, $data);
	}

	private function str2bin($data)
	{
		$len = strlen($data);
		return pack("a" . $len, $data);
	}

	private function intval32bits($value)
    {
        if ($value > 2147483647)
            $value = ($value - 4294967296);
		else if ($value < -2147483648)
            $value = ($value + 4294967296);

        return $value;
    }

	private function getHash($value)
	{
		$h = 0;
		for ($i = 0;$i < strlen($value);$i++)
		{
			$h = $this->intval32bits($this->add31T($h) + ord($value{$i}));
		}
		return $h;
	}

	private function add31T($value)
	{
		$result = 0;
		for($i=1;$i <= 31;$i++)
		{
			$result = $this->intval32bits($result + $value);
		}

		return $result;
	}
}
?>