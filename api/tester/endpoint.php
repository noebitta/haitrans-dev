<?php
$endpoints		= array('/user/login' 				=> array(
														'method'	=> 'POST',
														'auth'		=> 1,
														'params' 	=> array('username', 'password')
													),
						'/user/logout' 				=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array()
													),
						'/cabang' 					=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array()
													),
						'/jurusan' 					=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('asal')
													),
						'/jadwal' 					=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('id_jurusan', 'tgl_berangkat', 'pulang', 'tgl_berangkat_pergi', 'kode_jadwal_pergi')
													),
						'/kursi' 					=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('kode_jadwal', 'tgl_berangkat', 'pulang')
													),
						'/kursi/flag' 				=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('kode_jadwal', 'tgl_berangkat', 'no_kursi', 'flag')
													),
						'/pelanggan/cek' 			=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_telepon')
													),
						'/reservasi/jenis_diskon' 	=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('id_jurusan', 'jenis_penumpang', 'id_member', 'tgl_berangkat')
													),
						'/reservasi/jenis_pembayaran'=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array()
													),
						'/reservasi/booking' 		=> array(
														'method'	=> 'POST',
														'auth'		=> 2,
														'params' 	=> array(
																			'id_jurusan', 'kode_jadwal', 'tgl_berangkat',
																			'nama', 'no_telepon', 'id_member', 'keterangan',
																			'jenis_diskon', 'jenis_pemabayaran', 'cetak_tiket',
																			'id_jurusan_pulang', 'kode_jadwal_pulang', 'tgl_berangkat_pulang', 'kode_voucher'
																		)
													),
						'/reservasi/cetak_tiket' 	=> array(
														'method'	=> 'POST',
														'auth'		=> 2,
														'params' 	=> array('no_tiket', 'otp', 'cetak_ulang', 'nama', 'no_telepon', 'jenis_diskon', 
																			'jenis_penumpang', 'jenis_pembayaran', 'kode_voucher', 'username', 'password'
																		)
													),
						'/reservasi/mutasi' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_tiket', 'kode_jadwal', 'tgl_berangkat', 'no_kursi')
													),
						'/reservasi/penumpang' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_tiket')
													),
						'/reservasi/cek_voucher' 	=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('kode_voucher', 'kode_jadwal', 'tgl_berangkat')
													),
						'/reservasi/batal' 			=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_tiket')
													),
						'/reservasi/kirim_otp'		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_tiket')
													),
						'/spj/supir' 				=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array()
													),
						'/spj/kendaraan' 			=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array()
													),
						'/spj/cek' 					=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('kode_jadwal', 'tgl_berangkat')
													),
						'/spj/cetak' 				=> array(
														'method'	=> 'POST',
														'auth'		=> 2,
														'params' 	=> array('id_jurusan', 'kode_jadwal', 'tgl_berangkat', 'kode_supir', 'kode_kendaraan', 'otp', 'no_spj', 'cetak_ulang')
													),
						'/spj/request_otp' 			=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('kode_jadwal', 'tgl_berangkat')
													),
						'/laporan/penjualan' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('tgl_awal', 'tgl_akhir')
													),
						'/laporan/keuangan' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('tgl_awal', 'tgl_akhir')
													),
						'/laporan/setoran' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('tgl_awal', 'tgl_akhir')
													),
						'/laporan/rekap' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array()
													),
						'/laporan/rekap_detail' => array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_resi')
													),
						'/paket' 				=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('kode_jadwal', 'tgl_berangkat')
													),
						'/paket/jenis' 			=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array()
													),
						'/paket/harga' 			=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('id_jurusan', 'kode_jenis', 'koli', 'volume', 'berat', 'panjang', 'lebar', 'tinggi', 'ukuran')
													),
						'/paket/reservasi' 		=> array(
														'method'	=> 'POST',
														'auth'		=> 2,
														'params' 	=> array('kode_jadwal', 'id_jurusan', 'tgl_berangkat', 
																			'kode_jenis', 'koli', 'volume', 'berat', 'panjang', 'lebar', 'tinggi', 'ukuran', 'jenis_layanan', 'keterangan',
																			'nama_pengirim', 'telp_pengirim', 'alamat_pengirim', 'nama_penerima', 'telp_penerima', 'alamat_penerima', 'jenis_pembayaran'
																		)
													),
						'/paket/detail' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_resi')
													),
						'/paket/cetak_resi' 	=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_resi')
													),
						'/paket/batal' 			=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('no_resi')
													),
						'/paket/manifest' 		=> array(
														'method'	=> 'POST',
														'auth'		=> 2,
														'params' 	=> array('kode_jadwal', 'tgl_berangkat', 'no_spj')
													),
						'/paket/list_checkin' 	=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('kota', 'asal', 'tujuan', 'tgl_awal', 'tgl_akhir')
													),
						'/paket/checkin' 		=> array(
														'method'	=> 'POST',
														'auth'		=> 2,
														'params' 	=> array('no_resi')
													),
						'/paket/laporan' 		=> array(
														'method'	=> 'GET',
														'auth'		=> 2,
														'params' 	=> array('tgl_awal', 'tgl_akhir')
													),
                        );

?>