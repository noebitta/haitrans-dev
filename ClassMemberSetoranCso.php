<?php
class SetoranTopUpCSO{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1,$TABEL2,$TABEL3;
	
	//CONSTRUCTOR
	function SetoranTopUpCSO(){
		$this->ID_FILE="C-004";
		$this->TABEL1="tbl_member_posting_setoran_cso";
		$this->TABEL2="tbl_member_topup";
		$this->TABEL3="tbl_user";
	
	}
	
	//BODY
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:002
		Desc	:Mengembalikan data  sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":' ORDER BY waktu_terakhir_transaksi asc';
		
		$sql = 
			"SELECT     
				CONVERT(CHAR(17),MAX(tmt.waktu_topup),113) AS waktu_terakhir_transaksi, tc.username, tc.nama, SUM(tmt.jumlah_topup) AS jumlah_harus_setor
			FROM tbl_user tc INNER JOIN	
				tbl_member_topup tmt ON tc.username = tmt.operator
			WHERE (tmt.posting IS NULL) 
				AND (tmt.kode_posting IS NULL)
				AND (username='$pencari' OR nama LIKE '$pencari%')
			GROUP BY tc.username, tc.nama
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 002");
		}
		
	}//  END ambilData
	
	function ambilDataDetailTopup($operator){
		
		/*
		ID	:003
		Desc	:Mengembalikan data topup by operator
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				id_topup,id_member,CONVERT(CHAR(17),waktu_topup,113) as waktu_topup,jumlah_topup
			FROM $this->TABEL2
			WHERE operator='$operator' 
				AND posting IS NULL
				AND kode_posting IS NULL;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 003");
		}
		
	}//  END ambilDataDetailTopup
	
	function setorUangTopUp($total_harus_setor,$total_real_setor,$operator){
		
		/*
		ID	: 004
		IS	: data belum ada dalam database
		FS	: data tersimpan
		*/
		
		//kamus
		global $userdata;
		global $db;
		
		$kode_posting	= "POSTU".dateYMD().rand(1000,9999);
		$useraktif	= $userdata['username'];
		
		//MENGUPDATE KODE POSTING DAN STATUS POSTING
		$sql =
			"UPDATE $this->TABEL2
			SET posting=1,kode_posting='$kode_posting'
			WHERE operator='$operator' 
				AND posting IS NULL
				AND kode_posting IS NULL;";
				
		//MENYMPAN TRANSAKSI SETORAN
		
		$sql .=
			"INSERT INTO $this->TABEL1
				(kode_posting,total_harus_setor,total_real_setor,cso,
				operator,waktu_setor)
			VALUES(
				'$kode_posting',$total_harus_setor,$total_real_setor,'$operator',
				'$useraktif',{fn NOW()});";
		
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 004");
		}
		
		return $kode_posting;
	}
	
	function ambilDataPosting($kode_posting){
		
		/*
		ID	:005
		Desc	:Mengembalikan data posting setoran
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				kode_posting,total_harus_setor,total_real_setor,
				cso,operator,CONVERT(CHAR(17),waktu_setor,113) as waktu_setor
			FROM $this->TABEL1
			WHERE kode_posting='$kode_posting';";
				
		if ($result = $db->sql_query($sql)){
			$data	= $db->sql_fetchrow($result);
			return $data;
		} 
		else{
			die_error("Gagal $this->ID_FILE 005");
		}
		
	}//  END ambilDataPosting
	
}
?>