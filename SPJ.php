<?php
//
// SPJ
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassPromo.php');
include($adp_root_path . 'ClassPengaturanUmum.php');	


// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination


//PROCESS==================================================================

$tgl_berangkat		= $HTTP_GET_VARS['tglberangkat'];
$kode_jadwal			= $HTTP_GET_VARS['kodejadwal'];
$mode							= $HTTP_GET_VARS['mode']!=""?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$no_spj	          = $HTTP_GET_VARS['nospj'];
$sopir_dipilih    = $HTTP_GET_VARS['sopirdipilih'];
$mobil_dipilih    = $HTTP_GET_VARS['mobildipilih'];
$debug						= $HTTP_GET_VARS['debug'];

switch($mode){
	case "0":
		//PLOT DIALOG SPJ
		
		$Jadwal						= new Jadwal();
		$BiayaOperasional	= new BiayaOperasional();
		$Reservasi				= new Reservasi();
		
		#mengambil data jadwal
		$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
		
		$is_sub_jadwal = false;
		
		if($data_jadwal['FlagSubJadwal']!=1){
			//Jadwal Utama
			
			if($no_spj==""){
				$data_biaya_op= $BiayaOperasional->ambilBiayaOpByJurusan($data_jadwal["IdJurusan"]);
				$biaya_sopir	= $data_biaya_op['BiayaSopir'];
				$biaya_tol		= $data_biaya_op['BiayaTol'];
				$biaya_parkir	= $data_biaya_op['BiayaParkir'];
				$total_biaya	= $data_biaya_op['BiayaSopir']+$data_biaya_op['BiayaTol']+$data_biaya_op['BiayaParkir'];
				$event_hitung_bbm	= "setBiayaBBM();";
			}
			else{
				$data_biaya_op= $BiayaOperasional->ambilBiayaByNoSPJ($no_spj);
				$biaya_sopir	= $data_biaya_op[$FLAG_BIAYA_SOPIR];
				$biaya_tol		= $data_biaya_op[$FLAG_BIAYA_TOL];
				$biaya_parkir	= $data_biaya_op[$FLAG_BIAYA_PARKIR];
				$biaya_bbm		= $data_biaya_op[$FLAG_BIAYA_BBM];
				$total_biaya	= $data_biaya_op[$FLAG_BIAYA_SOPIR]+$data_biaya_op[$FLAG_BIAYA_TOL]+$data_biaya_op[$FLAG_BIAYA_PARKIR]+$data_biaya_op[$FLAG_BIAYA_BBM];
        $event_hitung_bbm	= "";
			}
			
			$kode_jadwal_utama	= $kode_jadwal;
			$disabled_pilih	= "";
			$cetak_disabled="";
		}
		else{
			//Sub Jadwal
			$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];
			
			$data_spj	= $Reservasi->ambilDetailSPJ($tgl_berangkat,$kode_jadwal_utama);
			
			$cetak_disabled	= $data_spj['NoSPJ']==""?"disabled":"";
			
			$is_sub_jadwal	= true;
			$disabled_pilih	= "disabled";
			$biaya_sopir	= 0;
			$biaya_tol		= 0;
			$biaya_parkir	= 0;
			$biaya_bbm		= 0;
			$total_biaya	= 0;
			$event_list_mobil="";
		}
		
		//set template
		$template->assign_vars (
		array(
      'MOBIL_DIPILIH'     => $mobil_dipilih,
      'SOPIR_DIPILIH'     => $sopir_dipilih,
			'BIAYA_SOPIR'				=> $biaya_sopir,
			'BIAYA_SOPIR_SHOW'	=> number_format($biaya_sopir,0,",","."),
			'BIAYA_TOL'					=> $biaya_tol,
			'BIAYA_TOL_SHOW'		=> number_format($biaya_tol,0,",","."),
			'BIAYA_PARKIR'			=> $biaya_parkir,
			'BIAYA_PARKIR_SHOW'	=> number_format($biaya_parkir,0,",","."),
			'BIAYA_BBM'					=> $biaya_bbm,
			'BIAYA_BBM_SHOW'		=> number_format($biaya_bbm,0,",","."),
			'TOTAL_BIAYA'				=> number_format($total_biaya,0,",","."),
      'EVENT_HITUNG_BBM'  => $event_hitung_bbm,
			'CETAK_DISABLED'		=> $cetak_disabled,
			)
		);
		
		$template->set_filenames(array('body' => 'SPJ.dialog.tpl')); 
		$template->pparse('body');
		
		exit;		
	case "1":
		//CETAK SPJ
		
		$template->set_filenames(array('body' => 'SPJ_body.tpl'));
		
		//tombol OK di klik untuk mencetak SPJ
		$Reservasi	= new Reservasi();
		$Jadwal			= new Jadwal();
		$Sopir			= new Sopir();
		$Mobil			= new Mobil();
		$Promo			= new Promo();
		$BiayaOperasional	= new BiayaOperasional();
		$PengaturanUmum	= new PengaturanUmum();

		$username			= $HTTP_GET_VARS['username'];
		$signature		= $HTTP_GET_VARS['signature'];
		$is_cetakulang_voucher_bbm= $HTTP_GET_VARS['iscetakulangvoucher'];
		
		$nourut = rand(1000,9999);
		$useraktif=$userdata['user_id'];
		
		if ($tgl_berangkat=='' || $kode_jadwal==''){
			exit;
		}
		
		#mengambil data jadwal
		$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
		
		$is_sub_jadwal	= $data_jadwal['FlagSubJadwal'];
		
		if($is_sub_jadwal!=1){
			$kode_jadwal_utama			= $kode_jadwal;
		}
		else{
			$kode_jadwal_utama			= $data_jadwal['KodeJadwalUtama'];
		}
		
		$jam_berangkat	= $data_jadwal['JamBerangkat'];
		$list_kode_jadwal		= $Jadwal->ambilListKodeJadwalByKodeJadwalUtama($kode_jadwal_utama);
		
		//mengambil layout kursi
		
		$layout_kursi = $Reservasi->ambilLayoutKursiByKodeJadwal($kode_jadwal_utama);
		
		$row	= $Sopir->ambilDataDetail($sopir_dipilih);
		$nama_sopir = $row['Nama'];
		
		$row				= $Mobil->ambilDataDetail($mobil_dipilih);
		$no_polisi 	= $row['NoPolisi'];
		$id_layout	= $row['IdLayout'];
		
		
		//mendirect pencetakan spj sesuai dengan layout kursi
		
		//mengambil data tiket
		
		$row	= $Reservasi->ambilDetailSPJ($tgl_berangkat,$kode_jadwal_utama);
		$no_polisi		= ($row['NoPolisi']=="")?$no_polisi:$row['NoPolisi'];
		
		$list_field_diupdate="";
		
		//Mengambil jumlah total penumpang dan omzet
		
		$data_total	=$Reservasi->hitungTotalOmzetdanJumlahPenumpangSPJ($tgl_berangkat,$kode_jadwal,$list_kode_jadwal);

		$total_omzet			=($data_total['TotalOmzet']!='')?$data_total['TotalOmzet']:0;
		$jumlah_penumpang	=($data_total['JumlahPenumpang']!='')?$data_total['JumlahPenumpang']:0;	
		
		//Mengambil jumlah total paket dan omzet
		$data_total_paket	=$Reservasi->hitungTotalOmzetdanJumlahPaketSPJ($tgl_berangkat,$list_kode_jadwal);
		
		$total_omzet_paket=($data_total_paket['TotalOmzet']!='')?$data_total_paket['TotalOmzet']:0;
		$jumlah_paket			=($data_total_paket['JumlahPaket']!='')?$data_total_paket['JumlahPaket']:0;	
		
		//Mengambil pembiayaan 
		$data_biaya_op		= $BiayaOperasional->ambilBiayaOpByJurusan($data_jadwal["IdJurusan"]);
		
		//mengupdate field utk SPJ di tbl posisi
		$no_spj	= $row['NoSPJ'];
		
		$total_biaya	= 0;
		
		if($no_spj==""){
			//UPDATE 20 JANUARI 2014 BY BARTON
			//setiap jadwal pick up (sub jadwal) dibuatkan SPJ nya tersendiri
			
			/*$no_spj= "MNF".substr($kode_jadwal_utama,0,3).dateYMD().$nourut; 
			
			$Reservasi->tambahSPJ(
				$no_spj, $kode_jadwal_utama, $tgl_berangkat, 
				$jam_berangkat, $layout_kursi, $jumlah_penumpang, 
				$mobil_dipilih, $useraktif, $sopir_dipilih,
				$nama_sopir,$total_omzet,
				$jumlah_paket,$total_omzet_paket);*/
			//dinonaktifkan karena setiap jadwal pickup juga dibuatkan spj nya sendiri
			
			//PENGELOLAAN VOUCHER BBM
			//note: voucher bbm hanya dicetak setiap 1 PP, jika manifest sebelumnya sudah cetak voucher bbm,
			//maka manifest ini tidak akan mencetak voucher BBM
			
			$no_spj= "MNF".substr($kode_jadwal,0,3).dateYMD().$nourut;
			//echo("SUB:$kode_jadwal VS ".substr($kode_jadwal,0,3));
			//memeriksa apakah manifest sebelumnya sudah cetak voucher BBM
			/*if(!$BiayaOperasional->isSudahCetakVoucherBBM($mobil_dipilih) && $data_biaya['IsVoucherBBM']==1){
				$template ->assign_block_vars('VOUCHER_BBM',array());
				$template->assign_vars(
					array(
						'BIAYA_BBM'   => number_format($data_biaya['BiayaBBM'],0,",",".")
					)
				);
				
				$is_cetak_voucher_bbm	= 1;
			}
			elseif($BiayaOperasional->isSudahCetakVoucherBBM($mobil_dipilih) && $data_biaya['IsVoucherBBM']==1 && $is_cetakulang_voucher_bbm==1){
					
				//CEK VERIFIKASI APPROVER
				include($adp_root_path . 'ClassUser.php');
				$User	= new User();
		
				$data_user	= $User->ambilDataDetailByUsername($username);
				
				$signature_balas = md5($username."#".$data_user['user_password']."#".$config['key_token']);
				
				if($signature==$signature_balas && $data_user['user_level']<=$USER_LEVEL_INDEX["SPV_OPERASIONAL"]){
					$BiayaOperasional->tambahLogCetakUlangVoucher(
					$no_spj, $kode_jadwal, $tgl_berangkat,
					$jam_berangkat, $mobil_dipilih, $sopir_dipilih,
					$nama_sopir,$data_biaya['BiayaBBM'],$useraktif,
					$userdata['nama'],$data_user['user_id'],$data_user['nama']);
					
					$template ->assign_block_vars('VOUCHER_BBM',array());
					$template->assign_vars(
						array(
							'BIAYA_BBM'   => number_format($data_biaya['BiayaBBM'],0,",",".")
						)
					);
					
					$is_cetak_voucher_bbm	= 1;
				}
			}*/
			
			//jika spj belum pernah dicetak, maka akan menambahkan biaya ke database
			
			//biaya sopir
			if(!$is_sub_jadwal){
				//jika bukan sub jadwal, biaya akan diposting
				
				if($data_biaya_op['BiayaSopir']>0){
					$BiayaOperasional->tambah(
						$no_spj,$data_biaya_op['KodeAkunBiayaSopir'],$FLAG_BIAYA_SOPIR,
						$mobil_dipilih,$sopir_dipilih,$data_biaya_op['BiayaSopir'],
						$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
					
					$total_biaya += $data_biaya_op['BiayaSopir'];
				}
				
				//biaya tol
				if($data_biaya_op['BiayaTol']>0){
					$BiayaOperasional->tambah(
						$no_spj,$data_biaya_op['KodeAkunBiayaTol'],$FLAG_BIAYA_TOL,
						$mobil_dipilih,$sopir_dipilih,$data_biaya_op['BiayaTol'],
						$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
					
					$total_biaya += $data_biaya_op['BiayaTol'];
				}
				
				//biaya parkir
				if($data_biaya_op['BiayaParkir']>0){
					$BiayaOperasional->tambah(
						$no_spj,$data_biaya_op['KodeAkunBiayaParkir'],$FLAG_BIAYA_PARKIR,
						$mobil_dipilih,$sopir_dipilih,$data_biaya_op['BiayaParkir'],
						$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
					
					$total_biaya += $data_biaya_op['BiayaParkir'];
				}
				
				//biaya bbm
				if(!$BiayaOperasional->isSudahCetakVoucherBBM($mobil_dipilih)){
					//mengambil harga bbm
					$harga_bbm_array		= $PengaturanUmum->ambilParameter("BBM_HARGA_PERLITER");
					$harga_bbm_perliter	= $harga_bbm_array["NilaiParameter"];	
					$bbm_decode					= $Mobil->layoutBodyDecode($data_biaya_op["LiterBBM"]);
						
					$biaya_bbm		= $bbm_decode["$id_layout"]*$harga_bbm_perliter;
					if($biaya_bbm>0){
						$total_biaya 			+= $biaya_bbm;
						
						if($data_biaya_op['IsVoucherBBM']==1){
							$temp_flag_biaya 	= $FLAG_BIAYA_VOUCHER_BBM;
						}
						else{
							$temp_flag_biaya 	= $FLAG_BIAYA_BBM;
							
						}
						
						$BiayaOperasional->tambah(
							$no_spj,$data_biaya_op['KodeAkunBiayaBBM'],$temp_flag_biaya,
							$mobil_dipilih,$sopir_dipilih,$biaya_bbm,
							$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
						
						$is_cetak_voucher_bbm	= 1;
					}
				}

			}
			
			$Reservasi->tambahSPJ(
				$no_spj, $kode_jadwal, $tgl_berangkat, 
				$jam_berangkat, $layout_kursi, $jumlah_penumpang, 
				$mobil_dipilih, $useraktif, $sopir_dipilih,
				$nama_sopir,$total_omzet,$jumlah_paket,
				$total_omzet_paket,$is_sub_jadwal, $is_cetak_voucher_bbm);
			
			$duplikat	= "";
			$jumlah_kali_cetak=1;
			
		}
		else{
			$jumlah_kali_cetak	= $Reservasi->ubahSPJ(
				$row['NoSPJ'], $jumlah_penumpang, 
				$mobil_dipilih, $useraktif, $sopir_dipilih,
				$nama_sopir,$total_omzet,
				$jumlah_paket,$total_omzet_paket,$is_sub_jadwal);
				
			
			$no_spj	= $row['NoSPJ'];
			
			/*if($BiayaOperasional->isSudahCetakVoucherBBM($mobil_dipilih) && $data_biaya['IsVoucherBBM']==1 && $is_cetakulang_voucher_bbm==1){
					
				//CEK VERIFIKASI APPROVER
				include($adp_root_path . 'ClassUser.php');
				$User	= new User();
				
				$data_user	= $User->ambilDataDetailByUsername($username);
				
				$signature_balas = md5($username."#".$data_user['user_password']."#".$config['key_token']);
				
				if($signature==$signature_balas && $data_user['user_level']<=$USER_LEVEL_INDEX["SPV_OPERASIONAL"]){
					$BiayaOperasional->tambahLogCetakUlangVoucher(
					$no_spj, $kode_jadwal, $tgl_berangkat,
					$jam_berangkat, $mobil_dipilih, $sopir_dipilih,
					$nama_sopir,$data_biaya['BiayaBBM'],$useraktif,
					$userdata['nama'],$data_user['user_id'],$data_user['nama']);
					
					$template ->assign_block_vars('VOUCHER_BBM',array());
					$template->assign_vars(
						array(
							'BIAYA_BBM'   => number_format($data_biaya['BiayaBBM'],0,",",".")
						)
					);
					
					$is_cetak_voucher_bbm	= 1;
				}
			}*/
			
			$duplikat	= "<br>**** COPY $jumlah_kali_cetak ****";
			
			$total_biaya	= !$is_sub_jadwal?$BiayaOperasional->ambilTotalBiayaByNoSPJTunai($no_spj):0;
		}
		
		//CATAT LOG MANIFEST
			$Reservasi->tambahLogCetakManifest(
				$no_spj,$kode_jadwal,$tgl_berangkat,
				$jam_berangkat,$layout_kursi,$jumlah_penumpang,
				$jumlah_paket,$mobil_dipilih,$sopir_dipilih,
				$nama_sopir,$total_omzet,$total_omzet_paket,
				$is_sub_jadwal,$userdata['user_id'],$userdata['nama']);
			
		//update data pada tbl posisi
		$Reservasi->ubahPosisiCetakSPJ(
			$kode_jadwal_utama, $tgl_berangkat,$list_field_diupdate, 
			$sopir_dipilih,$mobil_dipilih,$no_spj,$useraktif);
			
		//update tblReservasi
		$Reservasi->ubahDataReservasiCetakSPJ(
			$list_kode_jadwal, $tgl_berangkat,$sopir_dipilih,
			$mobil_dipilih,$no_spj);
			
		//update tblspj untuk insentif sopir
		/*$Reservasi->updateInsentifSopir(
			$no_spj,$layout_kursi, $INSENTIF_SOPIR_LAYOUT_MAKSIMUM, 
			$INSENTIF_SOPIR_JUMLAH_PNP_MINIMUM, $data_biaya['KomisiPenumpangSopir']);*/
		
		
		/*if($jumlah_paket>0){
			$result_paket = $Reservasi->ambilDataPaketUntukSPJ($tgl_berangkat,$list_kode_jadwal);
			$idx_no=0;
			while($row_paket=$db->sql_fetchrow($result_paket)){
				$idx_no++;
				
				$template ->assign_block_vars(
					'ROW_PAKET',
					array(
						'IDX_PAKET_NO'=>"(".$idx_no.")",
						'NO_TIKET_PAKET'=>$row_paket['NoTiket'],
						'TUJUAN'=>$row_paket['Tujuan'],
						'NAMA_PENGIRIM'=>$row_paket['NamaPengirim'],
						'TELP_PENGIRIM'=>$row_paket['TelpPengirim'],
						'NAMA_PENERIMA'=>$row_paket['NamaPenerima'],
						'TELP_PENERIMA'=>$row_paket['TelpPenerima']
						)
				);
			}
		}
		else{
			$tidak_ada_paket	= '<br>TIDAK ADA PAKET<br><br>';
		}
		*/
		//list penumpang
		$result_penumpang = $Reservasi->ambilDataPenumpangUntukSPJ($tgl_berangkat,$kode_jadwal,$list_kode_jadwal);
		
		$total_omzet_online	= 0;
		
		$total_jenis_penumpang	= array();
		$total_omzet_penumpang	= array();
		
		if($jumlah_penumpang>0){
			while($row_penumpang=$db->sql_fetchrow($result_penumpang)){
				
				if($row_penumpang['JenisPembayaran']!=3){
					//PEMBAYARAN MENGGUNAKAN BUKAN VOUCHER
					
					$nama_index		= $row_penumpang['JenisPenumpang'];
					$nilai_omzet	= $row_penumpang['Total'];	
					
					switch($row_penumpang['JenisPenumpang']){
						case "T":
							$nama_index		= "O";
							$nilai_omzet	= $row_penumpang['Total']-10000;
						break;
						
						/*case "R":
							$nama_index		= "U";
							$nilai_omzet	= $row_penumpang['HargaTiket'];
							$total_omzet_penumpang["r"]	+= $row_penumpang['HargaTiket']-$row_penumpang['Discount'];
						break;*/
					
					}
					
				}
				else{
					//PEMBAYARAN MENGGUNAKAN VOUCHER
					
					//Mengambil Nilai Voucher
					$nama_index		= "VR";
					$nilai_omzet	= $row_penumpang['Total'] - $Promo->getNilaiVoucherByNoTiketPulang($row_penumpang['NoTiket']);
				}
				
				$total_jenis_penumpang[$nama_index]++;
				$total_omzet_penumpang[$nama_index]	+= $nilai_omzet;
				
				if($row_penumpang['PetugasCetakTiket']!=0){
					$is_online	= "";
				}
				else{
					$is_online	= "<br/>**VIA ONLINE**";
					$total_omzet_online	+= $row_penumpang['Total'];
				}
				
				$template ->assign_block_vars(
					'ROW_PENUMPANG',
					array(
						'NOMOR_KURSI' => substr("0".$row_penumpang['NomorKursi'],-2),
						'NAMA'        => "(".$nama_index.") ".$row_penumpang['Nama'],
						'NO_TIKET'    => $row_penumpang['NoTiket'].$is_online,
						'RUTE'        => $row_penumpang['Asal']."->".$row_penumpang['Tujuan'],
						'KETERANGAN'  => ($row_penumpang['Alamat']!=""?$row_penumpang['Alamat']."<br>":""),
						)
				);
				
			}
			
		}
		else{
			$tidak_ada_penumpang	= '<br>TIDAK ADA PENUMPANG<br>';
		}
		//DEBUG
		//exit;
		
		$list_total_by_jenis_penumpang = "";
		
		foreach($total_jenis_penumpang as $index=>$value){
			$list_total_by_jenis_penumpang .= $index."=".$value."|";
		}
		
		$total_omzet_show	= 0;
		$total_pendapatan_tunai	= 0;
		
		foreach($total_omzet_penumpang as $index=>$value){
			$total_omzet_show	+= $value;
			
			if($index=="O" || $index=="VR"){
				$value	= -$value;
				$list_total_omzet_penumpang .= $index."=".substr("***************Rp.".number_format($value,0,",","."),-18)."<br>";
				$total_pendapatan_tunai	+= $value;
			}
		}
		
		$total_pendapatan_tunai	+= $total_omzet_show;
		
		$list_total_by_jenis_penumpang	= substr($list_total_by_jenis_penumpang,0,-1);
		
		$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
		
		//UPDATE SIGNATURE LAYOUT
		//$Reservasi->updateLayoutSignature($tgl_berangkat,$kode_jadwal_utama);
		
		$template->assign_vars(array(
			 'DUPLIKAT'    				=>$duplikat,
			 'NAMA_PERUSAHAAN'    =>$data_perusahaan['NamaPerusahaan'],
			 'ALAMAT_PERUSAHAAN'  =>$data_perusahaan['AlamatPerusahaan'],
			 'TELP_PERUSAHAAN' 		=>$data_perusahaan['TelpPerusahaan'],
			 'TRANSIT' 						=>($data_jadwal["FlagSubJadwal"]?"TRANSIT dari $kode_jadwal_utama<br>":""),
			 'NO_SPJ' 						=>$no_spj,
			 'TGL_BERANGKAT' 			=>dateparse(FormatMySQLDateToTgl($tgl_berangkat)),
			 'JURUSAN' 						=>$kode_jadwal." ".$jam_berangkat,
			 'TGL_CETAK' 					=>FormatMySQLDateToTglWithTime(dateNow(true)),
			 'NO_POLISI' 					=>$mobil_dipilih,
			 'SOPIR' 							=>$nama_sopir." (".$sopir_dipilih.")",
			 'TIDAK_ADA_PAKET' 		=>$tidak_ada_paket,
			 'JUMLAH_PAKET' 			=>$jumlah_paket,
			 //'OMZET_PAKET' 				=>number_format($total_omzet_paket,0,",","."),
			 'TIDAK_ADA_PENUMPANG'=>$tidak_ada_penumpang,
			 'JUMLAH_PENUMPANG' 	=>$jumlah_penumpang,
			 //'OMZET_PENUMPANG' 		=>substr("***************Rp.".number_format($total_omzet,0,",","."),-12),
			 //'OMZET_ONLINE'				=>substr("***************Rp.".number_format($total_omzet_online,0,",","."),-14),
			 //'TOTAL_OMZET' 				=>substr("***************Rp.".number_format($total_omzet-$total_omzet_online,0,",","."),-15),
			 'JENIS_PENUMPANG'		=>$list_total_by_jenis_penumpang,
			 //'LIST_OMZET_PENUMPANG'=>$list_total_omzet_penumpang,
			 'PENDAPATAN_TUNAI'		=>substr("***************Rp.".number_format($total_omzet,0,",","."),-14),
			 'CSO' 								=>$userdata['nama'],
			 'TOTAL_BIAYA'				=>substr("***************Rp.".number_format($total_biaya,0,",","."),-14),
			 'EMAIL_PERUSAHAAN' 	=>$data_perusahaan['EmailPerusahaan'],
			 'WEBSITE_PERUSAHAAN' =>$data_perusahaan['WebSitePerusahaan'],
			 'SID'								=>$userdata['session_id']
			 )
		);
		
		$template->pparse('body');
		exit;
		
	case "2":
		//SET HARGA BBM
		$BiayaOperasional	= new BiayaOperasional();
		$PengaturanUmum		= new PengaturanUmum();
		$Mobil						= new Mobil();
		
		$mobil_dipilih= $HTTP_GET_VARS['mobildipilih'];
		$no_spj				= $HTTP_GET_VARS['nospj'];
		$id_jurusan		= $HTTP_GET_VARS['idjurusan'];
		
		if($no_spj!=""){
			exit;
		}
		
		//data mobil
		$data_mobil	= $Mobil->ambilDataDetail($mobil_dipilih);
		
		//mengambil harga bbm
		$harga_bbm_array		= $PengaturanUmum->ambilParameter("BBM_HARGA_PERLITER");
		$harga_bbm_perliter	= $harga_bbm_array["NilaiParameter"];	
			
		$data_biaya_op		= $BiayaOperasional->ambilBiayaOpByJurusan($id_jurusan);
		
		$bbm_decode		= $Mobil->layoutBodyDecode($data_biaya_op["LiterBBM"]);
			
		$biaya_bbm		= $bbm_decode["$data_mobil[IdLayout]"]*$harga_bbm_perliter;
		
		$return_string="";
		
		if($no_spj==""){
			$return_string = !$BiayaOperasional->isSudahCetakVoucherBBM($mobil_dipilih)?
				"document.getElementById('biayabbm').value=$biaya_bbm;document.getElementById('showbiayabbm').innerHTML=\"Rp.".number_format($biaya_bbm,0,",",".")."\";hitungTotalBiayaOperasional();":
				"document.getElementById('biayabbm').value=0;document.getElementById('showbiayabbm').innerHTML=\"<font color='red'>SUDAH DIBERIKAN</font>\";hitungTotalBiayaOperasional();";
		}

		echo($return_string);
		exit;
	
	case "3":
		//VERIFIKASI USERNAME & PASSWORD Untuk cetak ulang SPJ
		include($adp_root_path . 'ClassUser.php');
		
		$User	= new User();
		
		$username			= $HTTP_POST_VARS['username'];
		$password			= $HTTP_POST_VARS['password'];
		
		$data_user	= $User->ambilDataDetailByUsername($username);
					
		$valid= $data_user['user_password']==md5($password) && $data_user['user_level']<=$USER_LEVEL_INDEX["SPV_OPERASIONAL"]?true:false;
		
		$return_string	= "document.getElementById('password_bbm').value='';";
		
		if($valid){
			
			$signature	= md5($username."#".$data_user['user_password']."#".$config['key_token']);
			
			$return_string	.=
				"dialog_voucherbbm.hide();
				document.getElementById('signature_bbm').value='$signature';
				CetakSPJ();";
		}
		else{
			$return_string	.=
				"document.getElementById('alasan_bbm').value='';
				alert('Anda tidak memiliki akses untuk melakukan proses ini!');";
		}
		
		echo($return_string);
		exit;
}
?>