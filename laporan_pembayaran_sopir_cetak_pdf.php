<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kota_dipilih 	= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$kota_dipilih	= ($kota_dipilih!='')?$kota_dipilih:"JAKARTA";
		
$kondisi_cari	=($cari=="")?
	" WHERE tms.KodeSopir LIKE '%' ":
	" WHERE (tms.KodeSopir LIKE '$cari%' OR tms.Nama LIKE '%$cari%')";
	

	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tms.KodeSopir":$sort_by;

$sum_total_jalan	= 0;
$sum_total_biaya	= 0;
$sum_total_kasbon	= 0;
$sum_total_terima	= 0;

//QUERY
$sql	=
	"SELECT tms.KodeSopir,tms.Nama,TotalJalan,vbs.Jumlah,vks.Jumlah AS JumlahKasbon,(IS_NULL(vbs.Jumlah,0)-IS_NULL(vks.Jumlah,0)) AS JumlahTerima
	FROM (tbl_md_sopir tms LEFT JOIN view_biaya_sopir vbs ON tms.KodeSopir=vbs.KodeSopir) 
		LEFT JOIN view_kasbon_sopir vks ON tms.KodeSopir=vks.KodeSopir
	$kondisi_cari
	ORDER BY $sort_by $order";
	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,40);
$pdf->Ln(15);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Biaya Sopir untuk Kota '.$kota_dipilih,'',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(100,5,'Nama','B',0,'C',1);
$pdf->Cell(20,5,'NRP','B',0,'C',1);
$pdf->Cell(30,5,'Ttl.Rit','B',0,'C',1);
$pdf->Cell(30,5,'Ttl.Biaya','B',0,'C',1);
$pdf->Cell(30,5,'Ttl.Kasbon','B',0,'C',1);
$pdf->Cell(30,5,'Ttl.Terima','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$pdf->Cell(5,5,$i,'',0,'C');
		$pdf->MultiCell2(100,5,$row['Nama'],'','L');
		$pdf->Cell(20,5,$row['KodeSopir'].'  ','',0,'R');
		$pdf->Cell(30,5,number_format($row['TotalJalan'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['Jumlah'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['JumlahKasbon'],0,",","."),'',0,'R');
		$pdf->Cell(30,5,number_format($row['JumlahTerima'],0,",","."),'',0,'R');
		$pdf->Ln(0);
		$pdf->Cell(245,1,'','B',0,'');
		$pdf->Ln();
		$i++;
		
		$sum_total_jalan	+=$row['TotalJalan'];
		$sum_total_biaya	+=$row['Jumlah'];
		$sum_total_kasbon	+=$row['JumlahKasbon'];
		$sum_total_terima	+=$row['JumlahTerima'];
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
} 

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(120,5,'Total ','B',0,'R',1);
$pdf->Cell(30,5,number_format($sum_total_jalan,0,",","."),'B',0,'R',1);
$pdf->Cell(30,5,number_format($sum_total_biaya,0,",","."),'B',0,'R',1);
$pdf->Cell(30,5,number_format($sum_total_kasbon,0,",","."),'B',0,'R',1);
$pdf->Cell(30,5,number_format($sum_total_terima,0,",","."),'B',0,'R',1);
										
$pdf->Output();
						
?>