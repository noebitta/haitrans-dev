<?php
//
// RESERVASI
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){  
  if(!isset($HTTP_GET_VARS['mode'])){
		redirect(append_sid('index.'.$phpEx),true); 
	}
	else{
		echo("Silahkan Login Ulang!");exit;
	}
}

include($adp_root_path . 'ClassPaketCargo.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Cargo	= new Cargo();

$tgl_berangkat			= $HTTP_GET_VARS['tglberangkat'];
$poin_pickedup			= $HTTP_GET_VARS['poinpickedup'];
$nama_poinpickedup	= $HTTP_GET_VARS['namapoinpickedup'];
$kurir							= $HTTP_GET_VARS['kurir'];
$namakurir					= $HTTP_GET_VARS['namakurir'];

$result_cargo				=  $Cargo->ambilListCargo($tgl_berangkat,$poin_pickedup);

$i=0;
while($row=$db->sql_fetchrow($result_cargo)){
	$i++;
	
	$template->assign_block_vars("DATA_CARGO",array(
		"no"							=> $i,
		"awb"							=> $row["NoAWB"],
		"pengirim"				=> $row["NamaPengirim"],
		"penerima"				=> $row["NamaPenerima"],
		"kota_asal"				=> $row["KodeAsal"],
		"kota_tujuan"			=> $row["KodeTujuan"],
		"koli"						=> $row["Koli"],
		"perhitungan"			=> ($row["IsVolumetric"]==0?"BERAT":"VOLUMETRIC"),
		"berat"						=> $row["Berat"],
		"deskripsi_barang"=> $row["DeskripsiBarang"]
	));
	
	$total_koli		+= $row['Koli'];
	$total_omzet	+= $row['TotalBiaya'];
	$total_berat	+= $row['Berat'];
	
}

$data_manifest			= $Cargo->ambilDataManifestByTglPoin($tgl_berangkat,$poin_pickedup);

$waktu_bawa	= date("Y-m-d H:i:s");
$cso		= $userdata['nama'];
$pembawa= $namakurir;

if($data_manifest["Id"]!=""){
	$Cargo->cetakUlangSPJ(
		$data_manifest["Id"],$userdata['user_id'],$userdata['nama'],
		$body,$sopir_dipilih,$nama_sopir,
		$i,$total_koli,$total_omzet,
		$total_berat,$kurir,$pembawa);
	
	$no_spj	= $data_manifest["NoSPJ"];
}
else{
	$nourut = rand(1000,9999);
	$no_spj	= "MNFC-".$poin_pickedup."-".dateYMD().$nourut;
	
	$Cargo->tambahSPJ($no_spj,$tgl_berangkat,$poin_pickedup,
		$nama_poinpickedup,$userdata['user_id'],$userdata['nama'],
		$body,$sopir_dipilih,$nama_sopir,
		$i,$total_koli,$total_omzet,
		$total_berat,$kurir,$pembawa);
}

$template->assign_vars(array(
	'NAMA_PERUSAHAAN'	=> $config["nama_perusahaan"],
	'NO_SPJ'					=> $no_spj,
	'CSO'							=> $cso,
	'PEMBAWA'					=> $pembawa,
	'WAKTU_BAWA'			=> dateparseWithTime(FormatMySQLDateToTglWithTime($waktu_bawa)),
));

$template->set_filenames(array('body' => 'reservasi.cargo/spj.tpl')); 

$template->pparse('body');
?>