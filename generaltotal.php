<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################
include($adp_root_path . 'ClassKota.php');

// PARAMETER
$perpage 	= $config['perpage'];
$tanggal  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$kota     = isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];

// LIST
$template->set_filenames(array('body' => 'generaltotal/index.tpl')); 

$tanggal	= ($tanggal!='')?$tanggal:dateD_M_Y();
$tanggal_mysql = FormatTglToMySQLDate($tanggal);

if($kota!=""){
  $kondisi_kota     = " AND f_cabang_get_kota_by_kode_cabang(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan))='$kota'";
}
else{
  $kondisi_kota = "";
}

//MEGAMBIL OPEN TRIP=========
//mengambil list jadwal yang aktif
$sql	=
	"SELECT KodeJadwal
	FROM tbl_penjadwalan_kendaraan
	WHERE TglBerangkat='$tanggal_mysql' AND StatusAktif=1 $kondisi_kota";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$list_kode_jadwal_aktif	= "";
$idx	=0;
while($row=$db->sql_fetchrow($result)){
	$list_kode_jadwal_aktif .="'".$row[0]."',";
	$idx++;
}

$jumlah_jadwal_diaktifkan	= $db->sql_numrows($result);
$list_kode_jadwal_aktif		= $list_kode_jadwal_aktif!=""?substr($list_kode_jadwal_aktif,0,-1):"''";


//mengambil list jadwal yang tidak aktif
$sql	=
	"SELECT KodeJadwal
	FROM tbl_penjadwalan_kendaraan
	WHERE TglBerangkat='$tanggal_mysql' AND StatusAktif=0 $kondisi_kota";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$list_kode_jadwal_nonaktif	= "";

while($row=$db->sql_fetchrow($result)){
	$list_kode_jadwal_nonaktif .="'".$row[0]."',";
}


$list_kode_jadwal_nonaktif		= $list_kode_jadwal_nonaktif!=""?substr($list_kode_jadwal_nonaktif,0,-1):"''";

$sql=
	"SELECT IS_NULL(COUNT(1),0)+$jumlah_jadwal_diaktifkan AS JadwalAktif
	FROM tbl_md_jadwal
	WHERE KodeJadwal NOT IN($list_kode_jadwal_aktif) AND KodeJadwal NOT IN($list_kode_jadwal_nonaktif) AND FlagAktif=1 $kondisi_kota";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_open_trip=$db->sql_fetchrow($result);

//END====MEGAMBIL OPEN TRIP=========

//MEGAMBIL TRIP ISI================
$sql	=
	"SELECT IS_NULL(COUNT(DISTINCT(KodeJadwal)),0) AS TripIsi
	FROM tbl_reservasi WHERE FlagBatal!=1 AND TglBerangkat='$tanggal_mysql' $kondisi_kota";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_trip_isi=$db->sql_fetchrow($result);
//END====MEGAMBIL TRIP ISI=========

//MEGAMBIL BOOKING & CANCEL================
$sql	=
	"SELECT IS_NULL(COUNT(1),0) AS Booking,IS_NULL(COUNT(IF(FlagBatal=1,1,NULL)),0) AS Cancel
	FROM tbl_reservasi WHERE TglBerangkat='$tanggal_mysql' $kondisi_kota";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_book=$db->sql_fetchrow($result);
//END====BOOKING & CANCEL=========

//MEGAMBIL DATA TIKET================
if($kota!=""){
  $sql	= "SELECT IdJurusan FROM tbl_md_jurusan WHERE f_cabang_get_kota_by_kode_cabang(KodeCabangAsal) LIKE '$kota'";

  if(!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
  }

  $list_jurusan	= "";

  while($row=$db->sql_fetchrow($result)){
    $list_jurusan .=$row[0].",";
  }

  $list_jurusan		= $list_jurusan!=""?substr($list_jurusan,0,-1):"''";

  $kondisi_jurusan  = "AND IdJurusan IN($list_jurusan)";

}
else{
  $kondisi_jurusan = "";
}

$sql	=
	"SELECT
		IS_NULL(COUNT(1),0) AS Penumpang,
		IS_NULL(COUNT(IF(PetugasCetakTiket=0,1,NULL)),0) AS PenumpangOnline,
		IS_NULL(SUM(Total),0) AS OmzetPenumpang,
		IS_NULL(SUM(IF(PetugasCetakTiket=0,Total,0)),0) AS OmzetPenumpangOnline
	FROM tbl_reservasi WHERE TglBerangkat='$tanggal_mysql' AND FlagBatal!=1 AND CetakTiket=1 $kondisi_jurusan";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_tiket=$db->sql_fetchrow($result);
//END==== DATA TIKET=========


//MEGAMBIL DATA PAKET================
//mengambil jurusan dari Jakarta

$sql	=
  "SELECT
		IS_NULL(COUNT(1),0) AS Paket,
		IS_NULL(SUM(HargaPaket),0) AS OmzetPaket
	FROM tbl_paket WHERE TglBerangkat='$tanggal_mysql' AND FlagBatal!=1 AND CetakTiket=1 $kondisi_jurusan";

if(!$result = $db->sql_query($sql)){
  echo("Err:".__LINE__);exit;
}

$data_paket=$db->sql_fetchrow($result);
//END==== DATA PAKET=========

//KENDARAAN=====================
//jumlah kendaraan keseluruhan
$sql	=
	"SELECT IS_NULL(COUNT(1),0)
	FROM tbl_md_kendaraan
	WHERE KodeKendaraan NOT LIKE '%WH-%'";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_kendaraan=$db->sql_fetchrow($result);

$sql	=
	"SELECT IS_NULL(COUNT(DISTINCT(NoPolisi)),0)
	FROM tbl_spj
	WHERE DATE(TglBerangkat)='$tanggal_mysql' $kondisi_kota";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_kendaraan_aktif=$db->sql_fetchrow($result);
//END KENDARAAN=================

//SOPIR=====================
//jumlah sopir keseluruhan
$sql	=
	"SELECT IS_NULL(COUNT(1),0)
	FROM tbl_md_sopir
	WHERE FlagAktif=1";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_sopir=$db->sql_fetchrow($result);

$sql	=
	"SELECT IS_NULL(COUNT(DISTINCT(KodeDriver)),0)
	FROM tbl_spj
	WHERE DATE(TglBerangkat)='$tanggal_mysql' $kondisi_kota";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_sopir_aktif=$db->sql_fetchrow($result);
//END SOPIR=================

$Kota = new Kota();

$page_title = "General Total";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('generaltotal.'.$phpEx).'">General Total</a>',
	'ACTION_CARI'		=> append_sid('generaltotal.'.$phpEx),
	'TGL_AWAL'			=> $tanggal,
	'TANGGAL'				=> dateparse($tanggal),
  'OPT_KOTA'			=> $Kota->setComboKota($kota),
  'OPEN_TRIP'			=> number_format($data_open_trip[0],0,",","."),
	'TRIP_ISI'			=> number_format($data_trip_isi[0],0,",","."),
	'BOOKING'				=> number_format($data_book['Booking'],0,",","."),
	'CANCEL'				=> number_format($data_book['Cancel'],0,",","."),
	'PNP'				    => number_format($data_tiket['Penumpang'],0,",","."),
	'PNP_OL'		    => number_format($data_tiket['PenumpangOnline'],0,",","."),
	'PKT'				    => number_format($data_paket['Paket'],0,",","."),
	'OMZ_PNP'		    => number_format($data_tiket['OmzetPenumpang'],0,",","."),
	'OMZ_OL'		    => number_format($data_tiket['OmzetPenumpangOnline'],0,",","."),
	'OMZ_PKT'		    => number_format($data_paket['OmzetPaket'],0,",","."),
	'UNIT_AKTIF'		=> number_format($data_kendaraan_aktif[0],0,",","."),
	'UNIT_NONAKTIF'	=> number_format($data_kendaraan[0]-$data_kendaraan_aktif[0],0,",","."),
	'SUPIR_AKTIF'		=> number_format($data_sopir_aktif[0],0,",","."),
	'SUPIR_NONAKTIF'=> number_format($data_sopir[0]-$data_sopir_aktif[0],0,",","."),
	'MEMBER_AKTIF'	=> number_format(0,0,",","."),
	'MEMBER_NONAKTIF'=> number_format(0,0,",",".")
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>