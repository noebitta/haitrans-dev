<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassUser.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//METHODS
function getDataSPJ($tgl_berangkat,$kode_jadwal){
	global $db;
	
	$sql=
		"SELECT 
			NoSPJ,KodeDriver,Driver,NoPolisi
		FROM tbl_spj
		WHERE KodeJadwal='$kode_jadwal' AND TglBerangkat='$tgl_berangkat'";

	if (!$result= $db->sql_query($sql)){
			echo("Error:".__LINE__);exit;
	}
	
	return $db->sql_fetchrow($result);
	
}

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$id_setoran		= $HTTP_GET_VARS['id'];
	
//INISIALISASI
$PengaturanUmum	= new PengaturanUmum();
$User						= new User();
$useraktif			= $userdata['user_id'];
$data_user			= $User->ambilDataDetail($useraktif);

$data_perusahaan	= $PengaturanUmum->ambilDataPerusahaan();

$template->set_filenames(array('body' => 'laporan.rekap.setoran/struk.tpl')); 

if($id_setoran==""){
	$kondisi		= "(IdSetoran='' OR IdSetoran IS NULL)";
	$kondisi_spj= "(ts.IdSetoran='' OR ts.IdSetoran IS NULL)";
}
else{
	$kondisi		= "IdSetoran='$id_setoran'";
	$kondisi_spj= "ts.IdSetoran='$id_setoran'";
}

//QUERY DATA TIKET
$sql=
	"SELECT 
		KodeJadwal,TglBerangkat,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS Asal,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan,
		JamBerangkat,JenisPembayaran,JenisPenumpang,
		COUNT(NoTiket) AS JumlahPenumpang,
		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalTunai,
		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalDebit,
		IS_NULL(SUM(IF(JenisPembayaran=2,Total,0)),0) AS TotalKartuKredit,
		IS_NULL(SUM(IF(JenisPembayaran!=3,Total,0)),0) AS Total,
		IS_NULL(SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IdJurusan
	FROM tbl_reservasi
	WHERE $kondisi
	AND CetakTiket=1 
	AND FlagBatal!=1 
	AND PetugasCetakTiket=$useraktif
	GROUP BY TglBerangkat,KodeJadwal,JamBerangkat
	ORDER BY TglBerangkat,Asal,Tujuan,JamBerangkat ";

if (!$result_penumpang_detail = $db->sql_query($sql)){
	echo("Error:".__LINE__);exit;
}

//DETAIL DATA TIKET
$jurusan_temp	="";
$tanggal_temp	="";

$total_pendapatan_tiket_tunai	= 0;
$total_pendapatan_tiket_debit	= 0;
$total_pendapatan_tiket_kredit= 0;
$total_pendapatan_tiket				= 0;

while ($data_penumpang_detail = $db->sql_fetchrow($result_penumpang_detail)){
	
	//MENGAMBIL DATA SPJ
	$data_spj	= getDataSPJ($data_penumpang_detail['TglBerangkat'],$data_penumpang_detail['KodeJadwal']);
	
	if($data_spj['NoSPJ']!=""){
		$ket_nospj	= $data_spj['NoSPJ'];
		$ket_driver	= $data_spj['KodeDriver']."|".$data_spj['Driver'];;
		$ket_nopol	= $data_spj['NoPolisi'];
	}
	else{
		$ket_nospj	= "Belum Cetak";
		$ket_driver	= "Belum Cetak";
		$ket_nopol	= "Belum Cetak";
	}
	
	if($tanggal_temp==$data_penumpang_detail['TglBerangkat']){
		$show_tanggal	= "";
	}
	else{
		$show_tanggal	= "<font style='font-weight: bold;font-size:16px;'>".dateparse(FormatMySQLDateToTgl($data_penumpang_detail['TglBerangkat']))."</font><br>";
		$tanggal_temp	= $data_penumpang_detail['TglBerangkat'];
	}
	
	if($jurusan_temp!=$data_penumpang_detail['IdJurusan']){
		$show_jurusan	= $data_penumpang_detail['Asal'].'-'.$data_penumpang_detail['Tujuan']."<br>";
		$jurusan_temp	= $data_penumpang_detail['IdJurusan'];
	}

	//list jenis penumpang
	$list_jenis_penumpang	= "";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangU']>0?"|U:".$data_penumpang_detail['TotalPenumpangU']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangM']>0?"|M:".$data_penumpang_detail['TotalPenumpangM']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangK']>0?"|K:".$data_penumpang_detail['TotalPenumpangK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangKK']>0?"|KK:".$data_penumpang_detail['TotalPenumpangKK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangG']>0?"|G:".$data_penumpang_detail['TotalPenumpangG']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangT']>0?"|O:".$data_penumpang_detail['TotalPenumpangT']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangR']>0?"|R:".$data_penumpang_detail['TotalPenumpangR']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangV']>0?"|V:".$data_penumpang_detail['TotalPenumpangV']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangVR']>0?"|VR:".$data_penumpang_detail['TotalPenumpangVR']:"";
	
	$total_penumpang_umum					+= $data_penumpang_detail['TotalPenumpangU'];
	$total_penumpang_diskon				+= $data_penumpang_detail['TotalPenumpangM']+$data_penumpang_detail['TotalPenumpangK']+$data_penumpang_detail['TotalPenumpangKK']+$data_penumpang_detail['TotalPenumpangG']+$data_penumpang_detail['TotalPenumpangT']+$data_penumpang_detail['TotalPenumpangR']+$data_penumpang_detail['TotalPenumpangV']+$data_penumpang_detail['TotalPenumpangVR'];
	$total_pendapatan_tiket_tunai	+= $data_penumpang_detail['TotalTunai'];
	$total_pendapatan_tiket_debit	+= $data_penumpang_detail['TotalDebit'];
	$total_pendapatan_tiket_kredit+= $data_penumpang_detail['TotalKartuKredit'];
	$total_pendapatan_tiket				+= $data_penumpang_detail['Total'];
	$total_diskon									+= $data_penumpang_detail['TotalDiscount'];
	
	$template->
		assign_block_vars(
			'TIKET',
			array(
				'SHOWTANGGAL'		=>$show_tanggal,
				'JURUSAN'				=>$show_jurusan,
				'JAM'						=>$data_penumpang_detail['JamBerangkat'],
				'KET_SPJ'				=>$ket_driver,
				'LIST_PENUMPANG'=>$data_penumpang_detail['JumlahPenumpang'].$list_jenis_penumpang,
				'TUNAI'					=>number_format($data_penumpang_detail['TotalTunai'],0,",","."),
				'DEBIT'					=>$data_penumpang_detail['TotalDebit']==0?"":"Debit:Rp.".number_format($data_penumpang_detail['TotalDebit'],0,",",".")."<br>",
				'KREDIT'				=>$data_penumpang_detail['TotalKartuKredit']==0?"":"Kredit:Rp.".number_format($data_penumpang_detail['TotalKartuKredit'],0,",",".")."<br>",
			)
	);
}

$show_sub_total_tiket	 = "Penj.Tunai:Rp.". number_format($total_pendapatan_tiket_tunai,0,",",".")."<br>";
$show_sub_total_tiket	.= $total_pendapatan_tiket_debit==0?"":"Penj.Debit:Rp.". number_format($total_pendapatan_tiket_debit,0,",",".")."<br>";
$show_sub_total_tiket	.= $total_pendapatan_tiket_kredit==0?"":"Penj.Kredit:Rp.". number_format($total_pendapatan_tiket_kredit,0,",",".")."<br>";
$show_sub_total_tiket .= "Penj.Tiket:Rp.".number_format($total_pendapatan_tiket,0,",",".")."<br>";

//END TIKET

//QUERY PAKET
$sql=
	"SELECT 
		IS_NULL(COUNT(NoTiket),0) AS JumlahTransasi,
		IS_NULL(SUM(JumlahKoli),0) AS JumlahPax,
		IS_NULL(SUM(Berat),0) AS JumlahKilo,
		IS_NULL(SUM(IF(JenisPembayaran=0,TotalBayar,0)),0) AS TotalTunai,
		IS_NULL(SUM(IF(JenisPembayaran=1,TotalBayar,0)),0) AS TotalDebit,
		IS_NULL(SUM(IF(JenisPembayaran=2,TotalBayar,0)),0) AS TotalKartuKredit,
		IS_NULL(SUM(TotalBayar),0) AS Total
	FROM tbl_paket
	WHERE
		$kondisi
		AND CetakTiket=1 
		AND FlagBatal!=1
		AND PetugasPenjual=$useraktif
	";

if (!$result_paket= $db->sql_query($sql)){
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$data_paket	= $db->sql_fetchrow($result_paket);

//QUERY CARGO
$sql=
	"SELECT 
		IS_NULL(COUNT(Id),0) AS JumlahTransasi,
		IS_NULL(SUM(Koli),0) AS JumlahPax,
		IS_NULL(SUM(Berat),0) AS JumlahKilo,
		IS_NULL(SUM(TotalBiaya),0) AS Total
	FROM tbl_paket_cargo
	WHERE
		$kondisi
		AND (DicetakOleh IS NOT NULL OR DicetakOleh!='') 
		AND IsBatal=0
		AND DicetakOleh=$userdata[user_id]
	";

if (!$result_cargo= $db->sql_query($sql)){
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$data_cargo	= $db->sql_fetchrow($result_cargo);

if($data_paket['JumlahTransasi']>0 || $data_cargo['JumlahTransasi']>0){	
	$template->
		assign_block_vars(
			'PAKET',
				array(
					'TUNAI'					=> number_format($data_paket['TotalTunai']+$data_cargo['Total'],0,",","."),
					'DEBIT'					=> $data_paket['TotalDebit']==0?"":"Debit:Rp.".number_format($data_paket['TotalDebit'],0,",",".")."<br>",
					'KREDIT'				=> $data_paket['TotalKartuKredit']==0?"":"Penj.Kredit:Rp.". number_format($data_paket['TotalKartuKredit'],0,",",".")."<br>",
					'TOTAL_PENJUALAN'=> number_format($data_paket['Total']+$data_cargo['Total'],0,",","."),
					'JUMLAH_TRANSAKSI'=> number_format($data_paket['JumlahTransasi']+$data_cargo['JumlahTransasi'],0,",","."),
					'JUMLAH_PAX'		=> number_format($data_paket['JumlahPax']+$data_cargo['JumlahPax'],0,",","."),
					'JUMLAH_KILO'		=> number_format($data_paket['JumlahKilo']+$data_cargo['JumlahKilo'],0,",","."),
		)
	);
}

//END PAKET

//QUERY BIAYA
$sql=
	"SELECT
		IS_NULL(SUM(IF(FlagJenisBiaya=2,Jumlah,0)),0) AS TotalBiayaSopir,
		IS_NULL(SUM(IF(FlagJenisBiaya=1,Jumlah,0)),0) AS TotalBiayaTol,
		IS_NULL(SUM(IF(FlagJenisBiaya=4,Jumlah,0)),0) AS TotalBiayaParkir,
		IS_NULL(SUM(IF(FlagJenisBiaya=5,Jumlah,0)),0) AS TotalBiayaInsentifSopir,
		IS_NULL(SUM(IF(FlagJenisBiaya=7,Jumlah,0)),0) AS TotalBiayaTambahanBBM,
		IS_NULL(SUM(IF(FlagJenisBiaya=8,Jumlah,0)),0) AS TotalBiayaTambahanTol,
		IS_NULL(SUM(IF(FlagJenisBiaya=9,Jumlah,0)),0) AS TotalBiayaTambahanLain,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE
		$kondisi
		AND IdPetugas='$useraktif'
		AND NOT FlagJenisBiaya IN($FLAG_BIAYA_BBM,$FLAG_BIAYA_VOUCHER_BBM)";

if (!$result_biaya = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$data_biaya	= $db->sql_fetchrow($result_biaya);

$show_total_setoran	= $total_pendapatan_tiket_debit+$data_paket['TotalDebit']==0?"":"Setoran Debit:Rp.". number_format($total_pendapatan_tiket_debit+$data_paket['TotalDebit'],0,",",".")."<br>";
$show_total_setoran	.= $total_pendapatan_tiket_kredit+$data_paket['TotalKartuKredit']==0?"":"Setoran Kredit:Rp.". number_format($total_pendapatan_tiket_kredit+$data_paket['TotalKartuKredit'],0,",",".")."<br>";

if($data_biaya['TotalBiayaTambahanBBM']>0){
	$template->assign_block_vars("ketbiayatambahan",array());
	$template->assign_block_vars("showtambahanbbm",array("JUMLAH"=> number_format($data_biaya['TotalBiayaTambahanBBM'],0,",",".")));
}

if($data_biaya['TotalBiayaTambahanTol']>0){
	$template->assign_block_vars("ketbiayatambahan",array());
	$template->assign_block_vars("showtambahantol",array("JUMLAH"=> number_format($data_biaya['TotalBiayaTambahanTol'],0,",",".")));
}

if($data_biaya['TotalBiayaTambahanLain']>0){
	$template->assign_block_vars("ketbiayatambahan",array());
	$template->assign_block_vars("showtambahanlain",array("JUMLAH"=> number_format($data_biaya['TotalBiayaTambahanLain'],0,",",".")));
}


//REKAP MANIFEST

//QUERY DATA PAKET
$sql=
	"SELECT
		ts.NoSPJ,	
		IS_NULL(SUM(JumlahKoli),0) AS JumlahPax,
		IS_NULL(SUM(TotalBayar),0) AS OmzetPaket
	FROM (tbl_spj ts INNER JOIN tbl_paket tp ON ts.NoSPJ=tp.NoSPJ)
	WHERE $kondisi_spj
	AND CetakTiket=1
	AND FlagBatal!=1
	AND CSO=$useraktif
	GROUP BY tp.NoSPJ
	ORDER BY ts.NoSPJ";

if (!$result = $db->sql_query($sql)){
	echo("Err: $sql".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_paket_spj[$row['NoSPJ']]['JumlahPax']	= $row['JumlahPax'];
	$data_paket_spj[$row['NoSPJ']]['OmzetPaket']	= $row['OmzetPaket'];
}

//QUERY DATA MANIFEST
$sql=
	"SELECT
		ts.KodeJadwal,ts.TglBerangkat,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(ts.IdJurusan)) AS Asal,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(ts.IdJurusan)) AS Tujuan,
		ts.JamBerangkat,
		COUNT(NoTiket) AS JumlahPenumpang,
		IS_NULL(SUM(IF(JenisPenumpang!='R',HargaTiket-Discount,HargaTiket)),0) AS TotalOmzet,
    IS_NULL(SUM(IF(JenisPembayaran!=3 AND JenisPenumpang!='T',Total,0)),0) AS Total,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangO,
		IS_NULL(COUNT(IF(JenisPenumpang='V' AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(SUM(IF(JenisPenumpang='R' AND JenisPembayaran!=3,HargaTiket-Discount,NULL)),0) AS PendapatanReturn,
		IS_NULL(SUM(IF(JenisPembayaran='3',Total,0)),0) AS PendapatanVoucherReturn,
		IS_NULL(SUM(IF(JenisPenumpang='T',Total,NULL)),0) AS PendapatanOnline,
		ts.IdJurusan,
		IS_NULL((SELECT SUM(Jumlah) FROM tbl_biaya_op WHERE NoSPJ=ts.NoSPJ AND NOT FlagJenisBiaya IN($FLAG_BIAYA_BBM,$FLAG_BIAYA_VOUCHER_BBM)),0) AS TotalBiaya
	FROM (tbl_spj ts LEFT JOIN tbl_reservasi tr ON ts.NoSPJ=tr.NoSPJ)
	WHERE $kondisi_spj
	AND CetakTiket=1
	AND FlagBatal!=1
	AND CSO=$useraktif
	GROUP BY ts.KodeJadwal,ts.JamBerangkat
	ORDER BY Asal,Tujuan,ts.JamBerangkat";

if (!$result_penumpang_detail = $db->sql_query($sql)){
	echo("Error:".__LINE__);exit;
}

$jurusan_temp	="";

$total_per_jurusan			= 0;
$total_pendapatan_tiket	= 0;
$total_pendapatan_paket	= 0;

$data_penumpang_detail = $db->sql_fetchrow($result_penumpang_detail);

while ($data_penumpang_detail){
	
	//MENGAMBIL DATA SPJ
	$data_spj	= getDataSPJ($data_penumpang_detail['TglBerangkat'],$data_penumpang_detail['KodeJadwal']);
	
	if($data_spj['NoSPJ']!=""){
		$ket_nospj	= $data_spj['NoSPJ'];
		$ket_driver	= $data_spj['KodeDriver']."|".$data_spj['Driver'];
		$ket_nopol	= $data_spj['NoPolisi']."|".$data_spj['NoPlat'];
	}
	else{
		$ket_nospj	= "Belum Cetak";
		$ket_driver	= "Belum Cetak";
		$ket_nopol	= "Belum Cetak";
	}
	
	if($jurusan_temp!=$data_penumpang_detail['IdJurusan']){
		$show_jurusan	= $data_penumpang_detail['Asal'].'-'.$data_penumpang_detail['Tujuan']."<br>";
		$jurusan_temp	= $data_penumpang_detail['IdJurusan'];
	}
	else{
		$show_jurusan="";
	}
	
	//list jenis penumpang
	$list_jenis_penumpang	= "";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangU']>0?"|U:".$data_penumpang_detail['TotalPenumpangU']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangM']>0?"|M:".$data_penumpang_detail['TotalPenumpangM']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangK']>0?"|K:".$data_penumpang_detail['TotalPenumpangK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangKK']>0?"|KK:".$data_penumpang_detail['TotalPenumpangKK']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangG']>0?"|G:".$data_penumpang_detail['TotalPenumpangG']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangR']>0?"|R:".$data_penumpang_detail['TotalPenumpangR']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangO']>0?"|O:".$data_penumpang_detail['TotalPenumpangO']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangV']>0?"|V:".$data_penumpang_detail['TotalPenumpangV']:"";
	$list_jenis_penumpang	.=$data_penumpang_detail['TotalPenumpangVR']>0?"|VR:".$data_penumpang_detail['TotalPenumpangVR']:"";
	
	$tgl_berangkat		= $data_penumpang_detail['TglBerangkat'];
	$kode_jadwal			= $data_penumpang_detail['KodeJadwal'];
	$jam_berangkat		= $data_penumpang_detail['JamBerangkat'];
	$jumlah_penumpang = $data_penumpang_detail['JumlahPenumpang'];
	$omzet_penumpang	= $data_penumpang_detail['Total'];
	$total_biaya			= $data_penumpang_detail['TotalBiaya'];
	
 	$total_per_jurusan	+= $data_penumpang_detail['Total']+$data_paket_spj[$data_spj['NoSPJ']]['OmzetPaket'];
	$total_pendapatan_tiket += $data_penumpang_detail['Total'];
	$total_pendapatan_paket += $data_paket_spj[$data_spj['NoSPJ']]['OmzetPaket']; 
	
	$data_penumpang_detail = $db->sql_fetchrow($result_penumpang_detail);
	
	if($jurusan_temp!=$data_penumpang_detail['IdJurusan']){
		$show_sub_total_tiket_perjurusan	=
			"Sub Total:".substr("********************Rp.".number_format($total_per_jurusan,0,",","."),-18)."<br>
			-----------------------------------------<br>";
		$total_per_jurusan	= 0;
	}
	else{
		$show_sub_total_tiket_perjurusan="";
	}
	
	$template->
		assign_block_vars(
			'SPJ_TIKET',
			array(
				'NO_SPJ'				=>$ket_nospj,
				'TGL_BERANGKAT'	=>dateparseWithTime(FormatMySQLDateToTgl(substr($tgl_berangkat,0,10))),
				'KODE_JADWAL'		=>$kode_jadwal,
				'JURUSAN'				=>$show_jurusan,
				'JAM'						=>$jam_berangkat,
				'KENDARAAN'			=>$ket_nopol,
				'SOPIR'					=>$ket_driver,
				'LIST_PENUMPANG'=>$jumlah_penumpang.$list_jenis_penumpang,
				'OMZET_PNP'			=>number_format($omzet_penumpang,0,",","."),
				'BIAYA'					=>number_format($total_biaya,0,",","."),
				'PAX_PAKET'			=>number_format($data_paket_spj[$data_spj['NoSPJ']]['JumlahPax'],0,",","."),
				'OMZET_PKT'			=>number_format($data_paket_spj[$data_spj['NoSPJ']]['OmzetPaket'],0,",","."),
				'SHOW_SUB_TOTAL_TIKET_JURUSAN'=>$show_sub_total_tiket_perjurusan,
			)
	);
}

$show_sub_total =
	"Omz.Tiket:".substr("********************Rp.".number_format($total_pendapatan_tiket,0,",","."),-18)."<br>
	Omz.Paket:".substr("********************Rp.".number_format($total_pendapatan_paket,0,",","."),-18)."<br>";

$show_total = "Tot.Omzet:".substr("********************Rp.".number_format($total_pendapatan_tiket+$total_pendapatan_paket,0,",","."),-18)."<br>";

//END SPJ TIKET 

//UPDATE LOG SETORAN
include($adp_root_path . 'ClassRekapSetoran.php');
	
$RekapSetoran		= new RekapSetoran();

if($id_setoran==""){
	//proses setoran awal
	
	
	$id_setoran	= $RekapSetoran->generateKodeSetoran();	
	
	$RekapSetoran->prosesSetoran(
		$id_setoran,$userdata['user_id'],$userdata['nama'],$total_penumpang_umum,
		$total_penumpang_diskon,$total_pendapatan_tiket_tunai,$total_pendapatan_tiket_debit,
		$total_pendapatan_tiket_kredit,$total_diskon,$data_paket['JumlahTransasi']+$data_cargo['JumlahTransasi'],
		$data_paket['Total']+$data_cargo['Total'],$data_biaya['TotalBiaya']);
	
	$tanggal			= date("Y-m-d");
	$waktu_cetak	= date("Y-m-d H:i:s");
	$cetak_ulang	= "false";
}
else{
	$data_setoran	= $RekapSetoran->ambilDataDetail($id_setoran);
	$tanggal			= substr($data_setoran['WaktuSetoran'],0,10);
	$waktu_cetak	= $data_setoran['WaktuSetoran'];
	$cetak_ulang	= "true";
}

//set template
$template->assign_vars (
	array(
	'NAMA_PERUSAHAAN'			=> $data_perusahaan['NamaPerusahaan'],
	'RESI'								=> $id_setoran,
	'CETAK_ULANG'					=> $cetak_ulang,
	'NAMA_CSO'						=> $data_user['nama'],
	'TANGGAL_TRANSAKSI'		=> dateparse(FormatMySQLDateToTgl($tanggal)),
	'SHOW_SUB_TOTAL_TIKET'=> $show_sub_total_tiket,
	'BIAYA_SOPIR'					=> number_format($data_biaya['TotalBiayaSopir'],0,",","."),
	'BIAYA_INSENTIF_SOPIR'=> number_format($data_biaya['TotalBiayaInsentifSopir'],0,",","."),
	'BIAYA_PARKIR'				=> number_format($data_biaya['TotalBiayaParkir'],0,",","."),
	'BIAYA_TOL'						=> number_format($data_biaya['TotalBiayaTol'],0,",","."),
	'TOTAL_BIAYA'					=> number_format($data_biaya['TotalBiaya'],0,",","."),
	'TOTAL_SETORAN'				=> number_format($total_pendapatan_tiket_tunai+$data_paket['TotalTunai']-$data_biaya['TotalBiaya'],0,",","."),
	'SHOW_TOTAL_SETORAN'	=> $show_total_setoran,
	'SPJ_SHOW_SUB_TOTAL'	=> $show_sub_total,
	'SPJ_SHOW_TOTAL'			=> $show_total,
	'WAKTU_CETAK'					=> dateparseWithTime(FormatMySQLDateToTglWithTime($waktu_cetak))
	)
);

$template->pparse('body');	


?>