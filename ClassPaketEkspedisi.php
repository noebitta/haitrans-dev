<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  //redirect('index.'.$phpEx,true); 
  exit;
}
//#############################################################################

class Paket{

  //KAMUS GLOBAL
  var $ID_FILE; //ID Kelas
  var $TABEL1;

  //CONSTRUCTOR
  function Paket(){
    $this->ID_FILE="C-PKTX";
  }

  //BODY

  function ambilDataHeaderLayout($tgl,$kode_jadwal){

    /*
    Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
    */

    //kamus
    global $db;

    $sql =
      "SELECT
				NoSPJ,NoPolisi,KodeDriver,Driver,
				TglSPJ
			FROM tbl_spj
			WHERE (KodeJadwal LIKE '$kode_jadwal' AND TglBerangkat='$tgl')";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
      exit;
    }

    $row=$db->sql_fetchrow($result);

    $data_layout['NoSPJ']					= $row['NoSPJ'];
    $data_layout['KodeKendaraan']	= $row['NoPolisi'];
    $data_layout['KodeDriver']		= $row['KodeDriver'];
    $data_layout['Driver']				= $row['Driver'];
    $data_layout['TglSPJ']				= $row['TglSPJ'];

    $sql =
      "SELECT NoPolisi
      FROM tbl_md_kendaraan
      WHERE KodeKendaraan='".$data_layout['KodeKendaraan']."'";

    if ($result = $db->sql_query($sql)){
      $row=$db->sql_fetchrow($result);

      $data_layout['NoPolisi']	= $row['NoPolisi'];
    }

    return $data_layout;

  }//  END ambilData

  function ambilDataSPJ($no_spj){

    //kamus
    global $db;

    $sql =
      "SELECT *
			FROM tbl_spj
			WHERE NoSPJ='$no_spj'";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return $row;

  }//  END ambilDataSPJ

  function periksaDuplikasi($no_tiket){

    /*
    ID	: 001
    Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
    */

    //kamus
    global $db;

    $sql = "SELECT f_paket_periksa_duplikasi('$no_tiket') AS jumlah_data";

    if ($result = $db->sql_query($sql)){
      while ($row = $db->sql_fetchrow($result)){
        //jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
        $ditemukan = ($row['jumlah_data']<=0)?false:true;
      }
    }
    else{
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return $ditemukan;

  }//  END periksaDuplikasi

  function tambah(
    $no_tiket, $kode_cabang, $kode_jadwal,
    $id_jurusan, $kode_kendaraan , $kode_sopir ,
    $tgl_berangkat, $jam_berangkat , $nama_pengirim ,
    $alamat_pengirim, $telp_pengirim,
    $nama_penerima, $alamat_penerima, $telp_penerima,
    $harga_paket,$diskon,$total_bayar,
    $keterangan_paket, $petugas_penjual,
    $komisi_paket_CSO, $komisi_paket_sopir,
    $kode_akun_pendapatan, $kode_akun_komisi_paket_CSO, $kode_akun_komisi_paket_sopir,
    $jumlah_koli,$berat,$dimensi,
    $is_volumetrik, $dimensi_p,$dimensi_l,
    $dimensi_t,$jenis_layanan,$cara_bayar){

    /*
    IS	: data paket belum ada dalam database
    FS	:Data paket baru telah disimpan dalam database
    */

    //kamus
    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
      "INSERT INTO tbl_paket (
				NoTiket, KodeCabang, KodeJadwal,
				IdJurusan, KodeKendaraan, KodeSopir,
				TglBerangkat, JamBerangkat, NamaPengirim,
				AlamatPengirim, TelpPengirim, WaktuPesan,
				NamaPenerima, AlamatPenerima, TelpPenerima,
				HargaPaket,Diskon,TotalBayar,KeteranganPaket, PetugasPenjual,
				NoSPJ, TglCetakSPJ,
				CetakSPJ, KomisiPaketCSO, KomisiPaketSopir,
				KodeAkunPendapatan,KodeAkunKomisiPaketCSO,KodeAkunKomisiPaketSopir,
				JumlahKoli,Berat,Dimensi,
				IsVolumetrik,Panjang,Lebar,
				Tinggi,JenisLayanan,CaraPembayaran,
				FlagBatal,InstruksiKhusus,KodePelanggan)
			VALUES(
				'$no_tiket', '$kode_cabang', '$kode_jadwal',
				'$id_jurusan', '$kode_kendaraan', '$kode_sopir',
				'$tgl_berangkat', '$jam_berangkat' , '$nama_pengirim' ,
				'$alamat_pengirim', '$telp_pengirim', NOW(),
				'$nama_penerima', '$alamat_penerima', '$telp_penerima',
				'$harga_paket','$diskon','$total_bayar','$keterangan_paket', '$petugas_penjual',
				'', '',
				'0', '$komisi_paket_CSO', '$komisi_paket_sopir',
				'$kode_akun_pendapatan', '$kode_akun_komisi_paket_CSO', '$kode_akun_komisi_paket_sopir',
				'$jumlah_koli','$berat','$dimensi',
				'$is_volumetrik','$dimensi_p','$dimensi_l',
				'$dimensi_t','$jenis_layanan','$cara_bayar',
				0,'','');";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }

  function pembatalan($no_tiket,$petugas_pembatalan){

    /*
    IS	: data paket sudah ada dalam database
    FS	:Data paket dibatalkan
    */

    //kamus
    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql =
      "CALL sp_paket_batal('$no_tiket','$petugas_pembatalan',NOW());";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }

  function  updateCetakTiket($no_tiket,$jenis_pembayaran,$cabang_transaksi=""){

    /*
    IS	: data paket sudah ada dalam database
    FS	:Data paket dibatalkan
    */

    //kamus
    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
      "UPDATE tbl_paket
				SET
					CetakTiket=1,
					JenisPembayaran='$jenis_pembayaran',
					KodeCabang='$cabang_transaksi',
					WaktuCetakTiket=NOW()
				WHERE
					NoTiket = '$no_tiket';";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }

  function ambilData($kondisi){

    /*
    ID	:003
    Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
    */

    //kamus
    global $db;

    $kondisi	= ($kondisi=='')?1:$kondisi;

    $sql =
      "SELECT *,f_user_get_nama_by_userid(PetugasPemberi) AS PetugasPemberi
			FROM tbl_paket
			WHERE $kondisi;";

    if ($result = $db->sql_query($sql)){
      return $result;
    }
    else{
      die_error("Err $this->ID_FILE".__LINE__);
    }

  }//  END ambilData

  function ambilDataDetail($no_tiket){

    /*
    Desc	:Mengembalikan data paket sesuai dengan kriteria yang dicari
    */

    //kamus
    global $db;

    $sql =
      "SELECT *,
			f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS NamaAsal,
			f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS NamaTujuan,
			f_user_get_nama_by_userid(PetugasPemberi) AS NamaPetugasPemberi
			FROM tbl_paket
			WHERE NoTiket='$no_tiket';";

    if ($result = $db->sql_query($sql)){
      $row=$db->sql_fetchrow($result);
      return $row;
    }
    else{
      die_error("Err: $this->ID_FILE".__LINE__);
    }

  }//  END ambilData

  function  updatePaketDiambil($no_tiket,$nama_pengambil,$no_ktp_pengambil,$petugas_pemberi,$cabang_ambil){

    /*
    IS	: data paket sudah ada dalam database
    FS	:Data paket dibatalkan
    */

    //kamus
    global $db;
    global $PAKET_CARA_BAYAR_DI_TUJUAN;

    //memeriksa Cara  bayar
    $sql =
      "SELECT CaraPembayaran
			FROM tbl_paket
			WHERE NoTiket='$no_tiket';";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $row=$db->sql_fetchrow($result);
    $cara_bayar	= $row[0];


    //MENAMBAHKAN DATA KEDALAM DATABASE

    if($cara_bayar!=$PAKET_CARA_BAYAR_DI_TUJUAN){
      $sql =
        "CALL sp_paket_ambil_paket('$no_tiket','$nama_pengambil','$no_ktp_pengambil','$petugas_pemberi');";
    }
    else{
      $sql =
        "UPDATE tbl_paket
			  SET
			    NamaPengambil='$nama_pengambil',
			    NoKTPPengambil='$no_ktp_pengambil',
			    PetugasPemberi='$petugas_pemberi',
			    WaktuPengambilan=NOW(),
					CetakTiket=1,
			    StatusDiambil=1,
					KodeCabang='$cabang_ambil'
			  WHERE
			    NoTiket = '$no_tiket'";
    }

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }

  function mutasi(
    $resi,
    $tanggal,$jam_berangkat,$id_jurusan,
    $kode_jadwal,$petugas,$nama_petugas){

    //kamus
    global $db;

    $data_paket	= $this->ambilDataDetail($resi);

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
      "UPDATE tbl_paket SET
				TglBerangkat='$tanggal',
				JamBerangkat='$jam_berangkat',
				KodeJadwal='$kode_jadwal',
				IdJurusan='$id_jurusan',
				NoSPJ='',
				TglCetakSPJ='',
				CetakSPJ=0,
				FlagMutasi=1,
				LogMutasi=CONCAT(IS_NULL(LogMutasi,''),'#$data_paket[TglBerangkat]|$data_paket[KodeJadwal]|$petugas|$nama_petugas|',NOW())
			WHERE NoTiket='$resi';";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function ambilDaftarHarga($id_jurusan){

    /*
    Desc	:Mengembalikan data-data harga paket
    */

    //kamus
    global $db;

    $sql =
      "SELECT
				HargaPaket1KiloPertama,HargaPaket1KiloBerikut,
				HargaPaket2KiloPertama,HargaPaket2KiloBerikut,
				HargaPaket3KiloPertama,HargaPaket3KiloBerikut,
				HargaPaket4KiloPertama,HargaPaket4KiloBerikut,
				HargaPaket5KiloPertama,HargaPaket5KiloBerikut,
				HargaPaket6KiloPertama,HargaPaket6KiloBerikut
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";

    if ($result = $db->sql_query($sql)){
      return $result;
    }
    else{
      die_error("Err $this->ID_FILE".__LINE__);
    }

  }//  END ambilDaftarHarga

  function ambilHargaPaketByKodeLayanan($id_jurusan,$dimensi){

    /*
    Desc	:Mengembalikan data-data harga paket
    */

    //kamus
    global $db;

    $sql =
      "SELECT
        HargaPaket".$dimensi."KiloPertama,HargaPaket".$dimensi."KiloBerikut
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";

    if ($result = $db->sql_query($sql)){
      $row=$db->sql_fetchrow($result);
      return $row;
    }
    else{
      die_error("Err: $this->ID_FILE".__LINE__);
    }

  }//  END ambilDaftarHarga

  function hitungTotalOmzetdanJumlahPaketPerSPJ($tgl_berangkat,$kode_jadwal){

    /*
    Desc	:Mengembalikan data penumpang
    */

    //kamus
    global $db;

    $sql =
      "SELECT
				COUNT(NoTiket) AS JumlahPaket,
				SUM(HargaPaket) AS TotalOmzet,
				SUM(JumlahKoli) AS JumlahPax
			FROM tbl_paket
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND KodeJadwal IN($kode_jadwal)
				AND FlagBatal != 1 
				AND CetakTiket=1";

    if ($result = $db->sql_query($sql)){
      $row=$db->sql_fetchrow($result);

      return $row;
    }
    else{
      die_error("Err: $this->ID_FILE".__LINE__);
    }

  }//  END hitungTotalOmzetdanJumlahPaketPerSPJ

  function tambahSPJ(
    $no_spj, $kode_jadwal, $tgl_berangkat,
    $jam_berangkat,$mobil_dipilih, $cso,
    $sopir_dipilih, $nama_sopir, $jumlah_paket,
    $jumlah_pax_paket,$total_omzet_paket){

    /*
    IS	: data spj belum ada dalam database
    FS	:Data lspj baru telah disimpan dalam database
    */

    //kamus
    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql	=
      "INSERT INTO tbl_spj
				(NoSPJ,KodeJadwal,TglBerangkat,JamBerangkat,
				JumlahKursiDisediakan,JumlahPenumpang,TglSPJ,
				NoPolisi,CSO,KodeDriver,
				Driver,IdJurusan,FlagAmbilBiayaOP,
				JumlahPaket,JumlahPaxPaket,TotalOmzetPaket,
				IsEkspedisi)
			VALUES(
				'$no_spj','$kode_jadwal','$tgl_berangkat', '$jam_berangkat',
				0,0,NOW(),
				'$mobil_dipilih','$cso','$sopir_dipilih',
				'$nama_sopir',f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'),0,
				'$jumlah_paket','$jumlah_pax_paket','$total_omzet_paket',
				1);";

    if (!$db->sql_query($sql)){
      die_error("Err $this->ID_FILE".__LINE__);
    }

    return true;
  }

  function ubahSPJ(
    $no_spj,
    $jumlah_paket, $jumlah_pax_paket, $cso, $total_omzet_paket,
    $mobil_dipilih="", $sopir_dipilih="", $nama_sopir=""){

    /*
    IS	: data spj sudah ada dalam database
    FS	:Data spj  telah diubah  dalam database
    */

    //kamus
    global $db;

    $sql =
      "SELECT IsEkspedisi
			FROM tbl_spj
			WHERE NoSPJ = '$no_spj'";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    $field_set="";

    if($row['IsEkspedisi']){
      //ekspedisi
      $field_set=
        ",NoPolisi='$mobil_dipilih',
				KodeDriver='$sopir_dipilih',
				Driver='$nama_sopir'";
    }

    $sql	=
      "UPDATE tbl_spj SET
				CSOPaket=$cso,
				JumlahPaket='$jumlah_paket',
				JumlahPaxPaket='$jumlah_pax_paket',
				TotalOmzetPaket='$total_omzet_paket'
				$field_set
			WHERE NoSPJ='$no_spj';";

    if (!$db->sql_query($sql)){
      echo("Err $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function updateInsentifSopir(
    $no_spj, $layout_maksimum, $minimum_paket, $besar_insentif_sopir){

    //kamus
    global $db;

    //Mengambil jumlah penumpang pada no spj yang bersangkutan
    $sql =
      "SELECT COUNT(NoTiket) AS JumlahPaket
			FROM tbl_paket
			WHERE NoSPJ='$no_spj' AND CetakTiket=1;";

    if ($result = $db->sql_query($sql)){
      $row = $db->sql_fetchrow($result);
      $jumlah_paket	= $row['JumlahPaket'];
    }
    else{
      die_error("Err $this->ID_FILE".__LINE__);
    }

    if($layout_kursi<=$layout_maksimum){
      if($jumlah_paket>$minimum_paket){
        $jumlah_paket_dapat_insentif	= $jumlah_paket-$minimum_paket;

        $jumlah_insentif	= $jumlah_paket_dapat_insentif * $besar_insentif_sopir;

        $sql = "CALL sp_spj_ubah_insentif_sopir('$no_spj','$jumlah_insentif');";

        if (!$db->sql_query($sql)){
          die_error("Err $this->ID_FILE".__LINE__);
        }

      }
    }

    return true;
  }

  function ambilDataPaketUntukSPJ($no_spj,$tgl_berangkat,$kode_jadwal){

    /*
    Desc	:Mengembalikan data penumpang
    */

    //kamus
    global $db;

    $sql =
      "SELECT
				NoTiket,NamaPengirim,NamaPenerima,
				TelpPengirim,TelpPenerima,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan,
				JumlahKoli,TotalBayar
			FROM tbl_paket
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND NoSPJ='$no_spj'
				AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)=f_jurusan_get_kode_cabang_asal_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'))
				AND FlagBatal!=1
				AND CetakTiket=1
			ORDER BY WaktuPesan";

    if ($result = $db->sql_query($sql)){
      return $result;
    }
    else{
      die_error("Err: $this->ID_FILE $sql".__LINE__);
    }

  }//  END ambilDataPaketUntukSPJ

  function checkInPaket($no_tiket){

    /*
    IS	: data paket sudah ada dalam database
    FS	:Data paket dibatalkan
    */

    //kamus
    global $db;
    global $userdata;

    $sql =
      "UPDATE tbl_paket
      SET
        IsSampai=1,
        WaktuSampai=NOW(),
        UserCheckIn=".$userdata['user_id']."
			WHERE
				NoTiket IN ($no_tiket)";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }

  function tambahLogCetakManifest(
    $NoSPJ,$KodeJadwal,$TglBerangkat,
    $JamBerangkat,$JumlahPaket,$JumlahPaxPaket,
    $NoPolisi,$KodeDriver,$Driver,
    $TotalOmzetPaket,$IsSubJadwal,$UserCetak,
    $NamaUserPencetak){

    //kamus
    global $db;

    //menghitung cetakan tiket
    $sql	=
      "SELECT IS_NULL(COUNT(1),0) FROM tbl_log_cetak_manifest WHERE NoSPJ='$NoSPJ'";

    if (!$result=$db->sql_query($sql)){
      die_error("Err $this->ID_FILE".__LINE__);
    }

    $row	= $db->sql_fetchrow($result);

    $CetakanKe=$row[0]+1;

    $sql	=
      "INSERT INTO tbl_log_cetak_manifest(
				NoSPJ,KodeJadwal,TglBerangkat,
				JamBerangkat,JumlahPaxPaket,JumlahPaket,
				NoPolisi,KodeDriver,Driver,
				TotalOmzetPaket,IsSubJadwal,WaktuCetak,
				UserCetak,NamaUserPencetak,CetakanKe
			)
			VALUES(
				'$NoSPJ','$KodeJadwal','$TglBerangkat',
				'$JamBerangkat','$JumlahPaxPaket','$JumlahPaket',
				'$NoPolisi','$KodeDriver','$Driver',
				'$TotalOmzetPaket','$IsSubJadwal',NOW(),
				'$UserCetak','$NamaUserPencetak','$CetakanKe');";

    if (!$db->sql_query($sql)){
      die_error("Err $this->ID_FILE".__LINE__);
    }

    return true;
  }
}
?>