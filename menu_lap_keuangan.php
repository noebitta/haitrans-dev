<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,300); 
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

// HEADER

$page_title	= "Menu Laporan Keuangan";
$interface_menu_utama=true;

// TEMPLATE
switch($userdata['user_level']){
	case $USER_LEVEL_INDEX["CSO"]:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX["CSO_PAKET"]:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX["ADMIN"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_lap_keuangan_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		
		//Menu Laporan Keuangan
		$template->assign_block_vars('menu_laporan_keuangan',array());
		//sub menu
		$template->assign_block_vars('menu_lap_keuangan_cso',array());
		$template->assign_block_vars('menu_lap_keuangan_cabang',array());
		$template->assign_block_vars('menu_lap_harian',array());
		$template->assign_block_vars('menu_lap_fee_rekon',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_pembayaran_sopir',array());
		$template->assign_block_vars('menu_lap_voucher_bbm',array());
		$template->assign_block_vars('menu_lap_biaya_op',array());
		//END Laporan Keuangan
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
		
	break;
	
	case $USER_LEVEL_INDEX["MANAJEMEN"] :
		$height_wrapper	= 220;
		$template->set_filenames(array('body' => 'menu_lap_keuangan_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		
		//Menu Laporan Keuangan
		$template->assign_block_vars('menu_laporan_keuangan',array());
		//sub menu
		$template->assign_block_vars('menu_lap_keuangan_cso',array());
		$template->assign_block_vars('menu_lap_keuangan_cabang',array());
		$template->assign_block_vars('menu_lap_harian',array());
		$template->assign_block_vars('menu_lap_fee_rekon',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_pembayaran_sopir',array());
		$template->assign_block_vars('menu_lap_tiketux',array());
		$template->assign_block_vars('menu_lap_voucher_bbm',array());
		$template->assign_block_vars('menu_lap_biaya_op',array());
		//END Laporan Keuangan
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX["MANAJER"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_lap_keuangan_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		
		//Menu Laporan Keuangan
		$template->assign_block_vars('menu_laporan_keuangan',array());
		//sub menu
		$template->assign_block_vars('menu_lap_keuangan_cso',array());
		$template->assign_block_vars('menu_lap_keuangan_cabang',array());
		$template->assign_block_vars('menu_lap_harian',array());
		$template->assign_block_vars('menu_lap_fee_rekon',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_pembayaran_sopir',array());
		$template->assign_block_vars('menu_lap_tiketux',array());
		$template->assign_block_vars('menu_lap_voucher_bbm',array());
		$template->assign_block_vars('menu_lap_biaya_op',array());
		//END Laporan Keuangan
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX["SPV_RESERVASI"] :
		$height_wrapper	= 100;
		
		$template->set_filenames(array('body' => 'menu_lap_keuangan_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		
		$template->assign_block_vars('menu_laporan_reservasi',array());
		
		//Menu Laporan Keuangan
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_lap_keuangan_cso',array());
		//end menu laporan keuangan
	break;
	
	case $USER_LEVEL_INDEX["KEUANGAN"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_lap_keuangan_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		
		//Menu Laporan Keuangan
		$template->assign_block_vars('menu_laporan_keuangan',array());
		//sub menu
		$template->assign_block_vars('menu_lap_keuangan_cso',array());
		$template->assign_block_vars('menu_lap_keuangan_cabang',array());
		$template->assign_block_vars('menu_lap_harian',array());
		$template->assign_block_vars('menu_lap_fee_rekon',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_lap_pembayaran_sopir',array());
		$template->assign_block_vars('menu_lap_tiketux',array());
		$template->assign_block_vars('menu_lap_voucher_bbm',array());
		$template->assign_block_vars('menu_lap_biaya_op',array());
		//END Laporan Keuangan
		
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_tiketux',array());
	break;
	
}

$template->assign_vars(array
  ( 'BCRUMP'    						=> '<a href="'.append_sid('menu_lap_keuangan.'.$phpEx.'?top_menu_dipilih=top_menu_lap_keuangan') .'">Home',
    /*'U_LAPORAN_UANG_CSO'		=> append_sid('laporan_keuangan_cso.'.$phpEx),*/
		'U_LAPORAN_UANG_CSO'		=> append_sid('laporan.keuangan.setorancso.'.$phpEx),
		'U_LAPORAN_UANG_CABANG'	=> append_sid('laporan_uang_cabang.'.$phpEx),
		'U_REKAP_UANG_HARIAN'		=> append_sid('laporan_keuangan_harian.'.$phpEx),
		'U_LAPORAN_ASURANSI'		=> append_sid('laporan_asuransi.'.$phpEx),
		'U_LAPORAN_PEMB_SOPIR'	=> append_sid('laporan_pembayaran_sopir.'.$phpEx),
		'U_LAPORAN_FEE'					=> append_sid('laporan_keuangan_fee.'.$phpEx),
		'U_LAPORAN_TIKETUX'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx),
		'U_LAPORAN_VOUCHER_BBM'	=> append_sid('laporan_voucher_bbm.'.$phpEx),
		'U_LAPORAN_BIAYA_OP'		=> append_sid('laporan.keuangan.biayaop.'.$phpEx),
		'HEIGT_WRAPPER'			=>$height_wrapper
	));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>