<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberTransaksi.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['level_pengguna']>=$LEVEL_MANAJEMEN && $userdata['level_pengguna']!=$LEVEL_PROMOTION)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act   	 = $HTTP_GET_VARS['act'];
$pesan   = $HTTP_GET_VARS['pesan'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$TransaksiMember	= new TransaksiMember();

switch($mode){

//TAMPILKAN DATA MUTASI SEMUA MEMBER ==========================================================================================================
case 'tampilkan_mutasi':
	
	$cari 			= $HTTP_GET_VARS['cari'];
	$tgl_mula		= $HTTP_GET_VARS['tgl_mula'];
	$tgl_akhir 	= $HTTP_GET_VARS['tgl_akhir'];
	$kategori_member 	= $HTTP_GET_VARS['kategori_member'];
	$sortby    	= $HTTP_GET_VARS['sortby'];
	$ascending	= $HTTP_GET_VARS['ascending'];
	$ascending	= ($ascending==0)?"asc":"desc";
	
	//HEADER TABEL
	$hasil ="
		<table width='100%' class='border'>
    <tr>
      <th>#</th>
			<th><a href='#' onClick='setOrder(\"T1.id_member\")'>
				<font color='white'>ID Member</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"nama\")'>
				<font color='white'>Nama</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"alamat\")'>
				<font color='white'>Alamat</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"kategori_member\")'>
				<font color='white'>Kategori member</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"jum_mutasi_topup\")'>
				<font color='white'>Jum.Mutasi TopUp</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"jum_mutasi_transaksi\")'>
				<font color='white'>Jum.Mutasi Transaksi</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"T1.saldo\")'>
				<font color='white'>Balance</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"T1.point\")'>
				<font color='white'>Point</font></a></th>
			
			<th width='90'>Aksi</th>
    </tr>";
    
	$result	= $TransaksiMember->ambilGroupMutasiByMember($tgl_mula,$tgl_akhir,$kategori_member,$cari,$sortby,$ascending);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$action = 
			"<input type='button' value='&nbsp;&nbsp;Detail&nbsp;&nbsp;&nbsp;' onClick='tampilkanDataByMember(\"$row[id_member]\")' />";
		
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[id_member]</td>
      <td class='$odd'>$row[nama]</td>
      <td class='$odd'>$row[alamat] $row[kota]</td>
      <td class='$odd'>$row[kategori_member]</td>
      <td class='$odd' align='right'>".number_format($row['jum_mutasi_topup'],0,",",".")."</td>
      <td class='$odd' align='right'>".number_format($row['jum_mutasi_transaksi'],0,",",".")."</td>
			<td class='$odd' align='right'>".number_format($row['saldo'],0,",",".")."</td>
			<td class='$odd' align='right'>".number_format($row['point'],0,",",".")."</td>
			<td class='$odd' align='center'>$action</td>
    </tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=9 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

//TAMPILKAN DATA MUTASI  MEMBER TERTENTU==========================================================================================================
case 'tampilkan_mutasi_by_member':
	
	$id_member 	= trim($HTTP_GET_VARS['id_member']);
	$tgl_mula		= trim($HTTP_GET_VARS['tgl_mula']);
	$tgl_akhir 	= trim($HTTP_GET_VARS['tgl_akhir']);
	
	$Member	= new Member();
	
	$data_member	=	$Member->ambilDataDetail($id_member);
	
	$kolom_header_aksi=($userdata['level_pengguna']!=$LEVEL_ADMIN)?"":"<th><font color='white'>Aksi</font></th>";
	
	
	//HEADER TABEL
	$hasil ="
		<input type='hidden' id='hdn_id_member' value='$id_member' />
		<table width='100%'>
			<tr><td width='15%'>ID Member<td width='1%'>:</td><td>$data_member[id_member]</td>
			<tr><td>Kategori Member<td>:</td><td>$data_member[kategori_member]</td>
			<tr><td>Nama<td>:</td><td>$data_member[nama]</td>
			<tr><td>Alamat<td>:</td><td>$data_member[alamat] $data_member[kota]</td>
			<tr><td>Telp<td>:</td><td>$data_member[telp_rumah] / $data_member[handphone]</td>
			<tr><td>Saldo<td>:</td><td>Rp. ".number_format($data_member['saldo'],0,",",".")."</td>
			<tr><td>Point<td>:</td><td>".number_format($data_member['point'],0,",",".")."</td>
			<tr><td colspan=3 align='right'><input type='button' OnClick='cetakLaporan();' value='Cetak Laporan' /></td></tr>
		</table> 	
		<table width='100%' class='border'>
    <tr>
      <th>#</th>
			<th><font color='white'>ID Mutasi</font></th>
			<th><font color='white'>Waktu transaksi</font></th>
			<th><font color='white'>Referensi</font></th>
			<th><font color='white'>Keterangan</font></th>
			<th><font color='white'>Mutasi Topup</font></th>
			<th><font color='white'>Mutasi Transaksi</font></th>
			<th><font color='white'>Balance</font></th>
			<th><font color='white'>Operator</font></th>
			$kolom_header_aksi
    </tr>";
    
	$result	= $TransaksiMember->ambilMutasiByMember($tgl_mula,$tgl_akhir,$id_member);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$kolom_aksi=($userdata['level_pengguna']!=$LEVEL_ADMIN || $row['flag_batal']==1)?"":"<td class='$odd' align='center'><input type='button' value='Koreksi' onClick='showDialogUbahSaldo(\"$id_member\",\"$row[id_mutasi]\");'/></td>";
		
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[id_mutasi]</td>
      <td class='$odd'>$row[waktu_transaksi_con]</td>
      <td class='$odd'>$row[referensi]</td>
      <td class='$odd'>$row[keterangan]</td>
      <td class='$odd' align='right'>".number_format($row['jum_mutasi_topup'],0,",",".")."</td>
      <td class='$odd' align='right'>".number_format($row['jum_mutasi_transaksi'],0,",",".")."</td>
			<td class='$odd' align='right'>".number_format($row['saldo'],0,",",".")."</td>
			<td class='$odd' align='center'>$row[operator]</td>
			$kolom_aksi
    </tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=9 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .=
		"	<tr><td colspan=9 align='right'><input type='button' OnClick='cetakLaporan();' value='Cetak Laporan' /></td></tr>
		</table>";
	
	echo($hasil);
	
exit;

}//switch mode

//MENAMPILKAN JAVA SCRIPT KHUSUS UNTUK ADMIN
if($userdata['level_pengguna']>=$LEVEL_ADMIN){
	$java_script_koreksi="
		function showDialogUbahSaldo(id_member,id_transaksi){
			this.open(\"".append_sid('member_penyesuaian_saldo.'.$phpEx)."&id_mutasi=\"+id_transaksi+\"&id_member=\"+id_member,\"CtrlWindow\",\"top=200,left=200,width=600,height=400,scrollbars=1,resizable=0\");	
		}";
}

$template->set_filenames(array('body' => 'member_laporan_mutasi.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  					=>$userdata['username'],
   	'BCRUMP'    					=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_laporan_mutasi.'.$phpEx).'">Laporan Mutasi</a>',
   	'JAVA_SCRIPT_KOREKSI' =>$java_script_koreksi,
		'PESAN'								=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>