<?php
  define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,201); 
init_userprefs($userdata);

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] )
{  
  redirect(append_sid('index.'.$phpEx),true); 
}

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$id = $userdata['user_id'];

function OptionJurusan($id,$jurusan){
  global $db;
  
  $sql = "SELECT DISTINCT kodejadwal
                FROM         Tbreservasi
                WHERE     idUser=$id";
  $opt = "";
  if (!$result = $db->sql_query($sql))
  {
    die_error('Cannot Load Jurusan',__LINE__,__FILE__,$sql);
  } else
  {
    while ($row = $db->sql_fetchrow($result))
    {
      //$opt .= ($jurusan == $row[0]) ? "<option selected=selected value='$row[0]'>$row[1] - $row[2]</option>" : "<option value='$row[0]'>$row[1] - $row[2]</option>";
        $opt .= ($jurusan == $row[0]) ? "<option selected=selected value='$row[0]'>$row[0]</option>" : "<option value='$row[0]'>$row[0]</option>";
    }    
  }
  $opt = "<option>(none)</option>" . $opt;
  return $opt;
}

$ojur = OptionJurusan($id,$rute);
if ($mode=='show')
	{
		$rute = $HTTP_POST_VARS['rute'];
		$tgl = $HTTP_POST_VARS['tgl'];
		$sqla = "SELECT     Kode0, Nama0, CONVERT(char(20), TGlPesan, 104) AS Expr1, CONVERT(char(20), JamPEsan, 108) AS Expr2, CONVERT(char(20), TGLBerangkat, 104) 
                      AS Expr3, CONVERT(char(20), JamBerangkat, 108) AS Expr4, KodeJadwal
					  FROM         TbReservasi
					  WHERE     (KodeJadwal = '$rute') AND (CONVERT(CHAR(20), TGLBerangkat, 104) LIKE '$tgl') AND (idUser='$id')";

			if (!$resulta = $db->sql_query($sqla)){
				die_error('Cannot Load data',__FILE__,__LINE__,$sqla);
			}
			else
			{   $i=1;
				while($rowa = $db->sql_fetchrow($resulta))
				{
					$odd ='odd';
			 		if (($i % 2)==0)
			 		{
			   			$odd = 'even';
					}
			 
			 		$link = 'http://'.$config['name'].'/trav/print.php?nu='.$rowa[0].'&'.$SID;
			 		$url = append_sid($link);
					$script = "<script type='text/javascript'>function cana$i(){window.open('$url','INVOICE','height=300,width=500,scrollbars=yes');}</script>";
					$act = "<input type='button' onclick='cana$i()' value='print'>";
			 
					$template->set_filenames(array('body' => 'invoice.tpl')); 
					$template->assign_block_vars('ROW',array('odd'=>$odd,'no'=>$i,'book'=>$rowa[0],'name'=>$rowa[1],'kode'=>$rowa[6],
					'tglpsn'=>$rowa[2],'jampsn'=>$rowa[3],'tglbrk'=>$rowa[4],'jambrk'=>$rowa[5],'prnt'=>$act,'script'=>$script));
					$i++;
				}
			}
	}




// HEADER
include($adp_root_path . 'includes/page_header.php');

// TEMPLATE
$template->set_filenames(array('body' => 'invoice_m.tpl'));
$template->assign_vars  (array('BCRUMP' =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('invoice_m.'.$phpEx).'">My Invoice</a>',
								'U_INVOICE_M'=> append_sid('invoice_m.'.$phpEx.'?mode=show'),
								'JURUSAN'=>$ojur
								));
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
