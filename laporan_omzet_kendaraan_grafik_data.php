<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode

//DATA GRAFIK

$tanggal  	= isset($HTTP_GET_VARS['tgl'])? $HTTP_GET_VARS['tgl'] : $HTTP_POST_VARS['tgl'];
$no_polisi	= isset($HTTP_GET_VARS['no_polisi'])? $HTTP_GET_VARS['no_polisi'] : $HTTP_POST_VARS['no_polisi'];

$arr_tanggal	= explode("-",$tanggal);
$bulan	= $arr_tanggal[1];
$tahun	= $arr_tanggal[2];

//INISIALISASI
$data	= array();
$axis	= array();

if($mode=='bulanan'){
	//AMBIL HARI
	$sql=
		"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		$temp_hari	= $row['Hari'];
	}

	$sql=
		"SELECT WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,IS_NULL(COUNT(NoTiket),0) AS JumPnp
		FROM tbl_reservasi
		WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1 AND CetakSPJ=1 AND KodeKendaraan='$no_polisi'
		GROUP BY TglBerangkat
		ORDER BY TglBerangkat";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		
		for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
			if($row['Tanggal']==$idx_tgl+1){
				$axis[$idx_tgl]	= $row['Tanggal']."(".HariStringShort($row['Hari']).")";
				$data[$idx_tgl]	= $row['JumPnp']; 
				$row = $db->sql_fetchrow($result);
			}
			else{
				$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
				
				$axis[$idx_tgl]=$idx_tgl+1 ."(".HariStringShort($idx_str_hari).")";
				$data[$idx_tgl]=0;
			}
			
			$temp_hari++;
		}
			
	} 
	else{
		//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
		echo("Error:".__LINE__);exit;
	}
	
	$judul_grafik	="Grafik Produktifitas Kendaraan: ".$no_polisi." Bulan: ".BulanString($bulan)." $tahun";
	$legend	="Tanggal";
} 
elseif($mode=='tahunan'){

	$sql=
		"SELECT MONTH(TglBerangkat) AS bulan,IS_NULL(COUNT(NoTiket),0) AS JumPnp
		FROM tbl_reservasi
		WHERE YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1 AND CetakSPJ=1 AND KodeKendaraan='$no_polisi'
		GROUP BY MONTH(TglBerangkat)
		ORDER BY bulan";

	if ($result = $db->sql_query($sql)){
		$row = $db->sql_fetchrow($result);
		
		for($idx_bln=0;$idx_bln<12;$idx_bln++){
			if($row['bulan']==$idx_bln+1){
				$axis[$idx_bln]	= BulanString($idx_bln+1);
				$data[$idx_bln]	= $row['JumPnp']; 
				$row = $db->sql_fetchrow($result);
			}
			else{
				$axis[$idx_bln]=BulanString($idx_bln+1);
				$data[$idx_bln]=0;
			}
		}	
	} 
	else{
		//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
		echo("Error:".__LINE__);exit;
	}
	
	$judul_grafik	="Grafik Produktifitas Kendaraan: $no_polisi Tahun: $tahun";
	$legend	="Bulan";
} 

include_once( 'chart/php-ofc-library/open-flash-chart.php' );
$g = new graph();

$g->title($judul_grafik, '{font-size: 20px; color: #736AFF}' );

// we add 3 sets of data:
$g->set_data($data);

// we add the 3 line types and key labels
//$g->line( 2, '0x9933CC', 'Page views', 10 );
//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
$g->line_hollow( 2, 4, '0x80a033', 'Produktifitas', 10 );

$g->set_x_labels($axis);
$g->set_x_label_style( 10, '0x000000', 0, 2 );
$g->set_x_legend($legend, 12, '#736AFF' );

$max_value_y	= max($data);

$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50;

$g->set_y_max($max_value);
$g->y_label_steps(5);
$g->set_y_legend( 'Jumlah penumpang', 12, '#736AFF' );
echo $g->render();
?>