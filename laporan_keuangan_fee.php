<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassRekon.php');
//include($adp_root_path . 'ClassCabang.php');


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$bulan			= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

//INISIALISASI
$Rekon					= new Rekon();
$PengaturanUmum	= new PengaturanUmum();
$fee_transaksi	= $PengaturanUmum->ambilFeeTransaksi();

$mode	= $mode==""?"explore":$mode;

switch($mode){
	case 'explore':
		$bulan	=($bulan!='')?$bulan:date("m");
		$tahun	= ($tahun!='')?$tahun:date("Y");
		
		//LIST BULAN
		$list_bulan="";
		
		for($idx_bln=1;$idx_bln<=12;$idx_bln++){
			
			$font_size	= 2;
			$font_color='';
			
			if($bulan==$idx_bln){
				$font_size=4;
				$font_color='008609';
			}
			
			$list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
		}
		
		// LIST
		$template->set_filenames(array('body' => 'laporan_keuangan_fee/laporan_keuangan_fee_body.tpl')); 
		
		
		//AMBIL HARI
		$sql=
			"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";
		
		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);
			$temp_hari	= $row['Hari'];
		}
		
		//MENGAMBIL DATA DARI TABEL REKON
		$result_rekon	= $Rekon->ambilDataRekonPerBulan($bulan,$tahun);
		
		$list_tgl_sudah_rekon	= "";
		
		while ($row = $db->sql_fetchrow($result_rekon)){
			$list_tgl_sudah_rekon .=$row["Tanggal"].",";
			
			$data_rekon[$row['Tanggal']]["IdRekonData"]				=  $row["IdRekonData"];
			$data_rekon[$row['Tanggal']]["Hari"]							=  $row["Hari"];
			$data_rekon[$row['Tanggal']]["JumlahSPJ"]					=  $row["JumlahSPJ"];
			$data_rekon[$row['Tanggal']]["JumlahPenumpang"]		=  $row["JumlahPenumpang"];
			$data_rekon[$row['Tanggal']]["JumlahTiketOnline"]	=  $row["JumlahTiketOnline"];
			$data_rekon[$row['Tanggal']]["JumlahTiketBatal"]	=  $row["JumlahTiketBatal"];
			$data_rekon[$row['Tanggal']]["JumlahFeeTiket"]		=  $row["JumlahFeeTiket"];
			$data_rekon[$row['Tanggal']]["JumlahPaket"]				=  $row["JumlahPaket"];
			$data_rekon[$row['Tanggal']]["JumlahFeePaket"]		=  $row["JumlahFeePaket"];
			$data_rekon[$row['Tanggal']]["JumlahSMS"]					=  $row["JumlahSMS"];
			$data_rekon[$row['Tanggal']]["JumlahFeeSMS"]			=  $row["JumlahFeeSMS"];
			$data_rekon[$row['Tanggal']]["TotalFee"]					=  $row["TotalFee"];
			$data_rekon[$row['Tanggal']]["TotalDiskonFee"]		=  $row["TotalDiskonFee"];
			$data_rekon[$row['Tanggal']]["TotalBayarFee"]			=  $row["TotalBayarFee"];
			$data_rekon[$row['Tanggal']]["FlagDibayar"]				=  $row["FlagDibayar"];
			$data_rekon[$row['Tanggal']]["WaktuCatatBayar"]		=  $row["WaktuCatatBayar"];
		}
		
		//END MENGAMBIL DATA DARI TABEL REKON
		
		//FEE PENUMPANG
		$sql=
			"SELECT
				WEEKDAY(DATE(WaktuCetakTiket))+1 AS Hari,
				DAY(WaktuCetakTiket) AS Tanggal,
				COUNT(IF((FlagBatal!=1 OR FlagBatal IS NULL) AND CetakTiket=1,NoTiket,NULL)) Tiket,
				COUNT(IF((FlagBatal!=1 OR FlagBatal IS NULL) AND CetakTiket=1 AND PetugasCetakTiket=0,NoTiket,NULL)) TiketOnline,
				COUNT(IF(FlagBatal=1,NoTiket,NULL)) TiketBatal
			FROM tbl_reservasi
			WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1
			GROUP BY DATE(WaktuCetakTiket)
			ORDER BY DATE(WaktuCetakTiket)";
		
		if (!$result = $db->sql_query($sql)){
			//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		}
		
		$total_tiket_todate	= 0;
		
		$fee_tiket	= $Rekon->inisial_fee_tiket;
			
		while ($row = $db->sql_fetchrow($result)){
			$data_penumpang[$row['Tanggal']]["Hari"]							=  $row["Hari"];
			$data_penumpang[$row['Tanggal']]["TotalTiket"]				=  $row["Tiket"];
			$data_penumpang[$row['Tanggal']]["TotalTiketOnline"]	=  $row["TiketOnline"];
			$data_penumpang[$row['Tanggal']]["TotalTiketBatal"]		=  $row["TiketBatal"];
			
			$total_tiket_todate	+=$row["Tiket"];
			
			$data_penumpang[$row['Tanggal']]["TotalFeeTiket"]			=  $Rekon->hitungFee($total_tiket_todate,$fee_tiket)-$Rekon->hitungFee($total_tiket_todate-($row["Tiket"]-$row["TiketOnline"]),$fee_tiket);
		}
		
		// SPJ
		$sql=
			"SELECT 
				WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
				IS_NULL(COUNT(NoSPJ),0) AS TotalSPJ
			FROM tbl_spj
			WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND IsEkspedisi=0
			GROUP BY TglBerangkat
			ORDER BY TglBerangkat ";
		
		if (!$result = $db->sql_query($sql)){
			//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		}
		
		while ($row = $db->sql_fetchrow($result)){
			$data_spj[$row['Tanggal']]["Hari"]					=  $row["Hari"];
			$data_spj[$row['Tanggal']]["TotalSPJ"]			=  $row["TotalSPJ"];
		}
		
		//END MENGAMBIL TOTAL SPJ
		
		$sum_trip									= 0;
		$sum_tiket								= 0;
		$sum_tiket_batal					= 0;
		$sum_fee_tiket						= 0;
		$sum_total_fee						= 0;
		$jumlah_rekon_terhutang		= 0; 
		
		//perulangan mengambil data selama 1 bulan
		
		$jumlah_hari	= getMaxDate($bulan,$tahun);
		
		for($idx_tgl=1;$idx_tgl<=$jumlah_hari;$idx_tgl++){
			$odd ='odd';
			
			$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
			
			$tgl_transaksi	= $idx_tgl."-".HariStringShort($idx_str_hari)."";
			
			if (($idx_tgl % 2)==0){
				$odd = 'even';
			}
			
			if($idx_str_hari!=7){
				$font_color	= "000000";
			}
			else{
				$font_color = "ffffff";
				$odd	='red';
			}
			
			$total_trip					= $data_spj[$idx_tgl]['TotalSPJ'];
			$total_tiket				= $data_penumpang[$idx_tgl]['TotalTiket'];
			$total_tiket_online	= $data_penumpang[$idx_tgl]['TotalTiketOnline']; 
			$total_tiket_batal	= $data_penumpang[$idx_tgl]['TotalTiketBatal'];
			$total_fee_tiket		= $data_penumpang[$idx_tgl]['TotalFeeTiket'];
				
			$status_rekon				= "Belum Closing";
			
			//MEMPLOT DATA
			if($data_rekon[$idx_tgl]["Hari"]==""){
				$jumlah_rekon_terhutang	+= $total_fee_tiket;
			}
			else{
				//FEE PENUMPANG
				$total_tiket				= $data_rekon[$idx_tgl]['JumlahPenumpang'];
				$total_tiket_online	= $data_rekon[$idx_tgl]['JumlahTiketOnline'];
				$total_tiket_batal	= $data_rekon[$idx_tgl]['JumlahTiketBatal'];
				$total_fee_tiket		= $data_rekon[$idx_tgl]['JumlahFeeTiket'];
				$total_trip					= $data_rekon[$idx_tgl]['JumlahSPJ'];
				
				if ($data_rekon[$idx_tgl]['FlagDibayar']!="1"){
					$status_rekon				= "CLOSING";
					$odd								= "yellow";
					$jumlah_rekon_terhutang	+= $total_fee_tiket;	
				}
				else{
					$status_rekon				= "LUNAS";
					$odd								= "green";
				}
			}
			
			$total_tiket				= $total_tiket!=""?$total_tiket:0;
			$total_tiket_online	= $total_tiket_online!=""?$total_tiket_online:0;
			$total_tiket_batal	= $total_tiket_batal!=""?$total_tiket_batal:0;
			$total_tiket_fee		= $total_tiket_fee!=""?$total_tiket_fee:0;
			$total_fee_tiket		= $total_fee_tiket!=""?$total_fee_tiket:0;
			//END MEMPLOT DATA
			
			$sum_trip									+= $total_trip;
			$sum_tiket								+= $total_tiket;
			$sum_tiket_online					+= $total_tiket_online;
			$sum_tiket_batal					+= $total_tiket_batal;
			$sum_tiket_fee						+= $total_tiket_fee;
			$sum_fee_tiket						+= $total_fee_tiket;
			
			$check=$data_rekon[$idx_tgl]['IdRekonData']!="" && $data_rekon[$idx_tgl]['FlagDibayar']!=1?"<input type='checkbox' id='checked_$idx_tgl' name='checked_$idx_tgl' value=\"'".$data_rekon[$idx_tgl]['IdRekonData']."'\" onClick='this.checked=!this.checked;'/>":"";
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'idx_tgl'=>$idx_tgl,
						'check'=>$check,
						'font_color'=>$font_color,
						'tgl'=>$tgl_transaksi,
						'spj'=>number_format($total_trip,0,",","."),
						'tiket'=>number_format($total_tiket,0,",","."),
						'tiket_online'=>number_format($total_tiket_online,0,",","."),
						'tiket_batal'=>number_format($total_tiket_batal,0,",","."),
						'tiket_fee'=>number_format($total_tiket_fee,0,",","."),
						'total_fee'=>"Rp. ".number_format($total_fee_tiket,0,",","."),
						'status_rekon'=>$status_rekon
					)
				);
				
			$temp_hari++;
		}
					
		
		
		//$parameter	= "&sort_by=".$sort_by."&order=".$order;
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&p1=".$bulan."&p2=".$tahun;
															
		$script_cetak_excel="Start('laporan_keuangan_fee_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		//--END KOMPONEN UNTUK EXPORT
		
		$page_title	= "Rekap Fee Transaksi";
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('menu_lap_keuangan.'.$phpEx.'?top_menu_dipilih=top_menu_lap_keuangan') .'">Home</a> | <a href="'.append_sid('laporan_keuangan_fee.'.$phpEx).'">Laporan Rekap Fee Transaksi</a>',
			'URL'						=> append_sid('laporan_keuangan_fee.php'),
			'LIST_BULAN'		=> "| ".$list_bulan,
			'JUMLAH_HARI'		=> $jumlah_hari, 
			'BULAN'					=> $bulan,
			'TAHUN'					=> $tahun,
			'SUM_SPJ'				=>number_format($sum_trip,0,",","."),
			'SUM_TIKET'			=>number_format($sum_tiket,0,",","."),
			'SUM_TIKET_ONLINE'=>number_format($sum_tiket_online,0,",","."),
			'SUM_TIKET_BATAL'	=>number_format($sum_tiket_batal,0,",","."),
			'SUM_TIKET_FEE'	=>number_format($sum_tiket_fee,0,",","."),
			'SUM_TOTAL_FEE'	=>number_format($sum_fee_tiket,0,",","."),
			'U_LAPORAN_OMZET_GRAFIK'=>append_sid('laporan_keuangan_fee_grafik.'.$phpEx.'?bulan='.$bulan.'&tahun='.$tahun),
			'CETAK_XL'			=> $script_cetak_excel,
			'JUMLAH_REKON_TERHUTANG'	=> number_format($Rekon->ambilTotalRekonTerhutang(),0,",",".")
			)
		);
		
		
		include($adp_root_path . 'includes/page_header.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
	exit;
	
	case 'closerekon':
		// aksi close rekon
		$tgl_transaksi	= $HTTP_GET_VARS['tgl_transaksi'];
		$total_fee			= $HTTP_GET_VARS['total_fee'];
		
		$Rekon->closeRekon($tgl_transaksi,$fee_transaksi);
		
		exit;

	case 'bayarrekon':
		// aksi bayar rekon
		$id							= $HTTP_GET_VARS['id'];
	
		$Rekon->bayarRekon($id);
		
		exit;
	
	case 'bayarrekonall':
		$id = str_replace("\'","'",$HTTP_GET_VARS['id']);
	
		$Rekon->bayarRekonAll($id);
		exit;
}   

?>