<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || ($userdata['user_level']>$LEVEL_MANAJER  && $userdata['user_level']!=$LEVEL_CSO)){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'includes/page_header.php');

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

if($mode=='cso'){
	//CSO
	
	if ($submode == 'result'){
		
		$tanggal_mulai  = $HTTP_POST_VARS['tanggal_mulai'];
		
		if($tanggal_mulai!=''){
			
			$kondisi_tanggal="(waktu_cetak_tiket BETWEEN CONVERT(datetime,'$tanggal_mulai 00:00:00',105) AND CONVERT(datetime,'$tanggal_mulai 23:59:59',105))";
			$judul_ext="";
			$username=$userdata['username'];
			$link_back='<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('laporan_rekap_uang_cso.'.$phpEx."?mode=cso").'">Laporan Rekap Uang per CSO</a>';
		}
		else{
			$tanggal_mulai  = $HTTP_GET_VARS['tanggal_mulai'];
			$tanggal_akhir  = $HTTP_GET_VARS['tanggal_akhir'];
			$username				= $HTTP_GET_VARS['username'];
			$tipe_laporan		= $HTTP_GET_VARS['tipe_laporan'];
			$kondisi_tanggal="(waktu_cetak_tiket BETWEEN CONVERT(datetime,'$tanggal_mulai',105) AND CONVERT(datetime,'$tanggal_akhir',105))";
			$judul_ext=" hingga tanggal $tanggal_akhir";
			
			$link_back='<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('laporan_periode.'.$phpEx).'">Laporan</a> \ <a href="'.append_sid('laporan_periode.'.$phpEx.'?mode=cso&tipe_laporan='.$tipe_laporan).'">CSO</a>';
		}
		
		$sql="SELECT 
						CONVERT(varchar(12),TglBerangkat,106) as TglBerangkat,KodeJadwal,CONVERT(varchar(5),JamBerangkat,114) as JamBerangkat,CONVERT(varchar(5),JamPesan,114) as JamPesan,
						NoUrut,Nama0,Alamat0,telp0,HP0,PesanKursi,NomorKursi,
						HargaTiket,SubTotal,Discount,Total,jenis_discount
					FROM 
						TbReservasi
					WHERE 
						$kondisi_tanggal AND 
						CSO='$username' AND CetakTiket=1
						ORDER BY TglBerangkat,KodeJadwal,jamBerangkat";	
			
		if ($result = $db->sql_query($sql)){
			
			$i=1;
			$total_omzet=0;
			$total_kursi=0;
			$total_discount=0;
			$total_pendapatan=0;
			
			while ($row = $db->sql_fetchrow($result)){
				
				$template ->assign_block_vars(
					'ROW',
					array(
						'num'=>$i,
						'tgl_berangkat'=>$row['TglBerangkat'],
						'kode_jadwal'=>$row['KodeJadwal'],
						'jam_berangkat'=>$row['JamBerangkat'],
						'jam_pesan'=>$row['JamPesan'],
						'no_tiket'=>$row['NoUrut'],
						'nama'=>$row['Nama0'],
						'no_kursi'=>$row['NomorKursi'],
						'harga_tiket'=>number_format($row['HargaTiket'],0,',','.'),
						'pesan_kursi'=>$row['PesanKursi'],
						'sub_total'=>number_format($row['SubTotal'],0,',','.'),
						'discount'=>number_format($row['Discount'],0,',','.'),
						'total'=>number_format($row['Total'],0,',','.'),
						'jenis_discount'=>($row['jenis_discount']=='')?"&nbsp;":$row['jenis_discount']
						)
				);	
				
				$total_omzet+=$row['SubTotal'];
				$total_kursi+=$row['PesanKursi'];
				$total_discount+=$row['Discount'];
				$total_pendapatan+=$row['Total'];
				$i++;
				
			}
		}
		else{
			die_error('GAGAL MENGAMBIL DATA',__LINE__);
		}
			
		$template->set_filenames(array('body' => 'laporan_rekap_uang_cso_hasil_body.tpl'));
		$template->assign_vars (
			array(
				'BCRUMP'  				=>$link_back,
				'JUDUL'    				=> 'Laporan Rekap Uang Pada Tanggal '.$tanggal_mulai.$judul_ext."<br>Nama CSO:".$username,
				'TOTAL_OMZET'			=>"Rp.".number_format($total_omzet,0,',','.'),
				'TOTAL_KURSI'			=>number_format($total_kursi,0,',','.'),
				'TOTAL_DISCOUNT'	=>"Rp.".number_format($total_discount,0,',','.'),
				'TOTAL_PENDAPATAN'=>"Rp.".number_format($total_pendapatan,0,',','.'),
				)
		); 
		
	}
	else{
		$template->set_filenames(array('body' => 'laporan_rekap_uang_cso_body.tpl')); 
		
		$template->assign_vars (
		array(
			'BCRUMP'   =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('laporan_rekap_uang_cso.'.$phpEx.'?mode=cso').'">Laporan Rekap Uang per CSO</a>',
			'U_PERIODE_LAPOR' => append_sid('laporan_rekap_uang_cso.'.$phpEx.'?mode=cso&submode=result'),
			)
		);

	}	
}

$template->pparse('body');

include($adp_root_path . 'includes/page_tail.php');
?>