<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){  
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

//$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

// LIST
$template->set_filenames(array('body' => 'laporan.paket/paket.perjurusan.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

//$is_today				= $is_today==""?"1":$is_today;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeJurusan LIKE '$cari%'
		OR KodeCabangAsal LIKE '%$cari%'
		OR KodeCabangTujuan LIKE '%$cari%'
		OR KodeArea LIKE '$cari%'
		OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%'
		OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabangAsal'$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Jurusan":$sort_by;
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeJurusan","tbl_md_jurusan",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan.paket.perjurusan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		IdJurusan,KodeJurusan,
		CONCAT(f_cabang_get_name_by_kode(KodeCabangAsal),'-',f_cabang_get_name_by_kode(KodeCabangTujuan)) AS Jurusan
	FROM tbl_md_jurusan
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS OmzPaket
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan),f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//debug
//echo($sql);exit;

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){
	$temp_array[$idx]['IdJurusan']		= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']	= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']			= $row['Jurusan'];
	$temp_array[$idx]['TotalPaket']		= $data_paket_total[$row['IdJurusan']]['TotalPaket'];
	$temp_array[$idx]['OmzPaket']			= $data_paket_total[$row['IdJurusan']]['OmzPaket'];
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan.paket.detail.php?tglmulai='.$tanggal_mulai.'&tglakhir='.$tanggal_akhir.'&paramfilter='.$temp_array[$idx]['IdJurusan'].'&tipe=jurusan&ket='.$temp_array[$idx]['Jurusan'].'&is_today='.$is_today)."\");return false'>Detail<a/>";		
	
	/*$act 	.="<a href='".append_sid('laporan.paket.perjurusan_grafik.'.$phpEx).'&kode_cabang='.$temp_array[$idx]['KodeCabang'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&sort_by='.$sort_by.'&order='.$order."'>Grafik<a/>";		
	*/

	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total
	$total									= $temp_array[$idx]['Total'];
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'						=>$odd,
				'no'						=>$idx+1,
				'jurusan'				=>$temp_array[$idx]['Jurusan']." (".$temp_array[$idx]['KodeJurusan'].")",
				'total_paket'		=>number_format($temp_array[$idx]['TotalPaket'],0,",","."),
				'omz_paket'			=>number_format($temp_array[$idx]['OmzPaket'],0,",","."),
				'act'						=>$act
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&is_today=$is_today&tglawal=".$tanggal_mulai_mysql."&tglakhir=".$tanggal_akhir_mysql.
										"&cari=".$cari."&sortby=".$sort_by."&order=".$order."";
													
$script_cetak_excel="Start('laporan.paket.perjurusan.cetak.excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "is_today".($is_today==""?"1":$is_today);
$$temp_var	= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('menu_lap_paket.php?top_menu_dipilih=top_menu_lap_paket') .'">Home</a> | <a href="'.append_sid('laporan.paket.perjurusan.'.$phpEx).'">Laporan Omzet Paket per Jurusan</a>',
	'ACTION_CARI'		=> append_sid('laporan.paket.perjurusan.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'IS_TODAY1'			=> $is_today1,
	'IS_TODAY0'			=> $is_today0,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['Nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=Jurusan'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Jurusan ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=TotalPaket'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Total paket ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan.paket.perjurusan.'.$phpEx.'?sort_by=OmzPaket'.$parameter_sorting),
	'TIPS_SORT_3'	=> "Urutkan Total omzet paket ($order_invert)"
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>