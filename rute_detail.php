<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']>=$LEVEL_MANAJER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode    		= $HTTP_GET_VARS['mode'];
$act    		= $HTTP_GET_VARS['act'];
$submode 		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'blank';

//MEMERIKSA DUPLIKASI ----------------------------------------------------------------------------------------------------------------------------------------------------------------
function PeriksaDuplikasi($kode_rute){
  global $db;
  
	$duplikasi=false;
	
	$sql = 
		"SELECT COUNT(kode)
		FROM TbMDJadwal
		WHERE kode='$kode_rute'"; 
	
	$result 	= $db->sql_query($sql);
	$row	 		= $db->sql_fetchrow($result);
		
	if($row[0]>0){
		$duplikasi=true;
	}
			
  return $duplikasi;
}
//END MEMERIKSA DUPLIKASI  ----------------------------------------------------------------------------------------------------------------------------------------------------------------

//MENGAMBIL DATA SOPIR ----------------------------------------------------------------------------------------------------------------------------------------------------------------
function ambiListKendaraan($no_pol){
  global $db;
  	
	$sql = 
		"SELECT kode,jenis,merek
		FROM TbMDKendaraan
		ORDER BY kode";
	
	$opt = "";  
	
	if ($result = $db->sql_query($sql)){
		
		while ($row = $db->sql_fetchrow($result)){			
			$opt .= ($no_pol != $row[0]) ? 
				"<option value='$row[0]'>$row[0] $row[1] $row[2]</option>" : 
				"<option selected value='$row[0]'>$row[0] $row[1] $row[2]</option>";
    }    
  } 
	else{
		//die_error('Gagal mengambil data sopir',__LINE__,__FILE__,$sql);
		die_error('Gagal mengambil data kendaraan');
  }
			
  return $opt;
}
//END MENGAMBIL DATA SOPIR ----------------------------------------------------------------------------------------------------------------------------------------------------------------

// membuat option Asal
function OptionAsal($asal){
  global $db;
    $sql = "SELECT Nama,Kode
            FROM TbMDKota
						ORDER BY Nama";
						
  $opt = "";
  if (!$result = $db->sql_query($sql)){
		die_error('Cannot Load point');//,__LINE__,__FILE__,$sql);
  } 
	else{
		while ($row = $db->sql_fetchrow($result)){
      //$opt .= ($jurusan == $row[0]) ? "<option selected=selected value='$row[0]'>$row[1] - $row[2]</option>" : "<option value='$row[0]'>$row[1] - $row[2]</option>";
        $opt .= ($asal == $row[0]) ? "<option selected=selected value='$row[1]'>$row[0]</option>" : "<option value='$row[1]'>$row[0]</option>";
    }    
  }
	
	$opt ="<option value=''>-pilih satu-</option>".$opt;
	
  return $opt;
}

//MEMBUAT JAM  ----------------------------------------------------------------------------------------------------------------------------------------------------------------
function buatJam($jam_dipilih){
	
	//membuat combo jam------------------------------------------------------------------------------------------
	for($idx_jam=0;$idx_jam<=23;$idx_jam++){
		
		if($idx_jam<10){
			//jika digit jam kurang dari 2 digit, maka akan ditambahkan angka 0 didepannya
			$jam="0".$idx_jam;
			
			$opt_jam .=($jam!=$jam_dipilih)?
				"<option value='$jam'>$jam</option>"
				:"<option selected value='$jam'>$jam</option>";
		}
		else{
			//jika digit jam sudah 2 digit, maka tidak perlu ditambahkan angka 0 didepannya
			
			$opt_jam .=($idx_jam!=$jam_dipilih)?
				"<option value='$idx_jam'>$idx_jam</option>" 
				:"<option selected value='$idx_jam'>$idx_jam</option>";
		}
	} //END combo jam------------------------------------------------------------------------------------------
	
  return $opt_jam;
}
//END MEMBUAT JAM ----------------------------------------------------------------------------------------------------------------------------------------------------------------

//MEMBUAT MENIT  ----------------------------------------------------------------------------------------------------------------------------------------------------------------
function buatMenit($menit_dipilih){
 
	//membuat combo menit------------------------------------------------------------------------------------------
	for($idx_menit=0;$idx_menit<60;$idx_menit+=15){
		
		if($idx_menit>=10){
			//jika digit jam sudah 2 digit, maka tidak perlu ditambahkan angka 0 didepannya
			
			$opt_menit .=($idx_menit!=$jmenit_dipilih)?
				"<option value='$idx_menit'>$idx_menit</option>" 
				:"<option selected value='$idx_menit'>$idx_menit</option>";
		}
		else{
			//jika digit jam kurang dari 2 digit, maka akan ditambahkan angka 0 didepannya
			$menit="0".$idx_menit;
			
			$opt_menit .=($menit!=$menit_dipilih)?
				"<option value='$menit'>$menit</option>" 
				:"<option selected value='$menit'>$menit</option>";
			
		}
	} //END combo menit-------------------------------------------------------------------------------------------

  return $opt_menit;
}
//END MEMBUAT MENIT ----------------------------------------------------------------------------------------------------------------------------------------------------------------


switch($mode){
//MENAMPILKAN KOLOM ISIAN BLANK==========================================================================================================
case 'blank':
	$pesan="Penambahan";
	$kode_rute="Akan ditampilkan kemudian";
	
	//Membuat kombo jenis rute
	$opt_layout_kursi="
		<option selected value='10'>10</option>
		<option value='27'>27</option>
		<option value='45'>45</option>";
	
	//membuat kombo status aktif
	$opt_status_aktif="
		<option selected value='1'>Aktif</option>
		<option value='0'>Nonaktif</option>";
		
	
	//membuat combo nama sopir
	$opt_kendaraan=ambiListKendaraan("");	
	
	//membuat combo asal
	$opt_asal=OptionAsal("");	
	$opt_tujuan=OptionAsal("");
	$opt_jam=buatJam("08");
	$opt_menit=buatMenit("00");
	
	$template->set_filenames(array('body' => 'rute_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  =>$userdata['username'],
	   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('rute.'.$phpEx).'">Rute</a>',
	   	'U_ADD' =>'<a href="#" onClick="TampilkanDialogRute(\'\');">Tambah Rute</a>',
			'PESAN'	=>$pesan,
			'OPT_ASAL'	=>$opt_asal,
			'OPT_TUJUAN'	=>$opt_tujuan,
			'OPT_KENDARAAN'	=>$opt_kendaraan,
			'OPT_LAYOUT_KURSI'	=>$opt_layout_kursi,
			'OPT_JAM'	=>$opt_jam,
			'OPT_MENIT'	=>$opt_menit,
			'OPT_STATUS'	=>$opt_status_aktif
		));
break;

//TAMBAH MEMBER BARU ==========================================================================================================
case 'simpan_rute':
	
	//ambil isi dari parameter
	$submode					=$HTTP_GET_VARS['submode']; //Penambahan / Pengubahan
	$asal							=$HTTP_GET_VARS['asal'];
	$tujuan						=$HTTP_GET_VARS['tujuan'];
	$jam							=$HTTP_GET_VARS['jam'];
	$menit						=$HTTP_GET_VARS['menit'];
	$kode_rute				=$HTTP_GET_VARS['kode_rute'];
	$kode_rute_old		=$HTTP_GET_VARS['kode_rute_old'];
	$nopol						=$HTTP_GET_VARS['nopol'];
	$harga						=$HTTP_GET_VARS['harga'];
	$layout_kursi			=$HTTP_GET_VARS['layout_kursi'];
	$status_aktif			=$HTTP_GET_VARS['status_aktif'];
	$waktu						=$jam.":".$menit;
	
	if($kode_rute!=$kode_rute_old){
		if(PeriksaDuplikasi($kode_rute)){
			echo("duplikasi");
			exit;
		}
	}
	
	$sql = 
		"SELECT 
			jenis,merek,sopir
		FROM	TbMDKendaraan WHERE kode='$nopol'";
				
	if ($result = $db->sql_query($sql)){
		while ($row=$db->sql_fetchrow($result)){   
			$jenis_kendaraan	=$row['jenis'];
			$merek						=$row['merek'];
			$sopir						=$row['sopir'];
		}
	}
	else {
		die_error('Gagal mengambil data');//,__FILE__,__LINE__,$sql);
	}
	
	$sql = 
		"SELECT nama
		FROM	TbMDKota WHERE kode='$asal'";
				
	if ($result = $db->sql_query($sql)){
		while ($row=$db->sql_fetchrow($result)){   
			$asal		=$row['nama'];
		}
	}
	else {
		die_error('Gagal mengambil asal');//,__FILE__,__LINE__,$sql);
	}
	
	$sql = 
		"SELECT nama
		FROM	TbMDKota WHERE kode='$tujuan'";
				
	if ($result = $db->sql_query($sql)){
		while ($row=$db->sql_fetchrow($result)){   
			$tujuan		=$row['nama'];
		}
	}
	else {
		die_error('Gagal mengambil tujuan');//,__FILE__,__LINE__,$sql);
	}
	
	if($submode=="Penambahan"){
		//jika operasi yang dilakukan adalah penambahan data baru
		
		$sql = 
			"INSERT INTO TbMDJadwal(
				kode,aktif,asal,tujuan,jam,
				nopol,jenis,merek,NamaSopir,
				harga,kursi,siap
			)
			VALUES (
				'$kode_rute','$status_aktif','$asal','$tujuan','1899-12-30 $waktu',
				'$nopol','$jenis_kendaraan','$merek','$sopir',
				$harga,'$layout_kursi',1
			);";
	}
	else{
		//jika operasi yang dilakukan adalah pengubahan data
		
		$sql_point_asal		="(SELECT point_asal FROM tbl_master_point_rute WHERE kode_point_rute='$rute')";
		$sql_point_tujuan	="(SELECT point_tujuan FROM tbl_master_point_rute WHERE kode_point_rute='$rute')";
		
		$sql	= 
			"UPDATE TbMDJadwal
			SET 
				kode='$kode_rute',aktif='$status_aktif',asal='$asal',tujuan='$tujuan',jam='1899-12-30 $waktu',
				nopol='$nopol',jenis='$jenis_kendaraan',merek='$merek',NamaSopir='$sopir',
				harga=$harga,kursi='$layout_kursi'
			WHERE kode='$kode_rute_old'";
	}
	
	if (!$result = $db->sql_query($sql)){
		die_error('Gagal menyimpan data ',__FILE__,__LINE__,$sql);
	}
	
	echo("sukses");
	
exit;

//Ambil data anggota ==========================================================================================================
case 'ambil_data_rute':
	
	$kode_rute    = $HTTP_GET_VARS['kode'];
	
	$sql = 
		"SELECT 
			aktif,asal,tujuan,CONVERT(VARCHAR(10),jam,114) as jam,nopol,harga,kursi
		FROM	TbMDJadwal WHERE kode='$kode_rute'";
				
	if ($result = $db->sql_query($sql)){
		while ($row=$db->sql_fetchrow($result)){   
						
			$status				=$row['aktif'];
			$asal					=$row['asal'];
			$tujuan				=$row['tujuan'];
			$waktu				=explode(":",$row['jam']);
			$jam					=$waktu[0];
			$menit				=$waktu[1];
			$harga				=$row['harga'];
			$layout_kursi	=$row['kursi'];
			$nopol				=$row['nopol'];
		}
		
	}
	else {
		die_error('Gagal mengambil data');//,__FILE__,__LINE__,$sql);
	}
	
	$pesan="Pengubahan";
	
	
	//membuat combo nama sopir
	$opt_kendaraan=ambiListKendaraan("$nopol");	
	
	//membuat combo asal
	$opt_asal=OptionAsal("$asal");	
	$opt_tujuan=OptionAsal("$tujuan");
	$opt_jam=buatJam("$jam");
	$opt_menit=buatMenit("$menit");
	
	//option status aktif
	$temp_var="selected_status_aktif".$status;
	$$temp_var="selected";

	$opt_status_aktif="
		<option $selected_status_aktif1 value='1'>Aktif</option>
		<option $selected_status_aktif0 value='0'>Nonaktif</option>";
	
	//Membuat kombo layout kursi
	$temp_var="selected_layout".$layout_kursi;
	$$temp_var="selected";
	$opt_layout_kursi="
		<option $selected_layout10 value='10'>10</option>
		<option $selected_layout27 value='27'>27</option>
		<option $selected_layout45 value='45'>45</option>";
	
	$template->set_filenames(array('body' => 'rute_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  				=>$userdata['username'],
	   	'BCRUMP'    				=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('rute.'.$phpEx).'">Rute</a>',
			'PESAN'							=>$pesan,
			'OPT_ASAL'					=>$opt_asal,
			'OPT_TUJUAN'				=>$opt_tujuan,
			'OPT_JAM'						=>$opt_jam,
			'OPT_MENIT'					=>$opt_menit,
			'OPT_KENDARAAN'			=>$opt_kendaraan,
			'HARGA_TIKET'				=>$harga,
			'OPT_LAYOUT_KURSI'	=>$opt_layout_kursi,
			'KODE'							=>$kode_rute,
			'OPT_STATUS'				=>$opt_status_aktif
  ));
	
break;

//UBAH STATUS AKTIF ==========================================================================================================
case 'ubah_status_aktif':
	
	//ambil isi dari inputan
	$kode_rute	=$HTTP_GET_VARS['kode_rute'];
		
	$sql = 
		"UPDATE
			tbMDJadwal
		SET 
			aktif=1-aktif
		WHERE kode='$kode_rute'";
	
	if(!$result = $db->sql_query($sql)){
		//die_error('Gagal mengubah data anggota',__FILE__,__LINE__,$sql);
		die_error('Gagal mengubah status');
	}
	
exit;
} //switch mode

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>