<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['SPV_OPERASIONAL']))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode       = $mode==""?"exp":$mode;

$Cabang	= new Cabang();

function setComboTipeCabang($dipilih){
	
	$temp_var		= "select_".$dipilih;
	
	$$temp_var	= "selected";
	
	$opt_tipe_cabang	= "<option $select_0 value=0>CABANG</option>
											 <option $select_1 value=1>AGEN</option>";
											 
	return $opt_tipe_cabang;
}

switch($mode){
  case "exp":
    /* VIEW MODE*/

    $template->set_filenames(array('body' => 'cabang/cabang_body.tpl'));

    if($HTTP_POST_VARS["txt_cari"]!=""){
      $cari=$HTTP_POST_VARS["txt_cari"];
    }
    else{
      $cari=$HTTP_GET_VARS["cari"];
    }

    $sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
    $order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

    $order	=($order=='')?"ASC":$order;

    $sort_by =($sort_by=='')?"Nama":$sort_by;

    $kondisi	=($cari=="")?"":
      " WHERE KodeCabang LIKE '%$cari%'
				OR Nama LIKE '%$cari%'
				OR Alamat LIKE '%$cari%'
				OR Kota LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR Fax LIKE '%$cari%'";

    //PAGING======================================================
    $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
    $paging=pagingData($idx_page,"KodeCabang","tbl_md_cabang","&cari=$cari",$kondisi,"pengaturan_cabang.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
    //END PAGING======================================================

    $sql =
      "SELECT KodeCabang,Nama,Alamat,Kota,Telp,Fax,FlagAgen
			FROM tbl_md_cabang $kondisi
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

    $idx_check=0;


    if ($result = $db->sql_query($sql)){
      $i = $idx_page*$VIEW_PER_PAGE+1;
      while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
          $odd = 'even';
        }

        $idx_check++;

        $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[KodeCabang]'\"/>";

        $act 	="<a href='".append_sid('pengaturan_cabang.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
        $act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
        $template->
          assign_block_vars(
            'ROW',
            array(
              'odd'		=>$odd,
              'check'	=>$check,
              'no'		=>$i,
              'kode'	=>$row['KodeCabang'],
              'nama'	=>$row['Nama'],
              'alamat'=>$row['Alamat'],
              'kota'	=>$row['Kota'],
              'telp'	=>$row['Telp'],
              'fax'		=>$row['Fax'],
              'tipe_cabang'=>($row['FlagAgen']==0)?"CABANG":"AGEN",
              'action'=>$act
            )
          );

        $i++;
      }

      if($i-1<=0){
        $no_data	=	"<tr><td colspan=20 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
      }
    }
    else{
      //die_error('Cannot Load cabang',__FILE__,__LINE__,$sql);
      echo("Err :".__LINE__);exit;
    }

    //paramter sorting
    $order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
    $parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

    $array_sort	=
      "'".append_sid('pengaturan_cabang.php?sort_by=KodeCabang'.$parameter_sorting)."',".
      "'".append_sid('pengaturan_cabang.php?sort_by=Nama'.$parameter_sorting)."',".
      "'".append_sid('pengaturan_cabang.php?sort_by=Alamat'.$parameter_sorting)."',".
      "'".append_sid('pengaturan_cabang.php?sort_by=Kota'.$parameter_sorting)."',".
      "'".append_sid('pengaturan_cabang.php?sort_by=Telp'.$parameter_sorting)."',".
      "'".append_sid('pengaturan_cabang.php?sort_by=Fax'.$parameter_sorting)."',".
      "'".append_sid('pengaturan_cabang.php?sort_by=FlagAgen'.$parameter_sorting)."'";

    $page_title	= "Pengaturan Cabang";

    $template->assign_vars(array(
        'BCRUMP'    		=> '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a>',
        'U_CABANG_ADD'	=> append_sid('pengaturan_cabang.'.$phpEx.'?mode=add'),
        'ACTION_CARI'		=> append_sid('pengaturan_cabang.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'NO_DATA'				=> $no_data,
        'PAGING'				=> $paging,
        'ARRAY_SORT'		=> $array_sort
      )
    );
  break;

  case "add":
		/*TAMBAH DATA CABANG*/
    include($adp_root_path . 'ClassKota.php');

    $Kota   = new Kota();

		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'cabang/add_body.tpl')); 

    $page_title = "Tambah Data Cabang";
    $template->assign_vars(array(
		 'BCRUMP'		       => '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=add").'">Tambah Cabang</a> ',
		 'JUDUL'		       => 'Tambah Data Cabang',
		 'MODE'   	       => 'save',
		 'SUB'    	       => '0',
		 'OPT_KOTA'        => $Kota->setComboKota(),
		 'OPT_TIPE_CABANG' => setComboTipeCabang(0),
		 'PESAN'					 => $pesan,
		 'BGCOLOR_PESAN'	 => $bgcolor_pesan,
		 'U_ADD'	         => append_sid('pengaturan_cabang.'.$phpEx)
		 )
		);
	break;

  case "save":
		// aksi menambah cabang
    include($adp_root_path . 'ClassKota.php');

    $Kota   = new Kota();

    $kode  		= str_replace(" ","",$HTTP_POST_VARS['kode']);
		$kode_old	= str_replace(" ","",$HTTP_POST_VARS['kode_old']);
		$nama   	= $HTTP_POST_VARS['nama'];
		$alamat   = $HTTP_POST_VARS['alamat'];
		$kota			= $HTTP_POST_VARS['kota'];
		$telp   	= $HTTP_POST_VARS['telp'];
		$fax			= $HTTP_POST_VARS['fax'];
		$flag_agen= $HTTP_POST_VARS['tipe_cabang'];
		
		$terjadi_error=false;
		
		if($Cabang->periksaDuplikasi($kode) && $kode!=$kode_old){
			$pesan="<font color='white' size=3>Kode cabang yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Cabang";
				$path	='<a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=add").'">Tambah Cabang</a> ';
				
				if($Cabang->tambah($kode,$nama,$alamat,$kota,$telp,$fax,$flag_agen)){
						
					redirect(append_sid('pengaturan_cabang.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data cabang Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_cabang.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
				}
			}
			else{
				
				$judul="Ubah Data Cabang";
				$path	='<a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=edit&id=$kode_old").'">Ubah Cabang</a> ';
				
				if($Cabang->ubah($kode_old,$kode,$nama,$alamat,$kota,$telp,$fax,$flag_agen)){
						
						//redirect(append_sid('pengaturan_cabang.'.$phpEx.'?mode=add',true));
						//die_message('<h2>Data cabang Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_cabang.'.$phpEx.'?mode=edit&id='.$kode).'">Sini</a> Untuk Melanjutkan','');
						$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
						$bgcolor_pesan="98e46f";
				}
			}
			
			//exit;
			
		}
		
		$template->set_filenames(array('body' => 'cabang/add_body.tpl'));

    $page_title = "Ubah Data Cabang";
		$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a> | '.$path,
		 'JUDUL'		=>$judul,
		 'MODE'   	=> 'save',
		 'SUB'    	=> $submode,
		 'KODE_OLD' => $kode_old,
		 'KODE'    	=> $kode,
		 'NAMA'    	=> $nama,
		 'ALAMAT'   => $alamat,
		 'OPT_KOTA'	=> $Kota->setComboKota(),
		 'TELP'			=> $telp,
		 'FAX'			=> $fax,
		 'OPT_TIPE_CABANG' => setComboTipeCabang($flag_agen),
		 'PESAN'		=> $pesan,
		 'BGCOLOR_PESAN'=> $bgcolor_pesan,
		 'U_ADD'=>append_sid('pengaturan_cabang.'.$phpEx)
		 )
		);

	break;

  case "edit":
		// edit
    include($adp_root_path . 'ClassKota.php');

    $Kota   = new Kota();

		$id = $HTTP_GET_VARS['id'];
		
		$row=$Cabang->ambilDataDetail($id);
		
		$template->set_filenames(array('body' => 'cabang/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx).'">Cabang</a> | <a href="'.append_sid('pengaturan_cabang.'.$phpEx."?mode=edit&id=$id").'">Ubah Cabang</a> ',
			 'JUDUL'		=>'Ubah Data Cabang',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'KODE_OLD'	=> $row['KodeCabang'],
			 'KODE'    	=> $row['KodeCabang'],
			 'NAMA'    	=> $row['Nama'],
			 'ALAMAT'   => $row['Alamat'],
			 'OPT_KOTA'	=> $Kota->setComboKota($row['Kota']),
			 'TELP'			=> $row['Telp'],
			 'FAX'			=> $row['Fax'],
			 'OPT_TIPE_CABANG' => setComboTipeCabang($row['FlagAgen']),
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_ADD'=>append_sid('pengaturan_cabang.'.$phpEx)
			 )
		);
	break;

  case "delete":
		// aksi hapus cabang
		$list_cabang = str_replace("\'","'",$HTTP_GET_VARS['list_cabang']);
		//echo($list_cabang. " asli :".$HTTP_GET_VARS['list_cabang']);
		$Cabang->hapus($list_cabang);
		
	exit;
}

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>