<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassWebService.php');

function catatLog(
	$kode_booking,$switching_id,$bank_id,
	$payment_code,$status,$trax_type,$amount,$client_signature){
	
	global $db;
	
		$sql =
		"INSERT INTO tbl_pembayaran_online (
			KodeBooking,SwitchingId,IdBank,
			KodeReferensi,WaktuPembayaran,Status,
			TraxType,Amount,ClientSignature)
		VALUES(
			'$kode_booking','$switching_id','$bank_id',
			'$payment_code',NOW(),'$status',
			'$trax_type','$amount','$client_signature'
		)
	";
			
	if (!$db->sql_query($sql)){
		//jika error
	}	
}
$WebService	= new WebService();

//MENGAMBIL ISI DARI PARAMETER
$id_alur						= $HTTP_POST_VARS['id_alur'];	
$signature_receive	= $HTTP_POST_VARS['signature'];

//PERIKSA SIGNATURE
$my_signature				= $WebService->generateSignature($id_alur,"hiduplahindonesiaraya");

//echo("Receive: ".$signature_receive." | own:".$my_signature);

if($signature_receive!=$my_signature){
	//jika tidak sesuai, proses dihentikan dan akan dikirimkan kode error
	$status	= $WebService->$LIST_RESPONSE['MAL_FUNCTION'];
	//catatLog($kode_booking,$switching_id,$bank_id,$payment_code,$status,$trax_type,$amount,$client_signature);
	echo($status);
	exit;
}

switch ($id_alur){
	case 1:
		//GET DATA POINT ASAL
		echo($WebService->getPointAsal("pilihAsal(#id)"));
		
	exit;
	//END GET DATA POINT ASAL
	
	case 2:
		//GET DATA POINT TUJUAN
		$point_asal	= $HTTP_POST_VARS['point_asal'];
		
		echo($WebService->getPointTujuan("pilihTujuan(#id)",$point_asal));
		
	exit;
	//END GET DATA POINT TUJUAN
	
	case 3:
		//GET DATA POINT JADWAL
		$tgl_berangkat	= $HTTP_POST_VARS['tgl_berangkat'];
		$id_jurusan			= $HTTP_POST_VARS['id_jurusan'];
		$jumlah_pesan		= $HTTP_POST_VARS['jumlah_pesan'];
		
		echo($WebService->getJadwal("pilihJadwal(#id)",$tgl_berangkat,$id_jurusan,$jumlah_pesan));
	
	case 20:
		//GET CABANG DETAIL
		$kode_cabang		= $HTTP_POST_VARS['kode_cabang'];
		
		echo($WebService->getCabangDetail($kode_cabang));
		
	exit;
	//END GET CABANG DETAIL
	
	case 21:
		//GET CABANG ASAL DAN TUJUAN
		$id_jurusan		= $HTTP_POST_VARS['id_jurusan'];
		
		echo($WebService->getAsalTujuanDetail($id_jurusan));
	exit;
	//END GET CABANG  ASAL DAN TUJUAN
}
	
?>