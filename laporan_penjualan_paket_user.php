<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO_PAKET']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
$asal  					= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  				= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 

$tgl_awal					= ($tgl_awal!='')?$tgl_awal:dateD_M_Y();
$tgl_akhir				= ($tgl_akhir!='')?$tgl_akhir:dateD_M_Y();
$tgl_awal_mysql		= FormatTglToMySQLDate($tgl_awal);
$tgl_akhir_mysql	= FormatTglToMySQLDate($tgl_akhir);
//end pemgaturan periode

//pengaturan pemilihan tabel pencarian
$tbl_pencarian	= "tbl_paket";
//end pengatran pemilihan tabel pencarian

$template->set_filenames(array('body' => 'laporan.paket/paket.data.tpl')); 

$kondisi	= " WHERE (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND FlagBatal!=1 AND IdJurusan AND CetakTiket=1  AND PetugasPenjual='$userdata[user_id]'";

$kondisi	.= ($cari=="")?"":" AND (NoTiket LIKE '%$cari%' OR NamaPengirim LIKE '%$cari%' OR AlamatPengirim LIKE '%$cari%' OR TelpPengirim LIKE '%$cari%' OR NamaPenerima LIKE '%$cari%' OR AlamatPenerima LIKE '%$cari%' OR TelpPenerima LIKE '%$cari%' OR KodeJadwal LIKE '$cari%')";

if($asal==""){
	$kondisi	.= "";
}
else{
	$kondisi	.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) LIKE '$asal' ";
	
	if($tujuan!=""){
		$kondisi	.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) LIKE '$tujuan' ";
	}
}	

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoTiket",$tbl_pencarian,
						"&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&asal=$asal&tujuan=$tujuan&cari=$cari",
						$kondisi,"laporan.paket.data.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql = 
	"SELECT *
	FROM $tbl_pencarian
	$kondisi 
	ORDER BY TglBerangkat,JamBerangkat,KodeJadwal LIMIT $idx_awal_record,$VIEW_PER_PAGE";

$idx_check=0;


if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
	echo("Err: ".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;
	
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
					
	if (($i % 2)==0){
		$odd = 'even';
	}

	if($row['NoSPJ']==""){
		$status				= "&nbsp;Belum Dikirim&nbsp;";
		$style_status	= "background: red; color:white;";
	}
	else{

		if($row['IsSampai']==0 && $row['StatusDiambil']==0){
			$status				= "&nbsp;DIKIRIM&nbsp;";
			$style_status	= "background: blue;color:white;";
		}
		else{
		
			if($row['StatusDiambil']==0){
				$status="&nbsp;SAMPAI&nbsp;";
				$style_status	= "background: yellow;color:black;";
			}
			else{
				$status="&nbsp;DIAMBIL/DITERIMA&nbsp;";
				$style_status	= "background: green;color:white;";
			}
		}
	}

	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'							=>$odd,
				'no'							=>$i,
				'no_tiket'				=>$row['NoTiket'],
				'kode_jadwal'			=>$row['KodeJadwal'],
				'waktu_berangkat'	=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
				'nama_pengirim'		=>$row['NamaPengirim'],
				'alamat_pengirim'	=>$row['AlamatPengirim'],
				'telp_pengirim'		=>$row['TelpPengirim'],
				'nama_penerima'		=>$row['NamaPenerima'],
				'alamat_penerima'	=>$row['AlamatPenerima'],
				'telp_penerima'		=>$row['TelpPenerima'],
				'berat'						=>$row['Berat'],
				'harga'						=>number_format($row['HargaPaket'],0,",","."),
				'diskon'					=>number_format($row['Diskon'],0,",","."),
				'bayar'						=>number_format($row['TotalBayar'],0,",","."),
				'layanan'					=>$row['Layanan'],
				'jenis_bayar'			=>$row['JenisPembayaran']==0?"TUNAI":"LANGGANAN",
				'status'					=>$status,
				'style_status'		=>$style_status
			)
		);
	
	$i++;
}

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('laporan_penjualan_user.'.$phpEx).'">Laporan Penjualan User</a>',
	'ACTION_CARI'		=> append_sid('laporan_penjualan_paket_user.'.$phpEx),
	'CARI'					=> $cari,
	'TGL_AWAL'			=> $tgl_awal,
	'TGL_AKHIR'			=> $tgl_akhir,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging
	)
);
	      

include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>