<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SUPERVISOR"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

function setComboCabang($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	$Cabang	= new Cabang();
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	
	$opt_cabang	="<option value=''>- Semua Cabang -</option>".$opt_cabang;
	
	return $opt_cabang;
	//END SET COMBO CABANG
}

function hitungTotalKendaraanTerpakai($tanggal_mulai,$tanggal_akhir){
	global $db;

	$sql	= 
		"SELECT IS_NULL(COUNT(DISTINCT(NoPolisi)),0) AS JumlahKendaraan
		FROM tbl_spj
		WHERE TglBerangkat BETWEEN '$tanggal_mulai' AND '$tanggal_akhir'";
	
	if(!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}

	$row = $db->sql_fetchrow($result);

	return $row[0];
}


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_kendaraan/laporan_omzet_kendaraan_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?
	" WHERE KodeKendaraan LIKE '%'":
	" WHERE (KodeKendaraan LIKE '$cari%')";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tmk.KodeKendaraan":$sort_by;
	
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tmk.KodeKendaraan","tbl_md_kendaraan tmk",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&cabang=$kode_cabang&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_omzet_kendaraan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$temp_tgl	= explode("-",$tanggal_mulai);

$bulan	= $temp_tgl[1];
$tahun	= $temp_tgl[2];


//MENGAMBIL BIAYA-BIAYA DARI TABEL BIAYA
$sql =
	"SELECT NoPolisi,IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op 
	WHERE (TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	GROUP BY NoPolisi";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalBiaya"]	= $row["TotalBiaya"];
}

//MENGAMBIL JUMLAH JALAN DARI SPJ
$sql =
	"SELECT 
		NoPolisi,IS_NULL(COUNT(1),0) AS TotalJalan,
		COUNT(DISTINCT(TglBerangkat)) AS TotalHariKerja
	FROM tbl_spj 
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	GROUP BY NoPolisi";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalJalan"]			= $row["TotalJalan"];
	$data_laporan[$row[0]]["TotalHariKerja"]	= $row["TotalHariKerja"];
}

//MENGAMBIL JUMLAH PENUMPANG
$sql =
	"SELECT KodeKendaraan,
		IS_NULL(COUNT(1),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet
	FROM tbl_reservasi 
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY KodeKendaraan";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalPenumpang"]	= $row["TotalPenumpang"];
	$data_laporan[$row[0]]["TotalOmzet"]			= $row["TotalOmzet"];
}

//MENGAMBIL DATA KENDARAAN
$sql	= 
	"SELECT 
		KodeKendaraan,f_cabang_get_name_by_kode(KodeCabang) AS Cabang,
		JumlahKursi AS LayoutKursi
	FROM tbl_md_kendaraan $kondisi_cari";

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

//PLOT KE ARRAY PENAMPUNGAN
$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result)){

	$temp_array[$idx]['KodeKendaraan']	= $row['KodeKendaraan'];
	$temp_array[$idx]['LayoutKursi']		= $row['LayoutKursi'];
	$temp_array[$idx]['Cabang']					= $row['Cabang'];
	$temp_array[$idx]['TotalJalan']			= $data_laporan[$row['KodeKendaraan']]['TotalJalan'];
	$temp_array[$idx]['TotalPenumpang']	= $data_laporan[$row['KodeKendaraan']]['TotalPenumpang'];
	$temp_array[$idx]['TotalOmzet']			= $data_laporan[$row['KodeKendaraan']]['TotalOmzet'];
	$temp_array[$idx]['TotalBiaya']			= $data_laporan[$row['KodeKendaraan']]['TotalBiaya'];
	$temp_array[$idx]['TotalProfit']		= $data_laporan[$row['KodeKendaraan']]['TotalOmzet']-$data_laporan[$row['KodeKendaraan']]['TotalBiaya'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	//$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_omzet_kendaraan_grafik.'.$phpEx).'&tanggal='.$tanggal_mulai.'&no_polisi='.$row['KodeKendaraan']."\");return false'>Grafik<a/>";
	$act 	="<a href='".append_sid('laporan_omzet_kendaraan_grafik.php?no_polisi='.$temp_array[$idx]['KodeKendaraan'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$kode_cabang.'&sort_by='.$sort_by.'&order='.$order)."'>Grafik<a/>";
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'no_polisi'=>$temp_array[$idx]['KodeKendaraan'],
				'layout_kursi'=>substr($temp_array[$idx]['LayoutKursi'],0,2),
				'cabang'=>$temp_array[$idx]['Cabang'],
				'total_jalan'=>number_format($temp_array[$idx]['TotalJalan'],0,",","."),
				'total_penumpang'=>number_format($temp_array[$idx]['TotalPenumpang'],0,",","."),
				'total_omzet'=>number_format($temp_array[$idx]['TotalOmzet'],0,",","."),
				'total_biaya'=>number_format($temp_array[$idx]['TotalBiaya'],0,",","."),
				'total_profit'=>number_format($temp_array[$idx]['TotalProfit'],0,",","."),
				'act'=>$act
			)
		);
	
	$idx++;
}

$temp_var		= "select_".$sort_by;
$$temp_var	= "selected";

$opt_sort_by=
	"<option value='KodeKendaraan' $select_Kode>No.Polisi</option>
	<option value='Cabang'$select_Cabang>Cabang</option>
	<option value='Jalan' $select_Jalan>Total Jalan</option>
	<option value='TotalPenumpang' $select_TotalPenumpang>Total Penumpang</option>
	<option value='TotalOmzet' $select_TotalOmzet>Total Omzet</option>
	<option value='TotalBiaya' $select_TotalBiaya>Total Biaya</option>
	<option value='TotalProfit' $select_TotalProfit>Total Profit</option>
	<option value='produktifitas' $select_produktifitas>Produktifitas</option>";
	
$temp_var		= "select_".$order;
$$temp_var	= "selected";

$opt_order=
	"<option value='asc' $select_asc>Menaik</option>
	<option value='desc' $select_desc>Menurun</option>";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$kode_cabang.
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
													
$script_cetak_excel="Start('laporan_omzet_kendaraan_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
	
	
$page_title = "Penjualan/Kendaraan";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('menu_lap_reservasi.'.$phpEx.'?top_menu_dipilih=top_menu_lap_reservasi').'">Home</a> | <a href="'.append_sid('laporan_omzet_kendaraan.'.$phpEx).'">Laporan Omzet Kendaraan</a>',
	'ACTION_CARI'		=> append_sid('laporan_omzet_kendaraan.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'JUMLAH_KENDARAAN_DIPAKAI'	=>hitungTotalKendaraanTerpakai($tanggal_mulai_mysql,$tanggal_akhir_mysql),
	'OPT_CABANG'		=> setComboCabang($kode_cabang),
	'OPT_SORT'			=> $opt_sort_by,
	'OPT_ORDER'			=> $opt_order,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=KodeKendaraan'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Nama Sopir ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=LayoutKursi'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan NRP ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=Cabang'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan Total Hari Kerja ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=TotalJalan'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan Total trip ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=TotalPenumpang'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan Total penumpang ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=TotalOmzet'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Total Omzet ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=TotalBiaya'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Total Biaya ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_omzet_kendaraan.'.$phpEx.'?sort_by=TotalProfit'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Total Profit ($order_invert)"
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>