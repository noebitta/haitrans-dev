<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,400); 
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SUPERVISOR'],$USER_LEVEL_INDEX["CUSTOMER_CARE"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################
// HEADER

$page_title	= "Menu Pengelolaan Member";
$interface_menu_utama=true;

// TEMPLATE
switch($userdata['user_level']){
	case $USER_LEVEL_INDEX["CSO"]:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX["CSO_PAKET"]:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX["ADMIN"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_pengelolaan_member_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_tiketux',array());
		
		//Menu  pengelolaan member
		$template->assign_block_vars('menu_pengelolaan_member',array());
		//sub menu
		$template->assign_block_vars('menu_data_member',array());
		$template->assign_block_vars('menu_member_ultah',array());
		$template->assign_block_vars('menu_potensial_jadi_member',array());
		$template->assign_block_vars('menu_frekwensi_member',array('br'=>'100'));
		
		$template->assign_block_vars('menu_member_hampir_expired',array());
		//END pengelolaan member
		
		$template->assign_block_vars('menu_pengaturan',array());
		
	break;
	
	case $USER_LEVEL_INDEX["MANAJEMEN"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_pengelolaan_member_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_tiketux',array());
		
		//Menu  pengelolaan member
		$template->assign_block_vars('menu_pengelolaan_member',array());
		//sub menu
		$template->assign_block_vars('menu_data_member',array());
		$template->assign_block_vars('menu_member_ultah',array());
		$template->assign_block_vars('menu_potensial_jadi_member',array());
		$template->assign_block_vars('menu_frekwensi_member',array('br'=>'100'));
		
		$template->assign_block_vars('menu_member_hampir_expired',array());
		//END pengelolaan member
		
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX["MANAJER"] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_pengelolaan_member_body.tpl')); 
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		
		//Menu  pengelolaan member
		$template->assign_block_vars('menu_pengelolaan_member',array());
		//sub menu
		$template->assign_block_vars('menu_data_member',array());
		$template->assign_block_vars('menu_member_ultah',array());
		$template->assign_block_vars('menu_potensial_jadi_member',array());
		$template->assign_block_vars('menu_frekwensi_member',array('br'=>'100'));
		
		$template->assign_block_vars('menu_member_hampir_expired',array());
		//END pengelolaan member
		
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
}

$template->assign_vars(array
  ( 'BCRUMP'    				=> '<a href="'.append_sid('menu_pengelolaan_member.'.$phpEx.'?top_menu_dipilih=top_menu_pengelolaan_member') .'">Home',
    'U_MEMBER'					=>append_sid('pengaturan_member.'.$phpEx),
		'U_MEMBER_ULTAH'		=>append_sid('pengaturan_member_ultah.'.$phpEx),
		'U_MEMBER_CALON'		=>append_sid('pengaturan_member_calon_member.'.$phpEx),
		'U_MEMBER_FREKWENSI'=>append_sid('pengaturan_member_frekwensi.'.$phpEx),
		'U_MEMBER_HAMPIR_EXPIRED'=>append_sid('pengaturan_member_hampir_expired.'.$phpEx),
		'HEIGT_WRAPPER'			=>$height_wrapper
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>