<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern

if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true);
	exit;
}

//#############################################################################

class BeritaAcaraInsentifSopir{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function BeritaAcaraInsentifSopir(){
		$this->ID_FILE="C-BAIP";
	}
	
	//BODY
	
	function tambah(
		$IdPenjadwalan, $NominalInsentif, $KodeSopir,
		$Keterangan){
	  
		//kamus
		global $db;
		global $userdata;
		
		//MENGAMBIL DETAIL PENJADWALAN
		$sql = 
			"SELECT *
			FROM tbl_penjadwalan_kendaraan
			WHERE IdPenjadwalan='$IdPenjadwalan';";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err $this->ID_FILE :".__LINE__);exit;
		}
		
		$data_penjadwalan	= $db->sql_fetchrow($result);
		
		//MENGAMBIL DETAIL SOPIR
		$sql = 
			"SELECT Nama
			FROM tbl_md_sopir
			WHERE KodeSopir='$KodeSopir';";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err $this->ID_FILE :".__LINE__);exit;
		}
		
		$data_sopir	= $db->sql_fetchrow($result);
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"INSERT INTO tbl_biaya_insentif_sopir(
				IdPenjadwalan,TglBerangkat,JamBerangkat,
				IdJurusan,KodeJadwal,KodeSopir,
				NamaSopir,Keterangan,IdPembuat,
				NamaPembuat,WaktuBuat,IdApprover,
				NamaApprover,WaktuApprove,NominalInsentif)
			VALUES(
				'".$IdPenjadwalan."','".$data_penjadwalan['TglBerangkat']."','".$data_penjadwalan['JamBerangkat']."',
				'".$data_penjadwalan['IdJurusan']."','".$data_penjadwalan['KodeJadwal']."','".$KodeSopir."',
				'".$data_sopir['Nama']."','".$Keterangan."','".$userdata["user_id"]."',
				'".$userdata["nama"]."',NOW(),'".$userdata["user_id"]."',
				'".$userdata["nama"]."',NOW(),'".$NominalInsentif."');";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE :".__LINE__);
		}
		
		return true;
	}
	
	function ubah(
		$IdBA, $NominalInsentif, $KodeSopir,
		$Keterangan){
	  
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"UPDATE tbl_biaya_insentif_sopir
			SET
				KodeSopir='$KodeSopir',
				NominalInsentif='$NominalInsentif',
				Keterangan='$Keterangan'
				WHERE Id='$IdBA'";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE :".__LINE__);
		}
		
		return true;
	}
	
	function bayar(
		$IdBA, $IdReleaser, $NamaReleaser){
	  
		//kamus
		global $db;
		
		$expired_time = 120; //menit
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"UPDATE tbl_biaya_insentif_sopir
			SET
				IdReleaser='$IdReleaser',
				NamaReleaser='$NamaReleaser',
				WaktuRelease=NOW(),
				IsRelease=1
			WHERE Id='$IdBA'
				AND TIME_TO_SEC(TIMEDIFF(WaktuBuat,NOW()))/60>=-$expired_time";
								
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE :".__LINE__);
		}
		
		return true;
	}
	
	function ambilDetail($IdBA){
	  
		//kamus
		global $db;
		
		//MENGAMBIL DATA
		$sql = 
			"SELECT * FROM tbl_biaya_insentif_sopir WHERE Id='$IdBA' AND IsRelease";
								
		if (!$result=$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE :".__LINE__);
		}
		
		$row	= $db->sql_fetchrow($result);
		
		return $row;
	}
	
}
?>