<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,400); 
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################
// HEADER

$page_title	= "Menu Pengaturan";
$interface_menu_utama=true;
$template->set_filenames(array('body' => 'menu_pengaturan_body.tpl'));

// TEMPLATE
switch($userdata['user_level']){
	case $USER_LEVEL_INDEX["ADMIN"]: 
		$height_wrapper	= 220;
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		
		//Menu  pengaturan
		$template->assign_block_vars('menu_pengaturan',array());
		//sub menu
		$template->assign_block_vars('menu_pengaturan_kota',array());
		$template->assign_block_vars('menu_pengaturan_cabang',array());
		$template->assign_block_vars('menu_pengaturan_jurusan',array());
		$template->assign_block_vars('menu_pengaturan_jadwal',array('br'=>'100px'));
		$template->assign_block_vars('menu_pengaturan_umum',array());
		
		$template->assign_block_vars('menu_pengaturan_sopir',array());
		$template->assign_block_vars('menu_pengaturan_mobil',array());
		$template->assign_block_vars('menu_pengaturan_user',array());
		//$template->assign_block_vars('menu_pengaturan_macaddress',array());
		//END pengaturan
	break;
	
	case $USER_LEVEL_INDEX["MANAJEMEN"]:
		$height_wrapper	= 220;

		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		
		//Menu  pengaturan
		$template->assign_block_vars('menu_pengaturan',array());
		//sub menu
    $template->assign_block_vars('menu_pengaturan_kota',array());
    $template->assign_block_vars('menu_pengaturan_cabang',array());
		$template->assign_block_vars('menu_pengaturan_jurusan',array());
		$template->assign_block_vars('menu_pengaturan_jadwal',array('br'=>'100px'));
		$template->assign_block_vars('menu_pengaturan_umum',array());
		
		$template->assign_block_vars('menu_pengaturan_sopir',array());
		$template->assign_block_vars('menu_pengaturan_mobil',array());
		$template->assign_block_vars('menu_pengaturan_user',array());
		//$template->assign_block_vars('menu_pengaturan_macaddress',array());
		//END pengaturan
	break;
	
	case $USER_LEVEL_INDEX["MANAJER"]:
		$height_wrapper	= 220;
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		
		//Menu  pengaturan
		$template->assign_block_vars('menu_pengaturan',array());
		//sub menu
		$template->assign_block_vars('menu_pengaturan_cabang',array());
		$template->assign_block_vars('menu_pengaturan_jurusan',array());
		$template->assign_block_vars('menu_pengaturan_jadwal',array());
		$template->assign_block_vars('menu_pengaturan_sopir',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_pengaturan_mobil',array());
		//$template->assign_block_vars('menu_pengaturan_macaddress',array());
		//END pengaturan 
	break;
	
	case $USER_LEVEL_INDEX["SPV_OPERASIONAL"]:
		$height_wrapper	= 220;
		
		$template->assign_block_vars('menu_operasional',array());
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		//Menu  pengaturan
		$template->assign_block_vars('menu_pengaturan',array());
		//sub menu
		$template->assign_block_vars('menu_pengaturan_cabang',array());
		$template->assign_block_vars('menu_pengaturan_jurusan',array());
		$template->assign_block_vars('menu_pengaturan_jadwal',array());
		$template->assign_block_vars('menu_pengaturan_sopir',array('br'=>'100px'));
		
		$template->assign_block_vars('menu_pengaturan_mobil',array());

		//END pengaturan 
	break;
	
}

$template->assign_vars(array
  ( 'BCRUMP'    				=> '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home',
    'U_KOTA'					  =>append_sid('pengaturan_kota.'.$phpEx),
    'U_CABANG'					=>append_sid('pengaturan_cabang.'.$phpEx),
		'U_JURUSAN'					=>append_sid('pengaturan_jurusan.'.$phpEx),
    'U_JADWAL'					=>append_sid('pengaturan_jadwal.'.$phpEx),
		'U_PENGATURAN_UMUM'	=>append_sid('pengaturan_umum.'.$phpEx),
    'U_SOPIR'						=>append_sid('pengaturan_sopir.'.$phpEx),
    'U_MOBIL'						=>append_sid('pengaturan_mobil.'.$phpEx),
    'U_USER'						=>append_sid('pengaturan_user.'.$phpEx),
    'U_MACADDRESS'			=>append_sid('pengaturan_macaddress.'.$phpEx),
		'U_MASTER_ASURANSI'	=>append_sid('pengaturan_asuransi.'.$phpEx),
		'HEIGT_WRAPPER'			=>$height_wrapper
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>