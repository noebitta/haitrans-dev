<?php
  define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']>=$LEVEL_MANAJEMEN){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

redirect(append_sid('main.'.$phpEx),true); 

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo submode kosong, defaultnya EXplorer Mode
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

function OptionJurusan($jurusan){
  global $db;
  
  $sql = "SELECT kodejadwal,CONVERT(CHAR(20),TGLBerangkat,104),ID
                FROM         tbPosisi ORDER BY tglberangkat DESC";
	//$sql .=  "WHERE tglberangkat between CONVERT(datetime,'01-jun-2007',104) AND CONVERT(datetime,'01-dec-2007',104)";
  $opt = "";
  if (!$result = $db->sql_query($sql))
  {
    die_error('Cannot Load Jurusan',__LINE__,__FILE__,$sql);
  } else
  {
    while ($row = $db->sql_fetchrow($result))
    {
      //$opt .= ($jurusan == $row[0]) ? "<option selected=selected value='$row[0]'>$row[1] - $row[2]</option>" : "<option value='$row[0]'>$row[1] - $row[2]</option>";
        $opt .= ($jurusan == $row[2]) ? "<option>(none)</option>" : "<option selected=selected value='$row[2]'>$row[0] - $row[1]</option>";
    }    
  }
  $opt = "<option>(none)</option>" . $opt;
  return $opt;
}
	//$now = date();
 $ojur = OptionJurusan($jur);
 if ($mode=='show')
 	{
 		$rute = $HTTP_POST_VARS['rute'];
 		$sql = "SELECT T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,kodejadwal,convert(char(20),tglBerangkat,104)  
 				FROM tbposisi 
 				WHERE ID = '$rute'";	
 		if (!$result = $db->sql_query($sql))
 		{
 			die_error('Cannot Load data',__FILE__,__LINE__,$sql);
 		}
 		else
 		{
 			$row=$db->sql_fetchrow($result);
 			$rutes = $row[10];
 			$jams = $row[11];
 			for ($i=0;$i<10;$i++)
 			{
 				if ($row[$i] != ''){
 					$sqlx = "SELECT     Nama0
							FROM         TbReservasi
							WHERE     (NoUrut = '$row[$i]')";
 					if (!$resultx = $db->sql_query($sqlx))
 						{
 							die_error('Cannot Load data',__FILE__,__LINE__,$sqlx);	
 						}
 					else
 						{
 					    	$rowx = $db->sql_fetchrow($resultx);
 					    	$nama = $rowx[0];
 					    	$template->set_filenames(array('body' => 'keberangkatan.tpl')); 
							$template->assign_block_vars('ROW',array('odd'=>$odd,'no'=>'K'.($i+1),'nama'=>$nama));
 						}
					}
 			}
 		}
 	}
 
 $template->set_filenames(array('body' => 'keberangkatan.tpl')); 
 $template->assign_vars(array(
   'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('pengaturan.'.$phpEx).'">Pengaturan</a>\<a href="'.append_sid('keberangkatan.'.$phpEx).'">Keberangkatan</a>',
   'JURUSAN' => $ojur,
   'U_KEBERANGKATAN'=> append_sid('keberangkatan.'.$phpEx.'?mode=show'),
   'RUTE'=> "Rute ".$rutes ." Jam $jams"
   ));

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
