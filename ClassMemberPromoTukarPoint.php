<?php
class PromoPoin{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function PromoPoin(){
		$this->ID_FILE="C-006";
		$this->TABEL1="tbl_member_promo_tukar_point";
	}
	
	//BODY
	
	function tambah($kode_promo,$nama_penukaran_poin, $poin){
	  
		/*
		ID	: 001
		IS	: data promo belum ada dalam database
		FS	:Data promo baru telah disimpan dalam database 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO $this->TABEL1(
				kode_promo,nama_penukaran_poin, poin, status_aktif,dibuat_oleh,waktu_dibuat)
			VALUES(
				'$kode_promo','$nama_penukaran_poin', '$poin',1,'$useraktif', {fn NOW()});";
								
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 001");
		}
		
		return true;
	}
	
	function ambilData($order_by,$asc){
		
		/*
		ID	:002
		Desc	:Mengembalikan data promo sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":"ORDER BY kode_promo desc";
		
		$sql = 
			"SELECT 
				id_promo_poin,kode_promo,nama_penukaran_poin, poin,status_aktif,
				dibuat_oleh, CONVERT(CHAR(25),waktu_dibuat,103) as waktu_dibuat,
				diubah_oleh, CONVERT(CHAR(25),waktu_diubah,103) as waktu_diubah
			FROM $this->TABEL1
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 002");
		}
		
	}//  END ambilData
	
	function ubah($id_promo_poin,$nama_penukaran_poin,$poin){
	  
		/*
		ID	: 003
		IS	: data promo sudah ada dalam database
		FS	:Data promo diubah 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE $this->TABEL1
			SET 
				nama_penukaran_poin='$nama_penukaran_poin', poin='$poin', 
				diubah_oleh='$useraktif',waktu_diubah={fn NOW()}
			WHERE id_promo_poin='$id_promo_poin';";
								
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 003");
		}
		
		return true;
	}
	
	function hapus($id_promo_poin){
	  
		/*
		ID	: 004
		IS	: data promo sudah ada dalam database
		FS	:Data promo dihapus
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM $this->TABEL1
			WHERE id_promo_poin='$id_promo_poin';";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Gagal $this->ID_FILE 004");
		}
		
		return true;
	}//end hapus
	
	function ubahStatus($id_promo_poin){
	  
		/*
		ID	: 005
		IS	: data promo sudah ada dalam database
		FS	: Status promo diubah 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE $this->TABEL1
			SET 
				status_aktif=1-status_aktif,
				diubah_oleh='$useraktif',waktu_diubah={fn NOW()}
			WHERE id_promo_poin='$id_promo_poin';";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Gagal $this->ID_FILE 005");
		}
		
		return true;
	}//end ubahStatus
	
	function ambilDataDetail($id_promo_poin){
		
		/*
		ID	:006
		Desc	:Mengembalikan data promo sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM $this->TABEL1
			WHERE id_promo_poin='$id_promo_poin';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Gagal $this->ID_FILE 006");
		}
		
	}//  END ambilDataDetail
	
}
?>