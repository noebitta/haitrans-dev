<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

//$kode_jadwal_dipilih  = "GRG-STB0600";
//$kode_jadwal_dipilih  = "GRG-CKP0600";
//$kode_jadwal_dipilih  = "GRG-MLT0600";
//$kode_jadwal_dipilih  = "GRG-SCI0600";
$kode_jadwal_dipilih  = "MLT-STB-T0800";
//$kode_jadwal_dipilih  = "MLT-SCI0800";
//$kode_jadwal_dipilih  = "MLT-CKP0800";
//$kode_jadwal_dipilih  = "CKP-SCI10000";
//$kode_jadwal_dipilih  = "CKP-STB1000";
//$kode_jadwal_dipilih  = "SCI-STB1200";

$tgl_berangkat  = '2015-02-26';

$sql =
  "SELECT KodeJadwalUtama,KodeCabangAsal,KodeCabangTujuan,FlagSubJadwal
	FROM tbl_md_jadwal
	WHERE KodeJadwal='$kode_jadwal_dipilih'";

if (!$result = $db->sql_query($sql)){
  echo("Err: $sql");exit;
}

$data_jadwal_dipilih  = $db->sql_fetchrow($result);

$kode_jadwal_utama  = $data_jadwal_dipilih['FlagSubJadwal']==0?$kode_jadwal_dipilih:$data_jadwal_dipilih["KodeJadwalUtama"];

echo("Kode Jadwal Utama: $kode_jadwal_utama<br>");

//MENGAMBIL JADWAL UTAMA
$sql =
  "SELECT CONCAT(KodeCabangAsal,',',IF(Via='','',CONCAT(Via,',')),KodeCabangTujuan) AS Rute
	FROM tbl_md_jadwal
	WHERE KodeJadwal='$kode_jadwal_utama'";

if (!$result = $db->sql_query($sql)){
  echo("Err: $sql");exit;
}

$row = $db->sql_fetchrow($result);

echo("Rute: ".$row['Rute']."<br>");

$rute_arr = explode(",",$row['Rute']);

print_r($rute_arr);

echo("<br>");


$key_asal  = array_search($data_jadwal_dipilih["KodeCabangAsal"],$rute_arr);

echo("Kode Cabang Asal: $data_jadwal_dipilih[KodeCabangAsal] | In Key: ".$key_asal."<br>");

$key_tujuan  = array_search($data_jadwal_dipilih["KodeCabangTujuan"],$rute_arr);

echo("Kode Cabang Tujuan: $data_jadwal_dipilih[KodeCabangTujuan] | In Key: ".$key_tujuan."<br><br>");

echo("Kode Jadwal Dipilih: ".$kode_jadwal_dipilih."<br><br>");

$filter_head  = "'',";

for($i=0;$i<=$key_asal;$i++){
  $filter_head .="'".$rute_arr[$i]."',";
}

$filter_head  = substr($filter_head,0,-1);

$filter_tail  = "'',";

for($i=$key_tujuan;$i<count($rute_arr);$i++){
  $filter_tail .="'".$rute_arr[$i]."',";
}

$filter_tail  = substr($filter_tail,0,-1);


$sql =
  "SELECT KodeJadwal
	FROM tbl_md_jadwal
	WHERE (KodeJadwalUtama='$kode_jadwal_utama' OR KodeJadwal='$kode_jadwal_utama')
	  AND KodeCabangTujuan NOT IN ($filter_head) AND KodeCabangAsal NOT IN ($filter_tail) ORDER BY JamBerangkat";

if (!$result = $db->sql_query($sql)){
  echo("Err: $sql");exit;
}

while ($row = $db->sql_fetchrow($result)){
  echo("CEK: $row[KodeJadwal]<br>");
}

echo("<br>KURSI TERDETEKSI:<br>");

/*$sql =
  "SELECT NomorKursi,GROUP_CONCAT(KodeJadwal SEPARATOR ',') AS GrupKodeJadwal,IF(SUM(StatusKursi)<=0,0,1) AS GrupStatusKursi
	FROM tbl_posisi_detail
	WHERE (KodeJadwalUtama='$kode_jadwal_utama' OR KodeJadwal='$kode_jadwal_utama')
	  AND KodeCabangTujuan NOT IN ($filter_head) AND KodeCabangAsal NOT IN ($filter_tail)
	GROUP BY NomorKursi
	ORDER BY NomorKursi";
*/

$sql =
  "SELECT NomorKursi,IF(SUM(StatusKursi)<=0,0,1) AS GrupStatusKursi,NoTiket,Nama,Session,StatusBayar,KodeBooking
			FROM tbl_posisi_detail
			WHERE
				(KodeJadwalUtama='$kode_jadwal_utama' OR KodeJadwal='$kode_jadwal_utama')
	      AND KodeCabangTujuan NOT IN ($filter_head) AND KodeCabangAsal NOT IN ($filter_tail)
				AND TglBerangkat='$tgl_berangkat'
				AND ((HOUR(TIMEDIFF(SessionTime,NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW())))<=$SESSION_TIME_EXPIRED
				OR NoTiket!=''
				OR NoTiket IS NOT NULL)
			GROUP BY NomorKursi";

if (!$result = $db->sql_query($sql)){
  echo("Err: $sql");exit;
}

while ($row = $db->sql_fetchrow($result)){
  echo("$row[NomorKursi] | $row[GrupKodeJadwal] | $row[GrupStatusKursi] | $row[Nama]<br>");
}

?>