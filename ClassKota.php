<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Kota{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Kota(){
		$this->ID_FILE="C-KOT";
	}
	
	//BODY
	
	function periksaDuplikasi($KodeKota){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) AS JumlahData FROM tbl_md_kota WHERE KodeKota='$KodeKota'";
				
		if (!$result = $db->sql_query($sql)){
		  die_error("Err:$this->ID_FILE".__LINE__);
		}

    $row = $db->sql_fetchrow($result);

    $ditemukan = ($row['JumlahData']<=0)?false:true;

		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($KodeKota,$NamaKota){
	  /*INSERT DATA BARU*/

		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "INSERT INTO tbl_md_kota (KodeKota,NamaKota) VALUES('$KodeKota','$NamaKota')";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}

	function ubah($KodeKota,$NamaKota,$KodeKotaOld){
	  
		/*MENGUBAH DATA*/
		
		//kamus
		global $db;
		
		//MENGUBAH DATA DI DATABASE
		$sql ="UPDATE tbl_md_kota SET KodeKota='$KodeKota',NamaKota='$NamaKota' WHERE KodeKota='$KodeKotaOld'";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list){
	  
		/*
		ID	: 005
		IS	: data Cabang sudah ada dalam database
		FS	:Data Cabang dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_kota
			WHERE KodeKota IN($list);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus

  function ambilData($kondisi_tambahan=""){

    /*
    ID	:007
    Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
    */

    //kamus
    global $db;

    $kondisi_tambahan = $kondisi_tambahan==""?"":" AND ".$kondisi_tambahan;

    $sql =
      "SELECT *
			FROM tbl_md_kota
			WHERE 1 $kondisi_tambahan
			ORDER BY NamaKota;";

    if (!$result = $db->sql_query($sql,TRUE)){
      die_error("Err:$this->ID_FILE ".__LINE__);
    }

    return $result;

  }//  END ambilData

	function ambilDataDetail($KodeKota=""){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_kota
			WHERE KodeKota='$KodeKota';";
		
		if ($result = $db->sql_query($sql,TRUE)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$error	= $db->sql_error();
			die_error("Err:$this->ID_FILE ".__LINE__);
		}
		
	}//  END ambilData

  function setComboKota($KotaDipilih=""){
    /*MENGEMBALIKAN STRING OPTION KOTA */

    global $db;

    $sql  = "SELECT * FROM tbl_md_kota ORDER BY NamaKota ASC";

    if(!$result = $db->sql_query($sql)){
      echo("Err:".__LINE__);exit;
    }

    $opt_kota="";

    while($row=$db->sql_fetchrow($result)){
      $selected	=($KotaDipilih!=$row['NamaKota'])?"":"selected";
      $opt_kota .="<option value='$row[NamaKota]' $selected>$row[NamaKota]</option>";
    }

    return $opt_kota;
  }//END SET COMBO KOTA
}
?>