<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
//$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);



// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO_PAKET'],$USER_LEVEL_INDEX['KEUANGAN']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$id_setoran = isset($HTTP_GET_VARS['id'])? $HTTP_GET_VARS['id'] : $HTTP_POST_VARS['id'];
$cso  			= isset($HTTP_GET_VARS['cso'])? $HTTP_GET_VARS['cso'] : $HTTP_POST_VARS['cso'];


// LIST
$template->set_filenames(array('body' => 'laporan.rekap.setoran/detail.tpl')); 

if($id_setoran==""){
	//jika menampilkan detail transaksi yang belum setor
	$kondisi	= " (t1.IdSetoran='' OR t1.IdSetoran IS NULL)";
}
else{
	$kondisi	= " t1.IdSetoran='$id_setoran'";	
}

//QUERY TIKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,HargaTiket,0),SubTotal) AS HargaTiket,SubTotal,
		IF(JenisPembayaran!=3,Discount,0) AS Discount,IF(JenisPembayaran!=3,Total,0) AS Total,
		IF(JenisPenumpang!='R',JenisDiscount,'RETURN') AS JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM 
		tbl_reservasi t1
	WHERE $kondisi ".($cso!=""?"AND PetugasCetakTiket='$cso'":"")."
	ORDER BY WaktuCetakTiket DESC";	

if(!$result = $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}

$i = 0;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "TIKET";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	$i++;
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_pesan'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),
				'no_tiket'=>$row['NoTiket'],
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'kode_jadwal'=>$row['KodeJadwal'],
				'nama'=>$row['Nama'],
				'telp'=>$row['Telp'],
				'no_kursi'=>$row['NomorKursi'],
				'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
				'discount'=>number_format($row['Discount'],0,",","."),
				'total'=>number_format($row['Total']+$besar_premi,0,",","."),
				'tipe_discount'=>$row['JenisDiscount'],
				'cso'=>$row['NamaCSO'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
}

if($i<=0){
	$template->assign_block_vars('NO_DATA_TIKET',array());
}

//QUERY PAKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,NamaPengirim,
		NamaPenerima,HargaPaket,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO
	FROM 
		tbl_paket t1
	WHERE $kondisi ".($cso!=""?"AND PetugasPenjual='$cso'":"")."
	ORDER BY WaktuPesan DESC";	

if(!$result = $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}

$i = 0;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "RESI";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	$i++;	
	$template->
		assign_block_vars(
			'ROWPAKET',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'no_tiket'=>$row['NoTiket'],
				'kode_jadwal'=>$row['KodeJadwal'],
				'dari'=>$row['NamaPengirim'],
				'untuk'=>$row['NamaPenerima'],
				'harga_paket'=>number_format($row['HargaPaket'],0,",","."),
				'cso'=>$row['NamaCSO'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
	
	$i++;
}
if($i<=0){
	$template->assign_block_vars('NO_DATA_PAKET',array());
}
///QUERY BIAYA
$sql=
	"SELECT WaktuCatat,ts.KodeJadwal,ts.TglBerangkat,ts.JamBerangkat,t1.NoSPJ,FlagJenisBiaya,tms.Nama AS NamaSopir,t1.NoPolisi,t1.Jumlah,t1.Keterangan
	FROM 
		(tbl_biaya_op t1 LEFT JOIN tbl_spj ts ON t1.NoSPJ=ts.NoSPJ) LEFT JOIN tbl_md_sopir tms ON t1.KodeSopir=tms.KodeSopir
	WHERE $kondisi ".($cso!=""?"AND IdPetugas='$cso'":"")." AND FlagJenisBiaya!='$FLAG_BIAYA_VOUCHER_BBM' 
	ORDER BY WaktuCatat DESC";	

if(!$result = $db->sql_query($sql)){
	die_error('Query Error',__LINE__,"Err","");
}

$i = 0;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$i++;	
	$template->
		assign_block_vars(
			'ROWBIAYA',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_transaksi'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCatat'])),
				'kode_jadwal'=>$row['KodeJadwal'],
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'nospj'=>$row['NoSPJ'],
				'jenis_biaya'=>$LIST_JENIS_BIAYA[$row['FlagJenisBiaya']],
				'sopir'=>$row['NamaSopir'],
				'unit'=>$row['NoPolisi'],
				'jumlah'=>number_format($row['Jumlah'],0,",","."),
				'ket'=>$row['Keterangan']
			)
		);
	
}

if($i<=0){
	$template->assign_block_vars('NO_DATA_BIAYA',array());
}

include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>