<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'ClassCabang.php');

// PARAMETER
$perpage 		= $config['perpage'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$cabang  		= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];

// LIST
$template->set_filenames(array('body' => 'dashboardops/index.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$cabang		= $cabang==""?"CHP":$cabang;

//GET LIST CABANG
$Cabang	= new Cabang();
$data_cabang	= $Cabang->ambilDataDetail($cabang);

$show_dashboard_cabang	= "";

$idx	= 0 ;

$show_dashboard_cabang	.=
	"<table width='1300'>
		<tr><td colspan='2' style='font-size: 36px; text-align:center;'>$data_cabang[Nama]</td></tr>
		<tr>";

//SET JURUSAN BERANGKAT DARI CABANG DIPILIH
$sql 	= 
	"SELECT IdJurusan,KodeCabangTujuan 
	FROM tbl_md_jurusan 
	WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='".$data_cabang['KodeCabang']."' ORDER BY KodeCabangTujuan";

if(!$result=$db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$id_rewrite	= "rewrite_".$row['IdJurusan'];

	$show_dashboard_cabang .=
		"<td valign='top' align='center'>
			<span id='".$id_rewrite."_0'><img src='./templates/images/loading_bar.gif' /></span>
			<script type='text/javascript'>setDashboardCabang('".$row['IdJurusan']."','".$id_rewrite."_0',1);</script>
		</td>";

	$show_dashboard_cabang .="<tr><td colspan='2'><br></td></tr></tr>";
}

$page_title = "Dashboard Operasional";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('dashboardops.'.$phpEx).'">Dashboard Operasional</a>',
	'ACTION_CARI'		=> append_sid('dashboardops.'.$phpEx),
	'TGL_AWAL'			=> $tanggal_mulai,
	'LIST_DASHBOARD'=> $show_dashboard_cabang,
	'OPT_CABANG'		=> $Cabang->setInterfaceComboCabang($cabang)
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>