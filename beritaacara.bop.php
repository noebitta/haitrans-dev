<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraBOP.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassKota.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$expired_time = 120; //menit
// LIST
$template->set_filenames(array('body' => 'beritaacara.bop/index.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$BeritaAcaraBOP = new BeritaAcaraBOP();

$mode	= $mode==""?"explore":$mode;

switch($mode){
	case 'explore':
		$kondisi =	$cari==""?"":
			" AND (KodeJadwal LIKE '$cari%'
				OR KodeSopir LIKE '$cari%' 
				OR NamaSopir LIKE '%$cari%'
				OR Keterangan LIKE '%$cari%' 
				OR NamaPembuat LIKE '%$cari%'
				OR NamaReleaser LIKE '%$cari%')";
		
		$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan))='$kota'":"";
		$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan)='$asal'":"";
		$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tpk.IdJurusan)='$tujuan'":"";
		
		$order	=($order=='')?"DESC":$order;
			
		$sort_by =($sort_by=='')?"WaktuTransaksi":$sort_by;
		
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging		= pagingData($idx_page,"1","tbl_ba_bop",
		"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
		"WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"beritaacara.bop.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql	=
			"SELECT *,IF(TIME_TO_SEC(TIMEDIFF(WaktuTransaksi,NOW()))/60>=-$expired_time OR WaktuTransaksi IS NULL,0,1) AS IsExpired
			FROM tbl_ba_bop
			WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
			$kondisi
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";
		
			
		if(!$result = $db->sql_query($sql)){
			die_error("Gagal eksekusi query!",__LINE__, $this->ID_FILE,"");
		}
		
		$i=1;
		
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
				
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$show_header	= ($i%$config['repeatshowheader']!=0 && $i!=1)?"none":"";
			
			if($row['IsRelease']==1){
				$odd="green";
				$act= "RELEASED";
			}
			elseif($row['IsExpired']==0){
				$odd="yellow";
				$act= in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['KEUANGAN'])) && $row['IsRelease']==0?
					"<span class='b_edit' onClick=\"false;setDataBA('".$row['Id']."');\" title='ubah' >&nbsp;</span> | <span class='b_delete' onClick=\"false;hapus('".$row['Id']."');\" title='hapus' >&nbsp;</span>":"";
			}
			else{
				$odd="red";
				$act= "EXPIRED";
			}
			
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'no'=>$i+$idx_page*$VIEW_PER_PAGE,
						'tglberangkat'=>dateparse(FormatMySQLDateToTglWithTime($row['TglBerangkat'])),
						'kodebody'=>$row['KodeKendaraan'],
						'namasopir'=>$row['NamaSopir'],
						'kodejadwal'=>$row['KodeJadwal'],
						'jamberangkat'=>substr($row['JamBerangkat'],0,5),
						'nospj'=>$row['NoSPJ'],
						'waktubuat'=>dateparse(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])),
						'dibuatoleh'=>$row['NamaPembuat'],
						'jenisbiaya'=>$LIST_JENIS_BIAYA[$row['JenisBiaya']],
						'jumlah'=>"Rp.".number_format($row['Jumlah'],0,",","."),
						'keterangan'=>$row['Keterangan'],
						'releaser'=>$row['NamaReleaser']==""?"Belum Released":$row['NamaReleaser'],
						'waktureleased'=>$row['WaktuRelease']==""?"Belum Released":dateparse(FormatMySQLDateToTglWithTime($row['WaktuRelease'])),
						'act'=>$act,
					)
				);
			$i++;
		}
		
		if($i-1<=0){
			$template->assign_block_vars('ROW',array('showheader'=>''));
			$no_data	=	"<div style='width:100%;' class='yellow' align='center'><font size=3><b>data tidak ditemukan</b></font></div>";
		}
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&sort_by=$sort_by&order=$order&cari=".$cari."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=$kota&asal=$asal&tujuan=$tujuan";								
		$script_cetak_excel="Start('beritaacara.bop.cetakexcel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		//--END KOMPONEN UNTUK EXPORT
		
		//paramter sorting
		$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
		$parameter_sorting	= "&page=$idx_page&kota=$kota&asal=$asal&tujuan=$tujuan&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
		
		$array_sort	= 
			"'".append_sid('beritaacara.bop.php?sort_by=TglBerangkat'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NoPolisi'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NamaSopir'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=KodeJadwal'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=JamBerangkat'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NoSPJ'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=WaktuTransaksi'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NamaPembuat'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=JenisBiaya'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=Jumlah'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=Keterangan'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NamaReleaser'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=WaktuRelease'.$parameter_sorting)."'";

    $Kota = new Kota();

		$page_title	= "Berita Acara Biaya Operasional";
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('beritaacara.bop.'.$phpEx).'">Berita Acara Biaya Operasional</a>',
			'ACTION_CARI'		=> append_sid('beritaacara.bop.'.$phpEx),
			'PAGING'				=> $paging,
			'CETAK_XL'			=> $script_cetak_excel,
			'TGL_AWAL'			=> $tanggal_mulai,
			'TGL_AKHIR'			=> $tanggal_akhir,
			'OPT_KOTA'			=> $Kota->setComboKota($kota),
			'TXT_CARI'			=> $cari,
			'KOTA'					=> $kota,
			'ASAL'					=> $asal,
			'TUJUAN'				=> $tujuan,
			'ARRAY_SORT'		=> $array_sort,
			'NO_DATA'				=> $no_data,
			'JENIS_BIAYA_TOL'=>$FLAG_BIAYA_TAMBAHAN_TOL,
			'JENIS_BIAYA_BBM'=>$FLAG_BIAYA_TAMBAHAN_BBM,
			'JENIS_BIAYA_LAINNYA'=>$FLAG_BIAYA_TAMBAHAN_LAINNYA
			)
		);
						
		
		include($adp_root_path . 'includes/page_header.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
		
		exit;
	case 'getasal':
		$Cabang	= new Cabang();
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		$Cabang	= new Cabang();
		
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;
	
	case "showdialog":
		
		$kode_ba	=  $BeritaAcaraBOP->generateKodeBA();
		
		echo("
			proses_ok	= true;
			nospj.style.display='inline-block';
			shownospj.style.display='none';
			detailmanifest.style.display='none';
			showtombol1.style.display='block';
			showkodeba.innerHTML	= '$kode_ba';
			kodeba.value					= '$kode_ba';
			jumlah.value					= '';
			keterangan.value			= '';
			dlg_ba.show();
		");
	exit;
	
	case "getdatamanifest":
		// aksi menambah member
		
		$no_spj  		= $HTTP_POST_VARS['nospj'];
		
		$data_manifest		= $BeritaAcaraBOP->ambilDetailSPJ($no_spj);
		
		if($data_manifest['NoSPJ']==""){
			echo("proses_ok=true;alert('Manifest dengan kode ".strtoupper($no_spj)." tidak ditemukan!');");exit;
		}
		
		//membandingkan waktu keberangkatan dengan waktu cetak sekarang
		$waktu_sekarang	= date("Y-m-d H:i:s");
		$waktu_sekarang	= strtotime($waktu_sekarang);
		
		$waktu_berangkat= strtotime(substr($data_manifest['TglBerangkat'],0,10)." ".$data_manifest['JamBerangkat'].":00");
		$selisih  			= $waktu_sekarang - $waktu_berangkat;
		$selisih_menit	= round($selisih / 60); //menit
		
		if($selisih_menit>120){
			echo("proses_ok=true;alert('Transaksi tidak dapat dibuat 2 jam setelah waktu keberangkatan!');");exit;
		}
		
		$return_val	=
			"proses_ok=true;
			nospj.style.display='none';
			shownospj.style.display='inline-block';
			shownospj.innerHTML='".$data_manifest['NoSPJ']."';
			detailmanifest.style.display='block';
			tglberangkat.innerHTML='".FormatMySQLDateToTgl($data_manifest['TglBerangkat'])."';
			kodejadwal.innerHTML='".$data_manifest['KodeJadwal']."';
			jamberangkat.innerHTML='".$data_manifest['JamBerangkat']."';
			kodebody.innerHTML='".$data_manifest['NoPolisi']."';
			sopir.innerHTML='".$data_manifest['Driver']."';
			jumlah.value='';
			keterangan.value='';
			showtombol1.style.display='none';
			dlg_ba.show();";
		
		echo($return_val);
	exit;
	
	case 'simpan':
		
		//MENYIMPAN BERITA ACARA
		$id_ba			= $HTTP_POST_VARS["idba"];
		$kode_ba		= $HTTP_POST_VARS["kodeba"];
		$no_spj			= $HTTP_POST_VARS["nospj"];
		$jenis_biaya= $HTTP_POST_VARS["jenisbiaya"];
		$jumlah			= $HTTP_POST_VARS["jumlah"];
		$keterangan	= $HTTP_POST_VARS["keterangan"];
		
		if($id_ba==""){
			//berita acara baru
			
			$data_manifest	= $BeritaAcaraBOP->ambilDetailSPJ($no_spj);
			
			if($BeritaAcaraBOP->tambah($kode_ba,$jenis_biaya,$jumlah,
				$no_spj,$data_manifest['TglBerangkat'],$data_manifest['KodeJadwal'],
				$data_manifest['JamBerangkat'],$data_manifest['IdJurusan'],$data_manifest['NoPolisi'],
				$data_manifest['KodeDriver'],$data_manifest['Driver'],$keterangan)){
				
				echo("proses_ok=true;location.reload();");
			}
			else{
				echo("proses_ok=false;");
			}
		}
		else{
			$data_ba	= $BeritaAcaraBOP->ambilDetail($id_ba);
			
			if($data_ba["IsRelease"]==1){
				echo("proses_ok=true;alert('Biaya sudah direlease');");exit;
			}
			
			if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['KEUANGAN']))){
				//berita acara sudah disimpan hanya level tertentu bisa mengedit
				if($BeritaAcaraBOP->ubah(
					$id_ba, $jenis_biaya,$jumlah,
					$keterangan)){
					
					echo("proses_ok=true;location.reload();");	
				}
				else{
					echo("proses_ok=false;");
				}
			}
		}
		
	exit;
	
	case 'hapus':
		
		//MENYIMPAN BERITA ACARA
		$id_ba	= $HTTP_POST_VARS["idba"];
		
		$data_ba	= $BeritaAcaraBOP->ambilDetail($id_ba);
		
		if($data_ba["IsRelease"]==1){
			echo("proses_ok=true;alert('Biaya sudah direlease');");exit;
		}
			
		if($BeritaAcaraBOP->hapus($id_ba)){
			echo("proses_ok=true;location.reload();");	
		}
		else{
			echo("proses_ok=false;");
		}
		
	exit;
	
	case "getdataba":
		$id_ba			= $HTTP_POST_VARS["id"];
		
		$data_ba	=  $BeritaAcaraBOP->ambilDetail($id_ba);
		
		if($data_ba['NoSPJ']==""){
			echo("proses_ok=true;alert('Data tidak ditemukan!');");exit;
		}
		
		$return_val	=
			"proses_ok=true;
			showkodeba.innerHTML='".$data_ba['KodeBA']."';
			nospj.style.display='none';
			shownospj.style.display='inline-block';
			shownospj.innerHTML='".$data_ba['NoSPJ']."';
			detailmanifest.style.display='block';
			tglberangkat.innerHTML='".FormatMySQLDateToTgl($data_ba['TglBerangkat'])."';
			kodejadwal.innerHTML='".$data_ba['KodeJadwal']."';
			jamberangkat.innerHTML='".substr($data_ba['JamBerangkat'],0,5)."';
			kodebody.innerHTML='".$data_ba['KodeKendaraan']."';
			sopir.innerHTML='".$data_ba['NamaSopir']."';
			jumlah.value='".$data_ba['Jumlah']."';
			keterangan.value='".$data_ba['Keterangan']."';
			setJenisBiaya('".$data_ba['JenisBiaya']."');
			showtombol1.style.display='none';
			dlg_ba.show();";
		
		echo($return_val);
		
	exit;
}
?>