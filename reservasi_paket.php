<?php
//
// RESERVASI
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){
  if(!isset($HTTP_GET_VARS['mode'])){
    redirect(append_sid('index.'.$phpEx),true);
  }
  else{
    echo("Silahkan Login Ulang!");exit;
  }
}

include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassKota.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$user_id  	= $userdata['user_id'];
$user_level	= $userdata['user_level'];
$ukuran_bagasi_idx = array(1=>"Kecil",2=>"Sedang",3=>"Besar");

$page_title = "Paket";

// membuat option Asal
function OptionAsal($kota){
  global $db;

  $Cabang	= new Cabang();

  $result	= $Cabang->setComboCabang($kota);

  $opt = "";

  if ($result){
    $opt = "<option value=''>(none)</option>" . $opt;

    while ($row = $db->sql_fetchrow($result)){
      $opt .= "<option value='$row[0]'>$row[1]</option>";
    }
  }
  else{
    $opt .="<option selected=selected>Error</option>";
  }

  return $opt;
}

// membuat option jurusan
function OptionJurusan($asal){

  global $db;

  $Jurusan	= new Jurusan();

  $result = $Jurusan->setComboJurusan($asal,"1,2");

  $opt = "";
  if ($result){
    $opt = "<option value=''>(none)</option>" . $opt;

    while ($row = $db->sql_fetchrow($result)){
      $opt .= "<option value='$row[0]'>$row[1] ($row[2])</option>";
    }
  }
  else{
    $opt .="<option selected=selected>Error</option>";
  }

  return $opt;
}

// membuat option jam
function OptionJadwal($tgl,$id_jurusan){
  global $db;

  if ($id_jurusan!=''){

    $Jadwal	= new Jadwal();

    $result	= $Jadwal->setComboJadwal($tgl,$id_jurusan);

    $opt = "";

    if ($result){
      $opt = "<option>(none)</option>" . $opt;
      while ($row = $db->sql_fetchrow($result)){
        $opt .= "<option value='$row[0]'>$row[0]-$row[1]</option>";
      }
    }
    else{
      $opt .="<option selected=selected>Error</option>";
    }
  }

  return $opt;
}

//PEMILIHAN PROSES
switch ($mode){

// PERIKSA NO TELP ===========================================================================================================================================================================================
  case "periksa_no_telp":
    $no_telp 			= $HTTP_GET_VARS['no_telp'];
    $flag_paket 	= $HTTP_GET_VARS['paket'];
    $flag_pengirim= $HTTP_GET_VARS['flag'];

    include($adp_root_path . 'ClassPelanggan.php');

    $Pelanggan = new Pelanggan();

    $data_pelanggan=$Pelanggan->ambilDataDetail($no_telp);

    if(count($data_pelanggan)>1){
      if($flag_paket!=1){
        echo("
				document.getElementById('hdn_no_telp_old').value='".trim($data_pelanggan['NoTelp'])."';
				document.getElementById('fnama').value='".trim($data_pelanggan['Nama'])."';
				document.getElementById('ftelp').value='".trim($data_pelanggan['NoTelp'])."';
			");
      }
      else{
        if($flag_pengirim==1){
          echo("
					document.getElementById('nama_pengirim').value='".trim($data_pelanggan['Nama'])."';
					document.getElementById('alamat_pengirim').value='".trim($data_pelanggan['Alamat'])."';
					document.getElementById('telp_pengirim').value='".trim($data_pelanggan['NoTelp'])."';
				");
        }
        else{
          echo("
					document.getElementById('nama_penerima').value='".trim($data_pelanggan['Nama'])."';
					document.getElementById('alamat_penerima').value='".trim($data_pelanggan['Alamat'])."';
					document.getElementById('telp_penerima').value='".trim($data_pelanggan['NoTelp'])."';
				");
        }
      }
    }
    else{
      echo(0);
    }

    exit;

//Asal  ===========================================================================================================================================================================================
  case "asal":
    // membuat nilai awal..
    $kota	 = $HTTP_GET_VARS['kota_asal'];

    $oasal = OptionAsal($kota);

    echo("
	  <table width='100%'> 
		  <tr><td>POINT KEBERANGKATAN<br></td></tr>
	    <tr>
				<td><select class='form-control' onchange='getUpdateTujuan(this.value);' id='rute_asal' name='rute_asal'>$oasal</select><span id='progress_asal' style='display:none;'><img src='./templates/images/loading.gif' /></span>
				</td>
			</tr>
		</table>");

    exit;

//TGL ===========================================================================================================================================================================================
  case "tujuan":
    // membuat nilai awal..
    $asal=$HTTP_GET_VARS['asal'];

    $ojur = OptionJurusan($asal);

    echo("
	    <table>
				<tr><td>POINT TUJUAN<br></td></tr>
	      <tr>
					<td><select class='form-control' onchange='getUpdateJadwal(this.value);' id='tujuan' name='tujuan'>$ojur</select><span id='progress_tujuan' style='display:none;'><img src='./templates/images/loading.gif' /></span>
					</td>
				</tr>
			</table>");
    exit;

// JAM ===========================================================================================================================================================================================
  case "jam":
    // memilih jam yang tersedia sesuai dengan rute yang dimasukan
    $tgl 				= $HTTP_GET_VARS['tgl'];
    $id_jurusan = $HTTP_GET_VARS['id_jurusan'];
    $ojam 			= OptionJadwal($tgl,$id_jurusan);
    echo("
	<table width='100%'>
		<tr><td>JAM KEBERANGKATAN<br></td></tr>
	  <tr><td><select class='form-control' onchange='changeRuteJam();' name='p_jadwal' id='p_jadwal'>$ojam</select><span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span></td></tr>
  </table>");

    /*echo("
    <table bgcolor='white' width='100%'>
      <tr><td>JAM KEBERANGKATAN<br></td></tr>
      <tr><td>$ojam</td></tr>
    </table>");*/

    exit;

// PENGUMUMAN LAYOUT===========================================================================================================================================================================================
  case "pengumuman":
    // menampilkan pengumuman
    include_once($adp_root_path . 'ClassPengumuman.php');

    $Pengumuman	= new Pengumuman();

    if ($userdata['user_level'] != $LEVEL_SCHEDULER){
      //HANYA BISA DIAKSES OLEH SELAIN AGEN

      $jumlah_pengumuman	= $Pengumuman->ambilJumlahPengumumanBaru($userdata['user_id']);

      if($jumlah_pengumuman>0){
        $jumlah_pengumuman	= "<blink>".$jumlah_pengumuman."</blink>";
        $blink_Pengumuman		= "<blink>Pesan</blink>";
      }
      else{
        $jumlah_pengumuman	= "";
        $blink_Pengumuman		= "Pesan";
      }

      //$isi_pengumuman

      $baca_pengumuman	= "Start('".append_sid('pengumuman_inbox.'.$phpEx)."');getPengumuman();return false;";

      echo("
			<a class='menu' href='#' onClick=$baca_pengumuman>&nbsp;&nbsp;&nbsp;&nbsp;<font color='red' size=5><b>$jumlah_pengumuman</b></font>&nbsp;&nbsp;&nbsp;&nbsp;
			<br><br><br>
			$blink_Pengumuman</a>
		");

    }

    exit;
    break;

//==PAKET==
// PAKET LAYOUT===========================================================================================================================================================================================
  case "paketlayout":
    // menampilkan paket yang tersedia sesuai dengan tanggal, rute dan jam yang dimasukan
    include($adp_root_path . 'ClassPaketEkspedisi.php');
    include($adp_root_path . 'ClassPenjadwalanKendaraan.php');

    $Paket		= new Paket();
    $Jadwal		= new Jadwal();
    $PenjadwalanKendaraan	= new PenjadwalanKendaraan();
    $Reservasi= new Reservasi();

    $tgl  					= $HTTP_GET_VARS['tanggal']; // tanggal
    $kode_jadwal 		= $HTTP_GET_VARS['jadwal'];    // jam
    $mode_mutasi_on	= $HTTP_GET_VARS['modemutasion'];

    if($kode_jadwal=="" || $kode_jadwal=="(none)"){
      exit;
    }

    $row = $Jadwal->ambilDataDetail($kode_jadwal);
    $jam_berangkat	= $row['JamBerangkat'];

    //MEMERIKSA HAK AKSES, JIKA CSO BIASA TIDAK BOLEH MEMESAN PADA WAKTU YANG SUDAH LALU
    if(!$Reservasi->periksaHakAkses($tgl,$jam_berangkat) && $user_level>=$USER_LEVEL_INDEX['CSO_PAKET']){
      echo("<br><br><br><br><br><br>
				<img src='./templates/images/icon_warning.png' />
				<font color='red'><h3>Anda tidak boleh memilih waktu yang sudah lalu!</h3></font>");
      exit;
    }
    //--END PERIKSA HAK AKSES

    //LAYOUT PAKET
    $row = $Jadwal->ambilDataDetail($kode_jadwal);
    $jam_berangkat	= $row['JamBerangkat'];
    $jurusan				= $row["NamaAsal"]."-".$row["NamaTujuan"];

    if($row['FlagSubJadwal']!=1){
      $kode_jadwal_utama	= $kode_jadwal;
    }
    else{
      $kode_jadwal_utama	= $row['KodeJadwalUtama'];
    }

    //header layout
    //mengambil header dari layout kursi
    $row				= $Paket->ambilDataHeaderLayout($tgl,$kode_jadwal_utama);
    $id 				= $row['ID'];
    $no_spj			= $row['NoSPJ'];

    //MEMERIKSA AKTIF TIDAKNYA JADWAL
    $data_penjadwalan		= $PenjadwalanKendaraan->ambilDataDetail($tgl,$kode_jadwal_utama);

    if($no_spj==""){
      $status_aktif 	= $data_penjadwalan['StatusAktif'];

      if($mode_mutasi_on==1){
        $template->assign_block_vars('TOMBOL_MUTASIKAN',array());
      }

      $template->assign_block_vars('BELUM_BERANGKAT',array());
    }
    else{
      $kode_kendaraan	= $row['KodeKendaraan'];
      $no_polisi			= $row['NoPolisi'];
      $kode_sopir 		= $row['KodeDriver'];
      $nama_sopir 		= $row['Driver'];
      $tgl_spj 				= $row['TglSPJ'];

      if($mode_mutasi_on==1 && $userdata["user_level"]<=$USER_LEVEL_INDEX["SPV_RESERVASI"]){
        $template->assign_block_vars('TOMBOL_MUTASIKAN',array());
      }

      //manifest sudah dicetak
      $template->assign_block_vars(
        'DATA_MANIFEST',array(
          'nospj'						=> $no_spj,
          'body'						=> $kode_kendaraan,
          'sopir'						=> $nama_sopir,
          'kodesopir'				=> $kode_sopir,
          'waktucetak'			=> dateparseWithTime(FormatMySQLDateToTglWithTime($tgl_spj))
        )
      );
    }

    //MEMERIKSA AKTIF TIDAKNYA JADWAL
    $data_jadwal_utama	= $Jadwal->ambilDataDetail($kode_jadwal_utama);
    $flag_aktif					= $data_jadwal_utama['FlagAktif'];

    //MEMERIKSA AKTIF TIDAKNYA JADWAL
    if(($status_aktif==0 && $status_aktif!='') || ($flag_aktif==0 && $status_aktif=='')){
      if($userdata['user_level']>$USER_LEVEL_INDEX['SPV_OPERASIONAL']){
        echo("<br><br><br><br><br><br>
				<img src='./templates/images/icon_warning.png' />
				<font color='red'><h3>Jadwal ini tidak dioperasikan!</h3></font>");
        exit;
      }
      else{
        echo("<div align='center'><img src='./templates/images/icon_warning.png' />
				<font color='red' style='text-decoration:blink;'><h3>Jadwal ini tidak dioperasikan!</h3></font></div>");
      }
    }

    // Mengambil daftar paket-paket
    $daftar_paket	= "";
    $total_omzet	= 0;
    $total_pax		= 0;

    $list_kode_jadwal	= $Jadwal->ambilListKodeJadwalByKodeJadwalUtama($kode_jadwal_utama);

    if ($result = $Paket->ambilData("TglBerangkat='$tgl' AND (KodeJadwal IN($list_kode_jadwal)) AND FlagBatal!=1 ORDER BY WaktuPesan ASC")){

      while ($row = $db->sql_fetchrow($result)){
        $i++;
        $odd ='odd';

        if (($i % 2)==0){
          $odd = 'even';
        }

        $nama_pengirim_disingkat=substr($row['NamaPengirim'],0,20);
        $nama_penerima_disingkat=substr($row['NamaPenerima'],0,20);

        $lokasi_barang	= $Jadwal->ambilKodeCabangAsal($kode_jadwal)==$Jadwal->ambilKodeCabangAsal($row['KodeJadwal'])?"di loket ini":"";

        $total_omzet	+=$row['CetakTiket']?$row['TotalBayar']:0;
        $total_pax		+=$row['JumlahKoli'];

        $label_berat  = "Berat";
        $berat  = number_format($row['Berat'],0,",",".")." Kg";
        $is_volumetrik  = (!$row['IsVolumetrik']?"BERAT":"VOLUMETRIK");

        if($row['Dimensi']==3){
          //bagasi
          $label_berat  = "Ukuran";
          $berat= $ukuran_bagasi_idx[$row['Berat']];
          $is_volumetrik="UKURAN";
        }

        $template->assign_block_vars(
          'DAFTAR_BARANG',array(
            'no'						=> $i,
            'odd'						=> $odd,
            'resi'					=> $row['NoTiket'],
            'jenisbarang'		=> $LIST_JENIS_LAYANAN_PAKET[$row['Dimensi']],
            'jenislayanan'	=> ($row['JenisLayanan']=='P2P'?"Port to Port":"Port to Door "),
            'poinberangkat'	=> $lokasi_barang,
            'rute'					=> $row['Asal']."-".$row["Tujuan"],
            'nama_pengirim'	=> substr($row['NamaPengirim'],0,15),
            'nama_penerima'	=> substr($row['NamaPenerima'],0,15),
            'isvolumetrik'	=> $is_volumetrik,
            'dimensi'	      => (!$row['IsVolumetrik']?"":"<div class='resvcargolistdata'>Dimensi (P x L x T): $row[Panjang] cm X $row[Lebar] cm X $row[Tinggi] cm</div>"),
            'koli'					=> number_format($row['JumlahKoli'],0,",","."),
            'labelberat'    => $label_berat,
            'berat'					=> $berat,
            'biaya'					=> number_format($row['TotalBayar'],0,",",".")
          )
        );
      }
    }
    else{
      //die_error('Cannot Load paket',__LINE__,__FILE__,$sql);
      echo("Error :".__LINE__);exit;
    }

    //$total_omzet	= number_format($total_omzet,0,",",".");
    //$total_pax		= number_format($total_pax,0,",",".");

    if($i<=0){
      $template->assign_block_vars('NO_DATA',array());
    }
    else{
      $template->assign_block_vars('TOMBOL_CETAK_MANIFEST',array());
    }

    $template->assign_vars(array(
      'NO_SPJ'										=> $no_spj,
      'TANGGAL_BERANGKAT_DIPILIH' => dateparse(FormatMySQLDateToTgl($tgl))." ".$jam_berangkat,
      'POIN_BERANGKAT_DIPILIH'		=> $jurusan,
      'TOTAL_TRANSAKSI'						=> number_format($i,0,",","."),
      'TOTAL_PAX'									=> number_format($total_pax,0,",","."),
      'TOTAL_OMZET'								=> number_format($total_omzet,0,",",".")
    ));

    $template->set_filenames(array('body' => 'reservasi.paket/layout.tpl'));
    $template->pparse('body');

    exit;
    break;

//PAKET TAMBAH ===========================================================================================================================================================================================
  case "pakettambah":
    //echo("prosesok=true;");exit;
    include($adp_root_path . 'ClassPaketEkspedisi.php');
    $Reservasi	= new Reservasi();
    $Paket			= new Paket();
    $Jadwal			= new Jadwal();
    $Jurusan		= new Jurusan();

    // UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
    $tgl     				= $HTTP_GET_VARS['tanggal'];   // tanggal
    $kode_jadwal		= $HTTP_GET_VARS['kode_jadwal'];   // kode jadwal
    $nama_pengirim	= $HTTP_GET_VARS['nama_pengirim'];  // nama
    $alamat_pengirim= $HTTP_GET_VARS['alamat_pengirim']; //alamat
    $telepon_pengirim=$HTTP_GET_VARS['telepon_pengirim']; // telepon
    $nama_penerima	= $HTTP_GET_VARS['nama_penerima'];  // nama
    $alamat_penerima= $HTTP_GET_VARS['alamat_penerima']; //alamat
    $telepon_penerima=$HTTP_GET_VARS['telepon_penerima']; // telepon
    $keterangan			= $HTTP_GET_VARS['keterangan']; // telepon
    $jumlah_koli		= $HTTP_GET_VARS['jumlah_koli'];
    $berat					= $HTTP_GET_VARS['berat'];
    $dimensi				= $HTTP_GET_VARS['dimensi'];
    $is_volumetrik  = $HTTP_GET_VARS['isvolumetric'];
    $dimensi_p			= $HTTP_GET_VARS['dimensipanjang'];
    $dimensi_l			= $HTTP_GET_VARS['dimensilebar'];
    $dimensi_t			= $HTTP_GET_VARS['dimensitinggi'];
    $jenis_layanan	= $HTTP_GET_VARS['jenis_layanan'];
    $cara_bayar			= $HTTP_GET_VARS['cara_bayar'];

    $kode_booking 	= generateNoTiketPaket();
    $no_spj					= $HTTP_GET_VARS['no_spj'];

    $useraktif=$userdata['user_id'];

    //memgambil data rute dan jadwal
    $row 					= $Jadwal->ambilDataDetail($kode_jadwal);
    $jam_berangkat= $row['JamBerangkat'];
    $id_jurusan 	= $row['IdJurusan'];
    $flag_sub_jadwal= $row['FlagSubJadwal'];
    $kode_jadwal_utama= $row['KodeJadwalUtama'];
    $kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;

    //UPDATE TGL 29-02-2016 UNTUK HAITRANS HARGA PAKET DI INPUT MANUAL CSO
    //hitung harga paket
    //$arr_harga_paket= $Paket->ambilHargaPaketByKodeLayanan($id_jurusan,$dimensi);
    //$harga_paket		= ($arr_harga_paket[0]+$arr_harga_paket[1]*($berat-1));
    $harga_paket = $HTTP_GET_VARS['harga_paket'];
    //mengambil 	data header tb posisi
    $row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_aktual);
    $kode_kendaraan	= $row['KodeKendaraan'];
    $kode_sopir			= $row['KodeSopir'];
    $tgl_cetak_SPJ	= $row['TglCetakSPJ'];
    $petugas_cetak_spj= $row['PetugasCetakSPJ'];
    $cetak_spj			= ($no_spj=="")?0:1;

    //mengambil data-data kode account dan komisi
    $row												= $Jurusan->ambilDataDetail($id_jurusan);
    $kode_cabang								= $userdata['KodeCabang'];
    $komisi_paket_CSO						= $row['KomisiPaketCSO'];
    $komisi_paket_sopir					= $row['KomisiPaketSopir'];
    $kode_akun_pendapatan				= $row['KodeAkunPendapatanPaket'];
    $kode_akun_komisi_paket_CSO	= $row['KodeAkunKomisiPaketCSO'];
    $kode_akun_komisi_paket_sopir	= $row['KodeAkunKomisiPaketSopir'];

    $Paket->tambah(
      $kode_booking, $kode_cabang, $kode_jadwal,
      $id_jurusan, $kode_kendaraan , $kode_sopir ,
      $tgl, $jam_berangkat , $nama_pengirim ,
      $alamat_pengirim, $telepon_pengirim,
      $nama_penerima, $alamat_penerima, $telepon_penerima,
      $harga_paket,0,$harga_paket,
      $keterangan, $userdata['user_id'],
      $komisi_paket_CSO, $komisi_paket_sopir,
      $kode_akun_pendapatan, $kode_akun_komisi_paket_CSO, $kode_akun_komisi_paket_sopir,
      $jumlah_koli,$berat,$dimensi,
      $is_volumetrik,$dimensi_p,$dimensi_l,
      $dimensi_t,$jenis_layanan,$cara_bayar);

    echo("prosesok=true;document.getElementById('notiketdicetak').value='$kode_booking';");

    exit;

//PAKET BATAL ===========================================================================================================================================================================================
  case "paketbatal":
    include($adp_root_path . 'ClassPaketEkspedisi.php');
    $Paket			= new Paket();

    $no_tiket	= $HTTP_GET_VARS['no_tiket'];// nourut

    //periksa wewenang
    if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SUPERVISOR']))){
      $Paket->pembatalan($no_tiket, $userdata['user_id']);
      echo(1);
    }
    else{
      echo(0);
    }

    exit;


// PAKET DETAIL  ===========================================================================================================================================================================================
  case "paketdetail":
    //operasi terhadap paket
    include($adp_root_path . 'ClassPaketEkspedisi.php');
    $Paket			= new Paket();

    $no_tiket = $HTTP_GET_VARS['no_tiket'];
    $submode  = $HTTP_GET_VARS['submode'];

    if ($userdata['user_level'] == $USER_LEVEL_INDEX['SCHEDULER']){
      exit;
    }

    $row	= $Paket->ambilDataDetail($no_tiket);

    $harga_paket	=number_format($row['HargaPaket'],0,",",".");

    if($submode=='ambil'){
      echo("
			<input type='hidden' id='dlg_ambil_paket_no_tiket' value='$row[NoTiket]'/>
			<input type='hidden' id='hdn_paket_cara_pembayaran' value='$row[CaraPembayaran]' />
			<table cellspacing=0 cellpadding=0 width='100%' height='100%'>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Pengirim</h2></td></tr>
						<tr><td>Telp Pengirim </td><td>:</td><td>$row[TelpPengirim]</td></tr>
						<tr><td width=200>Nama Pengirim</td><td width=5>:</td><td>$row[NamaPengirim]</td></tr>
						<tr><td>Alamat Pengirim</td><td>:</td><td>$row[AlamatPengirim]</td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Penerima</h2></td></tr>
						<tr><td>Telp Penerima </td><td>:</td><td>$row[TelpPenerima]</td></tr>
						<tr><td width=200>Nama Penerima </td><td width=5>:</td><td>$row[NamaPenerima]</td></tr>
						<tr><td>Alamat Penerima</td><td>:</td><td>$row[AlamatPenerima]</td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Paket</h2></td></tr>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td width='50%'>Jumlah Koli</td><td>:</td><td>".number_format($row['JumlahKoli'],0,",",".")."Pax</td></tr>
						<tr><td>Jenis Barang</td><td>:</td><td>$row[JenisBarang]</td></tr>
						<tr><td>Layanan</td><td>:</td><td>".$LIST_JENIS_LAYANAN_PAKET[$row['Layanan']]."</td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td>Harga</td><td>:</td><td>Rp. $harga_paket</td></tr>
						<tr><td>Cara Pembayaran</td><td>:</td><td>".$LIST_JENIS_PEMBAYARAN_PAKET[$row['CaraPembayaran']]."</td></tr>
						<tr><td valign='top'>Keterangan</td><td valign='top'>:</td><td>$row[KeteranganPaket]</td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Pengambil</h2></td></tr>
			<tr>
				<td width='50%' valign='top' colspan=2>
					<table>
						<tr>
							<td width='25%'>Nama Pengambil <span id='dlg_ambil_paket_nama_pengambil_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input type='text' id='dlg_ambil_paket_nama_pengambil' maxlength=20 onFocus='Element.hide(\"dlg_ambil_paket_nama_pengambil_invalid');\" /></td><td width=10></td>
							<td width='25%'>No. KTP <span id='dlg_ambil_paket_no_ktp_pengambil_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input type='text' id='dlg_ambil_paket_no_ktp_pengambil' maxlength=20 onFocus='Element.hide(\"dlg_ambil_paket_no_ktp_pengambil_invalid');\" /></td></tr>
					</table>
				</td>
			</tr>
		</table>
		");
      exit;
    }

    if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI']))){
      $action_batal		= "batalPaket('$row[NoTiket]');";
      $template->assign_block_vars("TOMBOL_MUTASI",array("action"=>"setFlagMutasiPaket();"));
    }
    else{
      $action_batal		= "";
    }

    $label_berat   ="Berat";
    $berat  = number_format($row['Berat'],0,",",".")." Kg";

    if($row['Dimensi']==3){
      //bagasi
      $label_berat  = "Ukuran";
      $berat= $ukuran_bagasi_idx[$row['Berat']];
    }

    $template->assign_vars(array(
      'RESI'						=> $row['NoTiket'],
      'KODE_JADWAL'			=> $row['KodeJadwal'],
      'TELP_PENGIRIM'		=> $row['TelpPengirim'],
      'NAMA_PENGIRIM'		=> $row['NamaPengirim'],
      'ALAMAT_PENGIRIM'	=> $row['AlamatPengirim'],
      'TELP_PENERIMA'		=> $row['TelpPenerima'],
      'NAMA_PENERIMA'		=> $row['NamaPenerima'],
      'ALAMAT_PENERIMA'	=> $row['AlamatPenerima'],
      'ASAL'						=> $row['NamaAsal'],
      'TUJUAN'					=> $row['NamaTujuan'],
      'JENIS_BARANG'		=> strtoupper($LIST_JENIS_LAYANAN_PAKET[$row['Dimensi']]),
      'LAYANAN'					=> ($row['JenisLayanan']=='P2P'?"Port to Port":"Port to Door "),
      'IS_VOLUMETRIK'		=> ($row['IsVolumetrik']==0?"BERAT":"VOLUMETRIK"),
      'KOLI'						=> number_format($row['JumlahKoli'],0,",","."),
      'LABEL_BERAT'			=> $label_berat,
      'BERAT'						=> $berat,
      'BIAYA_KIRIM'			=> number_format($row['HargaPaket'],0,",","."),
      'DESKRIPSI_BARANG'=> $row['KeteranganPaket'],
      'CSO'							=> $row['NamaCSO'],
      'WAKTU_TERIMA'		=> dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
      'ACTION_BATAL'		=> $action_batal
    ));

    if($row['IsVolumetrik']){
      $template->assign_block_vars("VOLUMETRIK",array("dimensi"=>$row["Panjang"]." cm X ".$row["Lebar"]." cm X ".$row["Tinggi"]." cm"));
    }

    if($row['FlagMutasi']==1){

      $row_mutasi	= explode("#",$row['LogMutasi']);
      $jumlah_data= count($row_mutasi);

      $data_mutasi= explode("|",$row_mutasi[$jumlah_data-1]);

      $template->assign_block_vars('INFO_MUTASI',array(
        'cso'			=> $data_mutasi[3],
        'waktu'		=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_mutasi[4])),
        'tgl'			=> dateparse(FormatMySQLDateToTgl($data_mutasi[0])),
        'jadwal'	=> $data_mutasi[1]
      ));
    }

    if($row['StatusDiambil']==1){
      $template->assign_block_vars('INFO_PENGAMBILAN',array(
        'waktu'		=> dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])),
        'nama'		=> $row['NamaPengambil'],
        'telp'		=> $row['TelpPengambil'],
        'cso'			=> $row['NamaPetugasPemberi']
      ));
    }

    $template->set_filenames(array('body' => 'reservasi.paket/paketdetail.tpl'));
    $template->pparse('body');
    exit;

    break;

//MUTASI PAKET=============================================================================================================================================
  case "mutasipaket":
    include($adp_root_path . 'ClassPaketEkspedisi.php');

    $resi					= $HTTP_GET_VARS['resi'];
    $tanggal			= $HTTP_GET_VARS['tanggal'];
    $kode_jadwal	= $HTTP_GET_VARS['kodejadwal'];

    $Jadwal = new Jadwal();
    $Paket	= new Paket();

    $data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);

    $Paket->mutasi($resi,$tanggal,$data_jadwal['JamBerangkat'],$data_jadwal['IdJurusan'],$kode_jadwal,$userdata['user_id'],$userdata['nama']);

    echo("prosesok=true;");
    exit;


//PAKET AMBIL ===========================================================================================================================================================================================
  case "paketambil":
    include($adp_root_path . 'ClassPaketEkspedisi.php');
    $Paket			= new Paket();

    $no_tiket					= $HTTP_GET_VARS['no_tiket'];
    $nama_pengambil		= $HTTP_GET_VARS['nama_pengambil'];
    $no_ktp_pengambil	= $HTTP_GET_VARS['no_ktp_pengambil'];

    if($Paket->updatePaketDiambil($no_tiket,$nama_pengambil,$no_ktp_pengambil,$userdata['user_id'],$userdata['KodeCabang'])){
      echo(1);
    }
    else{
      echo(0);
    }

    exit;

//CARI PAKET  ===========================================================================================================================================================================================
  case 'cari_paket':
    $Reservasi	= new Reservasi();

    $no_resi	= $HTTP_GET_VARS['no_resi'];

    $result	= $Reservasi->cariPaket($no_resi);

    if ($result){

      $return	= "
			<table>
				<tr>
					<th width=100>Tgl.Pergi</th>
					<th width=150>Asal</th>
					<th width=150>Tujuan</th>
					<th width=70>Jam</th>
					<th width=100>Id Kendaraan</th>
					<th width=100>Sopir</th>
					<th width=100>No.Resi</th>
					<th width=150>Pengirim</th>
					<th width=150>Penerima</th>
					<th width=150>Pengambil</th>
				</tr>";

      $jum_data	= 0;

      while ($row = $db->sql_fetchrow($result)){

        if($row['StatusDiambil']==1){
          $output_pengambilan	="
				<div valign='top'>
					Nama	: $row[NamaPengambil]<br>
					KTP		: $row[NoKTPPengambil]<br>
					Waktu	: ".FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])."
					CSO		: $row[NamaPetugasPemberi]
				</div>
				";
        }
        else{
          $output_pengambilan	="
					<div align='center'><a href='' onClick='ambilDataPaket(\"$row[NoTiket]\");return false;'>Ambil Paket</a></div>
				";
        }

        $return	.="
				<tr bgcolor='dfdfdf'>
					<td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
					<td>$row[Asal]</td>
					<td>$row[Tujuan]</td>
					<td align='center'>$row[JamBerangkat]</td>
					<td align='left'>$row[KodeKendaraan] ($row[NoPolisi])</td>
					<td align='left'>$row[NamaSopir]</td>
					<td>$row[NoTiket]</td>
					<td valign='top'>
						Nama: $row[NamaPengirim]<br>
						Alamat: $row[AlamatPengirim]<br>
						Telp: $row[TelpPengirim]
					</td>
					<td valign='top'>
						Nama: $row[NamaPenerima]<br>
						Alamat: $row[AlamatPenerima]<br>
						Telp: $row[TelpPenerima]
					</td>
					<td>
						$output_pengambilan
					</td>
				</tr>";

        $jum_data++;
      }
    }
    else{
      echo("Err :".__LINE__);exit;
    }

    if($jum_data==0){
      $return .="<tr><td colspan=11 class='banner' align='center'><h2>Data paket tidak ditemukan!</h2></td></tr>";
    }

    echo($return."</table>");

    exit;

//AMBIL LIST HARGA PAKET======================================================================================================================	
  case 'ambil_list_harga_paket':
    //Mengambil daftar harga paket
    include($adp_root_path .'/ClassPaketEkspedisi.php');
    $id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];

    $Paket= new Paket();

    $result= $Paket->ambilDaftarHarga($id_jurusan);

    if ($result){
      $row = $db->sql_fetchrow($result);

      //return eval
      echo("
			document.getElementById('harga_kg_pertama_1').value 		= $row[HargaPaket1KiloPertama];
			document.getElementById('harga_kg_pertama_2').value 		= $row[HargaPaket2KiloPertama];
			document.getElementById('harga_kg_pertama_3').value 		= $row[HargaPaket3KiloPertama];
			document.getElementById('harga_kg_pertama_4').value 		= $row[HargaPaket4KiloPertama];
			document.getElementById('harga_kg_pertama_5').value 		= $row[HargaPaket5KiloPertama];
			document.getElementById('harga_kg_pertama_6').value 		= $row[HargaPaket6KiloPertama];
			document.getElementById('harga_kg_berikutnya_1').value 	= $row[HargaPaket1KiloBerikut];
			document.getElementById('harga_kg_berikutnya_2').value 	= $row[HargaPaket2KiloBerikut];
			document.getElementById('harga_kg_berikutnya_3').value 	= $row[HargaPaket3KiloBerikut];
			document.getElementById('harga_kg_berikutnya_4').value 	= $row[HargaPaket4KiloBerikut];
			document.getElementById('harga_kg_berikutnya_5').value 	= $row[HargaPaket5KiloBerikut];
			document.getElementById('harga_kg_berikutnya_6').value 	= $row[HargaPaket6KiloBerikut];
		");

    }
    else{
      echo("Error ".__LINE__);
    }

    exit;
}

$Kota = new Kota();

$template->assign_vars (
  array(
    'BCRUMP'    					=> '<ul id="breadcrumb"><li><a href="'.append_sid('main.'.$phpEx) .'">Home</a></li></ul>',
    'TGL_SEKARANG'				=> dateY_M_D(),
    'OPT_KOTA'						=> $Kota->setComboKota(),
    'U_CHECKIN_PAKET'			=> "window.open('".append_sid('paket.checkin.'.$phpEx.'')."','_blank');",
    'U_LAPORAN_UANG'			=> "Start('".append_sid('laporan.rekap.setoran.'.$phpEx.'')."');",
    'U_LAPORAN_PENJUALAN'	=> "Start('".append_sid('laporan_penjualan_paket_user.'.$phpEx.'')."');",
    'U_UBAH_PASSWORD'			=> append_sid('ubah_password.'.$phpEx.'')
  )
);


$template->set_filenames(array('body' => 'reservasi.paket/index.tpl'));

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>