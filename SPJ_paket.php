<?php
//
// SPJ
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassPaketEkspedisi.php');

// SESSION
$userdata = session_pagestart($user_ip,803);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

//METHODS============================================================================

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	global $userdata;
	
	$Sopir = new Sopir();
	
	
	if($userdata['user_level']>$USER_LEVEL_INDEX['SPV_RESERVASI']){
		$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	}
	else{
		$sql = 
			"SELECT tms.*
			FROM tbl_md_sopir tms
			WHERE FlagAktif=1 AND tms.KodeSopir NOT IN(SELECT KodeDriver FROM tbl_spj WHERE TIMEDIFF(NOW(),TglSPJ)<'02:00:00');";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err $this->ID_FILE".__LINE__);exit;
		}
	}
	
	$opt_sopir="<option value=''>- silahkan pilih sopir  -</option>";
		
	while ($row = $db->sql_fetchrow($result)){
		$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
		$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
	}
			
	return $opt_sopir;
	//END SET COMBO SOPIR
}

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;
	global $userdata;
	$Mobil = new Mobil();
	
	if($userdata['user_level']>$USER_LEVEL_INDEX['SPV_RESERVASI']){
		$result=$Mobil->ambilDataForComboBox();
	}
	else{
		$sql = 
			"SELECT tmk.KodeKendaraan, tmk.Jenis, tmk.Merek,tmk.NoPolisi,tmk.JumlahKursi
			FROM tbl_md_kendaraan tmk
			WHERE FlagAktif=1 AND tmk.KodeKendaraan NOT IN(SELECT NoPolisi FROM tbl_spj WHERE TIMEDIFF(NOW(),TglSPJ)<'02:00:00')
			ORDER BY KodeKendaraan";
					
		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
	}

	$opt_mobil="<option value=''>- silahkan pilih kendaraan  -</option>";
	
	while ($row = $db->sql_fetchrow($result)){
		$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
		$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[Merek] $row[Jenis]</option>";
	}
	
	return $opt_mobil;
	//END SET COMBO MOBIL
}
//--END METHODS==========================================================

$tgl_berangkat		= $HTTP_GET_VARS['tgl_berangkat']!=""?$HTTP_GET_VARS['tgl_berangkat']:$HTTP_POST_VARS['tgl_berangkat'];
$kode_jadwal			= $HTTP_GET_VARS['kode_jadwal']!=""?$HTTP_GET_VARS['kode_jadwal']:$HTTP_POST_VARS['kode_jadwal'];
$sopir_sekarang		= $HTTP_GET_VARS['sopir_sekarang']!=""?$HTTP_GET_VARS['sopir_sekarang']:$HTTP_POST_VARS['sopir_sekarang'];
$mobil_sekarang		= $HTTP_GET_VARS['mobil_sekarang']!=""?$HTTP_GET_VARS['mobil_sekarang']:$HTTP_POST_VARS['mobil_sekarang'];
$mode							= $HTTP_GET_VARS['mode']!=""?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];

$Jadwal				= new Jadwal();
$Paket				= new Paket();
$Reservasi		= new Reservasi();

switch($mode){
	case "validasi":
		$Jurusan	= new Jurusan();

		if($kode_jadwal=="" || $tgl_berangkat=="") exit;

		$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
		
		//mengambil karakteristik jadwal
		if($data_jadwal['FlagSubJadwal']!=1){
			//jika jadwal adalah jadwal Induk
			$kode_jadwal_utama	= $kode_jadwal; 
			$id_jurusan					= $data_jadwal['IdJurusan'];
		}
		else{
			//jadwal transit
			$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];  //mengambil jadwal induk
			$data_jadwal_induk	= $Jadwal->ambilDataDetail($kode_jadwal_utama);
			$id_jurusan					= $data_jadwal_induk['IdJurusan'];
		}
		
		//Memeriksa jika sifat dari jurusan
		$data_jurusan	= $Jurusan->ambilDataDetail($id_jurusan);
		
		if($data_jurusan['FlagOperasionalJurusan']==2){
			//Sifat jurusan adalah: paket ikut mobil travel, maka tidak perlu ada biaya operasional
			//dan manifest hanya bisa dicetak jika manifest penumpang sudah dicetak
			
			//memeriksa apakah manifest jadwal induk apakah sudah dicetak
			$data_spj	= $Paket->ambilDataHeaderLayout($tgl_berangkat,$kode_jadwal_utama);
			$sopir = $data_spj['KodeDriver'];
			$mobil = $data_spj['KodeKendaraan'];
			if($data_spj['NoSPJ']==""){
				//belum cetak manifest maka tidak dapat dicetak manifest paketnya
				echo("proses_ok=true;alert('Manifest mobil penumpang belum dicetak');");	exit;
			}
		}
		else{
			//jika jurusan adalah: paket ikut mobil khusus paket, maka akan dikeluarkan biaya operasional
			exit;
		}
		
		$list_kode_jadwal	= $Jadwal->ambilListKodeJadwalByKodeJadwalUtama($kode_jadwal_utama);
		
		//Mengambil jumlah paket dan omzet
		$data_total_paket	=$Paket->hitungTotalOmzetdanJumlahPaketPerSPJ($tgl_berangkat,$list_kode_jadwal);
		
		$total_omzet_paket=($data_total_paket['TotalOmzet']!='')?$data_total_paket['TotalOmzet']:0;
		$jumlah_paket			=($data_total_paket['JumlahPaket']!='')?$data_total_paket['JumlahPaket']:0;
		$jumlah_pax				=($data_total_paket['JumlahPax']!='')?$data_total_paket['JumlahPax']:0;	

		
		if($Paket->ubahSPJ(
			$data_spj['NoSPJ'], $jumlah_paket,$jumlah_pax,
			$userdata['user_id'], $total_omzet_paket)){

			//Mengubah status SPJ
			$Reservasi->ubahDataReservasiCetakSPJ(
				$list_kode_jadwal, $tgl_berangkat,$sopir,
				$mobil,$data_spj['NoSPJ']);
			
			//CATAT LOG MANIFEST
			$Paket->tambahLogCetakManifest(
				$data_spj['NoSPJ'],$kode_jadwal_utama,$tgl_berangkat,
				$data_jadwal['JamBerangkat'],$jumlah_paket,$jumlah_pax,
				"","","",
				$total_omzet_paket,0,$userdata['user_id'],
				$userdata['nama']);
		}
		
		echo("proses_ok=true;Start(\"SPJ_paket.php?sid=$userdata[session_id]&mode=cetak&tgl_berangkat=$tgl_berangkat&kode_jadwal=$kode_jadwal&nospj=$data_spj[NoSPJ]\");");	exit;
		
	case "cetak":
		$no_spj	= $HTTP_GET_VARS['nospj']!=""?$HTTP_GET_VARS['nospj']:$HTTP_POST_VARS['nospj'];
		
		if($no_spj=="" || $kode_jadwal=="" || $tgl_berangkat=="") exit;
		
		$result_paket = $Paket->ambilDataPaketUntukSPJ($no_spj,$tgl_berangkat,$kode_jadwal);
		$idx_no=0;
		
		$jumlah_pax	= 0;
		$total_omzet_paket	= 0;
		
		while($row_paket=$db->sql_fetchrow($result_paket)){
			$idx_no++;
					
			$template ->assign_block_vars(
				'ROW_PAKET',array(
					'IDX_PAKET_NO'  =>$idx_no,
					'NO_TIKET_PAKET'=>$row_paket['NoTiket'],
					'TUJUAN'        =>$row_paket['Tujuan'],
					'NAMA_PENGIRIM' =>$row_paket['NamaPengirim'],
					'TELP_PENGIRIM' =>$row_paket['TelpPengirim'],
					'NAMA_PENERIMA' =>$row_paket['NamaPenerima'],
					'TELP_PENERIMA' =>$row_paket['TelpPenerima'],
					'JUMLAH_PAX'		=>$row_paket['JumlahKoli']
				)
			);
			
			$jumlah_pax	+=$row_paket['JumlahKoli'];
			//$total_omzet_paket +=$row_paket['TotalBayar'];
		}
		
		if($idx_no<=0){
			$tidak_ada_paket	= '<br>TIDAK ADA PAKET<br><br>';
		}
		
		$data_spj	= $Paket->ambilDataSPJ($no_spj);
		
		$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
			
		$template->set_filenames(array('body' => 'SPJ_paket_body.tpl')); 
		$template->assign_vars(array(
			'NAMA_PERUSAHAAN'   =>$data_perusahaan['NamaPerusahaan'],
			'ALAMAT_PERUSAHAAN' =>$data_perusahaan['AlamatPerusahaan'],
			'TELP_PERUSAHAAN'		=>$data_perusahaan['TelpPerusahaan'],
			'NO_SPJ' 						=>$no_spj,
			'TGL_BERANGKAT' 		=>dateparse(FormatMySQLDateToTgl($tgl_berangkat)),
			'JURUSAN' 					=>$kode_jadwal." ".$jam_berangkat_show,
			'TGL_CETAK' 				=>FormatMySQLDateToTglWithTime(dateNow(true)),
			'NO_POLISI' 				=>$data_spj['NoPolisi'],
			'SOPIR' 						=>$data_spj['Driver']." (".$data_spj['KodeDriver'].")",
			'TIDAK_ADA_PAKET' 	=>$tidak_ada_paket,
			'JUMLAH_PAKET' 			=>$idx_no,
			'JUMLAH_KOLI' 			=>number_format($jumlah_pax,0,",","."),
			'OMZET_PAKET' 			=>number_format($total_omzet_paket,0,",","."),
			'CSO' 							=>$userdata['nama'],
			'EMAIL_PERUSAHAAN' 	=>$data_perusahaan['EmailPerusahaan'],
			'WEBSITE_PERUSAHAAN'=>$data_perusahaan['WebSitePerusahaan'],
			'SID'								=>$userdata['session_id']
			)
		);
			
		$template->pparse('body');	
	
	exit;

}

?>