<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassKota.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$cari       = $HTTP_POST_VARS["cari"]!=''?$HTTP_POST_VARS["cari"]:$HTTP_GET_VARS["cari"];

$Kota	= new Kota();

$mode = $mode==""?"exp":$mode;

switch($mode){
  case 'exp':
    //view mode
    // LIST
    $template->set_filenames(array('body' => 'kota/index.tpl'));

    $sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
    $order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

    $order	=($order=='')?"ASC":$order;

    $sort_by =($sort_by=='')?"NamaKota":$sort_by;

    $kondisi	=($cari=="")?"":
      " WHERE KodeKota LIKE '%$cari%'
				OR NamaKota LIKE '%$cari%'";

    //PAGING======================================================
    $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
    $paging=pagingData($idx_page,"KodeKota","tbl_md_kota","&cari=$cari",$kondisi,"pengaturan_kota.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
    //END PAGING======================================================

    $sql =
      "SELECT *
			FROM tbl_md_kota
			$kondisi
			ORDER BY $sort_by $order";

    $idx_check=0;


    if ($result = $db->sql_query($sql)){
      $i = $idx_page*$VIEW_PER_PAGE+1;
      while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
          $odd = 'even';
        }

        $idx_check++;

        $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[KodeKota]'\"/>";

        $act 	="<a href='".append_sid('pengaturan_kota.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
        $act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
        $template->
          assign_block_vars(
            'ROW',
            array(
              'odd'		    =>$odd,
              'check'	    =>$check,
              'no'		    =>$i,
              'kodekota'  =>$row['KodeKota'],
              'namakota'	=>$row['NamaKota'],
              'action'    =>$act
            )
          );

        $i++;
      }

      if($i-1<=0){
       $template->assign_block_vars("NO_DATA",array());
      }
    }
    else{
      echo("Err :".__LINE__);exit;
    }

    //paramter sorting
    $order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
    $parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

    $array_sort	=
      "'".append_sid('pengaturan_kota.php?sort_by=KodeKota'.$parameter_sorting)."',".
      "'".append_sid('pengaturan_kota.php?sort_by=NamaKota'.$parameter_sorting)."',";

    $page_title	= "Pengaturan Kota";

    $template->assign_vars(array(
        'BCRUMP'    		=> '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan_kota.'.$phpEx).'">Kota</a>',
        'U_ADD'	        => append_sid('pengaturan_kota.'.$phpEx.'?mode=add'),
        'ACTION_CARI'		=> append_sid('pengaturan_kota.'.$phpEx),
        'CARI'			    => $cari,
        'NO_DATA'				=> $no_data,
        'PAGING'				=> $paging,
        'ARRAY_SORT'		=> $array_sort
      )
    );

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');
  exit;

  case 'add':
		// add
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
			$bgcolor_pesan="98e46f";
		}

    $page_title = "Tambah Data Kota";
		$template->set_filenames(array('body' => 'kota/add_body.tpl'));
		$template->assign_vars(array(
		 'BCRUMP'		        => '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_kota.'.$phpEx).'">Kota</a> | <a href="'.append_sid('pengaturan_kota.'.$phpEx."?mode=add").'">Tambah Kota</a> ',
		 'JUDUL'		        => 'Tambah Data Kota',
		 'MODE'   	        => 'save',
		 'PESAN'				    => $pesan,
		 'BGCOLOR_PESAN'    => $bgcolor_pesan,
		 'U_ADD'	          => append_sid('pengaturan_kota.'.$phpEx)
		 )
		);

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');
	exit;

  case 'save':
		/*MENYIMPAN PERUBAHAN DATA*/
		$kode_kota  		= strtoupper(str_replace(" ","",$HTTP_POST_VARS['kodekota']));
		$kode_kota_old	= strtoupper(str_replace(" ","",$HTTP_POST_VARS['kodekotaold']));
		$nama_kota   	  = strtoupper($HTTP_POST_VARS['namakota']);
		
		if($Kota->periksaDuplikasi($kode_kota) && $kode_kota!=$kode_kota_old){
			$pesan="<font color='white' size=3>Kode kota yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
		}
		else{
			if($kode_kota_old==""){
        /*MENGAMBAH DATA KOTA BARU*/
				$judul="Tambah Data Kota";
				$path	='<a href="'.append_sid('pengaturan_kota.'.$phpEx."?mode=add").'">Tambah Kota</a> ';
				
				if($Kota->tambah($kode_kota,$nama_kota)){
					redirect(append_sid('pengaturan_kota.'.$phpEx.'?mode=add&pesan=1',true));
				}
			}
			else{
				
				$judul="Ubah Data Kota";
				
				if($Kota->ubah($kode_kota,$nama_kota,$kode_kota_old)){
					$pesan          ="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan  ="98e46f";
          $kode_kota_old  = $kode_kota;
        }

        $path	='<a href="'.append_sid('pengaturan_kota.'.$phpEx."?mode=edit&id=$kode_kota_old").'">Ubah Kota</a> ';
			}
			
			//exit;
			
		}

    $page_title = "Ubah Data Kota";
		$template->set_filenames(array('body' => 'kota/add_body.tpl'));
		$template->assign_vars(array(
		 'BCRUMP'		    => '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_kota.'.$phpEx).'">Kota</a> | '.$path,
		 'JUDUL'		    => $judul,
		 'MODE'         => 'save',
		 'KODE_KOTA_OLD'=> $kode_kota_old,
		 'KODE_KOTA'    => $kode_kota,
		 'NAMA_KOTA'    => $nama_kota,
		 'PESAN'		    => $pesan,
		 'BGCOLOR_PESAN'=> $bgcolor_pesan,
		 'U_ADD'        => append_sid('pengaturan_kota.'.$phpEx)
		 )
		);

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');
	exit;

  case 'edit':
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Kota->ambilDataDetail($id);
		
		$template->set_filenames(array('body' => 'kota/add_body.tpl'));
		$template->assign_vars(array(
			 'BCRUMP'		    => '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_kota.'.$phpEx).'">Kota</a> | <a href="'.append_sid('pengaturan_kota.'.$phpEx."?mode=edit&id=$id").'">Ubah Kota</a> ',
			 'JUDUL'		    => 'Ubah Data Kota',
			 'MODE'   	    => 'save',
       'KODE_KOTA_OLD'=> $row["KodeKota"],
       'KODE_KOTA'    => $row["KodeKota"],
       'NAMA_KOTA'    => $row["NamaKota"],
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_ADD'        => append_sid('pengaturan_kota.'.$phpEx)
			 )
		);

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');

  exit;

  case 'delete':
		/*MENGHAPUS DATA*/
		$list = str_replace("\'","'",$HTTP_GET_VARS['list']);
		$Kota->hapus($list);
		
	exit;
}

?>