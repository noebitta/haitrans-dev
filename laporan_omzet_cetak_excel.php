<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

$Cabang = new Cabang();

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$bulan  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tahun  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];

//INISIALISASI

		
//QUERY
//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}

$kondisi_cabang	= $userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])?" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'":"";

//QUERY PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	$kondisi_cabang
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
//QUERY PAKET
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzet
	FROM tbl_paket
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	$kondisi_cabang
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//QUERY BIAYA
$sql=
	"SELECT 
		WEEKDAY(TglTransaksi)+1 AS Hari,DAY(TglTransaksi) AS Tanggal,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE MONTH(TglTransaksi)=$bulan AND YEAR(TglTransaksi)=$tahun
		AND FlagJenisBiaya!='$FLAG_BIAYA_VOUCHER_BBM'
		$kondisi_cabang
	GROUP BY TglTransaksi
	ORDER BY TglTransaksi ";

if ($result_biaya = $db->sql_query($sql)){
	$data_biaya = $db->sql_fetchrow($result_biaya);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

	
$sum_penumpang						= 0;
$sum_paket								= 0;
$sum_omzet_penumpang			= 0;
$sum_omzet_paket					= 0;
$sum_discount							= 0;
$sum_pendapatan_penumpang	= 0;
$sum_biaya								= 0;
$sum_profit								= 0;

	
//EXPORT KE MS-EXCEL

			
$i=1;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet periode '.BulanString($bulan).' '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Jum.Pnp');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Omzet pnp.');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Total Disc.');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Pendapatan Pnp.');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jum. Pkt.');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Omzet Pkt.');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Biaya OP');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'L/B Kotor');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);


$idx=0;

for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$idx++;
	
	$idx_row=$idx+3;
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;

	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	//OMZET PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_penumpang				= $data_penumpang['TotalPenumpang']; 
		$total_omzet_penumpang	= $data_penumpang['TotalOmzet']; 
		$total_discount					= $data_penumpang['TotalDiscount'];
		$pendapatan_penumpang		= $total_omzet_penumpang-$total_discount;
		$total_profit						= $pendapatan_penumpang;
		$data_penumpang 				= $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_penumpang				= 0;
		$total_omzet_penumpang	= 0;
		$total_discount					= 0;
		$pendapatan_penumpang		= 0;
		$total_profit						= 0;
	}
	
	//OMZET PAKET
	if($data_paket['Tanggal']==$idx_tgl+1){
		$total_paket				= $data_paket['TotalPaket']; 
		$total_omzet_paket	= $data_paket['TotalOmzet'];
		$total_profit				+= $total_omzet_paket;
		$data_paket 				= $db->sql_fetchrow($result_paket);
	}
	else{
		$total_paket				= 0;
		$total_omzet_paket	= 0;
	}
	
	//OMZET BIAYA
	if($data_biaya['Tanggal']==$idx_tgl+1){
		$total_biaya		= $data_biaya['TotalBiaya']; 
		$total_profit		-= $total_biaya;
		$data_biaya 		= $db->sql_fetchrow($result_biaya);
	}
	else{
		$total_biaya		= 0;
	}
	
	$sum_penumpang				+= $total_penumpang;
	$sum_paket						+= $total_paket;
	$sum_omzet_penumpang	+= $total_omzet_penumpang;
	$sum_omzet_paket			+= $total_omzet_paket;
	$sum_biaya						+= $total_biaya;
	$sum_discount					+= $total_discount;
	$sum_pendapatan_penumpang	+= $pendapatan_penumpang ;
	$sum_profit						+= $total_profit;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $tgl_transaksi);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $total_penumpang);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $total_omzet_penumpang);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $total_discount);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $pendapatan_penumpang);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $total_paket);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $total_omzet_paket);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $total_biaya);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $total_profit);

	
	$temp_hari++;
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, '=SUM(B4:B'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, '=SUM(C4:C'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D4:D'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');

	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet periode '.BulanString($bulan).' '.$tahun.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
 
  
  
?>
