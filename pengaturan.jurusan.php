<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_OPERASIONAL']))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Jurusan	= new Jurusan();
$Cabang		= new Cabang();

$mode     = $mode==""?"exp":$mode;

switch($mode){
  case "exp":
    /*VIEW MODE*/
    include($adp_root_path . 'ClassMobil.php');

    $Mobil	= new Mobil();

    $template->set_filenames(array('body' => 'jurusan/index.tpl'));

    if($HTTP_POST_VARS["txt_cari"]!=""){
      $cari=$HTTP_POST_VARS["txt_cari"];
    }
    else{
      $cari=$HTTP_GET_VARS["cari"];
    }

    $temp_cari=str_replace("asal=","",$cari);
    if($temp_cari==$cari){
      $kondisi_asal = "";
    }
    else{
      $kondisi_asal = "AND f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$temp_cari%' ";
      $cari=$temp_cari;
    }

    $temp_cari=str_replace("tujuan=","",$cari);
    if($temp_cari==$cari){
      $kondisi_tujuan = "";
    }
    else{
      $kondisi_tujuan = "AND f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$temp_cari%' ";
      $cari=$temp_cari;
    }

    $kondisi	=($cari=="")?"":
      " WHERE (KodeJurusan LIKE '%$cari%'
			OR KodeCabangAsal LIKE '%$cari%'
			OR KodeCabangTujuan LIKE '%$cari%'
			OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%'
			OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%')
			$kondisi_asal
			$kondisi_tujuan ";

    //PAGING======================================================
    $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
    $paging=pagingData($idx_page,"IdJurusan","tbl_md_jurusan","&cari=$cari",$kondisi,"pengaturan.jurusan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
    //END PAGING======================================================

    $sql =
      "SELECT *,f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaCabangAsal,f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan,FlagOperasionalJurusan
		FROM tbl_md_jurusan $kondisi
		ORDER BY KodeJurusan,NamaCabangAsal,NamaCabangTujuan LIMIT $idx_awal_record,$VIEW_PER_PAGE";

    $idx_check=0;


    if ($result = $db->sql_query($sql)){
      $i = $idx_page*$VIEW_PER_PAGE+1;

      while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
          $odd = 'even';
        }

        $idx_check++;

        $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"$row[0]\"/>";

        if(!$row['FlagTiketTuslah']){
          $tuslah = "";
          $css_harga_normal	= "green";
          $css_harga_tuslah	= "";
        }
        else{
          $tuslah	= "<b>TUSLAH<b>";
          $css_harga_normal	= "";
          $css_harga_tuslah	= "green";
        }

        if($row['FlagAktif']){
          $aktif = "<a href='#' onClick='ubahStatusAktif($row[IdJurusan])'>Aktif</a>";
        }
        else{
          $aktif ="<a href='#' onClick='ubahStatusAktif($row[IdJurusan])'>Tidak Aktif</a>";
          $odd	='red';
        }

        $class_op_jurusan="";

        $ops_jurusans = Array("REG","PKT","REG-PKT","NON-REG","CHRT");

        switch($row["FlagOperasionalJurusan"]){
          case "0":
            /*JURUSAN OPERASIONAL REGULER*/
          case "2":
          /*JURUSAN OPERASIONAL REGULER & PAKET*/

            $data_layout	= $Mobil->getArrayLayout();

            $harga_tiket_decode		      = $Mobil->layoutBodyDecode($row["HargaTiket"]);
            $harga_tiket_tuslah_decode	= $Mobil->layoutBodyDecode($row["HargaTiketTuslah"]);
            $harga_tiket                = "";
            $harga_tiket_tuslah         = "";

            foreach($data_layout as $layout_id	=> $array_value){
              $harga_tiket        .= $layout_id."= Rp.".number_format((float)$harga_tiket_decode["$layout_id"],0,",",".")."<br>";
              $harga_tiket_tuslah .= $layout_id."= Rp.".number_format((float)$harga_tiket_tuslah_decode["$layout_id"],0,",",".")."<br>";
            }
          break;

          case "3":
            /*JURUSAN OPERASIONAL NON-REGULER*/
            $harga_tiket        = "MUTLI";
            $harga_tiket_tuslah = "N/A";
          break;
        }

        $act 	="<a href='".append_sid('pengaturan.jurusan.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
        $act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";

        $template->
          assign_block_vars(
            'ROW',
            array(
              'odd'                 => $odd,
              'check'               => $check,
              'no'                  => $i,
              'kode'                => $row['KodeJurusan'],
              'asal'                => $row['NamaCabangAsal'],
              'tujuan'              => $row['NamaCabangTujuan'],
              'jenis'               => (($row['FlagLuarKota']==1)?"LUAR KOTA":"DALAM KOTA")." | ".$ops_jurusans[$row["FlagOperasionalJurusan"]],
              'titlejenis'          => $title_jenis,
              'harga_tiket'         => $harga_tiket,
              'harga_tiket_tuslah'  => $harga_tiket_tuslah,
              'harga_paket_1'       => number_format($row['HargaPaket1KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket1KiloBerikut'],0,",","."),
              'harga_paket_2'       => number_format($row['HargaPaket2KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket2KiloBerikut'],0,",","."),
              'harga_paket_3'       => number_format($row['HargaPaket3KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket3KiloBerikut'],0,",","."),
              'harga_paket_4'       => number_format($row['HargaPaket4KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket4KiloBerikut'],0,",","."),
              'harga_paket_5'       => number_format($row['HargaPaket5KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket5KiloBerikut'],0,",","."),
              'harga_paket_6'       => number_format($row['HargaPaket6KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket6KiloBerikut'],0,",","."),
              'biayasopir'          => number_format($row["BiayaSopir"],0,",","."),
              'biayatol'            => number_format($row["BiayaTol"],0,",","."),
              'biayaparkir'         => number_format($row["BiayaParkir"],0,",","."),
              'tuslah'              => $css_harga_tuslah,
              'status_tuslah'       => $tuslah,
              'status_aktif'        => $aktif,
              'action'              => $act
            )
          );

        $i++;
      }

      if($i-1<=0){
        $no_data	=	"<tr><td colspan=20 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
      }

    }
    else{
      //die_error('Cannot Load jurusan',__FILE__,__LINE__,$sql);
      echo("Error :".__LINE__);exit;
    }

    $page_title	= "Pengaturan Jurusan";

    $template->assign_vars(array(
        'BCRUMP'    		=> '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan.jurusan.'.$phpEx).'">Jurusan</a>',
        'U_JURUSAN_ADD'		=> append_sid('pengaturan.jurusan.'.$phpEx.'?mode=add'),
        'ACTION_CARI'		=> append_sid('pengaturan.jurusan.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'NO_DATA'				=> $no_data,
        'PAGING'				=> $paging
      )
    );
  break;

  case "add":
    /*ADD MODE*/
    include($adp_root_path . 'ClassMobil.php');
    $Mobil	= new Mobil();

    $pesan = $HTTP_GET_VARS['pesan'];

    if($pesan==1){
      $pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
      $bgcolor_pesan="98e46f";
    }

    $data_layout	= $Mobil->getArrayLayout();

    foreach($data_layout as $layout_id=> $layout_value){
      $template->assign_block_vars("LISTBBM",array(
          "idlayout"	=> $layout_id,
          "literbbm"	=> "")
      );
    }

    /*LOOP HARGA TIKET*/
    $ganjil =0;
    foreach($data_layout as $layout_id=> $layout_value){
      $odd=$ganjil%2==0?"even":"odd";

      $template->assign_block_vars("LISTHARGATIKET",array(
          "odd"	              => $odd,
          "idlayout"	        => $layout_id,
          "hargatiket"	      => "",
          "hargatikettuslah"	=> "")
      );

      $ganjil = 1-$ganjil;
    }

    $page_title = "Tambah Jurusan";

    $template->set_filenames(array('body' => 'jurusan/edit.tpl'));
    $template->assign_vars(array(
        'BCRUMP'		       =>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan.jurusan.'.$phpEx).'">Jurusan</a> | <a href="'.append_sid('pengaturan.jurusan.'.$phpEx."?mode=add").'">Tambah Jurusan</a> ',
        'JUDUL'			       =>'Tambah Data Jurusan',
        'MODE'   		       => 'save',
        'SUB'    		       => '0',
        'OPT_ASAL'         => $Cabang->setInterfaceComboCabang(),
        'OPT_TUJUAN'       => $Cabang->setInterfaceComboCabang(),
        'PESAN'				 	   => $pesan,
        'BGCOLOR_PESAN'    => $bgcolor_pesan,
        'U_ADD'	           => append_sid('pengaturan.jurusan.'.$phpEx),
        'OP_JUR_SELECT'    => 0,
        'FLAG_JENIS_SELECT'=> 1,
        'FLAG_AKTIF_SELECT'=> 1
      )
    );
  break;

  case "save":
    /*SAVING ACTION*/
    include($adp_root_path . 'ClassMobil.php');
    $Mobil	= new Mobil();

    // aksi menambah jurusan
    $id_jurusan  											= $HTTP_POST_VARS['id_jurusan'];
    $kode  														= str_replace(" ","",$HTTP_POST_VARS['kode']);
    $kode_jurusan_old									= str_replace(" ","",$HTTP_POST_VARS['kode_jurusan_old']);
    $asal   													= $HTTP_POST_VARS['asal'];
    $tujuan   												= $HTTP_POST_VARS['tujuan'];
    $harga_paket_1_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_1_kilo_pertama'];
    $harga_paket_1_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_1_kilo_berikut'];
    $harga_paket_2_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_2_kilo_pertama'];
    $harga_paket_2_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_2_kilo_berikut'];
    $harga_paket_3_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_3_kilo_pertama'];
    $harga_paket_3_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_3_kilo_berikut'];
    $harga_paket_4_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_4_kilo_pertama'];
    $harga_paket_4_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_4_kilo_berikut'];
    $harga_paket_5_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_5_kilo_pertama'];
    $harga_paket_5_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_5_kilo_berikut'];
    $harga_paket_6_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_6_kilo_pertama'];
    $harga_paket_6_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_6_kilo_berikut'];
    $flag_tuslah											= $HTTP_POST_VARS['flag_tuslah'];
    $flag_aktif												= $HTTP_POST_VARS['flag_aktif'];
    $flag_jenis												= $HTTP_POST_VARS['flag_jenis'];
    $flag_op_jurusan									= $HTTP_POST_VARS['flag_op_jurusan'];
    $biaya_sopir											= $HTTP_POST_VARS['biayasopir'];
    $flag_biaya_sopir_kumulatif				= $HTTP_POST_VARS['flag_biaya_sopir_kumulatif']!='on'?0:1;
    $biaya_tol												= $HTTP_POST_VARS['biayatol'];
    $biaya_parkir											= $HTTP_POST_VARS['biayaparkir'];
    $biaya_bbm												= $HTTP_POST_VARS['biaya_bbm'];
    $flag_biaya_voucher_bbm						= $HTTP_POST_VARS['flag_biaya_voucher_bbm']!='on'?0:1;
    $komisi_penumpang_sopir						= $HTTP_POST_VARS['komisi_penumpang_sopir'];
    $komisi_penumpang_cso							= $HTTP_POST_VARS['komisi_penumpang_cso'];
    $komisi_paket_sopir								= $HTTP_POST_VARS['komisi_paket_sopir'];
    $komisi_paket_cso									= $HTTP_POST_VARS['komisi_paket_cso'];
    $kode_akun_pendapatan_penumpang		= $HTTP_POST_VARS['kode_akun_pendapatan_penumpang'];
    $kode_akun_pendapatan_paket				= $HTTP_POST_VARS['kode_akun_pendapatan_paket'];
    $kode_akun_biaya_sopir						= $HTTP_POST_VARS['kode_akun_biaya_sopir'];
    $kode_akun_biaya_tol							= $HTTP_POST_VARS['kode_akun_biaya_tol'];
    $kode_akun_biaya_parkir						= $HTTP_POST_VARS['kode_akun_biaya_parkir'];
    $kode_akun_biaya_bbm							= $HTTP_POST_VARS['kode_akun_biaya_bbm'];
    $kode_akun_komisi_penumpang_sopir	= $HTTP_POST_VARS['kode_akun_komisi_penumpang_sopir'];
    $kode_akun_komisi_penumpang_cso		= $HTTP_POST_VARS['kode_akun_komisi_penumpang_cso'];
    $kode_akun_komisi_paket_sopir			= $HTTP_POST_VARS['kode_akun_komisi_paket_sopir'];
    $kode_akun_komisi_paket_cso				= $HTTP_POST_VARS['kode_akun_komisi_paket_cso'];
    $kode_akun_charge									= $HTTP_POST_VARS['kode_akun_charge'];

    $data_layout	= $Mobil->getArrayLayout();

    /*VERIFIKASI INPUT*/
    if($kode==""){
      redirect(append_sid('pengaturan.jurusan.'.$phpEx.'?mode=add',true));
    }

    switch($flag_op_jurusan){
      case "1":
        /*JIKA JENIS OPERASIONAL JURUSAN ADALAH PAKET*/
        $harga_tiket=0;
        $harga_tiket_tuslah=0;
        break;

      case "3":
        /*JIKA JENIS OPERASIONAL JURUSAN ADALAH NONREGULER*/

        $harga_tiket  = $HTTP_POST_VARS['hargatiketnonreg1'].";";
        $harga_tiket .= $HTTP_POST_VARS['hargatiketnonreg2'].";";
        $harga_tiket .= $HTTP_POST_VARS['hargatiketnonreg3'].";";
        $harga_tiket .= $HTTP_POST_VARS['hargatiketnonreg4'].";";
        break;

      case "0":
      case "2":
        $harga_tiket	      = "";
        $harga_tiket_tuslah = "";

        foreach($data_layout as $layout_id	=> $layout_value){
          $harga_tiket	      .= $layout_id."=".$HTTP_POST_VARS["hargatiket".$layout_id].";";
          $harga_tiket_tuslah	.= $layout_id."=".$HTTP_POST_VARS["hargatikettuslah".$layout_id].";";
        }
        break;
    }

    $terjadi_error=false;

    if($Jurusan->periksaDuplikasi($kode) && $kode!=$kode_jurusan_old){
      $pesan="<font color='white' size=3>Kode jurusan yang dimasukkan sudah terdaftar dalam sistem!</font>";
      $bgcolor_pesan="red";
      $terjadi_error=true;
    }
    else{

      $liter_bbm	= "";

      foreach($data_layout as $layout_id	=> $layout_value){
        $liter_bbm	.= $layout_id."=".$HTTP_POST_VARS["biayabbm".$layout_id].";";
      }

      if($submode==0){
        $judul="Tambah Data Jurusan";
        $path	='<a href="'.append_sid('pengaturan.jurusan.'.$phpEx."?mode=add").'">Tambah Jurusan</a> ';

        if($Jurusan->tambah(
          $kode,$asal,$tujuan,
          $harga_tiket,$harga_tiket_tuslah,$flag_tuslah,
          $biaya_sopir,$biaya_tol,$biaya_parkir,$biaya_bbm,
          $flag_jenis,
          $harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
          $harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
          $harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
          $harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
          $harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
          $harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
          $flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,
          $liter_bbm,$flag_aktif)){

          redirect(append_sid('pengaturan.jurusan.'.$phpEx.'?mode=add&pesan=1',true));
          //die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan.jurusan.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');

        }
      }
      else{

        $judul="Ubah Data Jurusan";
        $path	='<a href="'.append_sid('pengaturan.jurusan.'.$phpEx."?mode=edit&id=$id_jurusan").'">Ubah Jurusan</a> ';

        if($Jurusan->ubah(
          $id_jurusan,
          $kode,$asal,$tujuan,
          $harga_tiket,$harga_tiket_tuslah,$flag_tuslah,
          $biaya_sopir,$biaya_tol,$biaya_parkir,$biaya_bbm,
          $flag_jenis,
          $harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
          $harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
          $harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
          $harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
          $harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
          $harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
          $flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,
          $liter_bbm,$flag_aktif)){

          redirect(append_sid('pengaturan.jurusan.'.$phpEx.'?mode=edit&id='.$id_jurusan.'&pesan=1',true));
          //die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan.jurusan.'.$phpEx.'?mode=edit&id='.$id_jurusan).'">Sini</a> Untuk Melanjutkan','');

          $pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
          $bgcolor_pesan="98e46f";
        }
      }

    }
  break;

  case "edit":
    /*EDIT MODE*/
    include($adp_root_path . 'ClassMobil.php');
    $Mobil	= new Mobil();

    $id    = $HTTP_GET_VARS['id'];
    $pesan = $HTTP_GET_VARS['pesan'];

    $row=$Jurusan->ambilDataDetail($id);

    $data_layout	= $Mobil->getArrayLayout();

    $bbm_decode		              = $Mobil->layoutBodyDecode($row["LiterBBM"]);
    $harga_tiket_decode		      = $Mobil->layoutBodyDecode($row["HargaTiket"]);
    $harga_tiket_tuslah_decode	= $Mobil->layoutBodyDecode($row["HargaTiketTuslah"]);

    $ganjil =0;
    foreach($data_layout as $layout_id	=> $array_value){
      $template->assign_block_vars("LISTBBM",array(
          "idlayout"	=> $layout_id,
          "literbbm"	=> $bbm_decode["$layout_id"])
      );

      $odd=$ganjil%2==0?"even":"odd";
      $template->assign_block_vars("LISTHARGATIKET",array(
          "odd"             => $odd,
          "idlayout"	      => $layout_id,
          "hargatiket"	    => $harga_tiket_decode["$layout_id"],
          "hargatikettuslah"=> $harga_tiket_tuslah_decode["$layout_id"])
      );

      $ganjil=1-$ganjil;
    }

    /*MEMPARSING HARGA*/
    switch($row["FlagOperasionalJurusan"]){
      case "3":
        $harga_tikets = explode(";",$row["HargaTiket"]);
        $harga_tiket1 = $harga_tikets[0];
        $harga_tiket2 = $harga_tikets[1];
        $harga_tiket3 = $harga_tikets[2];
        $harga_tiket4 = $harga_tikets[3];
      break;
    }

    if($pesan==1){
      $pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
      $bgcolor_pesan="98e46f";
    }

    $page_title = "Ubah Jurusan";

    $template->set_filenames(array('body' => 'jurusan/edit.tpl'));
    $template->assign_vars(array(
        'BCRUMP'		                        => '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan.jurusan.'.$phpEx).'">Jurusan</a> | <a href="'.append_sid('pengaturan.jurusan.'.$phpEx."?mode=edit&id=$id").'">Ubah Jurusan</a> ',
        'JUDUL'		                          => 'Ubah Data Jurusan',
        'MODE'   	                          => 'save',
        'SUB'    	                          => '1',
        'ID_JURUSAN'				                => $id,
        'KODE_JURUSAN_OLD'	                => $row['KodeJurusan'],
        'KODE_JURUSAN'			                => $row['KodeJurusan'],
        'OPT_ASAL'    			                => $Cabang->setInterfaceComboCabang($row['KodeCabangAsal']),
        'OPT_TUJUAN'   		                  => $Cabang->setInterfaceComboCabang($row['KodeCabangTujuan']),
        'HARGA_TIKET'			                  => $row['HargaTiket'],
        'HARGA_TIKET1'			                => $harga_tiket1,
        'HARGA_TIKET2'			                => $harga_tiket2,
        'HARGA_TIKET3'			                => $harga_tiket3,
        'HARGA_TIKET4'			                => $harga_tiket4,
        'HARGA_TIKET_TUSLAH'                => $row['HargaTiketTuslah'],
        'HARGA_PAKET_1_KILO_PERTAMA'	      => $row['HargaPaket1KiloPertama'],
        'HARGA_PAKET_1_KILO_BERIKUT'	      => $row['HargaPaket1KiloBerikut'],
        'HARGA_PAKET_2_KILO_PERTAMA'        => $row['HargaPaket2KiloPertama'],
        'HARGA_PAKET_2_KILO_BERIKUT'        => $row['HargaPaket2KiloBerikut'],
        'HARGA_PAKET_3_KILO_PERTAMA'        => $row['HargaPaket3KiloPertama'],
        'HARGA_PAKET_3_KILO_BERIKUT'        => $row['HargaPaket3KiloBerikut'],
        'HARGA_PAKET_4_KILO_PERTAMA'	      => $row['HargaPaket4KiloPertama'],
        'HARGA_PAKET_4_KILO_BERIKUT'	      => $row['HargaPaket4KiloBerikut'],
        'HARGA_PAKET_5_KILO_PERTAMA'	      => $row['HargaPaket5KiloPertama'],
        'HARGA_PAKET_5_KILO_BERIKUT'	      => $row['HargaPaket5KiloBerikut'],
        'HARGA_PAKET_6_KILO_PERTAMA'	      => $row['HargaPaket6KiloPertama'],
        'HARGA_PAKET_6_KILO_BERIKUT'	      => $row['HargaPaket6KiloBerikut'],
        'FLAG_AKTIF_SELECT'                 => $row['FlagAktif'],
        'FLAG_JENIS_SELECT'                 => $row['FlagLuarKota'],
        'OP_JUR_SELECT'                     => $row['FlagOperasionalJurusan'],
        'BIAYA_SOPIR'			                  => $row['BiayaSopir'],
        'FLAG_BIAYA_SOPIR_KUMULATIF'	      => $row['IsBiayaSopirKumulatif']!=1?"":"checked",
        'BIAYA_TOL'				                  => $row['BiayaTol'],
        'BIAYA_PARKIR'			                => $row['BiayaParkir'],
        'BIAYA_BBM'				                  => $row['BiayaBBM'],
        'FLAG_BIAYA_VOUCHER_BBM'	          => $row['IsVoucherBBM']!=1?"":"checked",
        'KOMISI_PENUMPANG_SOPIR'					  => $row['KomisiPenumpangSopir'],
        'KOMISI_PENUMPANG_CSO'						  => $row['KomisiPenumpangCSO'],
        'KOMISI_PAKET_SOPIR'							  => $row['KomisiPaketSopir'],
        'KOMISI_PAKET_CSO'								  => $row['KomisiPaketCSO'],
        'KODE_AKUN_PENDAPATAN_PENUMPANG'	  => $row['KodeAkunPendapatanPenumpang'],
        'KODE_AKUN_PENDAPATAN_PAKET'			  => $row['KodeAkunPendapatanPaket'],
        'KODE_AKUN_BIAYA_SOPIR'					    => $row['KodeAkunBiayaSopir'],
        'KODE_AKUN_BIAYA_TOL'						    => $row['KodeAkunBiayaTol'],
        'KODE_AKUN_BIAYA_PARKIR'					  => $row['KodeAkunBiayaParkir'],
        'KODE_AKUN_BIAYA_BBM'						    => $row['KodeAkunBiayaBBM'],
        'KODE_AKUN_KOMISI_PENUMPANG_SOPIR'  => $row['KodeAkunKomisiPenumpangSopir'],
        'KODE_AKUN_KOMISI_PENUMPANG_CSO'	  => $row['KodeAkunKomisiPenumpangCSO'],
        'KODE_AKUN_KOMISI_PAKET_SOPIR'		  => $row['KodeAkunKomisiPaketSopir'],
        'KODE_AKUN_KOMISI_PAKET_CSO'			  => $row['KodeAkunKomisiPaketCSO'],
        'KODE_AKUN_CHARGE'								  => $row['KodeAkunCharge'],
        'BGCOLOR_PESAN'                     => $bgcolor_pesan,
        'U_ADD'                             => append_sid('pengaturan.jurusan.'.$phpEx),
        'PESAN'				 	                    => $pesan,
        'BGCOLOR_PESAN'                     => $bgcolor_pesan
      )
    );
  break;

  case "delete":
    /*DELETE ACTION*/
    $list_jurusan = str_replace("\'","'",$HTTP_GET_VARS['list_jurusan']);

    $Jurusan->hapus($list_jurusan);

  exit;

  case "ubahstatusaktif":
    /*UBAH STATUS AKTIF ACTION*/
    $id_jurusan = $HTTP_GET_VARS['id'];

    $Jurusan->ubahStatusAktif($id_jurusan);
  exit;
}

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>