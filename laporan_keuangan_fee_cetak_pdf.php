<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$bulan  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tahun  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];


//INISIALISASI
$PengaturanUmum	= new PengaturanUmum();
$fee_transaksi	= $PengaturanUmum->ambilFeeTransaksi();
$fee_tiket			= $fee_transaksi['FeeTiket']; 
$fee_paket			= $fee_transaksi['FeePaket']; 
$fee_sms				= $fee_transaksi['FeeSMS']; 
		
//QUERY
//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}
	
//FEE PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
		IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket,
		IS_NULL(SUM(IF(FlagBatal=1,1,0)),0) AS TotalTiketBatal
	FROM tbl_reservasi
	WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1
	GROUP BY DATE(WaktuCetakTiket)
	ORDER BY DATE(WaktuCetakTiket) ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
//FEE PAKET
$sql=
	"SELECT 
		WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
	FROM tbl_paket
	WHERE MONTH(WaktuPesan)=$bulan AND YEAR(WaktuPesan)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY DATE(WaktuPesan)
	ORDER BY DATE(WaktuPesan)";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//FEE SMS
$sql=
	"SELECT 
		WEEKDAY(WaktuKirim)+1 AS Hari,DAY(WaktuKirim) AS Tanggal,
		SUM(JumlahPesan) AS TotalSMS
	FROM tbl_log_sms
	WHERE MONTH(WaktuKirim)=$bulan AND YEAR(WaktuKirim)=$tahun
	GROUP BY DATE(WaktuKirim)
	ORDER BY DATE(WaktuKirim)";

if ($result_sms = $db->sql_query($sql)){
	$data_sms = $db->sql_fetchrow($result_sms);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//FEE SPJ
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoSPJ),0) AS TotalSPJ
	FROM tbl_spj
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun  AND IsEkspedisi=0
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_spj = $db->sql_query($sql)){
	$data_spj = $db->sql_fetchrow($result_spj);
} 
else{
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
$sum_spj									= 0;
$sum_tiket								= 0;
$sum_tiket_batal					= 0;
$sum_fee_tiket						= 0;
$sum_paket								= 0;
$sum_fee_paket						= 0;
$sum_sms									= 0;
$sum_fee_sms							= 0;
$sum_total_fee						= 0;
	
	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,80);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Rekap Fee','',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,BulanString($bulan).' '.$tahun,'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
#$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(25,5,'Tanggal','B',0,'C',1);
$pdf->Cell(25,5,'Jum.SPJ','B',0,'C',1);
$pdf->Cell(25,5,'Jum.Tiket','B',0,'C',1);
$pdf->Cell(25,5,'Jum.Tiket Batal','B',0,'C',1);
$pdf->Cell(25,5,'Jum.Paket','B',0,'C',1);
$pdf->Cell(25,5,'Jum.SMS','B',0,'C',1);
$pdf->Cell(30,5,'Fee Tiket','B',0,'C',1);
$pdf->Cell(30,5,'Fee Paket','B',0,'C',1);
$pdf->Cell(30,5,'Fee SMS','B',0,'C',1);
$pdf->Cell(30,5,'Total Fee','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

  
for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$odd ='odd';
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
	
	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	//FEE PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_tiket				= $data_penumpang['TotalTiket']; 
		$total_tiket_batal	= $data_penumpang['TotalTiketBatal']; 
		$total_fee_tiket		= $total_tiket*$fee_tiket;
		$data_penumpang 		= $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_tiket				= 0;
		$total_tiket_batal	= 0;
		$total_fee_tiket		= 0;
	}
	
	//FEE PAKET
	if($data_paket['Tanggal']==$idx_tgl+1){
		$total_paket				= $data_paket['TotalPaket']; 
		$total_fee_paket		= ($fee_paket<=1)?$data_paket['TotalOmzetPaket']*$fee_paket:$total_paket*$fee_paket;
		$data_paket 				= $db->sql_fetchrow($result_paket);
	}
	else{
		$total_paket			= 0;
		$total_fee_paket	= 0;
	}
	
	//OMZET SMS
	if($data_sms['Tanggal']==$idx_tgl+1){
		$total_sms				= $data_sms['TotalSMS']; 
		$total_fee_sms		= $total_sms*$fee_sms;
		
		$data_sms 				= $db->sql_fetchrow($result_sms);
	}
	else{
		$total_sms				= 0;
		$total_fee_sms		= 0;
	}
	
	//FEE SPJ
	if($data_spj['Tanggal']==$idx_tgl+1){
		$total_spj		= $data_spj['TotalSPJ']; 
		$data_spj 		= $db->sql_fetchrow($result_spj);
	}
	else{
		$total_spj		= 0;
	}
	
	$total_fee		=	$total_fee_tiket+$total_fee_paket+$total_fee_sms;
	
	$sum_spj									+= $total_spj;
	$sum_tiket								+= $total_tiket;
	$sum_tiket_batal					+= $total_tiket_batal;
	$sum_fee_tiket						+= $total_fee_tiket;
	$sum_paket								+= $total_paket;
	$sum_fee_paket						+= $total_fee_paket;
	$sum_sms									+= $total_sms;
	$sum_fee_sms							+= $total_fee_sms;
	$sum_total_fee						+= $total_fee;
	
	#$pdf->Cell(5,5,$i,'',0,'C');
	$pdf->Cell(25,5,$tgl_transaksi.'  ','',0,'L');
	$pdf->Cell(25,5,number_format($total_spj,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_tiket,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_tiket_batal,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_paket,0,",","."),'',0,'R');
	$pdf->Cell(25,5,number_format($total_sms,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_fee_tiket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_fee_paket,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_fee_sms,0,",","."),'',0,'R');
	$pdf->Cell(30,5,number_format($total_fee,0,",","."),'',0,'R');
	$pdf->Ln();
	$pdf->Cell(270,1,'','B',0,'');
	$pdf->Ln();
	
	$i++;
	$temp_hari++;
}


//FOOTER
$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
#$pdf->Cell(5,5,'','B',0,'C',1);
$pdf->Cell(25,5,'TOTAL','B',0,'C',1);
$pdf->Cell(25,5,number_format($sum_spj,0,",","."),'',0,'R',1);
$pdf->Cell(25,5,number_format($sum_tiket,0,",","."),'',0,'R',1);
$pdf->Cell(25,5,number_format($sum_tiket_batal,0,",","."),'',0,'R',1);
$pdf->Cell(25,5,number_format($sum_paket,0,",","."),'',0,'R',1);
$pdf->Cell(25,5,number_format($sum_sms,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_fee_tiket,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_fee_paket,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_fee_sms,0,",","."),'',0,'R',1);
$pdf->Cell(30,5,number_format($sum_total_fee,0,",","."),'',0,'R',1);
$pdf->Ln();
$pdf->Ln();
									
$pdf->Output();
						
?>