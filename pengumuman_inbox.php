<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengumuman.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Pengumuman	= new Pengumuman();

	if ($mode=='baca'){
		// edit
		
		$id_pengumuman = $HTTP_GET_VARS['id_pengumuman'];
		
		$row=$Pengumuman->ambilDataDetail($id_pengumuman);
		
		$baca_pengumuman	= "Start('".append_sid('pengumuman_inbox.'.$phpEx)."');return false;";
		
		$template->set_filenames(array('body' => 'pengumuman/pengumuman_inbox_baca_body.tpl')); 
		$template->assign_vars(array(
			 'KODE'    	=> $row['KodePengumuman'],
			 'JUDUL_PENGUMUMAN'=> $row['JudulPengumuman'],
			 'PENGUMUMAN'=> $row['Pengumuman'],
			 'PEMBUAT'	=> $row['NamaPembuat'],
			 'WAKTU_BUAT'=> FormatMySQLDateToTgl($row['WaktuPembuatanPengumuman']),
			 'U_BACK'	=>"<a href='#' onClick=$baca_pengumuman>Kembali ke Inbox</a>"
			 )
		);
	} 
	else {
		// LIST
		
		$Pengumuman->resetPengumumanBaru($userdata['user_id']);
		
		$template->set_filenames(array('body' => 'pengumuman/pengumuman_inbox_body.tpl')); 
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdPengumuman","tbl_pengumuman","",$kondisi,"pengaturan_pengumuman.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT 
				IdPengumuman,KodePengumuman,JudulPengumuman,
				LEFT(Pengumuman,100) AS Pengumuman,WaktuPembuatanPengumuman,
				f_user_get_nama_by_userid(PembuatPengumuman) AS NamaPembuat
			FROM tbl_pengumuman
			ORDER BY IdPengumuman DESC LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$baca_pengumuman	= "Start('".append_sid('pengumuman_inbox.'.$phpEx)."&mode=baca&id_pengumuman=$row[IdPengumuman]');return false";
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'		=>$odd,
							'no'		=>$i,
							'tanggal'=>FormatMySQLDateToTgl($row['WaktuPembuatanPengumuman']),
							'kode'	=>$row['KodePengumuman'],
							'judul'	=>"<a href='#' onClick=$baca_pengumuman>".$row['JudulPengumuman']."</a>",
							'pengumuman'=>"<a href='#' onClick=$baca_pengumuman>".$row['Pengumuman']."....</a>",
							'pembuat'	=>$row['NamaPembuat'],
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada pengumuman</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load pengumuman',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'U_CABANG_ADD'		=> append_sid('pengaturan_pengumuman.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_pengumuman.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>