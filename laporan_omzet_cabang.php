<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################

$Cabang =new Cabang();

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_cabang/laporan_omzet_cabang_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$kondisi_cari2	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

//kondisi_cari update
$kondisi_cari   =($cari=="")?"IS NOT NULL ":
        " IN (SELECT KodeCabang
                FROM tbl_md_cabang
                WHERE (KodeCabang LIKE '$cari%'
                OR Nama LIKE '%$cari%'
                OR Kota LIKE '%$cari%'
                OR Telp LIKE '$cari%'
                OR Alamat LIKE '%$cari%'))";


if($userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])){
	$kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Nama":$sort_by;
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeCabang","tbl_md_cabang",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_omzet_cabang.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//MENGAMBIL DATA-DATA MASTER CABANG
$sql_cabang=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari2";

//echo $sql_cabang;
if (!$result_laporan = $db->sql_query($sql_cabang)){
	echo("Err:".__LINE__);exit;
}


//MENGAMBIL NILAI JADWAL YANG DIBUKA UNTUK PERCABANG DAN PER TANGGAL TERTENTU
$sql=
	"SELECT
	  f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang,
	  (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
	  -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
	  +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
	  AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
	WHERE 
		f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) IN (SELECT KodeCabang
		FROM tbl_md_cabang
		$kondisi_cari)
	GROUP BY KodeCabang
	ORDER BY KodeCabang";

//sementara
$sql=
        "SELECT
          f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang,
          (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
          -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
          +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
        FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
          AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
        WHERE
                f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan)
		$kondisi_cari
        GROUP BY KodeCabang
        ORDER BY KodeCabang";
//echo $sql;

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_jadwal_tersedia[$row['KodeCabang']]	= $row['JadwalDibuka'];
}


//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(IF(CetakTiket!=1,NoTiket,NULL)),0) AS TotalPenumpangB,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='') AND CetakTiket=1  AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(PetugasCetakTiket=0,NoTiket,NULL)),0) AS TotalPenumpangO,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS TotalTiket,
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,SubTotal,0),0)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//DATA KEBERANGKATAN BY MANIFEST
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(1),0) AS TotalBerangkat
	FROM tbl_spj
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['KodeCabang']]	= $row['TotalBerangkat'];
}

//DATA KEBERANGKATAN MANIFEST PICKUP/TRANSIT
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(DISTINCT(IF(CetakTiket=1,NoSPJ,NULL))),0) AS TotalBerangkat
	FROM tbl_reservasi tr
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 AND (SELECT FlagSubJadwal FROM tbl_md_jadwal tmj WHERE tmj.KodeJadwal=tr.KodeJadwal)=1 $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['KodeCabang']]	= $jumlah_berangkat[$row['KodeCabang']]+$row['TotalBerangkat'];
}


//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['KodeCabang']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND FlagJenisBiaya!='$FLAG_BIAYA_VOUCHER_BBM' $kondisi_cabang
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

$total_b	= 0;
$total_u	= 0;
$total_m	= 0;
$total_k	= 0;
$total_kk	= 0;
$total_v	= 0;
$total_g	= 0;
$total_r	= 0;
$total_vr	= 0;
$total_t	= 0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Nama']								= $row['Nama'];
	$temp_array[$idx]['Alamat']							= $row['Alamat'];
	$temp_array[$idx]['Kota']								= $row['Kota'];
	$temp_array[$idx]['Telp']								= $row['Telp'];
	$temp_array[$idx]['TotalOpenTrip']			= $data_jadwal_tersedia[$row['KodeCabang']];
	$temp_array[$idx]['TotalPenumpangB']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangB'];
	$temp_array[$idx]['TotalPenumpangU']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangG']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangV']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangV'];
	$temp_array[$idx]['TotalPenumpangO']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
	$temp_array[$idx]['TotalBerangkat']			= $jumlah_berangkat[$row['KodeCabang']];
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenjualanPaket']= $data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalPaket']					= $data_paket_total[$row['KodeCabang']]['TotalPaket'];
	$temp_array[$idx]['TotalBiaya']					= $data_biaya_total[$row['KodeCabang']]['TotalBiaya'];
	$temp_array[$idx]['TotalPenumpangPerTrip']= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']							= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'];
	
	$total_b	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangB'];
	$total_u	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangU'];
	$total_m	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$total_k	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangK'];
	$total_kk	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangKK'];
	$total_g	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangG'];
	$total_r	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$total_v	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangV'];
	$total_o	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
	$total_t	+= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan.reservasi.cabang.detail.php?tglmulai='.$tanggal_mulai.'&tglakhir='.$tanggal_akhir.'&kodecabang='.$temp_array[$idx]['KodeCabang'])."\");return false'>Detail<a/>&nbsp;+&nbsp;";		
	
	$act 	.="<a href='".append_sid('laporan_omzet_cabang_grafik.php?kode_cabang='.$temp_array[$idx]['KodeCabang'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&sort_by='.$sort_by.'&order='.$order)."'>Grafik<a/>";		
	
	//total tiket
	$total_penjualan_tiket	= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket						= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total biaya
	$total_biaya						= $temp_array[$idx]['TotalBiaya'];
	
	//total
	$total									= $temp_array[$idx]['Total'];
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'kode_cabang'=>$temp_array[$idx]['KodeCabang'],
				'cabang'=>$temp_array[$idx]['Nama'],
				'alamat'=>$temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Kota'],
				'open_trip'=>number_format($temp_array[$idx]['TotalOpenTrip'],0,",","."),
				'total_keberangkatan'=>number_format($temp_array[$idx]['TotalBerangkat'],0,",","."),
				'total_penumpang_b'=>number_format($temp_array[$idx]['TotalPenumpangB'],0,",","."),
				'total_penumpang_u'=>number_format($temp_array[$idx]['TotalPenumpangU'],0,",","."),
				'total_penumpang_m'=>number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."),
				'total_penumpang_k'=>number_format($temp_array[$idx]['TotalPenumpangK'],0,",","."),
				'total_penumpang_kk'=>number_format($temp_array[$idx]['TotalPenumpangKK'],0,",","."),
				'total_penumpang_g'=>number_format($temp_array[$idx]['TotalPenumpangG'],0,",","."),
				'total_penumpang_r'=>number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."),
				'total_penumpang_v'=>number_format($temp_array[$idx]['TotalPenumpangV'],0,",","."),
				'total_penumpang_o'=>number_format($temp_array[$idx]['TotalPenumpangO'],0,",","."),
				'total_penumpang'=>number_format($total_tiket,0,",","."),
				'rata_pnp_per_trip'=>number_format($temp_array[$idx]['TotalPenumpangPerTrip'],0,",","."),
				'total_omzet'=>number_format($total_penjualan_tiket,0,",","."),
				'total_paket'=>number_format($total_paket,0,",","."),
				'total_omzet_paket'=>number_format($total_penjualan_paket,0,",","."),
				'total_discount'=>number_format($total_discount,0,",","."),
				'total_biaya'=>number_format($total_biaya,0,",","."),
				'total_profit'=>number_format($total,0,",","."),
				'act'=>$act
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tglmulai=".$tanggal_mulai."&tglakhir=".$tanggal_akhir."&kodecabang=".$temp_array[$idx]['KodeCabang'].
										"&cari=".$cari."&sortby=".$sort_by."&order=".$order."";
											
$script_cetak_excel="Start('laporan_omzet_cabang_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$array_sort	= 
	"'".append_sid('laporan_omzet_cabang.php?sort_by=Nama'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=KodeCabang'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=Alamat'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalOpenTrip'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalBerangkat'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangPerTrip'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenjualanTiket'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPaket'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenjualanPaket'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalDiscount'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalBiaya'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=Total'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangB'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangU'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangM'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangK'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangKK'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangG'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangR'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangV'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalPenumpangO'.$parameter_sorting)."',".
	"'".append_sid('laporan_omzet_cabang.php?sort_by=TotalTiket'.$parameter_sorting)."'";

$page_title	= "Penjualan Cabang";

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('menu_lap_reservasi.'.$phpEx.'?top_menu_dipilih=top_menu_lap_reservasi') .'">Home</a> | <a href="'.append_sid('laporan_omzet_cabang.'.$phpEx).'">Laporan Omzet Cabang</a>',
	'ACTION_CARI'		=> append_sid('laporan_omzet_cabang.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['Nama'],
	'TOTAL_B'				=> number_format($total_b,0,",","."),
	'TOTAL_U'				=> number_format($total_u,0,",","."),
	'TOTAL_M'				=> number_format($total_m,0,",","."),
	'TOTAL_K'				=> number_format($total_k,0,",","."),
	'TOTAL_KK'			=> number_format($total_kk,0,",","."),
	'TOTAL_V'				=> number_format($total_v,0,",","."),
	'TOTAL_G'				=> number_format($total_g,0,",","."),
	'TOTAL_R'				=> number_format($total_r,0,",","."),
	'TOTAL_V'				=> number_format($total_v,0,",","."),
	'TOTAL_O'				=> number_format($total_o,0,",","."),
	'TOTAL_T'				=> number_format($total_t,0,",","."),
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'ARRAY_SORT'		=> $array_sort
	)
);
	     
				
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
