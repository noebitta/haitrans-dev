<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassDeposit.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Deposit	=  new Deposit();


//mengambil parameter filter
$filstatus 			= isset($HTTP_GET_VARS['filterstatus'])? $HTTP_GET_VARS['filterstatus'] : $HTTP_POST_VARS['filterstatus'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari						= $HTTP_POST_VARS["txt_cari"]!=""?$HTTP_POST_VARS["txt_cari"]:$HTTP_GET_VARS["cari"];

// BODY
$template->set_filenames(array('body' => 'deposit_tiketux_laporan_trx/index_body.tpl')); 

$kondisi_cari	= $cari==""?"":" AND (Keterangan LIKE '%$cari%')";

if($filstatus!=""){
	switch($filstatus){
		case "0"	:
			//kredit
			$kondisi_status	= " AND IsDebit=0 ";
			break;
		case "1"	:
			//Debit
			$kondisi_status	= " AND IsDebit=1 ";
			break;
	}
}


$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_tanggal	= " AND (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')";

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"ID","tbl_deposit_log_trx",
"&cari=$cari&filterstatus=$filstatus&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir",
"WHERE 1 $kondisi_cari $kondisi_tanggal $kondisi_status","deposit_tiketux_lap_trx.php",
$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//QUERY
$sql	= 
	"SELECT *
	FROM tbl_deposit_log_trx
	WHERE 1 $kondisi_cari $kondisi_tanggal $kondisi_status
	ORDER BY ID LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=0;

//PLOT DATA
while($row = $db->sql_fetchrow($result)){
	
	$i++;
	
	$odd = $i%2==0?"even":"odd";
				
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'waktu_trx'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])),
				'jenis_trx'=>$row['IsDebit']==1?"DB":"CR",
				'keterangan'=>$row['Keterangan'],
				'jumlah'=>number_format($row['Jumlah'],0,",","."),
				'saldo'=>number_format($row['Saldo'],0,",",".")
			)
		);
		
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai_mysql."&tanggal_akhir=".$tanggal_akhir_mysql."&cari=".$cari."&status=".$filstatus."";
													
$script_cetak_excel="Start('deposit_tiketux_lap_trx_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$temp_var		= "opt_status".$filstatus;
$$temp_var	= "selected";
	
$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('menu_tiketux.'.$phpEx.'?top_menu_dipilih=top_menu_tiketux') .'">Home</a> | <a href="'.append_sid('deposit_tiketux_lap_trx.'.$phpEx).'">Laporan Transaksi Deposit</a>',
	'ACTION_CARI'		=> append_sid('deposit_tiketux_lap_trx.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_STATUS'		=> $opt_status,
	'OPT_STATUS0'		=> $opt_status0,
	'OPT_STATUS1'		=> $opt_status1,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'SALDO_DEPOSIT'	=> number_format($Deposit->getSaldoDeposit(),0,",","."),
	'JUM_TRX_DEBIT'	=> number_format($Deposit->getTotalTransaksi($tanggal_mulai_mysql,$tanggal_akhir_mysql),0,",","."),
	'JUM_TRX_KREDIT'=> number_format($Deposit->getTotalTransaksi($tanggal_mulai_mysql,$tanggal_akhir_mysql,0),0,",",".")
	)
);

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>