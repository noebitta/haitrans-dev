<script language="JavaScript">

var kode;

function cekValTahun(tahun){
	cek_value=tahun*0;
	
	if(cek_value!=0){
		return false;
	}
	
	if((tahun*1)<1920 || (tahun*1>2050)){
		return false;
	}
	else{
		return true;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_kendaraan_invalid');
	Element.hide('no_polisi_invalid');
	Element.hide('km_invalid');
	Element.hide('tahun_pembuatan_invalid');
	
	kode_kendaraan		= document.getElementById('kode_kendaraan');
	no_polisi					= document.getElementById('no_polisi');
	no_stnk						= document.getElementById('no_stnk');
	km								= document.getElementById('km');
	tahun_pembuatan		= document.getElementById('tahun_pembuatan');
	
	if(kode_kendaraan.value==''){
		valid=false;
		Element.show('kode_kendaraan_invalid');
	}
	
	if(no_polisi.value==''){
		valid=false;
		Element.show('no_polisi_invalid');
	}
	
	if(!cekValTahun(tahun_pembuatan.value)){	
		valid=false;
		Element.show('tahun_pembuatan_invalid');
	}
	
	cek_value=km.value*0;
	
	if(cek_value!=0){
		valid=false;
		Element.show('km_invalid');
	}
	
	if(km.value==''){
		km.value=0;
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>
<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 box">
			<form name="frm_data_mobil" action="{U_ADD_ACT}" method="post" onSubmit='return validateInput();'>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td class="pad10" class="whiter" valign="middle" align="center">
				<table>
					<tr><td class="pad10" colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
					<tr>
						<td class="pad10" align='center' valign='top'>
							<table>   
								<tr>
									<td class="pad10" colspan=3><h2>{JUDUL}</h2></td>
								</tr>
								<tr>
									<td class="pad10" width="200">Kode Promo</td><td class="pad10" width='10'></td>
									<td class="pad10" width="450">
										<input class="form-control" type="hidden" name="kode_promo_old" id="kode_promo_old" value='{KODE_PROMO}'/>
										<input placeholder="Kode Promo" class="form-control" type="text" name="kode_promo" id="kode_promo" value='{KODE_PROMO}'/>
									</td>
								</tr>
								<tr>
									<td class="pad10">Nama Promo</td><td class="pad10"></td>
									<td class="pad10">
										<input placeholder="Nama Promo" class="form-control" type="text" name="nama_promo" id="nama_promo" value='{NAMA_PROMO}'/></td>
								</tr>
								<tr>
									<td class="pad10">Priority</td><td class="pad10"></td>
									<td class="pad10">
										<select class="form-control" name="priority" id="priority">
											<option value=0 {PRIOR_0}>Low</option>
											<option value=1 {PRIOR_1}>Medium</option>
											<option value=2 {PRIOR_2}>High</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="pad10">Asal</td><td class="pad10"></td>
									<td class="pad10"><select class="form-control" name="asal" id="asal">{ASAL}</select></td>
								</tr>
								<tr>
									<td class="pad10">Tujuan</td><td class="pad10"></td>
									<td class="pad10"><select class="form-control" name="tujuan" id="tujuan">{TUJUAN}</select></td>
								</tr>
								<tr>
									<td class="pad10">Tanggal Mulai Promo</td>
									<td class="pad10"></td>
									<td class="">
										<div class="col-md-6" style="padding: 0; padding-left: 10px; padding-top: 14px;">
											<input placeholder="Tanggal Mulai" class="form-control" id="tanggal_mulai_promo" name="tanggal_mulai_promo" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_MULAI}">
										</div>
										<div class="col-md-6" style="padding: 0; padding-left: 10px; padding-right: 10px;">
											Jam <br />
											<input  placeholder="Jam Mulai" class="form-control" type="text" name="mulai_jam" id="mulai_jam" size="15" maxlength=5 value='{JAM_MULAI}' />
										</div>
									</td>
								</tr>
								<tr>
									<td class="pad10">Tanggal Akhir Promo</td><td class="pad10"></td>
									<td class="" style="padding-top: 10px;">
										<div class="col-md-6" style="padding: 0; padding-left: 10px; padding-top: 14px;">
											<input  placeholder="Tanggal Berakhir" class="form-control" id="tanggal_akhir_promo" name="tanggal_akhir_promo" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
										</div>
										<div class="col-md-6" style="padding: 0; padding-left: 10px; padding-right: 10px;">
											Jam <br />
											<input  placeholder="Jam Berakhir" class="form-control" type="text" name="akhir_jam" id="akhir_jam" size="15" maxlength=5  value='{JAM_AKHIR}' />
										</div>
									</td>
								</tr>
								<tr>
									<td class="pad10">Discount</td><td class="pad10"></td>
									<td class="pad10"><input  placeholder="Discount" class="form-control" type="text" name="discount" id="discount" value='{DISCOUNT}'/></td>
								</tr>
								<tr>
									<td class="pad10">Target Discount</td><td class="pad10"></td>
									<td class="pad10">
										<select class="form-control" name="target_discount" id="target_discount"/>
											<option value=0 {TARGET_0}>-Semua-</option>
											<option value=1 {TARGET_1}>Member Saja</option>
											<option value=2 {TARGET_2}>Umum Saja</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class="pad10">Jumlah Minimum</td><td class="pad10"></td>
									<td class="pad10">
									<input class="form-control"  placeholder="Jumlah Minimum" type="text" name="jumlah_minimum" id="jumlah_minimum" value='{JUM_MINIMUM}'/></td>
								</tr>
								<tr>
						      <td class="pad10">Status Aktif</td><td class="pad10"></td>
									<td class="pad10">
										<select class="form-control" id="aktif" name="aktif">
											<option value=1 {AKTIF_1}>AKTIF</option>
											<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
										</select>
									</td>
						    </tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align='center' valign='middle' style="padding-bottom: 50px;">
						<input type="hidden" name="mode" value="{MODE}">
					  	<input type="hidden" name="submode" value="{SUB}">
						<div class="col-md-6 col-md-offset-3">
							<div class="col-md-6">
								<input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI" style="width:100px;">
							</div>
							<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
						</div>
					</td>
					</tr>           
				</table>
				</td>
			</tr>
			</table>
			</form>
			</div>
	</div>
</div>