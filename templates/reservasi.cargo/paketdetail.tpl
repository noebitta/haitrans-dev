<div style="margin-left: 5px;">
	<input type="button" value="INPUT DATA BARU" style="width:150px;height: 30px;" onclick="document.getElementById('showdatadetail').innerHTML='';"><br>
	<input type="hidden" id="idpaket" value="{ID}"/>
	
	<h2>AWB: {AWB}</h2>
	<input type="button" value="C E T A K   A W B" style="width:250px;height: 50px;" onclick="cetakAWB('{AWB}');"><br><br>
	<h2>Data Pengirim</h2>
	<span class="resvcargobooklabel">Telp Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{TELP_PENGIRIM}</span></br>	
	<span class="resvcargobooklabel">Nama Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{NAMA_PENGIRIM}</span></br>
	<span class="resvcargobooklabel">Alamat Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{ALAMAT_PENGIRIM}</span></br>
	
	<h2>Data Penerima</h2>
	<span class="resvcargobooklabel">Telp Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{TELP_PENERIMA}</span></br>	
	<span class="resvcargobooklabel">Nama Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{NAMA_PENERIMA}</span></br>
	<span class="resvcargobooklabel">Alamat Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{ALAMAT_PENERIMA}</span></br>	
	
	<h2>Data Barang</h2>
	<span class="resvcargobooklabel">Asal (Origin)</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{KOTA_ASAL}</span></br>
	
	<span class="resvcargobooklabel">Tujuan (Destination)</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{KOTA_TUJUAN}</span></br>
	
	<span class="resvcargobooklabel">Jenis</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{JENIS_BARANG}</span></br>

	<span class="resvcargobooklabel">Via</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{VIA}</span></br>

	<span class="resvcargobooklabel">Service</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{LAYANAN}</span></br>
	
	<span class="resvcargobooklabel">Koli</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{KOLI} Pax</span></br>
	<span class="resvcargobooklabel">Perhitungan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{PERHITUNGAN}</span><br>
	
	<span style="display: {SHOW_DIMENSI};">
		<span class="resvcargobooklabel">Dimensi P x L x T</span><span class="resvcargobookfield">:</span>
		<span class="resvcargobookfield">{DIMENSI_PANJANG} cm x {DIMENSI_LEBAR} cm x {DIMENSI_TINGGI} cm</span>
	</span>
	
	<span class="resvcargobooklabel">Berat</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{BERAT} Kg</span><br>
	<span class="resvcargobooklabel">Biaya Kirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">Rp.{BIAYA_KIRIM}</span></br>
	<span class="resvcargobooklabel">Lead Time</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{LEAD_TIME} Hari</span></br>
	<span class="resvcargobooklabel">Biaya Tambahan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">Rp. {BIAYA_TAMBAHAN}</span></br>
	<span class="resvcargobooklabel">Biaya Packing</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">Rp. {BIAYA_PACKING}</span></br>
	
	<span class="resvcargobooklabel">Asuransi</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{IS_ASURANSI}</span>
	<span style="display:{SHOW_ASURANSI};">
		<span class="resvcargobooklabel">Harga Pengakuan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">Rp. {HARGA_DIAKUI}</span></br>
		<span class="resvcargobooklabel">Biaya Asuransi</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">Rp. {BIAYA_ASURANSI}(2 permil)</span>
	</span>
	
	<span class="resvcargobooklabel">Total Biaya</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">Rp. {TOTAL_BIAYA}</span></br>
	
	<span class="resvcargobooklabel">Cara Pembayaran</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{CARA_BAYAR}</span></br>
							
	<span class="resvcargobooklabel">Isi Barang (Description)</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{DESKRIPSI_BARANG}</span></br>
	<hr>
	<span class="resvcargobooklabel">CSO</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{CSO}</span></br>
	<span class="resvcargobooklabel">Waktu Terima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{WAKTU_TERIMA}</span></br>
	
	<!-- BEGIN INFO_MUTASI -->
	<hr>
	<h2>Mutasi</h2>
	<span class="resvcargobooklabel">CSO</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.cso}</span></br>
	<span class="resvcargobooklabel">Waktu Mutasi</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.waktu}</span></br>
	<span class="resvcargobooklabel">Tgl Lama</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.tgl}</span></br>
	<span class="resvcargobooklabel">Point Lama</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">{INFO_MUTASI.point}</span></br>
	<!-- END INFO_MUTASI -->
	<br><br>
	<center>
		<input type="button" value="B A T A L" style="width:250px;height: 30px;" onclick="{ACTION_BATAL}"><br><br>
		<!-- BEGIN TOMBOL_MUTASI -->
		<input type="button" value="M U T A S I" id="tombolmutasipaket" style="width:250px;height: 30px;" onclick="setFlagMutasiPaket();"><br>
		<!-- END TOMBOL_MUTASI -->
	</center>
	
</div>