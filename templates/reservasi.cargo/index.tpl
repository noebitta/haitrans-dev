<input type="hidden" value="{SID}" id="hdn_SID">

<input type="hidden" value=0 id="ismodemutasion">

<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi.cargo.js"></script>

<!--dialog Paket-->
<div dojoType="dialog" id="dialog_cari_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='900'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Paket</h2></td></tr>
			<tr><td><div id="rewrite_cari_paket"></div></td></tr>
		</table>
		<span id='progress_cari_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_paket_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog Paket-->

<!--dialog ambil paket-->
<div dojoType="dialog" id="dialog_ambil_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<div id="rewrite_ambil_paket"></div>
		<span id='progress_ambil_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_ambil_paket.hide();" id="dlg_ambil_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="prosesAmbilPaket();" id="dlg_ambil_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Ambil&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog ambil paket-->

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
			<tr><td>
				<input type='hidden' id='kode_booking_go_show' value='' />
				<table width='100%'>
					<tr>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(0);return false;"><img src="{TPL}images/icon_tunai.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(0);return false;"><span class="genmed">Tunai</span></a>    
						</td>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(1);return false;"><img src="{TPL}images/icon_debitcard.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(1);return false;"><span class="genmed">Debit Card</span></a> 
						</td>
					</tr>
					<tr>
						<td align='center' valign='middle'>
							<a href="" onClick="CetakTiket(2);return false;"><img src="{TPL}images/icon_mastercard.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(2);return false;"><span class="genmed">Credit Card</span></a>    
						</td>
						<!--<td align='center' valign='middle'>
							<a href="" onClick="CetakTiket(3);return false;"><img src="{TPL}images/icon_voucher.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(3);return false;"><span class="genmed">Voucher</span></a>    
						</td>-->
					</tr>
				</table>
			</td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="reset" id="dialog_pembayaran_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Pilih Pembayaran-->

<!--dialog SPJ-->
<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<div style="background: #ffffff;text-align: center;color: #606060;width: 300px;height: 100px;">
	<h2>Cetak Manifest</h2>
	<div id="showlistpembawa"></div>
	<br>
	<input type="button" value="CANCEL" style="width: 100px;height: 20px;" onclick="dialog_SPJ.hide();"/>&nbsp;&nbsp;<input type="button" value="CETAK" style="width: 100px;height: 20px;" onclick="cetakSPJ();"/>
</div>
</form>
</div>
<!--END dialog SPJ-->

<input type="hidden" id="hargaperkg" value=""/>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="whiter" valign="middle" align="center">
 
<table width="100%" cellspacing="0" cellpadding="4" border="0">
<tr>
 <td valign="middle" align="left">    
  <table width="100%" class='reservasi_background' cellpadding='0' cellspacing='0'>
		<tr>
			       
			<td width=1 bgcolor='e0e0e0'></td>
			<!--LAYOUT DATA ISIAN PENUMPANG-->
			<td valign='top' width='400'>
				

			</td>
    </tr>        
  </table>   
 </td>
</tr>
</table>

</td>
</tr>
</table>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="col-md-12 box box-2" style="min-height: 110px; margin-bottom: 15px;">
				<font color='505050'>Cari Paket (Masukkan No. Resi Paket/No.Telp Pelanggan)</font><br>
				<div class="input-group" style="width: 100%;">
			      <input class="form-control" name="txt_cari_paket" id="txt_cari_paket" type="text">
			      <span class="input-group-btn">
			        <input class='tombol form-control' name="btn_periksapaket" id="btn_periksapaket" value="Cari" type="button" onClick="periksaPaket(txt_cari_paket.value)">
			      </span>
			    </div><!-- /input-group -->
			</div>
		</div>
		<div class="col-md-8">
			<div class="col-md-12 box box-2" style="min-height: 110px;">
				<div class="menuiconsmall notifikasi_pengumuman" id="rewritepengumuman"></div>
				<div class="menuiconsmall"><a href="#" onClick="{U_LAPORAN_UANG}">
				<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g id="Laporan_Uang">
						<path d="M341.256,70.663c5.952,0,10.781,4.829,10.781,10.782c0,5.952-4.829,10.781-10.781,10.781H28.565v388.292H675.42V92.225
							H384.312c-5.951,0-10.781-4.829-10.781-10.781c0-5.952,4.829-10.782,10.781-10.782h301.619h0.269
							c5.952,0,10.782,4.829,10.782,10.782v409.586v0.267c0,5.952-4.829,10.782-10.782,10.782H18.075h-0.292
							c-5.952-0.001-10.781-4.83-10.781-10.782V81.735v-0.292c0-5.952,4.829-10.782,10.781-10.782L341.256,70.663L341.256,70.663z"/>
						<path d="M330.453,16.735c0-5.929,4.829-10.736,10.736-10.736c5.929,0,10.736,4.806,10.736,10.736l0.023,64.687
							c0,5.929-4.806,10.736-10.736,10.736s-10.736-4.806-10.736-10.736L330.453,16.735z"/>
						<path d="M330.497,534.243c0-5.932,4.806-10.737,10.736-10.737s10.736,4.807,10.736,10.737l-0.045,107.877
							c0,5.906-4.806,10.734-10.736,10.734s-10.736-4.806-10.736-10.734L330.497,534.243z"/>
						<path d="M179.902,531.301c1.618-5.727,7.569-9.029,13.296-7.412c5.705,1.618,9.029,7.569,7.412,13.296l-43.034,150.977
							c-1.617,5.728-7.569,9.031-13.296,7.412c-5.705-1.617-9.029-7.569-7.412-13.296L179.902,531.301z"/>
						<path d="M481.813,537.185c-1.618-5.727,1.684-11.678,7.412-13.296c5.727-1.619,11.678,1.684,13.296,7.412l43.102,150.977
							c1.618,5.706-1.683,11.679-7.411,13.296c-5.707,1.619-11.679-1.684-13.297-7.412L481.813,537.185z"/>
						<path d="M17.783,545.228c-5.952,0-10.781-4.828-10.781-10.781c0-5.953,4.829-10.781,10.781-10.781h668.418
							c5.952,0,10.782,4.828,10.782,10.781c0,5.928-4.829,10.781-10.782,10.781H17.783z"/>
						<path d="M332.877,329.514l-34.385,38.693l-77.849-97.276c-0.516-0.741-1.123-1.436-1.841-2.043
							c-4.514-3.863-11.297-3.346-15.161,1.168l-89.616,104.149l-53.119-0.09c-5.929,0-10.736,4.805-10.736,10.736
							c0,5.926,4.806,10.734,10.736,10.734l58.061,0.093c3.256,0,6.176-1.44,8.175-3.776l0.023,0.024l84.383-98.085l78.185,97.702
							c3.683,4.625,10.444,5.369,15.071,1.684c0.471-0.402,0.92-0.81,1.325-1.257l0.023,0.023l26.726-30.072V329.514z"/>
						<g>
							<path d="M591.92,295.7c-0.007-0.059-0.011-0.118-0.02-0.176c-0.042-0.289-0.097-0.578-0.175-0.863
								c-0.001-0.003-0.002-0.006-0.003-0.009c-0.073-0.266-0.166-0.529-0.271-0.789c-0.03-0.074-0.063-0.146-0.095-0.219
								c-0.083-0.188-0.175-0.372-0.276-0.554c-0.044-0.079-0.086-0.159-0.133-0.237c-0.025-0.042-0.045-0.086-0.072-0.128
								c-0.106-0.168-0.222-0.326-0.339-0.483c-0.022-0.029-0.04-0.06-0.062-0.089c-0.179-0.232-0.372-0.447-0.575-0.652
								c-0.038-0.039-0.079-0.074-0.119-0.112c-0.176-0.17-0.359-0.329-0.549-0.478c-0.049-0.038-0.096-0.076-0.146-0.112
								c-0.255-0.189-0.518-0.365-0.792-0.518l-105.478-59.956l60.731-38.242l101.814,57.872l-42.879,27.001
								c-3.346,2.106-4.351,6.527-2.244,9.873c2.106,3.346,6.527,4.351,9.873,2.244l53-33.373c2.131-1.341,3.4-3.703,3.343-6.22
								c-0.057-2.517-1.43-4.818-3.62-6.063l-115.937-65.901c-2.293-1.304-5.119-1.24-7.353,0.165l-74.428,46.867
								c-0.015,0.009-0.031,0.019-0.046,0.029l-37.192,23.42c-0.017,0.011-0.034,0.021-0.051,0.032l-74.479,46.9
								c-2.131,1.341-3.4,3.703-3.343,6.22c0.057,2.517,1.43,4.818,3.62,6.063l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936
								c1.327,0,2.651-0.369,3.815-1.101l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873c-2.106-3.346-6.527-4.352-9.873-2.244
								l-49.373,31.09l-101.814-57.874l60.732-38.243l108.589,61.724v81.75c0,2.608,1.417,5.008,3.699,6.268
								c1.079,0.596,2.27,0.891,3.46,0.891c1.327,0,2.652-0.369,3.815-1.101l37.24-23.45c2.082-1.311,3.345-3.598,3.345-6.058v-85.915
								c0-0.042-0.007-0.082-0.009-0.125C591.964,296.176,591.948,295.938,591.92,295.7z M554.737,392.935v-72.946
								c0-2.575-1.383-4.952-3.622-6.224l-105.54-59.991l23.493-14.794l101.814,57.873l-8.914,5.613
								c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346c1.303,0,2.623-0.356,3.807-1.102l8.061-5.075v68.994
								L554.737,392.935z"/>
							<path d="M655.481,272.22l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873C663.247,271.117,658.826,270.112,655.481,272.22
								z"/>
							<path d="M522.283,356.094l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,354.991,525.629,353.986,522.283,356.094z"/>
							<path d="M655.481,300.858l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873C663.247,299.755,658.826,298.75,655.481,300.858
								z"/>
							<path d="M522.283,384.732l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,383.629,525.629,382.624,522.283,384.732z"/>
							<path d="M655.481,329.496l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873
								C663.247,328.394,658.826,327.389,655.481,329.496z"/>
							<path d="M522.283,413.37l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,412.268,525.629,411.262,522.283,413.37z"/>
						</g>
					</g>
				</svg>
				<br>Laporan Uang</a></div>
				<div class="menuiconsmall">
					<a href="#" onClick="{U_LAPORAN_PENJUALAN}">
						<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
							<g id="Laporan_Data_Paket">
								<path style="display:inline;" d="M341.256,70.663c5.952,0,10.781,4.829,10.781,10.782c0,5.952-4.829,10.781-10.781,10.781H28.565
									v388.292H675.42V92.225H384.312c-5.951,0-10.781-4.829-10.781-10.781c0-5.952,4.829-10.782,10.781-10.782h301.619h0.269
									c5.952,0,10.782,4.829,10.782,10.782v409.586v0.267c0,5.952-4.829,10.782-10.782,10.782H18.075h-0.292
									c-5.952-0.001-10.781-4.83-10.781-10.782V81.735v-0.292c0-5.952,4.829-10.782,10.781-10.782L341.256,70.663L341.256,70.663z"/>
								<path style="display:inline;" d="M330.453,16.735c0-5.929,4.829-10.736,10.736-10.736c5.929,0,10.736,4.806,10.736,10.736
									l0.023,64.687c0,5.929-4.806,10.736-10.736,10.736s-10.736-4.806-10.736-10.736L330.453,16.735z"/>
								<path style="display:inline;" d="M330.497,534.243c0-5.932,4.806-10.737,10.736-10.737s10.736,4.807,10.736,10.737l-0.045,107.877
									c0,5.906-4.806,10.734-10.736,10.734s-10.736-4.806-10.736-10.734L330.497,534.243z"/>
								<path style="display:inline;" d="M179.902,531.301c1.618-5.727,7.569-9.029,13.296-7.412c5.705,1.618,9.029,7.569,7.412,13.296
									l-43.034,150.977c-1.617,5.728-7.569,9.031-13.296,7.412c-5.705-1.617-9.029-7.569-7.412-13.296L179.902,531.301z"/>
								<path style="display:inline;" d="M481.813,537.185c-1.618-5.727,1.684-11.678,7.412-13.296c5.727-1.619,11.678,1.684,13.296,7.412
									l43.102,150.977c1.618,5.706-1.683,11.679-7.411,13.296c-5.707,1.619-11.679-1.684-13.297-7.412L481.813,537.185z"/>
								<path style="display:inline;" d="M17.783,545.228c-5.952,0-10.781-4.828-10.781-10.781c0-5.953,4.829-10.781,10.781-10.781h668.418
									c5.952,0,10.782,4.828,10.782,10.781c0,5.928-4.829,10.781-10.782,10.781H17.783z"/>
								<path style="display:inline;" d="M332.877,329.514l-34.385,38.693l-77.849-97.276c-0.516-0.741-1.123-1.436-1.841-2.043
									c-4.514-3.863-11.297-3.346-15.161,1.168l-89.616,104.149l-53.119-0.09c-5.929,0-10.736,4.805-10.736,10.736
									c0,5.926,4.806,10.734,10.736,10.734l58.061,0.093c3.256,0,6.176-1.44,8.175-3.776l0.023,0.024l84.383-98.085l78.185,97.702
									c3.683,4.625,10.444,5.369,15.071,1.684c0.471-0.402,0.92-0.81,1.325-1.257l0.023,0.023l26.726-30.072V329.514z"/>
								<g style="display:inline;">
									<path d="M648.271,230.117l0.197-0.522l-141.229-55.17L366.24,230.167l-0.46-0.183v0.357l-0.02,0.011l0.02,0.06V407.38
										l141.395,55.423l141.415-55.425V229.987L648.271,230.117z M507.108,185.27l47.902,17.155l-130.581,50.697l-44.234-17.448
										L507.108,185.27z M502.135,450.301l-52.957-21.38l-26.577-9.985l-46.715-18.429V244.833l46.247,18.242v38.926l4.121-1.151
										l2.785,3.827l3.878-0.905l3.756,3.967l3.278-1.129l5.291,5.247V272.19l4.116,1.625l52.779,20.816L502.135,450.301L502.135,450.301
										z M507.187,285.771L455.553,265.4l127.345-51.611l52.52,21.396L507.187,285.771z M638.492,400.507l-126.258,49.794v-155.67
										L638.48,244.83v155.673L638.492,400.507L638.492,400.507z"/>
								</g>
							</g>
						</svg>
						<br>Laporan Paket
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="col-md-12 box box-2">
				<span class='formHeader sectiontitle'>1. Jadwal</span>
					<div class="col-md-12" style="text-align: center;">
						<h5>Tanggal Keberangkatan</h5>
						<!--  kolom kalender -->
							<!--  kolom kalender -->
							<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
							</iframe>
		
							<form name="formTanggal">
								<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />			
							</form>
							<!--end kolom kalender-->
							
							<input  class="mybutton paper paper-lift" style="width: 190px; margin: 0 auto;" name="update" onclick="getUpdateAsal(kota_asal.value,1)" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></input>
					</div>
					
					<div class="col-md-12">
						<br />Kota Keberangkatan<br>
						<div class="table-reservasi">
							<select class='form-control' onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select><br />
							<div id="showpointpickup"></div>
						</div>
						<hr style="margin-top: 20px; padding: 0; ">
						
					</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-12 box box-2">
				<span class='formHeader sectiontitle'>2. Daftar Barang</span>
				<br>
				<div id='loadingrefresh' class="resvcargoloadingcirc"></div>
				<div id="showlistpaket"><!-- LAYOUT PAKET--></div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-12 box box-2">
				<span class="formHeader sectiontitle">3. Data Paket Cargo</span><br />
				<br>							
				<!--data pelanggan-->
				<div id='loadingdetail' class="resvcargoloadingcirc"></div>
				<div id="showdatadetail" align='left'></div>
				
				<!--data isian pengiriman paket-->
				<form onsubmit="return false;">
					<div style="margin-left: 5px;">


						<table class="table">
			            	<thead><td><h4>Data Pengirim</h4></td></thead>
			            	<tr><td>Telp Pengirim<br /><input class="form-control" id='telppengirim' type='text' maxlength='15' onkeypress='validasiNoTelp(event);' onBlur="cariDataPelangganByTelp(this)" onfocus="this.style.background='';"/></span></td></tr>
			            	<tr><td>
			            		Nama Pengirim <span id='loadingcaritelppengirim' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=1 color="777777">&nbsp;mencari</font></span></span>
			            		<br />
			            		<input class="form-control" id='namapengirim' type='text' maxlength='100' onfocus="this.style.background='';"/>
			            	</td></tr>
			            	<tr><td><span class="resvcargobooklabel">Alamat Pengirim</span><br />
			            		<input class="form-control" id='alamatpengirim' type='text' maxlength='100' onfocus="this.style.background='';"/>
			            	</td></tr>
			            </table>


						<table class="table">
			            	<thead><td><h4>Data Penerima</h4></td></thead>
			            	<tr><td>Telp Penerima<br /><input class="form-control" id='telppenerima' type='text' maxlength='15' onkeypress='validasiNoTelp(event);' onBlur="cariDataPelangganByTelp(this)" onfocus="this.style.background='';"/></span></td></tr>
			            	<tr><td>
			            		Nama Penerima <span id='loadingcaritelppenerima' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=1 color="777777">&nbsp;mencari</font></span></span>
			            		<br />
			            		<input class="form-control" id='namapenerima' type='text' maxlength='100' onfocus="this.style.background='';"/>
			            	</td></tr>
			            	<tr><td><span class="resvcargobooklabel">Alamat Penerima</span><br />
			            		<input class="form-control" id='alamatpenerima' type='text' maxlength='100' onfocus="this.style.background='';"/>
			            	</td></tr>
			            </table>

						
						<table class="table">
			            	<thead><td><h4>Data Barang</h4></td></thead>
			            	<tr><td>Asal (Origin)<br />
							<span class="resvcargobookfield" style="width: 100%;">
								<div class="col-md-6" style="padding-left: 0;">
									<select class='form-control' id='provasal' onChange="setKotaAsal(this.value);" onfocus="this.style.background='';">
										<!-- BEGIN PROV -->
										<option value="{PROV.value}">{PROV.nama}</option>
										<!-- END PROV -->
									</select>&nbsp;
								</div>
								<div class="col-md-6">
									<span id="showoptkotaasal"></span>
								</div>
							</span>
			            	</td></tr>

			            	<tr><td>Tujuan (Destination)<br />
							<span class="resvcargobookfield" style="width: 100%;">
								<div class="col-md-6" style="padding-left: 0;">
									<select class='form-control' id='provtujuan' onChange="setKotaTujuan(this.value);" onfocus="this.style.background='';">
										<!-- BEGIN PROV -->
										<option value="{PROV.value}">{PROV.nama}</option>
										<!-- END PROV -->
									</select>&nbsp;
								</div>
								<div class="col-md-6">
									<span id="showoptkotatujuan"></span>
								</div>
							</span>
			            	</td></tr>

			            	<tr><td>Jenis<br />
								<select class='form-control' id='jenis'>
									<option value="DOK">Dokumen</option>
									<option value="PRC">Parcel</option>
								</select>
							</td></tr>

							<tr><td>Via<br />
								<select class='form-control' id='via'>
									<option value="DRT">Darat</option>
									<option value="UDR">Udara</option>
								</select>
							</td></tr>

							<tr><td>Service<br />
								<select class='form-control' id='service'>
									<option value="REG">Reguler</option>
									<option value="URG">Urgent</option>
								</select>
							</td></tr>

							<tr><td>Koli<br />
								<div class="input-group" style="width: 100%;">
							      <input class="form-control" id='koli' type='text' style='margin: 0;' size='10' maxlength='10' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" />
							      <span class="input-group-addon">
							        Pax
							      </span>
							    </div><!-- /input-group -->
							</td></tr>
			            	
			            	<tr><td>Perhitungan<br />
								<div class="radio">
								  <label>
								    <input type="radio" name="isvolumetric" id="isvolumetric0" onclick="setFormVolumetric();" selected/> Weight
								  </label>
								</div>
								<div class="radio">
								  <label>
								    <input type="radio" name="isvolumetric" id="isvolumetric1" onclick="setFormVolumetric();"/>Volumetric (PxLxT)
								  </label>
								</div>
							</td></tr>
			            </table>
						
						
						<span id="showinputvolumetric" style="display: none; width: 100%;">
							Dimensi P x L x T <br />

							<div class="input-group " style="width: 100%; font-size: 14px;">
						      <input class="form-control" id='dimensipanjang' type='text' style='text-align: right; margin: 0;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="P" onblur="hitungHargaPaket();"/>
						      <span class="input-group-addon">
						        cm
						      </span>
						    </div><!-- /input-group -->
							<br />
						    <div class="input-group " style="width: 100%; font-size: 14px;">
						      <input class="form-control" id='dimensilebar' type='text' style='text-align: right; margin: 0;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="L" onblur="hitungHargaPaket();"/>
						      <span class="input-group-addon">
						        cm
						      </span>
						    </div><!-- /input-group -->
							<br />
							<div class="input-group " style="width: 100%; font-size: 14px;">
						      <input class="form-control" id='dimensitinggi' type='text' style='text-align: right; margin: 0;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="T" onblur="hitungHargaPaket();"/>
						      <span class="input-group-addon">
						        cm
						      </span>
						    </div><!-- /input-group -->
							
							<table class="table">
								<tr>
									<td>
										<span class="resvcargobooklabel">Berat</span><span class="resvcargobookfield">&nbsp;<span id="beratvolumetric"></span>&nbsp;Kg</span>
									</td>
								</tr>
							</table>

						</span>
						
						<span id="showinputweight" style="display: none; width: 100%;">
							<table class="table">
								<tr>
									<td>
									Berat<br />
									<div class="input-group " style="width: 100%; font-size: 14px;">
								      <input class="form-control" id='berat' type='text' style='text-align: right; margin: 0;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" onblur="hitungHargaPaket();"/>
								      <span class="input-group-addon">
								        Kg
								      </span>
								    </div><!-- /input-group -->
									</td>
								</tr>
							</table>

							<table class="table">
								<tr>
									<td>
										<span class="resvcargobooklabel">Berat</span><span class="resvcargobookfield">&nbsp;&nbsp;Kg</span>
									</td>
								</tr>
							</table>

							
						</span>
						
						<table class="table">
							<tr>
								<td>Biaya Kirim</td>
								<td>Rp.<span id="biayakirim"></span></td>
							</tr>

							<tr>
								<td>Lead Time</td>
								<td>
									<span class="resvcargobookfield">&nbsp;<span id="leadtime"></span>&nbsp;Hari</span>
								</td>
							</tr>

							<tr>
								<td>Biaya Tambahan</td>
								<td>
									<div class="input-group" style="width: 100%;">
									  <span class="input-group-addon">
								      	Rp. 
								      </span>
								      <input class="form-control" id='biayatambahan' type='text' style='text-align: right; margin: 0;' size='10' maxlength='10' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" onblur="hitungHargaPaket();" />
								      
								    </div><!-- /input-group -->
								</td>
							</tr>

							<tr>
								<td>Biaya Packing</td>
								<td>
									<div class="input-group" style="width: 100%;">
									  <span class="input-group-addon">
								      	Rp. 
								      </span>
								      <input class="form-control"  style='text-align: right; margin: 0;' size='10' maxlength='10' name="txt_cari_jadwal" id="txt_cari_jadwal" type="text" class="form-control">
								      
								    </div><!-- /input-group -->
								</td>
							</tr>

							<tr>
								<td>Asuransi</td>
								<td>
									<div class="radio">
									  <label>
									    <input type="radio" name="isasuransi" id="isasuransi0" onclick="setFormAsuransi();" selected/>Tidak
									  </label>
									</div>
									<div class="radio">
									  <label>
									    <input type="radio" name="isasuransi" id="isasuransi1" onclick="setFormAsuransi();"/>Ya
									  </label>
									</div>
								</td>
							</tr>
						</table>

						<span id="showinputisasuransi" style="display: none; width: 100%;">
							<table class="table">
								<tr>
									<td>Harga Pengakuan</td>
									<td>
										<div class="input-group" style="width: 100%;">
									      <span class="input-group-addon">
											Rp.
									      </span>
									      <input class="form-control" id='hargadiakui' type='text' style='margin: 0; text-align: right;' size='10' maxlength='10' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" onblur="hitungHargaPaket();"/>
									    </div><!-- /input-group -->
									</td>
								</tr>
								<tr>
									<td>Biaya Asuransi</td>
									<td>
										Rp.<span id="biayaasuransi"></span>&nbsp;(2 permil)
									</td>
								</tr>
							</table>
						</span>

						<table class="table">
							<tr>
								<td>Total Biaya</td>
								<td>Rp.<span id="totalbiaya"></span></td>
							</tr>

							<tr>
								<td colspan="2">Cara Pembayaran<br />
										<select class='form-control' id='istunai'>
											<option value="1">Tunai</option>
											<option value="0">Kredit</option>
											</select>
									
								</td>
							</tr>

							<tr>
								<td colspan="2">
									Isi Barang (Description)<br />
									<textarea class="form-control" id='deskripsibarang' rows='3' cols='30' onfocus="this.style.background='';" ></textarea>
								</td>
							</tr>

							<tr>
								<td colspan="2">
									<div class="col-md-6" style="padding-left: 0;">
										<input class="btn mybutton paper topwidth" style="background: #B6B6B6;" type="button" onClick="resetIsianPaket();" id="button_cancel" value="Reset">
									</div>
									<div class="col-md-6" style="padding-right: 0;">
										<input class="btn mybutton paper topwidth" type="button" onclick="simpanPaket();" id="button_ok" value="Simpan">
									</div>
								</td>
							</tr>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>