<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
		window.opener.getUpdatePaket();
	}
</script>	 

<body class='tiket'>
<div style="font-size:14px;width:300px;">
	<span style="font-size:22px;">{NAMA_PERUSAHAAN} CARGO</span><br>
	<span style="font-size: 14px;font-weight: bold;text-transform: uppercase;">
		#Mnf :{NO_SPJ}<br>
	</span>
	<!-- BEGIN DATA_CARGO -->
	-------------------------------<br>
	<span style="font-size: 14px;font-weight: bold;text-transform: uppercase;">
		[{DATA_CARGO.no}] AWB: {DATA_CARGO.awb}<br>
	</span>
	<span class="resicargolabel">[{DATA_CARGO.kota_asal}-{DATA_CARGO.kota_tujuan}]</span></br>
	<span class="resicargolabel">Pengirim</span><span class="resicargofield">:{DATA_CARGO.pengirim}</span></br>
	<span class="resicargolabel">Penerima</span><span class="resicargofield">:{DATA_CARGO.penerima}</span></br>
	
	<span class="resicargolabel">Koli/Berat</span><span class="resicargofield">:</span><span class="resicargofield">{DATA_CARGO.koli} Pax/{DATA_CARGO.berat} Kg</span></br>
	<!--<span class="resicargolabel">Isi Barang</span><span class="resicargofield">:</span><span class="resicargofield">{DATA_CARGO.deskripsi_barang}</span></br>-->
	<!-- END DATA_CARGO -->
	-------------------------------<br>
	<span class="resicargolabel">TTD. CSO</span><span class="resicargofield"></span><br><br><br>
	<span class="resicargolabel">{CSO}</span></br>
	-------------------------------<br>
	<span class="resicargolabel">TTD. Pembawa</span><span class="resicargofield"></span><br><br><br>
	<span class="resicargolabel">{PEMBAWA}</span></br>
	-------------------------------<br>
	<span class="resicargolabel">Waktu Bawa</span><span class="resicargofield">:</span><span class="resicargofield">{WAKTU_BAWA}</span></br>
</div>
</body>


<script language="javascript">
	printWindow();
	window.close();
</script>
</html>