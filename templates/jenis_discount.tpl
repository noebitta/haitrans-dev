<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">
// global


function ValidasiAngka(objek,kolom){
	temp_nilai=objek.value*0;
	
	if(temp_nilai!=0){
		alert(kolom+" harus angka!");
		objek.setFocus;exit;
	}
	
}

function TampilkanSemuaData(){

	document.getElementById('txt_nama_discount').value=""; 
	document.getElementById('txt_besar_discount').value=""; 
	
	new Ajax.Updater("rewrite_hasil","jenis_discount.php?sid={SID}", 
	{
		asynchronous: true,
		method: "get",

		parameters: "mode=tampilkan_data",
		onLoading: function(request) 
		{
			Element.show('progress_data');
		},
		onComplete: function(request) 
		{
			Element.hide('progress_data');
		},
		onFailure: function(request) 
		{ 
			assignError(request.responseText); 
		}
	});   
}

function simpan(id_discount){
  //menyimpan
	
	temp_var	= 'txt'+id_discount;
	besar_discount	= document.getElementById(temp_var); 
	
	ValidasiAngka(besar_discount,"Besar discount");
	
	//operasi pengubahan account
	my_parameter="id_discount="+id_discount+"&besar_discount="+besar_discount.value+"&mode=ubah";
	
	
	new Ajax.Request("jenis_discount.php?sid={SID}", 
		{
	    asynchronous: true,
	    method: "get",
	    parameters: my_parameter,
	    onLoading: function(request) 
	    {
	    },
	    onComplete: function(request) 
	    {
				
	    },
	    onSuccess: function(request) 
	    {
				if(request.responseText==1){
					alert("Data Berhasil disimpan!");
					TampilkanSemuaData();
				}
				else{
					alert("Gagal menyimpan data discount");
				}
			},
	    onFailure: function(request) 
	    {     
	       assignError(request.responseText);
	    }
	  });
}

function ubahStatus(id_discount){
  //ubah account
	
	new Ajax.Request("jenis_discount.php?sid={SID}", 
		{
	    asynchronous: true,
	    method: "get",
	    parameters: "mode=ubah_status"+"&id_discount="+id_discount,
	    onLoading: function(request) 
	    {
	    },
	    onComplete: function(request) 
	    {
				
	    },
	    onSuccess: function(request) 
	    {
				TampilkanSemuaData();
			},
	    onFailure: function(request) 
	    {     
	       assignError(request.responseText);
	    }
	  });
}

function init(e) {
	//dialog dlg_discount__________________________________________________________________
	dlg_discount_btn_yes = document.getElementById("dlg_discount_btn_yes");
	dlg_discount_btn_no = document.getElementById("dlg_discount_btn_no");
	dlg_discount = dojo.widget.byId("dlg");

	dlg						= dojo.widget.byId("dialog");
	dlg_btn_cancel = document.getElementById("dialogcancel");

	//TampilkanSemuaData();
}

dojo.addOnLoad(init);

function Tambah() {
	dlg_discount.show();
	dlg.show();
}
</script>
<!--BEGIN Input -->
<div dojoType="dialog" width="400" id="dlg" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<font color='FFFFFF'><h3>Data Jenis Discount</h3></font>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center">
		<input type="hidden" id="hdn_id_discount"/>
		<table>
			<tr>
				<td>Kode Discount</td><td>:</td><td><input type="text" id="txt_kode"></td>
			</tr>
			<tr>
				<td>Nama Discount</td><td>:</td><td><input type="text" id="txt_nama_discount"></td>
			</tr>
			<tr>
				<td>Besarnya Discout (Rp.)</td><td>:</td><td><input type="text" id="txt_besar_discount"></td>
			</tr>
		</table>
	</td>
<tr>
	<td align="center">
		<br>
		<input type="button" id="dlg_discount_btn_no" value="&nbsp;Tidak&nbsp;">
		<input type="button" onclick="Simpan();" id="dlg_discount_btn_yes" value="&nbsp;Simpan&nbsp;&nbsp">
	</td>
</tr>
</table>
</form>
</div>
<!--END Input-->

<div dojoType="dialog" id="dialog" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;" id="myForm">
		<table width="500" style="background: white;">
			<tr>
				<td>
					<input type="button" style="border-radius: 0;" class="button-close" id="dialogcancel" onClick="dlg.hide();" value="&nbsp;X&nbsp;">
					<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Biaya Harian</h4>
				</td>
			</tr>
			<tr>
				<td align='center'>
					<table class="table">
						<tr>
							<td align='right'>
								Jenis Biaya :
							</td>
							<td>
								<select class="form-control" id='jenis' name='jenis' >
									<option value="">-- PILIH JENIS BIAYA --</option>
									<option value="ATK">ATK</option>
									<option value="RTK">RTK</option>
									<option value="Perawatan/Peralatan">Perawatan/Peralatan</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="right">Penerima :</td>
							<td>
								<input type="text" class="form-control" id="penerima" name="penerima">
							</td>
						</tr>
						<tr>
							<td align="right">Jumlah :</td>
							<td>
								<input type="text" id="jumlah" name="jumlah" class="form-control" onkeypress="validasi(event);">
							</td>
						</tr>
						<tr>
							<td align="right">Keterangan</td>
							<td>
								<textarea rows="3" class="form-control" id="keterangan" name="keterangan"></textarea>
							</td>
						</tr>
						<tr>
							<td></td>
							<td align="center">
								<input class="btn mybutton paper" type="button" onclick="create();" id="dialogproses" value="&nbsp;Proses&nbsp;">
							</td>
						</tr>
					</table>
					<span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
					<br>
				</td>
			</tr>
		</table>
	</form>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">
			    <!-- MODUL -->
			    <table width='100%' cellspacing="0">
					<tr class='' height=40>
						<td align='center' valign='middle' class="bannerjudul">&nbsp;Daftar Jenis Discount</td>
					</tr>
					<tr>
						<td height=30></td>
					</tr>
			    	<tr>
						<td valign="middle" align="center">
						{U_ADD}
							<button onclick="Tambah();">Tambah</button>
						</td>
					</tr>
					<tr>
						<td>
							<table id='rewrite_hasil' class="table table-bordered"></table>
							<span id='progress_data' style='display:none;'><img src='templates/images/loading.gif' />loading...</span>
						</td>
					</tr>
				</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
