<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('nama_invalid');
	Element.hide('hp_invalid');
	
	kode			= document.getElementById('kode_sopir');
	nama			= document.getElementById('nama');
	hp				= document.getElementById('hp');
		
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(!cekValue(hp.value)){	
		valid=false;
		Element.show('hp_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<form name="frm_data_mobil" action="{U_SOPIR_ADD_ACT}" method="post" onSubmit='return validateInput();'>
			<table width="100%" class="table" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td class="whiter" valign="middle" align="center">
				<table>
					<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
					<tr>
						<td align='center' valign='top'>
							<table>   
								<tr>
									<td colspan=3><h2>{JUDUL}</h2></td>
								</tr>
								<tr>
						      <input class="form-control"  type="hidden" name="kode_sopir_old" value="{KODE_SOPIR_OLD}">
									<td class="td-title" width='200'>Kode Sopir*</td><td width='5'></td>
									<td class="pad10">
										<input class="form-control"  type="text" id="kode_sopir" name="kode_sopir" value="{KODE_SOPIR}" maxlength=50 onChange="Element.hide('kode_invalid');">
										<span id='kode_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="td-title">Nama*</td><td></td>
									<td class="pad10">
										<input class="form-control"  type="text" id="nama" name="nama" value="{NAMA}" maxlength=100 onChange="Element.hide('nama_invalid');">
										<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="td-title" valign=''>Alamat</td><td class="pad10" valign='top'></td><td class="pad10"><textarea class="form-control" name="alamat" id="alamat" cols="30" rows="3"  maxlength=150>{ALAMAT}</textarea></td>
						    </tr>
								<tr>
						      <td class="td-title">HP</td><td></td>
									<td class="pad10">
										<input class="form-control"  type="text" id="hp" name="hp" value="{HP}" maxlength=50 onChange="Element.hide('hp_invalid');">
										<span id='hp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="td-title">No.SIM</td><td></td>
									<td class="pad10">
										<input class="form-control"  type="text" id="no_sim" name="no_sim" value="{NO_SIM}" maxlength=50 />
									</td>
						    </tr>
								<tr>
									<td class="td-title">Status Aktif</td><td></td>
									<td class="pad10">
										<select class="form-control" id="aktif" name="aktif">
											<option value=1 {AKTIF_1}>AKTIF</option>
											<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
					<td align='center' valign='middle' style="padding-bottom: 50px;">
						<input type="hidden" name="mode" value="{MODE}">
					  	<input type="hidden" name="submode" value="{SUB}">
						<input type="hidden" name="txt_cari" value="{CARI}">
						<input type="hidden" name="page" value="{PAGE}">
						<div class="col-md-8 col-md-offset-2">
							<div class="col-md-6">
								<input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI" style="width:100px;">
							</div>
							<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
						</div>
					</td>
				</tr>            
				</table>
				</td>
			</tr>
			</table>
			</form>
		</div>
	</div>
</div>