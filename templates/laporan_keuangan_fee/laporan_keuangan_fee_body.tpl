<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function closeRekon(tgl,total_fee){
		
		new Ajax.Request("laporan_keuangan_fee.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=closerekon&total_fee="+total_fee+"&tgl_transaksi="+tgl,
	   onLoading: function(request) 
	   {
				loading.show();
	   },
	   onComplete: function(request) 
	   {
				
	   },
	   onSuccess: function(request) 
	   {
			window.location.reload();
		 },
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function bayarRekon(id,tgl,total_fee){
		
		new Ajax.Request("laporan_keuangan_fee.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=bayarrekon&id="+id+"&tgl_transaksi="+tgl+"&total_fee="+total_fee,
	   onLoading: function(request) 
	   {
				loading.show();
	   },
	   onComplete: function(request) 
	   {
				
	   },
	   onSuccess: function(request) 
	   {
			window.location.reload();
		 },
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function bayarRekonAll(){
	
	if(confirm("Anda akan mengubah status rekon, klik 'OK' untuk melanjutkan atau 'Cancel' untuk membatalkan")){
		
		loop=true;
		list_dipilih="";
		for(i=1;i<={JUMLAH_HARI};i++){
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				if(chk.checked){
					if(list_dipilih==""){
						list_dipilih +=chk.value;
					}
					else{
						list_dipilih +=","+chk.value;
					}
				}
			}
		};
		
		new Ajax.Request("laporan_keuangan_fee.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=bayarrekonall&id="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {		
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function checkAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		for(i=1;i<={JUMLAH_HARI};i++){
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
		}
		
}

function uncheckAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		for(i=1;i<={JUMLAH_HARI};i++){
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
		}
		
}

function init(e) {
  // inisialisasi variabel
	
	//control dialog loading
	loading = dojo.widget.byId("loading");
	
}

dojo.addOnLoad(init);
</script>

<!--dialog Loading-->
<div dojoType="dialog" id="loading" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<br>
<table>
	<tr>
		<td align="center" valign="middle"><img src="{TPL}/images/loading2.gif"><br></td>
		<td align="center" valign="middle"><font color="white">Silahkan tunggu, sistem sedang memproses permintaan anda...</font> </td>
	</tr>
</table>
<br>
</div>
<!--END dialog Loading-->
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr height=40 class=''>
							<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap Fee Transaksi</td>
							<td class="bannerjudul">&nbsp;</td>
						</tr>
						<tr>
							<td class="pad10" colspan=2 align='center' valign='middle'>
								<a href="{U_LAPORAN_OMZET_GRAFIK}"><i class="fa fa-bar-chart"></i> Lihat Grafik</a>&nbsp;&nbsp;&nbsp;<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<br>
					<table cellspacing="0" cellpadding="0" >
						<tr>
							<td class="pad10" align='right' colspan="2">
								Total Fee Terhutang: <b>Rp. {JUMLAH_REKON_TERHUTANG}</b>&nbsp;
								&nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp										
							</td>
						</tr>
						<tr><td align='left'><a href='#' onClick='bayarRekonAll();false;'>Rekon Lunas</a></td><td align="right" style="padding-left: 10px;">{LIST_BULAN}</td></tr>
					</table>
					
					<table class="table table-hover table-bordered" style="margin-top: 20px;" >
			    <tr>
			       <th width='10'><input type='checkbox' id='checkall' name='checkall' onClick="if(this.checked){checkAll();}else{uncheckAll();}" /></th>
						 <th width="150">Tanggal</th>
						 <th width="150">Trip</th>
						 <th width="150">Tiket</th>
						 <th width="150">Tiket Online</th>
						 <th width="150">Tiket Batal</th>
						 <th width="150">Total Fee</th>
						 <th width="150">Status Rekon</th>
			     </tr>
			     <!-- BEGIN ROW -->
			     <tr class="{ROW.odd}" onClick="document.getElementById('checked_'+{ROW.idx_tgl}).checked=!document.getElementById('checked_'+{ROW.idx_tgl}).checked;">
						 <td><div align="center">{ROW.check}</div></td>
			       <td title='Tanggal'align="center"><font size=2 color='{ROW.font_color}'>{ROW.tgl}</font></div></td>
						 <td title='Jumlah Trip' align="right">{ROW.spj}</td>
						 <td title='Jumlah Tiket' align="right">{ROW.tiket}</td>
						 <td title='Jumlah Tiket Online' align="right">{ROW.tiket_online}</td>
						 <td title='Jumlah Tiket Batal' align="right">{ROW.tiket_batal}</td>
						 <td title='Jumlah Total Fee' align="right">{ROW.total_fee}</td>
						 <td title='Status Rekon' align="center">{ROW.status_rekon}</td>
			     </tr>
			     <!-- END ROW -->
					 <tr bgcolor='ffff00'>
						 <td>&nbsp;</td>
			       <td align="center"><font size=2 color='{ROW.font_color}'><b>Total</b></font></td>
						 <td align="right"><b>{SUM_SPJ}</b></td>
						 <td align="right"><b>{SUM_TIKET}</b></td>
						 <td align="right"><b>{SUM_TIKET_ONLINE}</b></td>
						 <td align="right"><b>{SUM_TIKET_BATAL}</b></td>
						 <td align="right"><b>{SUM_TOTAL_FEE}</b></td>
						 <td align="right">&nbsp;</td>
			     </tr>
			    </table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>