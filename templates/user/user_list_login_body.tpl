<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function toggleSelect(){
	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			if(chk.checked==true){
				chk.checked=false;
			}
			else{
				chk.checked=true;
			}
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);
}


function toggleSelect(){
	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			if(chk.checked==true){
				chk.checked=false;
			}
			else{
				chk.checked=true;
			}
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);
}

function ubahStatus(kode){
	
	if(confirm("Apakah anda yakin akan mengubah status online dari user-user ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
		
		new Ajax.Request("pengaturan_user_list_login.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubah_status_online&list_user="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudul">&nbsp;Status On-Line User</td>
							<td colspan=2 align='left' class="" valign='middle'>
								<form action="{ACTION_CARI}" method="post">
									<div class="col-md-8 col-md-offset-4" style="margin-top: 10px;">
										Cari<br />
										<div class="input-group" style="width: 100%;">
									      <input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
									      <span class="input-group-btn">
									        <input class="tombol form-control" type="submit" value="cari" />
									      </span>
									    </div><!-- /input-group -->
									</div>
								</form>
							</td>
						</tr>
						<tr><td height=30></td></tr>
						<tr>
							<td width='30%' align='left'>
								<a href="" onClick="return ubahStatus('');">[*] Ubah Status Online</a></td>
							<td width='70%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
					<table width='100%	'  class="table table-hover table-bordered table-responsive" style="margin-top: 20px;">
			    <tr>
			       <th width=10><input type='checkbox' onclick="toggleSelect();" id='ceker' /></th>
			       <th width=30>No</th>
						 <th width=100>Nama</th>
						 <th width=100>NRP</th>
						 <th width=100>Username</th>
						 <th width=200>Cabang</th>
						 <th width=70>user_level</th>
						 <th width=100>Telp/HP</th>
						 <th width=100>Login</th>
						 <th width=100>Logout</th>
						 <th width=70>Status</th>
			     </tr>
			     <!-- BEGIN ROW -->
			     <tr class="{ROW.odd}">
			       <td><div align="center">{ROW.check}</div></td>
			       <td><div align="right">{ROW.no}</div></td>
						 <td><div align="left">{ROW.nama}</div></td>
						 <td><div align="left">{ROW.nrp}</div></td>
						  <td><div align="left">{ROW.username}</div></td>
						  <td><div align="left">{ROW.cabang}</div></td>
						  <td><div align="left">{ROW.user_level}</div></td>
						  <td><div align="left">{ROW.telp_hp}</div></td>
						  <td><div align="left">{ROW.login}</div></td>
						  <td><div align="left">{ROW.logout}</div></td>
			       <td><div align="center">{ROW.status}</div></td>
			     </tr>  
			     <!-- END ROW -->
			    </table>
			    <table width='100%'>
						<tr>
							<td width='30%' align='left'>
								<a href="" onClick="return ubahStatus('');">[*] Ubah Status Online</a></td>
							<td width='70%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
