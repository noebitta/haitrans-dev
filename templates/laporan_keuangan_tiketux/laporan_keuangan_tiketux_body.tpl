<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","laporan_keuangan_tiketux.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		/*if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }*/
		
		new Ajax.Updater("rewrite_tujuan","laporan_keuangan_tiketux.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">

			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' class="table" cellpadding="1">
						<tr class='' height=40><td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Keuangan Tiketux</td></tr>
						<tr class='' height=40>
							<td align='left' valign='middle'>
								<div class="row">
									<div class="col-md-3">
										Jenis:<br /><select class="form-control" id='filterstatus' name='filterstatus'><option value='' {OPT_STATUS}>-semua jenis trx-</option><option value=0 {OPT_STATUS0}>Deposit</option><option value=1 {OPT_STATUS1}>Settlement</option></select>
									</div>
									<div class="col-md-3">
										Kota:<br /><select class="form-control" onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
									</div>
									<div class="col-md-3">
										Asal:<br /><div id='rewrite_asal'></div>
									</div>
									<div class="col-md-3">
										Tujuan:<br /><div id='rewrite_tujuan'></div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<br />Tgl:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
									</div>
									<div class="col-md-3">
										<br />s/d <br /><input  class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
									</div>
									<div class="col-md-4">
										<br />Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
									</div>
									<div class="col-md-2">
										<br /><input class="btn mybutton topwidth" name="btn_cari" type="submit" value="cari" />
									</div>
								</div>
								
							</td>
						</tr>
						<tr>
							<td colspan=2 align='center' valign='middle'>
								<table>
									<tr>
										<!--<td>
											 &nbsp;
										</td><td bgcolor='D0D0D0'></td>-->
										<td>
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<table width='100%'>
									<tr>
										<td>
											<!-- <table class="border table" style="margin: 0 auto;">
												<tr>
													<td><b>Total Tiket</b></td><td align="right">{TOTAL_TIKET} tiket</td>
												</tr>
												<tr>
													<td><b>Total Penjualan</b></td><td align="right">Rp. {TOTAL_SUB}</td>
												</tr>
												<tr>
													<td><b>Total Discount</b></td><td align="right" style="color: red;">Rp. {TOTAL_KOMISI}</td>
												</tr>
												<tr><td colspan="2" style="padding: 0;"><hr /></td></tr>
												<tr>
													<td><b>Total Penerimaan</b></td><td align="right">Rp. {TOTAL_JUAL}</td>
												</tr>
											</table> -->

											<span style="float: left;">
												Total Penerimaan: <b>Rp. {TOTAL_JUAL}</b> (<a class="" data-toggle="collapse" data-target="#tiket_1">
											Detail
											</a>)
											</span>

											<div id="tiket_1" class="collapse" style="float: left;">
												 &nbsp; &nbsp;Total Tiket: {TOTAL_TIKET} &nbsp; &nbsp; &nbsp;
												Total Penjualan: {TOTAL_SUB} &nbsp; &nbsp; &nbsp;
												Total Diskon: {TOTAL_KOMISI} &nbsp; &nbsp; &nbsp;
											</div>

										</td>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="border table table-hover table-bordered" width='100%' >
				    <tr>
							 <th width=30	><a class="th" >No</th>
							 <th width=200><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'	>Waktu Bayar</th>
							 <th width=200><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'	>No.Tiket</th>
							 <th width=200><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'	>Waktu Berangkat</th>
							 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'	>Kode Jadwal</th>
							 <th width=200><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'	>Nama</th>
							 <th width=50	><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'	>Kursi</th>
							 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'	>Harga Tiket</th>
							 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'	>Discount</th>
							 <th width=70	><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'	>Total</th>
							 <th width=100><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'	>Tipe Disc.</th>
							 <th width=100><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'	>Status</th>
							 <th width=100><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'	>Ket.</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td><div align="right">{ROW.no}</div></td>
				       <td><div align="left">{ROW.waktu_cetak_tiket}</div></td>
							 <td><div align="left">{ROW.no_tiket}</div></td>
				       <td><div align="left">{ROW.waktu_berangkat}</div></td>
				       <td><div align="left">{ROW.kode_jadwal}</div></td>
							 <td><div align="left">{ROW.nama}</div></td>
							 <td><div align="center">{ROW.no_kursi}</div></td>
							 <td><div align="right">{ROW.harga_tiket}</div></td>
							 <td><div align="right">{ROW.komisi}</div></td>
							 <td><div align="right">{ROW.total}</div></td>
							 <td><div align="left">{ROW.tipe_discount}</div></td>
				       <td><div align="center">{ROW.status}</div></td>
				       <td><div align="left">{ROW.ket}</div></td>
				     </tr>  
				     <!-- END ROW -->
				    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>