var SID;
var harga_minimum_paket;

function Start(page) {
  OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function cekValue(nilai){
  cek_value=nilai*0;

  if(cek_value==0){
    return true;
  }
  else{
    return false;
  }
}

function cekValueNoTelp(no_telp){
  //cek 1
  jum_digit=no_telp.length;

  if(jum_digit<8){
    //alert(1);
    return false;
  }

  //cek 2
  kode_inisial=no_telp.substring(0,1);

  if(kode_inisial!=0){
    //alert(2);
    return false;
  }

  //cek 3
  kode_area=no_telp.substring(1,2)*1;

  if(kode_area<=1){
    //alert(3);
    return false;
  }

  return true;

}

function validasiNoTelp(evt){
  var theEvent = evt || window.event;

  var key = theEvent.keyCode || theEvent.which;

  key = String.fromCharCode(key);

  var regex = /[0-9]/;

  if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 ||
    [evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;

  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    theEvent.preventDefault();
  }
}


function ShowDialogKode(jenis){
  document.getElementById('jenis_pembatalan').value=jenis;
  //document.getElementById('kode').value='';
  dlg_TanyaHapus.show();
  //HapusBerdasarkanJenis();
}

function Right(str, n){
  /*
   IN: str - the string we are RIGHTing
   n - the number of characters we want to return

   RETVAL: n characters from the right side of the string
   */
  if (n <= 0)     // Invalid bound, return blank string
    return "";
  else if (n > String(str).length)   // Invalid bound, return
    return str;                     // entire string
  else { // Valid bound, return appropriate substring
    var iLen = String(str).length;
    return String(str).substring(iLen, iLen - n);
  }
}

Number.prototype.formatMoney = function(c, d, t){
  var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function ValidasiAngka(objek,kolom){
  temp_nilai=objek.value*0;

  if(temp_nilai!=0){
    alert(kolom+" harus angka!");
    objek.setFocus;exit;
  }

  if(objek.value<0){
    alert(kolom+" tidak boleh kurang dari 0!");
    objek.setFocus;exit;
  }

}

function validasiInputanAngka(evt){
  var theEvent = evt || window.event;

  var key = theEvent.keyCode || theEvent.which;

  key = String.fromCharCode(key);

  var regex = /[0-9]/;

  if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13
    || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39 || [evt.keyCode||evt.which]==116)  return true;

  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    theEvent.preventDefault();
  }
}

function FormatUang(uang,separator){
  len_uang = String(uang).length;
  return_val='';
  for (i=len_uang;i>=0;i--){
    if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

    return_val =String(uang).substring(i,i-1)+return_val;
  }

  return return_val;
}

function getUpdateAsal(kota_asal,mode){
  // fungsi ini mengubah RUTE menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]


  document.getElementById('rewritepaket').innerHTML="";

  //mengambil tanggal yang diinput user dan rute
  tgl_user  = document.getElementById('p_tgl_user').value;

  //memeriksa combo rewritejadwal sudah ditampilkan atau belum

  temp_combo_rute=document.getElementById('rewritejadwal').innerHTML;

  if (temp_combo_rute != ""){
    //jika sudah muncul, akan diambil nilai dari combo rute
    jam=document.getElementById('p_jadwal').value;
    id_jurusan	= document.getElementById('tujuan').value;
  }
  else{
    //jika belum dimunculkan, maka nilai dari rute akan diisi dengan kosong
    jam='';
    mode=0;
  }

  if(mode==0){

    document.getElementById('rewritetujuan').innerHTML = "";
    document.getElementById('rewritejadwal').innerHTML = "";

    new Ajax.Updater("rewriteasal","reservasi_paket.php?sid="+SID, {
      asynchronous: true,
      method: "get",

      parameters: "mode=asal&kota_asal="+kota_asal,
      onLoading: function(request){
        Element.show('progress_asal');
      },
      onComplete: function(request){
        Element.show('rewriteasal');
        Element.hide('progress_asal');
      },
      onFailure: function(request){
        assignError(request.responseText);
      }
    });

  }
  else{
    getUpdatePaket();
    getPengumuman();
    getListHargaPaket();
  }


}

function getUpdateTujuan(asal){
  // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]

  new Ajax.Updater("rewritetujuan","reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "asal=" + asal + "&mode=tujuan",
      onLoading: function(request)
      {
        Element.show('progress_tujuan');
        document.getElementById('rewritejadwal').innerHTML="";
        //Element.hide('rewritejadwal');
      },
      onComplete: function(request)
      {
        Element.show('rewritetujuan');
        Element.hide('progress_tujuan');
        //Element.show('rewriteall');
      },
      onFailure: function(request)
      {
        assignError(request.responseText);
      }
    });
}

function getUpdateJadwal(id_jurusan){

  tgl=document.getElementById('p_tgl_user').value;

  // mengupdate JAM sesuai dengan RUTE yang dipilih
  document.getElementById('id_jurusan_aktif').value = id_jurusan;

  document.getElementById('rewritepaket').innerHTML="";

  new Ajax.Updater("rewritejadwal","reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "tgl=" + tgl + "&id_jurusan=" + id_jurusan +"&mode=jam",
      onLoading: function(request)
      {
        document.getElementById('rewritejadwal').innerHTML="";
        Element.show('progress_jam');
      },
      onComplete: function(request)
      {
        Element.hide('progress_jam');
        Element.show('rewritejadwal');
      },
      onFailure: function(request)
      {
        assignError(request.responseText);
      }
    });
}

function changeRuteJam(){
  document.getElementById("loadingrefresh").style.display="block";
  getUpdatePaket();
  document.getElementById("loadingrefresh").style.display="none";
  getListHargaPaket();
}

var obj_jadwal_dipilih;

function pilihJadwal(obj){
  if(obj_jadwal_dipilih){
    obj_jadwal_dipilih.className='a';
  }

  obj.className='selected';
  obj_jadwal_dipilih=obj;
}

function getPengumuman(){

  new Ajax.Updater("rewritepengumuman","reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "mode=pengumuman",
      onLoading: function(request)
      {

      },
      onComplete: function(request)
      {

      },
      onFailure: function(request)
      {
        assignError(request.responseText);
      }
    });
}

function getUpdatePaket(){
  //mereset timer waktu refresh
  resetTimerRefresh();

  new Ajax.Updater("rewritepaket","reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters:
        "mode=paketlayout"+
          "&tanggal="+document.getElementById('p_tgl_user').value+
          "&jadwal="+document.getElementById('p_jadwal').value+
          "&modemutasion="+document.getElementById("ismodemutasion").value,
      onLoading: function(request){
        document.getElementById("loadingrefresh").style.display="block";
      },
      onComplete: function(request){
        document.getElementById("loadingrefresh").style.display="none";
      },
      onFailure: function(request)
      {
        assignError(request.responseText);
      }
    });
}

function showPaket(no_tiket){
  document.getElementById('ismodemutasion').value=0;

  new Ajax.Updater("showdatadetail","reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "no_tiket="+no_tiket+"&mode=paketdetail",
      onLoading: function(request){
        document.getElementById("loadingdetail").style.display="block";
      },
      onComplete: function(request){
        document.getElementById('notiketdicetak').value=no_tiket;
        document.getElementById("loadingdetail").style.display="none";
      },
      onFailure: function(request){
        assignError(request.responseText);
      }
    });
}

function batalPaket(no_tiket){

  if(confirm("Apakah anda yakin akan membatalkan paket ini?")){

    new Ajax.Request("reservasi_paket.php?sid="+SID,{
      asynchronous: true,
      method: "get",
      parameters: "mode=paketbatal&no_tiket="+no_tiket,
      onLoading: function(request) {
      },
      onComplete: function(request)
      {

      },
      onSuccess: function(request) {
        if(request.responseText==1){
          document.getElementById('showdatadetail').innerHTML='';
          getUpdatePaket();
        }
        else{
          alert("Anda tidak memiliki wewenang untuk membatalkan tiket ini!");
        }

      },
      onFailure: function(request)
      {
      }
    })
  }

  return false;

}

function getListHargaPaket(){

  id_jurusan	= document.getElementById('id_jurusan_aktif').value

  if(id_jurusan==""){
    return;
  }

  new Ajax.Request("reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "id_jurusan="+id_jurusan+
        "&mode=ambil_list_harga_paket&paket=0",
      onLoading: function(request){},
      onComplete: function(request){},
      onSuccess: function(request){
        eval(request.responseText);
      },
      onFailure: function(request){
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    })

}

function setJenisBarang(jenisbarang){

  jenisbarang= jenisbarang*1;

  switch(jenisbarang){
    case 1:
      //Dokumen
      isvolumetric0.checked=true;
      setFormVolumetric();
      isvolumetric0.disabled=true;
      isvolumetric1.disabled=true;
      break;
    case 2:
      //barang
      isvolumetric0.disabled=false;
      isvolumetric1.disabled=false;
      break;
    case 3:
      //bagasi
      isvolumetric0.checked=true;
      isvolumetric0.disabled=true;
      isvolumetric1.disabled=true;
      showinputvolumetric.style.display="none";
      showinputweight.style.display="none";
      showinputsizebagasi.style.display="inline-block";
      break;
  }

  hitungHargaPaket();

}

function hitungHargaPaket(){

  dimensi     = document.getElementById('dimensi').value;

  if (isvolumetric0.checked) {
    berathitung	= dimensi!=3?berat.value:ukuranbagasi.value;
  }
  else{
    berathitung		= Math.ceil((dimensipanjang.value*dimensilebar.value*dimensitinggi.value)/4000);
    beratvolumetric.innerHTML	=  FormatUang(berathitung,'.');
  }

  if(berathitung<=0 || berathitung==''){
    document.getElementById('harga_paket').innerHTML	=  0;
    return;
  }

/*
harga paket di input sendiri
  switch(dimensi){
    case '1':
      //dokumen
      harga_paket_kg_pertama		= document.getElementById('harga_kg_pertama_1').value;
      harga_paket_kg_berikutnya	= document.getElementById('harga_kg_berikutnya_1').value;
      break;

    case '2':
      //barang
      harga_paket_kg_pertama		= document.getElementById('harga_kg_pertama_2').value;
      harga_paket_kg_berikutnya	= document.getElementById('harga_kg_berikutnya_2').value;
      break;

    case '3':
      //bagasi
      harga_paket_kg_pertama		= document.getElementById('harga_kg_pertama_3').value;
      harga_paket_kg_berikutnya	= document.getElementById('harga_kg_berikutnya_3').value;
      break;

    case '4':
      harga_paket_kg_pertama		= document.getElementById('harga_kg_pertama_4').value;
      harga_paket_kg_berikutnya	= document.getElementById('harga_kg_berikutnya_4').value;
      break;

    case '5':
      harga_paket_kg_pertama		= document.getElementById('harga_kg_pertama_5').value;
      harga_paket_kg_berikutnya	= document.getElementById('harga_kg_berikutnya_5').value;
      break;

    case '6':
      harga_paket_kg_pertama		= document.getElementById('harga_kg_pertama_6').value;
      harga_paket_kg_berikutnya	= document.getElementById('harga_kg_berikutnya_6').value;
      break;
  }

  total_harga_paket	= harga_paket_kg_pertama*1 + harga_paket_kg_berikutnya*(berathitung-1);
  document.getElementById('harga_paket_show').value	=  FormatUang(total_harga_paket,'.');
  */
}

function pesanPaket(){

  // melakukan saving / update transaksi/

  if(!document.getElementById('p_jadwal')){
    alert("Anda belum memilih jadwal keberangkatan!");
    return;
  }


  tgl  						= document.getElementById('p_tgl_user').value;
  kode_jadwal  		= document.getElementById('p_jadwal').value;
  nama_pengirim		= document.getElementById('nama_pengirim');
  alamat_pengirim	= document.getElementById('alamat_pengirim');
  telepon_pengirim= document.getElementById('telp_pengirim');
  nama_penerima		= document.getElementById('nama_penerima');
  alamat_penerima	= document.getElementById('alamat_penerima');
  telepon_penerima= document.getElementById('telp_penerima');
  keterangan			= document.getElementById('keterangan');
  jumlah_koli			= document.getElementById('jumlah_koli');
  jenis_layanan		= document.getElementById('jenislayanan');
  berat						= document.getElementById('berat');
  ukuranbagasi    = document.getElementById('ukuranbagasi');
  //jenis_barang		= document.getElementById('jenis_barang').value;			
  dimensi					= document.getElementById('dimensi').value;
  cara_bayar			= document.getElementById('cara_bayar');
  dimensipanjang  = document.getElementById('dimensipanjang');
  dimensilebar    = document.getElementById('dimensilebar');
  dimensitinggi   = document.getElementById('dimensitinggi');
  harga_paket = document.getElementById('harga_paket');
  valid=true;

  if (document.getElementById('txt_spj')){
    no_spj	= document.getElementById('txt_spj').value;
  }
  else{
    no_spj='';
  }

  if ((nama_pengirim.value=='') || (nama_pengirim.value==String.Empty)){
    nama_pengirim.style.background="red";
    valid=false;
  }

  if ((telepon_pengirim.value=='') || !cekValueNoTelp(telepon_pengirim.value)){
    telepon_pengirim.style.background="red";
    valid=false;
  }

  if ((nama_penerima.value=='') || (nama_penerima.value==String.Empty)){
    nama_penerima.style.background="red";
    valid=false;
  }

  if ((telepon_penerima.value=='') || !cekValueNoTelp(telepon_penerima.value)){
    telepon_penerima.style.background="red";
    valid=false;
  }

  if (keterangan.value==''){
    keterangan.style.background="red";
    valid=false;
  }

  if (!cekValue(jumlah_koli.value) || jumlah_koli.value*1<=0){
    jumlah_koli.style.background="red";
    valid=false;
  }

  if(harga_paket.value == "" || harga_paket.value*1<=0){
    harga_paket.style.background="red";
    valid=false;
  }

  if (isvolumetric0.checked) {
    berathitung	= dimensi!=3?berat.value:ukuranbagasi.value;
    console.log("berat");
    if (berathitung=="" || berathitung<=0){
      berat.style.background="red";
      valid=false;
    }
  }
  else{
    berathitung		= Math.ceil((dimensipanjang.value*dimensilebar.value*dimensitinggi.value)/4000);
    if (berathitung=="" || berathitung<=0){
      dimensipanjang.style.background="red";
      dimensilebar.style.background="red";
      dimensitinggi.style.background="red";
      valid=false;
    }
  }

  if(!valid){
    alert("data_invalid");
    return;
  }

  new Ajax.Request("reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters:
        "mode=pakettambah"+
          "&harga_paket="+harga_paket.value+
          "&nama_pengirim="+nama_pengirim.value+
          "&alamat_pengirim="+alamat_pengirim.value+
          "&telepon_pengirim="+telepon_pengirim.value+
          "&nama_penerima="+nama_penerima.value+
          "&alamat_penerima="+alamat_penerima.value+
          "&telepon_penerima="+telepon_penerima.value+
          "&keterangan="+keterangan.value+
          "&jumlah_koli="+jumlah_koli.value+
          "&berat="+berathitung+
          "&dimensi="+dimensi+
          "&isvolumetric=" +(isvolumetric0.checked?0:1)+
          "&dimensipanjang="+dimensipanjang.value+
          "&dimensilebar="+dimensilebar.value+
          "&dimensitinggi="+dimensitinggi.value+
          "&jenis_layanan="+jenis_layanan.value+
          "&cara_bayar="+cara_bayar+
          "&tanggal=" +tgl+
          "&kode_jadwal="+kode_jadwal+
          "&no_spj="+no_spj,

      onLoading: function(request)
      {
        Element.show('progress_paket');
      },
      onComplete: function(request)
      {
        console.log("complete");
      },
      onSuccess: function(request){
        console.log("sucses");
        console.log(request.responseText);
        var prosesok=false;

        eval(request.responseText);

        if(prosesok){
          resetIsianPaket();
          getUpdatePaket(); // update layout mobil :
          dialog_pembayaran.show();

        }
        else{
          alert("Terjadi kegagalan!");
        }

        Element.hide('progress_paket');
      },
      onFailure: function(request)
      {
        console.log("failure");
        alert('Gagal menyimpan');
        assignError(request.responseText);
      }
    })
}

function resetIsianPaket(){

  document.getElementById('nama_pengirim').value		="";
  document.getElementById('alamat_pengirim').value	="";
  document.getElementById('telp_pengirim').value		="";
  document.getElementById('nama_penerima').value		="";
  document.getElementById('alamat_penerima').value	="";
  document.getElementById('telp_penerima').value		="";
  document.getElementById('keterangan').value				="";
  document.getElementById('jumlah_koli').value			="";
  document.getElementById('berat').value						="";
  document.getElementById('harga_paket').value = "";
  hitungHargaPaket();

  document.getElementById("dimensi").value=1;
  setJenisBarang(1);
}

function ambilDataPaket(no_resi){
  // Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu

  dialog_cari_paket.hide();

  document.getElementById('rewrite_ambil_paket').innerHTML="";

  if(no_resi==""){
    alert("Anda belum memasukkan no resi paket yang akan dicari!");
    return;
  }

  dialog_ambil_paket.show();

  new Ajax.Updater("rewrite_ambil_paket","reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "no_tiket=" +no_resi+"&mode=paketdetail&submode=ambil",
      onLoading: function(request){
        Element.show('progress_ambil_paket');
      },
      onComplete: function(request) {
      },
      onSuccess: function(request){
        document.getElementById('notiketdicetak').value=no_resi;
        Element.hide('progress_ambil_paket');
      },
      onFailure: function(request)
      {
        assignError(request.responseText);
      }
    })

}

function prosesAmbilPaket(){

  // melakukan saving / update transaksi/

  no_tiket					= document.getElementById('dlg_ambil_paket_no_tiket').value;
  nama_pengambil		= document.getElementById('dlg_ambil_paket_nama_pengambil').value;
  no_ktp_pengambil	= document.getElementById('dlg_ambil_paket_no_ktp_pengambil').value;
  cara_bayar				= document.getElementById('hdn_paket_cara_pembayaran').value;

  if(cara_bayar==2){
    if(confirm("Paket ini akan dibayar oleh pelanggan yang mengambil paket, " +
      "silahkan informasikan terlebih dahulu kepada pelanggan."+
      "Klik OK untuk melanjutkan proses pengambilan atau klik CANCEL untuk membatalkannya!")){
    }
    else{
      return;
    }
  }

  valid=true;
  Element.hide('dlg_ambil_paket_nama_pengambil_invalid');
  Element.hide('dlg_ambil_paket_no_ktp_pengambil_invalid');

  if (no_tiket==''){
    return false;
  }

  if (nama_pengambil=='' || (nama_pengambil==String.Empty)){
    Element.show('dlg_ambil_paket_nama_pengambil_invalid');
    valid=false;
  }

  if ((no_ktp_pengambil=='') || (no_ktp_pengambil==String.Empty)){
    Element.show('dlg_ambil_paket_no_ktp_pengambil_invalid');
    valid=false;
  }

  if(!valid){
    return;
  }

  new Ajax.Request("reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters:
        "no_tiket="+no_tiket+"&nama_pengambil="+nama_pengambil+"&no_ktp_pengambil="+no_ktp_pengambil+
          "&mode=paketambil",

      onLoading: function(request)
      {
        Element.show('progress_ambil_paket');
      },
      onComplete: function(request)
      {

      },
      onSuccess: function(request)
      {

        if(request.responseText==1){
          Element.hide('progress_ambil_paket');
          dialog_ambil_paket.hide();
        }
        else{
          alert("Terjadi kegagalan pengambilan paket!");
        }

        Element.hide('progress_paket');
      },
      onFailure: function(request)
      {
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    })
}

function CetakResi(jenis_pembayaran){
  //mencetak tiket 

  no_tiket	= document.getElementById('notiketdicetak').value;

  parameter		= "tiket_paket.php?sid="+SID+"&no_tiket='"+no_tiket+"'&jenis_pembayaran="+jenis_pembayaran;
  Start(parameter);
  dialog_pembayaran.hide();
  document.getElementById("showdatadetail").innerHTML="";
  getUpdatePaket();
}

function cariDataPelangganByTelp4Paket(no_telp,flag){

  document.getElementById("telp_pengirim").style.background="";
  document.getElementById("nama_pengirim").style.background="";
  document.getElementById("telp_penerima").style.background="";
  document.getElementById("nama_penerima").style.background="";

  if(no_telp==""){
    return;
  }

  if(!cekValueNoTelp(no_telp)){
    alert("Nomor telepon yang anda masukkan tidak benar!");
    return;
  }

  new Ajax.Request("reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "no_telp="+no_telp+
        "&mode=periksa_no_telp&paket=1&flag="+flag,
      onLoading: function(request)
      {
        //Element.hide('label_not_found');
        //Element.show('progress_cari_penumpang');
        if(flag==1){
          document.getElementById("nama_pengirim").value="Mencari...";
        }
        else{
          document.getElementById("nama_penerima").value="Mencari...";
        }
      },
      onComplete: function(request)
      {

      },
      onSuccess: function(request)
      {
        if(request.responseText==0){
          //Element.show('label_not_found');
          if(flag==1){
            document.getElementById("nama_pengirim").value="";
          }
          else{
            document.getElementById("nama_penerima").value="";
          }
        }
        else{
          eval(request.responseText);
        }
      },
      onFailure: function(request)
      {
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    })

}

function periksaPaket(no_resi){
  // Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu


  document.getElementById('rewrite_cari_paket').innerHTML="";

  if(no_resi==""){
    alert("Anda belum memasukkan no resi paket yang akan dicari!");
    return;
  }

  dialog_cari_paket.show();

  new Ajax.Updater("rewrite_cari_paket","reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "no_resi=" +no_resi+"&mode=cari_paket",
      onLoading: function(request){
        Element.show('progress_cari_paket');
      },
      onComplete: function(request) {
      },
      onSuccess: function(request){
        Element.hide('progress_cari_paket');
      },
      onFailure: function(request)
      {
        assignError(request.responseText);
      }
    })
}

function setFlagMutasiPaket(){

  if(document.getElementById('ismodemutasion').value!=1){
    if(confirm("Anda akan mengaktifkan mode mutasi, " +
      "ketika anda memilih menekan tombol mutasi pada daftar paket, maka secara otomatis paket lama akan dimutasikan ke jadwal yang baru."+
      "Klik OK untuk melanjutkan proses mutasi atau klik CANCEL untuk keluar dari mode mutasi!")){
      lanjut = true;
    }
    else{
      lanjut = false;
    }
  }
  else{
    lanjut=true;
  }

  if(lanjut){
    document.getElementById('ismodemutasion').value	= 1-document.getElementById('ismodemutasion').value;

    if(document.getElementById('ismodemutasion').value==1){
      document.getElementById('tombolmutasipaket').value="B A T A L K A N   M U T A S I";
    }
    else{
      document.getElementById('tombolmutasipaket').value="M U T A S I";
    }

    getUpdatePaket();
  }
}

function mutasiPaket(){

  // melakukan saving / update transaksi/

  if(!document.getElementById('p_jadwal')){
    alert("Anda belum memilih jadwal keberangkatan!");
    return;
  }

  if(!confirm("Apakah anda yakin akan memindahkan paket ke jadwal ini?")){
    exit;
  }

  new Ajax.Request("reservasi_paket.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters:
        "mode=mutasipaket"+
          "&resi="+document.getElementById('noresi').value+
          "&tanggal="+document.getElementById('p_tgl_user').value+
          "&kodejadwal="+document.getElementById('p_jadwal').value,

      onLoading: function(request){
        document.getElementById("loadingrefresh").style.display="block";
      },
      onComplete: function(request){

      },
      onSuccess: function(request){
        var prosesok=false;

        eval(request.responseText);

        if(prosesok){
          document.getElementById('ismodemutasion').value=0;
          document.getElementById('showdatadetail').innerHTML='';

          getUpdatePaket();

        }
        else{
          alert("Terjadi kegagalan!");
        }

        document.getElementById("loadingrefresh").style.display="none";
      },
      onFailure: function(request){
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    })
}

function setDialogSPJ(){
  //menampilkan dialog spj

  sopir_sekarang  = document.getElementById('hide_nama_sopir_sekarang').value;
  mobil_sekarang  = document.getElementById('hide_mobil_sekarang').value;
  kode_jadwal  		= document.getElementById('p_jadwal').value;

  new Ajax.Updater("rewrite_list_sopir","SPJ.php?sid="+SID,
    {
      asynchronous: true,
      method: "get",
      parameters: "kode_jadwal="+kode_jadwal+"&sopir_sekarang=" + sopir_sekarang +"&mobil_sekarang=" + mobil_sekarang + "&aksi=0",
      onLoading: function(request)
      {
        document.getElementById('hide_nama_sopir_sekarang').innerHTML="";
        ocument.getElementById('hide_mobil_sekarang').innerHTML="";
        Element.show('progress_dialog_spj');
      },
      onComplete: function(request)
      {
        Element.hide('progress_dialog_spj');
      },
      onFailure: function(request)
      {
        assignError(request.responseText);
      }
    });

  dialog_SPJ.show();
}
function cetakSPJ(){
  //mencetak SPJ
  tgl_berangkat  	= document.getElementById('p_tgl_user').value;
  kode_jadwal 		= document.getElementById('p_jadwal').value;

  new Ajax.Request("SPJ_paket.php?sid="+SID,{
    asynchronous: true,
    method: "post",
    parameters:
      "mode=validasi&tgl_berangkat="+tgl_berangkat+
        "&kode_jadwal="+kode_jadwal,
    onLoading: function(request)
    {
      popup_loading.show();
    },
    onComplete: function(request)
    {
      popup_loading.hide();
    },
    onSuccess: function(request)
    {
      var proses_ok=false;

      eval(request.responseText);

      if(!proses_ok){
        alert("Terjadi kegagalan!");
      }

    },
    onFailure: function(request)
    {
      popup_loading.hide();
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });

  /*if(sopir_dipilih!='' && mobil_dipilih!=''){
   Start("SPJ_paket.php?sid="+SID+"&tgl_berangkat="+tgl_berangkat+"&kode_jadwal="+kode_jadwal+"&mobil_dipilih="+mobil_dipilih+"&sopir_dipilih="+sopir_dipilih+"&no_spj="+no_spj+"&aksi=1");
   getUpdatePaket();
   dialog_SPJ.hide();
   }
   else{
   alert("Anda belum memilih kendaraan atau nama sopir!");
   }*/
}

function setFormVolumetric(){

  if(isvolumetric0.checked) {
    berat.value="";
    showinputvolumetric.style.display="none";

    showinputweight.style.display="inline-block";
  }
  else{
    dimensipanjang.value="";
    dimensilebar.value="";
    dimensitinggi.value="";
    showinputvolumetric.style.display="inline-block";
    showinputweight.style.display="none";
  }

  hitungHargaPaket();

}

// global
var dialog_SPJ,dialog_ubah_tiket,btn_SPJ_OK,btn_SPJ_Cancel,msg;

function init(e) {
  // inisialisasi variabel
  SID =document.getElementById("hdn_SID").value;
  //harga_minimum_paket = document.getElementById("hdn_harga_minimum_paket").value;

  popup_loading	= dojo.widget.byId("popuploading");

  //control dialog cari paket
  dialog_cari_paket = dojo.widget.byId("dialog_cari_paket");
  btn_cari_paket_Cancel = document.getElementById("dialog_cari_paket_btn_Cancel");
  dialog_cari_paket.setCloseControl(btn_cari_paket_Cancel);

  //control dialog ambil paket
  dialog_ambil_paket 						= dojo.widget.byId("dialog_ambil_paket");
  dlg_ambil_paket_button_ok 		= document.getElementById("dlg_ambil_paket_button_ok");
  dlg_ambil_paket_button_cancel = document.getElementById("dlg_ambil_paket_button_cancel");

  //control dialog pembayaran
  dialog_pembayaran = dojo.widget.byId("dialog_pembayaran");
  btn_pembayaran_OK = document.getElementById("dialog_pembayaran_btn_OK");
  btn_pembayaran_Cancel = document.getElementById("dialog_pembayaran_btn_Cancel");
  dialog_pembayaran.setCloseControl(btn_pembayaran_Cancel);

  //control dialog box SPJ
  dialog_SPJ = dojo.widget.byId("dialog_SPJ");
  btn_SPJ_OK = document.getElementById("dialog_SPJ_btn_OK");
  btn_SPJ_Cancel = document.getElementById("dialog_SPJ_btn_Cancel");

  getPengumuman();
  resetIsianPaket();

}

dojo.addOnLoad(init);

var jdwl,tgl;
var waktu_refresh=10;//detik
var temp_waktu=waktu_refresh;
var g_flag_refresh=1;

function beginrefresh(){

  if (temp_waktu==1) {
    if(g_flag_refresh==1){

      temp_waktu=waktu_refresh;

      beginrefresh();

      //merefresh pengumumman
      getPengumuman();
      getUpdatePaket();

    }
  }
  else {
    if(g_flag_refresh==1){
      //get the minutes, seconds remaining till next refersh
      temp_waktu-=1;
      setTimeout("beginrefresh()",1000);
    }
  }

}

function resetTimerRefresh(){
  temp_waktu=waktu_refresh;
}

function setAutoRefreshOff(){
  g_flag_refresh=0;
}

function setAutoRefreshOn(){
  g_flag_refresh=1;
  beginrefresh();
}

//call the function beginrefresh
window.onload=beginrefresh();
//-->