var SID;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function cekValueNoTelp(no_telp){
	//cek 1
	jum_digit=no_telp.length;
	
	if(jum_digit<8){
		//alert(1);
		return false;
	}
	
	//cek 2
	kode_inisial=no_telp.substring(0,1);
	
	if(kode_inisial!=0){
		//alert(2);
		return false;
	}
	
	//cek 3
	kode_area=no_telp.substring(1,2)*1;
	
	if(kode_area<=1){
		//alert(3);
		return false;
	}
	
	return true;
	
}

function ShowDialogKode(jenis){
	document.getElementById('jenis_pembatalan').value=jenis;
	//document.getElementById('kode').value='';
	dlg_TanyaHapus.show();
	//HapusBerdasarkanJenis();
}

function Right(str, n){
	/*
      IN: str - the string we are RIGHTing
      n - the number of characters we want to return

      RETVAL: n characters from the right side of the string
       */
  if (n <= 0)     // Invalid bound, return blank string
		return "";
  else if (n > String(str).length)   // Invalid bound, return
		return str;                     // entire string
  else { // Valid bound, return appropriate substring
		var iLen = String(str).length;
    return String(str).substring(iLen, iLen - n);
  }
}

Number.prototype.formatMoney = function(c, d, t){
	var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function FormatUang(uang,separator){
	len_uang = String(uang).length;
	return_val='';
	for (i=len_uang;i>=0;i--){
		if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

		return_val =String(uang).substring(i,i-1)+return_val;
	}
	
	return return_val;
}

function getUpdateAsal(kota_asal,mode){
	// fungsi ini mengubah RUTE menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   			
		
	if(document.getElementById('flag_mutasi').value!=1){
		document.getElementById('dataPelanggan').innerHTML='';
	}
	
	//mengambil tanggal yang diinput user dan rute
	tgl_user  = document.getElementById('p_tgl_user').value;
	
	//memeriksa combo rewritejadwal sudah ditampilkan atau belum
	
	temp_combo_rute=document.getElementById('rewritejadwal').innerHTML;
	
	if (temp_combo_rute != ""){
		//jika sudah muncul, akan diambil nilai dari combo rute
		jam=document.getElementById('p_jadwal').value;
		id_jurusan	= document.getElementById('tujuan').value;
		//getUpdateJadwal(id_jurusan);
	}
	else{
		//jika belum dimunculkan, maka nilai dari rute akan diisi dengan kosong
		jam='';
		mode=0;
	}

	document.getElementById('rewritemobil').innerHTML = "";
					 
	if(mode==0){
		
		document.getElementById('rewritetujuan').innerHTML = "";
		document.getElementById('rewritejadwal').innerHTML = "";
		
		new Ajax.Updater("rewriteasal","reservasi.php?sid="+SID, {
			asynchronous: true,
			method: "get",

			parameters: "mode=asal&kota_asal="+kota_asal,
			onLoading: function(request){
				Element.show('progress_asal');
			},
			onComplete: function(request){
				Element.show('rewriteasal');
				Element.hide('progress_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});
		
	}
	else{
		getUpdateMobil();
		//setSisaKursi();
		//getPengumuman();
		getDataListDiscount(0,0);
		getListHargaPaket();
	}
	
			
}

function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
    
		document.getElementById('rewritemobil').innerHTML = "";
    
		if(document.getElementById('flag_mutasi').value!=1){
			document.getElementById('dataPelanggan').innerHTML='';
		}
		
		new Ajax.Updater("rewritetujuan","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&mode=tujuan",
        onLoading: function(request) 
        {
            Element.show('progress_tujuan');
            document.getElementById('rewritejadwal').innerHTML="";
						//Element.hide('rewritejadwal');
        },
        onComplete: function(request) 
        {
            Element.show('rewritetujuan');
						Element.hide('progress_tujuan');
						//Element.show('rewriteall');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJadwal(id_jurusan){
	
	tgl=document.getElementById('p_tgl_user').value;
	//document.getElementById('p_jadwal').value="";
	
	// mengupdate JAM sesuai dengan RUTE yang dipilih
	document.getElementById('id_jurusan_aktif').value = id_jurusan;
	
	document.getElementById('rewritemobil').innerHTML = "";
	
	new Ajax.Updater("rewritejadwal","reservasi.php?sid="+SID, 
	{
		asynchronous: true,
		method: "get",
		parameters: "tgl=" + tgl + "&id_jurusan=" + id_jurusan +"&mode=jam",
		onLoading: function(request) 
		{
			document.getElementById('rewritejadwal').innerHTML="";	
			Element.show('progress_jam');
		},
			onComplete: function(request) 
		{
			Element.hide('progress_jam');
			Element.show('rewritejadwal');
		},                
		onFailure: function(request) 
		{ 
			assignError(request.responseText);
		}
	});
}

function changeRuteJam(){
	getUpdateMobil();
	getDataListDiscount(0,0);
	//getUpdateWaitingList();
	//setSisaKursi();
	
}

var obj_jadwal_dipilih;

function pilihJadwal(obj){
	if(obj_jadwal_dipilih){
		obj_jadwal_dipilih.className='a';
	}
	
	obj.className='selected';
	obj_jadwal_dipilih=obj;
}

function getUpdateMobil(){
		
    // update informasi layout-mobil sesuai TANGGAL, RUTE dan JAM
    var tgl  		= document.getElementById('p_tgl_user').value;
    var jadwal  = document.getElementById('p_jadwal').value;
		
		if(!document.getElementById('kode_booking') || jadwal=="" || jadwal=="(none)"){
			kode_booking="";
			
			if(document.getElementById('flag_mutasi').value!=1){
				document.getElementById('dataPelanggan').innerHTML='';
			}
			
		}
		else{
			kode_booking=document.getElementById('kode_booking').value;
		}

		new Ajax.Updater("rewritemobil","reservasi.layoutkursi.php?sid="+SID,
    {
        asynchronous: true,
        method: "get",
        parameters: "tanggal=" + tgl + "&jadwal="+ jadwal +"&kode_booking_dipilih="+kode_booking,
        onLoading: function(request) 
        {
          count_timer_layout_kursi = interval_halt_layout;
					Element.show('progress_kursi'); 
        },
        onComplete: function(request) 
        {   	    
					count_timer_layout_kursi = interval_timer_layout;
					document.getElementById('answerrute').innerHTML=document.getElementById('p_jadwal').value;  		
					Element.hide('progress_kursi');
					
					
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getUpdateStatusKursi(no_kursi){
		
    // update informasi layout-mobil sesuai TANGGAL, RUTE dan JAM
    var tgl  		= document.getElementById('p_tgl_user').value;
    var jadwal  = document.getElementById('p_jadwal').value;
		
		target_element	= "rewrite_kursi"+no_kursi;	
		
		new Ajax.Updater(target_element,"reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=ambilstatuskursi&no_kursi=" + no_kursi + "&tanggal=" + tgl + "&jadwal="+ jadwal,
        onLoading: function(request) 
        {
					count_timer_layout_kursi = interval_halt_layout;
					Element.show('progress_kursi'); 
        },
        onComplete: function(request) 
        {
					count_timer_layout_kursi = interval_timer_layout;
					Element.hide('progress_kursi');
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getPengumuman(){

		new Ajax.Updater("rewritepengumuman","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=pengumuman",
        onLoading: function(request) 
        {
            
        },
        onComplete: function(request) 
        {   
					
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getUpdateWaitingList(){
		
    // update informasi layout-paket sesuai TANGGAL, RUTE dan JAM
    var tgl  = document.getElementById('p_tgl_user').value;
    var jam  = document.getElementById('p_jadwal').value;
    
		new Ajax.Updater("rewritewaitinglist","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "tanggal=" + tgl + "&jam="+ jam +"&mode=waiting_list&aksi=tampilkan",
        onLoading: function(request) 
        {
					Element.show('progress_waiting_list'); 
        },
        onComplete: function(request) 
        {   	    
					Element.hide('progress_waiting_list'); 
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });       
}

function setDialogSPJ(){
    //menampilkan dialog spj

		tgl_berangkat  	= document.getElementById('p_tgl_user').value;
		sopir_sekarang  = document.getElementById('hide_nama_sopir_sekarang').value;
    mobil_sekarang  = document.getElementById('hide_mobil_sekarang').value;
		kode_jadwal  		= document.getElementById('p_jadwal').value;
		no_spj					= document.getElementById('txt_spj').value;  
		
		new Ajax.Updater("rewritedialogspj","SPJ.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters:
          "mode=0"+
          "&tglberangkat="+tgl_berangkat+
          "&kodejadwal="+kode_jadwal+
          "&sopirdipilih="+sopir_sekarang+
          "&mobildipilih="+mobil_sekarang+
          "&nospj="+no_spj,
        onLoading: function(request) 
        {
					document.getElementById('hide_nama_sopir_sekarang').innerHTML="";
					document.getElementById('hide_mobil_sekarang').innerHTML="";
          Element.show('progress_dialog_spj');
        },
        onComplete: function(request) 
        {
          Element.hide('progress_dialog_spj');
          getListDataMobilSopir();
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText);
        }
    });

	dialog_SPJ.show();
}

var mobils  = [];
var mobilsid= [];
var sopirs  = [];
var sopirsid= [];

function getListDataMobilSopir(){

  kodejadwal  = document.getElementById('p_jadwal').value;

  new Ajax.Request("SPJ.controldata.php?sid="+SID,{
    asynchronous: true,
    method: "get",
    parameters:
      "mobildipilih="+mobildipilih.value+
      "&sopirdipilih="+sopirdipilih.value+
      "&kodejadwal="+kodejadwal,

    onLoading: function(request){
    },
    onComplete: function(request){
    },
    onSuccess: function(request){

      //alert(request.responseText);

      eval(request.responseText);

      new Combobox.Local('manflistmobil','manflistmobilchoices',mobils,{ignoreCase:true,choices:15,partialSearch:true,afterUpdateElement:setMobilManifest});
      new Combobox.Local('manflistsopir','manflistsopirchoices',sopirs,{ignoreCase:true,choices:15,partialSearch:true,afterUpdateElement:setSopirManifest});

    },
    onFailure: function(request){
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });


}

function setMobilManifest(text,li){
  ditemukan=false;

  i=0;
  while(i<mobils.length && !ditemukan){
    str1=mobils[i];
    str2=text.value;
    if(str1.toUpperCase()==str2.toUpperCase()){
      mobildipilih.value=mobilsid[i];
      ditemukan=true;
    }
    i++;
  }

  if(!ditemukan){
    manflistmobil.style.background="red";
    mobildipilih.value="";
  }
}

function setSopirManifest(text,li){
  ditemukan=false;

  i=0;
  while(i<sopirs.length && !ditemukan){
    str1=sopirs[i];
    str2=text.value;
    if(str1.toUpperCase()==str2.toUpperCase()){
      sopirdipilih.value=sopirsid[i];
      ditemukan=true;
    }
    i++;
  }

  if(!ditemukan){
    manflistsopir.style.background="red";
    sopirdipilih.value="";
  }
}

function getDataListDiscount(flag_koreksi,jenis_penumpang){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		kode_jadwal  		= document.getElementById('p_jadwal').value;
		tgl  						= document.getElementById('p_tgl_user').value;
		
		if(flag_koreksi==0){
			rewrite	= "rewrite_list_discount";
		}
		else{
			rewrite	= "rewrite_list_discount_dialog";
		}
		
		new Ajax.Updater(rewrite,"reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "mode=list_jenis_discount&kode_jadwal="+kode_jadwal+"&tgl_berangkat="+tgl+"&flag_koreksi="+flag_koreksi+"&jenis_penumpang="+jenis_penumpang,
	        onLoading: function(request) 
	        {
	        },
	        onComplete: function(request) 
	        {
	        },
	        onSuccess: function(request) 
	        {
	        },
	        onFailure: function(request) 
	        {
	          assignError(request.responseText);
	        }
	  }) 
}

function getDataListAsuransi(no_tiket){
	// Mendapatkan dan menampilkan list plan asuransi
	
	new Ajax.Updater("rewrite_list_plan_asuransi","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "mode=listplanasuransi&no_tiket="+no_tiket,
	        onLoading: function(request) 
	        {
	        },
	        onComplete: function(request) 
	        {
	        },
	        onSuccess: function(request) 
	        {
	        },
	        onFailure: function(request) 
	        {
	          assignError(request.responseText);
	        }
	  }) 
}

function showChair(chno,no_tiket){
	
	// Mendapatkan nilai kursi dari database (why ? biar ga bentrok :P)
	document.getElementById('flag_mutasi').value=0; 
	no_spj	= document.getElementById('txt_spj').value; 
	document.getElementById('hdn_id_member').value="";
	document.getElementById("dataPelanggan").innerHTML="<table><tr><td height='500'></td></tr></table>";
	
	new Ajax.Updater("dataPelanggan","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=showchair&kursi="+chno+"&no_tiket="+no_tiket+"&no_spj="+no_spj,
        onLoading: function(request) 
        {
					Element.show('loading_data_penumpang');
        },
        onComplete: function(request) 
        {              
					Element.hide('loading_data_penumpang');
					
					if(document.getElementById("kode_booking")){
						getUpdateMobil();
					}
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function updateTemp(chno){
  // Mendapatkan nilai kursi dari database (why ? biar ga bentrok :P)
	
	tgl  				= document.getElementById('p_tgl_user').value;
	jam 	 			= document.getElementById('p_jadwal').value; 	
	layout_kursi= document.getElementById('hide_layout_kursi').value; 	
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}

	if(document.getElementById('flag_mutasi').value!=1){
		document.getElementById('dataPelanggan').innerHTML='';
	}
	else{
		mutasiPenumpang(chno);
		return;
	}
	
	target_element	= "rewrite_kursi"+chno;	
	
	new Ajax.Updater(target_element,"reservasi.php?sid="+SID, 
  {
      asynchronous: true,
      method: "get",
      parameters: "tanggal=" + tgl + "&jam="+ jam +"&kursi="+chno+"&no_spj="+no_spj+"&layout_kursi="+layout_kursi+"&mode=temp",
      onLoading: function(request) 
      {
				Element.show('progress_kursi'); 
      },
      onComplete: function(request) 
      {
      	Element.hide('progress_kursi');
      },
      onSuccess: function(request) 
   		{
   		},
      onFailure: function(request) 
      { 
				assignError(request.responseText); 
      }
  });      
		
}

function pesanKursi(go_show){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	tgl  					= document.getElementById('p_tgl_user').value;
  	kode_jadwal  	= document.getElementById('p_jadwal').value;
	id_member			= document.getElementById('hdn_id_member').value;
	telp_old			= document.getElementById('hdn_no_telp_old').value;
	nama  				= document.getElementById('fnama').value;
	alamat  			= document.getElementById('fadd').value;
  	telepon  			= document.getElementById('ftelp').value;
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	
	if(document.getElementById('opt_jenis_discount')){
		id_discount		= document.getElementById('opt_jenis_discount').value;
	}
	else{
		id_discount="";
	}
	valid=true;
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
	
  if ((nama=='') || (nama==String.Empty)){
		Element.show('nama_invalid');
		valid=false;
	}
  
	if ((telepon=='') || !cekValueNoTelp(telepon)){
		Element.show('telp_invalid');
		valid=false;
  }
	
  if(!valid){
		return;
	}

  isgoshow.value  = go_show;

  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"id_member="+id_member+"&nama="+nama+"&alamat="+alamat+"&no_telp_old="+telp_old+
			"&telepon="+telepon+"&tanggal=" +tgl+ "&kode_jadwal="+kode_jadwal+
			"&no_spj="+no_spj+"&layout_kursi="+layout_kursi+"&jenis_discount="+id_discount+"&mode=book",
			
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {

      var prosesok    = false;
      var status      = "";
      var kodebooking = "";
      var notiket     = "";
      var listnokursi = "";

      eval(request.responseText);

      if(prosesok){
        switch(status){
          case 0:
            document.getElementById('hdn_id_member').value='';
            document.getElementById('hdn_no_telp_old').value='';
            document.getElementById('id_member').value='';
            document.getElementById('fnama').value='';
            document.getElementById('fadd').value='';
            document.getElementById('ftelp').value='';

            Element.show('rewrite_list_discount');
            Element.hide('rewrite_keterangan_member');

            data_kursi  = listnokursi.split("/");

            jumlah_kursi_dipesan	= data_kursi.length;

            for(idx=0;idx<jumlah_kursi_dipesan-1;idx++){
              getUpdateStatusKursi(data_kursi[idx]);
            }

            if(go_show==1){
              kodebookingdicetak.value=kodebooking;
              notiketdicetak.value=notiket;
              document.getElementById('cetaksemuatiket').value=1;
              dialog_pembayaran.show(kodebooking);
            }

            getDataListDiscount(0,0);
            Element.hide('progress_kursi');
            break;

          case 1:
            alert('Anda belum memilih kursi yang akan dipesan!');
            break;

          case 2:
            alert("Penumpang dengan nomor telepon " + telepon + " sudah pernah membooking kursi, untuk dapat melakukan pembooking, silahkan lakukan pelunasan atau batalkan pemesanan sebelumnya.");
            periksaJadwal(telepon);
            break;

          case 3:
            alert("Anda tidak diperbolehkan membooking kursi lebih dari 8 hari dari hari ini.");
            break;
        }
      }
      else{
        alert("Terjadi kegagalan!");
      }

			Element.hide('progress_kursi');
			Element.hide('label_not_found');

		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function ubahDataPenumpang(no_tiket){
  
  // melakukan saving / update data penumpang/
	kursi  				= document.getElementById('kursi').value;
	nama  				= document.getElementById('ubah_nama_penumpang').value;
	alamat  			= document.getElementById('ubah_alamat_penumpang').value;
  telepon  			= document.getElementById('ubah_telp_penumpang').value;
  id_discount		= document.getElementById('id_discount').value;
	id_member			= !document.getElementById('hdn_ubah_id_member')?"":document.getElementById('hdn_ubah_id_member').value;
	
	valid=true;
	
  if ((nama=='') || (nama==String.Empty)){
		Element.show('ubah_nama_invalid');
		valid=false;
	}
  
	if ((telepon=='') || !cekValueNoTelp(telepon)){
		Element.show('ubah_telp_invalid');
		valid=false;
  }
	
  if(!valid){
		return;
	}
	
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"no_tiket="+no_tiket+"&nama="+nama+"&alamat="+alamat+"&telepon="+telepon+"&kursi="+kursi+"&id_discount="+id_discount+"&id_member="+id_member+"&mode=ubahdatapenumpang",
			
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			if(request.responseText==1){
				alert('Data penumpang berhasil diubah!');
			}
			else{
				alert("Terjadi kegagalan!");
			}
			
			Element.hide('progress_kursi');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}


var batal_no_tiket,batal_no_kursi;

function tampilkanDialogPembatalan(no_tiket,no_kursi){
	document.getElementById('batal_username').value	= '';
	document.getElementById('batal_password').value	= '';
	dialog_pembatalan.show();
	batal_no_tiket	= no_tiket;
	batal_no_kursi	= no_kursi;
}

function batal(no_tiket,no_kursi){
	
	if(confirm("Apakah anda yakin akan membatalkan tiket ini?")){
	
		new Ajax.Request("reservasi.php?sid="+SID,{
			asynchronous: true,
			method: "post",
			parameters: "mode=pembatalan"+
				"&no_tiket="+no_tiket+
				"&no_kursi="+no_kursi+
				"&username="+batal_username.value+
				"&password="+batal_password.value,
			onLoading: function(request) {
				Element.show('loading_data_penumpang');
			},
			onComplete: function(request) 
			{
			
			},
			onSuccess: function(request) {
				
				if(request.responseText==1){
					document.getElementById('dataPelanggan').innerHTML='';
					getUpdateMobil();
				}
				else{
					alert("Anda tidak memiliki wewenang untuk membatalkan tiket ini!");
				}
				
				Element.hide('loading_data_penumpang');
				dialog_pembatalan.hide();
			},
			onFailure: function(request) 
			{
			}
			})  
	}
	
	return false;
		
}

function saveWaitingList(){
  
  // melakukan saving / update waiting list/
	tgl  			= document.getElementById('p_tgl_user').value;
  jam  			= document.getElementById('p_jadwal').value;  
	nama  		= document.getElementById('fnama').value;
	alamat  	= document.getElementById('fadd').value;
  telepon  	= document.getElementById('ftelp').value;
  hp 				= document.getElementById('fhp').value;
	jum_kursi_wl= document.getElementById('txt_jum_kursi_wl').value;
	
	if (document.getElementById('txt_spj').value!=""){
		exit;
	}
	
	ValidasiAngka(document.getElementById('txt_jum_kursi_wl'),"Jum kursi waiting list");
	jum_kursi_wl	= document.getElementById('txt_jum_kursi_wl').value;	
	
	if(jum_kursi_wl<=0){
		alert("Jumlah kursi waiting list tidak boleh nol!");
		exit;
	}
	
  // validasi	
	if ((nama=='') || (nama==String.Empty)){
		alert('Nama Tidak Boleh Kosong !'); 
    exit;
	  //return;
  }
  if ((telepon=='') || (telepon==String.Empty) || (hp=='') || (hp==String.Empty)){
		alert('Telepon Tidak Boleh Kosong !'); 
    exit;
	  //return;
  }
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "nama="+nama+"&add="+alamat+"&telepon="+telepon+"&hp="+hp+"&tanggal=" +tgl+ "&jam="+jam+"&jum_kursi="+jum_kursi_wl+"&mode=waiting_list&aksi=tambah",
    onLoading: function(request) 
    {
			Element.show('progress_waiting_list');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {
			document.getElementById('fnama').value='';
			document.getElementById('fadd').value='';
			document.getElementById('ftelp').value='';
			document.getElementById("fhp").value='';
			document.getElementById("txt_jum_kursi_wl").value='1';
			
			getUpdateWaitingList(); 
			
			Element.hide('progress_waiting_list');
				
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
}

function deleteWaitingList(id_waiting_list){
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "id_waiting_list="+id_waiting_list+"&mode=waiting_list&aksi=hapus",
    onLoading: function(request) 
    {
			Element.show('progress_waiting_list');
    },
    onComplete: function(request) 
    {			
    },
    onSuccess: function(request) 
    {
			getUpdateWaitingList(); // update layout waiting list
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function tampilkanDialogCetakUlangTiket(no_tiket){
	document.getElementById('cetak_ulang_tiket_username').value	= '';
	document.getElementById('cetak_ulang_tiket_password').value	= '';
	dialog_cetak_ulang_tiket.show();
}

function tampilkanDialogCetakUlangTiketOTP(no_tiket){
	document.getElementById('cetak_ulang_tiket_password_otp').value	= '';
	dialog_cetak_ulang_tiket_otp.show();
}

function tampilkanDialogDiscount(no_tiket,jenis_penumpang){
	document.getElementById('hdn_bayar_no_tiket').value=no_tiket;
	getDataListDiscount(1,jenis_penumpang);
	document.getElementById('korek_disc_username').value	= '';
	document.getElementById('korek_disc_password').value	= '';
	dialog_discount.show();
}

function ubahTiket(){
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_tiket="+dlg_no_tiket.innerHTML+
								"&nama="+dlg_nama.value+
								"&add="+dlg_address.value+
								"&telepon="+dlg_telp.value+
								"&hp="+dlg_hp.value+
								"&discount="+dlg_discount.value+
								"&mode=ubahTiket",
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {
       alert("Data tiket telah diubah!");
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function cetakTiketCepat(no_tiket){
	document.getElementById('notiketdicetak').value=no_tiket;
	document.getElementById("cetaksemuatiket").value=0;
	dialog_pembayaran.show();
}

function CetakTiket(jenispembayaran){
  //mencetak tiket
  notiket   = notiketdicetak.value;

	layout_kursi= document.getElementById('hide_layout_kursi').value;
	parameter		= "tiket.php?sid="+SID+"&cetaktiket=0"+
		"&kodebooking="+kodebookingdicetak.value+
    "&notiket="+notiket+
		"&jenispembayaran="+jenispembayaran+
		"&layoutkursi="+layout_kursi+
		"&cetaksemuatiket="+cetaksemuatiket.value+
    "&isgoshow="+isgoshow.value;

  notiketdicetak.value    = "";
  kodebookingdicetak.value= "";

  Start(parameter);
	
	dialog_pembayaran.hide();
	getUpdateMobil();
	
	document.getElementById("dataPelanggan").innerHTML="";
}

function CetakTiketByVoucher(kode_voucher){
  //mencetak tiket 
	
	if(!document.getElementById('kode_booking')){
		kode_booking	= document.getElementById('kode_booking_go_show').value;
		cetak_tiket	= 0;
	}
	else{
		kode_booking	= document.getElementById('kode_booking').value;
		cetak_tiket	= document.getElementById('cetak_tiket')?document.getElementById('cetak_tiket').value:0;
	}
	
	if(document.getElementById('no_tiket')){
		no_tiket	= document.getElementById('no_tiket').value;
	}
	else{
		//no_tiket	= document.getElementById('no_tiket_goshow').value
		no_tiket	= notiketdicetak.value
	}

	layout_kursi= document.getElementById('hide_layout_kursi').value;
	
	parameter		= "tiket.php?sid="+SID+"&cetaktiket="+cetak_tiket+
		"&kodebooking="+kode_booking+"&notiket="+no_tiket+
		"&jenispembayaran=3"+
		"&layoutkursi="+layout_kursi+"&kode_voucher="+kode_voucher;

  Start(parameter);
	
	dialog_pembayaran.hide();
	getUpdateMobil();
	
	document.getElementById("dataPelanggan").innerHTML="";
}

function cetakUlangTiket(){

	username	= document.getElementById('cetak_ulang_tiket_username').value;
	password	= document.getElementById('cetak_ulang_tiket_password').value;
	no_tiket	= notiketdicetak.value;
	
	if(username=='' || password==''){
		alert('Anda belum memasukkan username dan password yang memiliki otoritas untuk proses ini!');
		return;
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=cetakulangtiket&no_tiket="+no_tiket+"&username="+username+"&password="+password+"&cetaksemuatiket=0",
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
			dialog_cetak_ulang_tiket.hide();
			eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}

/*function PilihTiket(){
  //mencetak SPJ
	
	tgl_berangkat  	= document.getElementById('p_tgl_user').value;
  kode_jadwal 		= document.getElementById('p_jadwal').value;
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
  Start("tiket.php?sid={SID}&tgl_berangkat="+tgl_berangkat+"&kode_jadwal="+kode_jadwal+"&mode=pilih_tiket&layout_kursi="+layout_kursi);
}*/

function CetakSPJ(){

  //mencetak SPJ
	tgl_berangkat  	= document.getElementById('p_tgl_user').value;
  kode_jadwal 		= document.getElementById('p_jadwal').value; 
  sopir_dipilih		= document.getElementById('sopirdipilih').value;
	mobil_dipilih		= document.getElementById('mobildipilih').value;
	no_spj					= document.getElementById('txt_spj').value;
	parameter_biaya_bbm	= document.getElementById('biaya_bbm')?"&biaya_bbm="+document.getElementById('biaya_bbm').value:"";	
  username				= document.getElementById('username_bbm').value;
	alasan					= document.getElementById('alasan_bbm').value;
	signature				= document.getElementById('signature_bbm').value;
	iscetakulangbbm	= document.getElementById('chkcetakulangvoucherbbm')?(document.getElementById('chkcetakulangvoucherbbm').value?1:0):0;

	if(sopir_dipilih!='' && mobil_dipilih!=''){	
		Start("SPJ.php?sid="+SID+
					"&mode=1&tglberangkat="+tgl_berangkat+"&kodejadwal="+kode_jadwal+
					"&mobildipilih="+mobil_dipilih+"&sopirdipilih="+sopir_dipilih+
					"&nospj="+no_spj+parameter_biaya_bbm+
					"&username="+username+"&signature="+signature+
					"&iscetakulangvoucher="+iscetakulangbbm);
		getUpdateMobil();
		dialog_SPJ.hide();
	}
	else{
		alert("Anda belum memilih kendaraan atau nama sopir!");
	}
}

function cariDataPelangganByTelp(no_telp){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
	
	Element.hide('telp_invalid');
	Element.hide('nama_invalid');
	Element.hide('alamat_invalid');
	
	if(no_telp=="" || document.getElementById('hdn_id_member').value!=""){
		return;
	}
	
	if(!cekValueNoTelp(no_telp)){	
		alert("Nomor telepon yang anda masukkan tidak benar!");
		return;
	}
		
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_telp="+no_telp+
								"&mode=periksa_no_telp&paket=0",
    onLoading: function(request) 
    {
			Element.hide('label_not_found');
			Element.show('progress_cari_penumpang');
    },
    onComplete: function(request) 
    {
			Element.hide('progress_cari_penumpang');
    },
    onSuccess: function(request) 
    {		
			if(request.responseText==0){
				Element.show('label_not_found');
			}
			else{
				eval(request.responseText);
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function cariDataMemberById(id_member){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
	
	Element.hide('telp_invalid');
	Element.hide('nama_invalid');
	Element.hide('alamat_invalid');
	
	if(id_member==""){
		if(!document.getElementById('hdn_ubah_id_member')){
			document.getElementById('hdn_id_member').value="";
		}
		else{
			document.getElementById('hdn_ubah_id_member').value="";
		}
		return;
	}
		
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "id_member="+id_member+
								"&mode=periksa_id_member",
    onLoading: function(request) 
    {
			Element.hide('label_member_not_found');
			Element.show('progress_cari_member');
    },
    onComplete: function(request) 
    {
			Element.hide('progress_cari_member');
    },
    onSuccess: function(request) 
    {		
			if(request.responseText==0){
				if(!document.getElementById('hdn_ubah_id_member')){
					document.getElementById('hdn_id_member').value="";
				}
				else{
					document.getElementById('hdn_ubah_id_member').value="";
				}
				Element.show('label_member_not_found');
			}
			else{
				eval(request.responseText);
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function periksaMember(no_kartu){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
	if(no_kartu==""){
		/*document.getElementById('hdn_id_member').value = ""; 
		document.getElementById('fnama').value = ""; 
		document.getElementById('fadd').value = "";	
		document.getElementById('ftelp').value = "";
		document.getElementById('fhp').value = "";*/
		return;
	}
		
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_kartu="+no_kartu+
								"&mode=periksa_member_by_kartu",
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {		
			Element.hide('progress_kursi');
       if(request.responseText!='false'){
				eval(request.responseText);
			 }
			 else{
				//document.getElementById('hdn_id_member').value = ""; 
				document.getElementById('fnama').value = ""; 
				document.getElementById('fadd').value = "";	
				document.getElementById('ftelp').value = "";
				document.getElementById('fhp').value = "";
				alert("No Kartu tidak terdaftar!");
			 }
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function periksaJadwal(no_telp){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		//no_telp=document.getElementById('ftelp').value;
		document.getElementById('rewrite_keberangkatan_pelanggan').innerHTML="";
		
		if(no_telp==""){
			alert("Anda belum memasukkan no telepon yang akan dicari!");
			return;
		}
		
		dialog_cari_jadwal.show();
		
		new Ajax.Updater("rewrite_keberangkatan_pelanggan","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "no_telp=" +no_telp+"&mode=cari_jadwal_pelanggan",
	        onLoading: function(request){
						Element.show('progress_cari_jadwal');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_cari_jadwal');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
}

function koreksiDiscount(no_tiket){

	jenis_discount=document.getElementById('opt_jenis_discount_koreksi').value;
	username	= document.getElementById('korek_disc_username').value;
	password	= document.getElementById('korek_disc_password').value;
	
	if(jenis_discount==''){
		jum_discount=0;
	}
	
	if(username=='' || password==''){
		alert('Anda belum memasukkan username dan password yang memiliki otoritas untuk proses ini!');
		return;
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=beridiscount&jenis_discount="+jenis_discount+"&no_tiket="+no_tiket+"&username="+username+"&password="+password,
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
      if(request.responseText==1){
				alert("Pengubahan harga berhasil disimpan");
				getUpdateMobil(); // update layout mobil :
				dialog_discount.hide();
			}
			else if(request.responseText==0){
				document.getElementById('korek_disc_username').value	= '';
				document.getElementById('korek_disc_password').value	= '';
				alert("Anda tidak memiliki otoritas untuk melakukan proses ini");
			}
			else{
				alert("Terjadi kegagalan dalam pengubahan harga");
			}
			
			
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}

function koreksiAsuransi(no_tiket){
	
	plan_asuransi=document.getElementById('plan_asuransi').value;
	username	= document.getElementById('korek_asuransi_username').value;
	password	= document.getElementById('korek_asuransi_password').value;
	
	
	if(username=='' || password==''){
		alert('Anda belum memasukkan username dan password yang memiliki otoritas untuk proses ini!');
		return;
	}
	
	tgl_lahir="";
	
	if(plan_asuransi>0){
		tgl_lahir	= document.getElementById("opt_thn_lahir").value+"-"+ Right("0"+document.getElementById("opt_bln_lahir").value,2)+"-"+ Right("0"+document.getElementById("opt_tgl_lahir").value,2);
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=koreksiasuransi&plan_asuransi="+plan_asuransi+"&tgl_lahir="+tgl_lahir+"&no_tiket="+no_tiket+"&username="+username+"&password="+password,
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
			document.getElementById('korek_asuransi_username').value	= '';
			document.getElementById('korek_asuransi_password').value	= '';
			
      if(request.responseText==1){
				alert("Koresi asuransi berhasil dilakukan");
				dialog_koreksi_asuransi.hide();
			}
			else if(request.responseText==0){
				alert("Anda tidak memiliki otoritas untuk melakukan proses ini");
			}
			else{
				alert("Terjadi kegagalan dalam koreksi asuransi");
			}
			
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}


function showDialogSwapKartu(){
	tgl_berangkat = document.getElementById('p_tgl_user').value;
  kode_jadwal 	= document.getElementById('p_jadwal').value; 
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	
	this.open("member_transaksi.php?sid="+SID+"&tgl_berangkat="+tgl_berangkat+"&kode_jadwal="+kode_jadwal+"&layout_kursi="+layout_kursi,"CtrlWindow","top=200,left=200,width=600,height=400,scrollbars=1,resizable=0");	
	
}

function bayarByKartu(kode_book){
	tgl_berangkat = document.getElementById('p_tgl_user').value;
  kode_jadwal 	= document.getElementById('p_jadwal').value; 
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	this.open("member_transaksi.php?sid="+SID+"&mode=pilih_bayar&kode_book="+kode_book+"&kode_jadwal="+kode_jadwal+"&tgl_berangkat="+tgl_berangkat+"&layout_kursi="+layout_kursi,"CtrlWindow","top=200,left=200,width=600,height=400,scrollbars=1,resizable=0");	
	
}

function showDialogMutasi(){

	if(document.getElementById('flag_mutasi').value==1){
		setFlagMutasi();exit;
	}
	
	document.getElementById('mutasi_username').value	= '';
	document.getElementById('mutasi_password').value	= '';
	dlgmutasi.show();
}

function konfirmMutasi(){
	
	username	= document.getElementById('mutasi_username').value;
	password	= document.getElementById('mutasi_password').value;
	
	if(username=='' || password==''){
		alert('Anda belum memasukkan username dan password yang memiliki otoritas untuk proses ini!');
		return;
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=konfirmmutasi&username="+username+"&password="+password,
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request){
      eval(request.responseText);
			Element.hide('progress_kursi');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}


function setFlagMutasi(){
	
	if(document.getElementById('flag_mutasi').value!=1){
		if(confirm("Anda akan mengaktifkan mode mutasi, " + 
			"ketika anda memilih kursi, maka secara otomatis kursi lama akan dimutasikan ke kursi yang baru."+
			"Klik OK untuk melanjutkan proses mutasi atau klik CANCEL untuk keluar dari mode mutasi!")){
			lanjut = true;
		}
		else{
			lanjut = false;
		}
	}
	else{
		lanjut=true;
	}
	
	if(lanjut){
		document.getElementById('flag_mutasi').value	= 1-document.getElementById('flag_mutasi').value;
		
		if(document.getElementById('flag_mutasi').value==1){
			document.getElementById('btn_mutasi').value="BATALKAN MUTASI";
		}
		else{
			document.getElementById('btn_mutasi').value="MUTASI PENUMPANG";
		}
	}
}

function mutasiPenumpang(no_kursi){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	tgl  					= document.getElementById('p_tgl_user').value;
  kode_jadwal  	= document.getElementById('p_jadwal').value;  
	//id_member			= document.getElementById('hdn_id_member').value;
	no_tiket			= document.getElementById('no_tiket').value;
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	
	username	= document.getElementById('mutasi_username').value;
	password	= document.getElementById('mutasi_password').value;
	
	if(!confirm("Apakah anda yakin akan memindahkan penumpang ke kursi ini?")){
		exit;
	}
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: 
			"mode=mutasipenumpang&no_tiket="+no_tiket+"&no_kursi="+no_kursi+"&tanggal=" +tgl+ "&kode_jadwal="+kode_jadwal+
			"&no_spj="+no_spj+"&layout_kursi="+layout_kursi+"&username="+username+"&password="+password,
			
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			
			if(request.responseText==1){
				document.getElementById('flag_mutasi').value=0;
				document.getElementById('dataPelanggan').innerHTML='';
				
				getUpdateMobil(); // update layout mobil :
				
			}
			else if(request.responseText==2){
				getUpdateMobil(); // update layout mobil :
				alert("Kursi yang dituju sudah dipesan oleh penumpang lain!");
			}
			else{
				alert("Terjadi kegagalan!"+request.responseText);
			}
			
			Element.hide('progress_kursi');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function setComboTglLahir(tgl_dipilih,bulan,tahun){
	opt_tanggal_lahir	= "<select id='opt_tgl_lahir'>";
	
	if(((bulan%2==0 && bulan<8) || (bulan%2>0 && bulan>=8)) && bulan!=2){
		tgl_max=30;
	}
	else if(bulan!=2){
		tgl_max=31;
	}
	else{
		if(tahun%4!=0){
			tgl_max=28;
		}
		else{
			tgl_max=29;
		}
	}
	
	for(tgl=1;tgl<=tgl_max;tgl++){
		temp_tgl	= "0"+tgl;
		
		if(Right(temp_tgl,2)!=Right("0" + tgl_dipilih,2)){selected="";}else{selected="selected";}
		opt_tanggal_lahir	+= "<option value='"+tgl+"' "+selected+">"+tgl+"</option>";
	}
			
	opt_tanggal_lahir	+= "</select>";
	
	document.getElementById('span_tgl_lahir').innerHTML=opt_tanggal_lahir;
}

function setSisaKursi(){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
	/*id_jurusan		= document.getElementById('id_jurusan_aktif').value
	tgl_berangkat = document.getElementById('p_tgl_user').value;
  jam_berangkat	= document.getElementById('jam_berangkat_aktif').value; 	
		
	new Ajax.Updater("rewrite_sisa_kursi","reservasi.php?sid="+SID, 
	{
	      asynchronous: true,
	      method: "get",
	      parameters: "mode=sisa_kursi_next&id_jurusan="+id_jurusan+"&tgl_berangkat=" +tgl_berangkat+"&jam_berangkat="+jam_berangkat,
	      onLoading: function(request){
	      },
	      onComplete: function(request) {
	      },
	      onSuccess: function(request){
					
	      },
	      onFailure: function(request) 
	      {
	         assignError(request.responseText);
	      }
	}) */
}

function hitungTotalBiayaOperasional(){
	
	biaya_sopir	= document.getElementById('biayasopir').value*1;
	biaya_tol		= document.getElementById('biayatol').value*1;
	biaya_parkir= document.getElementById('biayaparkir').value*1;
	biaya_bbm		= document.getElementById('biayabbm').value*1;
	
	total_biaya	= biaya_sopir + biaya_tol + biaya_parkir + biaya_bbm;
	
	document.getElementById('biayatotal').innerHTML	= FormatUang(total_biaya,".");
}

function showInputVoucher(){
	dialog_pembayaran.hide();
	document.getElementById("input_kode_voucher").value	= "";
	dialog_voucher.show();
}

function bayarByVoucher(kode_voucher){
	
	if(kode_voucher==''){
		alert("Anda belum memasukkan kode voucher");
		return false;
	}
	
	tgl  				= document.getElementById('p_tgl_user').value;
	id_jurusan	= document.getElementById('id_jurusan_aktif').value;
	
	if(document.getElementById('no_tiket')){
		no_tiket	= document.getElementById('no_tiket').value;
	}
	else{
		//no_tiket	= document.getElementById('no_tiket_goshow').value
		no_tiket	= notiketdicetak.value
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: 
			"kode_voucher="+kode_voucher+"&tanggal="+tgl+"&id_jurusan="+id_jurusan+"&no_tiket="+no_tiket+
			"&mode=bayar_by_voucher",
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			document.getElementById("input_kode_voucher").value="";
    },
    onSuccess: function(request) 
    {
			eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function setBiayaBBM(){

  if(mobildipilih.value==""){
    return false;
  }

	no_spj			= document.getElementById('txt_spj').value;  
	id_jurusan	= document.getElementById('tujuan').value;

	new Ajax.Request("SPJ.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "mode=2" +
			"&nospj="+no_spj+
			"&mobildipilih="+mobildipilih.value+
			"&idjurusan="+id_jurusan,
    onLoading: function(request) 
    {
			Element.show('loadingbiayabbm');
    },
    onComplete: function(request) 
    {
			Element.hide('loadingbiayabbm');
    },
    onSuccess: function(request) 
    {
      eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Terjadi Kegagalan');        
       assignError(request.responseText);
    }
  });
	
}

function isCetakUlangVoucherBBM(){
	onclickaction	= !document.getElementById('chkcetakulangvoucherbbm').checked?"CetakSPJ();":"showDialogVerifikasiVoucherBBM();";
	document.getElementById('btnok_dialogcetakulangvoucherbbm').setAttribute("onclick",onclickaction);
}

function showDialogVerifikasiVoucherBBM(){
	document.getElementById('username_bbm').value = "";
	document.getElementById('password_bbm').value = "";  
	document.getElementById('alasan_bbm').value = "";
	document.getElementById('signature_bbm').value = "";
	
	sopir_dipilih		= document.getElementById('list_sopir').value; 
	mobil_dipilih		= document.getElementById('list_mobil').value; 
	
	if(sopir_dipilih!='' && mobil_dipilih!=''){	
		dialog_SPJ.hide();
		dialog_voucherbbm.show();
	}
	else{
		alert("Anda belum memilih kendaraan atau nama sopir!");
	}
}

function verifikasiCetakVoucherBBM(){
	username	= document.getElementById('username_bbm').value;
	password	= document.getElementById('password_bbm').value;  
	alasan		= document.getElementById('alasan_bbm').value;
	
	if(username=="" || password==""){
		alert("Anda belum memasukkan username atau password anda!");
		exit;
	}
	
	if(alasan==""){
		alert("Anda harus memasukkan alasan anda mengapa anda mencetak ulang voucher BBM!");
		exit;
	}
	
	new Ajax.Request("SPJ.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "aksi=3" +
			"&username="+username+
			"&password="+password+
			"&alasan="+alasan,
    onLoading: function(request) 
    {
			Element.show('loadingverifikasibbm');
    },
    onComplete: function(request) 
    {
			Element.hide('loadingverifikasibbm');
    },
    onSuccess: function(request) 
    {
			eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Terjadi Kegagalan');        
       assignError(request.responseText);
    }
  });
	
}

var Combobox={};

Combobox.Local=Class.create(Autocompleter.Local,{
  initialize:function($super,element,update,array,options){
    $super(element,update,array,options);
    this.options.minChars=this.options.minChars||0;
    Event.observe(this.element,'click',this.onClick.bindAsEventListener(this));
  },
  onClick:function($super,event){
    if(Event.element(event).id){
      if(this.active){
        this.hasFocus=false;
        this.active=false;
        this.hide();
      }else{
        this.activate();
        this.render();
      }
      return;
    }
    $super(event);
  }
});

// global
var dialog_SPJ,dialog_ubah_tiket,btn_SPJ_OK,btn_SPJ_Cancel,msg;

function init(e) {
	
  // inisialisasi variabel
	SID =document.getElementById("hdn_SID").value;

	//control dialog box SPJ
	dialog_SPJ = dojo.widget.byId("dialog_SPJ");
	
	//control dialog box discount
	dialog_discount = dojo.widget.byId("dialog_discount");
  btn_discount_OK = document.getElementById("dialog_discount_btn_OK");
  btn_discount_Cancel = document.getElementById("dialog_discount_btn_Cancel");
  //dialog_discount.setCloseControl(btn_discount_OK);
  dialog_discount.setCloseControl(btn_discount_Cancel);
	
	//control dialog cetak ulang tiket
	dialog_cetak_ulang_tiket		 = dojo.widget.byId("dialog_cetak_ulang_tiket");
  btn_cetak_ulang_tiket_Cancel = document.getElementById("dialog_cetak_ulang_tiket_btn_Cancel");
  dialog_cetak_ulang_tiket.setCloseControl(btn_cetak_ulang_tiket_Cancel);
	
	//control dialog cetak ulang tiket OTP
	dialog_cetak_ulang_tiket_otp			= dojo.widget.byId("dialog_cetak_ulang_tiket_otp");
  btn_cetak_ulang_tiket_Cancel_otp 	= document.getElementById("dialog_cetak_ulang_tiket_btn_Cancel_otp");
  dialog_cetak_ulang_tiket_otp.setCloseControl(btn_cetak_ulang_tiket_Cancel_otp);
	

	//control dialog box koreksi asuransi
	dialog_koreksi_asuransi = dojo.widget.byId("dialog_koreksi_asuransi");
  btn_koreksi_asuransi_OK = document.getElementById("dialog_koreksi_asuransi_btn_OK");
  btn_koreksi_asuransi_Cancel = document.getElementById("dialog_koreksi_asuransi_btn_Cancel");
  dialog_koreksi_asuransi.setCloseControl(btn_koreksi_asuransi_Cancel);
	
	//control dialog cari keberangkatan
	dialog_cari_jadwal = dojo.widget.byId("dialog_cari_keberangkatan");
  btn_cari_jadwal_Cancel = document.getElementById("dialog_cari_jadwal_btn_Cancel");
  dialog_cari_jadwal.setCloseControl(btn_cari_jadwal_Cancel);
	
	//control dialog pembayaran
	dialog_pembayaran = dojo.widget.byId("dialog_pembayaran");
  btn_pembayaran_OK = document.getElementById("dialog_pembayaran_btn_OK");
  btn_pembayaran_Cancel = document.getElementById("dialog_pembayaran_btn_Cancel");
  dialog_pembayaran.setCloseControl(btn_pembayaran_Cancel);
	
	//control dialog box batal
	dialog_pembatalan = dojo.widget.byId("dialog_batal");
  dialog_pembatalan_btn_ok = document.getElementById("dialog_batal_btn_ok");
  dialog_pembatalan_btn_cancel = document.getElementById("dialog_batal_btn_Cancel");
  dialog_pembatalan.setCloseControl(dialog_pembatalan_btn_cancel);
	
	//control dialog box mutasi
	dlgmutasi = dojo.widget.byId("dialog_mutasi");
	
	//control dialog box input voucher
	dialog_voucher = dojo.widget.byId("dialog_input_voucher");
  dialog_voucher_btn_ok = document.getElementById("dialog_input_voucher_btn_ok");
  dialog_voucher_btn_cancel = document.getElementById("dialog_input_voucher_btn_ok_cancel");
  dialog_voucher.setCloseControl(dialog_voucher_btn_cancel);
	
	//control dialog cetak ulang voucher BBM
	dialog_voucherbbm = dojo.widget.byId("dialogcetakulangvoucherbbm");
	btncancel_dialogcetakulangvoucherbbm = document.getElementById("btncanceldialogcetakulangvoucherbbm");
	dialog_voucherbbm.setCloseControl(btncancel_dialogcetakulangvoucherbbm);
	
	getPengumuman();
	timerLayoutKursi();
	timerPengumuman();

}

dojo.addOnLoad(init);

var jdwl,tgl;

//TIMER LAYOUT KURSI
var interval_timer_layout	= 10; //detik
var interval_halt_layout = 300; //detik
var count_timer_layout_kursi	= interval_timer_layout;

function timerLayoutKursi(){
	if (count_timer_layout_kursi<1) {
		//reset count down
		count_timer_layout_kursi = interval_timer_layout;
		
		//merefresh layout mobil
		
		if(document.getElementById("rewritemobil").innerHTML!=""){
			getUpdateMobil();
		}	
	}
	
	count_timer_layout_kursi-=1;
	setTimeout("timerLayoutKursi()",1000);
	//document.title	= count_timer_layout_kursi;
}
//END TIMER LAYOUT KURSI

//TIMER PENGUMUMAN
var interval_timer_pengumuman	= 60; //detik
var count_timer_pengumuman		= interval_timer_pengumuman;

function timerPengumuman(){
	if (count_timer_pengumuman<1) {
		//reset count down
		count_timer_pengumuman = interval_timer_pengumuman;
		
		//merefresh layout mobil
		getPengumuman();
			
	}
	
	count_timer_pengumuman-=1;
	setTimeout("timerPengumuman()",1000);
}
//END TIMER PENGUMUMAN


//-->