function findPosY(obj) {
  var curtop = 0;

  if(obj.offsetParent) {
    while(1) {
      curtop += obj.offsetTop;
      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;
    }
  } else if(obj.y) {
    curtop += obj.y;
  }

  return curtop;
}

function findPosX(obj) {
  var curleft = 0;

  if(obj.offsetParent) {
    while(1) {
      curleft += obj.offsetLeft;
      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;
    }
  } else if(obj.x) {
    curleft += obj.x;
  }

  obj.style.position = "static";

  return curleft;
}

function cekValueNoTelp(no_telp){
	//cek 1
	jum_digit=no_telp.length;
	
	if(jum_digit<8){
		//alert(1);
		return false;
	}
	
	//cek 2
	kode_inisial=no_telp.substring(0,1);
	
	if(kode_inisial!=0){
		//alert(2);
		return false;
	}
	
	//cek 3
	kode_area=no_telp.substring(1,2)*1;
	
	if(kode_area<=1){
		//alert(3);
		return false;
	}
	
	return true;
	
}

function cekAngka(no_telp){
	//cek 1
	jum_digit=no_telp.length;
	
	if(jum_digit<8){
		//alert(1);
		return false;
	}
	
	//cek 2
	kode_inisial=no_telp.substring(0,1);
	
	if(kode_inisial!=0){
		//alert(2);
		return false;
	}
	
	//cek 3
	kode_area=no_telp.substring(1,2)*1;
	
	if(kode_area<=1){
		//alert(3);
		return false;
	}
	
	return true;
	
}

function validasiNoTelp(evt){
  var theEvent = evt || window.event;

  var key = theEvent.keyCode || theEvent.which;

  key = String.fromCharCode(key);

  var regex = /[0-9]/;

  if (evt.keyCode==8 || evt.keyCode==9 || evt.keyCode==13 ||
    evt.keyCode==46 || evt.keyCode==37 || evt.keyCode==39)  return true;

  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    theEvent.preventDefault();
  }
}

function validasiAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if (evt.keyCode==8 || evt.keyCode==9 || evt.keyCode==13 ||
		evt.keyCode==46 || evt.which==46 || evt.keyCode==37 || evt.keyCode==39)  return true;
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
	
	return true;
}

function valIn(evt){
  var theEvent = evt || window.event;

  if( theEvent.which==39 || theEvent.which==34 ) {
    theEvent.returnValue = false;
    theEvent.preventDefault();
  }

  return true;
}

function echeck(str){

	var at="@"
	var dot="."
	var lat=str.indexOf(at)
	var lstr=str.length
	var ldot=str.indexOf(dot)
	
	if (str.indexOf(at)==-1){
	  return false
	}

	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
	  return false
	}

	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
	   return false
	}

	if (str.indexOf(at,(lat+1))!=-1){
	   return false
	}

	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
	   return false
	}

	if (str.indexOf(dot,(lat+2))==-1){
	   return false
	}
	
	if (str.indexOf(" ")!=-1){
	   return false
	}

 	return true;
}

function submitPilihanMenu(menu_dipilih){
	SID 	= document.getElementById("hdn_SID").value;
	
	document.getElementById('menu_dipilih').value	= menu_dipilih;
	
	if(menu_dipilih=='top_menu_operasional'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_operasional.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_lap_reservasi'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_lap_reservasi.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_lap_paket'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_lap_paket.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_lap_keuangan'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_lap_keuangan.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_tiketux'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_tiketux.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_pengelolaan_member'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_pengelolaan_member.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_pengaturan'){
		document.getElementById('top_menu_dipilih').value	= menu_dipilih;
		document.form_pilih_menu.action ="./menu_pengaturan.php?sid="+SID;
		document.form_pilih_menu.submit();
	}
	
}

function openNewPage(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function formatAngka(num) {
	num = num.toString().replace(/\$|\,/g,'');
	
	if(isNaN(num)) num = "0";
	
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	
	if(cents<10) cents = "0" + cents;
	
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'+
		num.substring(num.length-(4*i+3));
	
	return (((sign)?'':'-') + num);
}

function formatAngkaKalkulasi(str){
	hasil	=str.toString().replace(/\./g,'');
	return hasil;
}

function isValidCss(obj,isvalid){
	obj.style.background	= !isvalid?"red":"white";
}

function ubahPassword(password_lama,password_baru,konfirmasi_password){
	
	if(password_baru==''){
		alert("Anda BELUM MEMASUKKAN Password baru anda!");
		return false;
	}
	
	if(password_baru!=konfirmasi_password){
		alert("Konfirmasi Password Baru anda tidak sama dengan Password Baru yang anda masukkan!");
		return false;
	}
	
	new Ajax.Request("./../_auth/auth.php?sid="+SID,{
			asynchronous: true,
			method: "post",
			parameters: "mode=ubah_password&password_lama="+password_lama+"&password_baru="+password_baru,
			onLoading: function(request){
				
			},
	    onComplete: function(request){
			},
			onSuccess: function(request){			
				
				if(request.responseText==1){
					alert("Password BERHASIL DIUBAH!");
					Element.hide('pop_up_ubah_password');
				}
				else{
					alert("Password lama yang anda masukkan tidak benar!");
				}
			},
			onFailure: function(request) 
			{
				
			}
		})
}

function showUbahPassword(){
	password_lama.value='';
	password_baru.value='';
	konfirmasi_password.value='';
	document.getElementById("pop_up_ubah_password").style.left=findPosX(document.getElementById("show_ubah_password"))-187;
	Element.show("pop_up_ubah_password");
	
}

function strClearence(str){
	string_output	= str.replace(/&/gi,"%26");
	return string_output;
}

var SID="";

function init(e) {
  //inisialisasi variabel
	
	SID 	= document.getElementById("hdn_SID").value;
	
	if(document.getElementById("top_menu_dipilih")){
		top_menu_dipilih = document.getElementById("top_menu_dipilih").value;
		if (document.getElementById(top_menu_dipilih)) {
			document.getElementById(top_menu_dipilih).setAttribute("class", "top_menu_on_select");
		}
	}
	
}

dojo.addOnLoad(init);