<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
  <meta http-equiv="Content-Style-Type" content="text/css">
	<link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <link rel="stylesheet" href="{TPL}trav.css" type="text/css" />
</head>  
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/lib/prototype.js"></script>
<script type='text/javascript' src='{ROOT}js/prototype.js'></script>
<script type='text/javascript' src='{ROOT}js/scriptaculous.js?load=effects'></script>
<script type='text/javascript' src='{ROOT}js/prototip.js'></script>
<link rel="stylesheet" type="text/css" href="{ROOT}css/prototip.css" />


<link rel="stylesheet" type="text/css" href="{ROOT}css/style.css" />
<link rel="stylesheet" type="text/css" href="{ROOT}css/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="{ROOT}css/bootstrap.css" />
<script type="text/javascript" src="{ROOT}js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="{ROOT}js/bootstrap.js"></script>
<script type="text/javascript">
  (function($){
    $(function(){
          jQuery.noConflict();
      });

  })(jQuery)
</script>
<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,600,700&amp;subset=latin,latin-ext" type="text/css" rel="stylesheet" /> -->
<style>
  body {
    font-family: 'Open Sans', mono, 'ProximaNova', 'Helvetica Neue', helvetica, arial, sans-serif;
    font-size: 14px;
  }
</style>

<body>
<div align='center'>

