<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<!-- HEADER -->
			<div class="col-md-4 bannerjudulw">
				<div class="bannerjudul">&nbsp;Laporan Setoran CSO</div>
			</div>
			<div class="col-md-8 searchwrapper">
				<form action="{ACTION_CARI}" method="post">
					<div class="col-md-2 line-height left">
						Status:<br /><select class="form-control" id="status" name="status"><option value="" {SEL_STATUS}>-semua-</option><option value="0" {SEL_STATUS0}>Belum Setor</option><option value="1" {SEL_STATUS1}>Sudah Setor</option></select>
					</div>
					<div class="col-md-2 line-height left">
						Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
					</div>
					<div class="col-md-2 line-height left">
						s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
					</div>
					<div class="col-md-4 line-height left">
						Cari:<br /><input class="form-control" type="text" id="cari" name="cari" value="{CARI}" placeholder="Nama CSO, No. Resi"/>
					</div>
					<div class="col-md-2 " style="padding: 0;">
						<input class="btn mybutton mysearch" type="submit" value="cari" />
					</div>
				</form>
			</div>
			<br>
			<!--<center><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></center>-->
			<br>
			<!-- HEADER END -->
			<table width="100%" class="table table-hover table-bordered" cellspacing="1" cellpadding="1">
				<tr>
					<th width="50" rowspan="2">No.</th>
					<th width="150" rowspan="2">CSO</th>
					<th width="150" rowspan="2">Waktu</th>
					<th width="200" rowspan="2">#Resi</th>
					<th class="thintop" colspan="3">Penumpang</th>
					<th class="thintop" colspan="2">Paket</th>
					<th width="150" rowspan="2">Biaya Op.</th>
					<th width="150" rowspan="2">Total Setor</th>
					<th width="200" rowspan="2">Act</th>
				</tr>
				<tr>
					<th class="thintop" width="100">Qty</th>
					<th class="thintop" width="100">Diskon</th>
					<th class="thintop" width="100">Setoran</th>
					<th class="thintop" width="100">Qty</th>
					<th class="thintop" width="100">Setoran</th>
			  </tr>
			  <!-- BEGIN ROW -->
			  <tr class="">
					<td align="center">{ROW.no}</td>
					<td align="center">{ROW.cso}</td>
					<td align="center">{ROW.waktu_setor}</td>
					<td align="center">{ROW.no_resi}</td>
					<td align="right">{ROW.jum_pnp}</div></td>
					<td align="right">{ROW.disc_pnp}</div></td>
					<td align="right">{ROW.omz_pnp}</div></td>
					<td align="right">{ROW.jum_pkt}</div></td>
					<td align="right">{ROW.omz_pkt}</div></td>
					<td align="right">{ROW.biaya}</div></td>
					<td align="right">{ROW.total_setor}</div></td>
					<td align="center">{ROW.act}</div></td>
			  </tr>
			  <!-- END ROW -->
			</table>
			<!-- BEGIN NO_DATA -->
			<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
			<!-- END NO_DATA -->
		</div>
	</div>
</div>