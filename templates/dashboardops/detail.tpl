<table class="table" cellpadding=0 cellspacing=0>
	<tr>
		<td width="50%" valign="top">
			<table class="table table-hover table-striped table-bordered" cellpadding=0 >
				<tr><th colspan='8' style="font-size: 16px">Ke {TUJUAN}</th></tr>
				<tr>
					<td class="dashboardkolomheader" width=100>Keterangan</td>
					<td class="dashboardkolomheader" width=50>P</td> 
					<td class="dashboardkolomheader" width=50>B1</td>
					<td class="dashboardkolomheader" width=50>B2</td>					
					<td class="dashboardkolomheader" width=50>C</td>
					<td class="dashboardkolomheader" width=130>#Body</td>
					<td class="dashboardkolomheader" width=200>CGS</td>
					<td class="dashboardkolomheader" width=70>Time</td>
				</tr>
				<!-- BEGIN ROW1 -->
				<tr>
					<td ><div align="center">{ROW1.keterangan}</div></td>
					<td ><div align="right">{ROW1.p}</div></td>
					<td ><div align="right">{ROW1.b}</div></td>
					<td ><div align="right">{ROW1.t}</div></td>
					<td ><div align="right">{ROW1.c}</div></td>
					<td ><div align="left">{ROW1.body}</div></td>
					<td ><div align="left">{ROW1.cgs}</div></td>
					<td ><div align="center">{ROW1.time}</div></td>
				</tr>
				<!-- END ROW1 -->
			</table>
		</td>
		<td width="50%" valign="top">
			<table class="table table-hover table-striped table-bordered" cellpadding=0 >
				<tr><th colspan='8' style="font-size: 16px">dari {ASAL}</th></tr>
				<tr>
					<td class="dashboardkolomheader" width=70>Time</td>
					<td class="dashboardkolomheader" width=200>CGS</td>
					<td class="dashboardkolomheader" width=130>#Body</td>
					<td class="dashboardkolomheader" width=50>P</td> 
					<td class="dashboardkolomheader" width=50>B1</td>
					<td class="dashboardkolomheader" width=50>B2</td>
					<td class="dashboardkolomheader" width=50>C</td>
					<td class="dashboardkolomheader" width=100>Keterangan</td>
				</tr>
				<!-- BEGIN ROW2 -->
				<tr class="{ROW2.odd}">
					<td ><div align="center">{ROW2.time}</div></td>
					<td ><div align="left">{ROW2.cgs}</div></td>
					<td ><div align="left">{ROW2.body}</div></td>
					<td ><div align="right">{ROW2.p}</div></td>
				 	<td ><div align="right">{ROW2.b}</div></td>
					<td ><div align="right">{ROW2.t}</div></td>
				 	<td ><div align="right">{ROW2.c}</div></td>
					<td ><div align="center">{ROW2.keterangan}</div></td>
				</tr>
				<!-- END ROW2 -->
			</table>
		</td>
	</tr>
</table>

