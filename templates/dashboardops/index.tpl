<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
 
	function setDashboardCabang(id_jurusan,rewrite_id,is_arrival){
		
		new Ajax.Updater(rewrite_id,"dashboardops_detail.php?sid={SID}", {
			asynchronous: true,
			method: "post",
		
			parameters: "idjurusan="+id_jurusan+"&tgl="+document.getElementById("tanggal_mulai").value+"&isarrival="+is_arrival,
			onLoading: function(request){
			},
			onSuccess: function(request){
				
			},
			onComplete: function(request){
				
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});		
	}
	
	
</script>
<div class="container" style="width: 90%;">
	<div class="row">
		<div class="col-md-12 box">
			<div class="col-md-3" style="text-align: left;">
				<div class="bannerjudulw"><div class="bannerjudul">Dashboard Operasional</div></div>
			</div>
			<form action="dashboardops.php" method="post">
			<div class="col-md-9" style="text-align: left;">
				<div class="col-md-4">
					Cabang:<br /><select class="form-control" id='cabang' name='cabang'>{OPT_CABANG}</select>
				</div>
				<div class="col-md-4">
					Tgl:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
				</div>
				<div class="col-md-4">
					<input class="mybutton btn topwidth" style="margin-top: 17px;" name="btn_cari" type="submit" value="cari" />
				</div>
			</div>
			</form>

			<table class="table" cellspacing="0" cellpadding="0" >
				<tr>
				 <td class="whiter" valign="middle" align="center">		
					<form action="{ACTION_CARI}" method="post">
						<!--<div id="loading" style="background: url('./templates/images/loading_bar.gif') no-repeat;height: 19px">&nbsp;</div>-->
						{LIST_DASHBOARD}
					</form>
				 </td>
				</tr>
			</table>
		</div>
	</div>
</div>
