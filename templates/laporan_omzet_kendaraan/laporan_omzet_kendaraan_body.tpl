<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudulw">
							<div class="bannerjudul">
								Laporan Omzet Kendaraan
							</div></td>
							<td align='left' valign='middle'>
								<div class="col-md-3">
									Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
								</div>
								<div class="col-md-3">
									
										 s/d<br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
								</div>
								<div class="col-md-3">
									
										Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
										
								</div>
								<div class="col-md-3">
									<input class="btn mybutton topwidth" type="submit" value="cari" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan=2 align='center' valign='middle' class="pad10">
								<table>
									<tr>
										<td>
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table width='100%'>
									<tr>
										<td><font style="font-size:16px;">Jumlah Kendaraan Digunakan: <span style="color:green;">{JUMLAH_KENDARAAN_DIPAKAI}</span> Kendaraan</font></td>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="table table-hover table-bordered table-responsive" style="margin-top: 20px;" width='100%' >
			    <tr>
			       <th width=30>No</th>
						 <th width=200><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>No.Polisi</a></th>
						 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Layout Kursi</a></th>
						 <th width=300><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Cabang</a></th>
						 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Total Jalan</a></th>
						 <th width=200><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Total Penumpang</a></th>
						 <th width=200><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Total Omzet</a></th>
						 <th width=200><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Total Biaya Langsung</a></th>
						 <th width=200><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Total Laba Kotor</a></th>
						 <th width=100>Action</th>
			     </tr>
			     <!-- BEGIN ROW -->
			     <tr class="{ROW.odd}">
			       <td ><div align="right">{ROW.no}</div></td>
			       <td ><div align="left">{ROW.no_polisi}</div></td>
						 <td ><div align="right">{ROW.layout_kursi}</div></td>
			       <td ><div align="left">{ROW.cabang}</div></td>
			       <td ><div align="right">{ROW.total_jalan}</div></td>
						 <td ><div align="right">{ROW.total_penumpang}</div></td>
						 <td ><div align="right">{ROW.total_omzet}</div></td>
						 <td ><div align="right">{ROW.total_biaya}</div></td>
						 <td ><div align="right">{ROW.total_profit}</div></td>
			       <td ><div align="center">{ROW.act}</div></td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					<table width='100%' >
						<tr>
							<td align='right' width='100%' colspan=3>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>