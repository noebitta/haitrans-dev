<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	var idbaglobal;
	var idpenjadwalanglobal;
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","daftar_manifest.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
	}
	
	function setDataBA(idpenjadwalan,idba,sopirdipilih,nominalinsentif,keterangan){
		
		idbaglobal=idba;
		idpenjadwalanglobal=idpenjadwalan;
		
		document.getElementById("nominalinsentif").value=nominalinsentif;
		document.getElementById("keterangan").value=keterangan;
		
		objsopir	= document.getElementById("sopir");
		
		if(idbaglobal!=""){
			
			
			jumlahsopir=objsopir.length;
			
			sopirs=document.getElementById("sopir").options;
			
			i=0;
			ketemu=false;
			
			while(i<jumlahsopir && !ketemu){
				if(sopirs[i].value!=sopirdipilih){
					i++;
				}
				else{
					ketemu=true;
				}
			}
		}
		else{
			i=0;
			document.getElementById("nominalinsentif").value="";
			document.getElementById("keterangan").value="";
		}
		
		objsopir.selectedIndex=i;
		
		dlg_ba.show();
		
	}
	
	function simpanBA(){
		
		isvalid = true;
		
		if(document.getElementById("sopir").value==""){
			document.getElementById("sopir").style.background="red";
			isvalid=false;
		}
		
		if(document.getElementById("nominalinsentif").value==""){
			document.getElementById("nominalinsentif").style.background="red";
			isvalid=false;
		}
		
		if(document.getElementById("keterangan").value==""){
			document.getElementById("keterangan").style.background="red";
			isvalid=false;
		}
		
		if(!isvalid) exit;
		
		new Ajax.Request("beritaacara.insentifsopir.php?sid={SID}",{
    asynchronous: true,
    method: "post",
    parameters:
			"mode=simpan"+
			"&idpenjadwalan="+idpenjadwalanglobal+
			"&idba="+idbaglobal+
			"&sopir="+document.getElementById("sopir").value+
			"&nominalinsentif="+document.getElementById("nominalinsentif").value+
			"&keterangan="+document.getElementById("keterangan").value,
    onLoading: function(request){
			popup_loading.show();
			dlg_ba.hide();
    },
    onComplete: function(request){popup_loading.hide();},
    onSuccess: function(request) {
			if(request.responseText==0){
				alert("Data BERHASIL disimpan!");
				window.location.reload();
			}
			else{
				alert("Terjadi kegagalan!");
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
	}
	
	function init(e){
		
		//control dialog daftar sopir
		dlg_ba				= dojo.widget.byId("dlgberitaacara");
		dlg_ba_cancel	= document.getElementById("dlgberitaacaracancel");
		dlg_ba.setCloseControl(dlg_ba_cancel);
		
		popup_loading	= dojo.widget.byId("popuploading");
		
		getUpdateAsal("{KOTA}");
		getUpdateTujuan("{ASAL}");
		setSortId();
	}
	
	dojo.addOnLoad(init);
</script>

<!--BEGIN dialog buat BA-->
<div dojoType="dialog" id="dlgberitaacara" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table width="400" cellpadding="0" cellspacing="0" >
	<tr><td align="center" valign="middle" style="color: white;font-size: 16px;margin-top: 0px;">Berita Acara Insentif Sopir</td></tr>
	<tr>
		<td style="background-color: white;">
			<table width="100%">
				<tr><td>Sopir</td><td width="1">:</td><td><select id="sopir" name="sopir" onfocus="this.style.background='white'">{OPT_SOPIR}</select></td></tr>
				<tr><td>Nominal Insentif</td><td width="1">:</td><td><input type="text" id='nominalinsentif' name="nominalinsentif" value="{NOMINAL_INSENTIF}" maxlength=12 onkeypress='validasiAngka(event);' placeholder="rupiah" onfocus="this.style.background='white'"></td></tr>
				<tr><td>Keterangan</td><td width="1">:</td><td><textarea id='keterangan' name="keterangan" onfocus="this.style.background='white'" cols="40" rows="3">{KETERANGAN}</textarea></td></tr>
				<tr><td colspan="3" align="center"><br><input type="button" id="dlgberitaacarasimpan" value="Simpan" onclick="simpanBA();">&nbsp;<input type="button" id="dlgberitaacaracancel" value="Batal"></td></tr>
			</table>
		</td>
	</tr>
</table>
<br>
</div>
<!--END dialog buat BA-->

<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<form action="{ACTION_CARI}" method="post">

			<div class="row left">
				<div class="col-md-12">
					<div class="bannerjudulw"><div class="bannerjudul">BA Insentif Sopir</div></div>
				</div>
			</div>

			<div class="row left">
				<div class="col-md-4">
					Kota<br/><select class="form-control" onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
				</div>
				<div class="col-md-4">
					Asal<br /><div id='rewrite_asal'></div>
				</div>
				<div class="col-md-4">
					Tujuan:<br /><div id='rewrite_tujuan'></div>
				</div>
			</div>
			<div class="row left" style="margin-top: 20px;">
				<div class="col-md-3">
					Tgl:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
				</div>
				<div class="col-md-3">
					s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
				</div>
				<div class="col-md-3">
					Cari<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
				</div>
				<div class="col-md-3">
					<input class="btn mybutton topwidth" name="btn_cari" type="submit" value="cari" />
				</div>
			</div>

			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle'>
							<td align='right' valign='middle'>
								<table>
									<tr>
										<td class=''></td>
										<td class=''></td>
										<td class=''>&nbsp;</td>
										<td class='' colspan=2>&nbsp;</td>
										<td class='' colspan=2>&nbsp; </td>
										<td class=''></td>	
										<td class=''></td>								
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
							<td>
								<table width='100%'>
									<tr>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table width='100%' style="margin-top: 20px;" class="table table-bordered table-hover">
						<!-- BEGIN ROW -->
						<tr style="display:{ROW.showheader};">
							<th class="thin" colspan="5">1:Jadwal</th>
							<th class="thin" colspan="7">2:Berita Acara</th>
							<th class="thin" colspan="3">3:Release</th>
						</tr>
						<tr style="display:{ROW.showheader};">
							<th width=30>No</th>
							<th width=70><a class="th" id="sort1" href='#'>Tgl.Berangkat</a></th>
							<th width=100><a class="th" id="sort2" href='#'>#Jadwal</a></th>
							<th width=70><a class="th" id="sort3" href='#'>Jam</a></th>
							<th width=100><a class="th" id="sort4" href='#'>Remark</a></th>
							<th width=100><a class="th" id="sort5" href='#'>BA Oleh</a></th>
							<th width=100><a class="th" id="sort6" href='#'>Dibuat</a></th>
							<th width=100><a class="th" id="sort7" href='#'>Sopir</a></th>
							<th width=70><a class="th" id="sort8" href='#'>Jumlah</a></th>
							<th width=100><a class="th" id="sort9" href='#'>Approver</a></th>
							<th width=100><a class="th" id="sort10" href='#'>Approved</a></th>
							<th width=100><a class="th" id="sort11" href='#'>Keterangan</a></th>
							<th width=100><a class="th" id="sort12" href='#'>Releaser</a></th>
							<th width=100><a class="th" id="sort13" href='#'>Released</a></th>
							<th width=100><a class="th" href='#'>Act.</a></th>
						</tr>
						<tr class="{ROW.odd}">
							<td align="center">{ROW.no}</td>
							<td align="center">{ROW.tglberangkat}</td>
							<td align="center">{ROW.kodejadwal}</td>
							<td align="center">{ROW.jamberangkat}</td>
							<td align="center">{ROW.remark}</td>
							<td align="center">{ROW.dibuatoleh}</td>
							<td align="center">{ROW.waktubuat}</td>
							<td align="center">{ROW.sopir}</td>
							<td align="right">{ROW.jumlah}</td>
							<td align="center">{ROW.approver}</td>
							<td align="center">{ROW.waktuapproved}</td>
							<td align="left">{ROW.keterangan}</td>
							<td align="center">{ROW.releaser}</td>
							<td align="center">{ROW.waktureleased}</td>
							<td align="center">{ROW.act}</td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					{NO_DATA}
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
