<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","daftar_manifest.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr><td colspan="2" class="bannerjudulw"><div class="bannerjudul" style="padding-left: 12px;">Daftar Manifest</div></td></tr>
						<tr class='' height=40>
							<td colspan="2" align='left' valign='middle'>
								<div class="row">
									<div class="col-md-4">
										Kota:<br /><select class="form-control" onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
									</div>
									<div class="col-md-4">
										Asal:&nbsp; <div id='rewrite_asal'></div>
									</div>
									<div class="col-md-4">
										Tujuan:&nbsp; <div id='rewrite_tujuan'></div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										Tgl:&nbsp; <input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
									</div>
									<div class="col-md-4">
										s/d &nbsp; <input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
									</div>
									<div class="col-md-4">
										<div class="input-group" style="width: 100%; margin: 0; margin-top: 12px;">
									      <input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
									      <span class="input-group-btn"  style="padding: 0; margin: 0;">
									        <input class="form-control tombol" name="btn_cari" type="submit" value="cari" />
									      </span>
									    </div><!-- /input-group -->
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="pad10" align='center' valign='middle'>
								<table>
									<tr>
										<td class="pad10">

										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
							<td>
								<table width='100%'>
									<tr>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table  class="table table-hover table-bordered table-responsive" style="margin-top: 20px;" width='100%' >
			    <tr>
			       <th width=30>No</th>
						 <th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jadwal</a></th>
						 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Jadwal</a></th>
						 <th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Berangkat</a></th>
						 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Delay</a></th>
						 <th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>#Manifest</a></th>
						 <th width=200><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Sopir</a></th>
						 <th width=70><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>#Body</a></th>
						 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Pnp</a></th>
						 <th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Paket</a></th>
						 <th width=100><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>Check</a></th>
						 <th width=100><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>Checker</a></th>
						 <th width=100><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>Pnp.Chk</a></th>
						 <th width=100><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>Paket.Chk</a></th>
			     </tr>
			     <!-- BEGIN ROW -->
			     <tr class="{ROW.odd}">
			       <td align="right">{ROW.no}</td>
			       <td align="center">{ROW.jadwal}</td>
						 <td align="center">{ROW.kodejadwal}</td>
			       <td align="center">{ROW.berangkat}</td>
			       <td align="center" class='{ROW.flagterlambat}'>{ROW.keterlambatan}</td>
						 <td align="center">{ROW.manifest}</td>
						 <td align="center">{ROW.sopir}</td>
						 <td align="center">{ROW.mobil}</td>
						 <td align="right">{ROW.penumpang}</td>
						 <td align="right">{ROW.paket}</td>
						 <td align="center">{ROW.waktucheck}</td>
						 <td align="center">{ROW.checker}</td>
						 <td align="right" style="background: {ROW.bgpnpchk};">{ROW.penumpangcheck}</td>
						 <td align="right" style="background: {ROW.bgpktchk};">{ROW.paketcheck}</td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
