<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <table>
    <tr>
    <td align='center'>
       <h2>{JUDUL}</h2>
    </td>
    <tr>
      <td>
        <table class="border">
          <tr>
						<th>No</th>
						<th>TGL Brgkt</th>
            <th>Jam Brgkt</th>
						<th>KodeRute</th>
						<th>Asal</th>
						<th>Tujuan</th>
						<th>No.Kursi</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>Telp</th>
						<th>HP</th>
						<th>Jenis</th>
						<th>TGL Pesan</th>
						<th>Jam Pesan</th>
						<th>CSO pesan</th>
						<th>TGL Batal</th>
						<th>Jam Batal</th>
						<th>CSO Batal</th>
          </tr>
          <!-- BEGIN ROW -->
          <tr bgcolor='D0D0D0'>
						<td align="right" >{ROW.num}</td>
            <td align="center">{ROW.tglbrgkt}</td>
            <td align="center">{ROW.jambrgkt}</td>
						<td align="center">{ROW.koderute}</td>
						<td align="center">{ROW.asal}</td>
						<td align="center">{ROW.tujuan}</td>
						<td align="center">{ROW.nokursi}</td>
						<td align="center">{ROW.nama}</td>
						<td align="center">{ROW.alamat}</td>
						<td align="center">{ROW.telp}</td>
						<td align="center">{ROW.hp}</td>
						<td align="center">{ROW.jenis}</td>
						<td align="center">{ROW.tglpesan}</td>
						<td align="center">{ROW.jampesan}</td>
						<td align="center">{ROW.cso}</td>
						<td align="center">{ROW.tglbatal}</td>
						<td align="center">{ROW.jambatal}</td>
						<td align="center">{ROW.csobatal}</td>
          </tr>
          <!-- END ROW -->
        </table>
      </td>
    </tr>
    </table>	
 </td>
</tr>
</table>