<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	var idbaglobal;
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function showDialogBA(){
		document.getElementById('nospj').value="";
		idbaglobal="";
		
		new Ajax.Request("beritaacara.bop.php?sid={SID}", 
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=showdialog",
			onLoading: function(request) 
			{
			},
			onComplete: function(request) 
			{
			},
			onSuccess: function(request) 
			{
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
			},
			onFailure: function(request) 
			{
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		})
	}
	
	function getDataManifest(){
	
		nospj				= document.getElementById('nospj');
		tglberangkat= document.getElementById('tglberangkat');
		jamberangkat= document.getElementById('jamberangkat');
		kodejadwal	= document.getElementById('kodejadwal');
		kodebody		= document.getElementById('kodebody');
		sopir				= document.getElementById('sopir');
		jumlah			= document.getElementById('jumlah');
		keterangan	= document.getElementById('keterangan');
		
		nospj.style.background="white";
		jumlah.style.background="white";
		keterangan.style.background="white";
		
		valid=true;
		
		if(nospj.value==''){valid=false;nospj.style.background="red";}
		
		if(!valid){
			exit;
		}
		
		new Ajax.Request("beritaacara.bop.php?sid={SID}", 
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=getdatamanifest&nospj="+nospj.value,
			onLoading: function(request) 
			{
				popup_loading.show();
			},
			onComplete: function(request) 
			{
				popup_loading.hide();
			},
			onSuccess: function(request) 
			{
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
				
			},
			onFailure: function(request) 
			{
				popup_loading.hide();
				alert('Error !!! Cannot Save');        
				assignError(request.responseText);
			}
		});
		
	}
	
	function proses(){
	
		nospj				= document.getElementById('nospj');
		kodeba			= document.getElementById('kodeba');
		jenisbiaya	= document.getElementById('jenisbiaya');
		jumlah			= document.getElementById('jumlah');
		keterangan	= document.getElementById('keterangan');
		
		valid=true;
		
		if(nospj.value==''){valid=false;nospj.style.background="red";}
		if(jumlah.value=='' || jumlah.value*1<=0){valid=false;jumlah.style.background="red";}
		if(keterangan.value==''){valid=false;keterangan.style.background="red";}
		
		if(!valid){
			exit;
		}
		
		new Ajax.Request("beritaacara.bop.php?sid={SID}", 
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=simpan"+
				"&kodeba="+kodeba.value+
				"&nospj="+nospj.value+
				"&jenisbiaya="+jenisbiaya.value+
				"&jumlah="+jumlah.value+
				"&keterangan="+keterangan.value+
				"&idba="+idbaglobal,
			onLoading: function(request){
				popup_loading.show();
				dlg_ba.hide();
			},
			onComplete: function(request){
				
			},
			onSuccess: function(request) 
			{
				
				popup_loading.hide();
				
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
				
			},
			onFailure: function(request) 
			{
				popup_loading.hide();
				alert('Error !!! Cannot Save');        
				assignError(request.responseText);
			}
		})
	}
	
	function hapus(idba){
		
		
		if(!confirm("Apakah anda yakin akan menghapus data ini?")){
			exit;
		}
		
		new Ajax.Request("beritaacara.bop.php?sid={SID}", 
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=hapus"+
				"&idba="+idba,
			onLoading: function(request){
				popup_loading.show();
			},
			onComplete: function(request){
				
			},
			onSuccess: function(request) 
			{
				
				popup_loading.hide();
				
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
				
			},
			onFailure: function(request) 
			{
				popup_loading.hide();
				alert('Error !!! Cannot Save');        
				assignError(request.responseText);
			}
		})
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","daftar_manifest.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
	}
	
	function setDataBA(idba){
		
		idbaglobal=idba;
		
		nospj				= document.getElementById('nospj');
		tglberangkat= document.getElementById('tglberangkat');
		kodejadwal	= document.getElementById('kodejadwal');
		kodebody		= document.getElementById('kodebody');
		sopir				= document.getElementById('sopir');
		jumlah			= document.getElementById('jumlah');
		keterangan	= document.getElementById('keterangan');
		
		nospj.style.background="white";
		jumlah.style.background="white";
		keterangan.style.background="white";
		
		valid=true;
		
		new Ajax.Request("beritaacara.bop.php?sid={SID}", 
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=getdataba&id="+idbaglobal,
			onLoading: function(request) 
			{
				popup_loading.show();
			},
			onComplete: function(request) 
			{
				popup_loading.hide();
			},
			onSuccess: function(request) 
			{
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
				
			},
			onFailure: function(request) 
			{
				popup_loading.hide();
				alert('Error !!! Cannot Save');        
				assignError(request.responseText);
			}
		});
		
	}
	
	function setJenisBiaya(jenisbiaya){
		
		objjenisbiaya = document.getElementById("jenisbiaya");
		
		jumjenisbiaya=objjenisbiaya.length;
		jenisbiayas=objjenisbiaya.options;
			
		i=0;
		ketemu=false;
			
		while(i<jumjenisbiaya && !ketemu){
			if(jenisbiayas[i].value!=jenisbiaya){
				i++;
			}
			else{
				ketemu=true;
			}
		}
		
		objjenisbiaya.selectedIndex=i;
		
	}
	
	function init(e){
		
		//control dialog daftar sopir
		dlg_ba				= dojo.widget.byId("dlgberitaacara");
		
		popup_loading	= dojo.widget.byId("popuploading");
		
		getUpdateAsal("{KOTA}");
		getUpdateTujuan("{ASAL}");
		setSortId();
		
	}
	
	dojo.addOnLoad(init);
</script>

<!--BEGIN dialog buat BA-->
<div dojoType="dialog" id="dlgberitaacara" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
	<div class="dlgwrapper">
		<h3 style="padding-left: 10px;">BERITA ACARA BIAYA OPERASIONAL</h3>
		<div class="dlgcontent">
			<input type="hidden" id="kodeba">
			<span class="dlglabel"><b>Kode BA</b></span><span class="dlgfield"><b>: <span id="showkodeba"></span></b></span></br>
			<span class="dlglabel">#Manifest</span><span class="dlgfield">: <input type="text" id="nospj" name="nospj" maxlength="30" onfocus="this.style.background='white';" style="text-transform: uppercase;" /><span id="shownospj" style="display: none;text-transform: uppercase;"></span></span></br>
			<br>
			<span id="showtombol1" style="text-align: center;display: block;">
				<input type="button" value="BATAL" onclick="dlg_ba.hide();"  style="width: 100px;"/>&nbsp;
				<input type="button" id="btnpreproses" value="PROSES" onclick="getDataManifest();" style="width: 100px;" />
			</span><br/>
			
			<span id="detailmanifest" style="display: none;">
				<b>DATA MANIFEST</b><br>
				<span class="dlglabel">Tgl.Berangkat</span><span class="dlgfield">: <span id="tglberangkat"></span></span></br>
				<span class="dlglabel">#Jadwal</span><span class="dlgfield">: <span id="kodejadwal"></span></span></br>
				<span class="dlglabel">Jam</span><span class="dlgfield">: <span id="jamberangkat"></span></span></br>
				<span class="dlglabel">#Body</span><span class="dlgfield">: <span id="kodebody"></span></span></br>
				<span class="dlglabel">Sopir</span><span class="dlgfield">: <span id="sopir"></span></span></br>
				<span class="dlglabel">Jenis Biaya</span><span class="dlgfield">: <select id="jenisbiaya"><option value="{JENIS_BIAYA_TOL}">Tambahan TOL</option><option value="{JENIS_BIAYA_BBM}">Tambahan BBM</option><option value="{JENIS_BIAYA_LAINNYA}">Tambahan Lainnya</option></select></span></br>
				<span class="dlglabel">Jumlah (Rp.)</span><span class="dlgfield">: <input id="jumlah" type="text" size="20" maxlength="7" onfocus="this.style.background='white';" onkeypress="validasiAngka(event);" style="text-align: right;"/></span></br>
				<span class="dlglabel">Keterangan</span><span class="dlgfield">: <textarea id='keterangan' name="keterangan" onfocus="this.style.background='white';" cols="40" rows="3"></textarea></span></br>
				<br>
				<br><br>
				<span style="text-align: center;display: inline-block; width: 400px;">
					<input type="button" onclick="dlg_ba.hide();" value="BATAL" style="width: 100px;">
					&nbsp;&nbsp;&nbsp;<input type="button" onClick="proses();" value="PROSES" style="width: 100px;"><br><br>
				</span>
			</span>
		</div>
	</div>
<br>
</div>
<!--END dialog buat BA-->

<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<form action="{ACTION_CARI}" method="post">
			<div class="row"><div class="col-md-12"><div class="bannerjudulw"><div class="bannerjudul">BA Biaya OP</div></div></div></div>

			<div class="row left">
				<div class="col-md-4">
					Kota<br />
					<select class="form-control" onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
				</div>
				<div class="col-md-4">
					Asal<br /><div id='rewrite_asal'></div>
				</div>
				<div class="col-md-4">
					Tujuan<br /><div id='rewrite_tujuan'></div>
				</div>
			</div>
			<div class="row left" style="padding-top: 10px;">
				<div class="col-md-3">
					Tgl<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
				</div>
				<div class="col-md-3">
					s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
				</div>
				<div class="col-md-3">
					Cari<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
				</div>
				<div class="col-md-3">
					<input class="btn mybutton topwidth" name="btn_cari" type="submit" value="cari" />
				</div>
			</div>

			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		

					<!--HEADER-->
					<table width='100%' cellspacing="0" style="margin: 20px 0px;">
						<tr>
							<td>
								<a href="#" onClick="showDialogBA();return false;"> + Buat Berita Acara</a>&nbsp;&nbsp;&nbsp;
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
							<td>
								<div style="float: right;">{PAGING}</div>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="table table-hover table-bordered">
						<tr style="display:{ROW.showheader};">
							<th class="thin" colspan="7">1:Jadwal</th>
							<th class="thin" colspan="5">2:Berita Acara</th>
							<th class="thin" colspan="3">3:Release</th>
						</tr>
						<tr style="display:{ROW.showheader};">
							<th width=30>No</th>
							<th width=100><a class="th" id="sort1" href='#'>Berangkat</a></th>
							<th width=70> <a class="th" id="sort2" href='#'>#Body</a></th>
							<th width=100><a class="th" id="sort3" href='#'>Sopir</a></th>
							<th width=100><a class="th" id="sort4" href='#'>#Jadwal</a></th>
							<th width=70> <a class="th" id="sort5" href='#'>Jam</a></th>
							<th width=100><a class="th" id="sort6" href='#'>#Manifest</a></th>
							<th width=100><a class="th" id="sort7" href='#'>Waktu</a></th>
							<th width=100><a class="th" id="sort8" href='#'>Oleh</a></th>
							<th width=100><a class="th" id="sort9" href='#'>Jenis Biaya</a></th>
							<th width=100><a class="th" id="sort10" href='#'>Jumlah</a></th>
							<th width=100><a class="th" id="sort11" href='#'>Keterangan</a></th>
							<th width=100><a class="th" id="sort12" href='#'>Releaser</a></th>
							<th width=100><a class="th" id="sort13" href='#'>Released</a></th>
							<th width=100><a class="th" href='#'>Act.</a></th>
						</tr>
						<!-- BEGIN ROW -->
						<tr class="{ROW.odd}">
							<td align="center">{ROW.no}</td>
							<td align="center">{ROW.tglberangkat}</td>
							<td align="center">{ROW.kodebody}</td>
							<td align="center">{ROW.namasopir}</td>
							<td align="center">{ROW.kodejadwal}</td>
							<td align="center">{ROW.jamberangkat}</td>
							<td align="center">{ROW.nospj}</td>
							<td align="center">{ROW.waktubuat}</td>
							<td align="center">{ROW.dibuatoleh}</td>
							<td align="center">{ROW.jenisbiaya}</td>
							<td align="right">{ROW.jumlah}</td>
							<td align="center">{ROW.keterangan}</td>
							<td align="center">{ROW.releaser}</td>
							<td align="center">{ROW.waktureleased}</td>
							<td align="center">{ROW.act}</td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					{NO_DATA}
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
