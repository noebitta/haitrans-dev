<input type="hidden" value="{HARGA_MINIMUM_PAKET}" id="hdn_harga_minimum_paket">
<input type="hidden" value="{SID}" id="hdn_SID">

<input type="hidden" value=0 id="ismodemutasion">
<input type="hidden" value=0 id="notiketdicetak">	
<input type="hidden" value='' id="id_jurusan_aktif">

<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi_paket.js"></script>

<!--dialog Paket-->
<div dojoType="dialog" id="dialog_cari_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='900'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Paket</h2></td></tr>
			<tr><td><div id="rewrite_cari_paket"></div></td></tr>
		</table>
		<span id='progress_cari_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_paket_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog Paket-->

<!--dialog ambil paket-->
<div dojoType="dialog" id="dialog_ambil_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<div id="rewrite_ambil_paket"></div>
		<span id='progress_ambil_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_ambil_paket.hide();" id="dlg_ambil_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="prosesAmbilPaket();" id="dlg_ambil_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Ambil&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog ambil paket-->

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td style="background: white;">
		<input type="button" style="border-radius: 0;" class="button-close" id="dialog_pembayaran_btn_Cancel" value="&nbsp;X&nbsp;">
		<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Pilih Jenis Pembayaran</h4>
		<table width='100%'>
			<tr>
				<td>
					<br>
					<input type='hidden' id='kode_booking_go_show' value='' />
					<div style="padding: 20px 60px;">
						<table width='100%'>
							<tr>
								<td width='50%' align='center' valign='middle'>
									<a href="" onClick="CetakResi(0);return false;">
										<svg class="svgdialog" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
											<g id="Keseluruhan">
										<path d="M559.219,286.543l109.807-69.144c3.354-2.111,5.383-5.884,5.293-9.847c-0.09-3.962-2.285-7.64-5.73-9.597L428.383,61.418
											c-3.564-2.028-8.169-1.922-11.642,0.262L30.974,304.599c-3.354,2.111-5.382,5.884-5.292,9.846c0.089,3.962,2.285,7.639,5.73,9.598
											L271.619,460.58c3.563,2.028,8.168,1.925,11.638-0.262l109.807-69.146c2.563-1.612,4.343-4.126,5.014-7.078s0.152-5.988-1.461-8.55
											c-2.088-3.318-5.678-5.298-9.604-5.298c-2.129,0-4.213,0.604-6.024,1.745l-104.068,65.53L59.072,313.692l132.548-83.466
											l228.573,129.926v171.408c0,4.126,2.244,7.929,5.856,9.923c3.544,1.959,8.09,1.827,11.516-0.331l77.155-48.586
											c3.315-2.087,5.295-5.672,5.295-9.59v-177.89c-0.008-0.088-0.013-0.171-0.016-0.255c-0.008-0.437-0.034-0.817-0.077-1.183
											l-0.022-0.207c-0.076-0.552-0.16-0.979-0.262-1.361l-0.024-0.088c-0.105-0.386-0.246-0.793-0.431-1.246
											c-0.039-0.099-0.074-0.174-0.108-0.251l-0.043-0.097c-0.135-0.305-0.282-0.597-0.44-0.885l-0.057-0.104
											c-0.049-0.088-0.096-0.177-0.148-0.263c-0.064-0.109-0.107-0.188-0.149-0.265c-0.156-0.243-0.332-0.479-0.508-0.713
											c-0.044-0.058-0.094-0.128-0.143-0.202c-0.249-0.316-0.531-0.633-0.859-0.964l-0.161-0.149c-0.305-0.293-0.598-0.548-0.902-0.788
											l-0.194-0.151c-0.487-0.361-0.897-0.631-1.286-0.847L290.535,167.942l132.546-83.464L640.93,208.305l-93.787,59.058
											c-2.563,1.613-4.343,4.126-5.014,7.078c-0.671,2.952-0.152,5.988,1.462,8.55c2.088,3.317,5.677,5.297,9.603,5.297
											C555.322,288.288,557.406,287.685,559.219,286.543z M486.62,305.472l-23.416,14.746c-2.563,1.613-4.343,4.126-5.014,7.079
											c-0.671,2.952-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297c2.129,0,4.211-0.604,6.023-1.745l22.066-13.893v151.217
											l-54.487,34.31V353.56c0-4.067-2.196-7.843-5.733-9.852L213.38,216.526l55.394-34.882L486.62,305.472z"/>
										<path d="M559.221,345.876l109.807-69.144c2.563-1.613,4.343-4.126,5.014-7.079s0.152-5.988-1.462-8.55
											c-2.088-3.318-5.678-5.298-9.604-5.298c-2.129,0-4.212,0.604-6.024,1.746l-109.807,69.144c-2.563,1.613-4.343,4.126-5.014,7.079
											s-0.152,5.989,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.326,347.621,557.408,347.018,559.221,345.876z"/>
										<path d="M398.075,443.429c0.671-2.952,0.152-5.988-1.461-8.55c-2.087-3.318-5.678-5.299-9.604-5.299
											c-2.128,0-4.212,0.604-6.024,1.745l-104.067,65.53L42.612,363.672c-1.707-0.971-3.639-1.483-5.587-1.483
											c-4.076,0-7.856,2.197-9.865,5.734c-1.497,2.633-1.878,5.69-1.075,8.608c0.803,2.919,2.694,5.35,5.326,6.846l240.207,136.537
											c3.564,2.027,8.166,1.924,11.638-0.262l109.806-69.145C395.624,448.896,397.404,446.381,398.075,443.429z"/>
										<path d="M559.221,405.211l109.807-69.145c2.563-1.613,4.343-4.126,5.014-7.078c0.671-2.952,0.152-5.989-1.462-8.55
											c-2.087-3.318-5.677-5.298-9.603-5.298c-2.129,0-4.212,0.604-6.025,1.746l-109.807,69.144c-2.563,1.612-4.343,4.127-5.014,7.079
											s-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.326,406.955,557.408,406.352,559.221,405.211z"/>
										<path d="M398.075,502.764c0.671-2.952,0.152-5.988-1.461-8.55c-2.087-3.319-5.677-5.3-9.603-5.3c-2.129,0-4.213,0.604-6.026,1.745
											l-104.067,65.53L42.612,423.007c-1.707-0.971-3.639-1.483-5.587-1.483c-4.076,0-7.856,2.197-9.865,5.734
											c-1.497,2.633-1.878,5.689-1.075,8.607c0.803,2.919,2.695,5.351,5.327,6.847l240.206,136.537c3.563,2.027,8.167,1.924,11.638-0.262
											l109.806-69.146C395.624,508.229,397.404,505.715,398.075,502.764z"/>
										<path d="M559.222,464.545L669.027,395.4c2.563-1.612,4.343-4.126,5.014-7.078s0.152-5.988-1.462-8.55
											c-2.087-3.317-5.677-5.298-9.603-5.298c-2.129,0-4.213,0.604-6.025,1.746l-109.807,69.144c-2.563,1.612-4.343,4.127-5.014,7.078
											c-0.671,2.952-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.325,466.289,557.409,465.686,559.222,464.545z"/>
										<path d="M398.075,562.098c0.671-2.952,0.152-5.988-1.461-8.55c-2.088-3.319-5.678-5.3-9.604-5.3c-2.128,0-4.212,0.604-6.024,1.745
											l-104.067,65.53L42.612,482.341c-1.707-0.971-3.639-1.483-5.587-1.483c-4.076,0-7.856,2.197-9.865,5.734
											c-1.497,2.633-1.878,5.689-1.075,8.608c0.803,2.919,2.694,5.35,5.326,6.846l240.207,136.537c3.563,2.028,8.167,1.924,11.638-0.262
											l109.807-69.146C395.625,567.563,397.405,565.05,398.075,562.098z"/>
									</g>
										</svg>
									</a>
									<br />
									<a href="" onClick="CetakResi(0);return false;"><span class="genmed">Tunai</span></a>
								</td>
								<td width='50%' align='center' valign='middle'>
									<a href="" onClick="CetakResi(1);return false;">
										<svg class="svgdialog" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
											<g id="debitcard">
											<g>
												<path d="M694.925,442.914l-39.698-327.9c-2.672-22.071-22.674-37.753-44.745-35.081L44.433,148.464
														c-22.071,2.672-37.753,22.674-35.081,44.745l39.874,329.354c2.672,22.071,22.674,37.753,44.745,35.081l567.37-68.691
														C683.413,486.281,699.095,466.279,694.925,442.914z M668.933,447.536c1.008,8.326-5.208,14.577-12.081,15.409L90.804,531.476
														c-8.326,1.008-14.577-5.208-15.409-12.081L60.83,399.093l593.538-71.859L668.933,447.536z M651.04,299.745L58.956,371.428
														l-4.832-39.913l592.085-71.683L651.04,299.745z M643.04,233.664L49.502,305.523L35.696,191.495
														c-1.008-8.326,5.208-14.577,12.081-15.409l-0.16-1.322l566.049-68.531c8.326-1.008,14.577,5.208,15.409,12.081L643.04,233.664z"/>
												<path d="M434.031,181.945l-15.005,1.817l5.786,47.794l13.534-1.639c8.456-1.024,14.67-3.899,18.642-8.626
														c3.972-4.727,5.482-11.024,4.529-18.891c-0.894-7.388-3.749-12.88-8.562-16.477C448.141,182.327,441.833,181.001,434.031,181.945z
														 M438.281,221.433l-4.348,0.526l-3.768-31.121l5.394-0.653c9.066-1.098,14.221,3.486,15.463,13.751
														C452.292,214.419,448.044,220.251,438.281,221.433z"/>
												<polygon points="474.371,225.555 501.896,222.223 500.883,213.854 483.492,215.96 482,203.635 498.182,201.676 497.176,193.373
														480.995,195.332 479.724,184.838 497.115,182.733 496.11,174.43 468.585,177.762 		"/>
												<path d="M535.707,192.722l-0.04-0.327c2.178-0.64,3.826-1.956,4.945-3.949c1.119-1.993,1.505-4.417,1.16-7.272
														c-0.509-4.206-2.412-7.088-5.707-8.646c-3.296-1.558-8.332-1.927-15.11-1.106l-14.874,1.801l5.786,47.794l17.882-2.165
														c5.405-0.654,9.534-2.393,12.388-5.215c2.854-2.822,4.017-6.413,3.489-10.771c-0.372-3.073-1.34-5.454-2.905-7.145
														C541.156,194.03,538.818,193.031,535.707,192.722z M517.22,180.299l5.329-0.645c2.899-0.351,5.066-0.21,6.503,0.423
														c1.437,0.633,2.266,1.865,2.487,3.696c0.237,1.961-0.208,3.442-1.336,4.441c-1.128,0.999-3.066,1.665-5.812,1.997l-5.884,0.712
														L517.22,180.299z M533.576,208.271c-1.2,1.229-3.195,2.012-5.985,2.35l-6.603,0.799l-1.508-12.455l6.277-0.76
														c5.666-0.686,8.74,0.954,9.22,4.921C535.243,205.327,534.776,207.042,533.576,208.271z"/>

												<rect x="553.919" y="166.988" transform="matrix(-0.9928 0.1202 -0.1202 -0.9928 1136.9572 313.545)" width="10.208" height="48.143"/>
												<polygon points="569.573,174.093 582.552,172.522 587.317,211.881 597.451,210.654 592.686,171.295 605.664,169.724
														604.643,161.29 568.552,165.659 		"/>
											</g>
										</g>
										</svg>
									</a>
									<br />
									<a href="" onClick="CetakResi(1);return false;"><span class="genmed">Debit Card</span></a>
								</td>
							</tr>
							<tr>
								<td align='center' valign='middle'>
									<a href="" onClick="CetakResi(2);return false;">
										<svg class="svgdialog" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
									<g id="Creditcard">
										<g>
											<path d="M697.904,330.702L625.22,104.128c-3.068-9.563-9.677-17.358-18.61-21.951c-8.931-4.59-19.117-5.428-28.679-2.365
												L208.438,198.344c-19.743,6.334-30.649,27.547-24.318,47.29l14.787,46.097H37.598C16.866,291.731,0,308.597,0,329.329v237.947
												c0,20.732,16.866,37.598,37.598,37.598h413.774c20.732,0,37.598-16.866,37.598-37.598V437.215l184.612-59.223
												C693.325,371.661,704.235,350.448,697.904,330.702z M451.375,570.562H37.598c-1.81,0-3.283-1.473-3.283-3.283V329.332
												c0-1.81,1.473-3.283,3.283-3.283h413.774c1.81,0,3.283,1.473,3.283,3.283v237.947
												C454.658,569.089,453.185,570.562,451.375,570.562z M590.916,112.696c0.552,0.283,1.281,0.846,1.624,1.916l14.504,45.211
												L231.301,280.364l-14.504-45.214c-0.555-1.724,0.4-3.577,2.125-4.129l369.49-118.532c0.36-0.117,0.701-0.163,1.012-0.163
												C590.038,112.327,590.55,112.507,590.916,112.696z M451.375,291.731H308.211l309.317-99.229l47.699,148.683
												c0.552,1.727-0.4,3.58-2.125,4.132l-174.128,55.86v-71.848C488.973,308.597,472.107,291.731,451.375,291.731z"/>
											<path d="M161.683,376.814H88.288c-9.474,0-17.158,7.684-17.158,17.158c0,9.474,7.684,17.158,17.158,17.158h73.395
												c9.474,0,17.158-7.684,17.158-17.158C178.841,384.497,171.16,376.814,161.683,376.814z"/>
											<path d="M243.66,411.129c9.474,0,17.158-7.684,17.158-17.158c0-9.474-7.684-17.158-17.158-17.158h-26.689
												c-9.474,0-17.158,7.684-17.158,17.158c0,9.474,7.684,17.158,17.158,17.158H243.66z"/>
											<path d="M426.676,451.164c0-9.474-7.684-17.158-17.158-17.158h-83.881c-9.474,0-17.158,7.684-17.158,17.158v74.35
												c0,9.474,7.684,17.158,17.158,17.158h83.881c9.474,0,17.158-7.684,17.158-17.158V451.164z M392.361,468.322v40.035h-49.566
												v-40.035H392.361z"/>
										</g>
									</g>
								</svg>
									</a>
									<br />
									<a href="" onClick="CetakResi(2);return false;"><span class="genmed">Credit Card</span></a>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Pilih Pembayaran-->

<!--dialog SPJ-->
<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td bgcolor='ffffff'>
		<table>
			<tr><td><h2>Cetak Manifest</h2></td></tr>
			<tr><td><div id="rewrite_list_mobil"></div></td></tr>
			<tr><td><div id="rewrite_list_sopir"></div></td></tr>
		</table>
		<span id='progress_dialog_spj' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog SPJ-->

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-4">
			<div class="col-md-12 box box-2" style="min-height: 110px; margin-bottom: 15px;">
				<font color='505050'>Cari Paket</font><br>
				<div class="input-group" style="width: 100%;">
					<input name="txt_cari_paket" id="txt_cari_paket" type="text" class="form-control" placeholder="No Telp / No Resi Paket">
			      	<span class="input-group-btn">
			       		 <input class='tombol form-control' name="btn_periksapaket" id="btn_periksapaket" value="Cari" type="button" onClick="periksaPaket(txt_cari_paket.value)">
			      	</span>
				</div><!-- /input-group -->
			</div>
		</div>
		<div class="col-sm-8">
			<div class="col-md-12 box box-2" style="min-height: 110px;">
				<div class="menuiconsmall notifikasi_pengumuman" id="rewritepengumuman"></div>
				<div class="menuiconsmall">
					<a href="#" onClick="{U_LAPORAN_PENJUALAN}">
						<svg class="svgsmall" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g>
						<path d="M606.394,610.994h-43.491V581.47c0-1.68-1.374-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967
							V581.47c0-1.68-1.374-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.374-3.054-3.054-3.054
							c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.374-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054
							v29.524h-54.967V581.47c0-1.68-1.368-3.054-3.054-3.054s-3.054,1.374-3.054,3.054v29.524h-54.967V581.47
							c0-1.68-1.368-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.368-3.054-3.054-3.054
							c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.368-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054
							v29.524H18.322v-69.216H42.85c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-54.967H42.85
							c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054
							c0-1.686-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054c0-1.686-1.368-3.054-3.054-3.054H18.322
							v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054
							c0-1.686-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054c0-1.686-1.368-3.054-3.054-3.054H18.322
							v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-49.88c0-5.057-4.104-9.161-9.161-9.161
							S0,53.209,0,58.266v561.889c0,5.057,4.104,9.161,9.161,9.161h597.233c5.057,0,9.161-4.104,9.161-9.161
							S611.451,610.994,606.394,610.994z"/>
						<rect x="95.545" y="132.185" width="62.602" height="414.057"/>
						<path d="M290.985,327.051c-8.196-10.713-14.841-22.237-19.923-34.269h-42.679v253.461h62.602V327.051z"/>
						<path d="M422.302,388.718c-2.028,0.086-4.055,0.214-6.083,0.214c-19.654,0-38.721-3.603-56.519-10.413v167.718h62.602V388.718z"
						/>
						<path d="M559.721,426.389l-60.238-61.124c-0.776,0.476-1.588,0.892-2.364,1.35v179.621h62.602V426.389z"/>
						<path d="M694.733,489.742L525.696,318.225c42.386-54.674,38.612-133.833-11.574-184.013c-54.393-54.399-142.915-54.399-197.327,0
							c-54.393,54.406-54.393,142.915,0,197.327c50.185,50.185,129.332,53.954,184.019,11.574l169.037,171.504
							c6.871,6.871,17.999,6.871,24.87,0.006C701.604,507.753,701.604,496.613,694.733,489.742z M489.265,306.664
							c-40.688,40.688-106.893,40.694-147.581,0c-40.688-40.682-40.682-106.887,0-147.569c40.688-40.682,106.887-40.694,147.581,0
							C529.953,199.777,529.953,265.982,489.265,306.664z"/>
						<path d="M420.556,217.952c-15.959-6.602-20.576-10.596-20.576-17.828c0-5.778,4.233-12.533,16.154-12.533
							c11.219,0,18.28,4.037,21.297,5.759c0.77,0.446,1.698,0.519,2.547,0.226c0.843-0.305,1.502-0.953,1.826-1.789l4.532-11.971
							c0.531-1.393-0.024-2.962-1.313-3.713c-6.492-3.781-13.73-5.949-22.036-6.578v-14.689c0-1.686-1.368-3.054-3.054-3.054h-11.226
							c-1.686,0-3.054,1.368-3.054,3.054v15.776c-16.704,3.689-27.349,15.898-27.349,31.686c0,19.049,15.293,27.685,31.777,34.171
							c13.186,5.332,18.561,10.761,18.561,18.713c0,8.789-7.439,14.695-18.487,14.695c-8.55,0-17.443-2.651-24.393-7.268
							c-0.776-0.525-1.753-0.647-2.651-0.36c-0.898,0.287-1.606,0.983-1.912,1.869l-4.349,12.123c-0.458,1.289-0.012,2.724,1.105,3.518
							c6.352,4.532,16.466,7.72,26.427,8.41v15.366c0,1.686,1.374,3.054,3.054,3.054h11.409c1.686,0,3.054-1.368,3.054-3.054v-16.313
							c17.217-3.854,28.62-16.967,28.62-33.31C450.513,237.258,441.547,226.503,420.556,217.952z"/>
					</g>

				</svg>
						<br>Laporan Penjualan
					</a>
				</div>
				<div class="menuiconsmall">
					<a href="#" onClick="{U_LAPORAN_UANG}">
						<svg class="svgsmall" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g id="Laporan_Uang">
						<path d="M341.256,70.663c5.952,0,10.781,4.829,10.781,10.782c0,5.952-4.829,10.781-10.781,10.781H28.565v388.292H675.42V92.225
							H384.312c-5.951,0-10.781-4.829-10.781-10.781c0-5.952,4.829-10.782,10.781-10.782h301.619h0.269
							c5.952,0,10.782,4.829,10.782,10.782v409.586v0.267c0,5.952-4.829,10.782-10.782,10.782H18.075h-0.292
							c-5.952-0.001-10.781-4.83-10.781-10.782V81.735v-0.292c0-5.952,4.829-10.782,10.781-10.782L341.256,70.663L341.256,70.663z"/>
						<path d="M330.453,16.735c0-5.929,4.829-10.736,10.736-10.736c5.929,0,10.736,4.806,10.736,10.736l0.023,64.687
							c0,5.929-4.806,10.736-10.736,10.736s-10.736-4.806-10.736-10.736L330.453,16.735z"/>
						<path d="M330.497,534.243c0-5.932,4.806-10.737,10.736-10.737s10.736,4.807,10.736,10.737l-0.045,107.877
							c0,5.906-4.806,10.734-10.736,10.734s-10.736-4.806-10.736-10.734L330.497,534.243z"/>
						<path d="M179.902,531.301c1.618-5.727,7.569-9.029,13.296-7.412c5.705,1.618,9.029,7.569,7.412,13.296l-43.034,150.977
							c-1.617,5.728-7.569,9.031-13.296,7.412c-5.705-1.617-9.029-7.569-7.412-13.296L179.902,531.301z"/>
						<path d="M481.813,537.185c-1.618-5.727,1.684-11.678,7.412-13.296c5.727-1.619,11.678,1.684,13.296,7.412l43.102,150.977
							c1.618,5.706-1.683,11.679-7.411,13.296c-5.707,1.619-11.679-1.684-13.297-7.412L481.813,537.185z"/>
						<path d="M17.783,545.228c-5.952,0-10.781-4.828-10.781-10.781c0-5.953,4.829-10.781,10.781-10.781h668.418
							c5.952,0,10.782,4.828,10.782,10.781c0,5.928-4.829,10.781-10.782,10.781H17.783z"/>
						<path d="M332.877,329.514l-34.385,38.693l-77.849-97.276c-0.516-0.741-1.123-1.436-1.841-2.043
							c-4.514-3.863-11.297-3.346-15.161,1.168l-89.616,104.149l-53.119-0.09c-5.929,0-10.736,4.805-10.736,10.736
							c0,5.926,4.806,10.734,10.736,10.734l58.061,0.093c3.256,0,6.176-1.44,8.175-3.776l0.023,0.024l84.383-98.085l78.185,97.702
							c3.683,4.625,10.444,5.369,15.071,1.684c0.471-0.402,0.92-0.81,1.325-1.257l0.023,0.023l26.726-30.072V329.514z"/>
						<g>
							<path d="M591.92,295.7c-0.007-0.059-0.011-0.118-0.02-0.176c-0.042-0.289-0.097-0.578-0.175-0.863
								c-0.001-0.003-0.002-0.006-0.003-0.009c-0.073-0.266-0.166-0.529-0.271-0.789c-0.03-0.074-0.063-0.146-0.095-0.219
								c-0.083-0.188-0.175-0.372-0.276-0.554c-0.044-0.079-0.086-0.159-0.133-0.237c-0.025-0.042-0.045-0.086-0.072-0.128
								c-0.106-0.168-0.222-0.326-0.339-0.483c-0.022-0.029-0.04-0.06-0.062-0.089c-0.179-0.232-0.372-0.447-0.575-0.652
								c-0.038-0.039-0.079-0.074-0.119-0.112c-0.176-0.17-0.359-0.329-0.549-0.478c-0.049-0.038-0.096-0.076-0.146-0.112
								c-0.255-0.189-0.518-0.365-0.792-0.518l-105.478-59.956l60.731-38.242l101.814,57.872l-42.879,27.001
								c-3.346,2.106-4.351,6.527-2.244,9.873c2.106,3.346,6.527,4.351,9.873,2.244l53-33.373c2.131-1.341,3.4-3.703,3.343-6.22
								c-0.057-2.517-1.43-4.818-3.62-6.063l-115.937-65.901c-2.293-1.304-5.119-1.24-7.353,0.165l-74.428,46.867
								c-0.015,0.009-0.031,0.019-0.046,0.029l-37.192,23.42c-0.017,0.011-0.034,0.021-0.051,0.032l-74.479,46.9
								c-2.131,1.341-3.4,3.703-3.343,6.22c0.057,2.517,1.43,4.818,3.62,6.063l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936
								c1.327,0,2.651-0.369,3.815-1.101l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873c-2.106-3.346-6.527-4.352-9.873-2.244
								l-49.373,31.09l-101.814-57.874l60.732-38.243l108.589,61.724v81.75c0,2.608,1.417,5.008,3.699,6.268
								c1.079,0.596,2.27,0.891,3.46,0.891c1.327,0,2.652-0.369,3.815-1.101l37.24-23.45c2.082-1.311,3.345-3.598,3.345-6.058v-85.915
								c0-0.042-0.007-0.082-0.009-0.125C591.964,296.176,591.948,295.938,591.92,295.7z M554.737,392.935v-72.946
								c0-2.575-1.383-4.952-3.622-6.224l-105.54-59.991l23.493-14.794l101.814,57.873l-8.914,5.613
								c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346c1.303,0,2.623-0.356,3.807-1.102l8.061-5.075v68.994
								L554.737,392.935z"/>
							<path d="M655.481,272.22l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873C663.247,271.117,658.826,270.112,655.481,272.22
								z"/>
							<path d="M522.283,356.094l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,354.991,525.629,353.986,522.283,356.094z"/>
							<path d="M655.481,300.858l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873C663.247,299.755,658.826,298.75,655.481,300.858
								z"/>
							<path d="M522.283,384.732l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,383.629,525.629,382.624,522.283,384.732z"/>
							<path d="M655.481,329.496l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873
								C663.247,328.394,658.826,327.389,655.481,329.496z"/>
							<path d="M522.283,413.37l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,412.268,525.629,411.262,522.283,413.37z"/>
						</g>
					</g>
				</svg>
						<br>Laporan Uang
					</a>
				</div>
				<div class="menuiconsmall">
					<a href="#" onClick="{U_CHECKIN_PAKET}">
						<svg class="svgsmall" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g>
						<path d="M305.016,224.064v252.192l200.778-38.103V315.277c0-5.144,4.48-9.139,9.59-8.553l24.701,3.822
							c4.346,0.499,7.627,4.178,7.627,8.553v142.748c0,4.058-2.833,7.564-6.8,8.417l-219.787,47.258
							c-10.621,2.283-21.604,2.308-32.235,0.072L71.462,471.859c-3.902-0.82-6.73-4.214-6.834-8.201l-3.756-144.974
							c-0.118-4.547,3.322-8.4,7.852-8.798l184.692-16.229c3.203-0.281,5.982-2.325,7.207-5.297l27.824-67.573
							C292.037,212.067,305.016,214.635,305.016,224.064z M58.783,141.494L0.825,265.38c-2.832,6.054,2.014,12.885,8.663,12.213
							L227.149,255.6c3.076-0.311,5.749-2.247,7.003-5.073l70.035-157.78L65.026,136.675C62.289,137.178,59.963,138.973,58.783,141.494z
							 M304.188,92.747l74.106,162.712c1.295,2.844,4.029,4.762,7.145,5.013l217.25,17.471c6.547,0.526,11.248-6.178,8.522-12.154
							l-56.658-124.212c-1.172-2.569-3.528-4.399-6.308-4.9L304.188,92.747z"/>
					</g>
				</svg>
						<br>Check-in Paket
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<div class="col-md-12 box box-2">
				<span class='formHeader sectiontitle'>1. Jadwal</span>
				<div class="col-md-12" style="text-align: center;">
					<b><h5>Tanggal keberangkatan</h5></b>
					<!--kalender-->
					<iframe width=174 height=189  style="border-radius: 3px;" name="gToday:normal:agenda.js" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
					</iframe>
					<!--kalender-->
					<form name="formTanggal">
						<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />
					</form>
					<input class="mybutton paper paper-lift form-control" style="width: 190px; margin: 0 auto;" name="update" onclick="getUpdateAsal(kota_asal.value,1)" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
				</div>
				<div class="col-md-12">
					<br />Kota Keberangkatan<br>
					<div class="table-reservasi">
						<select class="form-control" onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select>
						<div id="rewritetype"></div>
						<div id="rewriteall">
							<div class="table-reservasi" id="rewriteasal"></div>
							<div class="table-reservasi" id="rewritetujuan"></div>
							<div class="table-reservasi" id="rewritejadwal"></div>
							<!--<span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="col-md-12 box box-2">
				<span class='formHeader sectiontitle'>2. List Paket</span>
				<br>
				<div id='loadingrefresh' class="resvcargoloadingcirc"></div>
				<div id="rewritepaket" align="center"><!-- LAYOUT PAKET--></div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="col-md-12 box box-2">
				<span class='formHeader sectiontitle'>3. Data Paket</span>
				<br>
				<!--data pelanggan-->
				<div id='loadingdetail' class="resvcargoloadingcirc"></div>
				<div id="showdatadetail" align='left'></div>

				<!--data isian pengiriman paket-->
				<form onsubmit="return false;">
					<div style="margin-left: 5px;">
						<h2>Data Pengirim</h2>
						<span class="resvcargobooklabel">Telp Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input class="form-control" id='telp_pengirim' type='text' maxlength='15' onkeypress='validasiNoTelp(event);' onBlur="cariDataPelangganByTelp4Paket(this.value,1)" onfocus="this.style.background='';"/></span></br>
						<span class="resvcargobooklabel">Nama Pengirim <span id='loadingcaritelppengirim' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=1 color="777777">&nbsp;mencari</font></span></span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input class="form-control" id='nama_pengirim' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>
						<span class="resvcargobooklabel">Alamat Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input class="form-control" id='alamat_pengirim' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>

						<h2>Data Penerima</h2>
						<span class="resvcargobooklabel">Telp Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input class="form-control" id='telp_penerima' type='text' maxlength='15' onkeypress='validasiNoTelp(event);' onBlur="cariDataPelangganByTelp4Paket(this.value,0)" onfocus="this.style.background='';"/></span></br>
						<span class="resvcargobooklabel">Nama Penerima <span id='loadingcaritelppenerima' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=1 color="777777">&nbsp;mencari</font></span></span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input class="form-control" id='nama_penerima' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>
						<span class="resvcargobooklabel">Alamat Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input class="form-control" id='alamat_penerima' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>

						<h2>Data Barang</h2>
						<input id='harga_kg_pertama_1' type='hidden' value='0'/>
						<input id='harga_kg_pertama_2' type='hidden' value='0'/>
						<input id='harga_kg_pertama_3' type='hidden' value='0'/>
						<input id='harga_kg_pertama_4' type='hidden' value='0'/>
						<input id='harga_kg_pertama_5' type='hidden' value='0'/>
						<input id='harga_kg_pertama_6' type='hidden' value='0'/>
						<input id='harga_kg_berikutnya_1' type='hidden' value='0'/>
						<input id='harga_kg_berikutnya_2' type='hidden' value='0'/>
						<input id='harga_kg_berikutnya_3' type='hidden' value='0'/>
						<input id='harga_kg_berikutnya_4' type='hidden' value='0'/>
						<input id='harga_kg_berikutnya_5' type='hidden' value='0'/>
						<input id='harga_kg_berikutnya_6' type='hidden' value='0'/>
						<span class="resvcargobooklabel">Jenis Barang</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select class="form-control" id='dimensi' onChange="setJenisBarang(this.value);"><option value="1">Dokumen</option><option value="2">Barang</option><option value="3">Bagasi</option></select></span></br>
						<span class="resvcargobooklabel">Koli</span><span class="resvcargobookfield">:</span>
							<span class="resvcargobookfield">
								<div class="input-group">
									<input class="form-control" id='jumlah_koli' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);'  onfocus="this.style.background='';" />
									<span class="input-group-addon" style="background: none; border: none;">Pax</span>
								</div>
							</span></br>
						<span class="resvcargobooklabel">Perhitungan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input type="radio" name="isvolumetric" id="isvolumetric0" onclick="setFormVolumetric();" selected/>Weight &nbsp; <input type="radio" name="isvolumetric" id="isvolumetric1" onclick="setFormVolumetric();"/>Volumetric (PxLxT)</span></br>

						<span id="showinputvolumetric" style="display: none;">
							<span class="resvcargobooklabel">Dimensi P x L x T</span><span class="resvcargobookfield">:</span>
							<span class="resvcargobookfield">
								<div class="input-group">
								<input class="form-control" id='dimensipanjang' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="P" onblur="hitungHargaPaket();"/>
									<span class="input-group-addon" style="background: none; border: none;">cm x</span>
								</div>
							</span><br>
							<span class="resvcargobookfield" style="padding-left: 210px">
								<div class="input-group">
									<input class="form-control" id='dimensilebar' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="L" onblur="hitungHargaPaket();"/>
									<span class="input-group-addon" style="background: none; border: none;">cm x</span>
								</div>
							</span><br>
							<span class="resvcargobookfield" style="padding-left: 210px">
								<div class="input-group">
								<input class="form-control" id='dimensitinggi' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="T" onblur="hitungHargaPaket();"/>
									<span class="input-group-addon" style="background: none; border: none;">cm</span>
								</div>
							</span></br>
							<span class="resvcargobooklabel">Berat</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<span id="beratvolumetric"></span>&nbsp;Kg</span>
						</span>

						<span id="showinputweight" style="display: none;">
							<span class="resvcargobooklabel">Berat</span>
							<span class="resvcargobookfield">:</span>
							<span class="resvcargobookfield">&nbsp;
								<div class="input-group">
									<input class="form-control" id='berat' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';"/>
									<span class="input-group-addon" style="background: none; border: none;">Kg</span>
								</div>
							</span>
						</span>
						 <span id="showinputsizebagasi" style="display: none;">
							<span class="resvcargobooklabel">Ukuran</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select class="form-control" id='ukuranbagasi' ><option value="1">Kecil</option><option value="2">Sedang</option><option value="3">Besar</option></select></span>
						</span>
						<br>
						<span class="resvcargobooklabel">Biaya Kirim</span>
							<span class="resvcargobookfield">:</span>
							<span class="resvcargobookfield">
								<div class="input-group">
									<span class="input-group-addon" style="background: none; border: none;">Rp.</span>
									<input class="form-control" id="harga_paket" type="text" style='text-align: right;' size='10' maxlength='7'  onkeypress='validasiInputanAngka(event);' onfocus="this.style.background=''";>
								</div>
							</span></br>
						<span class="resvcargobooklabel">Layanan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select class="form-control" id='jenislayanan'><option value='P2P'>Port to Port</option><option value='P2D'>Port to Door</option></select></span></br>
						<span class="resvcargobooklabel">Pembayaran</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select class="form-control" id='cara_bayar'><option value="0">Langsung</option><!--<option value="1">Langganan</option><option value="2">Bayar di Tujuan</option>--></select></span></br>
						<span class="resvcargobooklabel">Keterangan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<textarea class="form-control" id='keterangan' rows='3' cols='60' onfocus="this.style.background='';" ></textarea></span></br>
						<br>
						<center>
						<table>
							<tr>
								<td width="40%">
									<input type="button" class="form-control"  onClick="resetIsianPaket();" id="button_cancel" value="Cancel">
								</td>
								<td width="20%"></td>
								<td width="40%">
									<input type="button" class="form-control" onclick="pesanPaket();" id="button_ok" value="Simpan">
								</td>
							</tr>
						</table>
						</center>
					</div>
				</form>
			<div>
		</div>
	</div>

</div>
