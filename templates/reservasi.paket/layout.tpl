<input type='hidden' value='{NO_SPJ}' name='nospj' id='nospj' />
<input type='hidden' value='{JAM_BERANGKAT}' id='jamberangkataktif' />
										
<div id="cargolayoutwrapper" align="center">
	<!-- BEGIN TOMBOL_MUTASIKAN -->
	<center><input type="button" value="Mutasikan Kesini" style="width:150px;height: 30px;" onclick="mutasiPaket();"></center>
	<!-- END TOMBOL_MUTASIKAN -->
	
	<span class="resvcargolayoutlabel">Jadwal Berangkat</span><span class="resvcargolayoutfield">: {TANGGAL_BERANGKAT_DIPILIH}</span><br>
	<span class="resvcargolayoutlabel">Jurusan</span><span class="resvcargolayoutfield">: {POIN_BERANGKAT_DIPILIH}</span><br>
	
	<!-- BEGIN DATA_MANIFEST -->
	<input type='hidden' name='mobilsekarang' id='mobilsekarang' value='{DATA_MANIFEST.body}'/>
	<input type='hidden' name='sopirsekarang' id='sopirsekarang' value='{DATA_MANIFEST.kodesopir}' />
	
	<span class="resvcargolayoutlabel">#Manifest</span><span class="resvcargolayoutfield">: <b>{DATA_MANIFEST.nospj}</b></span><br>
	<span class="resvcargolayoutlabel">#Unit</span><span class="resvcargolayoutfield">: <b>{DATA_MANIFEST.body}</b></span><br>
	<span class="resvcargolayoutlabel">Sopir</span><span class="resvcargolayoutfield">: <b>{DATA_MANIFEST.sopir}</b></span><br>
	<span class="resvcargolayoutlabel">Waktu Cetak</span><span class="resvcargolayoutfield">: {DATA_MANIFEST.waktucetak}</span><br>
	<!-- END DATA_MANIFEST -->
	
	<!-- BEGIN BELUM_BERANGKAT -->
	<center><h2 style="color: red;">BELUM BERANGKAT</h2></center>
	<!-- END BELUM_BERANGKAT -->
	
	<br>
	<center>
		<!-- BEGIN TOMBOL_CETAK_MANIFEST -->
		<input type='button' value='CETAK MANIFEST' onclick='cetakSPJ();' style="width: 150px; height: 30px;margin-right: 5px;" />
		<!-- END TOMBOL_CETAK_MANIFEST -->
		<input type='button' value='REFRESH' onclick='getUpdatePaket();getPengumuman();' style="width: 150px; height: 30px;margin-left: 5px;"/>
		<hr>
		Trx:<b>{TOTAL_TRANSAKSI}</b> | Koli:<b>{TOTAL_PAX}</b> | Omzet:<b>Rp. {TOTAL_OMZET}</b>
		<hr>
	</center>
	<!-- BEGIN DAFTAR_BARANG -->
	<div class='{DAFTAR_BARANG.odd}' onclick="showPaket('{DAFTAR_BARANG.resi}')" style="cursor: pointer;">
		<h3 style="float:left;font-size: 20px;padding-left: 5px;">{DAFTAR_BARANG.no}</h3>
		<div class="resvcargolistawb">Resi: {DAFTAR_BARANG.resi}</div>
		<div class="resvcargolisttitle">{DAFTAR_BARANG.jenisbarang}</div>
		<div style='color:red;font-size:12px;font-weight:bold;float:right;text-transform:uppercase;'>{DAFTAR_BARANG.poinberangkat}</div>
		<div class="resvcargolisttitle">{DAFTAR_BARANG.jenislayanan}</div>
		<div class="resvcargolistdata">Pengirim: {DAFTAR_BARANG.nama_pengirim}</div>
		<div class="resvcargolistdata">Penerima: {DAFTAR_BARANG.nama_penerima}</div>
    <div class="resvcargolistpax">{DAFTAR_BARANG.isvolumetrik}</div>
    {DAFTAR_BARANG.dimensi}
		<div class="resvcargolistpax">Koli: {DAFTAR_BARANG.koli} Pax / {DAFTAR_BARANG.labelberat}: {DAFTAR_BARANG.berat}</div>
		<div style='color:green;font-size:12px;font-weight:bold;text-align:right;'>Biaya Rp. {DAFTAR_BARANG.biaya}</div>
		<hr>
	</div>
	<!-- END DAFTAR_BARANG -->
	
	<!-- BEGIN NO_DATA -->
	<div style="background: #ffcc00;height: 50px;text-align: center;"><h2>Daftar Barang Kosong</h2></div>
	<!-- END NO_DATA -->
</div>