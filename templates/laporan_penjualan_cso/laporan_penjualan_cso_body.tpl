<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudulw">
								<div class="bannerjudul">
									Laporan Penjualan CSO
								</div>
							</td>
							<td align='right' valign='middle' >
								<table>
									<tr><td class=''>
										<div class="col-md-3">
											Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
										</div>
										<div class="col-md-3">
											s/d<br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
										</div>
										<div class="col-md-3">
											Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
										</div>
										<div class="col-md-3"><input type="submit" class="btn mybutton topwidth" value="cari" /></div>
									</td></tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding: 10px 0px;">
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
							<td style="padding: 10px 0px;">
								<table width='100%'>
									<tr>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="table table-hover table-bordered">
				    <tr>
				      <th width=30>No</th>
							<th width=200><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Nama</a></th>
							<th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>NRP</a></th>
							<th width=100><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Cabang</a></th>
							<th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Tiket</a></th>
							<th width=400><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Penjualan Tiket</a></th>
							<th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Paket</a></th>
							<th width=400><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Penjualan Paket</a></th>
							<th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Discount</a></th>
							<th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Total</a></th>
							<th width=100>Action</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td valign='top'><div align="right">{ROW.no}</div></td>
				       <td valign='top'><div align="left">{ROW.nama}</div></td>
							 <td valign='top'><div align="left">{ROW.nrp}</div></td>
				       <td valign='top'><div align="left">{ROW.cabang}</div></td>
							 <td valign='top'><div align="right">{ROW.tiket}</div></td>
							 <td align='center'>
								Total: {ROW.total_uang_tiket} &nbsp;(<a class="" data-toggle="collapse" data-target="#tiket_{ROW.no}">
								Detail
								</a>)

								<div id="tiket_{ROW.no}" class="collapse">
									<table class="table" style="text-align: center; background: none;">
										<thead>
											<td>Tunai</td>
											<td>Debit</td>
											<td>Kredit</td>
										</thead>
										<tbody>
											<tr>
												<td>{ROW.total_tunai}</td>
												<td>{ROW.total_debit}</td>
												<td>{ROW.total_kredit}</td>
											</tr>
										</tbody>
									</table>
								</div>
							 </td>
							 <td valign='top'><div align="right">{ROW.paket}</div></td>
							 <td align='center'>
								Total: {ROW.total_uang_paket} &nbsp;(<a class="" data-toggle="collapse" data-target="#paket_{ROW.no}">
								Detail
								</a>)

								<div id="paket_{ROW.no}" class="collapse">
									<table class="table" style="text-align: center; background: none;">
										<thead>
											<td>Tunai</td>
											<td>Debit</td>
											<td>Kredit</td>
										</thead>
										<tbody>
											<tr>
												<td>{ROW.total_tunai_paket}</td>
												<td>{ROW.total_debit_paket}</td>
												<td>{ROW.total_kredit_paket}</td>
											</tr>
										</tbody>
									</table>
								</div>
							 </td>
							 <td valign='top'><div align="right">{ROW.discount}</div></td>
							 <td valign='top'><div align="right">{ROW.total}</div></td>
				       <td valign='top'><div align="center">{ROW.act}</div></td>
				     </tr>
				     <!-- END ROW -->
			    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='center'>
								<table>
									<tr>
										<td>
											 &nbsp;
										</td><td bgcolor='D0D0D0'></td>
										<td>
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>