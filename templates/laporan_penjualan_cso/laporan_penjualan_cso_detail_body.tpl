<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form action="{ACTION_CARI}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr bgcolor='505050' height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Penjualan Uang CSO</td>
								<td align='right' valign='middle' >
									<table>
										<tr><td class='bannernormal'>
											&nbsp;Periode:&nbsp;<input readonly="yes"  id="p0" name="p0" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
											&nbsp; s/d &nbsp;<input readonly="yes"  id="p1" name="p1" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
											&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
											<input type="submit" value="cari" />&nbsp;	
											<input type="hidden" id="p2" name="p2" value="{ID_USER}" />											
										</td></tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan=2 align='center' valign='middle'>
									<table>
										<tr>
											<td>
												 &nbsp;
											</td><td bgcolor='D0D0D0'></td>
											<td>
												<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table class="border">
    <tr>
       <th width=30>No</th>
			 <th width=200>Waktu Pesan</th>
			 <th width=200>No.Tiket</th>
			 <th width=200>Waktu Berangkat</th>
			 <th width=100>Kode Jadwal</th>
			 <th width=200>Nama</th>
			 <th width=50>Kursi</th>
			 <th width=100>Harga Tiket</th>
			 <th width=100>Discount</th>
			 <th width=70>Total</th>
			 <th width=100>Tipe Disc.</th>
			 <th width=200>CSO</th>
			 <th width=100>Status</th>
			 <th width=100>Ket.</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.waktu_pesan}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
       <td><div align="left">{ROW.waktu_berangkat}</div></td>
       <td><div align="left">{ROW.kode_jadwal}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
			 <td><div align="center">{ROW.no_kursi}</div></td>
			 <td><div align="right">{ROW.harga_tiket}</div></td>
			 <td><div align="right">{ROW.discount}</div></td>
			 <td><div align="right">{ROW.total}</div></td>
			 <td><div align="left">{ROW.tipe_discount}</div></td>
			 <td><div align="left">{ROW.cso}</div></td>
       <td><div align="center">{ROW.status}</div></td>
       <td><div align="left">{ROW.ket}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>