<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<!--HEADER-->
				<table width='100%' cellspacing="0">
					<tr class='' height=40>
						<td align='center' valign='top' class="bannerjudulw">
							<div class="bannerjudul">Laporan Voucher BBM</div>
						</td>
						<td valign='middle'>
							<form action="{ACTION_CARI}" method="post">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-2 col-md-offset-1">
											Kota:<br /><select class="form-control" id='kota' name='kota'>{OPT_KOTA}</select></div>
										<div class="col-md-2">
											Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
										</div>
										<div class="col-md-2">
											s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
										</div>
										<div class="col-md-3">
											Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
										</div>
										<div class="col-md-2">
											<input type="submit"  class="btn mybutton topwidth" style="height: 34px;" value="cari" />&nbsp;
											<input type='hidden' name='mode' value='0'/>
										</div>
									</div>
								</div>
							</form>
						</td>
					</tr>
					<tr>
						<td colspan=2 align='center' valign='middle'>
							<table>
								<tr>
									<td>
										 &nbsp;
									</td><!--<td bgcolor='D0D0D0'>--></td>
									<td>
										<!--<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>-->
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						
					</tr>
					<tr><td colspan=2>
						<table width="100%"><tr>
							<td align='left' valign='bottom' colspan=3>
						{SUMMARY}
						</td>
						<td align="right">{PAGING}</td>
						</tr></table>
					</td></tr>
				</table>
				<!-- END HEADER-->
				<table class="table table-hover table-bordered" style="margin-top: 10px;" width='100%' >
				  <tr>
				     <th width=30>No</th>
						 <th width=70><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Tanggal</a></th>
						 <th width=200><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Jurusan</a></th>
						 <th width=50><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Jam</a></th>
						 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>No.SPJ</a></th>
						 <th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Kendaraan</a></th>
						 <th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Sopir</a></th>
						 <th width=70><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Jumlah</a></th>
						 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Pencetak</a></th>
				   </tr>
				   <!-- BEGIN ROW -->
				   <tr class="{ROW.odd}">
				     <td ><div align="right">{ROW.no}</div></td>
				     <td ><div align="left">{ROW.tanggal}</div></td>
						 <td ><div align="left">{ROW.jurusan}</div></td>
				     <td ><div align="center">{ROW.jam}</div></td>
						 <td ><div align="left">{ROW.no_spj}</div></td>
						 <td ><div align="left">{ROW.kendaraan}</div></td>
						 <td ><div align="left">{ROW.sopir}</div></td>
						 <td ><div align="right">{ROW.jumlah}</div></td>
						 <td ><div align="center">{ROW.pencetak}</div></td>
				   </tr>
				   <!-- END ROW -->
			  </table>
				<table width='100%'>
					<tr>
						<td align='right' width='100%'>
							{PAGING}
						</td>
					</tr>
					<tr>
						<td align='left' valign='bottom' colspan=3>
						
						</td>
					</tr>
				</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>