<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudul">&nbsp;Member Hampir Expired</td>
							<td colspan=2 align='right' class="bannernormal" valign='middle'>
								<br>
								<form action="{ACTION_CARI}" method="post">
									<table>
									<tr><td class=''>
										<!--&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
										&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>-->
										Cari:<br />
										<div class="input-group" style="width: 100%;">
									      <input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
									      <span class="input-group-btn">
									        <input class="tombol form-control" type="submit" value="cari" />
									      </span>
									    </div><!-- /input-group -->
									</td></tr>
								</table>
								</form>
							</td>
						</tr>
						<tr>
							<td align='center' colspan=3>
								<table>
									<tr>
										<td>
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan=3 width='100%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
					<table width='100%' class="table table-bordered table-hover">
					    <tr>
					       <th width=30></th>
					       <th width=30>No</th>
								 <th width=200><a class="th" href='{A_SORT_BY_NAMA}'>Nama Member</a></th>
								 <th width=100><a class="th" href='{A_SORT_BY_KODE}'>Kode</a></th>
								 <th width=300><a class="th" href='{A_SORT_BY_ALAMAT}'>Alamat</a></th>
								 <th width=70><a 	class="th" href='{A_SORT_BY_HP}'>Telepon</a></th>
								 <th width=70><a 	class="th" href='{A_SORT_BY_EMAIL}'>Email</a></th>
								 <th width=100><a class="th" href='{A_SORT_BY_PEKERJAAN}'>Pekerjaan</a></th>
								 <th width=100><a class="th" href='{A_SORT_BY_FREKWENSI}'>Frekwensi</a></th>
								 <th width=100><a class="th" href='{A_SORT_BY_TGL_EXPIRED}'>Tgl.Expired</a></th>
								 <th width=100><a class="th" href='{A_SORT_BY_STATUS}'>Aktif</th>
					     </tr>
					     <!-- BEGIN ROW -->
					     <tr class="{ROW.odd}">
					       <td><div align="center">{ROW.check}</div></td>
					       <td><div align="right">{ROW.no}</div></td>
								 <td><div align="left">{ROW.nama}</div></td>
								  <td><div align="left">{ROW.id_member}</div></td>
					       <td><div align="left">{ROW.alamat}</div></td>
								 <td><div align="left">{ROW.hp}</div></td>
								 <td><div align="left">{ROW.email}</div></td>
								 <td><div align="left">{ROW.pekerjaan}</div></td>
								 <td><div align="right">{ROW.frekwensi}</div></td>
								 <td><div align="left">{ROW.expired}</div></td>
								 <td><div align="center">{ROW.aktif}</div></td>
					     </tr>  
					     <!-- END ROW -->
					 	{NO_DATA}
			   		</table>
			    	<table width='100%'>
						<tr>
							<td width='100%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
			 	</td>
			</tr>
			</table>
		</div>
	</div>
</div>
