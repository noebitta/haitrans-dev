<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script language="JavaScript">
// komponen khusus dojo 
dojo.require("dojo.widget.Dialog");
	
function Start(page) {
OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=no,resizable=yes");
}

function ValidasiAngka(objek,kolom){
	temp_nilai=objek.value*0;
	
	if(temp_nilai!=0){
		alert(kolom+" harus angka!");
		objek.setFocus;exit;
	}
	
	if(objek.value<0){
		alert(kolom+" tidak boleh kurang dari 0!");
		objek.setFocus;exit;
	}
	
}

function konfirmPassword(){	

	if(document.getElementById('jumlah_uang').value==""){
		alert("Anda belum memasukkan jumlah uang!");
		exit;
	}
	
	ValidasiAngka(document.getElementById('jumlah_uang'),"Jumlah uang");
	document.getElementById('password').value=""; 
	dlg_konf_password.show();
}
	
function init(e) {
	//dialog tanya hapus__________________________________________________________________
	dlg_konf_passwordBtnYes = document.getElementById("dlg_konf_passwordBtnYes");
	dlg_konf_passwordBtnNo = document.getElementById("dlg_konf_passwordBtnNo");
	dlg_konf_password = dojo.widget.byId("dlg_konf_password");
	dlg_konf_password.setCloseControl(dlg_konf_passwordBtnYes);
  dlg_konf_password.setCloseControl(dlg_konf_passwordBtnNo);
	
	document.forms.frm_input_kartu.id_kartu.focus();
}

dojo.addOnLoad(init);

</script>

<!--dialog konfirm password-->
<div dojoType="dialog" width="400" id="dlg_konf_password" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="100" style="display: none;">
<font color='FFFFFF'><h3>Otorisasi Transaksi Member</h3></font>
<form name="f_otorisasi" id="f_otorisasi" action="member_penyesuaian_saldo.php?sid={SID}&mode=proses_transaksi" method='post'>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center" colspan=3>
		Untuk melakukan proses ini dibutuhkan password otorisasi anda, <br>
		silahkan masukkan password anda
	</td>
</tr>
<tr>
	<td>Password</td>
	<td>:</td>
  <td>
		<Input type='password' id='password' name='password'  />
		{PARAMETER}
	</td>
</tr>
<tr>
	<td align="center" colspan=3>
		<br>
		<input type="button" id="dlg_konf_passwordBtnNo" value="&nbsp;&nbsp;Cancel&nbsp;&nbsp;">
		<input type="submit"  id="dlg_konf_passwordBtnYes" value="&nbsp&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp">
	</td>
</tr>
</table>
</form>
</div>
<!--END  konfirm password-->

<table  width= "100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
		<td align='center'>
			<h2>PENYESUAIAN SALDO MEMBER</h2>
	</tr>
	<tr>
		<td align='center'>{BODY}</td>
	</tr>
</table>
