<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container" style="width: 95%;">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
			 <td class="whiter" valign="middle" align="left">		
				<form action="{ACTION_CARI}" method="post">
					<!--HEADER-->
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudulw">
								<div class="bannerjudul">
									Laporan Omzet Jurusan
								</div>
							</td>
							<td align='left' valign='middle'>
								<div class="col-md-3">
									Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
								</div>
								<div class="col-md-3">
									s/d <br /><input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
								</div>
								<div class="col-md-3">
									Cari:<br /><input class="form-control" type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
								</div>
								<div class="col-md-3">
									<input class="btn mybutton topwidth" type="submit" value="cari" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan=2 align='center' valign='middle' style="padding-top: 10px;">
								<table>
									<tr>
										<td>
											<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan=2>
								<table width='100%'>
									<tr>
										<td align='right' valign='bottom'>
										{PAGING}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END HEADER-->
					<table class="table table-bordered table-hover table-responsive" width='100%' style="margin-top: 10px;">
			    <tr>
			       <th width=30 rowspan=2 >No</th>
						 <th width=400 rowspan=2><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jurusan</a></th>
						 <th width=100 rowspan=2><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Jurusan</a></th>
						 <th width=100 rowspan=2><a class="th">Open Trip</a></th>
						 <th width=100 rowspan=2><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Trip</a></th>
						 <th colspan=10 class="thintop">Total Penumpang</th>
						 <th width=100 rowspan=2><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>Pnp<br>/Trip</a></th>
						 <th width=200 rowspan=2><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>Tot.Omz.Pnp (Rp.)</a></th>
						 <th width=200 rowspan=2><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>Tot.Pkt</a></th>
						 <th width=200 rowspan=2><a class="th" href='{A_SORT_14}' title='{TIPS_SORT_14}'>Tot.Omz.Pkt</a></th>
						 <th width=200 rowspan=2><a class="th" href='{A_SORT_15}' title='{TIPS_SORT_15}'>Tot.Disc</a></th>
						 <th width=200 rowspan=2><a class="th" href='{A_SORT_16}' title='{TIPS_SORT_16}'>Tot.Biaya Langsung</a></th>
						 <th width=200 rowspan=2><a class="th" href='{A_SORT_17}' title='{TIPS_SORT_17}'>Tot.Laba Kotor</a></th>
						 <th width=100 rowspan=2>Action</a></th>
			     </tr>
					<tr>
						<th class='thinbottom'><a class="th" id="sort13" href='#'>B</a></th>
						<th class='thinbottom'><a class="th" id="sort14" href='#'>U</a></th>
						<th class='thinbottom'><a class="th" id="sort15" href='#'>A</a></th>
						<th class='thinbottom'><a class="th" id="sort16" href='#'>M</a></th>
						<th class='thinbottom'><a class="th" id="sort17" href='#'>S</a></th>
						<th class='thinbottom'><a class="th" id="sort18" href='#'>G</a></th>
						<th class='thinbottom'><a class="th" id="sort19" href='#'>R</a></th>
						<th class='thinbottom'><a class="th" id="sort20" href='#'>V</a></th>
						<th class='thinbottom'><a class="th" id="sort21" href='#'>O</a></th>
						<th class='thinbottom'><a class="th" id="sort22" href='#'>T</a></th>
					 </tr>
			     <!-- BEGIN ROW -->
			     <tr class="{ROW.odd}">
			       <td ><div align="right">{ROW.no}</div></td>
			       <td ><div align="left">{ROW.jurusan}</div></td>
						 <td ><div align="left">{ROW.kode_jurusan}</div></td>
						 <td ><div align="right">{ROW.open_trip}</div></td>
						 <td ><div align="right">{ROW.total_keberangkatan}</div></td>
						 <td width=60 style="background: #2196FD;color: white;"><div align="right" >{ROW.total_penumpang_b}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_u}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_m}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_k}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_kk}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_g}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_r}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_v}</div></td>
						 <td width=60><div align="right" >{ROW.total_penumpang_o}</div></td>
						 <td width=60 style="background: #FFD102;"><div align="right" >{ROW.total_penumpang}</div></td>
						 <td ><div align="right">{ROW.rata_pnp_per_trip}</div></td>
						 <td ><div align="right">{ROW.total_omzet}</div></td>
						 <td ><div align="right">{ROW.total_paket}</div></td>
						 <td ><div align="right">{ROW.total_omzet_paket}</div></td>
						 <td ><div align="right">{ROW.total_discount}</div></td>
						 <td ><div align="right">{ROW.total_biaya}</div></td>
						 <td ><div align="right">{ROW.total_profit}</div></td>
			       <td ><div align="center">{ROW.act}</div></td>
			     </tr>
			     <!-- END ROW -->
			    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
						<tr>
							<td align='left' valign='bottom' colspan=3>
								<table>
									<tr><td>B</td><td>=</td><td>Penumpang Booking</td></tr>
									<tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
									<tr><td>A</td><td>=</td><td>Anak/Lansia</td></tr>
									<tr><td>M</td><td>=</td><td>Member</td></tr>
									<tr><td>S</td><td>=</td><td>Special Ticket Staff</td></tr>
									<tr><td>G</td><td>=</td><td>Tiket Gratis</td></tr>
									<tr><td>O</td><td>=</td><td>online</td></tr>
									<tr><td>T</td><td>=</td><td>Total</td></tr>
								</table>
								</table>
							</td>
						</tr>
					</table>
				</form>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>