<script language="JavaScript">

var kode;

function cekValTahun(tahun){
	cek_value=tahun*0;
	
	if(cek_value!=0){
		return false;
	}
	
	if((tahun*1)<1920 || (tahun*1>2050)){
		return false;
	}
	else{
		return true;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_kendaraan_invalid');
	Element.hide('no_polisi_invalid');
	Element.hide('km_invalid');
	Element.hide('tahun_pembuatan_invalid');
	
	kode_kendaraan		= document.getElementById('kode_kendaraan');
	no_polisi					= document.getElementById('no_polisi');
	no_stnk						= document.getElementById('no_stnk');
	km								= document.getElementById('km');
	tahun_pembuatan		= document.getElementById('tahun_pembuatan');
	
	if(kode_kendaraan.value==''){
		valid=false;
		Element.show('kode_kendaraan_invalid');
	}
	
	if(no_polisi.value==''){
		valid=false;
		Element.show('no_polisi_invalid');
	}
	
	if(!cekValTahun(tahun_pembuatan.value)){	
		valid=false;
		Element.show('tahun_pembuatan_invalid');
	}
	
	cek_value=km.value*0;
	
	if(cek_value!=0){
		valid=false;
		Element.show('km_invalid');
	}
	
	if(km.value==''){
		km.value=0;
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<form name="frm_data_mobil" action="{U_MOBIL_ADD_ACT}" method="post" onSubmit='return validateInput();'>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td class="whiter" valign="middle" align="center">
				<table width='800'>
					<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
					<tr>
						<td align='center' valign='top' width='400' class="pad10">
							<table width='400'>   
								<tr>
									<td colspan=3><h2>{JUDUL}</h2></td>
								</tr>
								<tr><td colspan=3><h3><u>Data Umum Kendaraan</u></h3></td></tr>
								<tr>
						      <input class="form-control"  type="hidden" name="kode_kendaraan_old" value="{KODE_KENDARAAN_OLD}">
									<td class="td-title" width='200'><u>Kode Kendaraan</u></td>
									<td width='5'></td>
									<td class="pad10"><input class="form-control" placeholder="Kode Kendaraan"  type="text" id="kode_kendaraan" name="kode_kendaraan" value="{KODE_KENDARAAN}" maxlength=15 onChange="Element.hide('kode_kendaraan_invalid');"><span id='kode_kendaraan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
						    </tr>
								<tr>
						      <input class="form-control"  type="hidden" name="no_polisi_old" value="{NO_POLISI_OLD}">
									<td class="td-title" width='200'><u>Nomor Polisi</u></td><td width='5'></td><td class="pad10"><input class="form-control"  type="text"  placeholder="Nomor Polisi" id="no_polisi" name="no_polisi" value="{NO_POLISI}" maxlength=15 onChange="Element.hide('no_polisi_invalid');"><span id='no_polisi_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
						    </tr>
								<tr>
						      <td class="td-title">Jenis</td><td></td><td class="pad10"><input  placeholder="Jenis" class="form-control"  type="text" name="jenis" value="{JENIS}" maxlength=50></td>
						    </tr>
								<tr>
						      <td class="td-title">Merk</td><td></td><td class="pad10"><input placeholder="Merk" class="form-control"  type="text" name="merek" value="{MEREK}" maxlength=50></td>
						    </tr>
								<tr>
						      <td class="td-title"><u>Tahun pembuatan</u></td><td></td><td class="pad10"><input class="form-control"  type="text" id="tahun_pembuatan"  placeholder="Tahun Pembuatan" name="tahun_pembuatan" value="{TAHUN_PEMBUATAN}" maxlength=4 onChange="Element.hide('tahun_pembuatan_invalid');"><span id='tahun_pembuatan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
						    </tr>
								<tr>
						      <td class="td-title">Warna</td><td></td><td class="pad10"><input class="form-control" placeholder="Warna" type="text" name="warna" value="{WARNA}" maxlength=50></td>
						    </tr>
								<tr>
						      <td class="td-title"><u>Jum. Kursi</u></td><td></td><td class="pad10"><select  class="form-control" id="layoutkursi" name="layoutkursi">{KURSI}</select></td>
						    </tr>
								<tr>
						      <td class="td-title"><u>Sopir 1</u></td><td></td><td class="pad10"><select  class="form-control" id='sopir1' name='sopir1'>{SOPIR1}</select></td>
						    </tr>
								<tr>
						      <td class="td-title">Sopir 2</td><td></td><td class="pad10"><select  class="form-control" id='sopir2' name='sopir2'>{SOPIR2}</select></td>
						    </tr>
								<tr><td colspan=3 height=40 valign='bottom'><h3><u>Data surat-surat kendaraan</u></h3></td></tr>
								<tr>
						      <td class="td-title">No STNK</td><td></td><td class="pad10"><input placeholder="No. STNK" class="form-control"  type="text" id='no_stnk' name="no_stnk" value="{NO_STNK}" maxlength=50 ></td>
						    </tr>
								<tr>
						      <td class="td-title">No Mesin</td><td></td><td class="pad10"><input class="form-control" placeholder="No. Mesin"  type="text" id="no_mesin" name="no_mesin" value="{NO_MESIN}" maxlength=50></td>
						    </tr>
								<tr>
						      <td class="td-title">No Rangka</td><td></td><td class="pad10"><input class="form-control" placeholder="No. Rangka"  type="text" name="no_rangka" value="{NO_RANGKA}" maxlength=50></td>
						    </tr>
								<tr>
						      <td class="td-title">No BPKB</td><td></td><td class="pad10"><input class="form-control" placeholder="No. BPKB" type="text" name="no_bpkb" value="{NO_BPKB}" maxlength=50></td>
						    </tr>
							</table>
						</td>
						<td width=1 bgcolor='D0D0D0'></td>
						<td align='center' valign='top' style="padding-top: 70px;">
							<table width='400'>   
								<tr><td colspan=3><h3><u>Data Operasional</u></h3></td></tr>
								<tr>
						      <td class="td-title">KM terakhir</td><td></td><td class="pad10"><input class="form-control"  placeholder="KM Terakhir" type="text" id="km" name="km" value="{KM}" maxlength=10 onChange="Element.hide('km_invalid');"/><span id='km_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
						    </tr>
								<tr>
									<td class="td-title">Kepemilikan Cabang</td><td></td>
									<td class="pad10">
										<select class="form-control" id='cabang' name='cabang'>
											{OPT_CABANG}
										</select>
									</td>
								</tr>
								<tr>
						      <td class="td-title">Status Aktif</td><td></td>
									<td class="pad10">
										<select class="form-control" id="aktif" name="aktif">
											<option value=1 {AKTIF_1}>AKTIF</option>
											<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
										</select>
									</td>
						    </tr>
							</table>
						</td>
					</tr>           
				</table>
				</td>
			</tr>
			<tr>
						<td align='center' valign='middle' style="padding-bottom: 50px;">
						<input type="hidden" name="mode" value="{MODE}">
					  	<input type="hidden" name="submode" value="{SUB}">
						<div class="col-md-6 col-md-offset-3">
							<div class="col-md-6">
								<input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI" style="width:100px;">
							</div>
							<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
						</div>
					</td>
					</tr> 
			</table>
			</form>
		</div>
	</div>
</div>