<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
  <meta http-equiv="Content-Style-Type" content="text/css">
	<link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <link rel="stylesheet" href="{TPL}trav.css" type="text/css" />
  <script language="javaScript" type="text/javascript" src="includes/calendar.js"></script>
  <link href="includes/calendar.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">

  /*var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25961668-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/

</script>
</head>  
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/prototip.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/effects.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/controls.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/scriptaculous.js?load=effects"></script>
<!--<script type='text/javascript' src='{ROOT}js/scriptaculous.js?load=effects'></script>-->
<link rel="stylesheet" type="text/css" href="{ROOT}css/prototip.css" />
<script type="text/javascript" src="{ROOT}ajax/dojo.js"></script>
<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script> 

<link rel="stylesheet" type="text/css" href="{ROOT}css/style.css" />
<link rel="stylesheet" type="text/css" href="{ROOT}css/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="{ROOT}css/bootstrap.css" />
<script type="text/javascript" src="{ROOT}js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="{ROOT}js/bootstrap.js"></script>
<script type="text/javascript">
	(function($){
		$(function(){
	        jQuery.noConflict();
	    });

	})(jQuery)
</script>
<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,600,700&amp;subset=latin,latin-ext" type="text/css" rel="stylesheet" /> -->
<style>
  body {
    font-family: 'Open Sans', mono, 'ProximaNova', 'Helvetica Neue', helvetica, arial, sans-serif;
    font-size: 14px;
  }
</style>

<body>

<!--BEGIN popuploading -->
<div dojoType="dialog" id="popuploading" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table width="300" cellpadding="0" cellspacing="0" >
	<tr><td align="center" valign="middle" style="padding-top: 10px;">
		<font style="color: white;font-size: 12px;">sedang mengerjakan</font><br>
		<img src="{TPL}/images/loading_bar.gif" />
	</td></tr>
</table>
<br>
</div>
<!--END popuploading-->
	
<!-- BEGIN if_login -->

<!-- Static navbar -->
<nav class="navbar navbar-default">
<div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <!-- <a class="navbar-brand" href="https://www.tiketux.com/"><img src="{ROOT}/img/ic_logo_login.png" style="max-height: 23px; margin-top: 0;" /></a> -->
    <a class="navbar-brand" href="menu_operasional.php?sid={SID}"><img src="{ROOT}/img/logo-haitrans-nav-small.png" style="max-height: 20px; margin-top: 0;"></a>
  </div>
  <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
      <!-- <li class="active"><a href="#">Home</a></li> -->
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a>Cabang Login: {CABANG_LOGIN}</a></li>
      <li><a href="{U_UBAH_PASSWORD}"><i class="fa fa-pencil"></i> Ubah Password</a></li>
      <li><a href="{U_LOGOUT}">Logout <i class="fa fa-sign-out"></i></a></li>
    </ul>

  </div><!--/.nav-collapse -->
</div><!--/.container-fluid -->
</nav>

<div class="col-md-12" style="padding-top: 20px; text-align: left;">
	{BCRUMP}
</div>

<div align='center'>
<table style='margin-top: 10px;' width="100%" cellpadding=0 cellspacing=0>
<tr><td>

<!-- END if_login -->

<!-- MENU -->
<div align='center'>
<!-- BEGIN if_menu_utama --> 
<form name='form_pilih_menu' method='post'>
<!-- END if_menu_utama -->
<input id='menu_dipilih' name='menu_dipilih' type='hidden' value='{MENU_DIPILIH}' />
<input id='top_menu_dipilih' name='top_menu_dipilih' type='hidden' value='{TOP_MENU_DIPILIH}' />
<input id='hdn_SID' name='hdn_SID' type='hidden' value='{SID}' />

<table cellspacing='0' cellpadding='1'>
	<tr valign='bottom' height='35'>
		<!-- BEGIN menu_operasional -->
		<td>
			<div id='top_menu_operasional' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >
			<i class="fa fa-briefcase"></i>
			&nbsp;Operasional&nbsp;</div>
		</td>
		<!-- END menu_operasional -->
		
		<!-- BEGIN menu_laporan_reservasi -->
		<td>
			<div id='top_menu_lap_reservasi' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;" >
			<i class="fa fa-user"></i>
			&nbsp;Sales&nbsp;</div>
		</td>
		<!-- END menu_laporan_reservasi -->
		
		<!-- BEGIN menu_laporan_paket -->
		<td>
			<div id='top_menu_lap_paket' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  style="font-size: 12px;">
			<i class="fa fa-archive"></i>
			&nbsp;Paket & Cargo&nbsp;</div>
			</td>
		<!-- END menu_laporan_paket -->
		
		<!-- BEGIN menu_laporan_keuangan -->
		<td>
			<div id='top_menu_lap_keuangan' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >
			<i class="fa fa-money"></i>
			&nbsp;Keuangan&nbsp;</div>
			</td>
		<!-- END menu_laporan_keuangan -->
		
		<!-- BEGIN menu_tiketux -->
		<td>
			<div id='top_menu_tiketux' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >
			<i class="fa fa-ticket"></i>
			&nbsp;Tiketux&nbsp;</div>
			</td>
		<!-- END menu_tiketux -->
	
		<!-- BEGIN menu_pengelolaan_member -->
		<td>
			<div id='top_menu_pengelolaan_member' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >
			<i class="fa fa-users"></i>
			&nbsp;Member&nbsp;</div>
			</td>
		<!-- END menu_pengelolaan_member -->
		
		<!-- BEGIN menu_pengaturan -->
		<td>
			<div id='top_menu_pengaturan' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >
			<i class="fa fa-cog"></i>
			&nbsp;Pengaturan&nbsp;</div>
			</td>
		<!-- END menu_pengaturan -->
	</tr>
	
</table>