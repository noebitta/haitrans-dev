<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script language="JavaScript">
dojo.require("dojo.widget.Dialog");

function SetBlank(){
			kode_promo.value					='';
			nama_promo_poin.value			='';
			poin.value								='';			
}

function ValidasiAngka(objek){
	temp_nilai=objek.value*0;
	nama_objek=objek.name;
	
	if(temp_nilai!=0){
		alert(nama_objek+" harus angka!");
		objek.setFocus;exit;
	}
	
}

function SimpanPromo(){
  
	
	//memeriksa apakah biaya sudah valid(berisi angka)
	
  if(aksi.value=='Penambahan'){
		//jika id_member kosong, berarti adalah proses penambahan data member baru
		mode="tambah_promo";
	}
	else{
		//jika id_member tidak kosong, berarti adalah proses pengubahan data member
		mode="ubah_promo";
	}

	//ValidasiAngka(poin);

	new Ajax.Request("member_promo_poin_detail.php?sid={SID}", 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"id_promo_poin="+id_promo.value+
			"&kode_promo="+kode_promo.value+
			"&nama_promo_poin="+nama_promo_poin.value+
			"&poin="+poin.value+
			"&mode="+mode,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
		},
    onSuccess: function(request) 
    {
      if(request.responseText=='berhasil'){
				if(aksi.value=='Penambahan'){
					SetBlank();
					alert("Data Promo berhasil ditambahkan!");
				 //alert(request.responseText);
				}
				else{
					//kembali ke halaman member
					alert("Data Promo berhasil disimpan!");
					document.location="member_promo_poin.php?sid="+sid.value;
				}
			}
			else{
				alert("Terjadi kesalahan");
			}

		},
    onFailure: function(request) 
    {
       
    }
  })      
}

// global
var DataPromoBtnSimpan,DataPromoBtnNo,id_promo;

function init(e) {
  // inisialisasi variabel
	
	DataPromoBtnSimpan 	= document.getElementById("DataPromoBtnSimpan");
	DataPromoBtnNo 			= document.getElementById("DataPromoBtnNo");
	
  aksi								=document.getElementById("aksi");
	id_promo						=document.getElementById("hdn_id_promo_poin");
	kode_promo					=document.getElementById("kode_promo");
	nama_promo_poin			=document.getElementById("nama_promo_poin");
	poin								=document.getElementById("poin");
}

dojo.addOnLoad(init);

</script>
<input type='hidden' name='sid' id='sid' value='{SID}'>
<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer" >{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
		<!-- BODY -->
		<font><h2><u>{PESAN} Data Promo Poin</u></h2></font>
		
		<input type='hidden' name='aksi' id='aksi' value='{PESAN}'>
		<input type='hidden' name='hdn_id_promo_poin' id='hdn_id_promo_poin' value='{ID_PROMO_POIN}'>
		<form id='f_data_promo' name='f_data_promo' method="post">		
		<table bgcolor='FFFFFF' width='50%'> 
			<tr>
			  <td class='kolomwajib' width='30%'>Kode Promo</td>
				<td width='1%'>:</td>
				<td width='69%'>
					<input type='text' id='kode_promo' value='{KODE_PROMO}' />
				</td>
			</tr>
			<tr>
			  <td>Nama promo poin</td>
				<td>:</td>
				<td><input name="nama_promo_poin" id="nama_promo_poin" type="text" value='{NAMA_PROMO_POIN}'></td>
			</tr>
			<tr>
			  <td>Poin</td>
				<td>:</td>
				<td><input name="poin" id="poin" type="text" maxlength='3' value='{POIN}'></td>
			</tr>
			<tr>
			  <td>Dibuat oleh</td>
				<td>:</td>
				<td><label>{DIBUAT_OLEH}</label></td>
			</tr>
			<tr>
			  <td>Dibuat pada</td>
				<td>:</td>
				<td><label>{WAKTU_DIBUAT}</label></td>
			</tr>
			<tr>
			  <td>Data diubah oleh</td>
				<td>:</td>
				<td><label>{DIUBAH_OLEH}</label></td>
			</tr>
			<tr>
			  <td>Diubah pada</td>
				<td>:</td>
				<td><label>{WAKTU_DIUBAH}</label></td>
			</tr>
			<tr>
				<td align="center" colspan='3' bgcolor='C0C0C0' valign='middle' height='40'>
					<input type="button" id="DataPromoBtnSimpan" onClick="SimpanPromo();" value="&nbsp;Simpan&nbsp">
					<input type="button" onClick="javascript:document.location='member_promo.php?sid={SID}';" id="DataPromoBtnNo" value="&nbsp;Batal&nbsp;">
				</td>
			</tr>
		</table><!--END tabel 2 kolom-->
		</form>
	</td>
</tr>
</table>