<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('nama_invalid');
	Element.hide('telp_invalid');
	Element.hide('fax_invalid');
	
	kode			= document.getElementById('kode');
	nama			= document.getElementById('nama');
	telp			= document.getElementById('telp');
	fax				= document.getElementById('fax');
		
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(!cekValue(telp.value)){	
		valid=false;
		Element.show('telp_invalid');
	}
	
	if(!cekValue(fax.value)){	
		valid=false;
		Element.show('fax_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>
<div class="container">
<div class="row">
	<div class="col-md-12 box">
		<div class="bannerjudul judulhalaman"></div>
		<form name="frm_data_mobil" action="{U_ADD}" method="post" onSubmit='return validateInput();'>
			<table width="100%" class="table" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td class="whiter" valign="middle" align="center">
				<table width='800'>
					<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center' class="alert">{PESAN}</td></tr>
					<tr>
						<td align='center' valign='top' width='400'>
							<table width='400'>   
								<tr>
									<td colspan="3" class="judulhalaman orange mar10"><h3>{JUDUL}</h3></td>
								</tr>
								<tr>
							      <input type="hidden" name="kode_old" value="{KODE_OLD}">
										<td class="mytd" width='200'><h5>Kode Cabang*</h5></td><td></td>
										<td class="mytd">
											<input class="form-control" type="text" id="kode" placeholder="Kode Cabang" name="kode" value="{KODE}" maxlength=50 onChange="Element.hide('kode_invalid');" required>
											<span id='kode_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
										</td>
							    </tr>
								<tr>
						      	<td class="mytd"><h5>Nama*</h5></td><td></td>
									<td class="mytd">
										<input class="form-control" type="text" id="nama" placeholder="Nama" name="nama" value="{NAMA}" maxlength=100 onChange="Element.hide('nama_invalid');" required>
										<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    	</tr>
								<tr>
							      <td  class="mytd" valign='top'><h5>Alamat</h5></td><td  valign='top'></td><td><textarea class="form-control" placeholder="Alamat" name="alamat" id="alamat" cols="30" rows="3"  maxlength=150>{ALAMAT}</textarea></td>
							    </tr>
								<tr>
						      		<td class="mytd"><h5>Kota</h5></td><td></td>
									<td class="mytd"><select class="form-control" style="margin-top: 10px;" id='kota' placeholder="" name='kota'>{OPT_KOTA}</select></td>
						    	</tr>
								<tr>
						      	<td class="mytd"><h5>Telepon</h5></td><td></td>
									<td class="mytd">
										<input type="text" class="form-control" id="telp" placeholder="" name="telp" value="{TELP}" maxlength=50 onChange="Element.hide('telp_invalid');">
										<span id='telp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="mytd"><h5>Fax</h5></td><td></td>
									<td class="mytd">
										<input type="text" class="form-control" id="fax" placeholder="" name="fax" value="{FAX}" maxlength=50 onChange="Element.hide('fax_invalid');">
										<span id='fax_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
									</td>
						    </tr>
								<tr>
						      <td class="mytd"><h5>Tipe Cabang</h5></td><td></td>
									<td class="mytd"><select class="form-control" id='tipe_cabang' placeholder=""  name='tipe_cabang'>{OPT_TIPE_CABANG}</select></td>
						    </tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan=3 align='center' valign='middle'>
							<input type="hidden" name="mode" value="{MODE}">
						  <input type="hidden" name="submode" value="{SUB}">
							<div class="col-md-4 col-md-offset-4">
								<div class="col-md-6"><input type="button" style="margin: 0 auto; width: 100%;" class="mybutton" onClick="javascript: history.back();" value="KEMBALI">&nbsp;&nbsp;&nbsp;</div>
								<div class="col-md-6"><input type="submit" style="margin: 0 auto; width: 100%;" class="mybutton" name="submit" value="SIMPAN"></div>
							</div>
						</td>
					</tr>            
				</table>
				</td>
			</tr>
			</table>
			</form>
	</div>
</div>
</div>