<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
		// komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function selectAll(){
			
			i=1;
			loop=true;
			record_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					chk.checked=true;
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
			
	}
	
	function deselectAll(){
			
			i=1;
			loop=true;
			record_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					chk.checked=false;
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
			
	}

	function toggleSelect(){
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				if(chk.checked==true){
					chk.checked=false;
				}
				else{
					chk.checked=true;
				}
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
	}

	
	function hapusData(kode){
		
		if(confirm("Apakah anda yakin akan menghapus data ini?")){
			
			if(kode!=''){
				list_dipilih="'"+kode+"'";
			}
			else{
				i=1;
				loop=true;
				list_dipilih="";
				do{
					str_var='checked_'+i;
					if(chk=document.getElementById(str_var)){
						if(chk.checked){
							if(list_dipilih==""){
								list_dipilih +=chk.value;
							}
							else{
								list_dipilih +=","+chk.value;
							}
						}
					}
					else{
						loop=false;
					}
					i++;
				}while(loop);
			}
				
			new Ajax.Request("pengaturan_cabang.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=delete&list_cabang="+list_dipilih,
			 onLoading: function(request) 
			 {
			 },
			 onComplete: function(request) 
			 {
				
			 },
			 onSuccess: function(request) 
			 {			
				window.location.reload();
				deselectAll();
			},
			 onFailure: function(request) 
			 {
			 }
			})  
		}
		
		return false;
			
	}
		
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
		
	}
	
	function init(e){
		setSortId();
	}
	
	dojo.addOnLoad(init);
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table class="table table-hover" cellspacing="0" cellpadding="0" border="0">
				<tr>
				 <td class="whiter" valign="middle" align="center">		
						<table width='100%' cellspacing="0">
							<tr class='' height="">
								<td align='center' valign='middle' class="bannerjudul">Master Cabang </td>
								<td colspan=2 align='right' class="bannernormal" valign='middle'>
									<form action="{ACTION_CARI}" method="post">
										<div class="input-group">
									      <input type="text" class="form-control" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
									      <span class="input-group-btn">
									        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
									      </span>
									    </div><!-- /input-group -->
									</form>
								</td>
							</tr>
							<tr>
								<td class="mytd" align='left'>
									<a href="{U_CABANG_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
									<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
								<td class="mytd" width='70%' align='right'>
									{PAGING}
								</td>
							</tr>
						</table>
						<table width='100%' class="border table table-hover table-bordered">
						    <tr>
						       <th width="10"><input type='checkbox' onclick="toggleSelect();" id='ceker' /></th>
						       <th >No</th>
									 <th ><a class="th" id="sort1" href='#'>Kode</a></th>
									 <th ><a class="th" id="sort2" href='#'>Nama cabang</a></th>
									 <th ><a class="th" id="sort3" href='#'>Alamat</a></th>
									 <th ><a class="th" id="sort4" href='#'>Kota</a></th>
									 <th ><a class="th" id="sort5" href='#'>Telp</a></th>
									 <th ><a class="th" id="sort6" href='#'>Fax</a></th>
									 <th ><a class="th" id="sort7" href='#'>Tipe</a></th>
									 <th class="center">Action</th>
						     </tr>
						     <!-- BEGIN ROW -->
						     <tr class="{ROW.odd}">
						       <td><div align="center">{ROW.check}</div></td>
						       <td><div align="center">{ROW.no}</div></td>
						       <td><div align="left">{ROW.kode}</div></td>
									 <td><div align="left">{ROW.nama}</div></td>
						       <td><div align="left">{ROW.alamat}</div></td>
									 <td><div align="left">{ROW.kota}</div></td>
									 <td><div align="left">{ROW.telp}</div></td>
									 <td><div align="left">{ROW.fax}</div></td>
									 <td><div align="left">{ROW.tipe_cabang}</div></td>
						       <td><div align="left">{ROW.action}</div></td>
						     </tr>  
						     <!-- END ROW -->
								 {NO_DATA}
						    </table>
					    	<table class="table" width="100%">
								<tr>
									<td class="mytd" align='left'>
										<a href="{U_CABANG_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
										<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
									<td class="mytd" width='70%' align='right'>
										{PAGING}
									</td>
								</tr>
							</table>
				 </td>
				</tr>
				</table>
		</div>
	</div>
</div>