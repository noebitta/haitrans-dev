<div id="resvdetailbookwrapper">
	<input type='hidden' name='no_tiket' id='no_tiket' value='{NO_TIKET}'>
	<input type='hidden' name='kode_booking' id='kode_booking' value='{KODE_BOOKING}'>
	<input type='hidden' name='cetak_tiket' id='cetak_tiket' value='{CETAK_TIKET}'>
	<input type='hidden' name='kursi' id='kursi' value='{KURSI}'>

	<table class="table">
		<tr><td style="padding: 0;"><h3>Kursi: {KURSI}</h3></td><td style="padding: 0;" class="pull-right"><h3>({KODE_JADWAL})</h3></td></tr>
	</table>
	
	<div id="resvdetailbookcontent">
		<h4>PENUMPANG {PENUMPANG_KE}</h4>
		<table class="table">
			<tr><td>Kode Booking</td><td><span class="" style="color:2196FD; font-size: 18px; font-weight: bold;">{KODE_BOOKING}</span></td></tr>
			<tr><td>No. Tiket</td><td><span class="">{NO_TIKET}</span></td></tr>
			<!-- BEGIN showpaymentcode -->
			<tr><td>Kode Payment</td><td><span class="">{PAYMENT_CODE}</span></td></tr>
			<!-- END showpaymentcode -->
			<tr><td valign="middle">Telepon</td><td><span class=""><input class="form-control" type="text" id="ubah_telp_penumpang" value="{TELP}" onkeypress="validasiNoTelp(event);" onFocus="this.style.background='white';" /></span></td></tr>
			<tr><td>Nama</td><td><span class=""><input class="form-control" type="text" id="ubah_nama_penumpang" value="{NAMA}" onFocus="this.style.background='white';" /></span></td></tr>
			<tr><td>Penumpang</td><td><span class="">
				<!-- BEGIN showlistdiscount -->
				<select  class='form-control' id='id_discount'>{LIST_DISCOUNT}</select>
				<!-- END showlistdiscount -->
				
				<!-- BEGIN showpromoberlaku -->
				{LIST_DISCOUNT}
				<!-- END showpromoberlaku -->
			</span></td></tr>
			<tr><td>Keterangan</td><td><span class="">
				<textarea class="form-control" id="ubah_alamat_penumpang" cols="30" rows="2">{KETERANGAN}</textarea>
			</span></td></tr>
			<tr><td colspan="2">
				<!-- BEGIN tombolsimpan -->
				<div class="pull-right"><input class="btn mybutton" style="padding: 10px; width: 100px;" type="button" value="Simpan" onClick="ubahDataPenumpang('{NO_TIKET}');" /></div>
				<!-- END tombolsimpan -->
			</td></tr>
		</table>
		
		
		<hr noshade>
		<h4>TARIF</h4>
		<span class="resvdetailbooklabel">Harga tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{HARGA_TIKET}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{DISCOUNT}</span></br>	
		<span class="resvdetailbooklabel">Total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{TOTAL}</b></span></br>	
		
		<!-- BEGIN showalltarif -->
		<hr noshade>
		<h4>KESELURUHAN TARIF</h4>
		<span class="resvdetailbooklabel">Total tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">{TOTAL_TIKET}</span></br>	
		<span class="resvdetailbooklabel">Sub total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{SUB_TOTAL}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{TOTAL_DISCOUNT}</span></br>	
		<span class="resvdetailbooklabel">Total bayar</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{TOTAL_BAYAR}</b></span></br>	
		<!-- END showalltarif -->
		
		<!-- BEGIN showdatapembayaran -->
		<hr noshade>
		<h4>TIKET SUDAH DIBAYAR</h4>
		<span class="resvdetailbooklabel">Total tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">{showdatapembayaran.TOTAL_TIKET_DIBAYAR}</span></br>	
		<span class="resvdetailbooklabel">Sub total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.SUB_TOTAL_DIBAYAR}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.TOTAL_DISCOUNT_DIBAYAR}</span></br>	
		<span class="resvdetailbooklabel">Total Dibayar</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{showdatapembayaran.TOTAL_DIBAYAR}</b></span></br>
		
		<hr noshade>
		<h4>TIKET BELUM DIBAYAR</h4>
		<span class="resvdetailbooklabel">Total tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">{showdatapembayaran.TOTAL_TIKET_UTANG}</span></br>	
		<span class="resvdetailbooklabel">Sub total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.SUB_TOTAL_UTANG}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.TOTAL_DISCOUNT_UTANG}</span></br>	
		<span class="resvdetailbooklabel">Total Utang</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{showdatapembayaran.TOTAL_UTANG}</b></span></br>	
		<!-- END showdatapembayaran -->
		
		<hr noshade>
		<h4>INFO</h4>
		<span class="resvdetailbooklabel">CSO pemesan</span><span class="resvdetailbookfield">: <b>{CSO_PEMESAN}</b></span></br>
		<span class="resvdetailbooklabel">Waktu Pesan</span><span class="resvdetailbookfield">: {WAKTU_PESAN}</span></br>
		<span class="resvdetailbooklabel">Status Pesanan</span><span class="resvdetailbookfield">: <b>BELUM DIBAYAR</b></span></br>
		
		<!-- BEGIN infomutasi -->
		<hr noshade>
		<h4>MUTASI</h4>
		<span class="resvdetailbooklabel">Waktu mutasi</span><span class="resvdetailbookfield">: {WAKTU_MUTASI}</span></br>	
		<span class="resvdetailbooklabel">Dimutasi dari</span><span class="resvdetailbookfield">: {DIMUTASI_DARI}</span></br>	
		<span class="resvdetailbooklabel">Dimutasi oleh</span><span class="resvdetailbookfield">: {DIMUTASI_OLEH}</span></br>	
		<!-- END infomutasi -->
		
		<div style="text-align: center;">
			<hr noshade>
			
			<!-- BEGIN showtombol -->
			<div style="text-align: center; display: inline-block; margin: 0 auto;">
			<div class="menuiconsmall"  style="margin: 0 auto;"><a href="#" onclick="isgoshow.value=0;kodebookingdicetak.value='{KODE_BOOKING}';cetaksemuatiket.value=1;dialog_pembayaran.show();">
					<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" xml:space="preserve">
						<g id="Layer_38">
							<g>
								<g>
									<path d="M574.834,144.468h-14.131V16.672C560.703,7.465,553.238,0,544.03,0H157.834c-9.213,0-16.672,7.465-16.672,16.672v127.795
										h-14.139C71.876,144.466,27,189.336,27,244.489v206.51c0,55.154,44.876,100.021,100.023,100.021h14.139V681.03
										c0,9.208,7.458,16.672,16.672,16.672H544.03c9.208,0,16.672-7.465,16.672-16.672V551.021h14.131
										c55.147,0,100.023-44.867,100.023-100.021V244.49C674.857,189.336,629.981,144.468,574.834,144.468z M174.504,33.342h352.857
										v111.126H174.504V33.342z M527.361,664.364H174.504V458.757h352.857V664.364z M641.518,450.999
										c0,36.772-29.913,66.682-66.684,66.682h-14.131v-58.925h36.055c9.213,0,16.67-7.464,16.67-16.672
										c0-9.205-7.457-16.67-16.67-16.67h-52.726H157.834h-47.789c-9.206,0-16.672,7.465-16.672,16.67
										c0,9.209,7.465,16.672,16.672,16.672h31.117v58.925h-14.139c-36.769,0-66.682-29.913-66.682-66.682V244.49
										c0-36.769,29.913-66.681,66.682-66.681h30.809h386.195h30.807c36.768,0,66.681,29.913,66.681,66.681v206.509L641.518,450.999
										L641.518,450.999z"/>
									<path d="M544.03,244.286c-12.556,0-22.751,10.191-22.751,22.751c0,12.56,10.191,22.751,22.751,22.751
										c12.562,0,22.754-10.19,22.754-22.751C566.782,254.477,556.592,244.286,544.03,244.286z"/>
									<path d="M213.397,516.403h275.064c9.208,0,16.672-7.463,16.672-16.672c0-9.205-7.465-16.67-16.672-16.67H213.397
										c-9.206,0-16.672,7.465-16.672,16.67C196.726,508.941,204.191,516.403,213.397,516.403z"/>
									<path d="M213.397,578.234h275.064c9.208,0,16.672-7.468,16.672-16.672c0-9.208-7.465-16.67-16.672-16.67H213.397
										c-9.206,0-16.672,7.463-16.672,16.67C196.726,570.77,204.191,578.234,213.397,578.234z"/>
								</g>
							</g>
						</g>
					</svg>
				<br>Cetak Semua Tiket</a>
			</div>

			<div class="menuiconsmall"  style="margin: 0 auto;"><a href="#" onclick="{BATAL_ACTION};">
				<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" xml:space="preserve">
					<g id="Layer_39">
						<g>
							<path d="M637.106,46.475H63.067c-32.137,0-58.282,26.145-58.282,58.282v340.171c0,32.137,26.145,58.281,58.282,58.281h212.607
								c8.69,0,15.76-7.07,15.76-15.76c0-8.69-7.07-15.76-15.76-15.76H63.067c-14.756,0-26.761-12.005-26.761-26.761V104.757
								c0-14.756,12.005-26.761,26.761-26.761h574.039c14.756,0,26.761,12.005,26.761,26.761v340.171c0,8.69,7.07,15.76,15.76,15.76
								s15.76-7.07,15.76-15.76V104.757C695.388,72.621,669.243,46.475,637.106,46.475z"/>
							<path d="M286.305,397.277H94.958c-8.69,0-15.76,7.07-15.76,15.761c0,8.69,7.07,15.76,15.76,15.76h191.347
								c8.69,0,15.761-7.07,15.761-15.76C302.065,404.347,294.995,397.277,286.305,397.277z"/>
							<path d="M286.305,348.788H94.958c-8.69,0-15.76,7.07-15.76,15.76c0,8.69,7.07,15.76,15.76,15.76h191.347
								c8.69,0,15.761-7.07,15.761-15.76C302.065,355.858,294.995,348.788,286.305,348.788z"/>
							<path d="M562.693,258.712c32.137,0,58.282-26.145,58.282-58.282s-26.146-58.282-58.282-58.282h-63.782
								c-32.137,0-58.282,26.145-58.282,58.282s26.146,58.282,58.282,58.282H562.693z M562.693,227.191h-63.782
								c-14.756,0-26.761-12.005-26.761-26.761s12.005-26.761,26.761-26.761h63.782c14.756,0,26.761,12.005,26.761,26.761
								S577.45,227.191,562.693,227.191z"/>
							<path d="M477.651,312.234c-96.614,0-175.216,78.602-175.216,175.216s78.602,175.215,175.216,175.215
								s175.215-78.602,175.215-175.215S574.265,312.234,477.651,312.234z M621.345,487.45c0,79.233-64.461,143.695-143.694,143.695
								S333.956,566.683,333.956,487.45s64.462-143.694,143.695-143.694S621.345,408.217,621.345,487.45z"/>
							<path d="M567.823,413.038c0-4.211-1.64-8.169-4.617-11.144c-2.977-2.978-6.935-4.617-11.144-4.617
								c-4.21,0-8.168,1.639-11.144,4.617l-63.268,63.267l-63.269-63.267c-2.977-2.978-6.935-4.617-11.144-4.617
								c-4.21,0-8.168,1.639-11.144,4.617c-2.977,2.975-4.617,6.934-4.617,11.144c0,4.21,1.64,8.167,4.617,11.143l63.268,63.269
								l-63.268,63.269c-2.977,2.975-4.617,6.934-4.617,11.144c0,4.21,1.64,8.167,4.617,11.143c2.977,2.978,6.935,4.618,11.144,4.618
								c4.21,0,8.168-1.64,11.144-4.618l63.269-63.267l63.268,63.267c2.977,2.978,6.935,4.618,11.144,4.618
								c4.21,0,8.169-1.64,11.144-4.617c2.978-2.977,4.617-6.935,4.617-11.145s-1.64-8.168-4.617-11.143l-63.268-63.269l63.267-63.268
								C566.183,421.206,567.823,417.248,567.823,413.038z"/>
						</g>
					</g>
				</svg>
			<br>Batal</a></div>

			<div class="menuiconsmall"  style="margin: 0 auto;"><a href="#" onclick="{MUTASI_ACTION};">
					<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" xml:space="preserve">
						<g id="Layer_41">
							<g>
								<polygon points="225.884,445.646 195.582,415.34 145.835,468.014 93.307,415.483 63.007,445.783 115.537,498.312 62.865,548.059 
									93.168,578.361 145.839,528.616 195.724,578.5 226.022,548.201 176.139,498.316 		"/>
								<path d="M416.28,537.22V431.202L183.217,198.141h86.75v-43.144H109.776V315.19h43.143v-86.75l220.218,220.218v88.128
									c-33.741,8.887-58.525,40.48-58.525,75.792c0,44.913,35.182,80.095,80.095,80.095s80.095-35.182,80.095-80.095
									c0.001-16.957-6.693-34.146-18.85-48.398C445.43,551.841,431.493,542.401,416.28,537.22z M431.661,615.503
									c0,19.686-17.268,36.954-36.954,36.954c-19.685,0-36.953-17.268-36.953-36.954c0-17.914,12.951-36.954,36.953-36.954
									C418.71,578.549,431.661,597.588,431.661,615.503z"/>
								<polygon points="585.811,293.48 635.559,240.809 605.254,210.505 555.508,263.177 502.979,210.647 472.679,240.947 
									525.209,293.476 472.537,343.222 502.841,373.526 555.512,323.779 605.396,373.664 635.696,343.364 		"/>
								<polygon points="356.529,168.687 409.201,118.943 459.085,168.826 489.385,138.528 439.501,88.643 489.247,35.972 458.942,5.669 
									409.197,58.341 356.667,5.811 326.367,36.111 378.898,88.639 326.225,138.386 		"/>
							</g>
						</g>
					</svg>
				<br>Mutasi Penumpang</a></div>
			</div>
			<!-- END showtombol -->
		</div>
    <hr noshade>
	</div>
</div>

