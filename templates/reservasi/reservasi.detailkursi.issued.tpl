<div id="resvdetailbookwrapper">
	<input type='hidden' name='no_tiket' id='no_tiket' value='{NO_TIKET}'>
	<input type='hidden' name='kode_booking' id='kode_booking' value='{KODE_BOOKING}'>
	<input type='hidden' name='cetak_tiket' id='cetak_tiket' value='{CETAK_TIKET}'>
	<input type='hidden' name='kursi' id='kursi' value='{KURSI}'>
	
	<h1>Kursi: {KURSI}</h1>
	<h3>({KODE_JADWAL})</h3>
	
	<div id="resvdetailbookcontent">
		<h3>PENUMPANG {PENUMPANG_KE}</h3>
		<span class="resvdetailbooklabel">#Booking</span><span class="resvdetailbookfield" style="color:blue;"><b>: {KODE_BOOKING}</b></span></br>
		<span class="resvdetailbooklabel">#Tiket</span><span class="resvdetailbookfield"><b>: {NO_TIKET}</b></span></br>
		<!-- BEGIN showpaymentcode -->
		<span class="resvdetailbooklabel">Kode payment</span><span class="resvdetailbookfield"><b>: {PAYMENT_CODE}</b></span></br>
		<!-- END showpaymentcode -->
		<span class="resvdetailbooklabel">Telepon</span><span class="resvdetailbookfield">: {TELP}</span></br>
		<span class="resvdetailbooklabel">Nama</span><span class="resvdetailbookfield">: {NAMA}</span></br>
		<span class="resvdetailbooklabel">Penumpang</span><span class="resvdetailbookfield">: {PENUMPANG}</span></br>
		<span class="resvdetailbooklabel">Keterangan</span><span class="resvdetailbookfield">: {KETERANGAN}</span></br>
		
		<!-- BEGIN showmember -->
		<hr noshade>
		<h3>DATA MEMBER</h3>
		<span class="resvdetailbooklabel">Kode member</span><span class="resvdetailbookfield">: {KODE_MEMBER}</span></br>
		<span class="resvdetailbooklabel">Nama member</span><span class="resvdetailbookfield">: {NAMA_MEMBER}</span></br>
		<span class="resvdetailbooklabel">Telp member</span><span class="resvdetailbookfield">: {TELP_MEMBER}</span></br>
		<span class="resvdetailbooklabel">Tiket dibeli</span><span class="resvdetailbookfield">: {TOTAL_TIKET} Tiket</span></br>
		<span class="resvdetailbooklabel">Kode Voucher</span><span class="resvdetailbookfield">: {KODE_VOUCHER}</span></br>	
		<!-- END showmember -->
		
		<!-- BEGIN showtarif -->
		<hr noshade>
		<h3>TARIF</h3>
		<span class="resvdetailbooklabel">Harga tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{HARGA_TIKET}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{DISCOUNT}</span></br>	
		<span class="resvdetailbooklabel">Total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{TOTAL}</b></span></br>	
		<!-- END showtarif -->
		
		<!-- BEGIN showalltarif -->
		<hr noshade>
		<h3>KESELURUHAN TARIF</h3>
		<span class="resvdetailbooklabel">Total tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">{TOTAL_TIKET}</span></br>	
		<span class="resvdetailbooklabel">Sub total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{SUB_TOTAL}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{TOTAL_DISCOUNT}</span></br>	
		<span class="resvdetailbooklabel">Total bayar</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{TOTAL_BAYAR}</b></span></br>	
		<!-- END showalltarif -->
		
		<!-- BEGIN showdatapembayaran -->
		<hr noshade>
		<h3>TIKET SUDAH DIBAYAR</h3>
		<span class="resvdetailbooklabel">Total tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">{showdatapembayaran.TOTAL_TIKET_DIBAYAR}</span></br>	
		<span class="resvdetailbooklabel">Sub total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.SUB_TOTAL_DIBAYAR}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.TOTAL_DISCOUNT_DIBAYAR}</span></br>	
		<span class="resvdetailbooklabel">Total Dibayar</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{showdatapembayaran.TOTAL_DIBAYAR}</b></span></br>
		
		<hr noshade>
		<h3>TIKET BELUM DIBAYAR</h3>
		<span class="resvdetailbooklabel">Total tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">{showdatapembayaran.TOTAL_TIKET_UTANG}</span></br>	
		<span class="resvdetailbooklabel">Sub total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.SUB_TOTAL_UTANG}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{showdatapembayaran.TOTAL_DISCOUNT_UTANG}</span></br>	
		<span class="resvdetailbooklabel">Total Utang</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{showdatapembayaran.TOTAL_UTANG}</b></span></br>	
		<!-- END showdatapembayaran -->
		
		<hr noshade>
		<h3>INFO</h3>
		<span class="resvdetailbooklabel">CSO pemesan</span><span class="resvdetailbookfield">: <b>{CSO_PEMESAN}</b></span></br>
		<span class="resvdetailbooklabel">Waktu pesan</span><span class="resvdetailbookfield">: {WAKTU_PESAN}</span></br>
		<span class="resvdetailbooklabel">Status pesanan</span><span class="resvdetailbookfield">: <b>SUDAH DIBAYAR</b></span></br>
		<span class="resvdetailbooklabel">Waktu bayar</span><span class="resvdetailbookfield">: {WAKTU_BAYAR}</span></br>
		<span class="resvdetailbooklabel">CSO cetak tiket</span><span class="resvdetailbookfield">: <b>{CSO_CETAK_TIKET}</b></span></br>
		
		<!-- BEGIN infomutasi -->
		<hr noshade>
		<h3>MUTASI</h3>
		<span class="resvdetailbooklabel">Waktu mutasi</span><span class="resvdetailbookfield">: {WAKTU_MUTASI}</span></br>	
		<span class="resvdetailbooklabel">Dimutasi dari</span><span class="resvdetailbookfield">: {DIMUTASI_DARI}</span></br>	
		<span class="resvdetailbooklabel">Dimutasi oleh</span><span class="resvdetailbookfield">: {DIMUTASI_OLEH}</span></br>	
		<!-- END infomutasi -->
		
		<div style="text-align: center;">
			<hr noshade>
			<input type='button' onclick="{CETAK_TIKET_ACTION}" value="CETAK ULANG TIKET" style="width: 230px;height: 40px;font-size: 14px;font-weight: bold;" /><br><br>
			<!-- BEGIN showtombolbatal -->
			<input type='button' onclick="{BATAL_ACTION};" value="BATAL" style="width: {showtombolbatal.width}px;height: 20px;font-size: 14px;font-weight: bold;"/>{showtombolbatal.br}{showtombolbatal.br}
			<!-- END showtombolbatal -->
			<!-- showtombolkoreksidiskon
			&nbsp;&nbsp;<input type='button' onclick="tampilkanDialogDiscount('{NO_TIKET}','{JENIS_PENUMPANG}');" value="KOREKSI DISKON" style="width: 152px;height: 20px;font-size: 14px;font-weight: bold;" /><br><br>
			END showtombolkoreksidiskon -->
			<!-- BEGIN showtombolmutasi -->
			<input type='button' onclick="{MUTASI_ACTION};" id='btn_mutasi' value="MUTASI PENUMPANG" style="width: 230px;height: 20px;font-size: 14px;font-weight: bold;">
			<!-- END showtombolmutasi -->
		</div>
    <br><br>
    <hr noshade>
	</div>
</div>

