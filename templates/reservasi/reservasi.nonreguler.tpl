<SCRIPT LANGUAGE="JavaScript">

function Start(page) {
OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}
</SCRIPT>

<input type="hidden" value="{SID}" id="hdn_SID">
<input type="hidden" value="{HARGA_MINIMUM_PAKET}" id="hdn_harga_minimum_paket">

<input type="hidden" value=0 id="flag_mutasi">
<input type="hidden" value=0 id="flag_mutasi_paket">
<input type="hidden" value='' id="id_jurusan_aktif">
<input type="hidden" value='' id="jadwalaktif">

<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi.nonreguler.js"></script>

<!--dialog SPJ-->
<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td bgcolor='ffffff'>
		<table>
			<tr><td><h2>Cetak Manifest</h2></td></tr>
      <tr><td><div id="rewritedialogspj"></div></td></tr>
		</table>
		<span id='progress_dialog_spj' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog SPJ-->

<!--dialog keberangkatan pelanggan-->
<div dojoType="dialog" id="dialog_cari_keberangkatan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='700'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Keberangkatan</h2></td></tr>
			<tr><td><div id="rewrite_keberangkatan_pelanggan"></div></td></tr>
		</table>
		<span id='progress_cari_jadwal' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_jadwal_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog keberangkatan pelanggan-->

<!--dialog DISCOUNT-->
<div dojoType="dialog" id="dialog_discount" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Daftar Jenis Harga</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td>Silahkan pilih harga tiket</td><td>:</td><td><div id="rewrite_list_discount_dialog"></div><input type='hidden' id='hdn_bayar_no_tiket'/></td></tr>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='korek_disc_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='korek_disc_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_discount_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="koreksiDiscount(hdn_bayar_no_tiket.value);" id="dialog_discount_btn_OK" value="Simpan perubahan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog DISCOUNT-->

<!--dialog CETAK ULANG TIKET-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='cetak_ulang_tiket_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiket();" id="dialog_cetak_ulang_tiket_btn_OK" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog CETAK ULANG TIKET-->

<!--dialog OTP-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket_otp" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,silahkan masukkan kode OTP dari pelanggan</td></tr>
			<tr height=30><td>OTP</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password_otp' maxlength='6'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel_otp" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiketByOTP();" id="dialog_cetak_ulang_tiket_btn_OK_otp" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog OTP-->

<!--dialog KOREKSI ASURANSI-->
<div dojoType="dialog" id="dialog_koreksi_asuransi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Koreksi Asuransi</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3><div id="rewrite_list_plan_asuransi"></div><input type='hidden' id='hdn_no_tiket_koreksi_asuransi'/></td></tr>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='korek_asuransi_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='korek_asuransi_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_koreksi_asuransi_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="koreksiAsuransi(hdn_no_tiket_koreksi_asuransi.value);" id="dialog_koreksi_asuransi_btn_OK" value="Simpan perubahan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog KOREKSI ASURANSI-->

<!--dialog BATAL-->
<div dojoType="dialog" id="dialog_batal" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pembatalan Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='batal_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='batal_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_batal_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="batal(batal_no_tiket,batal_no_kursi);" id="dialog_batal_btn_OK" value="Batalkan Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog BATAL-->

<!--dialog MUTASI-->
<div dojoType="dialog" id="dialog_mutasi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Mutasi Kursi</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='mutasi_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='mutasi_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" onclick="dlgmutasi.hide()" value="Cancel" style="width: 110px;"> &nbsp;
		<input type="button" onclick="konfirmMutasi();" value="Proses" style="width: 110px;">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog MUTASI-->

<!--dialog Input Kode Voucher-->
<div dojoType="dialog" id="dialog_input_voucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Voucher</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Silahkan masukkan KODE VOUCHER</td></tr>
			<tr height=30><td>Kode Voucher</td><td>:</td><td><input type='password' id='input_kode_voucher'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_input_voucher_btn_ok_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="bayarByVoucher(input_kode_voucher.value);" id="dialog_input_voucher_btn_ok" value="   Bayar   ">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Input Kode Voucher-->

<!--dialog CETAK ULANG VOUCHER BBM-->
<div dojoType="dialog" id="dialogcetakulangvoucherbbm" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="300" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td align='center'>
		<input type="hidden" id="signature_bbm"/>
		<table bgcolor='white' width='100%'>
			<tr><td colspan='3'><h2>Cetak Ulang Voucher BBM</h2></td></tr>
			<tr height=30><td colspan=3 align="center">Untuk mencetak Manifest dan mencetak ulang voucher BBM,<br>minimal akses anda haruslah supervisor.<br>Silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='username_bbm'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='password_bbm'/></td></tr>
			<tr height=30><td>Alasan</td><td>:</td><td><textarea type='text' id='alasan_bbm' cols="40" rows="3"></textarea></td></tr>
		</table><br>
		<span id='loadingverifikasibbm' style='display:none;'><img src='./templates/images/loading.gif' />&nbsp;loading...</span>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="btncanceldialogcetakulangvoucherbbm" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="verifikasiCetakVoucherBBM();" value="Cetak Manifest">
	</td>
</tr>
</table>
</form>
</div>
<!--END CETAK ULANG VOUCHER BBM-->

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td align="center"><h2 style="color:#fff;">Pilih Jenis Pembayaran</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr><td>
				<br>
				<input type='hidden' id='isgoshow' value='' />
				<input type='hidden' id='notiketdicetak' value='' />
				<input type='hidden' id='kodebookingdicetak' value='' />
				<input type='hidden' id='kode_booking_go_show' value='' />
				<input type='hidden' id='no_tiket_goshow' value='' />
				<input type='hidden' id='cetaksemuatiket' value='1' />
				<table width='100%'>
					<tr>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(0);return false;"><img src="{TPL}images/icon_tunai.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(0);return false;"><span class="genmed">Tunai</span></a>    
						</td>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(1);return false;"><img src="{TPL}images/icon_debitcard.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(1);return false;"><span class="genmed">Debit Card</span></a> 
						</td>
					</tr>
					<tr>
						<td align='center' valign='middle'>
							<a href="" onClick="CetakTiket(2);return false;"><img src="{TPL}images/icon_mastercard.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(2);return false;"><span class="genmed">Credit Card</span></a>    
						</td>
						<td align='center' valign='middle'>
							<a href="" onClick="showInputVoucher();return false;"><img src="{TPL}images/icon_voucher.png" /></a>  
							<br />
							<a href="" onClick="showInputVoucher();return false;"><span class="genmed">Voucher</span></a>     
						</td>
					</tr>
				</table>
			</td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="reset" id="dialog_pembayaran_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Pilih Pembayaran-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="whiter" valign="middle" align="center">
 
<table width="100%" cellspacing="0" cellpadding="4" border="0">
<tr>
	<td height='80' width='100%'>
		<table width='100%' height='100%' cellspacing=0 cellpadding=0 class='header_reservasi'>
			<tr>	
				<td align='left' valign='middle' width='7%'  background='./templates/images/icon_cari.png' STYLE='background-repeat: no-repeat;background-position: left top;'></td>
				<td align='left' valign='middle' width='23%'>
					<font color='505050'><b>Cari keberangkatan</b></font><br>
					<input name="txt_cari_jadwal" id="txt_cari_jadwal" type="text">
					<input class='tombol' name="btn_periksajadwal" id="btn_periksajadwal" value="Cari" type="button" onClick="periksaJadwal(txt_cari_jadwal.value)"><br>	
					<font size=1 color='505050'>(masukkan no telepon pelanggan)</font>
        </td>
				<!--<td class='header_reservasi_separator'>&nbsp;</td>
				<td align='left' valign='middle' width='23%'>
					<font color='505050'>&nbsp;<b>Cari paket Xpress</b></font><br>
					&nbsp;<input name="txt_cari_paket" id="txt_cari_paket" type="text">
					&nbsp;<input class='tombol' name="btn_periksapaket" id="btn_periksapaket" value="Cari" type="button" onClick="periksaPaket(txt_cari_paket.value)"><br>	
					&nbsp;<font size=1 color='505050'>(masukkan no resi paket/ no.telp pelanggan )</font>
				</td>
				<td class='header_reservasi_separator'>&nbsp;</td>-->
				<td align='right' width='55%' valign='middle'>
					
					<div class="menuiconsmall"><a href="#" onClick="{U_UBAH_PASSWORD1}"><img src="{TPL}images/icon_password.png" /><br>Ubah<br>Password</a></div>
					<div class="notifikasi_pengumuman" id="rewritepengumuman"></div>
					<div class="menuiconsmall"><a href="#" onClick="{U_LAPORAN_PENJUALAN}"><img src="{TPL}images/icon_penjualan.png" /><br>Laporan<br>Penjualan</a></div>
					<div class="menuiconsmall"><a href="#" onClick="{U_LAPORAN_UANG}"><img src="{TPL}images/icon_penjualan_uang.png" /><br>Laporan<br>Uang</a></div>				
					<div class="menuiconsmall"><a href="#" onClick="{U_BA_SOPIR}"><img src="{TPL}images/icon_ba_insentif_sopir.png" /><br/>Bayar<br>Ins.Sopir</a></div>
					<div class="menuiconsmall"><a href="#" onClick="{U_BA_BOP}"><img src="{TPL}images/icon_bop_release.png" /><br/>Bayar BO<br>Tambahan</a></div>
			</tr>
		</table>
	</td>
</tr>
<tr>
 <td valign="middle" align="left">    
  <table width="100%" class='reservasi_background' cellpadding='0' cellspacing='0'>
		<tr>
			<td width="230" valign="top">       
				<table border="0" width="100%" cellpadding='0' cellspacing='0'>
					<tr><td colspan=2 class='formHeader'><strong>1. Jadwal</strong></td>
					</tr>
					<tr>
						<td colspan=2 align='center'>
							<b>Tanggal keberangkatan</b>
							<!--  kolom kalender -->
							<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
							</iframe>
		
							<form name="formTanggal">
								<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />			
							</form>
							<!--end kolom kalender-->


              <span class="rsvnonregwrapjadwal"">
                <span id="showloadingjadwal" class="rsv_loading_jadwal" style="display: none; position: absolute;padding-left:27px;padding-top:37px;">&nbsp;</span>
                <span style="display: inline-block; padding-top: 7px;padding-left: 32px;">Jadwal</span>
                <a href="#" onclick="reloadJadwal();return false;" class="b_rsv_reload_jadwal" title="reload jadwal" style="float: right;"></a>
              </span>
              <br>
              <input type="hidden" id="kotadipilih" name="kotadipilih" value="" />
              <input type="hidden" id="kodejadwaldipilih" name="kodejadwaldipilih" value="" />
              <div id="rewritelistjadwal" style="width: 170px"></div>
              <br>
						</td>
					</tr>
					<tr>
						<td colspan="2"><span class="noticeMessage">KETERANGAN:</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_kosong_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi masih kosong</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_pesan_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke POOL INI</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_konfirm_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke POOL ini</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_pesan1_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke pool PICK-UP</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_konfirm1_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke pool PICK-UP</span></td>
					</tr>
				</table>
				<!-- INFO SISA KURSI KOSONG 2 JAM BERIKUTNYA -->
				<!--<div class='border' align='center'><b>Sisa kursi di jadwal-jadwal berikutnya</b><br><hr>
				<div align='left' id="rewrite_sisa_kursi"></div>
				</div>-->
				<!-- info tweater-->
				<!--<script src='http://widgets.twimg.com/j/2/widget.js'></script>
				<script>
				new TWTR.Widget({
				  version: 2,
				  type: 'profile',
				  rpp: 7,
				  interval: 45000,
				  width: 250,
				  height: 300,
				  theme: {
				    shell: {
				      background: '#e64017',
				      color: '#ffffff'
				    },
				    tweets: {
				      background: '#fad419',
				      color: '#6e6e6e',
				      links: '#0753eb'
				    }
				  },
				  features: {
				    scrollbar: true,
				    loop: true,
				    live: true,
				    hashtags: true,
				    timestamp: true,
				    avatars: false,
				    behavior: 'default'
				  }
				}).render().setUser('@TMCPoldaMetro').start();
				</script>
				<!--end info teater-->
			</td>
			<td width=1 bgcolor='e0e0e0'></td>
			<td valign='top' width='400'>
				<!--LAYOUT KURSI DAN ISIAN DATA PENUMPANG-->
				<table width='100%' cellpadding='0' cellspacing='0'>
					<tr>
						<td width='100%' valign='top' align='center'>
							<!--LAYOUT KURSI-->
							<div align='left' class='formHeader'><strong>2. Layout Kursi</strong></div>
							<br>
							<div id="rewritemobil"></div>
						</td>
					</tr>
				</table>
			</td>       
			<td width=1 bgcolor='e0e0e0'></td>
			<!--LAYOUT DATA ISIAN PENUMPANG-->
			<td valign='top' width='370'>
				<div align='left' class='formHeader'><strong>3. Data Penumpang</strong></div>
				<br>							
				<!--data pelanggan-->
				<div align='center'>
					<span id='loading_data_penumpang' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Sedang memproses...</font></span>
					<div id="dataPelanggan"></div>
				</div>
				
					<table width='100%'>
						<tr>
							<td width='100%'>
								<table width="100%"> 
									<!--<tr>
										<td>Kartu:</td>
										<td>
											<input name="hdn_id_member" id="hdn_id_member" type="hidden"/>
											<input name="fkartu" id="fkartu" type="text" onBlur="periksaMember(fkartu.value)" onFocus="Element.hide('label_not_found');"/>
											<input class='tombol' name="btn_periksamember" id="btn_periksamember" value="Cari" type="button" onClick="periksaMember(fkartu.value)">
										</td>
									</tr>-->
									<tr>
										<td><font size=3><b>Id Member:</b></font></td>
										<td>
											<input name="hdn_id_member" id="hdn_id_member" type="hidden" />
											<input name="id_member" id="id_member" type="text" onBlur="if(this.value==''){Element.show('rewrite_list_discount');Element.hide('rewrite_keterangan_member');}else{cariDataMemberById(this.value);Element.hide('rewrite_list_discount');Element.show('rewrite_keterangan_member');}" onFocus="Element.hide('progress_cari_member');">
										</td>
									</tr>
									<tr>
										<td colspan=3 align='center' height=30 >
											<span id='label_member_not_found' style='display:none;'>
												<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>ID Member belum pernah terdaftar</b></font></td></tr></table>
											</span>
											<span id='progress_cari_member' style='display:none;'>
												<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data member...</b></font></td></tr></table>
											</span>
										</td>
									</tr>
									<tr>
										<td><font size=3><b>Telepon:</b></font><span id='telp_invalid' style='display:none;'><font color=red><b>(X)</b></font></span></td>
										<td>
											<input name="hdn_no_telp_old" id="hdn_no_telp_old" type="hidden"/>
											<input name="ftelp" id="ftelp" type="text" onkeypress='validasiNoTelp(event);'  onFocus="Element.hide('telp_invalid');Element.hide('progress_cari_penumpang');" onBlur="cariDataPelangganByTelp(ftelp.value)">
											<!--<input class='tombol' name="btn_periksanotelp" id="btn_periksanotelp" value="Cari" type="button" onClick="cariDataPelangganByTelp(ftelp.value)">-->
										</td>
									</tr>
									<tr>
										<td colspan=3 align='center' height=30 >
											<span id='label_not_found' style='display:none;'>
												<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>No.Telp belum pernah terdaftar</b></font></td></tr></table>
											</span>
											<span id='progress_cari_penumpang' style='display:none;'>
												<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data penumpang...</b></font></td></tr></table>
											</span>
										</td>
									</tr>
									<tr>
										<td>Nama: <span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
										<td>
											<input name="fnama" id="fnama" type="text" onFocus="Element.hide('nama_invalid');Element.hide('label_not_found');">
										</td>
									</tr>
									<tr>
										<td>Penumpang:</td>
										<td><span id="rewrite_list_discount"></span><span id="rewrite_keterangan_member" style='display:none'><b>Pelanggan Setia</b></span></td>
									</tr>
									<tr>
										<td>Keterangan: <span id='alamat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
										<td><textarea name="fadd" id="fadd" cols="30" rows="2" onFocus="Element.hide('alamat_invalid');Element.hide('label_not_found');"></textarea>
										</td>
									</tr>
									<tr><td align='center' colspan=2 height=40><input type="button" onclick="pesanKursi(1);" id="hider1" value="               G O   S H O W                "></td></tr>
									<tr><td align='center' colspan=2 height=40><input type="button" onclick="pesanKursi(0);" id="hider1" value="               B O O K I N G                "></td></tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign='top'>
								<table width='100%' >
									<!--<tr><td  align='center' height=40 ><input type="button" onclick="resetIsianPaket();dialog_paket.show();" id="btn_paket" value="          P A K E T   X P R E S S           "></td></tr>-->
									<!--<tr><td  align='center' height=40>Jum.Kursi:<input type='text' id='txt_jum_kursi_wl' size='3' maxlength='2' value='1'/>&nbsp;<input type="button" onclick="saveWaitingList();" id="hider2" value="&nbsp;&nbsp;Waiting list&nbsp;&nbsp;"></td></tr>
									<tr><td class="border" align='center'><input type="button" onclick="showDialogSwapKartu();" id="btn_transaksi_member" value="Transaksi Member"/></td></tr>-->
								</table>
							</td>
						</tr>
						<tr>
							<td align='center' valign='top' width='100%' colspan=3>
								<!-- LAYOUT WAITING LIST-->
								<!--<div id="rewritewaitinglist"></div>-->
							</td>
						</tr>
					</table>

			</td>
    </tr>        
  </table>   
 </td>
</tr>
</table>

</td>
</tr>
</table>