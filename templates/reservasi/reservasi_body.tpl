</td>
</tr>
</table>

<div class="container-fluid">
	<SCRIPT LANGUAGE="JavaScript">

function Start(page) {
OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}
</SCRIPT>

<input type="hidden" value="{SID}" id="hdn_SID">
<input type="hidden" value="{HARGA_MINIMUM_PAKET}" id="hdn_harga_minimum_paket">

<input type="hidden" value=0 id="flag_mutasi">
<input type="hidden" value=0 id="flag_mutasi_paket">
<input type="hidden" value='' id="id_jurusan_aktif">

<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi.js"></script>

<!--dialog SPJ-->
<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none; width: 80%;">
<form onsubmit="return false;">
<table class="table">
<tr>
	<td bgcolor='ffffff'>
		<table>
			<tr><td><h2>Cetak Manifest</h2></td></tr>
      <tr><td><div id="rewritedialogspj"></div></td></tr>
		</table>
		<span id='progress_dialog_spj' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog SPJ-->

<!--dialog keberangkatan pelanggan-->
<div dojoType="dialog" id="dialog_cari_keberangkatan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='700'>
<tr>
	<td bgcolor='ffffff' class="dialog" height="300" valign='top' align='center'>
		<input type="button" class="button-close" id="dialog_cari_jadwal_btn_Cancel" value="&nbsp;X&nbsp;"> 
		<table style="width: 100%;">
			<tr><td class="pad20"><h2>Cari Keberangkatan</h2></td></tr>
			<tr><td class="pad20"><div id="rewrite_keberangkatan_pelanggan"></div></td></tr>
		</table>
		<span class="pad20" id='progress_cari_jadwal' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>

</table>
</form>
</div>
<!--END dialog keberangkatan pelanggan-->

<!--dialog DISCOUNT-->
<div dojoType="dialog" id="dialog_discount" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table style="background: white; width: 400px;">
<tr>
	<td>
		<input type="button" style="border-radius: 0;" class="button-close" id="dialog_discount_btn_Cancel" value="&nbsp;X&nbsp;">
		<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Daftar Jenis Harga</h4>
	</td>
</tr>
<tr><td class="pad10">
	<table class="table" bgcolor='white' width='100%'>
		<tr height=30><td>Silahkan pilih harga tiket</td><td><div id="rewrite_list_discount_dialog"></div><input type='hidden' id='hdn_bayar_no_tiket'/></td></tr>
		<tr height=30><td colspan=2>Untuk melakukan proses ini, minimal akses anda haruslah supervisor, silahkan masukkan username dan password anda</td></tr>
		<tr height=30><td>Username</td><td><input class="form-control" placeholder="Username" type='text' id='korek_disc_username'/></td></tr>
		<tr height=30><td>Password</td><td><input class="form-control" placeholder="Password" type='password' id='korek_disc_password'/></td></tr>
	</table>
	<input class="btn mybutton paper" style="width: 40%; padding: 10px; float: right;" type="button" onclick="koreksiDiscount(hdn_bayar_no_tiket.value);" id="dialog_discount_btn_OK" value="Simpan perubahan">
</td></tr>
</table>
</form>
</div>
<!--END dialog DISCOUNT-->

<!--dialog CETAK ULANG TIKET-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='cetak_ulang_tiket_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiket();" id="dialog_cetak_ulang_tiket_btn_OK" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog CETAK ULANG TIKET-->

<!--dialog OTP-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket_otp" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,silahkan masukkan kode OTP dari pelanggan</td></tr>
			<tr height=30><td>OTP</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password_otp' maxlength='6'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel_otp" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiketByOTP();" id="dialog_cetak_ulang_tiket_btn_OK_otp" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog OTP-->

<!--dialog KOREKSI ASURANSI-->
<div dojoType="dialog" id="dialog_koreksi_asuransi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Koreksi Asuransi</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3><div id="rewrite_list_plan_asuransi"></div><input type='hidden' id='hdn_no_tiket_koreksi_asuransi'/></td></tr>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='korek_asuransi_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='korek_asuransi_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_koreksi_asuransi_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="koreksiAsuransi(hdn_no_tiket_koreksi_asuransi.value);" id="dialog_koreksi_asuransi_btn_OK" value="Simpan perubahan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog KOREKSI ASURANSI-->

<!--dialog BATAL-->
<div dojoType="dialog" id="dialog_batal" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pembatalan Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='batal_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='batal_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_batal_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="batal(batal_no_tiket,batal_no_kursi);" id="dialog_batal_btn_OK" value="Batalkan Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog BATAL-->

<!--dialog MUTASI-->
<div dojoType="dialog" id="dialog_mutasi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Mutasi Kursi</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='mutasi_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='mutasi_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" onclick="dlgmutasi.hide()" value="Cancel" style="width: 110px;"> &nbsp;
		<input type="button" onclick="konfirmMutasi();" value="Proses" style="width: 110px;">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog MUTASI-->

<!--dialog Input Kode Voucher-->
<div dojoType="dialog" id="dialog_input_voucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table style="background: white;">
<tr>
	<td>
		<input type="button" style="border-radius: 0;" class="button-close" id="dialog_input_voucher_btn_ok_cancel" value="&nbsp;X&nbsp;">
		<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Masukkan Kode Voucher</h4>
	</td>
</tr>
<tr><td class="pad10">
	<table class="table" width='100%'>
		<tr height=30><td colspan="2">Silakan masukkan kode voucher</td></tr>
		<tr height=30><td>Kode Voucher</td><td><input class="form-control" type='password' id='input_kode_voucher'/></td></tr>
	</table>
</td></tr>
<tr>
	<td class="pad10" align='right'>
		<input class="btn mybutton paper" type="button" onclick="bayarByVoucher(input_kode_voucher.value);" id="dialog_input_voucher_btn_ok" value="Bayar">	
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Input Kode Voucher-->

<!--dialog CETAK ULANG VOUCHER BBM-->
<div dojoType="dialog" id="dialogcetakulangvoucherbbm" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="300" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td align='center'>
		<input type="hidden" id="signature_bbm"/>
		<table bgcolor='white' width='100%'>
			<tr><td colspan='3'><h2>Cetak Ulang Voucher BBM</h2></td></tr>
			<tr height=30><td colspan=3 align="center">Untuk mencetak Manifest dan mencetak ulang voucher BBM,<br>minimal akses anda haruslah supervisor.<br>Silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='username_bbm'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='password_bbm'/></td></tr>
			<tr height=30><td>Alasan</td><td>:</td><td><textarea type='text' id='alasan_bbm' cols="40" rows="3"></textarea></td></tr>
		</table><br>
		<span id='loadingverifikasibbm' style='display:none;'><img src='./templates/images/loading.gif' />&nbsp;loading...</span>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="btncanceldialogcetakulangvoucherbbm" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="verifikasiCetakVoucherBBM();" value="Cetak Manifest">
	</td>
</tr>
</table>
</form>
</div>
<!--END CETAK ULANG VOUCHER BBM-->

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td style="background: white;">
		<input type="button" style="border-radius: 0;" class="button-close" id="dialog_pembayaran_btn_Cancel" value="&nbsp;X&nbsp;">
		<h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">Pilih Jenis Pembayaran</h4>
		<table width='100%'>
			<tr>
				<td>
				<br>
				<input type='hidden' id='isgoshow' value='' />
				<input type='hidden' id='notiketdicetak' value='' />
				<input type='hidden' id='kodebookingdicetak' value='' />
				<input type='hidden' id='kode_booking_go_show' value='' />
				<input type='hidden' id='no_tiket_goshow' value='' />
				<input type='hidden' id='cetaksemuatiket' value='1' />
				<div style="padding: 20px 60px;">
				<table width='100%'>
					<tr>
						<td width='50%' align='center' valign='middle'>
							<a href="#" onClick="CetakTiket(0);return false;">
								<svg class="svgdialog" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
									<g id="Keseluruhan">
										<path d="M559.219,286.543l109.807-69.144c3.354-2.111,5.383-5.884,5.293-9.847c-0.09-3.962-2.285-7.64-5.73-9.597L428.383,61.418
											c-3.564-2.028-8.169-1.922-11.642,0.262L30.974,304.599c-3.354,2.111-5.382,5.884-5.292,9.846c0.089,3.962,2.285,7.639,5.73,9.598
											L271.619,460.58c3.563,2.028,8.168,1.925,11.638-0.262l109.807-69.146c2.563-1.612,4.343-4.126,5.014-7.078s0.152-5.988-1.461-8.55
											c-2.088-3.318-5.678-5.298-9.604-5.298c-2.129,0-4.213,0.604-6.024,1.745l-104.068,65.53L59.072,313.692l132.548-83.466
											l228.573,129.926v171.408c0,4.126,2.244,7.929,5.856,9.923c3.544,1.959,8.09,1.827,11.516-0.331l77.155-48.586
											c3.315-2.087,5.295-5.672,5.295-9.59v-177.89c-0.008-0.088-0.013-0.171-0.016-0.255c-0.008-0.437-0.034-0.817-0.077-1.183
											l-0.022-0.207c-0.076-0.552-0.16-0.979-0.262-1.361l-0.024-0.088c-0.105-0.386-0.246-0.793-0.431-1.246
											c-0.039-0.099-0.074-0.174-0.108-0.251l-0.043-0.097c-0.135-0.305-0.282-0.597-0.44-0.885l-0.057-0.104
											c-0.049-0.088-0.096-0.177-0.148-0.263c-0.064-0.109-0.107-0.188-0.149-0.265c-0.156-0.243-0.332-0.479-0.508-0.713
											c-0.044-0.058-0.094-0.128-0.143-0.202c-0.249-0.316-0.531-0.633-0.859-0.964l-0.161-0.149c-0.305-0.293-0.598-0.548-0.902-0.788
											l-0.194-0.151c-0.487-0.361-0.897-0.631-1.286-0.847L290.535,167.942l132.546-83.464L640.93,208.305l-93.787,59.058
											c-2.563,1.613-4.343,4.126-5.014,7.078c-0.671,2.952-0.152,5.988,1.462,8.55c2.088,3.317,5.677,5.297,9.603,5.297
											C555.322,288.288,557.406,287.685,559.219,286.543z M486.62,305.472l-23.416,14.746c-2.563,1.613-4.343,4.126-5.014,7.079
											c-0.671,2.952-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297c2.129,0,4.211-0.604,6.023-1.745l22.066-13.893v151.217
											l-54.487,34.31V353.56c0-4.067-2.196-7.843-5.733-9.852L213.38,216.526l55.394-34.882L486.62,305.472z"/>
										<path d="M559.221,345.876l109.807-69.144c2.563-1.613,4.343-4.126,5.014-7.079s0.152-5.988-1.462-8.55
											c-2.088-3.318-5.678-5.298-9.604-5.298c-2.129,0-4.212,0.604-6.024,1.746l-109.807,69.144c-2.563,1.613-4.343,4.126-5.014,7.079
											s-0.152,5.989,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.326,347.621,557.408,347.018,559.221,345.876z"/>
										<path d="M398.075,443.429c0.671-2.952,0.152-5.988-1.461-8.55c-2.087-3.318-5.678-5.299-9.604-5.299
											c-2.128,0-4.212,0.604-6.024,1.745l-104.067,65.53L42.612,363.672c-1.707-0.971-3.639-1.483-5.587-1.483
											c-4.076,0-7.856,2.197-9.865,5.734c-1.497,2.633-1.878,5.69-1.075,8.608c0.803,2.919,2.694,5.35,5.326,6.846l240.207,136.537
											c3.564,2.027,8.166,1.924,11.638-0.262l109.806-69.145C395.624,448.896,397.404,446.381,398.075,443.429z"/>
										<path d="M559.221,405.211l109.807-69.145c2.563-1.613,4.343-4.126,5.014-7.078c0.671-2.952,0.152-5.989-1.462-8.55
											c-2.087-3.318-5.677-5.298-9.603-5.298c-2.129,0-4.212,0.604-6.025,1.746l-109.807,69.144c-2.563,1.612-4.343,4.127-5.014,7.079
											s-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.326,406.955,557.408,406.352,559.221,405.211z"/>
										<path d="M398.075,502.764c0.671-2.952,0.152-5.988-1.461-8.55c-2.087-3.319-5.677-5.3-9.603-5.3c-2.129,0-4.213,0.604-6.026,1.745
											l-104.067,65.53L42.612,423.007c-1.707-0.971-3.639-1.483-5.587-1.483c-4.076,0-7.856,2.197-9.865,5.734
											c-1.497,2.633-1.878,5.689-1.075,8.607c0.803,2.919,2.695,5.351,5.327,6.847l240.206,136.537c3.563,2.027,8.167,1.924,11.638-0.262
											l109.806-69.146C395.624,508.229,397.404,505.715,398.075,502.764z"/>
										<path d="M559.222,464.545L669.027,395.4c2.563-1.612,4.343-4.126,5.014-7.078s0.152-5.988-1.462-8.55
											c-2.087-3.317-5.677-5.298-9.603-5.298c-2.129,0-4.213,0.604-6.025,1.746l-109.807,69.144c-2.563,1.612-4.343,4.127-5.014,7.078
											c-0.671,2.952-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.325,466.289,557.409,465.686,559.222,464.545z"/>
										<path d="M398.075,562.098c0.671-2.952,0.152-5.988-1.461-8.55c-2.088-3.319-5.678-5.3-9.604-5.3c-2.128,0-4.212,0.604-6.024,1.745
											l-104.067,65.53L42.612,482.341c-1.707-0.971-3.639-1.483-5.587-1.483c-4.076,0-7.856,2.197-9.865,5.734
											c-1.497,2.633-1.878,5.689-1.075,8.608c0.803,2.919,2.694,5.35,5.326,6.846l240.207,136.537c3.563,2.028,8.167,1.924,11.638-0.262
											l109.807-69.146C395.625,567.563,397.405,565.05,398.075,562.098z"/>
									</g>
								</svg>
							</a>
							<br />
							<a href="#" onClick="CetakTiket(0);return false;"><span class="genmed">Tunai</span></a>
						</td>
						<td width='50%' align='center' valign='middle'>
							<a href="#" onClick="CetakTiket(1);return false;">
								<svg class="svgdialog" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
									<g id="debitcard">
										<g>
											<path d="M694.925,442.914l-39.698-327.9c-2.672-22.071-22.674-37.753-44.745-35.081L44.433,148.464
												c-22.071,2.672-37.753,22.674-35.081,44.745l39.874,329.354c2.672,22.071,22.674,37.753,44.745,35.081l567.37-68.691
												C683.413,486.281,699.095,466.279,694.925,442.914z M668.933,447.536c1.008,8.326-5.208,14.577-12.081,15.409L90.804,531.476
												c-8.326,1.008-14.577-5.208-15.409-12.081L60.83,399.093l593.538-71.859L668.933,447.536z M651.04,299.745L58.956,371.428
												l-4.832-39.913l592.085-71.683L651.04,299.745z M643.04,233.664L49.502,305.523L35.696,191.495
												c-1.008-8.326,5.208-14.577,12.081-15.409l-0.16-1.322l566.049-68.531c8.326-1.008,14.577,5.208,15.409,12.081L643.04,233.664z"/>
											<path d="M434.031,181.945l-15.005,1.817l5.786,47.794l13.534-1.639c8.456-1.024,14.67-3.899,18.642-8.626
												c3.972-4.727,5.482-11.024,4.529-18.891c-0.894-7.388-3.749-12.88-8.562-16.477C448.141,182.327,441.833,181.001,434.031,181.945z
												 M438.281,221.433l-4.348,0.526l-3.768-31.121l5.394-0.653c9.066-1.098,14.221,3.486,15.463,13.751
												C452.292,214.419,448.044,220.251,438.281,221.433z"/>
											<polygon points="474.371,225.555 501.896,222.223 500.883,213.854 483.492,215.96 482,203.635 498.182,201.676 497.176,193.373
												480.995,195.332 479.724,184.838 497.115,182.733 496.11,174.43 468.585,177.762 		"/>
											<path d="M535.707,192.722l-0.04-0.327c2.178-0.64,3.826-1.956,4.945-3.949c1.119-1.993,1.505-4.417,1.16-7.272
												c-0.509-4.206-2.412-7.088-5.707-8.646c-3.296-1.558-8.332-1.927-15.11-1.106l-14.874,1.801l5.786,47.794l17.882-2.165
												c5.405-0.654,9.534-2.393,12.388-5.215c2.854-2.822,4.017-6.413,3.489-10.771c-0.372-3.073-1.34-5.454-2.905-7.145
												C541.156,194.03,538.818,193.031,535.707,192.722z M517.22,180.299l5.329-0.645c2.899-0.351,5.066-0.21,6.503,0.423
												c1.437,0.633,2.266,1.865,2.487,3.696c0.237,1.961-0.208,3.442-1.336,4.441c-1.128,0.999-3.066,1.665-5.812,1.997l-5.884,0.712
												L517.22,180.299z M533.576,208.271c-1.2,1.229-3.195,2.012-5.985,2.35l-6.603,0.799l-1.508-12.455l6.277-0.76
												c5.666-0.686,8.74,0.954,9.22,4.921C535.243,205.327,534.776,207.042,533.576,208.271z"/>

											<rect x="553.919" y="166.988" transform="matrix(-0.9928 0.1202 -0.1202 -0.9928 1136.9572 313.545)" width="10.208" height="48.143"/>
											<polygon points="569.573,174.093 582.552,172.522 587.317,211.881 597.451,210.654 592.686,171.295 605.664,169.724
												604.643,161.29 568.552,165.659 		"/>
										</g>
									</g>
								</svg>
							</a>
							<br />
							<a href="#" onClick="CetakTiket(1);return false;"><span class="genmed">Debit Card</span></a>
						</td>
					</tr>
					<tr>
						<td align='center' valign='middle'>
							<a href="#" onClick="CetakTiket(2);return false;">
								<svg class="svgdialog" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
									<g id="Creditcard">
										<g>
											<path d="M697.904,330.702L625.22,104.128c-3.068-9.563-9.677-17.358-18.61-21.951c-8.931-4.59-19.117-5.428-28.679-2.365
												L208.438,198.344c-19.743,6.334-30.649,27.547-24.318,47.29l14.787,46.097H37.598C16.866,291.731,0,308.597,0,329.329v237.947
												c0,20.732,16.866,37.598,37.598,37.598h413.774c20.732,0,37.598-16.866,37.598-37.598V437.215l184.612-59.223
												C693.325,371.661,704.235,350.448,697.904,330.702z M451.375,570.562H37.598c-1.81,0-3.283-1.473-3.283-3.283V329.332
												c0-1.81,1.473-3.283,3.283-3.283h413.774c1.81,0,3.283,1.473,3.283,3.283v237.947
												C454.658,569.089,453.185,570.562,451.375,570.562z M590.916,112.696c0.552,0.283,1.281,0.846,1.624,1.916l14.504,45.211
												L231.301,280.364l-14.504-45.214c-0.555-1.724,0.4-3.577,2.125-4.129l369.49-118.532c0.36-0.117,0.701-0.163,1.012-0.163
												C590.038,112.327,590.55,112.507,590.916,112.696z M451.375,291.731H308.211l309.317-99.229l47.699,148.683
												c0.552,1.727-0.4,3.58-2.125,4.132l-174.128,55.86v-71.848C488.973,308.597,472.107,291.731,451.375,291.731z"/>
											<path d="M161.683,376.814H88.288c-9.474,0-17.158,7.684-17.158,17.158c0,9.474,7.684,17.158,17.158,17.158h73.395
												c9.474,0,17.158-7.684,17.158-17.158C178.841,384.497,171.16,376.814,161.683,376.814z"/>
											<path d="M243.66,411.129c9.474,0,17.158-7.684,17.158-17.158c0-9.474-7.684-17.158-17.158-17.158h-26.689
												c-9.474,0-17.158,7.684-17.158,17.158c0,9.474,7.684,17.158,17.158,17.158H243.66z"/>
											<path d="M426.676,451.164c0-9.474-7.684-17.158-17.158-17.158h-83.881c-9.474,0-17.158,7.684-17.158,17.158v74.35
												c0,9.474,7.684,17.158,17.158,17.158h83.881c9.474,0,17.158-7.684,17.158-17.158V451.164z M392.361,468.322v40.035h-49.566
												v-40.035H392.361z"/>
										</g>
									</g>
								</svg>
							</a>
							<br />
							<a href="#" onClick="CetakTiket(2);return false;"><span class="genmed">Credit Card</span></a>
						</td>
						<td align='center' valign='middle'>
							<a href="#" onClick="showInputVoucher();return false;">
								<svg class="svgdialog" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
									<g id="Voucher">
										<path d="M5.641,430.258h548.848c0.341,0,0.641-0.3,0.641-0.643v-3.051c0-1.848-1.242-4.979-2.507-6.32
											c-0.734-0.78-17.938-19.272-17.938-38.232c0-18.962,17.204-37.455,17.937-38.232c1.243-1.32,2.509-4.511,2.509-6.322v-7.151
											c0-1.846-1.242-4.978-2.509-6.323c-0.732-0.778-17.937-19.271-17.937-38.232s17.204-37.453,17.937-38.23
											c1.267-1.345,2.509-4.477,2.509-6.324v-7.149c0-1.846-1.242-4.978-2.509-6.323c-0.732-0.778-17.937-19.269-17.937-38.23
											s17.204-37.454,17.937-38.232c1.244-1.319,2.509-4.508,2.509-6.321v-5.873c0-0.341-0.3-0.641-0.641-0.641H5.641
											c-0.341,0-0.641,0.299-0.641,0.641v5.873c0,1.814,1.264,5.002,2.504,6.317c0.735,0.781,17.939,19.274,17.939,38.235
											c0,18.961-17.204,37.452-17.936,38.23C6.265,229.043,5,232.233,5,234.047v7.149c0,1.847,1.242,4.98,2.507,6.325
											c0.732,0.777,17.936,19.271,17.936,38.23c0,18.961-17.204,37.454-17.936,38.232C6.264,325.301,5,328.491,5,330.306v7.151
											c0,1.813,1.265,5.003,2.506,6.322c0.733,0.778,17.937,19.271,17.937,38.233c0,18.96-17.204,37.452-17.936,38.229
											C6.264,421.561,5,424.75,5,426.564v3.051C5,429.958,5.299,430.258,5.641,430.258z M50.021,170.097
											c0-5.867,4.774-10.641,10.643-10.641h438.801c5.867,0,10.641,4.773,10.641,10.641l0.002,228.489
											c0,5.867-4.773,10.641-10.641,10.641H60.663c-5.868,0-10.643-4.773-10.643-10.641V170.097z"/>
										<path d="M694.117,261.865l0.874-6.17c0.046-0.329-0.079-0.577-0.192-0.727c-0.112-0.15-0.314-0.338-0.639-0.384l-89.922-12.723
											c-0.858,5.789-2.632,15.436-5.66,22.261l34.614,4.964c5.986,0.848,10.173,6.416,9.325,12.411l-33.939,239.881
											c-0.764,5.385-5.437,9.445-10.872,9.445c-0.514,0-1.032-0.037-1.543-0.109l-396.652-56.122c-0.03-0.006-3.414-0.585-6.544-0.585
											H73.107c-0.154,1.894,0.635,5.115,1.603,6.059l576.01,81.498c0.047,0.008,0.137,0.032,0.137,0.01c0.42,0,0.899-0.315,0.974-0.844
											l0.455-3.207c0.282-1.995-0.583-5.572-1.747-7.221c-0.651-0.924-15.897-22.801-13.1-42.589c2.8-19.789,23.52-36.574,24.4-37.279
											c1.574-1.263,3.398-4.46,3.68-6.454l1.064-7.512c0.282-1.997-0.583-5.576-1.746-7.224c-0.651-0.923-15.9-22.8-13.104-42.587
											c2.803-19.791,23.521-36.576,24.402-37.281c1.578-1.263,3.402-4.46,3.684-6.456l1.064-7.511c0.282-1.997-0.582-5.576-1.746-7.223
											c-0.651-0.923-15.901-22.799-13.101-42.586c2.799-19.791,23.519-36.576,24.399-37.281
											C692.012,267.058,693.836,263.86,694.117,261.865z"/>
									</g>
								</svg>
							</a>
							<br />
							<a href="#" onClick="showInputVoucher();return false;"><span class="genmed">Voucher</span></a>
						</td>
					</tr>
				</table>
				</div>
			</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Pilih Pembayaran-->

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="col-md-12 box box-2" style="min-height: 110px; margin-bottom: 15px;">
				<font color='505050'>Cari keberangkatan</font><br>
				<div class="input-group" style="width: 100%;">
			      <input name="txt_cari_jadwal" id="txt_cari_jadwal" type="text" class="form-control" placeholder='No. Telepon'>
			      <span class="input-group-btn">
			        <input class='tombol form-control' name="btn_periksajadwal" id="btn_periksajadwal" value="Cari" type="button" onClick="periksaJadwal(txt_cari_jadwal.value)">
			      </span>
			    </div><!-- /input-group -->
			</div>
		</div>
		<div class="col-md-8">
			<div class="col-md-12 box box-2" style="min-height: 110px;">
				<div class="menuiconsmall notifikasi_pengumuman" id="rewritepengumuman"></div>
				<div class="menuiconsmall">
					<a href="#" onClick="{U_LAPORAN_PENJUALAN}">
						<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g>
						<path d="M606.394,610.994h-43.491V581.47c0-1.68-1.374-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967
							V581.47c0-1.68-1.374-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.374-3.054-3.054-3.054
							c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.374-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054
							v29.524h-54.967V581.47c0-1.68-1.368-3.054-3.054-3.054s-3.054,1.374-3.054,3.054v29.524h-54.967V581.47
							c0-1.68-1.368-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.368-3.054-3.054-3.054
							c-1.686,0-3.054,1.374-3.054,3.054v29.524h-54.967V581.47c0-1.68-1.368-3.054-3.054-3.054c-1.686,0-3.054,1.374-3.054,3.054
							v29.524H18.322v-69.216H42.85c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-54.967H42.85
							c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054
							c0-1.686-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054c0-1.686-1.368-3.054-3.054-3.054H18.322
							v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054
							c0-1.686-1.368-3.054-3.054-3.054H18.322v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054c0-1.686-1.368-3.054-3.054-3.054H18.322
							v-54.967H42.85c1.686,0,3.054-1.368,3.054-3.054s-1.368-3.054-3.054-3.054H18.322v-49.88c0-5.057-4.104-9.161-9.161-9.161
							S0,53.209,0,58.266v561.889c0,5.057,4.104,9.161,9.161,9.161h597.233c5.057,0,9.161-4.104,9.161-9.161
							S611.451,610.994,606.394,610.994z"/>
						<rect x="95.545" y="132.185" width="62.602" height="414.057"/>
						<path d="M290.985,327.051c-8.196-10.713-14.841-22.237-19.923-34.269h-42.679v253.461h62.602V327.051z"/>
						<path d="M422.302,388.718c-2.028,0.086-4.055,0.214-6.083,0.214c-19.654,0-38.721-3.603-56.519-10.413v167.718h62.602V388.718z"
							/>
						<path d="M559.721,426.389l-60.238-61.124c-0.776,0.476-1.588,0.892-2.364,1.35v179.621h62.602V426.389z"/>
						<path d="M694.733,489.742L525.696,318.225c42.386-54.674,38.612-133.833-11.574-184.013c-54.393-54.399-142.915-54.399-197.327,0
							c-54.393,54.406-54.393,142.915,0,197.327c50.185,50.185,129.332,53.954,184.019,11.574l169.037,171.504
							c6.871,6.871,17.999,6.871,24.87,0.006C701.604,507.753,701.604,496.613,694.733,489.742z M489.265,306.664
							c-40.688,40.688-106.893,40.694-147.581,0c-40.688-40.682-40.682-106.887,0-147.569c40.688-40.682,106.887-40.694,147.581,0
							C529.953,199.777,529.953,265.982,489.265,306.664z"/>
						<path d="M420.556,217.952c-15.959-6.602-20.576-10.596-20.576-17.828c0-5.778,4.233-12.533,16.154-12.533
							c11.219,0,18.28,4.037,21.297,5.759c0.77,0.446,1.698,0.519,2.547,0.226c0.843-0.305,1.502-0.953,1.826-1.789l4.532-11.971
							c0.531-1.393-0.024-2.962-1.313-3.713c-6.492-3.781-13.73-5.949-22.036-6.578v-14.689c0-1.686-1.368-3.054-3.054-3.054h-11.226
							c-1.686,0-3.054,1.368-3.054,3.054v15.776c-16.704,3.689-27.349,15.898-27.349,31.686c0,19.049,15.293,27.685,31.777,34.171
							c13.186,5.332,18.561,10.761,18.561,18.713c0,8.789-7.439,14.695-18.487,14.695c-8.55,0-17.443-2.651-24.393-7.268
							c-0.776-0.525-1.753-0.647-2.651-0.36c-0.898,0.287-1.606,0.983-1.912,1.869l-4.349,12.123c-0.458,1.289-0.012,2.724,1.105,3.518
							c6.352,4.532,16.466,7.72,26.427,8.41v15.366c0,1.686,1.374,3.054,3.054,3.054h11.409c1.686,0,3.054-1.368,3.054-3.054v-16.313
							c17.217-3.854,28.62-16.967,28.62-33.31C450.513,237.258,441.547,226.503,420.556,217.952z"/>
					</g>

				</svg>
						<br>Laporan Penjualan
					</a>
				</div>
				<div class="menuiconsmall"><a href="#" onClick="{U_LAPORAN_UANG}">
				<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g id="Laporan_Uang">
						<path d="M341.256,70.663c5.952,0,10.781,4.829,10.781,10.782c0,5.952-4.829,10.781-10.781,10.781H28.565v388.292H675.42V92.225
							H384.312c-5.951,0-10.781-4.829-10.781-10.781c0-5.952,4.829-10.782,10.781-10.782h301.619h0.269
							c5.952,0,10.782,4.829,10.782,10.782v409.586v0.267c0,5.952-4.829,10.782-10.782,10.782H18.075h-0.292
							c-5.952-0.001-10.781-4.83-10.781-10.782V81.735v-0.292c0-5.952,4.829-10.782,10.781-10.782L341.256,70.663L341.256,70.663z"/>
						<path d="M330.453,16.735c0-5.929,4.829-10.736,10.736-10.736c5.929,0,10.736,4.806,10.736,10.736l0.023,64.687
							c0,5.929-4.806,10.736-10.736,10.736s-10.736-4.806-10.736-10.736L330.453,16.735z"/>
						<path d="M330.497,534.243c0-5.932,4.806-10.737,10.736-10.737s10.736,4.807,10.736,10.737l-0.045,107.877
							c0,5.906-4.806,10.734-10.736,10.734s-10.736-4.806-10.736-10.734L330.497,534.243z"/>
						<path d="M179.902,531.301c1.618-5.727,7.569-9.029,13.296-7.412c5.705,1.618,9.029,7.569,7.412,13.296l-43.034,150.977
							c-1.617,5.728-7.569,9.031-13.296,7.412c-5.705-1.617-9.029-7.569-7.412-13.296L179.902,531.301z"/>
						<path d="M481.813,537.185c-1.618-5.727,1.684-11.678,7.412-13.296c5.727-1.619,11.678,1.684,13.296,7.412l43.102,150.977
							c1.618,5.706-1.683,11.679-7.411,13.296c-5.707,1.619-11.679-1.684-13.297-7.412L481.813,537.185z"/>
						<path d="M17.783,545.228c-5.952,0-10.781-4.828-10.781-10.781c0-5.953,4.829-10.781,10.781-10.781h668.418
							c5.952,0,10.782,4.828,10.782,10.781c0,5.928-4.829,10.781-10.782,10.781H17.783z"/>
						<path d="M332.877,329.514l-34.385,38.693l-77.849-97.276c-0.516-0.741-1.123-1.436-1.841-2.043
							c-4.514-3.863-11.297-3.346-15.161,1.168l-89.616,104.149l-53.119-0.09c-5.929,0-10.736,4.805-10.736,10.736
							c0,5.926,4.806,10.734,10.736,10.734l58.061,0.093c3.256,0,6.176-1.44,8.175-3.776l0.023,0.024l84.383-98.085l78.185,97.702
							c3.683,4.625,10.444,5.369,15.071,1.684c0.471-0.402,0.92-0.81,1.325-1.257l0.023,0.023l26.726-30.072V329.514z"/>
						<g>
							<path d="M591.92,295.7c-0.007-0.059-0.011-0.118-0.02-0.176c-0.042-0.289-0.097-0.578-0.175-0.863
								c-0.001-0.003-0.002-0.006-0.003-0.009c-0.073-0.266-0.166-0.529-0.271-0.789c-0.03-0.074-0.063-0.146-0.095-0.219
								c-0.083-0.188-0.175-0.372-0.276-0.554c-0.044-0.079-0.086-0.159-0.133-0.237c-0.025-0.042-0.045-0.086-0.072-0.128
								c-0.106-0.168-0.222-0.326-0.339-0.483c-0.022-0.029-0.04-0.06-0.062-0.089c-0.179-0.232-0.372-0.447-0.575-0.652
								c-0.038-0.039-0.079-0.074-0.119-0.112c-0.176-0.17-0.359-0.329-0.549-0.478c-0.049-0.038-0.096-0.076-0.146-0.112
								c-0.255-0.189-0.518-0.365-0.792-0.518l-105.478-59.956l60.731-38.242l101.814,57.872l-42.879,27.001
								c-3.346,2.106-4.351,6.527-2.244,9.873c2.106,3.346,6.527,4.351,9.873,2.244l53-33.373c2.131-1.341,3.4-3.703,3.343-6.22
								c-0.057-2.517-1.43-4.818-3.62-6.063l-115.937-65.901c-2.293-1.304-5.119-1.24-7.353,0.165l-74.428,46.867
								c-0.015,0.009-0.031,0.019-0.046,0.029l-37.192,23.42c-0.017,0.011-0.034,0.021-0.051,0.032l-74.479,46.9
								c-2.131,1.341-3.4,3.703-3.343,6.22c0.057,2.517,1.43,4.818,3.62,6.063l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936
								c1.327,0,2.651-0.369,3.815-1.101l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873c-2.106-3.346-6.527-4.352-9.873-2.244
								l-49.373,31.09l-101.814-57.874l60.732-38.243l108.589,61.724v81.75c0,2.608,1.417,5.008,3.699,6.268
								c1.079,0.596,2.27,0.891,3.46,0.891c1.327,0,2.652-0.369,3.815-1.101l37.24-23.45c2.082-1.311,3.345-3.598,3.345-6.058v-85.915
								c0-0.042-0.007-0.082-0.009-0.125C591.964,296.176,591.948,295.938,591.92,295.7z M554.737,392.935v-72.946
								c0-2.575-1.383-4.952-3.622-6.224l-105.54-59.991l23.493-14.794l101.814,57.873l-8.914,5.613
								c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346c1.303,0,2.623-0.356,3.807-1.102l8.061-5.075v68.994
								L554.737,392.935z"/>
							<path d="M655.481,272.22l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873C663.247,271.117,658.826,270.112,655.481,272.22
								z"/>
							<path d="M522.283,356.094l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,354.991,525.629,353.986,522.283,356.094z"/>
							<path d="M655.481,300.858l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873C663.247,299.755,658.826,298.75,655.481,300.858
								z"/>
							<path d="M522.283,384.732l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,383.629,525.629,382.624,522.283,384.732z"/>
							<path d="M655.481,329.496l-53,33.373c-3.346,2.106-4.351,6.527-2.244,9.873c1.36,2.162,3.686,3.346,6.066,3.346
								c1.303,0,2.623-0.356,3.807-1.102l53-33.373c3.346-2.106,4.351-6.527,2.244-9.873
								C663.247,328.394,658.826,327.389,655.481,329.496z"/>
							<path d="M522.283,413.37l-49.373,31.09l-112.211-63.783c-3.435-1.953-7.808-0.753-9.762,2.686
								c-1.954,3.437-0.752,7.808,2.686,9.762l115.938,65.901c1.098,0.625,2.318,0.936,3.538,0.936c1.327,0,2.651-0.369,3.815-1.101
								l53-33.374c3.346-2.106,4.351-6.527,2.244-9.873C530.05,412.268,525.629,411.262,522.283,413.37z"/>
						</g>
					</g>
				</svg>
				<br>Laporan Uang</a></div>				
				<div class="menuiconsmall"><a href="#" onClick="{U_PAKET}">
				<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g>
						<path d="M305.016,224.064v252.192l200.778-38.103V315.277c0-5.144,4.48-9.139,9.59-8.553l24.701,3.822
							c4.346,0.499,7.627,4.178,7.627,8.553v142.748c0,4.058-2.833,7.564-6.8,8.417l-219.787,47.258
							c-10.621,2.283-21.604,2.308-32.235,0.072L71.462,471.859c-3.902-0.82-6.73-4.214-6.834-8.201l-3.756-144.974
							c-0.118-4.547,3.322-8.4,7.852-8.798l184.692-16.229c3.203-0.281,5.982-2.325,7.207-5.297l27.824-67.573
							C292.037,212.067,305.016,214.635,305.016,224.064z M58.783,141.494L0.825,265.38c-2.832,6.054,2.014,12.885,8.663,12.213
							L227.149,255.6c3.076-0.311,5.749-2.247,7.003-5.073l70.035-157.78L65.026,136.675C62.289,137.178,59.963,138.973,58.783,141.494z
							 M304.188,92.747l74.106,162.712c1.295,2.844,4.029,4.762,7.145,5.013l217.25,17.471c6.547,0.526,11.248-6.178,8.522-12.154
							l-56.658-124.212c-1.172-2.569-3.528-4.399-6.308-4.9L304.188,92.747z"/>
					</g>
				</svg>
				<br>Ekspedisi</a></div>					
				<div class="menuiconsmall"><a href="#" onClick="{U_BA_SOPIR}">
				<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
					<g id="Biaya_Sopir">
						<path style="display:inline;" d="M338.751,484.399c-64.992,54.675-140.795,100.211-225.302,135.344
							c-1.891,0.786-3.853,1.159-5.781,1.159c-5.91,0-11.519-3.496-13.928-9.293c-3.196-7.689,0.446-16.513,8.133-19.711
							c2.082-0.866,4.168-1.742,6.242-2.622c0.162-0.618,0.367-1.235,0.613-1.843l56.345-138.872
							c17.342-42.732,35.632-72.972,57.558-95.17c28.948-29.307,64.741-44.166,106.381-44.166H379.7c8.328,0,15.077,6.75,15.077,15.077
							s-6.75,15.077-15.077,15.077h-50.686c-61.044,0-101.719,36.043-136.001,120.52l-45.184,111.363
							c142.753-69.006,256.981-171.506,317.526-286.256c3.883-7.366,13.003-10.187,20.371-6.299c7.364,3.885,10.184,13.007,6.299,20.371
							c-4.999,9.477-10.318,18.853-15.95,28.129c23.824,13.251,44.251,33.709,62.168,62.19c4.433,7.049,2.315,16.357-4.734,20.791
							c-7.048,4.433-16.357,2.315-20.79-4.734c-15.576-24.76-32.989-42.122-53.041-52.835
							C427.523,399.633,387.033,443.781,338.751,484.399z M273.944,165.211c0-50.972,41.469-92.441,92.438-92.441
							s92.436,41.469,92.436,92.441c0,50.971-41.467,92.439-92.436,92.439C315.413,257.649,273.944,216.182,273.944,165.211z
							 M304.099,165.211c0,34.344,27.941,62.285,62.285,62.285c34.342,0,62.282-27.941,62.282-62.285s-27.94-62.287-62.282-62.287
							C332.038,102.924,304.099,130.865,304.099,165.211z M337.002,594.75c-1.308-8.222-9.021-13.827-17.26-12.519
							c-8.224,1.31-13.829,9.038-12.519,17.262c5.672,35.626,21.617,68.676,46.109,95.579c2.975,3.268,7.056,4.927,11.154,4.927
							c3.622,0,7.255-1.298,10.145-3.928c6.158-5.605,6.604-15.142,1-21.299C355.108,652.227,341.748,624.556,337.002,594.75z
							 M674.791,541.32c-5.825-35.522-21.704-68.544-45.923-95.492c-5.568-6.192-15.101-6.701-21.292-1.136
							c-6.193,5.565-6.703,15.099-1.136,21.292c20.357,22.653,33.704,50.391,38.592,80.214c1.213,7.391,7.607,12.64,14.861,12.64
							c0.809,0,1.632-0.065,2.458-0.201C670.568,557.29,676.138,549.537,674.791,541.32z M602.666,511.774
							c32.18,61.371,8.435,137.48-52.932,169.661c-18.575,9.74-38.505,14.358-58.154,14.358c-45.268,0-89.064-24.495-111.509-67.287
							c-32.18-61.367-8.436-137.477,52.927-169.661c0.002,0,0.002,0,0.002,0C494.368,426.663,570.479,450.405,602.666,511.774z
							 M443.649,578.14c-1.246-7.396-0.744-15.034,1.553-22.401c3.848-12.331,12.266-22.426,23.705-28.427c0.002,0,0.002,0,0.002,0
							c18.952-9.939,41.563-6.012,56.154,8.132l42.841-22.468c-27.568-36.951-78.801-49.503-120.897-27.427
							c-42.099,22.08-60.902,71.358-46.178,115.047L443.649,578.14z M507.494,561.687c-4.664-8.891-15.687-12.33-24.579-7.671
							c-4.307,2.26-7.478,6.059-8.926,10.703c-1.447,4.642-1.002,9.569,1.256,13.876c4.664,8.89,15.69,12.328,24.586,7.668
							C508.715,581.601,512.154,570.574,507.494,561.687z M581.91,539.68l-42.841,22.468c3.344,20.044-6.28,40.876-25.234,50.818
							c-18.957,9.939-41.569,6.014-56.159-8.132l-42.842,22.467c27.568,36.956,78.798,49.507,120.898,27.43
							C577.832,632.652,596.637,583.372,581.91,539.68z"/>
						<g style="display:inline;">
							<path d="M112.56,101.066c-12.131-4.863-15.635-7.803-15.635-13.113c0-4.249,3.219-9.214,12.293-9.214
								c8.026,0,14.182,3.165,14.237,3.192c0.648,0.346,1.374,0.53,2.094,0.53c1.756,0,3.31-1.063,3.954-2.708l2.205-5.636
								c0.768-2.054-0.397-3.843-1.882-4.485c-4.942-2.151-14.6-3.816-14.698-3.833c-0.156-0.027-0.697-0.143-0.697-0.776l-0.031-8.176
								c0-2.462-2.056-4.467-4.58-4.467h-3.955c-2.522,0-4.577,2.003-4.577,4.468l0.01,8.596c0,0.659-0.714,0.945-0.975,1.006
								c-12.191,2.904-19.81,11.834-19.81,23.099c0,14.042,11.608,20.388,24.141,25.151c10.02,3.934,14.107,7.922,14.107,13.772
								c0,6.366-5.788,10.812-14.081,10.812c-7.081,0-16.665-4.489-16.76-4.534c-0.613-0.291-1.263-0.437-1.931-0.437
								c-1.824,0-3.412,1.103-4.047,2.811l-2.105,5.7c-0.748,2.128,0.404,3.856,1.879,4.625c5.886,3.065,17.275,4.862,17.782,4.941
								c0.136,0.019,0.83,0.263,0.83,0.897v8.558c0,2.462,2.056,4.467,4.581,4.467h4.087c2.526,0,4.582-2.003,4.582-4.467v-9.007
								c0-0.842,0.621-0.916,0.749-0.947c12.988-2.918,20.988-12.42,20.988-24.351C135.31,115.283,128.504,107.365,112.56,101.066z"/>
							<path d="M107.454,2C48.167,2,0.11,50.061,0.11,109.344c0,59.284,48.058,107.346,107.344,107.346
								c59.284,0,107.343-48.062,107.343-107.346C214.796,50.061,166.738,2,107.454,2z M107.454,191.812
								c-45.547,0-82.467-36.924-82.467-82.467s36.92-82.466,82.467-82.466c45.545,0,82.464,36.922,82.464,82.466
								S152.999,191.812,107.454,191.812z"/>
						</g>
					</g>
				</svg>
				<br/>Bayar Ins.Sopir</a></div>
				<div class="menuiconsmall"><a href="#" onClick="{U_BA_BOP}">
					<svg class="svgsmall" style="" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 700 700" style="enable-background:new 0 0 700 700;" xml:space="preserve">
						<g id="Keseluruhan">
							<path d="M559.219,286.543l109.807-69.144c3.354-2.111,5.383-5.884,5.293-9.847c-0.09-3.962-2.285-7.64-5.73-9.597L428.383,61.418
								c-3.564-2.028-8.169-1.922-11.642,0.262L30.974,304.599c-3.354,2.111-5.382,5.884-5.292,9.846c0.089,3.962,2.285,7.639,5.73,9.598
								L271.619,460.58c3.563,2.028,8.168,1.925,11.638-0.262l109.807-69.146c2.563-1.612,4.343-4.126,5.014-7.078s0.152-5.988-1.461-8.55
								c-2.088-3.318-5.678-5.298-9.604-5.298c-2.129,0-4.213,0.604-6.024,1.745l-104.068,65.53L59.072,313.692l132.548-83.466
								l228.573,129.926v171.408c0,4.126,2.244,7.929,5.856,9.923c3.544,1.959,8.09,1.827,11.516-0.331l77.155-48.586
								c3.315-2.087,5.295-5.672,5.295-9.59v-177.89c-0.008-0.088-0.013-0.171-0.016-0.255c-0.008-0.437-0.034-0.817-0.077-1.183
								l-0.022-0.207c-0.076-0.552-0.16-0.979-0.262-1.361l-0.024-0.088c-0.105-0.386-0.246-0.793-0.431-1.246
								c-0.039-0.099-0.074-0.174-0.108-0.251l-0.043-0.097c-0.135-0.305-0.282-0.597-0.44-0.885l-0.057-0.104
								c-0.049-0.088-0.096-0.177-0.148-0.263c-0.064-0.109-0.107-0.188-0.149-0.265c-0.156-0.243-0.332-0.479-0.508-0.713
								c-0.044-0.058-0.094-0.128-0.143-0.202c-0.249-0.316-0.531-0.633-0.859-0.964l-0.161-0.149c-0.305-0.293-0.598-0.548-0.902-0.788
								l-0.194-0.151c-0.487-0.361-0.897-0.631-1.286-0.847L290.535,167.942l132.546-83.464L640.93,208.305l-93.787,59.058
								c-2.563,1.613-4.343,4.126-5.014,7.078c-0.671,2.952-0.152,5.988,1.462,8.55c2.088,3.317,5.677,5.297,9.603,5.297
								C555.322,288.288,557.406,287.685,559.219,286.543z M486.62,305.472l-23.416,14.746c-2.563,1.613-4.343,4.126-5.014,7.079
								c-0.671,2.952-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297c2.129,0,4.211-0.604,6.023-1.745l22.066-13.893v151.217
								l-54.487,34.31V353.56c0-4.067-2.196-7.843-5.733-9.852L213.38,216.526l55.394-34.882L486.62,305.472z"/>
							<path d="M559.221,345.876l109.807-69.144c2.563-1.613,4.343-4.126,5.014-7.079s0.152-5.988-1.462-8.55
								c-2.088-3.318-5.678-5.298-9.604-5.298c-2.129,0-4.212,0.604-6.024,1.746l-109.807,69.144c-2.563,1.613-4.343,4.126-5.014,7.079
								s-0.152,5.989,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.326,347.621,557.408,347.018,559.221,345.876z"/>
							<path d="M398.075,443.429c0.671-2.952,0.152-5.988-1.461-8.55c-2.087-3.318-5.678-5.299-9.604-5.299
								c-2.128,0-4.212,0.604-6.024,1.745l-104.067,65.53L42.612,363.672c-1.707-0.971-3.639-1.483-5.587-1.483
								c-4.076,0-7.856,2.197-9.865,5.734c-1.497,2.633-1.878,5.69-1.075,8.608c0.803,2.919,2.694,5.35,5.326,6.846l240.207,136.537
								c3.564,2.027,8.166,1.924,11.638-0.262l109.806-69.145C395.624,448.896,397.404,446.381,398.075,443.429z"/>
							<path d="M559.221,405.211l109.807-69.145c2.563-1.613,4.343-4.126,5.014-7.078c0.671-2.952,0.152-5.989-1.462-8.55
								c-2.087-3.318-5.677-5.298-9.603-5.298c-2.129,0-4.212,0.604-6.025,1.746l-109.807,69.144c-2.563,1.612-4.343,4.127-5.014,7.079
								s-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.326,406.955,557.408,406.352,559.221,405.211z"/>
							<path d="M398.075,502.764c0.671-2.952,0.152-5.988-1.461-8.55c-2.087-3.319-5.677-5.3-9.603-5.3c-2.129,0-4.213,0.604-6.026,1.745
								l-104.067,65.53L42.612,423.007c-1.707-0.971-3.639-1.483-5.587-1.483c-4.076,0-7.856,2.197-9.865,5.734
								c-1.497,2.633-1.878,5.689-1.075,8.607c0.803,2.919,2.695,5.351,5.327,6.847l240.206,136.537c3.563,2.027,8.167,1.924,11.638-0.262
								l109.806-69.146C395.624,508.229,397.404,505.715,398.075,502.764z"/>
							<path d="M559.222,464.545L669.027,395.4c2.563-1.612,4.343-4.126,5.014-7.078s0.152-5.988-1.462-8.55
								c-2.087-3.317-5.677-5.298-9.603-5.298c-2.129,0-4.213,0.604-6.025,1.746l-109.807,69.144c-2.563,1.612-4.343,4.127-5.014,7.078
								c-0.671,2.952-0.152,5.988,1.461,8.55c2.087,3.317,5.678,5.297,9.605,5.297C555.325,466.289,557.409,465.686,559.222,464.545z"/>
							<path d="M398.075,562.098c0.671-2.952,0.152-5.988-1.461-8.55c-2.088-3.319-5.678-5.3-9.604-5.3c-2.128,0-4.212,0.604-6.024,1.745
								l-104.067,65.53L42.612,482.341c-1.707-0.971-3.639-1.483-5.587-1.483c-4.076,0-7.856,2.197-9.865,5.734
								c-1.497,2.633-1.878,5.689-1.075,8.608c0.803,2.919,2.694,5.35,5.326,6.846l240.207,136.537c3.563,2.028,8.167,1.924,11.638-0.262
								l109.807-69.146C395.625,567.563,397.405,565.05,398.075,562.098z"/>
						</g>
					</svg>
				<br/>Bayar BO<br>Tambahan</a></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="col-md-12 box box-2">
				<span class='formHeader sectiontitle'>1. Jadwal</span>
					<div class="col-md-12" style="text-align: center;">
						<h5>Tanggal Keberangkatan</h5>
						<!--  kolom kalender -->
							<iframe width=174 height=189 style="border-radius: 3px;" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
							</iframe>

							<form name="formTanggal">
								<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />			
							</form>
						<!--end kolom kalender-->
							
							<input class="mybutton paper paper-lift" style="width: 190px; margin: 0 auto;" name="update" class="form-control" onclick="getUpdateAsal(kota_asal.value,1)" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></input>
					</div>
					
					<div class="col-md-12">
						<br />Kota Keberangkatan<br>
						<div class="table-reservasi">
							<select class="form-control" onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select>
							<div id="rewritetype"></div>
							<div id="rewriteall">
								<div class="table-reservasi" id="rewriteasal"></div>
								<div class="table-reservasi" id="rewritetujuan"></div>
								<div class="table-reservasi" id="rewritejadwal"></div>
								<!--<span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span>-->
							</div>
						</div>
						<hr  style="margin-top: 20px; padding: 0; ">
						Keterangan:
						<table>
							<tr>
								<td valign="top" class="pad102"><img class='img_keterangan' src='./templates/images/kursi_kosong_ket.gif' /></td>
								<td class="pad102"><span class="noticeMessage"><br />Kursi masih kosong</span></td>
							</tr>
							<tr>
								<td valign="top" class="pad102"><img class='img_keterangan' src='./templates/images/kursi_pesan_ket.gif' /></td>
								<td class="pad102"><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke POOL INI</span></td>
							</tr>
							<tr>
								<td valign="top" class="pad102"><img class='img_keterangan' src='./templates/images/kursi_konfirm_ket.gif' /></td>
								<td class="pad102"><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke POOL ini</span></td>
							</tr>
							<tr>
								<td valign="top" class="pad102"><img class='img_keterangan' src='./templates/images/kursi_pesan1_ket.gif' /></td>
								<td class="pad102"><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke pool PICK-UP</span></td>
							</tr>
							<tr>
								<td valign="top" class="pad102"><img class='img_keterangan' src='./templates/images/kursi_konfirm1_ket.gif' /></td>
								<td class="pad102"><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke pool PICK-UP</span></td>
							</tr>
						</table>
					</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-12 box box-2">
				<!--LAYOUT KURSI-->
				<span class='formHeader sectiontitle'>2. Layout Kursi</span>
				<br>
				<div id="rewritemobil"></div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-12 box box-2">
				<span class='formHeader sectiontitle'>3. Data Penumpang</span>
					<br>							
					<!--data pelanggan-->
					<div align='center'>
						<span id='loading_data_penumpang' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Sedang memproses...</font></span>
						<div id="dataPelanggan"></div>
					</div>
					
						<table width='100%'>
							<tr>
								<td width='100%'>
									<table width="80%" align="center"> 
										<!--<tr>
											<td>Kartu:</td>
											<td>
												<input name="hdn_id_member" id="hdn_id_member" type="hidden"/>
												<input name="fkartu" id="fkartu" type="text" onBlur="periksaMember(fkartu.value)" onFocus="Element.hide('label_not_found');"/>
												<input class='tombol' name="btn_periksamember" id="btn_periksamember" value="Cari" type="button" onClick="periksaMember(fkartu.value)">
											</td>
										</tr>-->
										<tr>
											<td colspan="2" class="pad10">
												ID Member<br />
												<input name="hdn_id_member" id="hdn_id_member" type="hidden" />
												<input placeholder="ID Member" class="form-control" name="id_member" id="id_member" type="text" onBlur="if(this.value==''){Element.show('rewrite_list_discount');Element.hide('rewrite_keterangan_member');}else{cariDataMemberById(this.value);Element.hide('rewrite_list_discount');Element.show('rewrite_keterangan_member');}" onFocus="Element.hide('progress_cari_member');">
			

												<span id='label_member_not_found' style='display:none;'>
													<div class="alert alert-danger">ID Member belum pernah terdaftar</div>
												</span>
												<span id='progress_cari_member' style='display:none;'>
													<div class="alert alert-info"><img src='./templates/images/loading.gif' />&nbsp; Mencari data member..</div>
												</span>



												<br />Telepon<br />
												<span id='telp_invalid' style='display:none;'><font style="color: #FF552A;"><b>Tidak Valid</b></font></span>
												<input name="hdn_no_telp_old" id="hdn_no_telp_old" type="hidden"/>
												<input class="form-control" placeholder="No. Telepon" name="ftelp" id="ftelp" type="text" onkeypress='validasiNoTelp(event);'  onFocus="Element.hide('telp_invalid');Element.hide('progress_cari_penumpang');" onBlur="cariDataPelangganByTelp(ftelp.value)">

												
												<!--<input class='tombol' name="btn_periksanotelp" id="btn_periksanotelp" value="Cari" type="button" onClick="cariDataPelangganByTelp(ftelp.value)">-->

												<span id='label_not_found' style='display:none;'>
													<div class="alert alert-danger">No.Telp belum pernah terdaftar</div>
													<!-- <table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>No.Telp belum pernah terdaftar</b></font></td></tr></table> -->
												</span>
												<span id='progress_cari_penumpang' style='display:none;'>
													<div class="alert alert-info"><img src='./templates/images/loading.gif' />&nbsp; Mencari data penumpang, harap tunggu..</div>
												</span>

												<br />Nama<br />
												<span id='nama_invalid' style='display:none;'><font style="color: #FF552A;">&nbsp;<b>Tidak Valid</b></font></span>
												<input class="form-control" placeholder="Nama" name="fnama" id="fnama" type="text" onFocus="Element.hide('nama_invalid');Element.hide('label_not_found');" onkeypress="valIn(event)"><br />
												Penumpang:<br />
												<span id="rewrite_list_discount"></span><span id="rewrite_keterangan_member" style='display:none'><div class="alert alert-info">Pelanggan Setia</div></span>
												

												
												<br />Keterangan <span id='alamat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span><br />
												<textarea placeholder="Keterangan" style="resize: none; height: 100px;" class="form-control" name="fadd" id="fadd" cols="30" rows="2" onFocus="Element.hide('alamat_invalid');Element.hide('label_not_found');"></textarea><br />
												<input class="mybutton paper paper-lift" style="width: 100%;" type="button" onclick="pesanKursi(1);" id="hider1" value="GO Show"><br />
												<input class="mybutton paper paper-lift" style="width: 100%;" type="button" onclick="pesanKursi(0);" id="hider1" value="Booking">
												

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign='top'>
									<table width='100%' >
										<!--<tr><td  align='center' height=40 ><input type="button" onclick="resetIsianPaket();dialog_paket.show();" id="btn_paket" value="          P A K E T   X P R E S S           "></td></tr>-->
										<!--<tr><td  align='center' height=40>Jum.Kursi:<input type='text' id='txt_jum_kursi_wl' size='3' maxlength='2' value='1'/>&nbsp;<input type="button" onclick="saveWaitingList();" id="hider2" value="&nbsp;&nbsp;Waiting list&nbsp;&nbsp;"></td></tr>
										<tr><td class="border" align='center'><input type="button" onclick="showDialogSwapKartu();" id="btn_transaksi_member" value="Transaksi Member"/></td></tr>-->
									</table>
								</td>
							</tr>
							<tr>
								<td align='center' valign='top' width='100%' colspan=3>
									<!-- LAYOUT WAITING LIST-->
									<!--<div id="rewritewaitinglist"></div>-->
								</td>
							</tr>
						</table>
			</div>
		</div>
	</div>
</div>
</div>