<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}
</script>	 

<body class='tiket'>
<div style="font-size:14px;width:400px;height: 470px;">
	<span style="font-size:22px;">
		REKAP MANIFEST<br>
		{NAMA_PERUSAHAAN}<br>
	</span>
	-----------------------------------------<br>
	CSO:{NAMA_CSO}<br>
	Tgl:{TANGGAL_TRANSAKSI}<br>
	Login:{WAKTU_LOGIN}<br>
	-----------------------------------------<br>
	<!-- BEGIN TIKET -->
	{TIKET.JURUSAN}
	Jam:{TIKET.JAM}|{TIKET.KENDARAAN}<br>
	{TIKET.SOPIR}<br>
	Pnp:{TIKET.LIST_PENUMPANG}<br>
	Omz.Pnp:Rp.{TIKET.OMZET_PNP}<br>
	Pkt:{TIKET.PAX_PAKET} pax<br>
	Omz.Pkt:Rp.{TIKET.OMZET_PKT}<br>
	Biaya:Rp.{TIKET.BIAYA}<br>
	-----------------------------------------<br>
	{TIKET.SHOW_SUB_TOTAL_TIKET_JURUSAN}
	<!-- END TIKET -->
	{SHOW_SUB_TOTAL}
	-----------------------------------------<br>
	{SHOW_TOTAL}
	-----------------------------------------<br>
	Waktu Cetak {WAKTU_CETAK}<br>
	
</div>
</body>

<script language="javascript">
	printWindow();
	window.close();
</script>
</html>
