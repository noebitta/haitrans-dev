<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Penjualan Paket {NAMA}</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">					
						&nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">					
						<input type="submit" value="cari" />&nbsp;
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table >
		<tr>
    <tr>
			<td width='100%' align='right' colspan=17 valign='bottom'>{PAGING}</td>
		</tr>
		<tr>
       <th rowspan="2" width="30">No</th>
       <th rowspan="2" width="120">#resi</th>
       <th rowspan="2" width="100">#Jadwal</th>
			 <th rowspan="2" width="100">Waktu Berangkat</th>
			 <th colspan='3' class="thintop">Pengirim</th>
			 <th colspan='3' class="thintop"top>Penerima</th>
			 <th rowspan="2" width="50">Berat<br>(Kg)</th>
			 <th rowspan="2" width="70">Harga</th>
			 <th rowspan="2" width="70">Diskon</th>
			 <th rowspan="2" width="70">Bayar</th>
			 <th rowspan="2" width="70">Layanan</th>
			 <th rowspan="2" width="100">Jenis Bayar</th>
			 <th rowspan="2" width="100">Status</th>
     </tr>
     <tr>
 			 <th width="100" class="thinbottom">Nama</th>	
			 <th width="100" class="thinbottom">Alamat</th>
			 <th width="70"  class="thinbottom">Telp</th>
			 <th width="100" class="thinbottom">Nama</th>
			 <th width="100" class="thinbottom">Alamat</th>
			 <th width="70" class="thinbottom">Telp</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="left">{ROW.no}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
			 <td><div align="center">{ROW.kode_jadwal}</div></td>
			 <td><div align="center">{ROW.waktu_berangkat}</div></td>
			 <td><div align="left">{ROW.nama_pengirim}</div></td>
			 <td><div align="left">{ROW.alamat_pengirim}</div></td>
			 <td><div align="left">{ROW.telp_pengirim}</div></td>
			 <td><div align="left">{ROW.nama_penerima}</div></td>
			 <td><div align="left">{ROW.alamat_penerima}</div></td>
			 <td><div align="left">{ROW.telp_penerima}</div></td>
			 <td><div align="right">{ROW.berat}</div></td>
			 <td><div align="right">{ROW.harga}</div></td>
			 <td><div align="right">{ROW.diskon}</div></td>
			 <td><div align="right">{ROW.bayar}</div></td>
			 <td><div align="center">{ROW.layanan}</div></td>
			 <td><div align="center">{ROW.jenis_bayar}</div></td>
			 <td style="{ROW.style_status}"><div align="center">{ROW.status}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>