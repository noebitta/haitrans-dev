<script type="text/javascript">	
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%'>
			<tr>
				<td align='center' valign='middle' class="bannerjudul">Laporan Biaya-Biaya</td>
				<td colspan=2 align='right' class="bannercari" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Dikeluarkan di cabang:&nbsp;<select id='opt_cabang' name='opt_cabang'>{OPT_CABANG}</select><br><br>
						Jenis Biaya:&nbsp;<select id='opt_jenis_biaya' name='opt_jenis_biaya'>
																<option value='' {JENIS_BIAYA_}>-semua biaya-</option>
																<option value=0  {JENIS_BIAYA_0}>Jasa Sopir</option>
																<option value=1  {JENIS_BIAYA_1}>Tol</option>
																<option value=4  {JENIS_BIAYA_4}>Parkir</option>
																<option value=2  {JENIS_BIAYA_2}>Fee Driver</option>
																<option value=3  {JENIS_BIAYA_3}>BBM</option>
															</select><br><br>
						&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
						&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
						&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
						<input type="submit" value="cari" />&nbsp;		
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
							<td class='border'>
								 &nbsp;
							</td><td width=1></td>
							<td class='border'>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
				<td colspan=2 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table class="border">
    <tr>
       <th width=30>No</th>
			 <th width=200>Tgl.Transaksi</th>
			 <th width=200>No.SPJ</th>
			 <th width=200>No.Pol</th>
			 <th width=300>Sopir</th>
			 <th width=300>Jurusan</th>
			 <th width=200>Jenis Biaya</th>
			 <th width=200>Jumlah</th>
			 <th width=100>Cabang</th>
			 <th width=100>Kasir</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.tgl_transaksi}</div></td>
			 <td><div align="left">{ROW.no_spj}</div></td>
			 <td><div align="left">{ROW.no_pol}</div></td>
       <td><div align="left">{ROW.sopir}</div></td>
       <td><div align="left">{ROW.jurusan}</div></td>
			 <td><div align="left">{ROW.jenis_biaya}</div></td>
			 <td><div align="right">{ROW.jumlah}</div></td>
			 <td><div align="left">{ROW.cabang}</div></td>
			 <td><div align="left">{ROW.kasir}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>