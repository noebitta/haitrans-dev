<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }
		
		new Ajax.Updater("rewrite_tujuan","laporan_omzet_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
          Element.show('loading_tujuan');
        },
        onComplete: function(request) 
        {
					Element.hide('loading_tujuan');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

getUpdateTujuan("{ASAL}");

</script>
<div>
	<div class="row">
		<div class="col-md-12 box">

			<table class="table" cellspacing="0" cellpadding="0">
			<tr>
			 <td valign="middle" align="center">		
					<form action="{ACTION_CARI}" method="post" name="my_form">
					<input type='hidden' id='is_cari' name='is_cari' value='1'>
					<table width='100%' cellspacing="0" class="table">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudulw">
								<div class="bannerjudul">Laporan Data Paket</div></td>
								
						</tr>
						<tr>
							<td align='left' valign='middle'>
									<div class="col-md-3">
										Tgl:<br /><input class="form-control" readonly="yes"  id="tgl_awal" name="tgl_awal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
									</div>
									<div class="col-md-3">
										s/d<br /><input class="form-control" readonly="yes"  id="tgl_akhir" name="tgl_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
									</div>
									<div class="col-md-3">
										Cari:<br /><input class="form-control" type="text" id="cari" name="cari" value="{CARI}" />
									</div>
									<div class="col-md-3">
										<input type="submit" class="btn mybutton topwidth" style="height: 34px;" value="cari" />
									</div>
								</td>
						</tr>
						<tr>
							<td width='100%' align='right' class="pad10" colspan="17" valign='bottom'>{PAGING}</td>
						</tr>
					</table>
					</form>
					<table class="table table-hover table-bordered">
						<tr>
					       <th rowspan="2" >No</th>
					       <th rowspan="2" >#resi</th>
					       <th rowspan="2" >#Jadwal</th>
								 <th rowspan="2" >Waktu Berangkat</th>
								 <th colspan='3' class="thintop">Pengirim</th>
								 <th colspan='3' class="thintop"top>Penerima</th>
								 <th rowspan="2" >Berat<br>(Kg)</th>
								 <th rowspan="2" >Harga</th>
								 <th rowspan="2" >Diskon</th>
								 <th rowspan="2" >Bayar</th>
								 <th rowspan="2" >Layanan</th>
								 <th rowspan="2" >Jenis Bayar</th>
								 <th rowspan="2" >Status</th>
					     </tr>
					     <tr>
					 			 <th  class="thinbottom">Nama</th>	
								 <th  class="thinbottom">Alamat</th>
								 <th   class="thinbottom">Telp</th>
								 <th  class="thinbottom">Nama</th>
								 <th  class="thinbottom">Alamat</th>
								 <th  class="thinbottom">Telp</th>
					     </tr>
					     <!-- BEGIN ROW -->
					     <tr class="{ROW.odd}">
					       <td><div align="left">{ROW.no}</div></td>
								 <td><div align="left">{ROW.no_tiket}</div></td>
								 <td><div align="center">{ROW.kode_jadwal}</div></td>
								 <td><div align="center">{ROW.waktu_berangkat}</div></td>
								 <td><div align="left">{ROW.nama_pengirim}</div></td>
								 <td><div align="left">{ROW.alamat_pengirim}</div></td>
								 <td><div align="left">{ROW.telp_pengirim}</div></td>
								 <td><div align="left">{ROW.nama_penerima}</div></td>
								 <td><div align="left">{ROW.alamat_penerima}</div></td>
								 <td><div align="left">{ROW.telp_penerima}</div></td>
								 <td><div align="right">{ROW.berat}</div></td>
								 <td><div align="right">{ROW.harga}</div></td>
								 <td><div align="right">{ROW.diskon}</div></td>
								 <td><div align="right">{ROW.bayar}</div></td>
								 <td><div align="center">{ROW.layanan}</div></td>
								 <td><div align="center">{ROW.jenis_bayar}</div></td>
								 <td style="{ROW.style_status}"><div align="center">{ROW.status}</div></td>
					     </tr>  
					     <!-- END ROW -->
				    </table>
			    <table class="table">
					<tr>
						<td width='100%' align='right' colspan=13 valign='bottom'>{PAGING}</td>
					</tr>
					<tr>
	                    <td align="left">
	                        <table>
	                            <tr><td colspan="3">LAYANAN:<td></tr>
	                            <tr><td >P2</td><td >:</td><td>POINT TO POINT</td><tr>
	                            <tr><td>D2</td><td>:</td><td>DOOR TO DOOR</td><tr>
	                        </table>
	                    </td>
	                </tr>
				</table>
			 </td>
			</tr>
			</table>

		</div>
	</div>
</div>
