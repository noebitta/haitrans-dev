<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<div class="container" style="width: 95%;">
	<div class="row">
		<div class="col-md-12 box" style="overflow-x: scroll;">
			<div class="banner"><div class="bannerjudul">&nbsp;Detail Rekap Transaksi</div></div>
			<br>
			<h2>Detail Tiket</h2>
			<table class="table table-responsive table-hover table-bordered" width="100%"  cellspacing="1" cellpadding="1">
				<tr>
					<th >No</th>
					<th >Waktu Pesan</th>
					<th >No.Tiket</th>
					<th >Waktu Berangkat</th>
					<th >Kode Jadwal</th>
					<th >Nama</th>
					<th >Telp</th>
					<th >Kursi</th>
					<th >Harga Tiket</th>
					<th >Discount</th>
					<th >Total</th>
					<th >Tipe Disc.</th>
					<th >CSO</th>
					<th >Status</th>
					<th >Ket.</th>
				</tr>
				<!-- BEGIN ROW -->
				<tr>
					<td>{ROW.no}</td>
					<td>{ROW.waktu_pesan}</td>
					<td>{ROW.no_tiket}</td>
					<td>{ROW.waktu_berangkat}</td>
					<td>{ROW.kode_jadwal}</td>
					<td>{ROW.nama}</td>
					<td>{ROW.telp}</td>
					<td>{ROW.no_kursi}</td>
					<td>{ROW.harga_tiket}</td>
					<td>{ROW.discount}</td>
					<td>{ROW.total}</td>
					<td>{ROW.tipe_discount}</td>
					<td>{ROW.cso}</td>
					<td>{ROW.status}</td>
					<td>{ROW.ket}</td>
				</tr>  
				<!-- END ROW -->
			</table>
			<!-- BEGIN NO_DATA_TIKET -->
			<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
			<!-- END NO_DATA_TIKET -->
			<hr>

			<h2>Detail Paket</h2>
			<table class="table table-responsive table-hover table-bordered"  width="100%" cellspacing="1" cellpadding="1">
				<tr>
					<th >No</th>
					<th >Waktu Berangkat</th>
					<th >No.Tiket</th>
					<th >Kode Jadwal</th>
					<th >Dari</th>
					<th >Untuk</th>
					<th >Harga Paket</th>
					<th >CSO</th>
					<th >Status</th>
					<th >Ket.</th>
			  </tr>
			  <!-- BEGIN ROWPAKET -->
				<tr class="{ROWPAKET.odd}">
					<td>{ROWPAKET.no}</td>
					<td>{ROWPAKET.waktu_berangkat}</td>
					<td>{ROWPAKET.no_tiket}</td>
					<td>{ROWPAKET.kode_jadwal}</td>
					<td>{ROWPAKET.dari}</td>
					<td>{ROWPAKET.untuk}</td>
					<td align="right">{ROWPAKET.harga_paket}</td>
					<td>{ROWPAKET.cso}</td>
					<td>{ROWPAKET.status}</td>
					<td>{ROWPAKET.ket}</td>
				</tr>  
			  <!-- END ROWPAKET -->
			</table>
			<!-- BEGIN NO_DATA_PAKET -->
			<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
			<!-- END NO_DATA_PAKET -->
			<hr>
				
			<h2>Detail Cargo</h2>
			<table class="table table-responsive table-hover table-bordered"  width="100%" cellspacing="1" cellpadding="1">
				<tr>
					<th >No</th>
					<th >Waktu Berangkat</th>
					<th width=120>AWB</th>
					<th >Destinasi</th>
					<th >Pick Up</th>
					<th >Dari</th>
					<th >Untuk</th>
					<th >Koli</th>
					<th >Berat</th>
					<th >Biaya</th>
					<th >CSO</th>
					<th >Status</th>
					<th >Ket.</th>
			  </tr>
			  <!-- BEGIN ROWCARGO -->
				<tr class="{ROWCARGO.odd}">
					<td>{ROWCARGO.no}</td>
					<td>{ROWCARGO.waktu_berangkat}</td>
					<td>{ROWCARGO.noawb}</td>
					<td>{ROWCARGO.destinasi}</td>
					<td>{ROWCARGO.pickup}</td>
					<td>{ROWCARGO.dari}</td>
					<td>{ROWCARGO.untuk}</td>
					<td align="right">{ROWCARGO.koli} Pax</td>
					<td align="right">{ROWCARGO.berat} Kg</td>
					<td align="right">{ROWCARGO.biaya}</td>
					<td>{ROWCARGO.cso}</td>
					<td>{ROWCARGO.status}</td>
					<td>{ROWCARGO.ket}</td>
				</tr>  
			  <!-- END ROWCARGO -->
			</table>
			<!-- BEGIN NO_DATA_CARGO -->
			<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
			<!-- END NO_DATA_CARGO -->
			<hr>
				
			<h2>Detail Biaya</h2>
			<table class="table table-responsive table-hover table-bordered"  width="100%" cellspacing="1" cellpadding="1">
				<tr>
					<th >No</th>
					<th >Waktu Trx</th>
					<th width=150>#Jadwal</th>
					<th width=150>Waktu Berangkat</th>
					<th width=150>#Manifest</th>
					<th width=150>Jenis Biaya</th>
					<th >Sopir</th>
					<th >Unit</th>
					<th >Jumlah</th>
					<th >Ket.</th>
			  </tr>
			  <!-- BEGIN ROWBIAYA -->
				<tr class="{ROWBIAYA.odd}">
					<td>{ROWBIAYA.no}</td>
					<td>{ROWBIAYA.waktu_transaksi}</td>
					<td>{ROWBIAYA.kode_jadwal}</td>
					<td>{ROWBIAYA.waktu_berangkat}</td>
					<td>{ROWBIAYA.nospj}</td>
					<td>{ROWBIAYA.jenis_biaya}</td>
					<td>{ROWBIAYA.sopir}</td>
					<td>{ROWBIAYA.unit}</td>
					<td align="right">{ROWBIAYA.jumlah}</td>
					<td>{ROWBIAYA.ket}</td>
				</tr>  
			  <!-- END ROWBIAYA -->
			</table>
			<!-- BEGIN NO_DATA_BIAYA -->
			<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
			<!-- END NO_DATA_BIAYA -->
			<hr>
		</div>
	</div>
</div>