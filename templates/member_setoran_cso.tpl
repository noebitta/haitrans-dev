<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>

<script language="JavaScript">

function konfirmasi(){
  dlg_konfirmasi.show();
}

function setOrder(sortby){
	ascending	= document.getElementById('hdn_asc').value;
	tampilkanData(sortby);
	document.getElementById('hdn_asc').value = 1-ascending;
}

function tampilkanData(sortby){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
    
		cari			= document.getElementById('txt_cari').value;
		ascending	= document.getElementById('hdn_asc').value;
    
		new Ajax.Updater("rewrite_hasil","member_setoran_cso.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=tampilkan_data&sortby="+sortby+"&ascending="+ascending+"&cari="+cari,
        onLoading: function(request) 
        {
            Element.show('progress');
						//Element.hide('rewritejam');
        },
        onComplete: function(request) 
        {
						Element.hide('progress');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function tampilkanDataDetail(operator,nama,uang_harus_setor,uang_outstanding){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		ascending	= document.getElementById('hdn_asc').value;
    
		new Ajax.Updater("rewrite_hasil","member_setoran_cso.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=tampilkan_data_detail"+
										"&operator="+operator+
										"&nama="+nama+
										"&uang_harus_setor="+uang_harus_setor+
										"&uang_outstanding="+uang_outstanding,
        onLoading: function(request) 
        {
            Element.show('progress');
						//Element.hide('rewritejam');
        },
        onComplete: function(request) 
        {
						Element.hide('progress');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function prosesSetor(){
	
	uang_harus_setor	= document.getElementById('uang_harus_setor').value;
	operator					= document.getElementById('operator').value;
	
	new Ajax.Request("member_setoran_cso.php?sid={SID}", 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"mode=setor"+
			"&total_setor="+uang_harus_setor+
			"&operator="+operator,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
		},
    onSuccess: function(request) 
    {
			cetakKwitansi(request.responseText);
			document.location="member_setoran_cso.php?sid={SID}";
		},
    onFailure: function(request) 
    {
    }
	})
	
}

// global
var dlg_konfirmasi,dlg_konfirmasiBtnYes,dlg_konfirmasiBtnNo;

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=no,resizable=yes");
}

function cetakKwitansi(kode_posting){
  //mencetak kwitansi
	Start("member_setoran_cso_kwitansi.php?sid={SID}&kode_posting="+kode_posting);
}

function init(e) {
  // inisialisasi variabel
		
	//dialog tanya hapus__________________________________________________________________
	dlg_konfirmasiBtnYes = document.getElementById("dlg_konfirmasiBtnYes");
	dlg_konfirmasiBtnNo = document.getElementById("dlg_konfirmasiBtnNo");
	dlg_konfirmasi = dojo.widget.byId("dlg_konfirmasi");
	dlg_konfirmasi.setCloseControl(dlg_konfirmasiBtnYes);
  dlg_konfirmasi.setCloseControl(dlg_konfirmasiBtnNo);
	tampilkanData('');
}

dojo.addOnLoad(init);



</script>

<div dojoType="dialog" width="400" id="dlg_konfirmasi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="100" style="display: none;">
<form onsubmit="return false;">
<font color='FFFFFF'><h3>Konfirmasi Setoran</h3></font>
<table bgcolor='FFFFFF'>
<tr>
  <td align="center">
		<br><br>
		<font color='red'>Perhatian: Data setoran yang sudah disimpan tidak bisa dibatalkan.</font><br>
		Silahkah klik [LAKUKAN] untuk melaksanakan operasi penyimpanan data<br>
		atau [Tidak] bila ingin membatalkannya<br><br><br>
	</td>
<tr>
	<td align="center">
		<br>
		<input type="button" onclick="prosesSetor();" id="dlg_konfirmasiBtnYes" value="LAKUKAN">
		<input type="button" id="dlg_konfirmasiBtnNo" value="&nbsp;Tidak&nbsp;">
	</td>
</tr>
</table>
</form>
</div>

<input type='hidden' id='hdn_asc' value='0' >
<input type='hidden' id='kode_posting' value='{KODE_POSTING}' >

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <table width='100%'>
    <tr>
    <td valign="middle" align="center">
		<h1>Posting Setoran CSO untuk Uang Top Up Member</h1>
		{U_ADD}
    <table width='100%'>
    <tr>
    </tr>
		<tr>
      <td align='right'>
				<!--field cari-->
				<input type="text" name="txt_cari" id="txt_cari">&nbsp;&nbsp;&nbsp;<input type="button" onClick="tampilkanData('');" value="Cari">
      </td>
    </tr>
    </table>
    <div id='rewrite_hasil'></div>
		<span id='progress' style='display:none;'><img src='./images/progress.gif' /> Sedang memproses...</span><br>
	{PESAN}
	{U_ADD}
 </td>
</tr>
</table>
