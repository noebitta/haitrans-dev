<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script language="JavaScript">
dojo.require("dojo.widget.Dialog");

function SetBlank(){
			id_promo.value						='';
			kode_jadwal.value					='';
			masa_berlaku_mula.value		='';
			masa_berlaku_akhir.value	='';
			discount_jumlah.value			='';
			discount_persentase.value	='';
			point.value								='';			
}

function ValidasiAngka(objek){
	temp_nilai=objek.value*0;
	nama_objek=objek.name;
	
	if(temp_nilai!=0){
		alert(nama_objek+" harus angka!");
		objek.setFocus;exit;
	}
	
}

function SimpanPromo(){
  
	if(layer_promo.value==''){
		alert("Layer promo tidak boleh kosong!");
		document.forms.f_data_promo.layer_promo.focus();
		return false;
	}
	
	if(masa_berlaku_mula.value==''){
		alert("Masa mulai berlaku tidak boleh kosong!");
		document.forms.f_data_promo.masa_berlaku_mula.focus();
		return false;
	}
	
	if(masa_berlaku_akhir.value==''){
		alert("Masa akhir berlaku tidak boleh kosong!");
		document.forms.f_data_promo.masa_berlaku_akhir.focus();
		return false;
	}
	
	//memeriksa apakah biaya sudah valid(berisi angka)
	
  if(aksi.value=='Penambahan'){
		//jika id_member kosong, berarti adalah proses penambahan data member baru
		mode="tambah_promo";
	}
	else{
		//jika id_member tidak kosong, berarti adalah proses pengubahan data member
		mode="ubah_promo";
	}

	new Ajax.Request("member_promo_detail.php?sid={SID}", 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"id_promo="+id_promo.value+
			"&kode_jadwal="+kode_jadwal.value+
			"&layer_promo="+layer_promo.value+
			"&masa_berlaku_mula="+masa_berlaku_mula.value+
			"&masa_berlaku_akhir="+masa_berlaku_akhir.value+
			"&point="+point.value+
			"&discount_jumlah="+discount_jumlah.value+
			"&discount_persentase="+discount_persentase.value+
			"&mode="+mode,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
		},
    onSuccess: function(request) 
    {
      if(request.responseText=='berhasil'){
				if(aksi.value=='Penambahan'){
					SetBlank();
					alert("Data Promo berhasil ditambahkan!");
				 //alert(request.responseText);
				}
				else{
					//kembali ke halaman member
					alert("Data Promo berhasil disimpan!");
					document.location="member_promo.php?sid="+sid.value;
				}
			}
			else{
				alert("Isian penulisan tanggal anda salah, silahkan periksa kembali");
				document.forms.f_data_promo.masa_berlaku_mula.focus();
			}

		},
    onFailure: function(request) 
    {
       
    }
  })      
}

// global
var DataPromoBtnSimpan,DataPromoBtnNo,id_promo;

function init(e) {
  // inisialisasi variabel
	
	DataPromoBtnSimpan 	= document.getElementById("DataPromoBtnSimpan");
	DataPromoBtnNo 			= document.getElementById("DataPromoBtnNo");
	
  aksi								=document.getElementById("aksi");
	layer_promo					=document.getElementById("layer_promo");
	id_promo						=document.getElementById("hdn_id_promo");
	kode_jadwal					=document.getElementById("kode_jadwal");
	masa_berlaku_mula		=document.getElementById("masa_berlaku_mula");
	masa_berlaku_akhir	=document.getElementById("masa_berlaku_akhir");
	point								=document.getElementById("point");
	discount_jumlah			=document.getElementById("discount_jumlah");
	discount_persentase	=document.getElementById("discount_persentase");

}

dojo.addOnLoad(init);

</script>
<input type='hidden' name='sid' id='sid' value='{SID}'>
<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer" >{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
		<!-- BODY -->
		<font><h2><u>{PESAN} Data Member</u></h2></font>
		
		<input type='hidden' name='aksi' id='aksi' value='{PESAN}'>
		<input type='hidden' name='hdn_id_promo' id='hdn_id_promo' value='{ID_PROMO}'>
		<form id='f_data_promo' name='f_data_promo' method="post">		
		<table bgcolor='FFFFFF' width='100%'> <!-- table 2 kolom -->
			<tr>
				<td valign='top'align='center'>
					<table> <!-- tabel kolom 1-->
						<tr>
						  <td width='30%' class='kolomwajib'>Layer Promo</td>
							<td width='5%'>:</td>
							<td>
								<select  name="Layer promo" id="layer_promo" >{LAYER_PROMO}</select>
							</td>
						</tr>
						<tr>
						  <td class='kolomwajib'>Rute</td>
							<td>:</td>
							<td>
								<input type='text' id='kode_jadwal' value='{KODE_JADWAL}' />
							</td>
						</tr>
						<tr>
						  <td class='kolomwajib'>Berlaku mulai</td>
							<td>:</td>
							<td><input  id="masa_berlaku_mula" type="text" maxlength='10' value='{MASA_BERLAKU_MULA}'>(DD/MM/YYYY)</td>
						</tr>
						<tr>
						  <td class='kolomwajib'>Berakhir pada</td>
							<td>:</td>
							<td><input  id="masa_berlaku_akhir" type="text" maxlength='10' value='{MASA_BERLAKU_AKHIR}'>(DD/MM/YYYY)</td>
						</tr>
						
					</table> <!--END tabel kolom 2-->
				</td>
				<td bgcolor='D0D0D0'><!--garis--></td>
				<td valign='top' align='center'>
					<table> <!--tabel kolom 2-->
						<tr>
						  <td>Point</td>
							<td>:</td>
							<td><input name="point" id="point" type="text" maxlength='3' value='{POINT}'></td>
						</tr>
						<tr>
						  <td>Discount (Rp.)</td>
							<td>:</td>
							<td><input name="Discount Rp" id="discount_jumlah" type="text" maxlength='6' value='{DISCOUNT_JUMLAH}'></td>
						</tr>
						<tr>
						  <td>Discount (%)</td>
							<td>:</td>
							<td><input name="Discount persen" id="discount_persentase" type="text" maxlength='3' value='{DISCOUNT_PERSENTASE}'>%</td>
						</tr>
						<tr>
						  <td colspan='3' height='20'></td>
						</tr>
					</table> <!--END tabel kolom 2-->
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
						  <td>Dibuat oleh</td>
							<td>:</td>
							<td><label>{DIBUAT_OLEH}</label></td>
						</tr>
						<tr>
						  <td>Dibuat pada</td>
							<td>:</td>
							<td><label>{WAKTU_DIBUAT}</label></td>
						</tr>
						<tr>
						  <td>Data diubah oleh</td>
							<td>:</td>
							<td><label>{DIUBAH_OLEH}</label></td>
						</tr>
						<tr>
						  <td>Diubah pada</td>
							<td>:</td>
							<td><label>{WAKTU_DIUBAH}</label></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colspan='3' bgcolor='C0C0C0' valign='middle' height='40'>
					<input type="button" id="DataPromoBtnSimpan" onClick="SimpanPromo();" value="&nbsp;Simpan&nbsp">
					<input type="button" onClick="javascript:document.location='member_promo.php?sid={SID}';" id="DataPromoBtnNo" value="&nbsp;Batal&nbsp;">
				</td>
			</tr>
		</table><!--END tabel 2 kolom-->
		</form>
	</td>
</tr>
</table>