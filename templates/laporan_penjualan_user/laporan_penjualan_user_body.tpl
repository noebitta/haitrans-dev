<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<div class="container" style="width: 90%;">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Penjualan {NAMA}</td>
							<td colspan=2 align='left' class="" valign='middle'>
								<br>
								<form action="{ACTION_CARI}" method="post">
									<div class="col-md-4">
										Periode:<br /><input class="form-control" readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
									</div>
									<div class="col-md-4">
										s/d<br />
										<input class="form-control" readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">	
									</div>
									<div class="col-md-4">
										<input class="mybutton btn topwidth" style="margin-top: 16px;" type="submit" value="cari" />
									</div>
								</form>
							</td>
						</tr>
					</table>
				 	<table  width='100%' cellspacing="0">
						<tr>
							<td>{SUMMARY}</td>
						</tr>
					</table>
				 	<table width='100%' cellspacing="0">
						<tr>
							<td align='right' valign='bottom'>
								{PAGING}
							</td>
						</tr>
					</table>
					<table class="table table-bordered table-hover">
				    <tr>
				       <th width=30>No</th>
							 <th width=200>Waktu Pesan</th>
							 <th width=200>No.Tiket</th>
							 <th width=200>Waktu Berangkat</th>
							 <th width=100>Kode Jadwal</th>
							 <th width=200>Nama</th>
						     <th width=200>Nama</th>
							 <th width=50>Kursi</th>
							 <th width=100>Harga Tiket</th>
							 <th width=100>Discount</th>
							 <th width=70>Total</th>
							 <th width=100>Tipe Disc.</th>
							 <th width=200>CSO</th>
							 <th width=100>Status</th>
							 <th width=100>Ket.</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td><div align="right">{ROW.no}</div></td>
				       <td><div align="left">{ROW.waktu_pesan}</div></td>
							 <td><div align="left">{ROW.no_tiket}</div></td>
				       <td><div align="left">{ROW.waktu_berangkat}</div></td>
				       <td><div align="left">{ROW.kode_jadwal}</div></td>
							 <td><div align="left">{ROW.nama}</div></td>
						 <td><div align="left">{ROW.nohp}</div></td>
							 <td><div align="center">{ROW.no_kursi}</div></td>
							 <td><div align="right">{ROW.harga_tiket}</div></td>
							 <td><div align="right">{ROW.discount}</div></td>
							 <td><div align="right">{ROW.total}</div></td>
							 <td><div align="left">{ROW.tipe_discount}</div></td>
							 <td><div align="left">{ROW.cso}</div></td>
				       <td><div align="center">{ROW.status}</div></td>
				       <td><div align="left">{ROW.ket}</div></td>
				     </tr>  
				     <!-- END ROW -->
				    </table>
					<table width='100%'>
						<tr>
							<td align='right' width='100%'>
								{PAGING}
							</td>
						</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>
