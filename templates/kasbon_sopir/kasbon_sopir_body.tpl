<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
</script>

<script LANGUAGE="javascript">

var DialogInput,DialogError,OptSopir,TglTransaksi,JumlahKasbon;

function init(e) {
  // inisialisasi variabel
	
	//control dialog box
	//set position
	DialogInput	= document.getElementById("dialog_input");
	DialogInput.setAttribute("style", "display:none;width:400px;left:"+ (screen.width-400)/2 +"px;top:150px;");
	
	OptSopir			= document.getElementById("opt_sopir");
	JumlahKasbon	= document.getElementById("txt_jumlah");
	TglTransaksi	= document.getElementById("tanggal_transaksi");
	DialogError		= document.getElementById("dialog_error");
	
}

dojo.addOnLoad(init);

function showDialogInput(){
	OptSopir.value			= '';
	JumlahKasbon.value	= '';
	DialogInput.show();
}

function validasiAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function postingKasbon(){
	new Ajax.Request("kasbon_sopir.php?sid={SID}", 
  {
    asynchronous: true,
    method: "post",
    parameters: 
			"mode=1"+
			"&TglTransaksi="+TglTransaksi.value+
			"&KodeSopir="+OptSopir.value+
			"&Jumlah="+JumlahKasbon.value,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {			
			eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  });
}

function batalkanTransaksi(ID){
	
	is_yes	= confirm("Apakah anda yakin akan membatalkan transaksi ini?");
	
	if(is_yes){
		new Ajax.Request("kasbon_sopir.php?sid={SID}", 
	  {
	    asynchronous: true,
	    method: "post",
	    parameters: 
				"mode=2"+
				"&ID="+ID,
	    onLoading: function(request) 
	    {
	    },
	    onComplete: function(request) 
	    {
	    },
	    onSuccess: function(request) 
	    {			
				dialog_error.innerHTML=request.responseText;
				dialog_error.show();
				eval(request.responseText);
			},
	    onFailure: function(request) 
	    {
	       alert('Error !!! Cannot Save');        
	       assignError(request.responseText);
	    }
	  });
	}
}

</script>

<!-- dialog error -->
<span id="dialog_error" class="show_popup" style="display:none;top:0px;left:0px;"></span>
<!-- END dialog error-->

<!--dialog Input Kasbon-->
<span id="dialog_input" class="show_popup" style="display:none;">
<table width='100%'>
<tr><td align='center' style="font-size:18px;color:#ffffff;">&nbsp;<b>Kasbon Sopir</b></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr><td>Tgl.Transaksi</td><td>:</td><td><input readonly="yes"  id="tanggal_transaksi" name="tanggal_transaksi" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10></td></tr>
			<tr><td>Sopir</td><td>:</td><td><select id="opt_sopir" name="opt_sopir">{OPT_SOPIR}</select></td></tr>
			<tr><td>Jumlah Rp.</td><td>:</td><td><input type="text" id="txt_jumlah" name="txt_jumlah" onkeypress='validasiAngka(event);' maxlength="6" /></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" onclick="DialogInput.hide();" id="dlg_input_btn_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="postingKasbon();" id="dlg_input_btn_ok" value="&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;">
	</td>
</tr>
</table>
</span>
<!--END dialog Input Kasbon-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<!--HEADER-->
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td align='center' valign='middle' class="bannerjudul">&nbsp;Kasbon Sopir</td>
			<td align='right' valign='middle'>
				<form id="form_cari" name="form_cari" action="{ACTION_CARI}" method="post">
				<table>
					<tr><td class='bannernormal'>
						Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
						&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
						&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
						<input type="submit" value="cari" />&nbsp;
						<input type='hidden' name='mode' value='0'/>
					</td></tr>
				</table>
				</form>
			</td>
		</tr>
		<tr>
			<td colspan=2 align='center' valign='middle'>
				<table>
					<tr>
						<td>
							 &nbsp;
						</td><td bgcolor='D0D0D0'></td>
						<td>
							<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><a href='#' onClick='false;showDialogInput();'>[+] Tambah Kasbon</a></td><td align='right'>{PAGING}</td></tr>
	</table>
	<!-- END HEADER-->
	<table class="border" width='100%' >
  <tr>
     <th width=30>No</th>
		 <th width=200><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Tgl.Trx</a></th>
		 <th width=200><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Nama Sopir</a></th>
		 <th width=100><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>NRP</a></th>
		 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Jumlah</a></th>
		 <th width=200><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Kasir</a></th>
		 <th width=300><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Keterangan</a></th>
		 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Action</a></th>
   </tr>
   <!-- BEGIN ROW -->
   <tr class="{ROW.odd}">
     <td ><div align="right">{ROW.no}</div></td>
     <td ><div align="left"><b>{ROW.TglTransaksi}</b></div></td>
     <td ><div align="left"><b>{ROW.NamaSopir}</b></div></td>
		 <td ><div align="left">{ROW.KodeSopir}</div></td>
     <td ><div align="right">{ROW.Jumlah}</div></td>
		 <td ><div align="left">{ROW.Kasir}</div></td>
		 <td ><div align="left">{ROW.Keterangan}</div></td>
		 <td ><div align="center">{ROW.Action}</div></td>
   </tr>
   <!-- END ROW -->
  </table>
	<table width='100%'>
		<tr>
			<td align='right' width='100%'>
				{PAGING}
			</td>
		</tr>
		<tr>
			<td align='left' valign='bottom' colspan=3>
			{SUMMARY}
			</td>
		</tr>
	</table>
 </td>
</tr>
</table>