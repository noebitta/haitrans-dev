<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function toggleSelect(){
	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			if(chk.checked==true){
				chk.checked=false;
			}
			else{
				chk.checked=true;
			}
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);
}


function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
			
		new Ajax.Request("pengaturan_pengumuman.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_pengumuman="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}
	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12 box">
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td class="whiter" valign="middle" align="center">		
					<table width='100%' cellspacing="0">
						<tr class='' height=40>
							<td align='center' valign='middle' class="bannerjudul">&nbsp;Pengumuman</td>
							<td colspan=2 align='right' class="bannernormal" valign='middle'>
								<form action="{ACTION_CARI}" method="post">
									<div class="input-group">
								      <input type="text" class="form-control" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />
								      <span class="input-group-btn">
								        <input type="submit" class="tombol btn btn-default form-control" value="cari" />
								      </span>
								    </div><!-- /input-group -->
								</form>
							</td>
						</tr>
						<tr height=30><td></td></tr>
						<tr>
							<td class="mytd" align='left'>
								<a href="{U_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
								<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
							<td class="mytd" width='70%' align='right'>
								{PAGING}
							</td>
						</tr>
					</table>
					<table width='100%' class="border table table-hover table-bordered">
				    <tr>
				       <th style="padding-left: 20px;"><input type='checkbox' onclick="toggleSelect();" id='ceker' /></th>
				       <th >No</th>
							 <th >Tanggal</th>
							 <th >Kode</th>
							 <th >Judul Pengumuman</th>
							 <th >Pengumuman</th>
							 <th >Pembuat</th>
							 <th >Action</th>
				     </tr>
				     <!-- BEGIN ROW -->
				     <tr class="{ROW.odd}">
				       <td><div align="center">{ROW.check}</div></td>
				       <td><div align="center">{ROW.no}</div></td>
				       <td><div>{ROW.tanggal}</div></td>
				       <td><div>{ROW.kode}</div></td>
							 <td><div>{ROW.judul}</div></td>
				       <td><div>{ROW.pengumuman}</div></td>
							 <td><div>{ROW.pembuat}</div></td>
				       <td><div>{ROW.action}</div></td>
				     </tr>  
				     <!-- END ROW -->
					 {NO_DATA}
			    </table>
			    <table width='100%'>
						<tr>
						<td class="mytd" align='left'>
							<a href="{U_ADD}"><i class="fa fa-plus"></i> Tambah</a>&nbsp;|&nbsp;
							<a href="" onClick="return hapusData('');"><i class="fa fa-trash-o"></i> Hapus</a></td>
						<td class="mytd" width='70%' align='right'>
							{PAGING}
						</td>
					</tr>
					</table>
			 </td>
			</tr>
			</table>
		</div>
	</div>
</div>