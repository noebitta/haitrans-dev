<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function ubahStatus(id_discount){
        //ubah account

        new Ajax.Request("jenis_discount.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "mode=ubah_status"+"&id_discount="+id_discount,
                    onLoading: function(request)
                    {
                    },
                    onComplete: function(request)
                    {

                    },
                    onSuccess: function(request)
                    {
                        window.location.reload();
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    function validasi(evt){
        var theEvent = evt || window.event;

        var key = theEvent.keyCode || theEvent.which;

        key = String.fromCharCode(key);

        var regex = /[0-9]/;

        if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 ||
                [evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;

        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            theEvent.preventDefault();
        }
    }

    function showDialog() {
        dlg.show();
    }

    function showEditDialog(id_discount,jml_discount,kode_discount,nama_discount,flag_kota) {
        document.getElementById("edit_kode_discount").value = kode_discount;
        document.getElementById("edit_jml_discount").value = jml_discount;
        dialogEdit.show();
    }

    function hapus(id_discount) {

        new Ajax.Request("jenis_discount.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "mode=hapus&id_discount="+id_discount,
                    onLoading: function(request)
                    {
                    },
                    onComplete: function(request)
                    {

                    },
                    onSuccess: function(request)
                    {
                        if(request.responseText==1){
                            alert("Data Berhasil Dihapus!");
                            window.location.reload();
                        }
                        else{
                            alert("Gagal menyimpan data discount");
                        }
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    function Simpan(){

        kodediscount = document.getElementById("jenis").value;
        namadiscount = document.getElementById("nama").value;
        besar_discount = document.getElementById("txt_besar_discount").value;
        flag_kota = document.getElementById("luar").value

        my_parameter="kode_discount="+kodediscount+"&besar_discount="+besar_discount+"&mode=tambah&flag_kota="+flag_kota+"&nama_discount="+namadiscount;


        new Ajax.Request("jenis_discount.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: my_parameter,
                    onLoading: function(request)
                    {
                    },
                    onComplete: function(request)
                    {

                    },
                    onSuccess: function(request)
                    {
                        if(request.responseText==1){
                            alert("Data Berhasil disimpan!");
                            window.location.reload();
                        }
                        else{
                            alert("Gagal menyimpan data discount");
                        }
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    function update(){

    }

    function init(e){
        //control dialog paket
        dlg						= dojo.widget.byId("dialog");
        dlg_btn_cancel = document.getElementById("dialogcancel");

        dialogEdit = dojo.widgetbyId("dialogedit")
        dialogEditcancel = document.getElementById("dialogeditcancel")
    }

    dojo.addOnLoad(init);
</script>
<!--dialog Create Voucher-->
<div dojoType="dialog" id="dialog" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;" id="myForm">
        <table width="500" style="background: white;">
            <tr>
                <td>
                    <input type="button" style="border-radius: 0;" class="button-close" id="dialogcancel" onClick="dlg.hide();" value="&nbsp;X&nbsp;">
                    <h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">JENIS DISCOUNT</h4>
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <table class="table">
                        <tr>
                            <td align='right'>
                                Kode Discount :
                            </td>
                            <td>
                                <select class="form-control" id='jenis' name='jenis' >
                                    <option value="">-- PILIH KODE DISCOUNT --</option>
                                    <option value="R">PULANG PERGI</option>
                                    <option value="KK">SPESIAL TIKET</option>
                                    <option value="M">MAHASISWA</option>
                                    <option value="G">GRATIS</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Nama Discount :</td>
                            <td>
                                <input type="text" id="nama" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td align='right'>
                                Jenis Discount :
                            </td>
                            <td>
                                <select class="form-control" id='luar' name='luar' >
                                    <option value="1">LUAR KOTA</option>
                                    <option value="0">DALAM KOTA</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Besarnya Discout (Rp.) :</td>
                            <td>
                                <input type="text" id="txt_besar_discount" class="form-control" onkeypress="validasi(event);">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center">
                                <input class="btn mybutton paper" type="button" onclick="Simpan();" id="dialogproses" value="&nbsp;Proses&nbsp;">
                            </td>
                        </tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog create voucher-->

<!--dialog Edit-->
<div dojoType="dialog" id="dialogedit" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;" id="Form">
        <table width="500" style="background: white;">
            <tr>
                <td>
                    <input type="button" style="border-radius: 0;" class="button-close" id="dialogeditcancel" onClick="dlg.hide();" value="&nbsp;X&nbsp;">
                    <h4 class="formHeader sectiontitle left" style="margin-top: 5px; margin-left: 10px;">EDIT JENIS DISCOUNT</h4>
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <table class="table">
                        <tr>
                            <td align='right'>
                                Kode Discount :
                            </td>
                            <td>
                                <select class="form-control" id='jenis' name='edit_kode_discount' >
                                    <option value="">-- PILIH KODE DISCOUNT --</option>
                                    <option value="R">PULANG PERGI</option>
                                    <option value="KK">SPESIAL TIKET</option>
                                    <option value="M">MAHASISWA</option>
                                    <option value="G">GRATIS</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Nama Discount :</td>
                            <td>
                                <input type="text" id="edit_nama_discount" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td align='right'>
                                Jenis Discount :
                            </td>
                            <td>
                                <select class="form-control" id='luar' name='edit_flag_kota' >
                                    <option value="1">LUAR KOTA</option>
                                    <option value="0">DALAM KOTA</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Besarnya Discout (Rp.) :</td>
                            <td>
                                <input type="text" id="edit_jml_discount" class="form-control" onkeypress="validasi(event);">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center">
                                <input class="btn mybutton paper" type="button" onclick="update();" id="dialogproses" value="&nbsp;Proses&nbsp;">
                            </td>
                        </tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog Edit-->


<div class="container">
    <div class="row">
        <div class="col-md-12 box">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="whiter" valign="middle" align="left">
                            <!--HEADER-->
                            <table width='100%' cellspacing="0">
                                <tr class='' height=40>
                                    <td align='center' valign='top' class="bannerjudulw"><div class="bannerjudul">Jenis Discount</div></td>
                                </tr>
                            </table>
                            <table style="margin-top: 20px; margin-bottom: 20px; width: 100%">
                                <tr>
                                    <td align="left" valign="bottom">
                                        <div style="float: left"><a href="#" onclick="showDialog();return false;"> + Tambah Discount</a></div>
                                    </td>
                                </tr>
                            </table>
                            <!-- END HEADER-->
                            <table width='100%' class="table table-bordered table-hover">
                                <tr>
                                    <th>No</th>
                                    <th>Kode Discount</th>
                                    <th>Nama Discount</th>
                                    <th>Jumlah Discount</th>
                                    <th>Status</th>
                                    <th>Jenis</th>
                                    <th>Action</th>
                                </tr>
                                <!-- BEGIN ROW -->
                                <tr class="{ROW.odd}">
                                    <td align="center">{ROW.no}</td>
                                    <td align="center">{ROW.Kode}</td>
                                    <td align="left">{ROW.Nama}</td>
                                    <td align="left">{ROW.Jumlah}</td>
                                    <td align="left">{ROW.Status}</td>
                                    <td align="left">{ROW.Jenis}</td>
                                    <td align="center">{ROW.update}&nbsp;|&nbsp;{ROW.hapus}</td>
                                </tr>
                                <!-- END ROW -->
                            </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
