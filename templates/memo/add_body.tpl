<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('memo_invalid');
	
	if(document.getElementById('jadwal')){
		jadwal	= document.getElementById('jadwal').value;
	}
	else{
		jadwal	= "";
	}
	
	memo		= document.getElementById('memo');
	
	if(jadwal==''){
		valid=false;
		alert("Anda belum memilih jadwal memo");
	}
	
	if(memo.value==''){
		valid=false;
		Element.show('memo_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

function getUpdateAsal(kota_asal,mode){
	// fungsi ini mengubah RUTE menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   			
	
	//mengambil tanggal yang diinput user dan rute
	
	if(document.getElementById('p_tgl_user')){
		tgl_user  = document.getElementById('p_tgl_user').value;
	}
	else{
		tgl_user	='{TGL_BERANGKAT}';
	}
	
	if(mode==0){
		
		if(document.getElementById('rewritetujuan')){
			document.getElementById('rewritetujuan').innerHTML = "";
			document.getElementById('rewritejadwal').innerHTML = "";
		}
		
		new Ajax.Updater("rewriteasal","pengaturan_memo.php?sid={SID}", {
			asynchronous: true,
			method: "get",

			parameters: "mode=asal&kota_asal="+kota_asal+"&asal={ASAL}",
			onLoading: function(request){
				Element.show('progress_asal');
			},
			onComplete: function(request){
				Element.show('rewriteasal');
				Element.hide('progress_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});
		
	}
	
			
}

function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewritetujuan')){
			document.getElementById('rewritetujuan').innerHTML = "";
		}
		
		if(document.getElementById('rewritejadwal')){
			document.getElementById('rewritejadwal').innerHTML = "";
		}
		
		new Ajax.Updater("rewritetujuan","pengaturan_memo.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=tujuan",
        onLoading: function(request) 
        {
            Element.show('progress_tujuan');
						//Element.hide('rewritejadwal');
        },
        onComplete: function(request) 
        {
            Element.show('rewritetujuan');
						Element.hide('progress_tujuan');
						//Element.show('rewriteall');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJadwal(id_jurusan){
			
	if(document.getElementById('rewritejadwal')){
		document.getElementById('rewritejadwal').innerHTML = "";
		tgl=document.getElementById('p_tgl_user').value;
	}
	else{
		tgl='{TGL_BERANGKAT}';
	}
	
	
	
	new Ajax.Updater("rewritejadwal","pengaturan_memo.php?sid={SID}", 
	{
		asynchronous: true,
		method: "get",
		parameters: "tgl=" + tgl + "&id_jurusan=" + id_jurusan +"&jadwal={JADWAL}&mode=jam",
		onLoading: function(request) 
		{
			Element.show('progress_jam');
		},
			onComplete: function(request) 
		{
			Element.hide('progress_jam');
		  Element.show('rewritejadwal');
		},                
		onFailure: function(request) 
		{ 
			assignError(request.responseText);
		}
	});
}

function getMemo(){
			
	if(document.getElementById('rewritejadwal')){
		tgl					= document.getElementById('p_tgl_user').value;
		kode_jadwal	= document.getElementById('jadwal').value;
	}
	else{
		return;
	}
	
	new Ajax.Request("pengaturan_memo.php?sid={SID}", 
  {
     asynchronous: true,
     method: "get",
     parameters: "tgl_berangkat=" + tgl + "&kode_jadwal="+ kode_jadwal +"&mode=getmemo",
     onLoading: function(request) 
     {
      
     },
     onComplete: function(request) 
     {
       
     },
     onSuccess: function(request) 
     {
				eval(request.responseText);
		 },
     onFailure: function(request) 
     {
				assignError(request.responseText);
     }
  });
}

getUpdateAsal('{KOTA_ASAL}',0);
getUpdateTujuan('{ASAL}');
getUpdateJadwal('{TUJUAN}');

</script>

<form name="frm_data" action="{U_MEMO_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td align='center' valign='middle' class="bannerjudul">&nbsp;Memo</td>
		</tr>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='400'>
				<table width='600' border=0	>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <input type="hidden" name="id_pengumuman" value="{ID_PENGUMUMAN}">
			      <input type="hidden" name="kode_old" value="{KODE_OLD}">
			    </tr>
					<tr>
						<td width=200>Tanggal keberangkatan</td><td width=1>:</td>
						<td><input readonly="yes"  id="p_tgl_user" name="p_tgl_user" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERANGKAT}" size=15 {DISABLED}></td>
					</tr>
					<tr>
						<td>Kota keberangkatan</td><td>:</td>
						<td><select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal' {DISABLED}>{OPT_KOTA}</select></td>
					</tr>
					<tr>
						<td>Point asal</td><td>:</td>
						<td><div id="rewriteasal"></div></td>
					</tr>
					<tr>
						<td>Point tujuan</td><td>:</td>
						<td><div id="rewritetujuan"></div></td>
					</tr>
					<tr>
						<td>Jam berangkat</td><td>:</td>
						<td><div id="rewritejadwal"></div></td>
					</tr>	
					<tr>
						<td valign='top'>Memo</td>
						<td valign='top'>:</td>
						<td>
							<textarea name="memo" id="memo" cols="30" rows="3"  onChange="Element.hide('memo_invalid');">{MEMO}</textarea>
							<span id='memo_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
						<td valign='top'>Pembuat Memo</td>
						<td valign='top'>:</td>
						<td>
							<b><span id='nama_pembuat'>{PEMBUAT}</span></b>
						</td>
					</tr>
					<tr>
						<td valign='top'>Waktu Buat</td>
						<td valign='top'>:</td>
						<td>
							<b><span id='waktu_pembuat'>{WAKTU_BUAT}</span></b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>