<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
		window.opener.getUpdateMobil();
	}
</script>	 

<body class='tiket'>
	
<!-- BEGIN ROW -->
<div style="font-size:14px;width:400px;height: 470px;">
	<span style="font-size:22px;">{ROW.NAMA_PERUSAHAAN}</span><br>
	{ROW.NO_TIKET}<br>
	{ROW.DUPLIKAT}
	By:{ROW.JENIS_BAYAR}<br>
	Pergi:{ROW.TGL_BERANGKAT}<br>
	{ROW.JURUSAN}<br>
	<br>
	<div style="text-align: left;width:400px;padding-left:30px;">
		{ROW.JAM_BERANGKAT}<br>
		{ROW.NAMA_PENUMPANG}<br>
		Kursi<br>
		<span style="font-size:22px;">{ROW.NOMOR_KURSI}</span><br>
		{ROW.NAMA}<br>
	</div>
	{ROW.HARGA_TIKET}
	Disc.:{ROW.DISKON}<br>
	Bayar:{ROW.BAYAR}<br>
	{ROW.POINT_MEMBER}
	CSO:{ROW.CSO}<br>
	WaktuCetak<br>
	{ROW.WAKTU_CETAK}<br>
	<br>
	{ROW.PESAN}<br>
	<br>
	<div style="text-align: center;width:400px;"">-- Terima Kasih --</div><br>
</div>
<!-- END ROW -->
<!-- BEGIN kupon_undian -->
<div style="font-size:14px;width:400px; height: 470px;">
	
	<div style="text-align: center;width:400px;">
		<br><br><br>
		---KUPON UNDIAN--<br>
		<br>
		<span style="font-size:20px;">{KODE_KUPON}</span><br><br>
		<b>{ID_MEMBER}</b><br>
		Nama:{NAMA_PENUMPANG}<br><br>
		Telp:{TELP_PENUMPANG}<br><br>
		#Tiket:{NO_TIKET}<br>
		WaktuCetak<br>
		{WAKTU_CETAK}<br>
		CSO:{PENCETAK_KUPON}<br><br><br>
	</div>
</div>
<!-- END kupon_undian -->
<!-- BEGIN VOUCHER -->
<div style="font-size:14px;width:400px; height: 450px;">
	<div style="text-align: center;width:400px;">
		<br><br><br>
		---------VOUCHER--------<br>
		<br>
		Nama:{VOUCHER.NAMA_PENUMPANG}<br><br>
		{VOUCHER.JURUSAN_VOUCHER}<br><br>
		TIKET BALIK<br><br>
		FREE<br><br>
		<span style="font-size:20px;">{VOUCHER.KODE_VOUCHER}</span><br><br>
		berlaku hingga<br>
		{VOUCHER.EXPIRED_VOUCHER}<br><br>
		{VOUCHER.PESAN_TUSLAH_VOUCHER}
	</div>
	<br><br><br>
</div>
<!-- END VOUCHER -->
</body>

<script language="javascript">
	printWindow();
	window.close();
</script>
</html>
