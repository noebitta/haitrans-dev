<html>
	<head>
		<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
	</head>
	<script type="text/javascript">
		function printWindow() {
			bV = parseInt(navigator.appVersion);
			if (bV >= 4) window.print();
			window.opener.getUpdateMobil();
		}
	</script>	

	<body class='spj'>
	<div style="font-size:14px;width:600px;height: 470px;">
		<div style="font-size:22px;"> -- [MANIFEST] -- {DUPLIKAT}</div>
		<div style="font-size:22px;">{NAMA_PERUSAHAAN}</div>
		{NO_SPJ}<br>
    {TRANSIT}
		{TGL_BERANGKAT}<br>
		{JURUSAN}<br>
		Tgl.Cetak:<br>
		{TGL_CETAK}<br>
	
		Mobil:{NO_POLISI}<br>
		Sopir:{SOPIR}<br>
		DAFTAR PENUMPANG {TIDAK_ADA_PENUMPANG}<br>
    <br>
		<!-- BEGIN ROW_PENUMPANG -->
		{ROW_PENUMPANG.NOMOR_KURSI}|{ROW_PENUMPANG.NAMA}|{ROW_PENUMPANG.NO_TIKET}<br>
    {ROW_PENUMPANG.RUTE}<br>
    {ROW_PENUMPANG.KETERANGAN}
    {ROW_PENUMPANG.TELP}<br>
    <br>
		<!-- END ROW_PENUMPANG -->
		Jum.Pnp:{JUMLAH_PENUMPANG}<br>   
		{JENIS_PENUMPANG}<br>
		{LIST_OMZET_PENUMPANG}
		---------------------------<br>
		Tunai:{PENDAPATAN_TUNAI}<br>
		Biaya:{TOTAL_BIAYA}<br>
		CSO: {CSO}<br>
		<font size='2'>&nbsp;&nbsp;&nbsp;&nbsp;{EMAIL_PERUSAHAAN}</font><br>
		<font size='2'>&nbsp;&nbsp;&nbsp;&nbsp;{WEBSITE_PERUSAHAAN}</font><br>
		<!-- BEGIN VOUCHER_BBM -->
		<br><br>
		<div style="font-size:22px;"> -- [VOUCHER BBM] -- {DUPLIKAT}</div><br>
		<div style="font-size:18px;">{NAMA_PERUSAHAAN}</div>
		BBM-{NO_SPJ}<br>
		Untuk Unit:<b>{NO_POLISI}</b><br>
		Sopir:{SOPIR}<br>
		mohon untuk mengisi<br>
		bahan bakar kendaraan<br>
		tersebut Sejumlah:<br>
		<b>Rp.{BIAYA_BBM}</b><br>
		Petugas Cetak: {CSO}<br><br>
		TTD Petugas SPBU<br><br><br><br>
		........................<br>
		-Terima kasih-
		<br><br>
		-------potong disini-------<br>
		<br>
		<div style="font-size:22px;"> -- [VOUCHER BBM] -- {DUPLIKAT}</div><br>
		<div style="font-size:18px;">{NAMA_PERUSAHAAN}</div>
		UNTUK DIBERIKAN KEPADA<br>
		PETUGAS SPBU<br>
		BBM-{NO_SPJ}<br>
		Untuk Unit:<b>{NO_POLISI}</b><br>
		Sopir:{SOPIR}<br>
		telah diisi BBM <br>
		Jenis: SOLAR<br>
		Vol: 30 liter<br>
		Jumlah: Rp. 225.000<br>
		Petugas: {CSO}<br><br>
		TTD Petugas<br><br><br><br>
		........................<br>
		-Terima kasih-
		<!-- END VOUCHER_BBM -->
		<br>
		<br>
	</div>
	-
	<script language="javascript">
		printWindow();
		window.close();
	</script>
	</body>
	</html>