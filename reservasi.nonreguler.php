<?php
//
// RESERVASI
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';

include($adp_root_path . 'common.php');
// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){
  redirect(append_sid('index.'.$phpEx),true);
}

//INCLUDES
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassReservasiNonReguler.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassPromo.php');


// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$user_id  	= $userdata['user_id']; 
$user_level	= $userdata['user_level'];

// membuat option Asal
function OptionAsal($kota){
	global $db;
	
	$Cabang	= new Cabang();
	
	$result	= $Cabang->setComboCabang($kota);
	
  $opt = "";
	
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
      $opt .= "<option value='$row[0]'>$row[1]</option>";
    }    
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
			
  return $opt;
}

// membuat option jurusan
function OptionJurusan($asal){
	
	global $db;
	
	$Jurusan	= new Jurusan();
	
	$result = $Jurusan->setComboJurusan($asal);
	
  $opt = "";
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
			$opt .= "<option value='$row[0]'>$row[1] ($row[2])</option>";
    }  
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
	
  return $opt;
}

// membuat option jam
function OptionJadwal($tgl,$id_jurusan){
  global $db;
	
  if ($id_jurusan!=''){
		
		$Jadwal	= new Jadwal();
		
		$result	= $Jadwal->setComboJadwal($tgl,$id_jurusan);
		
	  $opt = "";
		
	  if ($result){
			$opt = "<option>(none)</option>" . $opt;  
			while ($row = $db->sql_fetchrow($result)){
				$opt .= "<option value='$row[0]'>$row[0]-$row[1]</option>";
			}    
	  } 
		else{
			$opt .="<option selected=selected>Error</option>";
	  }
		
		/*if ($result){
			while ($row = $db->sql_fetchrow($result)){
				$opt .= "<a href='#' id='$row[0]' onClick=\"p_jadwal.value='$row[0]';changeRuteJam();pilihJadwal(this);return false;\">$row[1] ($row[0])</a><hr>";
			}    
			
			$opt	= substr($opt,0,-4);
	  } 
		else{
			$opt .="Gagal mengambil data";
	  }*/
  }  
  
  return $opt;
}

function setListDiscount($kode_jadwal,$tgl_berangkat,$kode_discount=NULL){
		
		global $db;
		
		$sql =
			"SELECT FlagLuarKota,f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','$tgl_berangkat') AS HargaTiket,
				HargaTiket AS HargaTiketNormal
			FROM   tbl_md_jurusan
			WHERE IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal')";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota	= $row['FlagLuarKota'];
		$harga_tiket		= $row['HargaTiket'];
		$harga_tiket_normal	= $row['HargaTiketNormal'];
		
    $sql = "SELECT IdDiscount,NamaDiscount,JumlahDiscount,KodeDiscount,IsHargaTetap
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota'  AND KodeDiscount!='V'
            ORDER BY NamaDiscount ASC";
	  
		$opt = "<option value=''>UMUM Rp. ".number_format($harga_tiket,0,",",".")."</option>";
		
		$keterangan_harga_discount = "";
		
		$temp_harga_tiket	= $harga_tiket;
		
	  if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				
				if($row['IsHargaTetap']==1){
					$temp_harga_tiket		= $row['JumlahDiscount'];
					$besar_discount			= 0;
				}
				else if($row['JumlahDiscount']>$harga_tiket){
					$temp_harga_tiket		= $harga_tiket;
					$besar_discount	= $harga_tiket;
					$keterangan_harga_discount	= "Promo";
				}
				else if($row['JumlahDiscount']>0 && $row['JumlahDiscount']<=1){
					$temp_harga_tiket	= $harga_tiket;
					$besar_discount	= $harga_tiket*$row['JumlahDiscount'];
					$keterangan_harga_discount	= "";
				}
				else{
					$temp_harga_tiket	= $harga_tiket;
					$besar_discount	= $row['JumlahDiscount'];
					$keterangan_harga_discount	= "";
				}
				
				switch($row['KodeDiscount']){
					case "M":
						$keterangan	= "MAHASISWA $keterangan_harga_discount Rp. ".number_format($temp_harga_tiket-$besar_discount,0,",",".");
						break;
					
					case "KK":
						$keterangan	= "SPECIAL TICKET $keterangan_harga_discount Rp. ".number_format($temp_harga_tiket-$besar_discount,0,",",".");
						break;
					
					case "G":
						$keterangan	= "GRATIS $keterangan_harga_discount Rp. ".number_format($temp_harga_tiket-$besar_discount,0,",",".");
						break;
					
					case "R":
						$keterangan	= "RETURN Rp. ".number_format($temp_harga_tiket+$harga_tiket_normal-$besar_discount,0,",",".");
						break;
				}
				
				$selected	= ($kode_discount!=$row['KodeDiscount'])?"":"selected";
				
	      $opt .= "<option value=$row[IdDiscount] $selected>$keterangan</option>";
	    } 
	  } 
		else{ 
			return "Error";
		}
		
		return $opt;
}

$mode = $mode!=""?$mode:"load";

//PEMILIHAN PROSES
switch($mode){

  case "load": /*LOAD HALAMAN AWAL**************************************************************************************/

    if($user_level!=$LEVEL_SCHEDULER){
      //jika  bukan member, dapat memilih tipe (penumpang/paket)

      $tombol_cetak_spj=
        "<a href='#' onclick='setDialogSPJ();'><img src='templates/images/icon_cetak_spj.gif'></a>
         </br>
         <a href='#' onclick='setDialogSPJ();'><span class='genmed'>Cetak Manifest</span></a>";
    }
    else{
      //jika   member, tidak dapat memilih tipe (penumpang/paket)
      $select_tipe	=	"<input name='ftipe' id='ftipe' type='hidden' value='penumpang'>Penumpang";
      $kolom_discount="<td colspan='2'></td>";
      $tombol_cetak_spj="";
    }

    include($adp_root_path .'/ClassPengaturanUmum.php');

    $PengaturanUmum	= new PengaturanUmum();

    $page_title	= "Rsv-Reguler";

    $template->assign_vars (
      array(
        'BCRUMP'    					=> '<a href="'.append_sid('main.'.$phpEx) .'">Home',
        'TOMBOL_CETAK_SPJ'		=> $tombol_cetak_spj,
        'TGL_SEKARANG'				=> dateY_M_D(),
        'U_LAPORAN_UANG'			=> "window.open('".append_sid('laporan.rekap.setoran.'.$phpEx.'')."','_blank');",
        'U_LAPORAN_PENJUALAN'	=> "window.open('".append_sid('laporan_penjualan_user.'.$phpEx.'')."','_blank');",
        'U_BA_SOPIR'					=> "window.open('".append_sid('reservasi.releaseinsentifsopir.'.$phpEx.'')."','_blank');",
        'U_BA_BOP'						=> "window.open('".append_sid('reservasi.releasebabop.'.$phpEx.'')."','_blank');",
        'U_UBAH_PASSWORD1'		=> "window.open('".append_sid('ubah_password.'.$phpEx.'')."','_blank');"
      )
    );

    $template->set_filenames(array('body' => 'reservasi/reservasi.nonreguler.tpl'));

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');

  exit; /*=============================================================================================================*/

  case "hargatiket": /*ACTION AMBIL HARGA TIKET*************************************************************************/
    $rute = $HTTP_GET_VARS['rute'];
    $harga_tiket = getHargaTiket($rute);

    echo $harga_tiket;
  exit; /*=============================================================================================================*/

  // PERIKSA NO TELP ===========================================================================================================================================================================================
  case "periksa_no_telp":
    $no_telp 			= $HTTP_GET_VARS['no_telp'];
    $flag_paket 	= $HTTP_GET_VARS['paket'];
    $flag_pengirim= $HTTP_GET_VARS['flag'];

    include($adp_root_path . 'ClassPelanggan.php');

    $Pelanggan = new Pelanggan();

    $data_pelanggan=$Pelanggan->ambilDataDetail($no_telp);

    if(count($data_pelanggan)>1){
      if($flag_paket!=1){
        echo("
          document.getElementById('hdn_no_telp_old').value='".trim($data_pelanggan['NoTelp'])."';
          document.getElementById('fnama').value='".trim($data_pelanggan['Nama'])."';
          document.getElementById('ftelp').value='".trim($data_pelanggan['NoTelp'])."';
        ");
      }
      else{
        if($flag_pengirim==1){
          echo("
            document.getElementById('dlg_paket_nama_pengirim').value='".trim($data_pelanggan['Nama'])."';
            document.getElementById('dlg_paket_alamat_pengirim').value='".trim($data_pelanggan['Alamat'])."';
            document.getElementById('dlg_paket_telp_pengirim').value='".trim($data_pelanggan['NoTelp'])."';
          ");
        }
        else{
          echo("
            document.getElementById('dlg_paket_nama_penerima').value='".trim($data_pelanggan['Nama'])."';
            document.getElementById('dlg_paket_alamat_penerima').value='".trim($data_pelanggan['Alamat'])."';
            document.getElementById('dlg_paket_telp_penerima').value='".trim($data_pelanggan['NoTelp'])."';
          ");
        }
      }
    }
    else{
      echo(0);
    }

  exit;

   // PERIKSA ID Member ===========================================================================================================================================================================================
  case "periksa_id_member":
    $id_member = $HTTP_GET_VARS['id_member'];

    $Member = new Member();

    $data_member=$Member->ambilData($id_member);

    if(count($data_member)>1){

      echo("
        if('".substr($data_member['TglLahir'],-5)."'=='".substr(dateNow(),-5)."'){
          alert('Ucapkan SELAMAT ULANG TAHUN pada member ini, karena member ini sedang berulang tahun hari ini!');
        }

        if($data_member[MasaBerlaku]>$THRESHOLD_MEMBER_EXPIRED){
          if(!document.getElementById('hdn_ubah_id_member')){
            document.getElementById('hdn_id_member').value='".trim($data_member['IdMember'])."';
            document.getElementById('fnama').value='".trim($data_member['Nama'])."';
            document.getElementById('ftelp').value='".trim($data_member['Handphone'])."';
          }
          else{
            document.getElementById('hdn_ubah_id_member').value='".trim($data_member['IdMember'])."';
            document.getElementById('ubah_nama_penumpang').value='".trim($data_member['Nama'])."';
            document.getElementById('ubah_telp_penumpang').value='".trim($data_member['Handphone'])."';
          }
        }
        else if($data_member[MasaBerlaku]>0 && $data_member[MasaBerlaku]<=$THRESHOLD_MEMBER_EXPIRED){
          if(!document.getElementById('hdn_ubah_id_member')){
            document.getElementById('hdn_id_member').value='".trim($data_member['IdMember'])."';
            document.getElementById('fnama').value='".trim($data_member['Nama'])."';
            document.getElementById('ftelp').value='".trim($data_member['Handphone'])."';
          }
          else{
            document.getElementById('hdn_ubah_id_member').value='".trim($data_member['IdMember'])."';
            document.getElementById('ubah_nama_penumpang').value='".trim($data_member['Nama'])."';
            document.getElementById('ubah_telp_penumpang').value='".trim($data_member['Handphone'])."';
          }

          alert('Masa berlaku keanggotan sebentar lagi akan berakhir, mohon informasikan kepada pelanggan');
        }
        else{

          if(!document.getElementById('hdn_ubah_id_member')){
            document.getElementById('hdn_id_member').value='';
          }
          else{
            document.getElementById('hdn_ubah_id_member').value='';
          }

          alert('Maaf, masa keanggotaan member yang bersangkutan sudah berakhir');
        }

      ");
    }
    else{
      echo(0);
    }

  exit;

  // PERIKSA MEMBER ===========================================================================================================================================================================================
  case "periksa_member_by_kartu":
    $no_seri_kartu = $HTTP_GET_VARS['no_kartu'];

    $Member = new Member();

    $data_member=$Member->ambilDataByNoSeriKartu($no_seri_kartu);

    if(count($data_member)>1){
      echo("
        document.getElementById('hdn_id_member').value='".trim($data_member['id_member'])."';
        document.getElementById('fnama').value='".trim($data_member['nama'])."';
        document.getElementById('fadd').value='".trim($data_member['alamat'])." ".trim($data_member['kota'])."';
        document.getElementById('ftelp').value='".trim($data_member['telp_rumah'])."';
      ");
    }
    else{
      echo("false");
    }

  exit;

  //Asal  ===========================================================================================================================================================================================
  case "asal":
      // membuat nilai awal..
    $kota	 = $HTTP_GET_VARS['kota_asal'];

    $oasal = OptionAsal($kota);

    echo("
      <table width='100%'>
        <tr><td>POINT KEBERANGKATAN<br></td></tr>
        <tr>
          <td><select onchange='getUpdateTujuan(this.value);' id='rute_asal' name='rute_asal'>$oasal</select><span id='progress_asal' style='display:none;'><img src='./templates/images/loading.gif' /></span>
          </td>
        </tr>
      </table>");

    exit;

  //TGL ===========================================================================================================================================================================================
  case "tujuan":
      // membuat nilai awal..
    $asal=$HTTP_GET_VARS['asal'];

    $ojur = OptionJurusan($asal);

    echo("
        <table>
          <tr><td>POINT TUJUAN<br></td></tr>
          <tr>
            <td><select onchange='getUpdateJadwal(this.value);' id='tujuan' name='tujuan'>$ojur</select><span id='progress_tujuan' style='display:none;'><img src='./templates/images/loading.gif' /></span>
            </td>
          </tr>
        </table>");
  exit;

  // JAM ===========================================================================================================================================================================================
  case "jam":
    // memilih jam yang tersedia sesuai dengan rute yang dimasukan
    $tgl 				= $HTTP_GET_VARS['tgl'];
    $id_jurusan = $HTTP_GET_VARS['id_jurusan'];
    $ojam 			= OptionJadwal($tgl,$id_jurusan);
    echo("
    <table width='100%'>
      <tr><td>JAM KEBERANGKATAN<br></td></tr>
      <tr><td><select onchange='changeRuteJam();' name='p_jadwal' id='p_jadwal'>$ojam</select><span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span></td></tr>
    </table>");

    /*echo("
    <table bgcolor='white' width='100%'>
      <tr><td>JAM KEBERANGKATAN<br></td></tr>
      <tr><td>$ojam</td></tr>
    </table>");*/

   exit;

  // PENGUMUMAN LAYOUT===========================================================================================================================================================================================
  case "pengumuman":
    // menampilkan pengumuman
    include_once($adp_root_path . 'ClassPengumuman.php');

    $Pengumuman	= new Pengumuman();

    if ($userdata['user_level'] != $LEVEL_SCHEDULER){
      //HANYA BISA DIAKSES OLEH SELAIN AGEN

      $jumlah_pengumuman	= $Pengumuman->ambilJumlahPengumumanBaru($userdata['user_id']);

      if($jumlah_pengumuman>0){
        $jumlah_pengumuman	= "<blink>".$jumlah_pengumuman."</blink>";
        $blink_Pengumuman		= "<blink>Pesan</blink>";
      }
      else{
        $jumlah_pengumuman	= "";
        $blink_Pengumuman		= "Pesan";
      }

      //$isi_pengumuman

      $baca_pengumuman	= "Start('".append_sid('pengumuman_inbox.'.$phpEx)."');getPengumuman();return false;";

      echo("
        <a href='#' onClick=$baca_pengumuman>&nbsp;&nbsp;&nbsp;&nbsp;<font color='red' size=5><b>$jumlah_pengumuman</b></font>&nbsp;&nbsp;&nbsp;&nbsp;
        <br><br><br>
        $blink_Pengumuman</a>
      ");

    }

  exit;

  // SHOW CHAIR ===========================================================================================================================================================================================
  case "showchair":
    // mendapatkan nilai kursi yang tersedia dan value nilai lain sesuai dengan tanggal, rute, jam dan kode

    $kursi  		= trim($HTTP_GET_VARS['kursi']);
    $no_tikets  = "'".str_replace(",","','",trim($HTTP_GET_VARS['no_tiket']))."'";
    $no_spj			= trim($HTTP_GET_VARS['no_spj']);

    $Reservasi		= new ReservasiNonReguler();
    $User					= new User();

    $res_kursi    = $Reservasi->ambilDataKursiMulti($no_tikets);

    $idx  = 0;
    $jumlah_data  = $db->sql_numrows($res_kursi);

    while($data_kursi=$db->sql_fetchrow($res_kursi)){
      $idx++;
      $template->destroy();

      $no_tiket     = $data_kursi["NoTiket"];
      $kode_jadwal	= $data_kursi['KodeJadwal'];
      $tgl_berangkat= $data_kursi['TglBerangkat'];
      $harga_tiket	= number_format($data_kursi['HargaTiket'],0,",",".");
      $discount			= number_format($data_kursi['Discount'],0,",",".");
      $sub_total_pertiket= number_format($data_kursi['HargaTiket'],0,",",".");
      $total				= number_format($data_kursi['Total'],0,",",".");
      $nama_cso			= $User->ambilNamaById($data_kursi['PetugasPenjual']);
      $cetak_tiket	= $data_kursi['CetakTiket'];
      $id_member		= trim($data_kursi['IdMember']);
      $kode_booking	= $data_kursi['KodeBooking'];
      $jenis_penumpang	= $data_kursi['JenisPenumpang'];
      $jenis_pembayaran	= $data_kursi['JenisPembayaran'];
      $kode_voucher	= $data_kursi['KodeVoucher'];

      //Mengambil data total bayar
      $data_total = $Reservasi->ambilTotalPesananByKodeBooking($kode_booking);

      $sub_total			=($data_total['HargaTiket']!='')?$data_total['HargaTiket']:0;
      $jumlah_kursi		=($data_total['JumlahKursi']!='')?$data_total['JumlahKursi']:0;
      $total_discount	=($data_total['TotalDiscount']!='')?$data_total['TotalDiscount']:0;
      $total_bayar		=($data_total['TotalBayar']!='')?$data_total['TotalBayar']:0;

      //Mengambil data total yang sudah bayar
      $data_total_dibayar = $Reservasi->ambilTotalPesananDibayarByKodeBooking($kode_booking);
      $sub_total_dibayar	=($data_total_dibayar['HargaTiket']!='')?$data_total_dibayar['HargaTiket']:0;

      if($total_bayar-$sub_total_dibayar==0 || $sub_total_dibayar==0){
        //nothing
      }
      else{
        $jumlah_kursi_dibayar		=($data_total_dibayar['JumlahKursi']!='')?$data_total_dibayar['JumlahKursi']:0;
        $total_discount_dibayar	=($data_total_dibayar['TotalDiscount']!='')?$data_total_dibayar['TotalDiscount']:0;
        $total_bayar_dibayar		=($data_total_dibayar['TotalBayar']!='')?$data_total_dibayar['TotalBayar']:0;

        $template->assign_block_vars('showdatapembayaran',
          array(
            'TOTAL_TIKET_DIBAYAR'		=> number_format($jumlah_kursi_dibayar,0,",","."),
            'SUB_TOTAL_DIBAYAR'			=> number_format($sub_total_dibayar,0,",","."),
            'TOTAL_DISCOUNT_DIBAYAR'=> number_format($total_discount_dibayar,0,",","."),
            'TOTAL_DIBAYAR'					=> number_format($total_bayar_dibayar,0,",","."),
            'TOTAL_TIKET_UTANG'			=> number_format($jumlah_kursi-$jumlah_kursi_dibayar,0,",","."),
            'SUB_TOTAL_UTANG'				=> number_format($sub_total-$sub_total_dibayar,0,",","."),
            'TOTAL_DISCOUNT_UTANG'	=> number_format($total_discount-$total_discount_dibayar,0,",","."),
            'TOTAL_UTANG'						=> number_format($total_bayar-$total_bayar_dibayar,0,",","."))
        );

      }

      $sub_total			= number_format($sub_total,0,",",".");
      $jumlah_kursi		= number_format($jumlah_kursi,0,",",".");
      $total_discount	= number_format($total_discount,0,",",".");
      $total_bayar		= number_format($total_bayar,0,",",".");

      //memeriksa ada tidaknya payment code
      $payment_code	= $data_kursi['PaymentCode']==""?"":$data_kursi['PaymentCode'];
      if($payment_code!="")$template->assign_block_vars('showpaymentcode',array());

      //Memeriksa informasi mutasi
      if($data_kursi['Pemutasi']!='')$template->assign_block_vars('infomutasi',array());

      if($no_spj=="" || $userdata['user_level']<=$USER_LEVEL_INDEX['SPV_OPERASIONAL']){
        $mutasi_action	= "setFlagMutasi();";
      }
      else{
        $mutasi_action	= "showDialogMutasi();";
      }

      if(!$cetak_tiket){
        //belum cetak tiket (masih booking)

        if($data_kursi['PetugasPenjual']!=0 || $userdata['user_level'] == $USER_LEVEL_INDEX["ADMIN"]){
          //jika cso bukan TIKETUX

          //---MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU JIKA DITEMUKAN ADA PROMO, MAKA DISCOUNT YANG LAIN TIDAK BERLAKU
          $Promo	= new Promo();

          $data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl_berangkat);

          $target_promo	= $data_discount_point['FlagTargetPromo'];

          if($target_promo=='' || $target_promo==1){
            $list_discount	= setListDiscount($kode_jadwal,$tgl_berangkat,$jenis_penumpang);

            $template->assign_block_vars('showlistdiscount',array());
          }
          else{
            $besar_discount		= ($data_discount_point['JumlahDiscount']>1)?"Rp.".number_format($data_discount_point['JumlahDiscount'],0,",","."):($data_discount_point['JumlahDiscount']*100)."%";
            $list_discount		= "<b>Promo Discount $besar_discount</b>";

            $template->assign_block_vars('showpromoberlaku',array());
          }

          //--END MEMERIKSA DISCOUNT

          $batal_action = in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["CSO"]))? "batal('$no_tiket','$kursi')":"tampilkanDialogPembatalan('$no_tiket','$kursi')";

          $template->assign_block_vars('tombolsimpan',array());
          $template->assign_block_vars('showtombol',array());
        }
        else{
          $list_discount	= "<b>ONLINE</b>";
          $template->assign_block_vars('showpromoberlaku',array());
        }

        //memeriksa pembelian yang lebih dari satu
        if($jumlah_kursi>1)$template->assign_block_vars('showalltarif',array());

        $template->assign_vars(array(
            'PENUMPANG_KE'      => ($jumlah_data<=1?"":$idx." DARI ".$jumlah_data),
            'NO_TIKET'					=> $no_tiket,
            'KODE_BOOKING'			=> $kode_booking,
            'PAYMENT_CODE'			=> $payment_code,
            'CETAK_TIKET'				=> $cetak_tiket,
            'KURSI'							=> $kursi,
            'KODE_JADWAL'				=> $kode_jadwal,
            'TELP'							=> $data_kursi['Telp'],
            'NAMA'							=> $data_kursi['Nama'],
            'KETERANGAN'				=> $data_kursi['Alamat'],
            'HARGA_TIKET'				=> $sub_total_pertiket,
            'DISCOUNT'					=> $discount,
            'TOTAL'							=> $total,
            'TOTAL_TIKET'				=> $jumlah_kursi,
            'SUB_TOTAL'					=> $sub_total,
            'TOTAL_DISCOUNT'		=> $total_discount,
            'TOTAL_BAYAR'				=> $total_bayar,
            'CSO_PEMESAN'				=> $nama_cso,
            'WAKTU_PESAN'				=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuPesan'])),
            'BATAL_ACTION'			=> $batal_action,
            'MUTASI_ACTION'			=> $mutasi_action,
            'JENIS_PENUMPANG'		=> $jenis_penumpang,
            'LIST_DISCOUNT'			=> $list_discount,
            'WAKTU_MUTASI'			=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuMutasi'])),
            'DIMUTASI_DARI'			=> $data_kursi['MutasiDari'],
            'DIMUTASI_OLEH'			=> $User->ambilNamaById($data_kursi['Pemutasi']),
          )
        );

        $template->set_filenames(array('body'.$idx => 'reservasi/reservasi.detailkursi.book.tpl'));
      }
      else{
        //Sudah Cetak Tiket
        $Member	= new Member();

        if($id_member==""){
          //jika bukan member

          //menampilkan tarif
          $template->assign_block_vars('showtarif',array());

          //memeriksa pembelian yang lebih dari satu
          if($jumlah_kursi>1)$template->assign_block_vars('showalltarif',array());

        }
        else{
          $data_member	= $Member->ambilData($id_member);
          $template->assign_block_vars('showmember',array());
        }

        $show_penumpang	= $jenis_pembayaran!=3?$config['jenis_penumpang'][$jenis_penumpang]:"VOUCHER";

        if($data_kursi['PetugasPenjual']!=0){
          //jika cso bukan TIKETUX

          if($userdata['user_level'] > $USER_LEVEL_INDEX["SPV_RESERVASI"]){
            $cetak_tiket_action	= "notiketdicetak.value='$no_tiket';tampilkanDialogCetakUlangTiket();";
          }
          else{
            if($jenis_penumpang!="MBR"){
              $cetak_tiket_action	= "notiketdicetak.value='$no_tiket';CetakTiket(0);";
            }
            else{
              $cetak_tiket_action	= "notiketdicetak.value='$no_tiket';CetakTiketByVoucherMember('$kode_voucher')";
            }
          }

          $batal_action = in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["SPV_RESERVASI"]))? "batal('$no_tiket','$kursi')":"tampilkanDialogPembatalan('$no_tiket','$kursi')";

          $template->assign_block_vars('showtombolbatal',array("width"=>"70"));
          $template->assign_block_vars('showtombolkoreksidiskon',array());
          $template->assign_block_vars('showtombolmutasi',array());
        }
        else{
          //JIKA CSO ADALAH TIKETUX
          $show_penumpang	= "<b>ONLINE</b>";

          $sub_total_pertiket	= number_format($data_kursi['HargaTiketux'],0,",",".");
          $discount						= 0;
          $total							= $sub_total_pertiket;

          $sub_total					= number_format($data_total['HargaTiketux'],0,",",".");
          $total_discount			= 0;
          $total_bayar				= $sub_total;

          //periska penggunaa kode OTP
          if($data_kursi['OTPUsed']!=1 && $userdata['user_level']!=$USER_LEVEL_INDEX["ADMIN"]){
            $cetak_tiket_action	= "notiketdicetak.value='$no_tiket';tampilkanDialogCetakUlangTiketOTP('$no_tiket');";
          }
          else{
            $cetak_tiket_action	= "notiketdicetak.value='$no_tiket';tampilkanDialogCetakUlangTiket();";
          }

          if($userdata['user_level']==$USER_LEVEL_INDEX["ADMIN"]){
            $template->assign_block_vars('showtombolbatal',array("width"=>"230","br"=>"<br><br>"));
            $batal_action	= "batal('$no_tiket','$kursi')";
          }

          if($data_kursi['Pemutasi']=="" || $userdata['user_level']<=$USER_LEVEL_INDEX["SPV_RESERVASI"]){
            $template->assign_block_vars('showtombolmutasi',array());
          }
        }

        $template->assign_vars(array(
            'PENUMPANG_KE'      => ($jumlah_data<=1?"":$idx." DARI ".$jumlah_data),
            'NO_TIKET'					=> $no_tiket,
            'KODE_BOOKING'			=> $kode_booking,
            'PAYMENT_CODE'			=> $payment_code,
            'CETAK_TIKET'				=> $cetak_tiket,
            'KURSI'							=> $kursi,
            'KODE_JADWAL'				=> $kode_jadwal,
            'TELP'							=> $data_kursi['Telp'],
            'NAMA'							=> $data_kursi['Nama'],
            'KETERANGAN'				=> $data_kursi['Alamat'],
            'PENUMPANG'					=> $show_penumpang,
            'JENIS_PENUMPANG'		=> $jenis_penumpang,
            'KODE_MEMBER'				=> $data_member['IdMember'],
            'NAMA_MEMBER'				=> $data_member['Nama'],
            'TELP_MEMBER'				=> $data_member['Handphone'],
            'KODE_VOUCHER'			=> $kode_voucher,
            'HARGA_TIKET'				=> $sub_total_pertiket,
            'DISCOUNT'					=> $discount,
            'TOTAL'							=> $total,
            'TOTAL_TIKET'				=> $jumlah_kursi,
            'SUB_TOTAL'					=> $sub_total,
            'TOTAL_DISCOUNT'		=> $total_discount,
            'TOTAL_BAYAR'				=> $total_bayar,
            'CSO_PEMESAN'				=> $nama_cso,
            'WAKTU_PESAN'				=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuPesan'])),
            'WAKTU_BAYAR'				=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuCetakTiket'])),
            'CSO_CETAK_TIKET'		=> $User->ambilNamaById($data_kursi['PetugasCetakTiket']),
            'CETAK_TIKET_ACTION'=> $cetak_tiket_action,
            'BATAL_ACTION'			=> $batal_action,
            'MUTASI_ACTION'			=> $mutasi_action,
            'WAKTU_MUTASI'			=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_kursi['WaktuMutasi'])),
            'DIMUTASI_DARI'			=> $data_kursi['MutasiDari'],
            'DIMUTASI_OLEH'			=> $User->ambilNamaById($data_kursi['Pemutasi']),
          )
        );

        $template->set_filenames(array('body'.$idx  => 'reservasi/reservasi.detailkursi.issued.tpl'));
      }
      $template->pparse('body'.$idx );
    }


  exit;

  // AMBIL STATUS KURSI SATUAN ==========================================================================================================================================================================================
  case "ambilstatuskursi":
    include($adp_root_path . 'ClassLayoutKendaraan.php');

    $tgl 					= $HTTP_GET_VARS['tanggal'];
    $kode_jadwal 	= $HTTP_GET_VARS['jadwal'];
    $no_kursi 				= $HTTP_GET_VARS['no_kursi'];

    $LayoutKendaraan	= new LayoutKendaraan();
    $Jadwal						= new Jadwal();

    echo $LayoutKendaraan->getStatusKursi($tgl,$kode_jadwal,$no_kursi);

  exit;

  // UBAH DATA PENUMPANG ===========================================================================================================================================================================================
  case "ubahdatapenumpang":
    // mendapatkan nilai kursi yang tersedia dan value nilai lain sesuai dengan tanggal, rute, jam dan kode

      $no_tiket			= trim($HTTP_GET_VARS['no_tiket']);
      $kursi 				= trim($HTTP_GET_VARS['kursi']);
      $nama  				= trim($HTTP_GET_VARS['nama']);
      $telp  				= trim($HTTP_GET_VARS['telepon']);
      $alamat				= trim($HTTP_GET_VARS['alamat']);
      $id_discount	= trim($HTTP_GET_VARS['id_discount']);
      $id_member		= trim($HTTP_GET_VARS['id_member']);
      $tgl_lahir		= $HTTP_GET_VARS['tgl_lahir'];

      $Reservasi		= new ReservasiNonReguler();

      $Reservasi->ubahDataPenumpang($no_tiket, $nama , $alamat, $telp, $kursi, $id_discount, $id_member);

      echo(1);

  exit;


  // TEMP ==========================================================================================================================================================================================
  case "temp":
    include($adp_root_path . 'ClassLayoutKendaraan.php');
    include($adp_root_path . 'ClassPenjadwalanKendaraan.php');

    $tgl 					= $HTTP_GET_VARS['tanggal'];
    $kode_jadwal 	= $HTTP_GET_VARS['jam'];
    $kursi 				= $HTTP_GET_VARS['kursi'];
    $no_spj 			= $HTTP_GET_VARS['no_spj'];
    $layout_kursi = $HTTP_GET_VARS['layout_kursi'];

    //UPDATE BY: BARTON
    //TGL UPDATE: 19 NOVEMBER 2013
    //JIKA LEVEL CSO, AKAN DICEK TANGGAL BERANGKAT, KARENA TIDAK BOLEH MEMBOOKING LEWAT 8 HARI DARI SEKARANG
    $temp_tgl 	= str_replace("-", "/", $tgl);
    $start_time_stamp = strtotime(date("Y/m/d"));
    $end_time_stamp = strtotime($temp_tgl);

    $time_diff = abs($end_time_stamp - $start_time_stamp);

    $number_days = $time_diff/86400;  // 86400 seconds in one day

    // and you might want to convert to integer
    $number_days = intval($number_days);

    $Reservasi	    = new ReservasiNonReguler();
    $Penjadwalan	  = new PenjadwalanKendaraan();
    $LayoutKendaraan= new LayoutKendaraan();

    $data_jadwal = $Penjadwalan->ambilDataDetail($tgl,$kode_jadwal);

    $session_id = $userdata['user_id'];

    //if($number_days<=8 || $userdata["user_level"]<=$USER_LEVEL_INDEX["SPV_OPERASIONAL"]){
      //JIKA TANGGAL BOOKING KURANG DARI SAMA DENGAN 8 HARI, AKSES DIBERIKAN
      $Reservasi->updateStatusKursi($kursi,$session_id,$kode_jadwal,$data_jadwal["JamBerangkat"],$data_jadwal["KodeCabangAsal"],$data_jadwal["KodeCabangTujuan"],$kode_jadwal,$tgl,$userdata['user_level']);
    //}
    //END==TGL UPDATE: 19 NOVEMBER 2013
    echo $LayoutKendaraan->getStatusKursi($tgl,$kode_jadwal,$kursi);

  exit;

  //BOOK ===========================================================================================================================================================================================
  case "book":
    include($adp_root_path . 'ClassLayoutKendaraan.php');
    include($adp_root_path . 'ClassPenjadwalanKendaraan.php');

    // UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
    $tgl     		= $HTTP_GET_VARS['tanggal'];   // tanggal
    $kode_jadwal= $HTTP_GET_VARS['kode_jadwal'];   // jam
    $id_member	= $HTTP_GET_VARS['id_member'];  // id member
    $nama    		= $HTTP_GET_VARS['nama'];  // nama
    $alamat 		= $HTTP_GET_VARS['alamat']; //alamat
    $no_telp_old= $HTTP_GET_VARS['no_telp_old']; // telepon
    $telepon 		= $HTTP_GET_VARS['telepon']; // telepon
    $no_spj			= $HTTP_GET_VARS['no_spj'];
    $layout_kursi= $HTTP_GET_VARS['layout_kursi'];
    $id_discount= $HTTP_GET_VARS['jenis_discount'];

    $LayoutKendaraan  = new LayoutKendaraan();
    $Reservasi	      = new ReservasiNonReguler();
    $Penjadwalan      = new PenjadwalanKendaraan();

    //memgambil data rute dan jadwal
    $row 					= $Penjadwalan->ambilDataDetail($tgl,$kode_jadwal);
    $jam_berangkat= $row['JamBerangkat'];
    $id_jurusan 	= $row['IdJurusan'];

    //MEMERIKSA APAKAH PENUMPANG  SUDAH PERNAH BOOKING ATAU BELUM

    //memeriksa apakah sudah ada bookingan sebelumnya
    /*if($Reservasi->adaPesananBelumDibayarSebelumnya($telepon,$tgl,$jam_berangkat)){
      echo ("2||");
      exit;
    }*/

    $Promo			= new Promo();
    $Jurusan		= new Jurusan();

    //UPDATE BY: BARTON
    //TGL UPDATE: 19 NOVEMBER 2013
    //JIKA LEVEL CSO, AKAN DICEK TANGGAL BERANGKAT, KARENA TIDAK BOLEH MEMBOOKING LEWAT 8 HARI DARI SEKARANG
    $temp_tgl 	= str_replace("-", "/", $tgl);
    $start_time_stamp = strtotime(date("Y/m/d"));
    $end_time_stamp = strtotime($temp_tgl);

    $time_diff = abs($end_time_stamp - $start_time_stamp);

    $number_days = $time_diff/86400;  // 86400 seconds in one day

    // and you might want to convert to integer
    $number_days = intval($number_days);

    /*if($number_days>8 && $userdata["user_level"]>$USER_LEVEL_INDEX["SPV_OPERASIONAL"]){
      //JIKA TANGGAL BOOKING LEBIH DARI 8 HARI, AKSES DITOLAK
      echo("3/||");
      exit;
    }*/
    //END==TGL UPDATE: 19 NOVEMBER 2013

    $kode_booking	= generateKodeBookingPenumpang();
    $payment_code	= "";

    $session_id	= $userdata['session_id'];
    $useraktif	= $userdata['user_id'];


    //mengambil 	data header tb posisi
    $row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal);
    $kode_kendaraan	= $row['KodeKendaraan'];
    $kode_sopir			= $row['KodeSopir'];
    $tgl_cetak_SPJ	= $row['TglCetakSPJ'];
    $petugas_cetak_spj= $row['PetugasCetakSPJ'];
    $cetak_spj			= ($no_spj=="")?0:1;

    //mengambil data-data kode account dan komisi
    $row														= $Jurusan->ambilDataDetail($id_jurusan);
    $komisi_penumpang_CSO						= $row['KomisiPenumpangCSO'];
    $kode_akun_pendapatan						= $row['KodeAkunPendapatanPenumpang'];
    $kode_akun_komisi_penumpang_CSO	= $row['KodeAkunKomisiPenumpangCSO'];

    $harga_tiket=$Reservasi->getHargaTiket($kode_jadwal,$tgl,$id_jurusan,$userdata['user_id']);
    $charge	=0;
    $PPN=0;
    $jenis_muatan=0;
    $flag_pesanan=0;

    //MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU
    $data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl);

    $discount		= ($data_discount_point['FlagDiscount']!=1)?$data_discount_point['JumlahDiscount']:($data_discount_point['JumlahDiscount']/100)*$harga_tiket;
    $point			= $data_discount_point['JumlahPoint'];
    $target_promo	= $data_discount_point['FlagTargetPromo'];

    $jenis_penumpang="U";

    if(trim($id_member)!=''){
      $Member = new Member();

      $data_member	= $Member->ambilData($id_member);
      $nama    			= $data_member['Nama'];
      //$alamat 			= $data_member['Alamat'];
      //$telepon 			= $data_member['Handphone'];

      //memeriksa discount
      if($nama!="" && $data_member['MasaBerlaku']>0){
        $discount				= ($target_promo <=1 )?$discount:0;
        $point_member		= ($target_promo <=1 )?$point:0;
        $jenis_penumpang="K";
      }
    }
    else{
      //memeriksa discount
      $discount	= ($target_promo ==2 || $target_promo ==0 )?$discount:0;
    }

    $jenis_discount	= ($discount==0)?"":$data_discount_point['NamaPromo'];

    //MEMERIKSA APAKAH ADA PEMBERIAN DISCOUNT
    if($id_discount!='' && ($discount==0 || $discount=='')){
      //jika memang discount diberikan, maka akan diupdate pada database
      $data_discount	= $Reservasi->ambilDiscount($id_discount);
      $jenis_penumpang= $data_discount['KodeDiscount'];
      $jenis_discount	= $data_discount['NamaDiscount'];

      if($data_discount['IsHargaTetap']==1){
        $besar_discount	= $harga_tiket-($data_discount['JumlahDiscount']<=$harga_tiket?$data_discount['JumlahDiscount']:$harga_tiket);
      }
      else if($data_discount['JumlahDiscount']>$harga_tiket){
        $besar_discount	= $harga_tiket;
      }
      else if($data_discount['JumlahDiscount']>0 && $data_discount['JumlahDiscount']<=1){
        $besar_discount	= $harga_tiket*$data_discount['JumlahDiscount'];
      }
      else{
        $besar_discount	= $data_discount['JumlahDiscount'];
      }

      $discount	= ($besar_discount<=$harga_tiket)?$besar_discount:$harga_tiket;
    }

    //LAYOUT KURSI
    $result_layout_kursi = $Reservasi->ambilDataFlagLayout($tgl,$kode_jadwal,$userdata['user_id']);

    //mengisi  flag kursi dan session

    $jumlah_kursi_dipesan=0;

    $list_nomor_kursi="";

    $jumlah_bayar	= 0;

    while ($data_kursi = $db->sql_fetchrow($result_layout_kursi)){

      //Mengenerate no tiket
      $no_tiket = generateNoTiketPenumpang();

      $sub_total	= $harga_tiket;
      $total			= $sub_total+$charge+$PPN-$discount;

      $no_kursi	= $data_kursi['NomorKursi'];

      //MEMERIKSA APAKAH KURSI MASIH FREE (add 17 Oktober 2011)
      $sql =
        "SELECT IF(StatusKursi IS NULL,0,StatusKursi) AS SudahTerisi
          FROM tbl_posisi_detail
          WHERE KodeJadwal LIKE '$kode_jadwal' AND TglBerangkat LIKE '$tgl' AND NomorKursi='$no_kursi' AND Session!='$userdata[user_id]'";

      if (!$result = $db->sql_query($sql)){
        die_error("Err: $this->ID_FILE".__LINE__);
      }

      $row=$db->sql_fetchrow($result);
      $status_kursi	= $row[0];

      if($status_kursi==0 || $status_kursi==''){
        $Reservasi->booking(
          $no_tiket, $userdata['KodeCabang'], $kode_jadwal,
          $id_jurusan, $kode_kendaraan , $kode_sopir ,
          $tgl, $jam_berangkat , $kode_booking,
          $id_member, $point_member, $nama ,
          $alamat, $telepon, $hp,
          $no_kursi, $harga_tiket,
          $charge, $sub_total, $discount,
          $PPN, $total, $userdata['user_id'],
          $flag_pesanan, $no_spj, $tgl_cetak_SPJ,
          $cetak_spj, $komisi_penumpang_CSO,
          $petugas_cetak_spj, $keterangan, $jenis_discount,
          $kode_akun_pendapatan,$jenis_penumpang, $kode_akun_komisi_penumpang_CSO,
          $payment_code);

        $jumlah_bayar	+= $total;

        if(($id_discount=='' && $target_promo==1) || ($id_discount!='' && $data_discount['KodeDiscount']!="R")){
          //DIKARENAKAN DISCOUNT  HANYA DIBERIKAN KEPADA PELANGGAN PERTAMA,MAKA SETELAH DISCOUNT DIBERIKAN, BESAR DISCOUNT AKAN DI FREE KEMBALI
          //ket: $target_promo=1 adalah promo yang hanya untuk member saja

          $id_member			= '';
          $discount				= 0; //discount diset kembali menjadi 0
          $jenis_discount	= ''; //jenis discount direset
          $jenis_penumpang= 'U'; //penumpang direset menjadi penumpang umum
        }

        $list_nomor_kursi .= $no_kursi."/";

        $jumlah_kursi_dipesan++;
      }
    }

    //jika ada kursi yang dipesan
    if($jumlah_kursi_dipesan>0){
      //mengirim messange true ke body

      $Reservasi->ubahPosisi($kode_jadwal, $tgl, $jumlah_kursi_dipesan,$id_jurusan);

      //MENGIRIM KE TIKETUX
      /*include($adp_root_path . 'ClassTiketux.php');

      $Tiketux = new Tiketux();

      $parameter_kirim["kode_booking"]	= $kode_booking;
      $parameter_kirim["jumlah"]				= $jumlah_bayar;
      //$Tiketux->sendParameterRequest($parameter_kirim);
      */
      $list_nomor_kursi = substr($list_nomor_kursi,0,-1);
      echo("prosesok=true;status=0;kodebooking='$kode_booking';notiket='$no_tiket';listnokursi='$list_nomor_kursi';");
    }
    else{
      //belum memilih kursi
      echo("prosesok=true;status=1;");
    }


    echo($pesan);
  exit;

  //MEMBATALKAN PESANAN ====================================================================================================================================
  case 'pembatalan':
    include($adp_root_path . 'ClassAsuransi.php');

    $no_tiket	= $HTTP_POST_VARS['no_tiket'];// nourut
    $kursi		= $HTTP_POST_VARS['no_kursi']; // kursi
    $Reservasi	= new ReservasiNonReguler();

    $data_tiket	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);

    $valid=false;

    //NO UPDATE: UP1311081520================================================================
    //UPDATE PEMBATALAN SEMUA PEMESANAN BAIK BOOK MAUPUN SUDAH CETAK TIKET JIKA MEMBATALKAN HARUS OTORISASI SPV
    //REQUEST BY PAK CHAN tgl 8 NOVEMBER 2013 PUKUL 15:20
    //PIC: BARTON YAN FARI

    if($data_tiket['CetakTiket']!=1 || $userdata['user_level']<=$USER_LEVEL_INDEX["SPV_RESERVASI"]){
      $valid= true;
      $user_pembatal= $userdata['user_id'];
    }
    else{
      $username				= $HTTP_POST_VARS['username'];
      $password				= $HTTP_POST_VARS['password'];

      $User = new User();

      $data_user	= $User->ambilDataDetailByUsername($username);

      $valid= $data_user['user_password']==md5($password) && $data_user['user_level']<=$USER_LEVEL_INDEX["SPV_RESERVASI"]?true:false;
      $user_pembatal= $data_user['user_id'];
    }

    /*if($data_tiket['CetakTiket']==0){
      //BELUM CETAK TIKET
      $valid			 	= true;
      $user_pembatal= $userdata['user_id'];
    }
    else{
      //SUDAH CETAK TIKET
      if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["SPV_RESERVASI"]))){
        $valid= true;
        $user_pembatal= $userdata['user_id'];
      }
      else{
        $username				= $HTTP_POST_VARS['username'];
        $password				= $HTTP_POST_VARS['password'];

        $User = new User();

        $data_user	= $User->ambilDataDetailByUsername($username);

        $valid= $data_user['user_password']==md5($password) && $data_user['user_level']<=$USER_LEVEL_INDEX["SPV_RESERVASI"]?true:false;
        $user_pembatal= $data_user['user_id'];
      }

    }*/

    //==END NO UPDATE: UP1311081520================================================================

    if($valid){
      $Reservasi->pembatalan($no_tiket, $kursi, $user_pembatal);
      echo(1);
    }
    else{
      echo(0);
    }

  exit;


  //APPROVAL CETAK ULANG TIKET================================================================================================================================

  case "cetakulangtiket":

    $username				= $HTTP_POST_VARS['username'];
    $password				= $HTTP_POST_VARS['password'];

    $User = new User();

    $Reservasi= new ReservasiNonReguler();

    $no_tiket				= $HTTP_POST_VARS['no_tiket'];
    $data_penumpang	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);

    $data_user	    = $User->ambilDataDetailByUsername($username);


    if($data_user['user_password']==md5($password) && $data_user['user_level']<=$USER_LEVEL_INDEX['SPV_RESERVASI']){
      $data_kursi	= $Reservasi->ambilDataKursi($no_tiket);

      $Reservasi->tambahLogCetakTiket(
        $no_tiket,$data_kursi['TglBerangkat'],$data_kursi['JamBerangkat'],
        $data_kursi['KodeJadwal'],$data_kursi['IdJurusan'],$data_kursi['NomorKursi'],
        $userdata['user_id'],$userdata['nama'],$data_user['user_id'],
        $data_user['nama']);

      echo("document.getElementById('cetaksemuatiket').value=0;CetakTiket(0);");
    }
    else{
      //jika password tidak sesuai maka tidak diijinkan melakukna proses ini
      echo("alert('Anda tidak memiliki otoritas untuk melakukan aksi ini!');");
    }

  exit;


  //APPROVAL CETAK ULANG TIKET MENGGUNAKAN KODE OTP================================================================================================================================

  case 'cetakulangtiketotp':

    $password				= $HTTP_POST_VARS['password'];
    $no_tiket				= $HTTP_POST_VARS['no_tiket'];

    $User = new User();

    //TIKETUX
    $Reservasi= new ReservasiNonReguler();

    $data_penumpang	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);


    if($userdata['user_level']==$LEVEL_ADMIN || $data_penumpang['otp']==$password){
      echo("CetakTiket(0);");
    }
    else{
      echo("alert('Kode OTP yang anda masukkan tidak benar!');");
    }

  exit;

  //KOREKSI DISCOUNT================================================================================================================================

  case 'beridiscount':

    $no_tiket				= $HTTP_POST_VARS['no_tiket'];// nourut
    $jenis_discount	= $HTTP_POST_VARS['jenis_discount'];
    $username				= $HTTP_POST_VARS['username'];
    $password				= $HTTP_POST_VARS['password'];

    $Reservasi	= new ReservasiNonReguler();

    //memeriksa apakah tiket sudah dicetak atau belum
    $data_tiket	= $Reservasi->ambilDataKursi($no_tiket);

    if($data_tiket['CetakTiket']==1){
      //jika tiket sudah dicetak, maka memerlukan otorisasi untuk meneruskan proses ini

      $User = new User();

      $data_user	= $User->ambilDataDetailByUsername($username);

      if($data_user['user_password']!=md5($password)){
        //jika password tidak sesuai maka tidak diijinkan melakukna proses ini
        echo(0);
        exit;
      }

      $petugas_otorisasi	= $data_user['user_id'];
    }
    else{
      $petugas_otorisasi	= "NULL";
    }

    //mengambil harga tiket berlaku
    $harga_tiket= $Reservasi->getHargaTiket($data_tiket['KodeJadwal'],$data_tiket['TglBerangkat']);

    //MEMERIKSA DISCOUNT
    if($jenis_discount!=''){
      $data_discount	= $Reservasi->ambilDiscount($jenis_discount);
      $kode_discount	= $data_discount['KodeDiscount'];
      $nama_discount	= $data_discount['NamaDiscount'];
      $jumlah_discount= $data_discount['JumlahDiscount'];

      //MEMERIKSA DISCOUNT
      if($data_discount['IsHargaTetap']==1){
        $besar_discount	= $harga_tiket-($data_discount['JumlahDiscount']<=$harga_tiket?$data_discount['JumlahDiscount']:$harga_tiket);
      }
      else if($besar_discount>1 && $besar_discount<=$harga_tiket){
        $besar_discount	= $data_discount['JumlahDiscount'];
      }
      else if($besar_discount>0 && $besar_discount<=1){
        $besar_discount	= $harga_tiket*$besar_discount;
      }
      else if($besar_discount>$harga_tiket){
        $besar_discount	= $harga_tiket;
      }

    }
    else{
      $nama_discount		= "";
      $jumlah_discount	= 0;
      $besar_discount		= 0;
      $nama_discount		= "";
      $kode_discount		= "";
    }


    //mengupdate total bayar sesuai dengan jumlah discount
    $sql	=
      "UPDATE tbl_reservasi SET
        JenisPenumpang='$kode_discount',
        Discount=$besar_discount,
        JenisDiscount='$nama_discount',
        SubTotal=$harga_tiket,
        Total=SubTotal-Discount
      WHERE NoTiket IN('$no_tiket');";

    if (!$result = $db->sql_query($sql)){
      //die_error('GAGAL MENGUBAH DATA');//,__LINE__,__FILE__,$sql);
    }

    //INSERT KE LOG KOREKSI DISCOUNT
    $sql	=
      "INSERT INTO tbl_log_koreksi_disc(
        NoTiket,KodeCabang,KodeJadwal,
        IdJurusan,TglBerangkat,JamBerangkat,
        KodeBooking,Nama,Telp,
        WaktuPesan,NomorKursi,HargaTiket,
        Charge,SubTotal,DiscountMula,
        DiscountBaru,Total,PetugasPenjual,
        CetakTiket,PetugasCetakTiket,WaktuCetakTiket,
        JenisDiscountMula,JenisDiscountBaru,JenisPembayaran,
        FlagBatal,JenisPenumpangMula,JenisPenumpangBaru,
        PetugasPengkoreksi,PetugasOtorisasi,WaktuKoreksi,
        WaktuCetakSPJ
      )VALUES(
        '$data_tiket[NoTiket]','$data_tiket[KodeCabang]','$data_tiket[KodeJadwal]',
        '$data_tiket[IdJurusan]','$data_tiket[TglBerangkat]','$data_tiket[JamBerangkat]',
        '$data_tiket[KodeBooking]','$data_tiket[Nama]','$data_tiket[Telp]',
        '$data_tiket[WaktuPesan]','$data_tiket[NomorKursi]','$data_tiket[HargaTiket]',
        '$data_tiket[Charge]','$data_tiket[SubTotal]','$data_tiket[Discount]',
        IF($jumlah_discount>1,IF($jumlah_discount<=$harga_tiket,$jumlah_discount,$harga_tiket),$jumlah_discount*$harga_tiket),SubTotal-DiscountBaru,'$data_tiket[PetugasPenjual]',
        '$data_tiket[CetakTiket]','$data_tiket[PetugasCetakTiket]','$data_tiket[WaktuCetakTiket]',
        '$data_tiket[JenisDiscount]','$nama_discount','$data_tiket[JenisPembayaran]',
        '$data_tiket[FlagBatal]','$data_tiket[JenisPenumpang]','$kode_discount',
        '$userdata[user_id]',$petugas_otorisasi,NOW(),
        '$$data_tiket[TglCetakSPJ]');";

    if (!$result = $db->sql_query($sql)){
      die_error('GAGAL MENGUBAH DATA');//,__LINE__,__FILE__,$sql);
    }

    echo(1);
  exit;


  //KONFIRM MUTASI================================================================================================================================

  case 'konfirmmutasi':

    $username				= $HTTP_POST_VARS['username'];
    $password				= $HTTP_POST_VARS['password'];

    $User = new User();

    $data_user	= $User->ambilDataDetailByUsername($username);

    if($data_user['user_password']!=md5($password)){
      //jika password tidak sesuai maka tidak diijinkan melakukna proses ini
      echo(
        "alert('Anda tidak memiliki otoritas untuk melakukan proses ini!');
        document.getElementById('mutasi_username').value='';
        document.getElementById('mutasi_password').value=''
        dlgmutasi.hide();");
      exit;
    }

    echo("dlgmutasi.hide();setFlagMutasi();");
  exit;


  //MUTASI PENUMPANG===============================================================================================================================================================================
  case "mutasipenumpang":

    //dikomen tanggal 5 maret 2014 16:56 atas permintaan pak chan sehingga CSO juga bisa mutasi
    // UPDATE !!! nilai kursi sesuai dengan nilai yang dimasukan...
    /*if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['CSO2']))){
      exit;
    }*/

    $tgl     			= $HTTP_POST_VARS['tanggal'];
    $kode_jadwal	= $HTTP_POST_VARS['kode_jadwal'];
    $no_tiket			= $HTTP_POST_VARS['no_tiket'];
    $no_kursi			= $HTTP_POST_VARS['no_kursi'];
    $no_spj				= $HTTP_POST_VARS['no_spj'];
    $layout_kursi	= $HTTP_POST_VARS['layout_kursi'];

    if($no_spj!="" && $userdata['user_level']>=$USER_LEVEL_INDEX['SPV_OPERASIONAL']){
      $username		= $HTTP_POST_VARS['username'];
      $password		= $HTTP_POST_VARS['password'];

      $User = new User();

      $data_user	= $User->ambilDataDetailByUsername($username);

      if($data_user['user_password']!=md5($password)){
        //jika password tidak sesuai maka tidak diijinkan melakukna proses ini
        echo(3);exit;
      }
    }

    $Reservasi	= new ReservasiNonReguler();
    $Jadwal			= new Jadwal();
    $Promo			= new Promo();
    $Jurusan		= new Jurusan();

    $session_id	= $userdata['session_id'];
    $useraktif	= $userdata['user_id'];

    //Mengambil data tiket yang lama
    $data_tiket_lama 	= $Reservasi->ambilDataKursiByNoTiket($no_tiket);

    $id_member				= $data_tiket_lama['IdMember'];
    $tgl_lama					= $data_tiket_lama['TglBerangkat'];
    $kode_jadwal_lama	= $data_tiket_lama['KodeJadwal'];
    $no_kursi_lama		= $data_tiket_lama['NomorKursi'];
    $nama_lama				= $data_tiket_lama['Nama'];
    $cetak_tiket_lama	= $data_tiket_lama['CetakTiket'];
    //--END data tiket lama

    //memgambil data rute dan jadwal lama
    $row 					          = $Jadwal->ambilDataDetail($kode_jadwal_lama);
    $kode_jadwal_utama_lama = $row['KodeJadwalUtama'];
    $kode_jadwal_layout_lama= ($row['FlagSubJadwal']!=1)? $kode_jadwal_lama : $row['KodeJadwalUtama'];

    //memgambil data rute dan jadwal
    $row 					      = $Jadwal->ambilDataDetail($kode_jadwal);
    $jam_berangkat      = $row['JamBerangkat'];
    $id_jurusan 	      = $row['IdJurusan'];
    $kode_jadwal_utama  = $row['KodeJadwalUtama'];
    $is_sub_jadwal      = $row['FlagSubJadwal'];
    $kode_jadwal_layout	= ($is_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
    $kode_cabang_asal   = $row['KodeCabangAsal'];
    $kode_cabang_tujuan = $row['KodeCabangTujuan'];

    //mengambil 	data header tb posisi
    $row						= $Reservasi->ambilDataHeaderLayout($tgl,$kode_jadwal_layout);
    $kode_kendaraan	= $row['KodeKendaraan'];
    $kode_sopir			= $row['KodeSopir'];
    $tgl_cetak_SPJ	= $row['TglCetakSPJ'];
    $petugas_cetak_spj= $row['PetugasCetakSPJ'];
    $cetak_spj			= ($no_spj=="")?0:1;

    //mengambil data-data kode account dan komisi
    $row														= $Jurusan->ambilDataDetail($id_jurusan);
    $komisi_penumpang_CSO						= $row['KomisiPenumpangCSO'];
    $kode_akun_pendapatan						= $row['KodeAkunPendapatanPenumpang'];
    $kode_akun_komisi_penumpang_CSO	= $row['KodeAkunKomisiPenumpangCSO'];

    $harga_tiket= $Reservasi->getHargaTiket($kode_jadwal,$tgl);
    $charge	=0;
    $PPN=0;
    $jenis_muatan=0;

    //Mengubah posisi TUJUAN
    if(!$Reservasi->ubahPosisi($kode_jadwal_layout, $tgl, 1)){
      echo(0);
      exit;
    }

    //Mereset posisi SEBELUMNYA
    if(!$Reservasi->ubahPosisi($kode_jadwal_layout_lama, $tgl_lama, -1)){
      echo(0);
      exit;
    }

    $sub_total	= $harga_tiket;
    $total			= $sub_total+$charge+$PPN-$discount;


    if(!$Reservasi->mutasiPenumpang(
      $no_tiket, $kode_jadwal, $kode_jadwal_utama,
      $id_jurusan, $kode_cabang_asal, $kode_cabang_tujuan,
      $is_sub_jadwal, $kode_kendaraan, $kode_sopir,
      $tgl, $jam_berangkat, $no_kursi,
      $harga_tiket, $sub_total, $total,
      $no_spj, $tgl_cetak_SPJ, $cetak_spj,
      $komisi_penumpang_CSO, $petugas_cetak_spj, $keterangan,
      $kode_akun_pendapatan, $kode_akun_komisi_penumpang_CSO, $payment_code,
      $nama_lama, $cetak_tiket_lama, $no_kursi_lama,
      $userdata['user_id'])){

      echo(2);
      exit;
    }

    /*include($adp_root_path . 'ClassAsuransi.php');

    $Asuransi	= new Asuransi();

    $data_asuransi	= $Asuransi->ambilDataDetailByNoTiket($no_tiket);

    if($data_asuransi['IdAsuransi']!=""){

      $Asuransi->ubahAsuransi(
        $data_asuransi['IdAsuransi'],$tgl, $id_jurusan,
        $kode_jadwal,  $jam_berangkat, $no_tiket,
        $data_asuransi['Nama'], $data_asuransi['TglLahir'], $data_asuransi['Telp'],
        $data_asuransi['HP'], $data_asuransi['PlanAsuransi'],  $data_asuransi['BesarPremi'],
        $userdata['user_id'],  $data_asuransi['WaktuTransaksi'], $userdata['KodeCabang'],
        $data_asuransi['FlagBatal']);
    }*/

    echo(1);
  exit;

  // UBAH TIKET==========================================================================================================================================================================================
  case "ubahTiket":
    //mengubah data tiket yang sudah pernah diinput. operasi ini bersifat konfirmasi
    $no_tiket		= $HTTP_GET_VARS['no_tiket'];   // no tiket
    $nama    		= $HTTP_GET_VARS['nama'];  // nama
    $alamat 				= $HTTP_GET_VARS['alamat']; //alamat
    $telepon 		= $HTTP_GET_VARS['telepon']; // telepon
    $hp      		= $HTTP_GET_VARS['hp']; // hp
    $discount		= $HTTP_GET_VARS['discount']; // discount

    $sql	="UPDATE TbReservasi
            SET
              nama0='$nama',
              alamat0='$alamat',
              telp0='$telepon',
              hp0='$hp',
              discount=$discount,
              total=(pesanKursi*(HargaTiket-$discount))
            WHERE noUrut='$no_tiket'";

    if (!$db->sql_query($sql)){
      //die_error('Cannot Save Transaksi',__LINE__,__FILE__,$sql);
      echo("Error :".__LINE__);exit;
    }

  exit;

  // KODE PEMBATALAN==========================================================================================================================================================================================
  case "kode_pembatalan":

    $sandi_pembatalan=new Enkripsi();

    //mengambil jam server
    $sql ="SELECT
      DAY(GETDATE()),
      MONTH(GETDATE()),
      YEAR(GETDATE());";

    if ($result = $db->sql_query($sql)){
      while ($row = $db->sql_fetchrow($result)){
        $kode_batal=$sandi_pembatalan->Encrypt(($row[2]%$row[0])+($row[1]*$row[2]).BulanString($row[0]).(($row[1]*$row[2])%$row[0]));
      }
    }
    else{
      //die_error('gagal');
      echo("Error :".__LINE__);exit;
    }

    echo($kode_batal);

  exit;

  // LIST JENIS DISCOUNT=========================================================================================================================
  case 'list_jenis_discount':
      // membuat list jenis discount
      $Promo	= new Promo();

      $kode_jadwal 		= $HTTP_GET_VARS['kode_jadwal'];
      $tgl_berangkat 	= $HTTP_GET_VARS['tgl_berangkat'];
      $flag_koreksi 	= $HTTP_GET_VARS['flag_koreksi'];
      $jenis_penumpang= $HTTP_GET_VARS['jenis_penumpang'];

      //MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU JIKA DITEMUKAN ADA PROMO, MAKA DISCOUNT YANG LAIN TIDAK BERLAKU
      $data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl_berangkat);

      $target_promo	= $data_discount_point['FlagTargetPromo'];

      if($target_promo=='' || $target_promo==1){
        if($flag_koreksi=='' || $flag_koreksi==0){
          $opt = "<SELECT id='opt_jenis_discount'>".setListDiscount($kode_jadwal,$tgl_berangkat)."</select>";
        }
        else{
          $opt = "<SELECT id='opt_jenis_discount_koreksi'>".setListDiscount($kode_jadwal,$tgl_berangkat,$jenis_penumpang)."</select>";
        }
      }
      else{
        $besar_discount		= ($data_discount_point['JumlahDiscount']>1)?"Rp.".number_format($data_discount_point['JumlahDiscount'],0,",","."):$data_discount_point['JumlahDiscount']."%";
        $opt	="<b>Promo Discount $besar_discount<b>";
      }

      echo($opt);

    exit;


  case 'cari_jadwal_pelanggan':
      // membuat list jenis discount
      $Reservasi	= new ReservasiNonReguler();

      $no_telp	= $HTTP_GET_VARS['no_telp'];

      $result	= $Reservasi->cariJadwalKeberangkatan($no_telp);

      if ($result){

        $return	= "
          <table>
            <tr>
              <th width=100>Nama</th>
              <th width=100>Tgl.Pergi</th>
              <th width=100>Jam</th>
              <th width=200>Asal</th>
              <th width=200>Tujuan</th>
              <th width=100>Kode Booking</th>
              <th width=100>No.Kursi</th>
              <th width=100>Tiket</th>
              <th width=100>Status</th>
            </tr>";

        $idx	= 0;
        while ($row = $db->sql_fetchrow($result)){
          $idx++;
          $nomor_kursi	="";

          $result_nomor_kursi	= $Reservasi->ambilNomorKursi($row['KodeBooking']);

          while($data_no_kursi = $db->sql_fetchrow($result_nomor_kursi)){
            $nomor_kursi	.=$data_no_kursi[0].",";
          }

          $nomor_kursi	= substr($nomor_kursi,0,-1);

          $status_tiket			= ($row['CetakTiket'])?"Dibayar":"Book";
          $status_berangkat	= ($row['CetakSPJ'])?"Berangkat<BR>".dateparse(FormatMySQLDateToTglWithTime($row['TglCetakSPJ'])):"Belum Berangkat";

          $return	.="
            <tr bgcolor='dfdfdf'>
              <td>$row[Nama]</td>
              <td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
              <td align='center'>$row[JamBerangkat]</td>
              <td>$row[Asal]</td>
              <td>$row[Tujuan]</td>
              <td>$row[KodeBooking]</td>
              <td align='center'>$nomor_kursi</td>
              <td align='center'>$status_tiket</td>
              <td align='center'>$status_berangkat</td>
            </tr>";
        }
      }
      else{
        echo("Err :".__LINE__);exit;
      }

      if($idx==0){
        $return .="<tr><td colspan=9 class='banner' align='center'><h2>Tidak ada keberangkatan ditemukan!</h2></td></tr>";
      }

      echo($return."</table>");

    exit;

    case 'cari_paket':
      // membuat list jenis discount
      $Reservasi	= new ReservasiNonReguler();

      $no_resi	= $HTTP_GET_VARS['no_resi'];

      $result	= $Reservasi->cariPaket($no_resi);

      if ($result){

        $return	= "
          <table>
            <tr>
              <th width=100>Tgl.Pergi</th>
              <th width=150>Asal</th>
              <th width=150>Tujuan</th>
              <th width=70>Jam</th>
              <th width=100>Id Kendaraan</th>
              <th width=100>Sopir</th>
              <th width=100>Kode Booking</th>
              <th width=150>Pengirim</th>
              <th width=150>Penerima</th>
              <th width=150>Pengambil</th>
            </tr>";

        $jum_data	= 0;

        while ($row = $db->sql_fetchrow($result)){

          if($row['StatusDiambil']==1){
            $output_pengambilan	="
            <div valign='top'>
              Nama	: $row[NamaPengambil]<br>
              KTP		: $row[NoKTPPengambil]<br>
              Waktu	: ".FormatMySQLDateToTglWithTime($row['WaktuPengambilan'])."
              CSO		: $row[NamaPetugasPemberi]
            </div>
            ";
          }
          else{
            $output_pengambilan	="
              <div align='center'><a href='' onClick='ambilDataPaket(\"$row[NoTiket]\");return false;'>Ambil Paket</a></div>
            ";
          }

          $return	.="
            <tr bgcolor='dfdfdf'>
              <td>".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))."</td>
              <td>$row[Asal]</td>
              <td>$row[Tujuan]</td>
              <td align='center'>$row[JamBerangkat]</td>
              <td align='left'>$row[KodeKendaraan] ($row[NoPolisi])</td>
              <td align='left'>$row[NamaSopir]</td>
              <td>$row[NoTiket]</td>
              <td valign='top'>
                Nama: $row[NamaPengirim]<br>
                Alamat: $row[AlamatPengirim]<br>
                Telp: $row[TelpPengirim]
              </td>
              <td valign='top'>
                Nama: $row[NamaPenerima]<br>
                Alamat: $row[AlamatPenerima]<br>
                Telp: $row[TelpPenerima]
              </td>
              <td>
                $output_pengambilan
              </td>
            </tr>";

          $jum_data++;
        }
      }
      else{
        echo("Err :".__LINE__);exit;
      }

      if($jum_data==0){
        $return .="<tr><td colspan=11 class='banner' align='center'><h2>Data paket tidak ditemukan!</h2></td></tr>";
      }

      echo($return."</table>");

    exit;

  case "updatestatuscetaktiket":

    $Reservasi	= new ReservasiNonReguler();

    $cetak_tiket				= $HTTP_GET_VARS['cetak_tiket'];
    $list_no_tiket			= str_replace("\'","'",$HTTP_GET_VARS['list_no_tiket']);
    $kode_jadwal				= $HTTP_GET_VARS['kode_jadwal'];
    $tanggal						= $HTTP_GET_VARS['tanggal'];
    $jenis_pembayaran		= $HTTP_GET_VARS['jenis_pembayaran'];
    $list_kode_booking	= str_replace("\'","'",$HTTP_GET_VARS['list_kode_booking']);

    if($cetak_tiket!=1){
      //UPDATE FLAG TIKET DI LAYOUT KURSI
      //mengambil jadwal utama
      $sql	=
        "SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
        FROM tbl_md_jadwal
        WHERE KodeJadwal='$kode_jadwal';";

      if (!$result = $db->sql_query($sql)){
        echo("Err :".__LINE__);
        exit;
      }

      $row = $db->sql_fetchrow($result);
      $kode_jadwal_utama	= $row[0];

      $sql =
        "UPDATE tbl_posisi_detail SET StatusBayar=1
        WHERE
          KodeJadwal='$kode_jadwal_utama'
          AND TGLBerangkat='$tanggal'
          AND NoTiket IN ($list_no_tiket);";

      if(!$result = $db->sql_query($sql)){
        echo("Err :".__LINE__);
        exit;
      }

      //mengupate flag cetak tiket

      $list_kode_booking	= substr($list_kode_booking,0,-1);
      $Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
    }


    exit;
  //=====================================================================================================================
    case 'sisa_kursi_next':
      //MENGAMBIL DAN MENAMPILKAN SISA KURSI UNTUK BEBERAPA JAM KEDEPAN
      $id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];
      $tgl 						= $HTTP_GET_VARS['tgl_berangkat'];
      $jam_berangkat 	= $HTTP_GET_VARS['jam_berangkat'];

      $Jadwal	= new Jadwal();

      $result	= $Jadwal->setSisaKursiJadwalNext($tgl,$id_jurusan,$jam_berangkat);

      echo $result;

    exit;

  //======================================================================================================================
    case 'ambil_list_harga_paket':
      //Mengambil daftar harga paket
      include($adp_root_path .'/ClassPaket.php');
      $id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];

      $Paket= new Paket();

      $result= $Paket->ambilDaftarHarga($id_jurusan);

      if ($result){
        $row = $db->sql_fetchrow($result);

        //return eval
        echo("
          document.getElementById('dlg_paket_harga_kg_pertama_1').value 		= $row[HargaPaket1KiloPertama];
          document.getElementById('dlg_paket_harga_kg_pertama_2').value 		= $row[HargaPaket2KiloPertama];
          document.getElementById('dlg_paket_harga_kg_pertama_3').value 		= $row[HargaPaket3KiloPertama];
          document.getElementById('dlg_paket_harga_kg_pertama_4').value 		= $row[HargaPaket4KiloPertama];
          document.getElementById('dlg_paket_harga_kg_pertama_5').value 		= $row[HargaPaket5KiloPertama];
          document.getElementById('dlg_paket_harga_kg_berikutnya_1').value 	= $row[HargaPaket1KiloBerikut];
          document.getElementById('dlg_paket_harga_kg_berikutnya_2').value 	= $row[HargaPaket2KiloBerikut];
          document.getElementById('dlg_paket_harga_kg_berikutnya_3').value 	= $row[HargaPaket3KiloBerikut];
          document.getElementById('dlg_paket_harga_kg_berikutnya_4').value 	= $row[HargaPaket4KiloBerikut];
          document.getElementById('dlg_paket_harga_kg_berikutnya_5').value 	= $row[HargaPaket5KiloBerikut];
        ");

      }
      else{
        echo("Error ".__LINE__);
      }

    exit;

    //VOUCHER======================================================================================================================
    case 'bayar_by_voucher':
      include($adp_root_path . 'ClassVoucher.php');

      $Voucher			= new Voucher();
      $Reservasi	= new ReservasiNonReguler();

      $kode_voucher	= $HTTP_POST_VARS['kode_voucher'];
      $tanggal			= $HTTP_POST_VARS['tanggal'];
      $id_jurusan		= $HTTP_POST_VARS['id_jurusan'];
      $no_tiket			= $HTTP_POST_VARS['no_tiket'];

      $data_penumpang = $Reservasi->ambilDataKursi($no_tiket);

      if($data_penumpang['JenisPenumpang']=='R'){
        //VOUCHER TIDAK BOLEH DIGUNAKAN UNTUK MEMBAYAR TIKET KHUSUS
        echo("alert('VOUCHER hanya dapat digunakan untuk pembayaran tiket reguler!');");
        exit;
      }

      $is_day_allowed	= 0;

      $data_return	= $Voucher->verifyVoucher($kode_voucher,$tanggal,$id_jurusan);

      if(!$data_return['status']){
        switch($data_return['error']){
          case 'INVALID':
            echo("alert('Kode Voucher yang anda masukkan TIDAK VALID!');");
          break;

          case 'EXPIRED':
            echo("alert('Kode Voucher yang anda masukkan telah EXPIRED!');");
          break;

          case 'INVALID_DAY':
            echo("alert('Voucher anda hanya dapat digunakan pada hari WEEK DAY saja.');");
          break;

          case 'INVALID_RUTE':
            echo("alert('Voucher anda adalah voucher RETURN, hanya bisa digunakan untuk keberangkatan sebaliknya dari RUTE keberangkatan anda terdahulu.');");
          break;

        }
        exit;
      }

      echo("CetakTiketByVoucher('".$kode_voucher."');dialog_voucher.hide();");

    exit;
}
?>
