<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']>=$LEVEL_MANAJER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode    		= $HTTP_GET_VARS['mode'];
$act    		= $HTTP_GET_VARS['act'];
$submode 		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'blank';

function PeriksaKodePointRute($kode_point_rute){
	global $db;
	
	$sql = "SELECT COUNT(kode)
						FROM	TbMDKota
						WHERE kode='$kode_point_rute'";
					
	if ($result = $db->sql_query($sql)){
			
		while ($row=$db->sql_fetchrow($result)){   
			$data_ditemukan=$row[0];
		}
	}
	else{
		die_error('GAGAL memeriksa kode_point_rute');//,__FILE__,__LINE__,$sql);exit;
	}
	
	//jika kode_point_rute belum ada, maka akan memberikan nilai valid (1), jika usernam sudah ada, akan memberikan nilai false (0)
	return (1-$data_ditemukan);
}

switch($mode){

//MENAMPILKAN KOLOM ISIAN BLANK==========================================================================================================
case 'blank':
		
	$pesan="Penambahan";
	
	$template->set_filenames(array('body' => 'point_rute_detail.tpl')); 
	$template->assign_vars(array
	  ( 'USERNAME'  =>$userdata['username'],
	   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('point_rute.'.$phpEx).'">Point Rute</a>',
			'PESAN'	=>$pesan
		));
break;

//TAMBAH PENGGUNA BARU ==========================================================================================================
case 'tambah_point_rute':
	
	//ambil isi dari parameter
	
	$kode_point_rute		=$HTTP_GET_VARS['kode_point_rute'];
	
	//memeriksa  apakah kode_point_rute sudah ada dalam sistem atau belum
	if(!PeriksaKodePointRute($kode_point_rute)){
		echo("duplikasi"); 
		exit;
	}
	
	$nama		=$HTTP_GET_VARS['nama'];
	$kota		=$HTTP_GET_VARS['kota'];
	
	$sql = 
		"INSERT INTO TbMDKota(
			kode,nama,namakota,keterangan,Reguler)
		VALUES (
			'$kode_point_rute','$nama','$kota','REGULER',1
		);";
	
	if (!$result = $db->sql_query($sql)){
		die_error('Gagal menyimpan data');//,__FILE__,__LINE__,$sql);
	}
	
exit;

//UBAH POINT KOTA ==========================================================================================================
case 'ubah_point_rute':
	
	//ambil isi dari inputan
	$kode_point_rute_old=$HTTP_GET_VARS['kode_point_rute_old'];
	$kode_point_rute		=$HTTP_GET_VARS['kode_point_rute'];
	$nama								=$HTTP_GET_VARS['nama'];
	$kota								=$HTTP_GET_VARS['kota'];
	
	
	//memeriksa  apakah kode_point_rute sudah ada dalam sistem atau belum
	if(!PeriksaKodePointRute($kode_point_rute) && $kode_point_rute!=$kode_point_rute){
		echo("duplikasi"); 
		exit;
	}
	
	$sql = 
		"UPDATE
			TbMDKota
		SET 
			kode='$kode_point_rute',
			nama='$nama',
			namakota='$kota'
		WHERE kode='$kode_point_rute_old'";
		
	if(!$result = $db->sql_query($sql)){
		//die_error('Gagal mengubah data anggota',__FILE__,__LINE__,$sql);
		die_error('Gagal mengubah data point_rute');
	}
	
exit;

//Ambil data anggota ==========================================================================================================
case 'ambil_data_point_rute':
	
	$kode_point_rute    = $HTTP_GET_VARS['kode_point_rute'];
	
	$sql = 
		"SELECT 
			kode,
			nama,namakota
		FROM	TbMDKota WHERE kode='$kode_point_rute'";
				
	if ($result = $db->sql_query($sql)){
		while ($row=$db->sql_fetchrow($result)){   
			$kode_point_rute			=$row['kode'];
			$nama									=$row['nama'];
			$kota									=$row['namakota'];
		}
		
	}
	else {
		die_error('Gagal mengamnbil data');
	}
	
	//memeriksa apakah data point_rute dengan kode_point_rute ditemukan dalam database
	if($kode_point_rute==""){
		//jika tidak ditemukan, akan langsung didirect ke halaman point_rute
		redirect(append_sid('point_rute.'.$phpEx),true); 
	}
	
	$pesan="Pengubahan";
	
	$template->set_filenames(array('body' => 'point_rute_detail.tpl')); 
	$template->assign_vars(array
	  ( 'BCRUMP'    			=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('point_rute.'.$phpEx).'">Point Rute</a>',
			'PESAN'						=>$pesan,
			'KODE_POINT_RUTE'	=>$kode_point_rute,
			'NAMA'						=>$nama,
			'KOTA'						=>$kota
  ));
	
break;
} //switch mode

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>