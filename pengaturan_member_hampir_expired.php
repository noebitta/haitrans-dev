<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CCARE))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Member	= new Member();


	
		// LIST
		$template->set_filenames(array('body' => 'member/member_hampir_expired_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";
		
		$kondisi	=($cari=="")?"WHERE 1 ":
			" WHERE (IdMember LIKE '%$cari%' 
				OR Nama LIKE '%$cari%' 
				OR Alamat LIKE '%$cari%' 
				OR Handphone LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR Email LIKE '%$cari%'
				OR Pekerjaan LIKE '%$cari%')";
		
		$kondisi.=
			"	AND DATEDIFF(ExpiredDate,NOW())<=$THRESHOLD_MEMBER_EXPIRED";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdMember","tbl_md_member","&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",$kondisi,"pengaturan_member_hampir_expired.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *,
				f_member_hitung_frekwensi_by_tanggal(IdMember,'2010-12-31',NOW()) AS Frekwensi,
				DATEDIFF(ExpiredDate,NOW()) AS MasaBerlaku
			FROM tbl_md_member $kondisi
			$kondisi_sort LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['MasaBerlaku']>0){
					$status="Aktif";
				}
				else{
					$odd	= "red";
					$status="EXPIRED";
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama'=>$row['Nama'],
							'id_member'=>$row['IdMember'],
							'alamat'=>$row['Alamat'],
							'hp'=>$row['Telp']."/".$row['Handphone'],
							'email'=>$row['Email'],
							'pekerjaan'=>$row['Pekerjaan'],
							'frekwensi'=>$row['Frekwensi'],
							'tgl_expired'=>dateparse(FormatMySQLDateToTgl($row['ExpiredDate'])),
							'aktif'	=>$status
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=11 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			echo("Error :".__LINE__);exit;
		} 
		
		$order_invert	= ($order=='asc' || $order=='')?'desc':'asc';
		
		$parameter_sorting	= 'pengaturan_member_hampir_expired.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir;
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&cari=".$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir."&sort_by=".$sort_by."&order=".$order;
			
		//$script_cetak_pdf="Start('pengaturan_member_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
														
		$script_cetak_excel="Start('pengaturan_member_hampir_expired_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		
		//--END KOMPONEN UNTUK EXPORT
		
		$page_title	= "Member Hampir Expired";
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('menu_pengelolaan_member.'.$phpEx.'?top_menu_dipilih=top_menu_pengelolaan_member') .'">Home</a> | <a href="'.append_sid('pengaturan_member_hampir_expired.'.$phpEx).'">Member Hampir Expired</a>',
			'ACTION_CARI'		=> append_sid('pengaturan_member_hampir_expired.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'TGL_AWAL'			=> $tanggal_mulai,
			'TGL_AKHIR'			=> $tanggal_akhir,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging,
			'A_SORT_BY_ULTAH'			=>  append_sid($parameter_sorting.'&sort_by=TglLahir&order='.$order_invert),
			'A_SORT_BY_NAMA'			=>  append_sid($parameter_sorting.'&sort_by=Nama&order='.$order_invert),
			'A_SORT_BY_KODE'			=>  append_sid($parameter_sorting.'&sort_by=IdMember&order='.$order_invert),
			'A_SORT_BY_ALAMAT'		=>  append_sid($parameter_sorting.'&sort_by=Nama&order='.$order_invert),
			'A_SORT_BY_HP'				=>  append_sid($parameter_sorting.'&sort_by=Handphone&order='.$order_invert),
			'A_SORT_BY_EMAIL'			=>  append_sid($parameter_sorting.'&sort_by=Email&order='.$order_invert),
			'A_SORT_BY_PEKERJAAN'	=>  append_sid($parameter_sorting.'&sort_by=Pekerjaan&order='.$order_invert),
			'A_SORT_BY_FREKWENSI'	=>  append_sid($parameter_sorting.'&sort_by=Frekwensi&order='.$order_invert),
			'A_SORT_BY_TGL_EXPIRED'	=>  append_sid($parameter_sorting.'&sort_by=ExpiredDate&order='.$order_invert),
			'CETAK_XL'						=> $script_cetak_excel
			)
		);
		
	  

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>