<html>
<head>
<title>PHPMailer - SMTP (Gmail) advanced test</title>
</head>
<body>

<?php
require_once('PHPMailer_v5_1/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

$mail->IsSMTP(); // telling the class to use SMTP

try {
$mail->Host = "smtp.gmail.com"; // SMTP server
$mail->SMTPDebug = 2; // enables SMTP debug information (for testing)
$mail->SMTPAuth = true; // enable SMTP authentication
$mail->SMTPSecure = "ssl"; // sets the prefix to the servier
$mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
$mail->Port = 465; // set the SMTP port for the GMAIL server
$mail->Username = "yan.fari@gmail.com"; // GMAIL username
$mail->Password = "b412t0n"; // GMAIL password

//$mail->AddReplyTo('name@yourdomain.com', 'First Last');
//$mail->AddAddress('user1@gmail.com', 'user1'); //Set Send To Address
$mail->AddAddress('barton@tbk.co.id', 'user2'); //Set Send To Address

$mail->SetFrom('yan.fari@gmail.com', 'Me'); // Set Send From Address
//$mail->AddReplyTo('name@yourdomain.com', 'First Last');

$mail->Subject = 'Email with Attachment with PHP is done';
$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
$mail->MsgHTML(file_get_contents('content.html'));

//$mail->AddAttachment('/images/asdf.pdf'); // attachment
//$mail->AddAttachment('/images/asd.wav'); // attachment


$mail->Send();
echo "Message Sent OK</p>\n";

} catch (phpmailerException $e) {
echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
echo $e->getMessage(); //Boring error messages from anything else!
}
?>

</body>
</html>