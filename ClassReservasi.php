<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
	//redirect('index.'.$phpEx,true);
	exit;
}
//#############################################################################
class Reservasi{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	var $WAKTU_BACKUP;

	//CONSTRUCTOR
	function Reservasi(){
		$this->ID_FILE="C-RSV";
		$this->WAKTU_BACKUP = 2; //hari
	}

	//BODY

	function ambilDataHeaderLayout($tgl,$kode_jadwal){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$temp_array_date	= explode("-",$tgl);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi":"tbl_posisi";

		$sql =
			"SELECT
				ID,KodeKendaraan,JumlahKursi,
				KodeSopir,NoSPJ,TglCetakSPJ,PetugasCetakSPJ,
				Memo,PembuatMemo,WaktuBuatMemo,FlagMemo
			FROM $nama_tbl
			WHERE (KodeJadwal LIKE '$kode_jadwal' AND TglBerangkat='$tgl')";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);

			$data_posisi['ID']							= $row['ID'];
			$data_posisi['KodeKendaraan']		= $row['KodeKendaraan'];
			$data_posisi['JumlahKursi']			= $row['JumlahKursi'];
			$data_posisi['KodeSopir']				= $row['KodeSopir'];
			$data_posisi['NoSPJ']						= $row['NoSPJ'];
			$data_posisi['TglCetakSPJ']			= $row['TglCetakSPJ'];
			$data_posisi['PetugasCetakSPJ']	= $row['PetugasCetakSPJ'];
			$data_posisi['Memo']						= $row['Memo'];
			$data_posisi['PembuatMemo']			= $row['PembuatMemo'];
			$data_posisi['WaktuBuatMemo']		= $row['WaktuBuatMemo'];
			$data_posisi['FlagMemo']				= $row['FlagMemo'];

			$sql =
				"SELECT NoPolisi
				FROM tbl_md_kendaraan
				WHERE KodeKendaraan='".$data_posisi['KodeKendaraan']."'";

			if ($result = $db->sql_query($sql)){
				$row=$db->sql_fetchrow($result);

				$data_posisi['NoPolisi']	= $row['NoPolisi'];
			}

			$sql =
				"SELECT Nama
			  FROM tbl_md_sopir
			  WHERE KodeSopir='".$data_posisi['KodeSopir']."'";

			if ($result = $db->sql_query($sql)){
				$row=$db->sql_fetchrow($result);

				$data_posisi['NamaSopir']	= $row['Nama'];
			}


			return $data_posisi;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilData

	function ambilDataDetailLayout($tgl,$kode_jadwal){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$temp_array_date	= explode("-",$tgl);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi":"tbl_posisi";

		$sql =
			"SELECT *
			FROM $nama_tbl
			WHERE (KodeJadwal LIKE '$kode_jadwal' AND TglBerangkat='$tgl')";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilData

	function ambilDataFlagLayout($tgl,$kode_jadwal,$session_id){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;
		global $SESSION_TIME_EXPIRED;

		$temp_array_date	= explode("-",$tgl);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi_detail":"tbl_posisi_detail";

		$sql =
			"SELECT NomorKursi
			FROM $nama_tbl
			WHERE KodeJadwal LIKE '$kode_jadwal' 
				AND TglBerangkat='$tgl'
				AND Session=$session_id
				AND StatusKursi=1
				AND (NoTiket='' OR NoTiket IS NULL)
				AND f_reservasi_session_time_selisih(SessionTime)<=$SESSION_TIME_EXPIRED";

		if ($result = $db->sql_query($sql)){
			return $result;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilDataFlagLayout

	function tambahPosisi(
		$kode_jadwal, $tgl_berangkat, $jam_berangkat,
		$jumlah_kursi, $kode_kendaraan, $kode_sopir){

		/*
		IS	: data layout posisi belum ada dalam database
		FS	:Data layout posisi baru telah disimpan dalam database 
		*/

		//kamus
		global $db;

		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_posisi_tambah(
							'$kode_jadwal', '$tgl_berangkat', '$jam_berangkat',
							'$jumlah_kursi', '$kode_kendaraan', '$kode_sopir')";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE $sql".__LINE__);
		}

		return true;
	}

	function ubahPosisi($kode_jadwal, $tgl_berangkat, $jumlah_kursi_dipesan){

		/*
		IS	: data layout posisi belum ada dalam database
		FS	:Data layout posisi baru telah disimpan dalam database 
		*/

		//kamus
		global $db;

		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) (int) $temp_array_date[1] , (int) (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi":"tbl_posisi";

		//MENGUBAH DATA KEDALAM DATABASE
		$sql =
			"UPDATE $nama_tbl SET 
				SisaKursi=SisaKursi-$jumlah_kursi_dipesan
			WHERE TglBerangkat = '$tgl_berangkat' AND 
				KodeJadwal='$kode_jadwal';";

		/*$sql = 
			"UPDATE tbl_posisi SET 
				SisaKursi=SisaKursi-$jumlah_kursi_dipesan
			WHERE TglBerangkat = '$tgl_berangkat' AND 
				KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal('$kode_jadwal');";*/

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		return true;
	}

	function updateStatusKursi($kursi,$session_id,$kode_jadwal_dipilih,$jam_berangkat,$kode_cabang_asal,$kode_cabang_tujuan,$kode_jadwal_utama,$tgl,$user_level){

		/*
		IS	: data layout posisi belum ada dalam database
		FS	:Data layout posisi baru telah disimpan dalam database 
		*/

		//kamus
		global $db;
		global $USER_LEVEL_INDEX;
		global $SESSION_TIME_EXPIRED;

		$status_kursi_admin= 0;

		//MENGUBAH DATA KEDALAM DATABASE
		if(in_array($user_level,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"]))){
			$status_kursi_admin	=1;
		}

		$is_sub_jadwal  = $kode_jadwal_dipilih==$kode_jadwal_utama?0:1;

		//BERSIH-BERSIH
		$sql=
			"DELETE FROM tbl_posisi_detail
      WHERE
				KodeJadwalUtama='$kode_jadwal_utama'
				AND KodeJadwal!='$kode_jadwal_dipilih'
        AND TglBerangkat='$tgl'
        AND (NoTiket='' OR NoTiket IS NULL)
        AND Session=$session_id; ";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		$sql	=
			"CALL sp_reservasi_update_status_kursi(
				'$kursi', '$session_id', '$kode_jadwal_dipilih',
				'$jam_berangkat','$kode_cabang_asal','$kode_cabang_tujuan',
				'$is_sub_jadwal','$kode_jadwal_utama','$tgl',
				'$status_kursi_admin','$SESSION_TIME_EXPIRED')";

		if (!$db->sql_query($sql)){
			echo("kursi sudah terisi");
			exit;
		}

		/*
		if($db->sql_affectedrows()==0){
			$sql	=
				"INSERT INTO tbl_posisi_detail (
			    NomorKursi, KodeJadwal, TglBerangkat,
			    StatusKursi, Session,SessionTime)
			  VALUES(
			    $kursi, '$kode_jadwal', '$tgl',
			    1, '$session_id',NOW());";
			
			if (!$db->sql_query($sql)){
				die_error("Err $this->ID_FILE".__LINE__);
			}
		}*/

		return true;
	}

	function booking(
		$no_tiket, $kode_cabang, $kode_jadwal,
		$id_jurusan, $kode_kendaraan , $kode_sopir ,
		$tgl_berangkat, $jam_berangkat , $kode_booking,
		$id_member, $point_member, $nama ,
		$alamat, $telp, $HP,
		$nomor_kursi, $harga_tiket,
		$charge, $sub_total, $discount,
		$PPN, $total, $petugas_penjual,
		$flag_pesanan, $no_SPJ, $tgl_cetak_SPJ,
		$cetak_SPJ, $komisi_penumpang_CSO,
		$petugas_cetak_SPJ, $keterangan, $jenis_discount,
		$kode_akun_pendapatan, $jenis_penumpang, $kode_akun_komisi_penumpang_CSO,
		$payment_code){

		/*
		IS	: data layout posisi belum ada dalam database
		FS	:Data layout posisi baru telah disimpan dalam database 
		*/

		//kamus
		global $db;

		//MENGUBAH DATA KEDALAM DATABASE
		$sql = "CALL sp_reservasi_tambah(
		  '$no_tiket', '$kode_cabang', '$kode_jadwal',
		  $id_jurusan, '$kode_kendaraan ', '$kode_sopir',
		  '$tgl_berangkat', '$jam_berangkat' , '$kode_booking',
		  '$id_member', '$point_member', '$nama' ,
		  '$alamat', '$telp', '$HP',
		  NOW(), $nomor_kursi, $harga_tiket,
		  '$charge', '$sub_total', '$discount',
		  '$PPN', '$total', $petugas_penjual,
		  0, '$no_SPJ', '$tgl_cetak_SPJ','$cetak_SPJ', '$komisi_penumpang_CSO', 0,
		  '$petugas_cetak_SPJ', '$keterangan', '$jenis_discount',
		  '$kode_akun_pendapatan', '$jenis_penumpang', '$kode_akun_komisi_penumpang_CSO',
		  '$payment_code');";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		return true;
	}

	function ubahDataPenumpang($no_tiket, $nama , $alamat, $telp, $no_kursi, $id_discount, $id_member){

		/*
		IS	: data layout posisi belum ada dalam database
		FS	:Data layout posisi baru telah disimpan dalam database 
		*/

		//kamus
		global $db;

		//MENGUBAH DATA KEDALAM DATABASE
		$sql = "CALL sp_reservasi_ubah_data_penumpang('$no_tiket', '$nama', '$alamat','$telp','$id_member');";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		//MENGUPDATE TBL POSISI,
		$sql =
			"UPDATE tbl_posisi_detail
				SET Nama='$nama'
			WHERE NoTiket='$no_tiket' AND StatusBayar!=1;";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}


		$besar_discount	= 0;
		$kode_discount	= "U";
		$nama_discount	= "";

		//MEMERIKSA APAKAH ADA PEMBERIAN DISCOUNT
		if(trim($id_member)==''){

			if($id_discount!=''){
				//jika memang discount diberikan, maka akan diupdate pada database
				$data_discount	= $this->ambilDiscount($id_discount);
				$kode_discount	= $data_discount['KodeDiscount'];
				$nama_discount	= $data_discount['NamaDiscount'];
				$besar_discount	= $data_discount['JumlahDiscount'];

				$data_kursi = $this->ambilDataKursi($no_tiket);
				$harga_tiket= $data_kursi['HargaTiket'];

				//MEMERIKSA DISCOUNT
				if($data_discount['IsHargaTetap']==1){
					$besar_discount	= $harga_tiket-($data_discount['JumlahDiscount']<=$harga_tiket?$data_discount['JumlahDiscount']:$harga_tiket);
				}
				else if($besar_discount>1 && $besar_discount<=$harga_tiket){
					$besar_discount	= $data_discount['JumlahDiscount'];
				}
				else if($besar_discount>0 && $besar_discount<=1){
					$besar_discount	= $harga_tiket*$besar_discount;
				}
				else if($besar_discount>$harga_tiket){
					$besar_discount	= $harga_tiket;
				}

			}
			else{
				$Promo	= new Promo();

				$data_kursi = $this->ambilDataKursi($no_tiket);

				$data_discount_point	= $Promo->ambilDiscountPoint($data_kursi['KodeJadwal'],$data_kursi['TglBerangkat']);

				if($data_discount_point['FlagTargetPromo']==0 || $data_discount_point['FlagTargetPromo']==2){
					$besar_discount	= ($data_discount_point['JumlahDiscount']>1)?$data_discount_point['JumlahDiscount']:($data_discount_point['JumlahDiscount']/100)*$data_kursi['HargaTiket'];
				}
				else{
					$besar_discount	= 0;
				}
			}

		}
		else{
			//JIKA MEMBER AKAN MENGAMBIL DISCOUNT MEMBER

			$Promo	= new Promo();

			$data_kursi = $this->ambilDataKursi($no_tiket);

			$data_discount_point	= $Promo->ambilDiscountPoint($data_kursi['KodeJadwal'],$data_kursi['TglBerangkat']);

			$besar_discount		= ($data_discount_point['JumlahDiscount']>1)?$data_discount_point['JumlahDiscount']:($data_discount_point['JumlahDiscount']/100)*$data_kursi['HargaTiket'];
			$point			= $data_discount_point['JumlahPoint'];
			$target_promo	= $data_discount_point['FlagTargetPromo'];

			$Member = new Member();

			$data_member	= $Member->ambilData($id_member);
			$nama    			= $data_member['Nama'];
			//$alamat 			= $data_member['Alamat']; 
			//$telepon 			= $data_member['Handphone']; 

			//memeriksa discount 
			if($nama!="" && $data_member['MasaBerlaku']>0){
				$discount				= ($target_promo <=1 )?$discount:0;
				$point_member		= ($target_promo <=1 )?$point:0;
				$jenis_penumpang="K";
			}


			$jenis_discount	= ($discount==0)?"":$data_discount_point['NamaPromo'];
		}

		//MENGUBAH DATA KEDALAM DATABASE
		/*$sql = "CALL sp_reservasi_ubah_discount('$no_tiket', '$kode_discount', '$nama_discount','$besar_discount');";
		
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}*/

		$temp_array_date	= explode("-",$data_kursi['TglBerangkat']);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] ,(int) $temp_array_date[2]+2,(int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) < date('Y-m-d');

		if($tanggal_lampau!=1){
			$tbl_reservasi		= "tbl_reservasi";
		}
		else{
			$tbl_reservasi		= "tbl_reservasi";
		}

		if($kode_discount!="R"){
			$diskon	= $besar_discount>1?$besar_discount:$data_kursi['HargaTiket']*$besar_discount;
			$diskon	= $diskon<=$data_kursi['HargaTiket']?$diskon:$data_kursi['HargaTiket'];

			$sub_total	= $data_kursi['HargaTiket'];
		}
		else{
			$sql =
				"SELECT HargaTiket
				FROM  tbl_md_jurusan
				WHERE IdJurusan=".$data_kursi['IdJurusan'];

			if (!$result = $db->sql_query($sql)){
				return "Error";
			}

			$data_harga = $db->sql_fetchrow($result);

			$harga_tiket_normal	= $data_harga['HargaTiket'];

			if($besar_discount>1){
				$diskon	= $besar_discount;
			}
			else{
				$diskon	= $harga_tiket_normal*$besar_discount;
			}

			$diskon	= $diskon<=$harga_tiket_normal?$diskon:$harga_tiket_normal;

			$sub_total	= $data_kursi['HargaTiket']+$harga_tiket_normal;
		}

		$sql	=
			"UPDATE $tbl_reservasi SET
				JenisDiscount='$nama_discount',
				Discount=$diskon,
				SubTotal=$sub_total,
				Total=SubTotal-Discount,
				JenisPenumpang='$kode_discount'
			WHERE NoTiket IN('$no_tiket');";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE $sql".__LINE__);
		}


		$sql	=
			"SELECT TglBerangkat,KodeJadwal
			FROM tbl_posisi_detail 
			WHERE NoTiket='$no_tiket';";

		if(!$result=$db->sql_query($sql)){
			echo("Err $this->ID_FILE".__LINE__);
			return false;
		}

		$row=$db->sql_fetchrow($result);

		if($row[0]==""){
			$sql	=
				"SELECT TglBerangkat,KodeJadwal
				FROM tbl_posisi_detail_backup 
				WHERE NoTiket='$no_tiket';";

			if(!$result=$db->sql_query($sql)){
				echo("Err $this->ID_FILE".__LINE__);
				return false;
			}

			$row=$db->sql_fetchrow($result);

		}

		return true;
	}

	function updateStatusCetakTiket($cso_cetak_tiket,$jenis_pembayaran,$list_no_tiket,$cabang_transaksi){

		/*
		IS	: data tiket status cetak tiket telah terdefinisi
		FS	:data tiket status cetak tiket telah diupdate menjadi 1 
		*/

		//kamus
		global $db;

		$sql =
			"UPDATE tbl_reservasi 
				SET 
					CetakTiket=1,PetugasCetakTiket='$cso_cetak_tiket',
					WaktuCetakTiket=NOW(),
					JenisPembayaran=$jenis_pembayaran,
					KodeCabang='$cabang_transaksi'
				WHERE NoTiket IN($list_no_tiket) AND FlagBatal!=1 AND CetakTiket!=1;";

		if (!$db->sql_query($sql)){
			echo("Err $this->ID_FILE".__LINE__);
			return false;
		}

		/*if($db->sql_affectedrows()==0){
			//MENGUPDATE TBL POSISI,
			$sql =
				"UPDATE tbl_reservasi 
				SET 
					CetakTiket=1,PetugasCetakTiket='$cso_cetak_tiket',
					WaktuCetakTiket=NOW(),
					JenisPembayaran=$jenis_pembayaran,
					KodeCabang='$cabang_transaksi'
				WHERE KodeBooking IN($list_kode_booking) AND FlagBatal!=1 AND CetakTiket!=1;";
			
			if (!$db->sql_query($sql)){
				die_error("Err $this->ID_FILE $sql".__LINE__);
			}
		}*/

		return true;
	}

	function pembatalan($no_tiket, $kursi, $user_pembatal){

		/*
		IS	: data transaksi sudah ada  dalam database
		FS	:tiket dengan no tiket dibatalkan 
		*/

		//kamus
		global $db;
		global $LEVEL_CSO;
		global $LEVEL_ADMIN;
		global $LEVEL_SUPERVISOR;

		//Mengupdate flag batal pada tgl reservasi
		$sql =
			"UPDATE tbl_reservasi SET
			FlagBatal=1,PetugasPembatalan='$user_pembatal',
			WaktuPembatalan=NOW()
		WHERE NoTiket='$no_tiket';";

		if (!$db->sql_query($sql)){
			echo("Err $this->ID_FILE".__LINE__);
			return false;
		}

		if($db->sql_affectedrows()==0){
			//MENGUPDATE TBL RESERVASI OLAP,
			"UPDATE tbl_reservasi_olap SET
				FlagBatal=1,PetugasPembatalan='$user_pembatal',
				WaktuPembatalan=NOW()
			WHERE NoTiket='$no_tiket';";

			if (!$db->sql_query($sql)){
				echo("Err $this->ID_FILE".__LINE__);
				return false;
			}
		}

		//MENGUPDATE TBL POSISI, MEMBEBASKAN STATUS KURSI YANG DIBATALKAN
		$sql =
			"UPDATE tbl_posisi_detail 
			SET StatusKursi = 0, Nama=NULL, NoTiket=NULL,
				KodeBooking=NULL,Session=NULL,StatusBayar=0
			WHERE NoTiket='$no_tiket';";

		if (!$db->sql_query($sql)){
			echo("Err $this->ID_FILE".__LINE__);
			return false;
		}

		if($db->sql_affectedrows()==0){
			//MENGUPDATE TBL POSISI,
			$sql =
				"UPDATE tbl_posisi_detail_backup 
				SET StatusKursi = 0, Nama=NULL, NoTiket=NULL,
					KodeBooking=NULL,Session=NULL,StatusBayar=0
				WHERE NoTiket='$no_tiket';";

			if (!$db->sql_query($sql)){
				echo("Err $this->ID_FILE".__LINE__);
				return false;
			}

		}

		//MEMBATALKAN PENGGUNAAN VOUCHER
		$sql = "DELETE FROM tbl_voucher WHERE NoTiketBerangkat='$no_tiket';";

		if (!$db->sql_query($sql)){
			echo("Err $this->ID_FILE".__LINE__);
			return false;
		}

		return true;
	}

	/*FUNCTION LAMA
	 function getHargaTiket($kode_jadwal,$tgl_berangkat){
    global $db;
		
		$sql="SELECT f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','$tgl_berangkat');";
    
		
		if ($result = $db->sql_query($sql)){
      while ($row = $db->sql_fetchrow($result)){
				$harga = $row[0];
      }
		} 
		else{      
			$harga="Error ".__LINE__;
    }

    return $harga;
	}*/

	function getHargaTiket($kode_jadwal,$id_layout,$tgl_berangkat){
		global $db;

		$sql="SELECT f_jurusan_get_harga_tiket_by_kode_jadwal('$kode_jadwal','$tgl_berangkat');";


		if(!$result = $db->sql_query($sql)){
			$harga_tiket="Error ".__LINE__;
		}

		$row = $db->sql_fetchrow($result);

		/*$idx_layout   = strpos($row[0],$id_layout);
		$string_temp  = substr($row[0],$idx_layout);
		$idx_equal    = strpos($string_temp,"=");
		$idx_delimiter= strpos($string_temp,";");
		$harga_tiket  = substr($string_temp,$idx_equal+1,$idx_delimiter-$idx_equal-1);*/
		$harga_tiket = $row[0];

		return $harga_tiket;
	}

	/* script lama
	 function getHargaTiketNormal($id_jurusan){
    global $db;
		
		$sql="SELECT HargaTiket FROM tbl_md_jurusan WHERE IdJurusan='$id_jurusan'";
    
		
		if ($result = $db->sql_query($sql)){
      while ($row = $db->sql_fetchrow($result)){
				$harga = $row[0];
      }
		} 
		else{      
			$harga="Error ".__LINE__;
    }      
    return $harga;
	}*/

	function getHargaTiketNormal($id_jurusan,$id_layout){
		global $db;

		$sql="SELECT HargaTiket FROM tbl_md_jurusan WHERE IdJurusan='$id_jurusan'";


		if(!$result = $db->sql_query($sql)){
			$harga_tiket="Error ".__LINE__;
		}

		$row = $db->sql_fetchrow($result);

		$idx_layout   = strpos($row[0],$id_layout);
		$string_temp  = substr($row[0],$idx_layout);
		$idx_equal    = strpos($string_temp,"=");
		$idx_delimiter= strpos($string_temp,";");
		$harga_tiket  = substr($string_temp,$idx_equal+1,$idx_delimiter-$idx_equal);

		return $harga_tiket;
	}

	function periksaHakAkses($tgl,$jam_berangkat){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;
		global $TOLERANSI_KEBERANGKATAN;

		$valid	=	false;

		$sql =
			"SELECT TIMEDIFF('".$tgl." ".$jam_berangkat.":00',NOW()) AS selisih";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);

			$temp_selisih	= explode(':',$row[0]);

			$jam	= $temp_selisih[0];
			$menit= $temp_selisih[1];

			$selisih	= ($jam>=0)?0:abs($jam*60) + $menit; //dalam menit

			$valid	= ($selisih<=$TOLERANSI_KEBERANGKATAN)?true:false;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return $valid;
	}//  END periksaHakAkses

	function ambilDataKursi($no_tiket){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT 
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,
				IF(JenisPenumpang!='R',IF(JenisPembayaran=3,0,HargaTiket),SubTotal) AS HargaTiket,
				Charge,SubTotal,
				IF(JenisPembayaran=3,0,Discount) AS Discount,
				IF(JenisPembayaran=3,0,Total) AS Total,
				PetugasPenjual,
				CetakTiket,
				PetugasCetakTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,
				WaktuCetakTiket,
				WaktuMutasi,Pemutasi,JenisPenumpang,IdJurusan,
				JamBerangkat,MutasiDari
			FROM tbl_reservasi
			WHERE NoTiket='$no_tiket' AND FlagBatal!=1";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		if($db->sql_numrows($result)<=0){
			$sql =
				"SELECT 
					NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,
				IF(JenisPenumpang!='R',IF(JenisPembayaran=3,0,HargaTiket),SubTotal) AS HargaTiket,
				Charge,SubTotal,
				IF(JenisPembayaran=3,0,Discount) AS Discount,
				IF(JenisPembayaran=3,0,Total) AS Total,
				PetugasPenjual,
				CetakTiket,
				PetugasCetakTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,
				WaktuCetakTiket,
				WaktuMutasi,Pemutasi,JenisPenumpang,IdJurusan,
				JamBerangkat,MutasiDari
				FROM tbl_reservasi
				WHERE NoTiket='$no_tiket' AND FlagBatal!=1";

			if ($result = $db->sql_query($sql)){
				$row=$db->sql_fetchrow($result);
			}
			else{
				die_error("Err: $this->ID_FILE".__LINE__);
			}
		}

		return $row;

	}//  END ambilDataKursi

	function ambilDataKursiMulti($list_no_tiket){

		/*
        Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
        */

		//kamus
		global $db;

		$sql =
			"SELECT
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,
				IF(JenisPenumpang!='R',IF(JenisPembayaran=3,0,HargaTiket),SubTotal) AS HargaTiket,
				Charge,SubTotal,
				IF(JenisPembayaran=3,0,Discount) AS Discount,
				IF(JenisPembayaran=3,0,Total) AS Total,
				PetugasPenjual,
				CetakTiket,
				PetugasCetakTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,
				WaktuCetakTiket,
				WaktuMutasi,Pemutasi,JenisPenumpang,IdJurusan,
				JamBerangkat,MutasiDari,HargaTiketux
			FROM tbl_reservasi
			WHERE NoTiket IN($list_no_tiket) AND FlagBatal!=1
			ORDER BY JamBerangkat";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}


		return $result;

	}//  END ambilDataKursiMulti

	function ambilDataKursiByKodeBooking($kode_booking){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT 
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,HargaTiket,Charge,SubTotal,
				Discount,Total,PetugasPenjual,
				f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,CetakTiket,
				PetugasCetakTiket,f_user_get_nama_by_userid(PetugasCetakTiket) AS NamaCSOTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,JamBerangkat,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS NamaAsal,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS NamaTujuan
			FROM tbl_reservasi
			WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		if($db->sql_numrows($result)<=0){
			$sql =
				"SELECT 
					NoTiket,KodeJadwal,KodeBooking,
					IdMember,Nama,Alamat,
					Telp,HP,WaktuPesan,
					NomorKursi,HargaTiket,Charge,SubTotal,
					Discount,Total,PetugasPenjual,
					f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,CetakTiket,
					PetugasCetakTiket,f_user_get_nama_by_userid(PetugasCetakTiket) AS NamaCSOTiket,JenisDiscount,
					JenisPembayaran,PaymentCode,TglBerangkat,JamBerangkat,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS NamaAsal,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS NamaTujuan
				FROM tbl_reservasi
				WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
		}

		return $result;

	}//  END ambilDataKursi

	function ambilDataKursiByNoTiket($no_tiket){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT 
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,HargaTiket,Charge,SubTotal,
				Discount,Total,PetugasPenjual,
				f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,CetakTiket,
				PetugasCetakTiket,f_user_get_nama_by_userid(PetugasCetakTiket) AS NamaCSOTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,JamBerangkat,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS NamaAsal,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS NamaTujuan
			FROM tbl_reservasi
			WHERE NoTiket='$no_tiket' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		if($db->sql_numrows($result)<=0){
			$sql =
				"SELECT 
					NoTiket,KodeJadwal,KodeBooking,
					IdMember,Nama,Alamat,
					Telp,HP,WaktuPesan,
					NomorKursi,HargaTiket,Charge,SubTotal,
					Discount,Total,PetugasPenjual,
					f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,CetakTiket,
					PetugasCetakTiket,f_user_get_nama_by_userid(PetugasCetakTiket) AS NamaCSOTiket,JenisDiscount,
					JenisPembayaran,PaymentCode,TglBerangkat,JamBerangkat,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS NamaAsal,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS NamaTujuan
				FROM tbl_reservasi
				WHERE NoTiket='$no_tiket' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
		}

		$row=$db->sql_fetchrow($result);

		return $row;

	}//  END ambilDataKursiByNoTiket

	function ambilTotalPesananByKodeBooking($kode_booking){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT
				SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=3,0,HargaTiket),SubTotal)) AS HargaTiket,
		    SUM(SubTotal) AS SubTotal, 
		    COUNT(NoTiket) AS JumlahKursi, 
		    SUM(IF(JenisPembayaran=3,0,Discount)) AS TotalDiscount, 
		    SUM(IF(JenisPembayaran=3,0,Total)) AS TotalBayar
		  FROM tbl_reservasi
		  WHERE (KodeBooking LIKE '$kode_booking') AND FlagBatal!=1";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		if($row['SubTotal']==''){
			$sql =
				"SELECT 
			    SUM(SubTotal) AS SubTotal, 
			    COUNT(NoTiket) AS JumlahKursi, 
			    SUM(Discount) AS TotalDiscount, 
			    SUM(Total) AS TotalBayar
			  FROM tbl_reservasi
			  WHERE (KodeBooking LIKE '$kode_booking') AND FlagBatal!=1";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}

			$row=$db->sql_fetchrow($result);
		}



		return $row;

	}//  END ambilTotalPesananByKodeBooking

	function ambilTotalPesananDibayarByKodeBooking($kode_booking){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT
				SUM(IF(JenisPenumpang!='R',IF(JenisPembayaran=3,0,HargaTiket),SubTotal)) AS HargaTiket,
		    SUM(SubTotal) AS SubTotal, 
		    COUNT(NoTiket) AS JumlahKursi, 
		    SUM(IF(JenisPembayaran=3,0,Discount)) AS TotalDiscount, 
		    SUM(IF(JenisPembayaran=3,0,Total)) AS TotalBayar
		  FROM tbl_reservasi
		  WHERE (KodeBooking LIKE '$kode_booking') AND FlagBatal!=1 AND CetakTiket=1";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		if($row['SubTotal']==''){
			$sql =
				"SELECT 
			    SUM(SubTotal) AS SubTotal, 
			    COUNT(NoTiket) AS JumlahKursi, 
			    SUM(Discount) AS TotalDiscount, 
			    SUM(Total) AS TotalBayar
			  FROM tbl_reservasi
			  WHERE (KodeBooking LIKE '$kode_booking') AND FlagBatal!=1 AND CetakTiket=1";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}

			$row=$db->sql_fetchrow($result);
		}



		return $row;

	}//  END ambilTotalPesananDibayarByKodeBooking

	function ambilDiscount($id){

		/*
		Desc	:Mengembalikan besar discount
		*/

		//kamus
		global $db;

		$sql =
			"SELECT IF(JumlahDiscount IS NOT NULL,JumlahDiscount,0) AS JumlahDiscount,NamaDiscount ,KodeDiscount,IsHargaTetap
			FROM tbl_jenis_discount 
			WHERE IdDiscount=$id";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);

			return $row;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilJumlahDiscount

	function ambilPesanUntukDiTiket(){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT PesanDiTiket
			FROM tbl_pengaturan_umum LIMIT 0,1";

		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);

			$pesan	=$row[0];

			return $pesan;
		}
		else{
			//die_error("Err: $this->ID_FILE".__LINE__);
			return "-E-";
		}

	}//  END ambilPesanUntukDiTiket

	function ambilDataPerusahaan(){

		/*
		Desc	:Mengembalikan data perusahaan
		*/

		//kamus
		global $db;

		$sql =
			"SELECT NamaPerusahaan,AlamatPerusahaan,TelpPerusahaan,EmailPerusahaan,WebSitePerusahaan
			FROM tbl_pengaturan_umum LIMIT 0,1";

		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);

			return $row;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilDataPerusahaan

	function ambilLayoutKursiByKodeJadwal($kode_jadwal){

		/*
		Desc	:Mengembalikan besar discount
		*/

		//kamus
		global $db;

		$sql =
			"SELECT JumlahKursi
				FROM tbl_md_jadwal
				WHERE (KodeJadwal LIKE '$kode_jadwal')";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);

			return $row[0];
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilLayoutKursiByKodeJadwal


	//========UNTUK SPJ=======================================================================
	function ambilDataPosisiUntukSPJ($tgl_berangkat,$kode_jadwal){

		/*
		Desc	:Mengembalikan data penumpang
		*/

		//kamus
		global $db;

		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi":"tbl_posisi";

		$sql =
			"SELECT *
			FROM $nama_tbl
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND KodeJadwal='$kode_jadwal'";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);

			return $row;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilDataPosisiUntukSPJ

	function ambilDataPenumpangUntukSPJ($tgl_berangkat,$kode_jadwal,$list_kode_jadwal){

		/*
		Desc	:Mengembalikan data penumpang
		*/

		//kamus
		global $db;

		$sql =
			"SELECT 
				NoTiket,Nama,Alamat,
				Telp,HP,NomorKursi,
				f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS Asal,
				f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) AS Tujuan,
				JenisPenumpang,PetugasCetakTiket
			FROM tbl_reservasi
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND KodeJadwal IN($list_kode_jadwal)
				/*AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)=f_jurusan_get_kode_cabang_asal_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'))*/
				AND FlagBatal!=1
				AND CetakTiket=1
			ORDER BY NomorKursi,WaktuPesan";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return $result;

	}//  END ambilDataPenumpangUntukSPJ

	function ambilDataPaketUntukSPJ($tgl_berangkat,$kode_jadwal){

		/*
		Desc	:Mengembalikan data penumpang
		*/

		//kamus
		global $db;

		$sql =
			"SELECT
				NoTiket,NamaPengirim,NamaPenerima,
				TelpPengirim,TelpPenerima,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan
			FROM tbl_paket
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND KodeJadwal IN($kode_jadwal)
				AND FlagBatal!=1
			ORDER BY WaktuPesan";

		if ($result = $db->sql_query($sql)){
			return $result;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilDataPaketUntukSPJ

	function hitungTotalOmzetdanJumlahPenumpangSPJ($tgl_berangkat,$kode_jadwal,$list_kode_jadwal){

		/*
		Desc	:Mengembalikan data penumpang
		*/

		//kamus
		global $db;

		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$nama_tbl	= $tanggal_lampau==1?"tbl_reservasi":"tbl_reservasi";

		$sql =
			"SELECT 
				COUNT(NoTiket) AS JumlahPenumpang,
				SUM(Total) AS TotalOmzet
			FROM $nama_tbl
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND KodeJadwal IN($list_kode_jadwal)
				AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)=f_jurusan_get_kode_cabang_asal_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'))
				AND FlagBatal!= 1 
				AND CetakTiket=1";

		if(!$result = $db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		return $row;

	}//  END ambilDataPenumpangUntukSPJ

	function hitungTotalOmzetdanJumlahPaketSPJ($tgl_berangkat,$kode_jadwal){

		/*
		Desc	:Mengembalikan data penumpang
		*/

		//kamus
		global $db;

		$sql =
			"SELECT 
				COUNT(NoTiket) AS JumlahPaket,
				SUM(HargaPaket) AS TotalOmzet
			FROM tbl_paket
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND KodeJadwal IN($kode_jadwal)
				AND FlagBatal != 1 
				AND CetakTiket=1";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);

			return $row;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END hitungTotalOmzetdanJumlahPaketPerSPJ

	function tambahSPJ(
		$no_spj, $kode_jadwal, $tgl_berangkat,
		$jam_berangkat, $layout_kursi, $jumlah_penumpang,
		$mobil_dipilih, $cso, $sopir_dipilih,
		$nama_sopir,$total_omzet, $jumlah_paket,
		$total_omzet_paket,$is_sub_jadwal, $is_cetak_voucher_bbm){

		/*
		IS	: data spj belum ada dalam database
		FS	:Data lspj baru telah disimpan dalam database 
		*/

		//kamus
		global $db;

		//MENAMBAHKAN DATA KEDALAM DATABASE

		$sql=
			"INSERT INTO tbl_spj
				(NoSPJ,KodeJadwal,TglBerangkat,JamBerangkat,
				JumlahKursiDisediakan,JumlahPenumpang,TglSPJ,
				NoPolisi,CSO,KodeDriver,
				Driver,TotalOmzet,IdJurusan,
				FlagAmbilBiayaOP, JumlahPaket,TotalOmzetPaket,
				IsEkspedisi,IsSubJadwal,IsCetakVoucherBBM)
			VALUES(
				'$no_spj','$kode_jadwal','$tgl_berangkat', '$jam_berangkat',
				'$layout_kursi','$jumlah_penumpang',NOW(),
				'$mobil_dipilih','$cso','$sopir_dipilih',
				'$nama_sopir','$total_omzet',f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'),
				0,'$jumlah_paket','$total_omzet_paket',
				0,'$is_sub_jadwal','$is_cetak_voucher_bbm');
		";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return true;
	}

	function ubahSPJ(
		$no_spj, $jumlah_penumpang,
		$mobil_dipilih, $cso, $sopir_dipilih,
		$nama_sopir,$total_omzet,
		$jumlah_paket,$total_omzet_paket,$is_sub_jadwal){

		/*
		IS	: data spj sudah ada dalam database
		FS	:Data spj  telah diubah  dalam database 
		*/

		//kamus
		global $db;

		$kondisi_sub_jadwal	= $is_sub_jadwal!=1?"NoPolisi='$mobil_dipilih',KodeDriver='$sopir_dipilih',Driver='$nama_sopir',":"";

		//MENGUBAH DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_spj SET
				JumlahPenumpang='$jumlah_penumpang',
				$kondisi_sub_jadwal
				TotalOmzet='$total_omzet',
				JumlahPaket='$jumlah_paket',
				TotalOmzetPaket='$total_omzet_paket'
			WHERE NoSPJ='$no_spj';";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		return true;
	}

	function ubahPosisiCetakSPJ(
		$kode_jadwal, $tgl_berangkat,$list_field_diupdate,
		$sopir_dipilih,$mobil_dipilih,$no_spj,$cso){

		/*
		IS	: data layout posisi sudah ada dalam database
		FS	:Data layout posisi  telah diubah dalam database 
		*/

		//kamus
		global $db;

		//MENGUBAH DATA KEDALAM DATABASE
		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi":"tbl_posisi";

		$sql =
			"UPDATE $nama_tbl
				SET 
					$list_field_diupdate
					KodeSopir='$sopir_dipilih',
					KodeKendaraan='$mobil_dipilih',
					NoSPJ='$no_spj'
				WHERE 
					TglBerangkat 		= '$tgl_berangkat' 
					AND KodeJadwal	= '$kode_jadwal';";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		return true;
	}

	function ubahDataReservasiCetakSPJ(
		$list_kode_jadwal, $tgl_berangkat,$sopir_dipilih,
		$mobil_dipilih,$no_spj){

		/*
		IS	: data layout posisi sudah ada dalam database
		FS	:Data layout posisi  telah diubah dalam database 
		*/

		//kamus
		global $db;

		$kode_jadwal	= explode(",",$list_kode_jadwal);

		for($idx=0;$idx<count($kode_jadwal);$idx++){
			//MENGUBAH DATA KEDALAM DATABASE
//			$sql =
//				"CALL sp_reservasi_ubah_data_after_spj(
//					$kode_jadwal[$idx], '$tgl_berangkat', '$sopir_dipilih',
//					'$mobil_dipilih','$no_spj');";
			$sql = "UPDATE tbl_reservasi
					  SET
						KodeSopir='$sopir_dipilih',
						KodeKendaraan='$mobil_dipilih',
						NoSPJ='$no_spj',
						TglCetakSPJ=NOW(),
						CetakSPJ=1
					  WHERE
						TglBerangkat = '$tgl_berangkat'
						AND KodeJadwal=$kode_jadwal[$idx];";
			if (!$db->sql_query($sql)){
				die_error("Err $this->ID_FILE".__LINE__);
			}

			if($db->sql_affectedrows() <= 0){
				$sql = "UPDATE tbl_reservasi_olap
					  SET
						KodeSopir='$sopir_dipilih',
						KodeKendaraan='$mobil_dipilih',
						NoSPJ='$no_spj',
						TglCetakSPJ=NOW(),
						CetakSPJ=1
					  WHERE
						TglBerangkat = '$tgl_berangkat'
						AND KodeJadwal=$kode_jadwal[$idx];";
				if (!$db->sql_query($sql)){
					die_error("Err $this->ID_FILE".__LINE__);
				}
			}

			$sql = "UPDATE tbl_paket
					  SET
						KodeSopir='$sopir_dipilih',
						KodeKendaraan='$mobil_dipilih',
						NoSPJ='$no_spj',
						TglCetakSPJ=NOW(),
						CetakSPJ=1
					  WHERE
						TglBerangkat = '$tgl_berangkat'
						AND KodeJadwal=$kode_jadwal[$idx];";
			if (!$db->sql_query($sql)){
				die_error("Err $this->ID_FILE".__LINE__);
			}
		}

		return true;
	}

	function ambilDetailSPJ($tgl_berangkat,$kode_jadwal){

		/*
		Desc	:Mengembalikan data penumpang
		*/

		//kamus
		global $db;

		$sql =
			"SELECT *
			FROM tbl_spj
			WHERE 
				TglBerangkat = '$tgl_berangkat' 
				AND KodeJadwal='$kode_jadwal'";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return $db->sql_fetchrow($result);;

	}//  END ambilDataPosisiUntukSPJ

	function cariJadwalKeberangkatan($no_telp){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT DISTINCT
				TglBerangkat,JamBerangkat,KodeJadwal,KodeBooking,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS Asal,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan,
				Nama,Alamat,CetakTiket,CetakSPJ,TglCetakSPJ
			FROM tbl_reservasi
			WHERE Telp='$no_telp' AND FlagBatal!=1 ORDER BY TglBerangkat DESC,JamBerangkat DESC LIMIT 0,3";

		if ($result = $db->sql_query($sql)){
			return $result;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END cariJadwalKeberangkatan

	function adaPesananBelumDibayarSebelumnya($no_telp,$tgl_berangkat,$jam_berangkat){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$temp_tgl_berangkat	= FormatTglToMySQLDate($tgl_berangkat);

		$sql =
			"SELECT COUNT(1)
			FROM tbl_reservasi
			WHERE Telp='$no_telp' AND FlagBatal!=1 AND CetakTiket!=1 AND CONCAT(TglBerangkat,' ',JamBerangkat,':00')>=NOW()
				AND CONCAT(TglBerangkat,' ',JamBerangkat,':00')>=CONCAT('$tgl_berangkat',' ','$jam_berangkat',':00')
				AND CONCAT('$tgl_berangkat',' ','$jam_berangkat',':00')>=NOW()";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		if($row[0]>=1){
			return 1;
		}
		else{
			return 0;
		}

	}//  END adaPesananBelumDibayarSebelumnya

	function cariPaket($no_resi){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT DISTINCT
				TglBerangkat,JamBerangkat,KodeJadwal,NoTiket,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS Asal,
				f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan,
				NamaPengirim,AlamatPengirim,TelpPengirim,NamaPenerima,AlamatPenerima,TelpPenerima,
				f_kendaraan_ambil_nopol_by_kode(KodeKendaraan) AS NoPolisi,
				KodeKendaraan,f_sopir_get_nama_by_id(KodeSopir) AS NamaSopir,
				f_user_get_nama_by_userid(PetugasPemberi) AS NamaPetugasPemberi,
				NamaPengambil,StatusDiambil,NoKTPPengambil,WaktuPengambilan
			FROM tbl_paket
			WHERE (NoTiket LIKE '%$no_resi' OR TelpPengirim='$no_resi' OR TelpPenerima='$no_resi') 
				AND FlagBatal!=1 
			ORDER BY TglBerangkat DESC LIMIT 0,3";

		if ($result = $db->sql_query($sql)){
			return $result;
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END cariPaket

	function ambilNomorKursi($kode_booking){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT NomorKursi
			FROM tbl_reservasi
			WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		}
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		if($row['NomorKursi']==''){
			$sql =
				"SELECT NomorKursi
				FROM tbl_reservasi
				WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
		}

		return $result;
	}//  END cariJadwalKeberangkatan

	function mutasiPenumpang(
		$no_tiket, $kode_jadwal, $kode_jadwal_utama,
		$id_jurusan, $kode_cabang_asal, $kode_cabang_tujuan,
		$is_sub_jadwal, $kode_kendaraan, $kode_sopir,
		$tgl_berangkat, $jam_berangkat, $no_kursi,
		$harga_tiket, $sub_total, $total,
		$no_spj, $tgl_cetak_SPJ, $cetak_spj,
		$komisi_penumpang_CSO, $petugas_cetak_spj, $keterangan,
		$kode_akun_pendapatan, $kode_akun_komisi_penumpang_CSO, $payment_code,
		$nama_lama, $cetak_tiket_lama, $no_kursi_lama,
		$pemutasi){

		//kamus
		global $db;

		//cek status kursi tujuan

		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		if($tanggal_lampau==1){
			$tbl_posisi_detail	= "tbl_posisi_detail";
			$tbl_reservasi			= "tbl_reservasi";
		}
		else{
			$tbl_posisi_detail	= "tbl_posisi_detail";
			$tbl_reservasi			= "tbl_reservasi";
		}

		$sql	=
			"SELECT StatusKursi
			FROM $tbl_posisi_detail 
			WHERE 
				NomorKursi='$no_kursi'
				AND KodeJadwal='$kode_jadwal'
				AND TglBerangkat='$tgl_berangkat';";

		if (!$result = $db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		$data_kursi_tujuan = $db->sql_fetchrow($result);

		if($data_kursi_tujuan[0]==1){
			//jika kursi sudah di flag maka proses mutasi dibatalkan
			return false;
		}

		//mengambil kode booking
		$sql	=
			"SELECT
				KodeBooking, KodeJadwal, TglBerangkat,Discount
			FROM tbl_reservasi WHERE NoTiket='$no_tiket';";

		if (!$result = $db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		if($db->sql_numrows($result)<=0){
			$sql	=
				"SELECT
					KodeBooking, KodeJadwal, TglBerangkat,Discount
				FROM tbl_reservasi WHERE NoTiket='$no_tiket';";

			if (!$result = $db->sql_query($sql)){
				die_error("Err $this->ID_FILE".__LINE__);
			}
		}

		$data_tiket_lama = $db->sql_fetchrow($result);

		if($cetak_tiket_lama!=1){
			$discount	= $data_tiket_lama['Discount'];

			if($discount=='' || $discount==0){

				$Promo	= new Promo();

				$data_discount_point	= $Promo->ambilDiscountPoint($kode_jadwal,$tgl_berangkat);

				$discount		= ($data_discount_point['FlagDiscount']!=1)?$data_discount_point['JumlahDiscount']:($data_discount_point['JumlahDiscount']/100)*$harga_tiket;
				$point			= $data_discount_point['JumlahPoint'];
				$target_promo	= $data_discount_point['FlagTargetPromo'];

				if($data_tiket_lama['IdMember']!='' && $target_promo<=1){
					//do nothing
				}
				elseif($data_tiket_lama['IdMember']=='' && ($target_promo==2 || $target_promo==0)){
					//do nothing
				}
				else{
					$discount	= 0;
					$point		= 0;
				}

				$query_update_discount	= ",Discount='$discount'";
				$jenis_discount	= ($discount==0)?"":"PROMO";
			}

			$total	= $total-$discount;
			$query_ubah_harga	= ",HargaTiket='$harga_tiket',SubTotal='$sub_total',Total='$total' ".$query_update_discount;
		}

		$sql	=
			"UPDATE $tbl_reservasi SET
		    KodeJadwal='$kode_jadwal', IdJurusan='$id_jurusan',
		    KodeKendaraan='$kode_kendaraan', KodeSopir='$kode_sopir', TglBerangkat='$tgl_berangkat',
		    JamBerangkat='$jam_berangkat', NomorKursi='$no_kursi', NoSPJ='$no_spj',
		    TglCetakSPJ='$tgl_cetak_SPJ', CetakSPJ='$cetak_spj', KomisiPenumpangCSO='$komisi_penumpang_CSO',
		    PetugasCetakSPJ='$petugas_cetak_spj', Keterangan='$keterangan', JenisDiscount='$jenis_discount',
		    KodeAkunPendapatan='$kode_akun_pendapatan', KodeAkunKomisiPenumpangCSO='$kode_akun_komisi_penumpang_CSO',PaymentCode='$payment_code',
		    WaktuMutasi=NOW(),Pemutasi='$pemutasi',
		    KodeBooking=if('".$data_tiket_lama['KodeJadwal']."'='$kode_jadwal' AND '".$data_tiket_lama['TglBerangkat']."'='$tgl_berangkat',KodeBooking,CONCAT(KodeBooking,'M')),
				MutasiDari='$data_tiket_lama[KodeJadwal]'
				$query_ubah_harga
			WHERE NoTiket='$no_tiket';";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		//memeriksa kursi yang akan dibooking
		$sql  =
			"SELECT COUNT(NomorKursi) FROM $tbl_posisi_detail WHERE NomorKursi='$no_kursi' AND TglBerangkat='$tgl_berangkat' AND KodeJadwal='$kode_jadwal';";

		if (!$result=$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		$row = $db->sql_fetchrow($result);

		if($row[0]<=0){
			//jika data kursi belum ada, diinsert dulu
			$sql=
				"INSERT INTO $tbl_posisi_detail(
          NomorKursi,KodeJadwal,JamBerangkat,
          IsSubJadwal,KodeJadwalUtama,KodeCabangAsal,
          KodeCabangTujuan,TglBerangkat,StatusKursi,
          NoTiket,Nama,StatusBayar,
          KodeBooking)
         VALUES(
          '$no_kursi','$kode_jadwal','$jam_berangkat',
          '$is_sub_jadwal','$kode_jadwal_utama', '$kode_cabang_asal',
          '$kode_cabang_tujuan', '$tgl_berangkat','1',
          '$no_tiket','$nama_lama','$cetak_tiket_lama',
          if('".$data_tiket_lama['KodeJadwal']."'='$kode_jadwal' AND '".$data_tiket_lama['TglBerangkat']."'='$tgl_berangkat','".$data_tiket_lama['KodeBooking']."',CONCAT('".$data_tiket_lama['KodeBooking']."','M'))
          )";
		}
		else{
			#UPDATE Di Layout kursi yang baru
			$sql	=
				"UPDATE $tbl_posisi_detail SET
          StatusKursi=1,Nama='$nama_lama',NoTiket='$no_tiket',
          KodeBooking=if('".$data_tiket_lama['KodeJadwal']."'='$kode_jadwal' AND '".$data_tiket_lama['TglBerangkat']."'='$tgl_berangkat','".$data_tiket_lama['KodeBooking']."',CONCAT('".$data_tiket_lama['KodeBooking']."','M')),
          Session=NULL,StatusBayar='$cetak_tiket_lama'
        WHERE
          NomorKursi='$no_kursi'
          AND KodeJadwal='$kode_jadwal'
          AND TglBerangkat='$tgl_berangkat';";
		}

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		#MEMBERSIHKAN layout kursi yang lama

		$temp_array_date	= explode("-",$data_tiket_lama['TglBerangkat']);
		$temp_tgl_berangkat	= mktime(0, 0, 0, (int) $temp_array_date[1] , (int) $temp_array_date[2]+$this->WAKTU_BACKUP, (int) $temp_array_date[0]);

		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');

		$tbl_posisi_detail	= $tanggal_lampau==1?"tbl_posisi_detail":"tbl_posisi_detail";

		$sql	=
			"UPDATE $tbl_posisi_detail SET
				StatusKursi=0,Nama=NULL,NoTiket=NULL,
				KodeBooking=NULL,Session=NULL,StatusBayar=0
			WHERE
				NomorKursi='$no_kursi_lama'
				AND KodeJadwal='".$data_tiket_lama['KodeJadwal']."'
				AND TglBerangkat='".$data_tiket_lama['TglBerangkat']."';";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		//UPDATE KE LOG MUTASI
		$this->tambahLogMutasi(
			$no_tiket,$data_tiket_lama['KodeBooking'],$data_tiket_lama['TglBerangkat'],
			$data_tiket_lama['JamBerangkat'],$data_tiket_lama['KodeJadwal'],$data_tiket_lama['IdJurusan'],
			$no_kursi_lama,$data_tiket_lama['HargaTiket'],$userdata['user_id'],$userdata['nama']);

		return true;
	}

	function updateInsentifSopir(
		$no_spj,$layout_kursi, $layout_maksimum, $minimum_penumpang, $besar_insentif_sopir){

		/*
		IS	: data spj sudah ada dalam database
		FS	:Data spj  telah diubah  dalam database 
		*/

		//kamus
		global $db;

		//Mengambil jumlah penumpang pada no spj yang bersangkutan
		$sql =
			"SELECT COUNT(NoTiket) AS JumlahPenumpang 
			FROM tbl_reservasi
			WHERE NoSPJ='$no_spj' AND CetakTiket=1 AND Total>0;";

		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);
			$jumlah_penumpang	= $row['JumlahPenumpang'];
		}
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}

		if($layout_kursi<=$layout_maksimum){
			if($jumlah_penumpang>$minimum_penumpang){
				$jumlah_penumpang_dapat_insentif	= $jumlah_penumpang-$minimum_penumpang;

				$jumlah_insentif	= $jumlah_penumpang_dapat_insentif * $besar_insentif_sopir;

				$sql = "CALL sp_spj_ubah_insentif_sopir('$no_spj','$jumlah_insentif');";

				if (!$db->sql_query($sql)){
					die_error("Err $this->ID_FILE".__LINE__);
				}

			}
		}

		return true;
	}

	function updateFrekwensiMember($id_member,$tgl_berangkat){

		/*
		IS	: data member sudah ada dalam sistem
		FS	: frekwensi berangkat member bersangkutan bertambah 1 
		*/

		//kamus
		global $db;

		//MENGUBAH DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_member_frekwensi_berangkat
				SET 
					FrekwensiBerangkat=FrekwensiBerangkat+1
				WHERE 
					LEFT(TglBerangkat,7) = LEFT('$tgl_berangkat',7) 
					AND IdMember	= '$id_member';";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		if($db->sql_affectedrows()==0){
			//MENAMBAHKAN DATA KEDALAM DATABASE
			$sql =
				"INSERT INTO tbl_member_frekwensi_berangkat (IdMember,TglBerangkat,FrekwensiBerangkat)
					VALUES 
						('$id_member','$tgl_berangkat',1)";

			if (!$db->sql_query($sql)){
				die_error("Err $this->ID_FILE".__LINE__);
			}
		}

		return true;
	}

	function ambilDataKursiByKodeBooking4Tiket($kode_booking){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT 
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,HargaTiket,Charge,SubTotal,
				Discount,Total, PetugasPenjual,CetakTiket,
				PetugasCetakTiket,JenisDiscount,JenisPembayaran,
				PaymentCode,TglBerangkat,JamBerangkat,
				IdJurusan,JenisPenumpang
			FROM tbl_reservasi
			WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 AND CetakTiket!=1 ORDER BY NomorKursi ASC";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		if($db->sql_numrows($result)<=0){
			$sql =
				"SELECT 
					NoTiket,KodeJadwal,KodeBooking,
					IdMember,Nama,Alamat,
					Telp,HP,WaktuPesan,
					NomorKursi,HargaTiket,Charge,SubTotal,
					Discount,Total, PetugasPenjual,CetakTiket,
					PetugasCetakTiket,JenisDiscount,JenisPembayaran,
					PaymentCode,TglBerangkat,JamBerangkat,
					IdJurusan,JenisPenumpang
				FROM tbl_reservasi
				WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
		}

		return $result;

	}//  END ambilDataKursiByKodeBooking4Tiket

	function ambilDataKursiByNoTiket4Tiket($no_tiket){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT 
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,HargaTiket,Charge,SubTotal,
				Discount,Total,PetugasPenjual,
				CetakTiket,PetugasCetakTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,
				JamBerangkat,IdJurusan,WaktuCetakTiket,
				JenisPenumpang
			FROM tbl_reservasi
			WHERE NoTiket='$no_tiket' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		if($db->sql_numrows($result)<=0){
			$sql=
				"SELECT
				NoTiket,KodeJadwal,KodeBooking,
				IdMember,Nama,Alamat,
				Telp,HP,WaktuPesan,
				NomorKursi,HargaTiket,Charge,SubTotal,
				Discount,Total,PetugasPenjual,
				CetakTiket,PetugasCetakTiket,JenisDiscount,
				JenisPembayaran,PaymentCode,TglBerangkat,
				JamBerangkat,IdJurusan,JenisPenumpang
			FROM tbl_reservasi
			WHERE NoTiket='$no_tiket' AND FlagBatal!=1 ORDER BY NomorKursi ASC";

			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
		}

		return $result;

	}//  END ambilDataKursiByNoTiket4Tiket

	function tambahLogMutasi(
		$NoTiket,$KodeBookingSebelumnya,$TglBerangkatSebelumnya,
		$JamBerangkatSebelumnya,$KodeJadwalSebelumnya,$IdJurusanSebelumnya,
		$NomorKursiSebelumnya,$HargaTiketSebelumnya,$UserMutasi,
		$NamaUserMutasi){

		//kamus
		global $db;

		//cek status kursi tujuan

		//mengambil Data Sebelumnya
		$data_kursi	= $this->ambilDataKursi($NoTiket);

		if($data_kursi['CetakTiket']!=1 || ($TglBerangkatSebelumnya==$data_kursi['TglBerangkat'] && $KodeJadwalSebelumnya==$data_kursi['KodeJadwal'])){
			return true;
		}

		$sql	=
			"INSERT INTO tbl_log_mutasi(
				NoTiket,NamaPenumpang,TelpPenumpang,
				KodeBookingSebelumnya,TglBerangkatSebelumnya,JamBerangkatSebelumnya,
				KodeJadwalSebelumnya,IdJurusanSebelumnya,NomorKursiSebelumnya,
				HargaTiketSebelumnya,KodeBookingMutasi,TglBerangkatMutasi,
				JamBerangkatMutasi,KodeJadwalMutasi,NomorKursiMutasi,
				IdJurusanMutasi,HargaTiketMutasi,IsCetakTiket,
				IsCetakSPJ,Charge,TotalDiskon,NoSPJ,
				WaktuCetakSPJ,UserMutasi,NamaUserMutasi,
				WaktuMutasi)
			VALUES(
				'$NoTiket','$data_kursi[Nama]','$data_kursi[Telp]',
				'$KodeBookingSebelumnya','$TglBerangkatSebelumnya','$JamBerangkatSebelumnya',
				'$KodeJadwalSebelumnya','$IdJurusanSebelumnya','$NomorKursiSebelumnya',
				'$HargaTiketSebelumnya','$data_kursi[KodeBooking]','$data_kursi[TglBerangkat]',
				'$data_kursi[JamBerangkat]','$data_kursi[KodeJadwal]','$data_kursi[NomorKursi]',
				'$data_kursi[IdJurusan]','$data_kursi[HargaTiket]','$data_kursi[CetakTiket]',
				'$data_kursi[CetakSPJ]','0','$data_kursi[Diskon]','$data_kursi[NoSPJ]',
				'$data_kursi[TglCetakSPJ]','$UserMutasi','$NamaUserMutasi',
				NOW());";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		return true;
	}

	function tambahLogCetakTiket(
		$NoTiket,$TglBerangkat,$JamBerangkat,
		$KodeJadwal,$IdJurusan,$NomorKursi,
		$UserPencetak,$NamaUserPencetak,$UserOtorisasi,
		$NamaUserOtorisasi){

		//kamus
		global $db;

		//mengambil Data Kursi
		$data_kursi	= $this->ambilDataKursi($NoTiket);

		//menghitung cetakan tiket
		$sql	=
			"SELECT IS_NULL(COUNT(1),0) FROM tbl_log_cetak_tiket WHERE NoTiket='$NoTiket'";

		if (!$result=$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		$row	= $db->sql_fetchrow($result);

		$CetakanKe=$row[0]+1;

		$sql	=
			"INSERT INTO tbl_log_cetak_tiket(
				NoTiket,NamaPenumpang,TelpPenumpang,
				TglBerangkat,JamBerangkat,KodeJadwal,
				IdJurusan,NomorKursi,CetakanKe,
				IsCetakSPJ,NoSPJ,WaktuCetakSPJ,
				UserPencetak,NamaUserPencetak,WaktuCetak,
				UserOtorisasi,NamaUserOtorisasi
			)
			VALUES(
				'$NoTiket','$data_kursi[Nama]','$data_kursi[Telp]',
				'$TglBerangkat','$JamBerangkat','$KodeJadwal',
				'$IdJurusan','$NomorKursi','$CetakanKe',
				'$data_kursi[CetakSPJ]','$data_kursi[NoSPJ]','$data_kursi[TglCetakSPJ]',
				'$UserPencetak','$NamaUserPencetak',NOW(),
				'$UserOtorisasi','$NamaUserOtorisasi');";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		return true;
	}

	function tambahLogCetakManifest(
		$NoSPJ,$KodeJadwal,$TglBerangkat,
		$JamBerangkat,$JumlahKursiDisediakan,$JumlahPenumpang,
		$JumlahPaket,$NoPolisi,$KodeDriver,
		$Driver,$TotalOmzet,$TotalOmzetPaket,
		$IsSubJadwal,$UserCetak,$NamaUserPencetak){

		//kamus
		global $db;

		//menghitung cetakan tiket
		$sql	=
			"SELECT IS_NULL(COUNT(1),0) FROM tbl_log_cetak_manifest WHERE NoSPJ='$NoSPJ'";

		if (!$result=$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		$row	= $db->sql_fetchrow($result);

		$CetakanKe=$row[0]+1;

		$sql	=
			"INSERT INTO tbl_log_cetak_manifest(
				NoSPJ,KodeJadwal,TglBerangkat,
				JamBerangkat,JumlahKursiDisediakan,JumlahPenumpang,
				JumlahPaket,NoPolisi,KodeDriver,
				Driver,TotalOmzet,TotalOmzetPaket,
				IsSubJadwal,WaktuCetak,UserCetak,
				NamaUserPencetak,CetakanKe
			)
			VALUES(
				'$NoSPJ','$KodeJadwal','$TglBerangkat',
				'$JamBerangkat','$JumlahKursiDisediakan','$JumlahPenumpang',
				'$JumlahPaket','$NoPolisi','$KodeDriver',
				'$Driver','$TotalOmzet','$TotalOmzetPaket',
				'$IsSubJadwal',NOW(),'$UserCetak',
				'$NamaUserPencetak','$CetakanKe');";

		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}

		return true;
	}

}
?>