-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2016 at 09:31 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tiketux_hai`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_biaya_op_tambah`(
  p_no_spj VARCHAR(20), p_kode_akun VARCHAR(20), p_flag_jenis_biaya INT(1),
  p_kode_kendaraan VARCHAR(20),p_kode_sopir VARCHAR(50),p_jumlah DOUBLE,
  p_id_petugas INTEGER,p_kode_jadwal VARCHAR(30),p_kode_cabang VARCHAR(50))
BEGIN

   INSERT INTO tbl_biaya_op (
     NoSPJ, KodeAkun, FlagJenisBiaya,
     TglTransaksi, NoPolisi, KodeSopir,
     Jumlah, IdPetugas, IdJurusan,
     KodeCabang)
   VALUES (
     p_no_spj, p_kode_akun, p_flag_jenis_biaya,
     NOW(),p_kode_kendaraan,p_kode_sopir,
     p_jumlah, p_id_petugas, f_jadwal_ambil_id_jurusan_by_kode_jadwal(p_kode_jadwal),
     p_kode_cabang);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_hapus`(p_list_kode VARCHAR(255))
BEGIN

  DELETE FROM tbl_md_cabang
  WHERE KodeCabang IN(p_list_kode);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_tambah`(p_kode VARCHAR(20), p_nama VARCHAR(70),
  p_alamat VARCHAR(255),p_kota VARCHAR(20),
  p_telp VARCHAR(50),p_fax VARCHAR(50),
  p_flag_agen INT(1))
BEGIN

   INSERT INTO tbl_md_cabang (KodeCabang,Nama, Alamat,Kota,Telp,Fax,FlagAgen)
   VALUES (p_kode,p_nama,p_alamat,p_kota,p_telp,p_fax,p_flag_agen);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_ubah`(
  p_kode_old VARCHAR(20),
  p_kode VARCHAR(20), p_nama VARCHAR(70),
  p_alamat VARCHAR(255),p_kota VARCHAR(20),
  p_telp VARCHAR(50),p_fax VARCHAR(50),

  p_flag_agen INT(1))
BEGIN

   UPDATE tbl_md_cabang
   SET
     KodeCabang=p_kode, Nama=p_nama, Alamat=p_alamat, Kota=p_kota, Telp=p_telp,
     Fax=p_fax,FlagAgen=p_flag_agen
   WHERE KodeCabang=p_kode_old;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deposit_debit`(
-- ----------------------------
  p_kode_booking VARCHAR(30),
  p_jumlah DOUBLE,
  p_keterangan TEXT)
BEGIN

  DECLARE saldo_deposit DOUBLE;
  DECLARE p_komisi DOUBLE;

  START TRANSACTION;

    #Mengambil nilai saldo terakhir
    SELECT NilaiParameter INTO saldo_deposit FROM tbl_pengaturan_parameter WHERE NamaParameter ='DEPOSIT_SALDO';

    #Mengambil nilai komisi
    SELECT SUM(Komisi) INTO p_komisi
    FROM tbl_reservasi
    WHERE KodeBooking=p_kode_booking;

    IF saldo_deposit>=p_jumlah-p_komisi THEN
      #jika saldo masih cukup, lakukan proses berikut

      #mengurangi saldo di tabel pengaturan
      UPDATE tbl_pengaturan_parameter SET NilaiParameter=NilaiParameter-(p_jumlah-p_komisi) WHERE NamaParameter='DEPOSIT_SALDO';

      #mengisi transaksi ke tabel log transaksi
      INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
        VALUES(NOW(),1,p_keterangan,p_jumlah-p_komisi,saldo_deposit-(p_jumlah-p_komisi));

      #mengubah flag isSettlement di tabel reservasi menjadi 0 (karena dipotong dari deposit)
      UPDATE tbl_reservasi SET IsSettlement=0 WHERE KodeBooking=p_kode_booking;

    END IF;
    #Jika saldo tidak cukup, maka tidak akan dilakukan proses apapun

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deposit_topup`(p_kode_referensi VARCHAR(25))
BEGIN


  DECLARE jumlah_topup DOUBLE;
  DECLARE saldo_terakhir DOUBLE;

  START TRANSACTION;
  SELECT Jumlah INTO jumlah_topup FROM tbl_deposit_log_topup WHERE KodeReferensi=p_kode_referensi;

  UPDATE tbl_pengaturan_parameter SET NilaiParameter=NilaiParameter+jumlah_topup WHERE NamaParameter='DEPOSIT_SALDO';

  SELECT NilaiParameter INTO saldo_terakhir FROM tbl_pengaturan_parameter WHERE NamaParameter='DEPOSIT_SALDO';

  INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
    VALUES (NOW(),0,CONCAT('TOP UP DEPOSIT ',p_kode_referensi),jumlah_topup,saldo_terakhir);


  COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_tambah`(
  p_kode_jadwal VARCHAR(30), p_id_jurusan INTEGER,
  p_jam VARCHAR(5),p_jumlah_kursi INT(2),
  p_flag_sub_jadwal INT(1),p_kode_jadwal_utama VARCHAR(30),
  p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_md_jadwal (
    KodeJadwal,IdJurusan,JamBerangkat,
    JumlahKursi,FlagSubJadwal,KodeJadwalUtama,
    FlagAktif)
  VALUES(
    p_kode_jadwal,p_id_jurusan,p_jam,
    p_jumlah_kursi,p_flag_sub_jadwal,p_kode_jadwal_utama,
    p_flag_aktif);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah`(
  p_kode_jadwal_old VARCHAR(30),
  p_kode_jadwal VARCHAR(30), p_id_jurusan INTEGER,
  p_jam VARCHAR(5),p_jumlah_kursi INT(2),
  p_flag_sub_jadwal INT(1),p_kode_jadwal_utama VARCHAR(30),
  p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_md_jadwal SET
    KodeJadwal=p_kode_jadwal,IdJurusan=p_id_jurusan,
    JamBerangkat=p_jam,JumlahKursi=p_jumlah_kursi,
    FlagSubJadwal=p_flag_sub_jadwal,KodeJadwalUtama=p_kode_jadwal_utama,
    FlagAktif=p_flag_aktif
  WHERE KodeJadwal = p_kode_jadwal_old;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah_kode_jadwal`(kode_jadwal_lama VARCHAR(30),kode_jadwal_baru VARCHAR(30))
BEGIN
  UPDATE tbl_posisi SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
  UPDATE tbl_posisi_backup SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_posisi_detail SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
  UPDATE tbl_posisi_detail_backup SET KodeJadwal=kode_jadwal_baru  WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_reservasi SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
  UPDATE tbl_reservasi_olap SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_spj SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_penjadwalan_kendaraan SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah_status_aktif`(p_kode_jadwal VARCHAR(30))
BEGIN

   UPDATE tbl_md_jadwal SET
     FlagAktif=1-FlagAktif
   WHERE KodeJadwal=p_kode_jadwal;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jenis_discount_ubah_status_aktif`(p_id_discount INTEGER)
BEGIN

   UPDATE tbl_jenis_discount SET
     FlagAktif=1-FlagAktif
   WHERE IdDiscount=p_id_discount;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_tambah`(
  p_kode_jurusan VARCHAR(10), p_kode_cabang_asal VARCHAR(20), p_kode_cabang_tujuan VARCHAR(20),
  p_harga_tiket DOUBLE,p_harga_tiket_tuslah DOUBLE,
  p_flag_tiket_tuslah INT(1),
  p_kode_akun_pendapatan_penumpang VARCHAR(20),
  p_kode_akun_pendapatan_paket VARCHAR(20),
  p_kode_akun_charge VARCHAR(20),
  p_kode_akun_biaya_sopir VARCHAR(20),
  p_biaya_sopir DOUBLE,
  p_kode_akun_biaya_tol VARCHAR(20),
  p_biaya_tol DOUBLE,
  p_kode_akun_biaya_parkir VARCHAR(20),
  p_biaya_parkir DOUBLE,
  p_kode_akun_biaya_bbm VARCHAR(20),
  p_biaya_bbm DOUBLE,
  p_kode_akun_komisi_penumpang_sopir VARCHAR(20),
  p_komisi_penumpang_sopir DOUBLE,
  p_kode_akun_komisi_penumpang_cso VARCHAR(20),
  p_komisi_penumpang_cso DOUBLE,
  p_kode_akun_komisi_paket_sopir VARCHAR(20),
  p_komisi_paket_sopir DOUBLE,
  p_kode_akun_komisi_paket_cso VARCHAR(20),
  p_komisi_paket_cso DOUBLE,
  p_flag_aktif INT(1),p_flag_luar_kota INT(1),
  p_harga_paket_1_kilo_pertama DOUBLE,p_harga_paket_1_kilo_berikut DOUBLE,
  p_harga_paket_2_kilo_pertama DOUBLE,p_harga_paket_2_kilo_berikut DOUBLE,
  p_harga_paket_3_kilo_pertama DOUBLE,p_harga_paket_3_kilo_berikut DOUBLE,
  p_harga_paket_4_kilo_pertama DOUBLE,p_harga_paket_4_kilo_berikut DOUBLE,
  p_harga_paket_5_kilo_pertama DOUBLE,p_harga_paket_5_kilo_berikut DOUBLE,
  p_harga_paket_6_kilo_pertama DOUBLE,p_harga_paket_6_kilo_berikut DOUBLE,
  p_flag_op_jurusan INT(1),p_is_biaya_sopir_kumulatif INT(1),p_is_voucher_bbm INT(1)
  )
BEGIN

   INSERT INTO tbl_md_jurusan (
     KodeJurusan, KodeCabangAsal, KodeCabangTujuan, HargaTiket,
     HargaTiketTuslah, FlagTiketTuslah,KodeAkunPendapatanPenumpang, KodeAkunPendapatanPaket,
     KodeAkunCharge, KodeAkunBiayaSopir, BiayaSopir, KodeAkunKomisiPenumpangSopir,
     KomisiPenumpangSopir, KodeAkunKomisiPenumpangCSO, KomisiPenumpangCSO, KodeAkunKomisiPaketSopir,
     KomisiPaketSopir, KodeAkunKomisiPaketCSO, KomisiPaketCSO,FlagAktif,
     KodeAkunBiayaTol,BiayaTol,
     KodeAkunBiayaParkir,BiayaParkir,
     KodeAkunBiayaBBM,BiayaBBM,FlagLuarKota,
     HargaPaket1KiloPertama,HargaPaket1KiloBerikut,
     HargaPaket2KiloPertama,HargaPaket2KiloBerikut,
     HargaPaket3KiloPertama,HargaPaket3KiloBerikut,
     HargaPaket4KiloPertama,HargaPaket4KiloBerikut,
     HargaPaket5KiloPertama,HargaPaket5KiloBerikut,
     HargaPaket6KiloPertama,HargaPaket6KiloBerikut,
     FlagOperasionalJurusan,IsBiayaSopirKumulatif,
     IsVoucherBBM)
   VALUES (
     p_kode_jurusan, p_kode_cabang_asal, p_kode_cabang_tujuan, p_harga_tiket,
     p_harga_tiket_tuslah, p_flag_tiket_tuslah, p_kode_akun_pendapatan_penumpang, p_kode_akun_pendapatan_paket,
     p_kode_akun_charge, p_kode_akun_biaya_sopir, p_biaya_sopir, p_kode_akun_komisi_penumpang_sopir,
     p_komisi_penumpang_sopir, p_kode_akun_komisi_penumpang_cso, p_komisi_penumpang_cso, p_kode_akun_komisi_paket_sopir,
     p_komisi_paket_sopir, p_kode_akun_komisi_paket_cso, p_komisi_paket_cso,p_flag_aktif,
     p_kode_akun_biaya_tol,p_biaya_tol,

     p_kode_akun_biaya_parkir,p_biaya_parkir,
     p_kode_akun_biaya_bbm,p_biaya_bbm,p_flag_luar_kota,
     p_harga_paket_1_kilo_pertama,p_harga_paket_1_kilo_berikut,
     p_harga_paket_2_kilo_pertama,p_harga_paket_2_kilo_berikut,
     p_harga_paket_3_kilo_pertama,p_harga_paket_3_kilo_berikut,
     p_harga_paket_4_kilo_pertama,p_harga_paket_4_kilo_berikut,
     p_harga_paket_5_kilo_pertama,p_harga_paket_5_kilo_berikut,
     p_harga_paket_6_kilo_pertama,p_harga_paket_6_kilo_berikut,
     p_flag_op_jurusan,p_is_biaya_sopir_kumulatif,
     p_is_voucher_bbm);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah`(
  p_id_jurusan INTEGER,
  p_kode_jurusan VARCHAR(10), p_kode_cabang_asal VARCHAR(20), p_kode_cabang_tujuan VARCHAR(20),
  p_harga_tiket DOUBLE,p_harga_tiket_tuslah DOUBLE,
  p_flag_tiket_tuslah INT(1),
  p_kode_akun_pendapatan_penumpang VARCHAR(20),
  p_kode_akun_pendapatan_paket VARCHAR(20),
  p_kode_akun_charge VARCHAR(20),
  p_kode_akun_biaya_sopir VARCHAR(20),
  p_biaya_sopir DOUBLE,
  p_kode_akun_biaya_tol VARCHAR(20),
  p_biaya_tol DOUBLE,
  p_kode_akun_biaya_parkir VARCHAR(20),
  p_biaya_parkir DOUBLE,
  p_kode_akun_biaya_bbm VARCHAR(20),
  p_biaya_bbm DOUBLE,
  p_kode_akun_komisi_penumpang_sopir VARCHAR(20),
  p_komisi_penumpang_sopir DOUBLE,
  p_kode_akun_komisi_penumpang_cso VARCHAR(20),
  p_komisi_penumpang_cso DOUBLE,
  p_kode_akun_komisi_paket_sopir VARCHAR(20),
  p_komisi_paket_sopir DOUBLE,
  p_kode_akun_komisi_paket_cso VARCHAR(20),
  p_komisi_paket_cso DOUBLE,
  p_flag_aktif INT(1),p_flag_luar_kota INT(1),
  p_harga_paket_1_kilo_pertama DOUBLE,p_harga_paket_1_kilo_berikut DOUBLE,
  p_harga_paket_2_kilo_pertama DOUBLE,p_harga_paket_2_kilo_berikut DOUBLE,
  p_harga_paket_3_kilo_pertama DOUBLE,p_harga_paket_3_kilo_berikut DOUBLE,
  p_harga_paket_4_kilo_pertama DOUBLE,p_harga_paket_4_kilo_berikut DOUBLE,
  p_harga_paket_5_kilo_pertama DOUBLE,p_harga_paket_5_kilo_berikut DOUBLE,
  p_harga_paket_6_kilo_pertama DOUBLE,p_harga_paket_6_kilo_berikut DOUBLE,
  p_flag_op_jurusan INT(1),p_is_biaya_sopir_kumulatif INT(1),p_is_voucher_bbm INT(1))
BEGIN

   UPDATE tbl_md_jurusan SET
     KodeJurusan=p_kode_jurusan,
     KodeCabangAsal=p_kode_cabang_asal,KodeCabangTujuan=p_kode_cabang_tujuan,
     HargaTiket=p_harga_tiket,HargaTiketTuslah=p_harga_tiket_tuslah,
     FlagTiketTuslah=p_flag_tiket_tuslah,KodeAkunPendapatanPenumpang=p_kode_akun_pendapatan_penumpang,
     KodeAkunPendapatanPaket=p_kode_akun_pendapatan_paket,KodeAkunCharge=p_kode_akun_charge,
     KodeAkunBiayaSopir=p_kode_akun_biaya_sopir, BiayaSopir=p_biaya_sopir,
     KodeAkunKomisiPenumpangSopir=p_kode_akun_komisi_penumpang_sopir, KomisiPenumpangSopir=p_komisi_penumpang_sopir,
     KodeAkunKomisiPenumpangCSO=p_kode_akun_komisi_penumpang_cso, KomisiPenumpangCSO=p_komisi_penumpang_cso,
     KodeAkunKomisiPaketSopir=p_kode_akun_komisi_paket_sopir, KomisiPaketSopir=p_komisi_paket_sopir,
     KodeAkunKomisiPaketCSO=p_kode_akun_komisi_paket_cso, KomisiPaketCSO=p_komisi_paket_cso,
     FlagAktif=p_flag_aktif,KodeAkunBiayaTol=p_kode_akun_biaya_tol,
     BiayaTol=p_biaya_tol,KodeAkunBiayaParkir=p_kode_akun_biaya_parkir,
     BiayaParkir=p_biaya_parkir,KodeAkunBiayaBBM=p_kode_akun_biaya_bbm,
     BiayaBBM=p_biaya_bbm,FlagLuarKota=p_flag_luar_kota,
     HargaPaket1KiloPertama=p_harga_paket_1_kilo_pertama,HargaPaket1KiloBerikut=p_harga_paket_1_kilo_berikut,
     HargaPaket2KiloPertama=p_harga_paket_2_kilo_pertama,HargaPaket2KiloBerikut=p_harga_paket_2_kilo_berikut,
     HargaPaket3KiloPertama=p_harga_paket_3_kilo_pertama,HargaPaket3KiloBerikut=p_harga_paket_3_kilo_berikut,
     HargaPaket4KiloPertama=p_harga_paket_4_kilo_pertama,HargaPaket4KiloBerikut=p_harga_paket_4_kilo_berikut,
     HargaPaket5KiloPertama=p_harga_paket_5_kilo_pertama,HargaPaket5KiloBerikut=p_harga_paket_5_kilo_berikut,
     HargaPaket6KiloPertama=p_harga_paket_6_kilo_pertama,HargaPaket6KiloBerikut=p_harga_paket_6_kilo_berikut,
     FlagOperasionalJurusan=p_flag_op_jurusan,IsBiayaSopirKumulatif=p_is_biaya_sopir_kumulatif,
     IsVoucherBBM=p_is_voucher_bbm
   WHERE IdJurusan=p_id_jurusan;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah_status_aktif`(p_id_jurusan INTEGER)
BEGIN

   UPDATE tbl_md_jurusan SET
     FlagAktif=1-FlagAktif
   WHERE IdJurusan=p_id_jurusan;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah_tuslah`(p_id_jurusan INTEGER,p_tuslah INT(1))
BEGIN

   UPDATE tbl_md_jurusan SET
     FlagTiketTuslah=p_tuslah
   WHERE IdJurusan=p_id_jurusan;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_tambah`(
  p_kode_kendaraan VARCHAR(20), p_kode_cabang VARCHAR(20), p_no_polisi VARCHAR(20),
  p_jenis VARCHAR(50),p_merek VARCHAR(50), p_tahun VARCHAR(50), p_warna VARCHAR(50),
  p_jumlah_kursi INT(2), p_kode_sopir1 VARCHAR(50), p_kode_sopir2 VARCHAR(50),
  p_no_STNK VARCHAR(50), p_no_BPKB VARCHAR(50), p_no_rangka VARCHAR(50),
  p_no_mesin VARCHAR(50), p_kilometer_akhir DOUBLE, p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_md_kendaraan (
    KodeKendaraan, KodeCabang, NoPolisi,
    Jenis,Merek, Tahun, Warna,
    JumlahKursi, KodeSopir1, KodeSopir2,
    NoSTNK, NoBPKB, NoRangka,
    NoMesin, KilometerAkhir, FlagAktif)
  VALUES(
   p_kode_kendaraan, p_kode_cabang, p_no_polisi,
    p_jenis,p_merek, p_tahun, p_warna,
    p_jumlah_kursi, p_kode_sopir1, p_kode_sopir2,
    p_no_STNK, p_no_BPKB, p_no_rangka,
    p_no_mesin, p_kilometer_akhir, p_flag_aktif);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_ubah`(
  p_kode_kendaraan_old VARCHAR(20),
  p_kode_kendaraan VARCHAR(20), p_kode_cabang VARCHAR(20), p_no_polisi VARCHAR(20),
  p_jenis VARCHAR(50),p_merek VARCHAR(50), p_tahun VARCHAR(50), p_warna VARCHAR(50),
  p_jumlah_kursi INT(2), p_kode_sopir1 VARCHAR(50), p_kode_sopir2 VARCHAR(50),
  p_no_STNK VARCHAR(50), p_no_BPKB VARCHAR(50), p_no_rangka VARCHAR(50),
  p_no_mesin VARCHAR(50), p_kilometer_akhir DOUBLE, p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_md_kendaraan SET
    KodeKendaraan=p_kode_kendaraan, KodeCabang=p_kode_cabang, NoPolisi=p_no_polisi,
    Jenis=p_jenis,Merek=p_merek, Tahun=p_tahun, Warna=p_warna,
    JumlahKursi=p_jumlah_kursi, KodeSopir1=p_kode_sopir1, KodeSopir2=p_kode_sopir2,
    NoSTNK=p_no_STNK, NoBPKB=p_no_BPKB, NoRangka=p_no_rangka,
    NoMesin=p_no_mesin, KilometerAkhir=p_kilometer_akhir, FlagAktif=p_flag_aktif
  WHERE KodeKendaraan = p_kode_kendaraan_old;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_ubah_status_aktif`(p_kode_kendaraan VARCHAR(50))
BEGIN

   UPDATE tbl_md_kendaraan SET
     FlagAktif=1-FlagAktif
   WHERE KodeKendaraan=p_kode_kendaraan;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_log_user_login`(
  p_user_id INTEGER, p_user_ip VARCHAR(255))
BEGIN

   INSERT INTO  tbl_log_user
     (user_id, waktu_login, user_ip)
   VALUES (p_user_id, NOW(), p_user_ip);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_log_user_logout`(
  p_user_id INTEGER)
BEGIN

  DECLARE p_id_log INTEGER;

  SELECT id_log INTO p_id_log
  FROM tbl_log_user
  WHERE
    user_id=p_user_id
    AND waktu_logout IS NULL
  ORDER BY waktu_login DESC LIMIT 0,1;

  UPDATE tbl_log_user
  SET waktu_logout=NOW()
  WHERE id_log=p_id_log;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_deposit_topup`(p_id_member VARCHAR(30),p_jumlah_topup DOUBLE,p_kode_referensi VARCHAR(30),p_petugas_topup INT(11))
BEGIN

  DECLARE saldo_terakhir DOUBLE;

  START TRANSACTION;
  
  #Mengisi ke log deposit
  INSERT INTO tbl_member_deposit_topup_log (IdMember,KodeReferensi,WaktuTransaksi,Jumlah,PetugasTopUp)
  VALUES(p_id_member,p_kode_referensi,NOW(),p_jumlah_topup,p_petugas_topup);

  #mengupdate deposit member di tabel member
  UPDATE tbl_md_member SET 
    SaldoDeposit=SaldoDeposit+p_jumlah_topup,WaktuTransaksiTerakhir=NOW(),
    Signature=MD5(CONCAT(WaktuTransaksiTerakhir,'#',Handphone,'#',IdMember,'#indonesiatanahairbeta#',Nama,'#',SaldoDeposit,'#')) 
  WHERE IdMember=p_id_member;

  #mengambil saldo terakhir
  SELECT SaldoDeposit INTO saldo_terakhir FROM tbl_md_member WHERE IdMember=p_id_member;

  #Mengisi ke tabel log transaksi
  INSERT INTO tbl_member_deposit_trx_log (IdMember,WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo,Signature)
    VALUES (p_id_member,NOW(),0,CONCAT('TOP UP DEPOSIT ',p_id_member,' ',p_kode_referensi),p_jumlah_topup,saldo_terakhir,
      MD5(CONCAT(NOW(),'#',p_id_member,'#indonesiatanahairbeta#',p_jumlah_topup,'#',saldo_terakhir,'#')) );

  COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_deposit_trx_log`(p_id_member VARCHAR(30),p_kode_booking VARCHAR(30), p_jumlah DOUBLE, p_keterangan TEXT)
BEGIN

  DECLARE saldo_deposit DOUBLE;

  START TRANSACTION;

    #Mengambil nilai saldo terakhir
    SELECT SaldoDeposit INTO saldo_deposit FROM tbl_md_member WHERE IdMember =p_id_member;

    IF saldo_deposit>=p_jumlah THEN
      #jika saldo masih cukup, lakukan proses berikut

      #mengurangi saldo member
      UPDATE tbl_md_member SET
       SaldoDeposit=SaldoDeposit-p_jumlah,WaktuTransaksiTerakhir=NOW(),
       Signature=MD5(CONCAT(WaktuTransaksiTerakhir,'#',Handphone,'#',IdMember,'#indonesiatanahairbeta#',Nama,'#',SaldoDeposit,'#'))
      WHERE IdMember=p_id_member;

      #mengisi transaksi ke tabel log transaksi
      INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
        VALUES(NOW(),1,p_keterangan,p_jumlah,saldo_deposit-p_jumlah,
          MD5(CONCAT(NOW(),'#',p_id_member,'#indonesiatanahairbeta#',p_jumlah,'#',saldo_deposit-p_jumlah,'#')) );

    END IF;
    #Jika saldo tidak cukup, maka tidak akan dilakukan proses apapun

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_tambah`(
  p_id_member VARCHAR(25), p_nama VARCHAR(150), p_jenis_kelamin INT(1),
  p_kategori_member INT(1),p_tempat_lahir VARCHAR(30),p_tgl_lahir DATE,
  p_no_ktp VARCHAR(30),p_tgl_registrasi DATE,p_alamat TEXT,
  p_kota VARCHAR(30),p_kode_pos VARCHAR(6),p_telp VARCHAR(20),
  p_handphone VARCHAR(20),p_email VARCHAR(30),p_pekerjaan VARCHAR(50),
  p_point DOUBLE,p_expired_date DATE, p_id_kartu VARCHAR(255),
  p_no_seri_kartu VARCHAR(30),p_kata_sandi TEXT,p_flag_aktif INT(1),
  p_cabang_daftar VARCHAR(20))
BEGIN

  INSERT INTO tbl_md_member (
    IdMember, Nama, JenisKelamin,
    KategoriMember, TempatLahir, TglLahir,
    NoKTP, TglRegistrasi, Alamat,
    Kota, KodePos, Telp,
    Handphone, Email, Pekerjaan,
    Point, ExpiredDate, IdKartu,
    NoSeriKartu, KataSandi, FlagAktif,
    CabangDaftar)
  VALUES(
    p_id_member, p_nama, p_jenis_kelamin,
    p_kategori_member,p_tempat_lahir,p_tgl_lahir,
    p_no_ktp, p_tgl_registrasi, p_alamat,
    p_kota, p_kode_pos, p_telp,
    p_handphone, p_email, p_pekerjaan,
    p_point, p_expired_date, p_id_kartu,
    p_no_seri_kartu, p_kata_sandi, p_flag_aktif,
    p_cabang_daftar);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_ubah`(
  p_id_member VARCHAR(25), p_nama VARCHAR(150), p_jenis_kelamin INT(1),
  p_kategori_member INT(1),p_tempat_lahir VARCHAR(30),p_tgl_lahir DATE,
  p_no_ktp VARCHAR(30),p_tgl_registrasi DATE,p_alamat TEXT,
  p_kota VARCHAR(30),p_kode_pos VARCHAR(6),p_telp VARCHAR(20),
  p_handphone VARCHAR(20),p_email VARCHAR(30),p_pekerjaan VARCHAR(50),
  p_point DOUBLE,p_expired_date DATE, p_id_kartu VARCHAR(255),
  p_no_seri_kartu VARCHAR(30),p_kata_sandi TEXT,p_flag_aktif INT(1),
  p_cabang_daftar VARCHAR(20))
BEGIN

  UPDATE tbl_md_member SET
    Nama=p_nama, JenisKelamin=p_jenis_kelamin,
    KategoriMember=p_kategori_member, TempatLahir=p_tempat_lahir, TglLahir=p_tgl_lahir,
    NoKTP=p_no_ktp, TglRegistrasi=p_tgl_registrasi, Alamat=p_alamat,
    Kota=p_kota, KodePos=p_kode_pos, Telp=p_telp,
    Handphone=p_handphone, Email=p_email, Pekerjaan=p_pekerjaan,
    Point=p_point, ExpiredDate=p_expired_date, IdKartu=p_id_kartu,
    NoSeriKartu=p_no_seri_kartu, KataSandi=p_kata_sandi, FlagAktif=p_flag_aktif,
    CabangDaftar=p_cabang_daftar
  WHERE IdMember=p_id_member;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_ubah_status_aktif`(p_id_member VARCHAR(25))
BEGIN

   UPDATE tbl_md_member SET
     FlagAktif=1-FlagAktif
   WHERE IdMember=p_id_member;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ambil_paket`(
  p_no_tiket VARCHAR(50),p_nama_pengambil VARCHAR(20),p_no_ktp_pengambil VARCHAR(20),
  p_petugas_pemberi INTEGER)
BEGIN

  UPDATE tbl_paket
  SET
    NamaPengambil=p_nama_pengambil,
    NoKTPPengambil=p_no_ktp_pengambil,
    PetugasPemberi=p_petugas_pemberi,
    WaktuPengambilan=NOW(),
    StatusDiambil=1
  WHERE
    NoTiket = p_no_tiket;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_batal`(
  p_NoTiket VARCHAR(50),p_PetugasPembatalan INTEGER,p_WaktuPembatalan DATETIME)
BEGIN

  UPDATE tbl_paket SET
    FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
    WaktuPembatalan=p_WaktuPembatalan
  WHERE NoTiket=p_NoTiket;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_mutasi`(
  p_kode_jadwal VARCHAR(50),p_id_jurusan INTEGER, p_tgl_berangkat DATE,p_jam_berangkat VARCHAR(10),
  p_user_pemutasi TEXT,p_no_tiket VARCHAR(20))
BEGIN

  UPDATE tbl_paket
  SET
    KodeJadwal=p_kode_jadwal,
    IdJurusan=p_id_jurusan,
    TglBerangkat=p_tgl_berangkat,
    JamBerangkat=p_jam_berangkat,
    FlagMutasi=1,
    WaktuMutasi=CONCAT(NOW(),';',WaktuMutasi),
    UserPemutasi=CONCAT(p_user_pemutasi,';',UserPemutasi)
  WHERE
    NoTiket = p_no_tiket;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_tambah`(
  p_no_tiket VARCHAR(50), p_kode_cabang VARCHAR(20), p_kode_jadwal VARCHAR(30),
  p_id_jurusan INTEGER, p_kode_kendaraan VARCHAR(20), p_kode_sopir VARCHAR(50),
  p_tgl_berangkat DATE, p_jam_berangkat VARCHAR(10), p_nama_pengirim VARCHAR(100),
  p_alamat_pengirim VARCHAR(100), p_telp_pengirim VARCHAR(15), p_waktu_pesan DATETIME,
  p_nama_penerima VARCHAR(100), p_alamat_penerima VARCHAR(100), p_telp_penerima VARCHAR(15),
  p_harga_paket DOUBLE,p_keterangan_paket TEXT, p_petugas_penjual INTEGER,
  p_no_SPJ VARCHAR(35), p_tgl_cetak_SPJ DATE,
  p_cetak_SPJ INT(1), p_komisi_paket_CSO DOUBLE, p_komisi_paket_sopir DOUBLE,
  p_kode_akun_pendapatan VARCHAR(20), p_kode_akun_komisi_paket_CSO VARCHAR(20), p_kode_akun_komisi_paket_sopir VARCHAR(20),
  p_jumlah_koli DOUBLE,p_berat DOUBLE,p_jenis_barang VARCHAR(10),
  p_layanan CHAR(2),p_cara_bayar INT(1),p_is_ekspedisi INT(1))
BEGIN

  INSERT INTO tbl_paket (
    NoTiket, KodeCabang, KodeJadwal,
    IdJurusan, KodeKendaraan, KodeSopir,
    TglBerangkat, JamBerangkat, NamaPengirim,
    AlamatPengirim, TelpPengirim, WaktuPesan,
    NamaPenerima, AlamatPenerima, TelpPenerima,
    HargaPaket,KeteranganPaket, PetugasPenjual,
    NoSPJ, TglCetakSPJ,
    CetakSPJ, KomisiPaketCSO, KomisiPaketSopir,
    KodeAkunPendapatan,KodeAkunKomisiPaketCSO,KodeAkunKomisiPaketSopir,
    JumlahKoli,Berat,JenisBarang,
    Layanan,CaraPembayaran,
    FlagBatal,IsEkspedisi)
  VALUES(
    p_no_tiket, p_kode_cabang, p_kode_jadwal,
    p_id_jurusan, p_kode_kendaraan , p_kode_sopir ,
    p_tgl_berangkat, p_jam_berangkat , p_nama_pengirim ,
    p_alamat_pengirim, p_telp_pengirim, p_waktu_pesan,
    p_nama_penerima, p_alamat_penerima, p_telp_penerima,
    p_harga_paket,p_keterangan_paket, p_petugas_penjual,
    p_no_SPJ, p_tgl_cetak_SPJ,
    p_cetak_SPJ, p_komisi_paket_CSO, p_komisi_paket_sopir,
    p_kode_akun_pendapatan, p_kode_akun_komisi_paket_CSO, p_kode_akun_komisi_paket_sopir,
    p_jumlah_koli,p_berat,p_jenis_barang,
    p_layanan,p_cara_bayar,
    0,p_is_ekspedisi);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ubah_data`(
  p_no_tiket VARCHAR(50),
  p_nama_pengirim VARCHAR(100),
  p_alamat_pengirim VARCHAR(100),
  p_telp_pengirim VARCHAR(15),
  p_nama_penerima VARCHAR(100),
  p_alamat_penerima VARCHAR(100),
  p_telp_penerima VARCHAR(15))
BEGIN

  UPDATE tbl_paket SET
    NamaPengirim=p_nama_pengirim,
    AlamatPengirim=p_alamat_pengirim,
    TelpPengirim=p_telp_pengirim,
    NamaPenerima=p_nama_penerima,
    AlamatPenerima=p_alamat_penerima,
    TelpPenerima=p_telp_penerima
  WHERE
    NoTiket=p_no_tiket;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ubah_data_after_spj`(
  p_kode_jadwal VARCHAR(50), p_tgl_berangkat DATE, p_sopir_dipilih VARCHAR(50),
  p_mobil_dipilih VARCHAR(50), p_no_spj VARCHAR(50))
BEGIN

  UPDATE tbl_paket

  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    TglCetakSPJ=NOW(),
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_update_cetak_tiket`(
  p_no_tiket VARCHAR(50), p_jenis_pembayaran INT(1),p_cabang_transaksi VARCHAR(30))
BEGIN

  UPDATE tbl_paket
  SET
    CetakTiket=1,
    JenisPembayaran=p_jenis_pembayaran,
    KodeCabang=p_cabang_transaksi
  WHERE
    NoTiket = p_no_tiket;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pelanggan_tambah`(
  p_no_hp VARCHAR(20), p_no_telp VARCHAR(20),p_nama VARCHAR(150),
  p_alamat VARCHAR(50), p_tgl_pertama_transaksi DATE, p_tgl_terakhir_transaksi DATE,
  p_cso_terakhir INTEGER, p_kode_jurusan_terakhir VARCHAR(10),p_frekwensi_pergi DOUBLE,
  p_flag_member INT(1))
BEGIN

  INSERT INTO tbl_pelanggan (
    NoHP, NoTelp, Nama,
    Alamat, TglPertamaTransaksi, TglTerakhirTransaksi,
    CSOTerakhir, KodeJurusanTerakhir, FrekwensiPergi,
    FlagMember)
  VALUES(
    p_no_hp, p_no_telp,p_nama,
    p_alamat, p_tgl_pertama_transaksi, p_tgl_terakhir_transaksi,
    p_cso_terakhir, p_kode_jurusan_terakhir,p_frekwensi_pergi,
    p_flag_member);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pelanggan_ubah`(
  p_no_telp_old VARCHAR(20),p_no_hp_old VARCHAR(20),
  p_no_hp VARCHAR(20), p_no_telp VARCHAR(20),p_nama VARCHAR(150),
  p_alamat VARCHAR(50), p_tgl_terakhir_transaksi DATE,
  p_cso_terakhir INTEGER, p_kode_jurusan_terakhir VARCHAR(10))
BEGIN

  UPDATE tbl_pelanggan SET
    NoHP=p_no_hp, NoTelp=p_no_telp, Nama=p_nama,
    TglTerakhirTransaksi=p_tgl_terakhir_transaksi,
    CSOTerakhir=p_cso_terakhir, KodeJurusanTerakhir=p_kode_jurusan_terakhir, FrekwensiPergi=FrekwensiPergi+1
  WHERE NoTelp=p_no_telp_old;

  IF p_alamat!='' THEN
    UPDATE tbl_pelanggan SET
      Alamat=p_alamat
    WHERE NoTelp=p_no_telp_old;
  END IF;

  END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengumuman_tambah`(
  p_kode_pengumuman VARCHAR(20),p_judul_pengumuman VARCHAR(255), p_pengumuman TEXT,
  p_kota VARCHAR(20), p_pembuat_pengumuman INTEGER, p_waktu_pembuatan DATE)
BEGIN

  INSERT INTO tbl_pengumuman (
    KodePengumuman,JudulPengumuman, Pengumuman,
    Kota,PembuatPengumuman, WaktuPembuatanPengumuman)
  VALUES(
    p_kode_pengumuman,p_judul_pengumuman, p_pengumuman,
    p_kota, p_pembuat_pengumuman, p_waktu_pembuatan);

  CALL sp_user_update_pengumuman_baru();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengumuman_ubah`(
  p_id_pengumuman INTEGER,p_kode_pengumuman_old VARCHAR(20),
  p_kode_pengumuman VARCHAR(20),p_judul_Pengumuman VARCHAR(255), p_pengumuman TEXT,
  p_kota VARCHAR(20), p_pembuat_pengumuman INTEGER, p_waktu_pembuatan DATE)
BEGIN

  UPDATE tbl_pengumuman SET
    KodePengumuman=p_kode_pengumuman, JudulPengumuman=p_judul_pengumuman, Pengumuman=p_pengumuman,
    Kota=p_kota, PembuatPengumuman=p_pembuat_pengumuman, WaktuPembuatanPengumuman=p_waktu_pembuatan
  WHERE IdPengumuman=p_id_pengumuman;

  CALL sp_user_update_pengumuman_baru();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_penjadwalan_kendaraan_ubah_status_aktif`(p_id_jadwal INTEGER)
BEGIN

   UPDATE tbl_penjadwalan_kendaraan SET
     StatusAktif=1-StatusAktif

   WHERE IdPenjadwalan=p_id_jadwal;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_posisi_tambah`(p_kode_jadwal VARCHAR(50),p_tgl_berangkat DATE,p_jam_berangkat TIME,
  p_jumlah_kursi INT(2),p_kode_kendaraan VARCHAR(20),p_kode_sopir VARCHAR(50))
BEGIN

  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_tgl_berangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN
    INSERT INTO tbl_posisi
      (KodeJadwal,TglBerangkat,JamBerangkat,JumlahKursi,SisaKursi,
      KodeKendaraan,KodeSopir)
    VALUES(
      p_kode_jadwal,p_tgl_berangkat,p_jam_berangkat,p_jumlah_kursi,p_jumlah_kursi,
      p_kode_kendaraan,p_kode_sopir);
  ELSE
    INSERT INTO tbl_posisi
      (KodeJadwal,TglBerangkat,JamBerangkat,JumlahKursi,SisaKursi,
      KodeKendaraan,KodeSopir)
    VALUES(
      p_kode_jadwal,p_tgl_berangkat,p_jam_berangkat,p_jumlah_kursi,p_jumlah_kursi,
      p_kode_kendaraan,p_kode_sopir);
  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_ubah_status_aktif`(p_kode varchar(50))
BEGIN

   UPDATE tbl_md_promo SET
     FlagAktif=1-FlagAktif
   WHERE KodePromo=p_kode;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_batal`(
  p_NoTiket VARCHAR(50),p_PetugasPembatalan INTEGER,p_WaktuPembatalan DATETIME)
BEGIN

  UPDATE tbl_reservasi SET
    FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
    WaktuPembatalan=p_WaktuPembatalan
  WHERE NoTiket=p_NoTiket;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
      WaktuPembatalan=p_WaktuPembatalan
    WHERE NoTiket=p_NoTiket;
  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_insert_status_kursi`(
  p_NomorKursi INT(2), p_Session INTEGER, p_KodeJadwal VARCHAR(50),
  p_TglBerangkat DATE)
BEGIN

  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN

    INSERT INTO tbl_posisi_detail (
      NomorKursi, KodeJadwal, TglBerangkat,
      StatusKursi, Session,SessionTime)
    VALUES(
      p_NomorKursi, p_KodeJadwal, p_TglBerangkat,
      1, p_Session,NOW());
  ELSE
    INSERT INTO tbl_posisi_detail_backup (
      NomorKursi, KodeJadwal, TglBerangkat,
      StatusKursi, Session,SessionTime)
    VALUES(
      p_NomorKursi, p_KodeJadwal, p_TglBerangkat,
      1, p_Session,NOW());
  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_mutasi`(
  p_NoTiket VARCHAR(50), p_KodeJadwal VARCHAR(50), p_IdJurusan INTEGER,
  p_KodeKendaraan VARCHAR(20), p_KodeSopir VARCHAR(50), p_TglBerangkat DATE,
  p_JamBerangkat VARCHAR(10), p_NomorKursi INT(2), p_HargaTiket DOUBLE,

  p_Charge DOUBLE, p_SubTotal DOUBLE, p_Discount DOUBLE,
  p_PPN DOUBLE, p_Total DOUBLE, p_NoSPJ VARCHAR(35),
  p_TglCetakSPJ DATETIME, p_CetakSPJ INT(1), p_KomisiPenumpangCSO DOUBLE,
  p_PetugasCetakSPJ INTEGER, p_Keterangan VARCHAR(100), p_JenisDiscount VARCHAR(50),
  p_KodeAkunPendapatan VARCHAR(20), p_KodeAkunKomisiPenumpangCSO VARCHAR(20), p_PaymentCode VARCHAR(20),
  p_Nama VARCHAR(50),p_StatusBayar INT(1),p_KodeJadwalLama VARCHAR(50),
  p_TglBerangkatLama DATE,p_NomorKursiLama INT(2),p_Pemutasi INTEGER)
BEGIN

  

  DECLARE p_KodeBooking VARCHAR(50);
  DECLARE p_KodeJadwalOld VARCHAR(50);
  DECLARE p_TglBerangkatOld DATE;

  SELECT
    KodeBooking,KodeJadwal,TglBerangkat
    INTO p_KodeBooking,p_KodeJadwalOld,p_TglBerangkatOld
  FROM tbl_reservasi WHERE NoTiket=p_NoTiket;

  UPDATE tbl_reservasi SET
    KodeJadwal=p_KodeJadwal, IdJurusan=p_IdJurusan,
    KodeKendaraan=p_KodeKendaraan, KodeSopir=p_KodeSopir, TglBerangkat=p_TglBerangkat,
    JamBerangkat=p_JamBerangkat, NomorKursi=p_NomorKursi, NoSPJ=p_NoSPJ,
    TglCetakSPJ=p_TglCetakSPJ, CetakSPJ=p_CetakSPJ, KomisiPenumpangCSO=p_KomisiPenumpangCSO,
    PetugasCetakSPJ=p_PetugasCetakSPJ, Keterangan=p_Keterangan, JenisDiscount=p_JenisDiscount,
    KodeAkunPendapatan=p_KodeAkunPendapatan, KodeAkunKomisiPenumpangCSO=p_KodeAkunKomisiPenumpangCSO,PaymentCode=p_PaymentCode,
    WaktuMutasi=NOW(),Pemutasi=p_Pemutasi,
    KodeBooking=if(p_KodeJadwalOld=p_KodeJadwal AND p_TglBerangkatOld=p_TglBerangkat,KodeBooking,CONCAT(KodeBooking,"M"))
  WHERE NoTiket=p_NoTiket;

  
  CALL sp_reservasi_set_status_kursi(
    p_NomorKursi,null, f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal),
    p_TglBerangkat,0,0);

  
  UPDATE tbl_posisi_detail SET
    StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
    KodeBooking=if(f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwalOld)=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal) AND p_TglBerangkatOld=p_TglBerangkat,p_KodeBooking,CONCAT(p_KodeBooking,"M")),
    Session=NULL,StatusBayar=p_StatusBayar
  WHERE
    NomorKursi=p_NomorKursi
    AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
    AND TglBerangkat=p_TglBerangkat;


  
  UPDATE tbl_posisi_detail SET
    StatusKursi=0,Nama=NULL,NoTiket=NULL,
    KodeBooking=NULL,Session=NULL,StatusBayar=0
  WHERE
    NomorKursi=p_NomorKursiLama
    AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwalLama)
    AND TglBerangkat=p_TglBerangkatLama;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_set_status_kursi`(p_NomorKursi INT(2), p_Session INTEGER, p_KodeJadwal VARCHAR(50),
  p_TglBerangkat DATE,p_JamBerangkat VARCHAR(8), p_KodeCabangAsal VARCHAR(10), p_KodeCabangTujuan VARCHAR(10), p_StatusKursiAdmin INT(1),p_session_time_expired int)
BEGIN

  DECLARE p_ditemukan INT;
  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN
    SELECT COUNT(NomorKursi) INTO p_ditemukan FROM tbl_posisi_detail WHERE NomorKursi=p_NomorKursi AND TglBerangkat=p_TglBerangkat AND KodeJadwal=P_KodeJadwal;
  ELSE
    SELECT COUNT(NomorKursi) INTO p_ditemukan FROM tbl_posisi_detail_backup WHERE NomorKursi=p_NomorKursi AND TglBerangkat=p_TglBerangkat AND KodeJadwal=P_KodeJadwal;
  END IF;

  IF(p_ditemukan>0) THEN
    CALL sp_reservasi_update_status_kursi(p_NomorKursi,p_Session,p_KodeJadwal,p_TglBerangkat,p_StatusKursiAdmin,p_session_time_expired);
  ELSE
    CALL sp_reservasi_insert_status_kursi(p_NomorKursi,p_Session,p_KodeJadwal,p_TglBerangkat);
  END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_tambah`(IN `p_NoTiket` VARCHAR(50), IN `p_KodeCabang` VARCHAR(20), IN `p_KodeJadwal` VARCHAR(50), IN `p_IdJurusan` INT, IN `p_KodeKendaraan` VARCHAR(20), IN `p_KodeSopir` VARCHAR(50), IN `p_TglBerangkat` DATE, IN `p_JamBerangkat` VARCHAR(10), IN `p_KodeBooking` VARCHAR(50), IN `p_IdMember` VARCHAR(25), IN `p_PointMember` INT(3), IN `p_Nama` VARCHAR(100), IN `p_Alamat` VARCHAR(100), IN `p_Telp` VARCHAR(15), IN `p_HP` VARCHAR(15), IN `p_WaktuPesan` DATETIME, IN `p_NomorKursi` INT(2), IN `p_HargaTiket` DOUBLE, IN `p_Charge` DOUBLE, IN `p_SubTotal` DOUBLE, IN `p_Discount` DOUBLE, IN `p_PPN` DOUBLE, IN `p_Total` DOUBLE, IN `p_PetugasPenjual` INT, IN `p_FlagPesanan` INT(1), IN `p_NoSPJ` VARCHAR(35), IN `p_TglCetakSPJ` DATETIME, IN `p_CetakSPJ` INT(1), IN `p_KomisiPenumpangCSO` DOUBLE, IN `p_FlagSetor` INT(1), IN `p_PetugasCetakSPJ` INT, IN `p_Keterangan` VARCHAR(100), IN `p_JenisDiscount` VARCHAR(50), IN `p_KodeAkunPendapatan` VARCHAR(20), IN `p_JenisPenumpang` CHAR(6), IN `p_KodeAkunKomisiPenumpangCSO` VARCHAR(20), IN `p_PaymentCode` VARCHAR(20))
BEGIN

    INSERT INTO tbl_reservasi (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0);

    UPDATE tbl_posisi_detail SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=p_KodeJadwal
      AND TglBerangkat=p_TglBerangkat;
  
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_tambah_with_komisi`(
  p_NoTiket VARCHAR(50), p_KodeCabang VARCHAR(20), p_KodeJadwal VARCHAR(50),
  p_IdJurusan INTEGER, p_KodeKendaraan VARCHAR(20), p_KodeSopir VARCHAR(50),
  p_TglBerangkat DATE, p_JamBerangkat VARCHAR(10), p_KodeBooking VARCHAR(50),
  p_IdMember VARCHAR(25), p_PointMember INT(3), p_Nama VARCHAR(100),
  p_Alamat VARCHAR(100), p_Telp VARCHAR(15), p_HP VARCHAR(15),
  p_WaktuPesan DATETIME, p_NomorKursi INT(2), p_HargaTiket DOUBLE,
  p_Charge DOUBLE, p_SubTotal DOUBLE, p_Discount DOUBLE,
  p_PPN DOUBLE, p_Total DOUBLE, p_PetugasPenjual INTEGER,
  p_FlagPesanan INT(1), p_NoSPJ VARCHAR(35), p_TglCetakSPJ DATETIME,
  p_CetakSPJ INT(1), p_KomisiPenumpangCSO DOUBLE, p_FlagSetor INT(1) ,
  p_PetugasCetakSPJ INTEGER, p_Keterangan VARCHAR(100), p_JenisDiscount VARCHAR(50),
  p_KodeAkunPendapatan VARCHAR(20), p_JenisPenumpang CHAR(2), p_KodeAkunKomisiPenumpangCSO VARCHAR(20),
  p_PaymentCode VARCHAR(20))
BEGIN

  DECLARE p_selisih_hari INTEGER;
  DECLARE p_komisi DOUBLE;

  #Mengambil nilai komisi untuk jurusan luar kota dan bukan jenis penumpang RETURN
  SELECT
    IF((SELECT FlagLuarKota FROM tbl_md_jurusan WHERE IdJurusan=p_IdJurusan)=1
      AND p_JenisPenumpang!='R',NilaiParameter,0) INTO p_komisi
  FROM tbl_pengaturan_parameter
  WHERE NamaParameter='KOMISITTX1';

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN

    INSERT INTO tbl_reservasi (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal,Komisi)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0,p_komisi);

    UPDATE tbl_posisi_detail SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
      AND TglBerangkat=p_TglBerangkat;
  ELSE
    INSERT INTO tbl_reservasi_olap (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal,Komisi)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0,p_komisi);

    UPDATE tbl_posisi_detail_backup SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
      AND TglBerangkat=p_TglBerangkat;
  END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_data_after_spj`(p_kode_jadwal VARCHAR(50), p_tgl_berangkat DATE, p_sopir_dipilih VARCHAR(50),
  p_mobil_dipilih VARCHAR(50), p_no_spj VARCHAR(50))
BEGIN

  UPDATE tbl_reservasi
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap
    SET
      KodeSopir=p_sopir_dipilih,
      KodeKendaraan=p_mobil_dipilih,
      NoSPJ=p_no_spj,
      CetakSPJ=1
    WHERE
      TglBerangkat = p_tgl_berangkat
      AND KodeJadwal=p_kode_jadwal;
  END IF;

  UPDATE tbl_paket
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_data_penumpang`(
  p_no_tiket VARCHAR(50),p_nama VARCHAR(100),p_alamat VARCHAR(100),p_telp VARCHAR(15),
  p_id_member VARCHAR(25))
BEGIN

  UPDATE tbl_reservasi SET
    Nama=p_nama,Alamat=p_alamat,Telp=p_telp,IdMember=p_id_member
  WHERE NoTiket=p_no_tiket AND FlagBatal!=1 AND CetakTiket!=1;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      Nama=p_nama,Alamat=p_alamat,Telp=p_telp,IdMember=p_id_member
    WHERE NoTiket=p_no_tiket AND FlagBatal!=1 AND CetakTiket!=1;
  END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_discount`(IN `p_NoTiket` VARCHAR(50), IN `p_kode_discount` CHAR(6), IN `p_NamaDiscount` VARCHAR(30), IN `p_JumlahDiscount` DOUBLE)
BEGIN

  DECLARE p_kode_booking VARCHAR(50);

  

  

  

  UPDATE tbl_reservasi SET
    JenisPenumpang=p_kode_discount,
    Discount=IF(p_JumlahDiscount>1,IF(p_JumlahDiscount<=HargaTiket,p_JumlahDiscount,HargaTiket),p_JumlahDiscount*HargaTiket),
    JenisDiscount=p_NamaDiscount,
    Total=SubTotal-Discount
  WHERE NoTiket IN(p_NoTiket);

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      JenisPenumpang=p_kode_discount,
      Discount=IF(p_JumlahDiscount>1,IF(p_JumlahDiscount<=HargaTiket,p_JumlahDiscount,HargaTiket),p_JumlahDiscount*HargaTiket),
      JenisDiscount=p_NamaDiscount,
      Total=SubTotal-Discount
  WHERE NoTiket IN(p_NoTiket);
  END IF;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_update_status_kursi`(p_NomorKursi INT(2), p_Session INTEGER, p_KodeJadwalDipilih VARCHAR(50), p_JamBerangkat TIME,
 p_KodeCabangAsal VARCHAR(50), p_KodeCabangTujuan VARCHAR(50), p_IsSubJadwal INT(1), p_KodeJadwalUtama VARCHAR(50), p_TglBerangkat DATE,p_StatusKursiAdmin INT(1),p_session_time_expired int)
BEGIN


  DECLARE p_selisih_hari INTEGER;
  DECLARE p_ditemukan INTEGER;

  /*SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;*/

  /*IF p_selisih_hari<=0 THEN*/

    SELECT COUNT(1) INTO p_ditemukan FROM tbl_posisi_detail
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=p_KodeJadwalDipilih
			AND KodeJadwalUtama=p_KodeJadwalUtama
			AND IsSubJadwal=p_IsSubJadwal
      AND TglBerangkat=p_TglBerangkat
      AND (NoTiket='' OR NoTiket IS NULL)
      AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);

    IF p_ditemukan<=0 THEN
     INSERT INTO tbl_posisi_detail (
        NomorKursi, KodeJadwal, JamBerangkat,
				IsSubJadwal, KodeCabangAsal,KodeCabangTujuan, 
				KodeJadwalUtama,TglBerangkat,StatusKursi, 
				Session,SessionTime)
      VALUES(
        p_NomorKursi, p_KodeJadwalDipilih , p_JamBerangkat,
				p_IsSubJadwal, p_KodeCabangAsal ,p_KodeCabangTujuan ,
				p_KodeJadwalUtama, p_TglBerangkat,1, 
				p_Session,NOW());
    ELSE
      UPDATE tbl_posisi_detail SET
        StatusKursi=IF(f_reservasi_session_time_selisih(SessionTime)<=p_session_time_expired OR StatusKursi=0,1-StatusKursi,StatusKursi), Session=p_Session , SessionTime=NOW()
      WHERE
        NomorKursi=p_NomorKursi
        AND KodeJadwal=p_KodeJadwalDipilih
				AND KodeJadwalUtama=p_KodeJadwalUtama
				AND IsSubJadwal=p_IsSubJadwal
        AND TglBerangkat=p_TglBerangkat
        AND (NoTiket='' OR NoTiket IS NULL)
        AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);

    END IF;

   /*ELSE
     SELECT COUNT(1) INTO p_ditemukan FROM tbl_posisi_detail
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=p_KodeJadwalDipilih 
      AND TglBerangkat=p_TglBerangkat
      AND (NoTiket='' OR NoTiket IS NULL)
      AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);

    IF p_ditemukan<=0 THEN
     INSERT INTO tbl_posisi_detail_backup (
        NomorKursi, KodeJadwal, TglBerangkat,
        StatusKursi, Session,SessionTime)
      VALUES(
        p_NomorKursi, p_KodeJadwal, p_TglBerangkat,
        1, p_Session,NOW());
    ELSE
      UPDATE tbl_posisi_detail_backup SET
        StatusKursi=IF(f_reservasi_session_time_selisih(SessionTime)<=p_session_time_expired OR StatusKursi=0,1-StatusKursi,StatusKursi), Session=p_Session , SessionTime=NOW()
      WHERE
        NomorKursi=p_NomorKursi
        AND KodeJadwal=p_KodeJadwal
        AND TglBerangkat=p_TglBerangkat
        AND (NoTiket='' OR NoTiket IS NULL)
        AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);
    END IF;
  END IF;*/
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sessions_create`(
  p_user_id INTEGER, p_session_id VARCHAR(255),
  p_user_ip VARCHAR(255),p_current_time FLOAT(15),
  p_page_id FLOAT(15),p_login FLOAT(15),
  p_admin FLOAT(15))
BEGIN

   DELETE FROM tbl_sessions
   WHERE session_user_id=-1 OR session_user_id=p_user_id;

   INSERT INTO  tbl_sessions
     (session_id, session_user_id, session_start, session_time, session_ip, session_page, session_logged_in, session_admin)
   VALUES (p_session_id, p_user_id, p_current_time, p_current_time,p_user_ip, p_page_id, p_login, p_admin);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sessions_end`(
  p_user_id INTEGER, p_session_id VARCHAR(255))
BEGIN

   DELETE FROM  tbl_sessions
   WHERE session_id = p_session_id
   AND session_user_id = p_user_id;

   UPDATE tbl_user
   SET status_online=0,waktu_logout=NOW()
   WHERE user_id = p_user_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_tambah`(
  p_kode_sopir VARCHAR(50), p_nama VARCHAR(150), p_HP VARCHAR(50),
  p_alamat VARCHAR(50), p_no_SIM VARCHAR(50), p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_md_sopir (
    KodeSopir, Nama, HP, Alamat, NoSIM, FlagAktif)
  VALUES(
    p_kode_sopir, p_nama, p_HP, p_alamat, p_no_SIM, p_flag_aktif);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_ubah`(
  p_kode_sopir_old VARCHAR(50),
  p_kode_sopir VARCHAR(50), p_nama VARCHAR(150), p_HP VARCHAR(50),
  p_alamat VARCHAR(50), p_no_SIM VARCHAR(50), p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_md_sopir SET
    KodeSopir=p_kode_sopir, Nama=p_nama, HP=p_HP,
    Alamat=p_alamat, NoSIM=p_no_SIM, FlagAktif=p_flag_aktif
  WHERE KodeSopir=p_kode_sopir_old;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_ubah_status_aktif`(p_kode_sopir VARCHAR(50))
BEGIN

   UPDATE tbl_md_sopir SET
     FlagAktif=1-FlagAktif
   WHERE KodeSopir=p_kode_sopir;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_tambah`(
  p_no_spj VARCHAR(50), p_kode_jadwal VARCHAR(50), p_tgl_berangkat DATE,
  p_jam_berangkat VARCHAR(10), p_layout_kursi INT(2), p_jumlah_penumpang INT(2),
  p_mobil_dipilih VARCHAR(50), p_cso INTEGER, p_sopir_dipilih VARCHAR(50),
  p_nama_sopir VARCHAR(50),p_total_omzet DOUBLE,
  p_jumlah_paket INT(3), p_total_omzet_paket DOUBLE,p_is_ekspedisi INT(1))
BEGIN

  INSERT INTO tbl_spj
    (NoSPJ,KodeJadwal,TglBerangkat,JamBerangkat,
    JumlahKursiDisediakan,JumlahPenumpang,TglSPJ,
    NoPolisi,CSO,KodeDriver,
    Driver,TotalOmzet,IdJurusan,
    FlagAmbilBiayaOP, JumlahPaket,TotalOmzetPaket,
    IsEkspedisi)
  VALUES(
    p_no_spj,p_kode_jadwal,p_tgl_berangkat, p_jam_berangkat,
    p_layout_kursi,p_jumlah_penumpang,NOW(),
    p_mobil_dipilih,p_cso,p_sopir_dipilih,
    p_nama_sopir,p_total_omzet,f_jadwal_ambil_id_jurusan_by_kode_jadwal(p_kode_jadwal),
    0,p_jumlah_paket,p_total_omzet_paket,
    p_is_ekspedisi);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_ubah`(p_no_spj VARCHAR(50),p_jumlah_penumpang INT(2), p_mobil_dipilih VARCHAR(50),
  p_cso INTEGER, p_sopir_dipilih VARCHAR(50),p_nama_sopir VARCHAR(50),
  p_total_omzet DOUBLE,
  p_jumlah_paket INT(3),p_total_omzet_paket DOUBLE)
BEGIN

  UPDATE tbl_spj SET
    JumlahPenumpang=p_jumlah_penumpang,
    NoPolisi=p_mobil_dipilih,
    CSO=p_cso,
    KodeDriver=p_sopir_dipilih,
    Driver=p_nama_sopir,
    TotalOmzet=p_total_omzet,
    JumlahPaket=p_jumlah_paket,
    TotalOmzetPaket=p_total_omzet_paket
  WHERE NoSPJ=p_no_spj;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_ubah_insentif_sopir`(
  p_no_spj VARCHAR(50),p_insentif_sopir DOUBLE)
BEGIN

  UPDATE tbl_spj SET
    InsentifSopir=p_insentif_sopir
  WHERE NoSPJ=p_no_spj;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_baca_pengumuman_tambah`(
  p_id_pengumuman INTEGER,p_user_id INTEGER)
BEGIN

  INSERT INTO tbl_user_baca_pengumuman (
    user_id,IdPengumuman)
  VALUES(
    p_user_id, p_id_pengumuman);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_reset_pengumuman_baru`(p_user_id INTEGER)
BEGIN

  UPDATE tbl_user SET jumlah_pengumuman_baru=0
  WHERE user_id=p_user_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_tambah`(
  p_username VARCHAR(50), p_user_password VARCHAR(255),p_NRP VARCHAR(20),
  p_nama VARCHAR(255),p_telp VARCHAR(20), p_hp VARCHAR(20),
  p_email VARCHAR(50),p_address VARCHAR(255), p_KodeCabang VARCHAR(20),
  p_status_online BIT, p_berlaku DATETIME, p_user_level DECIMAL(3,1),
  p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_user (
    username, user_password, NRP,
    nama, telp, hp,
    email, address, KodeCabang,
    status_online,berlaku,user_level,
    user_active)
  VALUES(
    p_username, p_user_password,p_NRP,
    p_nama,p_telp, p_hp,
    p_email,p_address, p_KodeCabang,
    p_status_online, p_berlaku, p_user_level,
    p_flag_aktif);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_ubah`(
  p_user_id INTEGER,
  p_username VARCHAR(50), p_NRP VARCHAR(20),
  p_nama VARCHAR(255),p_telp VARCHAR(20), p_hp VARCHAR(20),
  p_email VARCHAR(50),p_address VARCHAR(255), p_KodeCabang VARCHAR(20),
  p_status_online BIT, p_berlaku DATETIME, p_user_level DECIMAL(3,1),
  p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_user SET
    username=p_username, NRP=p_NRP,
    nama=p_nama, telp=p_telp, hp=p_hp,
    email=p_email, address=p_address, KodeCabang=p_KodeCabang,
    status_online=p_status_online,berlaku=p_berlaku,user_level=p_user_level,
    user_active=p_flag_aktif
  WHERE user_id=p_user_id;    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_update_pengumuman_baru`()
BEGIN

  UPDATE tbl_user SET jumlah_pengumuman_baru=jumlah_pengumuman_baru+1;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_get_kota_by_kode_cabang`(p_kode VARCHAR(30)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kota VARCHAR(50);

  SELECT
    Kota INTO p_kota
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_kota;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_get_name_by_kode`(p_kode VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(50);

  SELECT
    Nama INTO p_nama
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_nama;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeCabang) INTO p_jum_data
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_discount_get_jumlah_by_id`(p_id INTEGER) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT JumlahDiscount INTO p_jumlah
  FROM tbl_jenis_discount
  WHERE IdDiscount=p_id;

  RETURN p_jumlah;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_id_jurusan_by_kode_jadwal`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_id_jurusan INTEGER;

  SELECT  IdJurusan INTO p_id_jurusan
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_id_jurusan;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_jumlah_kursi_by_kode_jadwal`(p_kode VARCHAR(50)) RETURNS int(2)
BEGIN
  DECLARE p_jumlah_kursi INT(2);

  SELECT  JumlahKursi INTO p_jumlah_kursi
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_jumlah_kursi;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_kodeutama_by_kodejadwal`(p_kode VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kode_jadwal_utama VARCHAR(50);

  SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama) INTO p_kode_jadwal_utama
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_kode_jadwal_utama;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeJadwal) INTO p_jum_data
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_harga_tiket_by_id_jurusan`(
  p_id_jurusan INT(11),p_tgl_berangkat DATE) RETURNS double
BEGIN
  DECLARE p_harga_tiket DOUBLE;
  DECLARE p_tgl_awal_tuslah1 DATE;
  DECLARE p_tgl_akhir_tuslah1 DATE;
  DECLARE p_tgl_awal_tuslah2 DATE;
  DECLARE p_tgl_akhir_tuslah2 DATE;

  SELECT NilaiParameter INTO p_tgl_awal_tuslah1
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_MULAI_TUSLAH1';

  SELECT NilaiParameter INTO p_tgl_akhir_tuslah1
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_AKHIR_TUSLAH1';
  
  SELECT NilaiParameter INTO p_tgl_awal_tuslah2
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_MULAI_TUSLAH2';

  SELECT NilaiParameter INTO p_tgl_akhir_tuslah2
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_AKHIR_TUSLAH2';

  SELECT 
    IF(FlagLuarKota=1,IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah1 AND p_tgl_akhir_tuslah1,HargaTiketTuslah,HargaTiket),IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah2 AND p_tgl_akhir_tuslah2,HargaTiketTuslah,HargaTiket)) INTO p_harga_tiket
  FROM tbl_md_jurusan
  WHERE IdJurusan=p_id_jurusan;
  
  RETURN p_harga_tiket;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_harga_tiket_by_kode_jadwal`(
  p_kode_jadwal VARCHAR(30),p_tgl_berangkat DATE) RETURNS double
BEGIN
  DECLARE p_harga_tiket DOUBLE;
  DECLARE p_tgl_awal_tuslah DATE;
  DECLARE p_tgl_akhir_tuslah DATE;

  SELECT TglMulaiTuslah,TglAkhirTuslah INTO p_tgl_awal_tuslah,p_tgl_akhir_tuslah
  FROM tbl_pengaturan_umum LIMIT 0,1;

  SELECT IF(p_tgl_berangkat NOT BETWEEN p_tgl_awal_tuslah AND p_tgl_akhir_tuslah,HargaTiket,HargaTiketTuslah) INTO p_harga_tiket
  FROM tbl_md_jurusan AS tmjr INNER JOIN tbl_md_jadwal tmj ON tmjr.IdJurusan=tmj.IdJurusan
  WHERE tmj.KodeJadwal=p_kode_jadwal;
  
  RETURN p_harga_tiket;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_kode_cabang_asal_by_jurusan`(p_id_jurusan INTEGER) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_kode_cabang_asal VARCHAR(20);

    SELECT KodeCabangAsal INTO p_kode_cabang_asal FROM tbl_md_jurusan WHERE IdJurusan=p_id_jurusan;

  
    RETURN p_kode_cabang_asal;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_kode_cabang_tujuan_by_jurusan`(p_id_jurusan INTEGER) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_kode_cabang_tujuan VARCHAR(20);

    SELECT KodeCabangTujuan INTO p_kode_cabang_tujuan FROM tbl_md_jurusan WHERE IdJurusan=p_id_jurusan;
  
    RETURN p_kode_cabang_tujuan;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeJurusan) INTO p_jum_data
  FROM tbl_md_jurusan
  WHERE KodeJurusan=p_kode;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_ambil_nopol_by_kode`(p_kode VARCHAR(50)) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_nopol VARCHAR(20);

  SELECT NoPolisi INTO p_nopol
  FROM tbl_md_kendaraan
  WHERE KodeKendaraan=p_kode;

  RETURN p_nopol;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeKendaraan) INTO p_jum_data
  FROM tbl_md_kendaraan
  WHERE KodeKendaraan=p_kode;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_periksa_duplikasi_by_nopol`(p_nopol VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeKendaraan) INTO p_jum_data
  FROM tbl_md_kendaraan
  WHERE NoPolisi=p_nopol;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_laporan_user_get_summary`(
  p_userid INTEGER,
  p_tgl_awal DATE,
  p_tgl_akhir DATE,
  p_jenis_pembayaran INT(1)) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT IF(SUM(Total) IS NOT NULL,SUM(Total),0) INTO p_jumlah
  FROM tbl_reservasi
  WHERE PetugasPenjual LIKE p_userid
  AND (DATE(WaktuPesan) BETWEEN p_tgl_awal AND p_tgl_akhir)
  AND (CetakTiket=1 )
  AND Jenispembayaran=p_jenis_pembayaran
  AND FlagBatal!=1;

  RETURN p_jumlah;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_laporan_user_get_total_discount`(
  p_userid INTEGER,
  p_tgl_awal DATE,
  p_tgl_akhir DATE) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;


  SELECT IF(SUM(Discount) IS NOT NULL,SUM(Discount),0) INTO p_jumlah
  FROM tbl_reservasi
  WHERE PetugasPenjual LIKE p_userid
  AND (DATE(WaktuPesan) BETWEEN p_tgl_awal AND p_tgl_akhir)
  AND (CetakTiket=1 OR TglBerangkat>=DATE(NOW()))
  AND FlagBatal!=1;

  RETURN p_jumlah;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_log_user_get_last_login_by_user_id`(p_userid INTEGER) RETURNS int(11)
BEGIN
  DECLARE p_id_log INTEGER;

  SELECT id_log INTO p_id_log
  FROM tbl_log_user
  WHERE
    user_id=p_userid
    AND waktu_logout IS NULL
  ORDER BY waktu_login DESC LIMIT 0,1;

  RETURN p_id_log;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_hitung_frekwensi`(
  p_id_member VARCHAR(25)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT IS_NULL(SUM(FrekwensiBerangkat),0) INTO p_jum_data
  FROM tbl_member_frekwensi_berangkat
  WHERE IdMember=p_id_member ;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_hitung_frekwensi_by_tanggal`(
  p_id_member VARCHAR(25),p_tgl_awal DATE, p_tgl_akhir DATE) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT IS_NULL(SUM(FrekwensiBerangkat),0) INTO p_jum_data
  FROM tbl_member_frekwensi_berangkat
  WHERE IdMember=p_id_member
    AND  (LEFT(TglBerangkat,7) BETWEEN LEFT(p_tgl_awal,7) AND LEFT(p_tgl_akhir,7));

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_periksa_duplikasi`(
  p_id_member VARCHAR(25),p_email VARCHAR(30),p_handphone VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdMember) INTO p_jum_data
  FROM tbl_md_member
  WHERE IdMember=p_id_member OR Email=p_email OR handphone=p_handphone; 

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_paket_periksa_duplikasi`(p_no_tiket VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT

    COUNT(NoTiket) INTO p_jum_data
  FROM tbl_paket
  WHERE NoTiket=p_kode;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_pelanggan_periksa_duplikasi`(p_no_telp VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdPelanggan) INTO p_jum_data
  FROM tbl_pelanggan
  WHERE NoTelp=p_no_telp;

  RETURN p_jum_data;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_pengumuman_periksa_duplikasi`(p_kode VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdPengumuman) INTO p_jum_data
  FROM tbl_pengumuman
  WHERE KodePengumuman=p_kode;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_promo_periksa_duplikasi`(
  p_kode VARCHAR(50),p_asal VARCHAR(20),p_tujuan VARCHAR(20),
  p_berlaku_mula DATETIME,p_berlaku_akhir DATETIME,p_target_promo INT,
  p_level_promo INT) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;


  SELECT
    COUNT(KodePromo) INTO p_jum_data
  FROM tbl_md_promo
  WHERE KodePromo=p_kode
    OR (
      ((p_berlaku_mula BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir))
        OR (p_berlaku_akhir BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir)))
    AND ((KodeCabangAsal LIKE CONCAT(p_asal,'%') AND KodeCabangTujuan LIKE CONCAT(p_tujuan,'%'))
         OR (KodeCabangAsal LIKE CONCAT(p_tujuan,'%') AND KodeCabangTujuan LIKE CONCAT(p_asal,'%')))
    AND LevelPromo=p_level_promo
    AND (FlagTargetPromo=0 OR FlagTargetPromo=p_target_promo)
    );

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_cabang_get_name_by_kode`(p_kode VARCHAR(50)) RETURNS varchar(100) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(100);
  
    SELECT nama INTO p_nama FROM tbl_md_cabang WHERE KodeCabang=p_kode;
  
    RETURN p_nama;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_get_kode_booking_by_no_tiket`(p_no_tiket VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kode_booking VARCHAR(50);

  SELECT KodeBooking INTO p_kode_booking
  FROM tbl_reservasi
  WHERE NoTiket=p_no_tiket;

  RETURN p_kode_booking;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_session_time_selisih`(
  p_session_time DATETIME) RETURNS int(11)
BEGIN
  DECLARE p_selisih INTEGER;

  SELECT
   HOUR(TIMEDIFF(p_session_time,NOW()))*3600 + MINUTE(TIMEDIFF(p_session_time,NOW()))*60 + SECOND(TIMEDIFF(p_session_time,NOW())) INTO p_selisih;

  RETURN p_selisih;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_get_insentif`(
  p_kode VARCHAR(20),p_tgl_awal DATE,p_tgl_akhir DATE) RETURNS double
BEGIN
  DECLARE p_return DOUBLE;

  SELECT SUM(InsentifSopir) INTO p_return
  FROM tbl_spj
  WHERE KodeDriver=p_kode
    AND (TglBerangkat BETWEEN p_tgl_awal AND p_tgl_akhir);

  RETURN p_return;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_get_nama_by_id`(p_kode VARCHAR(20)) RETURNS varchar(150) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(150);

  SELECT Nama INTO p_nama
  FROM tbl_md_sopir
  WHERE KodeSopir=p_kode;

  RETURN p_nama;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeSopir) INTO p_jum_data
  FROM tbl_md_sopir
  WHERE KodeSopir=p_kode;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_get_jumlah_pengumuman_baru`(p_userid INTEGER) RETURNS int(11)
BEGIN
  DECLARE p_jumlah INT;

  SELECT jumlah_pengumuman_baru INTO p_jumlah
  FROM tbl_user
  WHERE user_id=p_userid;

  RETURN p_jumlah;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_get_nama_by_userid`(p_userid INTEGER) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(50);

  SELECT nama INTO p_nama
  FROM tbl_user
  WHERE user_id=p_userid;

  RETURN p_nama;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_periksa_duplikasi`(p_username VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(user_id) INTO p_jum_data
  FROM tbl_user
  WHERE username=p_username;

  RETURN p_jum_data;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `IS_NULL`(p_input DOUBLE,p_pengganti DOUBLE) RETURNS double
BEGIN
  DECLARE p_output DOUBLE;

  SELECT IF(!ISNULL(p_input),p_input,p_pengganti) INTO p_output;
  
  RETURN p_output;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ba_bop`
--

CREATE TABLE IF NOT EXISTS `tbl_ba_bop` (
  `Id` int(11) NOT NULL,
  `KodeBA` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuTransaksi` datetime DEFAULT NULL,
  `JenisBiaya` int(2) DEFAULT NULL,
  `Jumlah` double DEFAULT NULL,
  `NoSPJ` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPembuat` int(11) DEFAULT NULL,
  `NamaPembuat` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IsRelease` int(1) DEFAULT '0',
  `IdReleaser` int(11) DEFAULT NULL,
  `NamaReleaser` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuRelease` datetime DEFAULT NULL,
  `CabangRelease` varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ba_check`
--

CREATE TABLE IF NOT EXISTS `tbl_ba_check` (
  `NoBA` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `TglBA` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeJurusan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahPenumpangCheck` int(11) DEFAULT NULL,
  `JumlahPaketCheck` int(11) DEFAULT NULL,
  `IdChecker` int(11) DEFAULT NULL,
  `NamaChecker` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `CatatanTambahanCheck` text COLLATE latin1_general_ci,
  `IsCheck` int(1) DEFAULT NULL,
  `isCetakVoucherBBM` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_biaya_insentif_sopir`
--

CREATE TABLE IF NOT EXISTS `tbl_biaya_insentif_sopir` (
  `Id` int(11) NOT NULL,
  `IdPenjadwalan` int(11) DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeJadwal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeKendaraan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPembuat` int(11) DEFAULT NULL,
  `NamaPembuat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuat` datetime DEFAULT NULL,
  `IdApprover` int(11) DEFAULT NULL,
  `NamaApprover` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuApprove` datetime DEFAULT NULL,
  `NominalInsentif` double DEFAULT NULL,
  `IdReleaser` int(11) DEFAULT NULL,
  `NamaReleaser` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuRelease` datetime DEFAULT NULL,
  `CabangRelease` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IsRelease` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_biaya_op`
--

CREATE TABLE IF NOT EXISTS `tbl_biaya_op` (
  `IDBiayaOP` int(10) unsigned NOT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkun` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagJenisBiaya` int(1) DEFAULT NULL,
  `TglTransaksi` date DEFAULT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Jumlah` double DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPetugas` int(10) unsigned DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeCabang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCatat` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deposit_log_topup`
--

CREATE TABLE IF NOT EXISTS `tbl_deposit_log_topup` (
  `ID` int(10) unsigned NOT NULL,
  `KodeReferensi` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `Jumlah` double NOT NULL DEFAULT '0',
  `OTP` varchar(6) COLLATE latin1_general_ci NOT NULL COMMENT 'menyimpan kode ',
  `IsVerified` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'jika OTP benar, akan diverifikasi',
  `WaktuVerifikasi` datetime DEFAULT NULL,
  `PetugasTopUp` int(10) unsigned NOT NULL,
  `PetugasVerifikasi` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deposit_log_trx`
--

CREATE TABLE IF NOT EXISTS `tbl_deposit_log_trx` (
  `ID` int(10) unsigned NOT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `IsDebit` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'debit=pengurangan saldo, kredit=penambahan saldo',
  `Keterangan` text COLLATE latin1_general_ci,
  `Jumlah` double NOT NULL DEFAULT '0',
  `Saldo` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_discount`
--

CREATE TABLE IF NOT EXISTS `tbl_jenis_discount` (
  `IdDiscount` int(10) unsigned NOT NULL,
  `KodeDiscount` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDiscount` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `JumlahDiscount` double NOT NULL,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `FlagLuarKota` int(1) DEFAULT '1',
  `IsHargaTetap` int(1) DEFAULT '0' COMMENT 'jika diset=1 maka nilai harganya bukan diskon melainkan harga tetap sesuai dengan field jenisdiskon'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kasbon_sopir`
--

CREATE TABLE IF NOT EXISTS `tbl_kasbon_sopir` (
  `ID` int(10) unsigned NOT NULL,
  `KodeAkun` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `TglTransaksi` date NOT NULL,
  `KodeSopir` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `Jumlah` double NOT NULL,
  `IdKasir` int(10) unsigned NOT NULL,
  `DiberikanDiCabang` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `IsBatal` int(1) unsigned NOT NULL DEFAULT '0',
  `Pembatal` int(10) unsigned NOT NULL,
  `WaktuBatal` datetime NOT NULL,
  `WaktuCatatTransaksi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kendaraan_administratif`
--

CREATE TABLE IF NOT EXISTS `tbl_kendaraan_administratif` (
  `id_kendaraan_administratif` int(10) unsigned NOT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TanggalAktif` date DEFAULT NULL,
  `StatusKepemilikan` int(1) DEFAULT NULL,
  `Keterangan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglValidSTNK` date DEFAULT NULL,
  `TglValidKIR` date DEFAULT NULL,
  `BiayaLeasing` double DEFAULT NULL,
  `NamaLeasing` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaAngsuran` double DEFAULT NULL,
  `AngsuranKe` int(2) DEFAULT NULL,
  `JumlahAngsuran` int(2) DEFAULT NULL,
  `JatuhTempoAngsuran` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_cetakulang_voucher_bbm`
--

CREATE TABLE IF NOT EXISTS `tbl_log_cetakulang_voucher_bbm` (
  `Id` int(11) NOT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahRupiah` double DEFAULT '0',
  `WaktuCetak` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `UserCetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `UserApproval` int(11) DEFAULT NULL,
  `NamaUserApproval` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_cetak_manifest`
--

CREATE TABLE IF NOT EXISTS `tbl_log_cetak_manifest` (
  `Id` int(11) NOT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursiDisediakan` int(2) DEFAULT NULL,
  `JumlahPenumpang` int(2) DEFAULT '0',
  `JumlahPaket` int(3) DEFAULT NULL,
  `JumlahPaxPaket` int(3) DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalOmzetPaket` double DEFAULT NULL,
  `IsSubJadwal` int(1) DEFAULT '0',
  `WaktuCetak` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `UserCetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_cetak_tiket`
--

CREATE TABLE IF NOT EXISTS `tbl_log_cetak_tiket` (
  `Id` int(11) NOT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenumpang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT NULL,
  `IsCetakSPJ` int(1) DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  `UserPencetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `UserOtorisasi` int(11) DEFAULT NULL,
  `NamaUserOtorisasi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_finpay`
--

CREATE TABLE IF NOT EXISTS `tbl_log_finpay` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `module` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `action` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `url` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `data` text COLLATE latin1_general_ci,
  `user_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_koreksi_disc`
--

CREATE TABLE IF NOT EXISTS `tbl_log_koreksi_disc` (
  `IdLog` int(10) unsigned NOT NULL,
  `NoTiket` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabang` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) unsigned DEFAULT NULL,
  `HargaTiket` double DEFAULT NULL,
  `Charge` double DEFAULT NULL,
  `SubTotal` double DEFAULT NULL,
  `DiscountMula` double DEFAULT NULL,
  `DiscountBaru` double DEFAULT NULL,
  `Total` double DEFAULT NULL,
  `PetugasPenjual` int(10) unsigned DEFAULT NULL,
  `CetakTiket` int(1) unsigned DEFAULT NULL,
  `PetugasCetakTiket` int(10) unsigned DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `JenisDiscountMula` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscountBaru` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) unsigned DEFAULT NULL,
  `FlagBatal` int(1) unsigned DEFAULT NULL,
  `JenisPenumpangMula` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpangBaru` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPengkoreksi` int(10) unsigned DEFAULT NULL,
  `PetugasOtorisasi` int(10) unsigned DEFAULT NULL,
  `WaktuKoreksi` datetime DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_mutasi`
--

CREATE TABLE IF NOT EXISTS `tbl_log_mutasi` (
  `Id` int(11) NOT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBookingSebelumnya` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenumpang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkatSebelumnya` date DEFAULT NULL,
  `JamBerangkatSebelumnya` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwalSebelumnya` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusanSebelumnya` int(11) DEFAULT NULL,
  `NomorKursiSebelumnya` int(2) DEFAULT NULL,
  `HargaTiketSebelumnya` double DEFAULT NULL,
  `KodeBookingMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkatMutasi` date DEFAULT NULL,
  `JamBerangkatMutasi` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwalMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NomorKursiMutasi` int(2) DEFAULT NULL,
  `IdJurusanMutasi` int(11) DEFAULT NULL,
  `HargaTiketMutasi` double DEFAULT NULL,
  `IsCetakTiket` int(1) DEFAULT NULL,
  `IsCetakSPJ` int(1) DEFAULT NULL,
  `Charge` double DEFAULT NULL,
  `TotalDiskon` double DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  `UserMutasi` int(11) DEFAULT NULL,
  `NamaUserMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_sms`
--

CREATE TABLE IF NOT EXISTS `tbl_log_sms` (
  `IdLogSms` int(10) unsigned NOT NULL,
  `NoTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenerima` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuKirim` datetime DEFAULT NULL,
  `JumlahPesan` int(2) DEFAULT NULL,
  `FlagTipePengiriman` int(2) DEFAULT NULL,
  `KodeReferensi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_token_sms`
--

CREATE TABLE IF NOT EXISTS `tbl_log_token_sms` (
  `IdLog` int(11) NOT NULL,
  `WaktuCatat` datetime NOT NULL,
  `JumlahSisaToken` double NOT NULL DEFAULT '0',
  `JumlahSMSDikirim` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_user`
--

CREATE TABLE IF NOT EXISTS `tbl_log_user` (
  `id_log` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `waktu_login` datetime DEFAULT NULL,
  `waktu_logout` datetime DEFAULT NULL,
  `user_ip` varchar(100) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_log_user`
--

INSERT INTO `tbl_log_user` (`id_log`, `user_id`, `waktu_login`, `waktu_logout`, `user_ip`) VALUES
(164, 1, '2015-04-23 15:19:20', NULL, '7f000001'),
(165, 1, '2016-02-22 14:40:59', '2016-02-22 14:41:04', '00000000'),
(166, 1, '2016-02-22 14:41:25', '2016-02-22 14:45:42', '00000000'),
(167, 1, '2016-02-22 14:47:00', NULL, '00000000'),
(168, 1, '2016-02-23 05:27:13', '2016-02-23 08:35:10', '00000000'),
(169, 1, '2016-02-23 08:35:16', '2016-02-23 08:57:37', '00000000'),
(170, 4, '2016-02-23 08:57:43', '2016-02-23 08:58:55', '00000000'),
(171, 1, '2016-02-23 08:59:01', '2016-02-23 09:01:19', '00000000'),
(172, 8, '2016-02-23 09:01:26', '2016-02-23 09:01:36', '00000000'),
(173, 1, '2016-02-23 09:04:02', '2016-02-23 10:16:51', '00000000'),
(174, 1, '2016-02-23 11:56:09', NULL, 'c0a80364'),
(175, 1, '2016-02-23 11:57:14', '2016-02-23 14:03:42', 'c0a80364'),
(176, 1, '2016-02-23 14:11:39', '2016-02-23 14:12:34', 'c0a80364'),
(177, 1, '2016-02-23 14:13:13', '2016-02-23 14:14:20', 'c0a80364'),
(178, 1, '2016-02-23 14:15:04', NULL, 'c0a80364'),
(179, 1, '2016-02-23 14:36:45', NULL, '00000000'),
(180, 1, '2016-02-23 14:38:42', NULL, 'c0a80364'),
(181, 8, '2016-02-23 15:07:33', NULL, '00000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mac_address`
--

CREATE TABLE IF NOT EXISTS `tbl_mac_address` (
  `Id` int(10) unsigned NOT NULL,
  `MacAddress` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NamaKomputer` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `AddBy` int(10) unsigned NOT NULL,
  `AddByName` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `WaktuTambah` datetime NOT NULL,
  `IsAktif` int(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_cabang`
--

CREATE TABLE IF NOT EXISTS `tbl_md_cabang` (
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Kota` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Fax` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `FlagAgen` int(1) DEFAULT NULL,
  `IsPusat` int(1) NOT NULL DEFAULT '0',
  `COAAR` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COASalesTiket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COAMarkDownTiket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COASalesPaket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COAMarkDownPaket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COACash` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COABank` varchar(30) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_cabang`
--

INSERT INTO `tbl_md_cabang` (`KodeCabang`, `Nama`, `Alamat`, `Kota`, `Telp`, `Fax`, `FlagAgen`, `IsPusat`, `COAAR`, `COASalesTiket`, `COAMarkDownTiket`, `COASalesPaket`, `COAMarkDownPaket`, `COACash`, `COABank`) VALUES
('BAN', 'haiTRANS Bandung', 'Jl. Tamansari No. 66 Kota Bandung', 'BANDUNG', '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('DEP', 'haiTRANS Depok', 'Jl. Siliwangi No. 15 Kota Depok', 'DEPOK', '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('KAR', 'haiTRANS Karawang', '', 'KARAWANG', '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('PUR', 'haiTRANS Purwakarta', '', 'PURWAKARTA', '', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_harga_promo`
--

CREATE TABLE IF NOT EXISTS `tbl_md_harga_promo` (
  `KodeHargaPromo` int(10) unsigned NOT NULL,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `BerlakuMula` date NOT NULL,
  `BerlakuAkhir` date NOT NULL,
  `HargaPromo` double NOT NULL,
  `FlagAktif` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_jadwal`
--

CREATE TABLE IF NOT EXISTS `tbl_md_jadwal` (
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(11) NOT NULL,
  `KodeCabangAsal` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JamBerangkat` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL,
  `FlagSubJadwal` int(1) DEFAULT '0',
  `KodeJadwalUtama` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaSopir1` double DEFAULT NULL,
  `BiayaSopir2` double DEFAULT NULL,
  `BiayaSopir3` double DEFAULT NULL,
  `IsBiayaSopirKumulatif` int(1) DEFAULT '0',
  `BiayaTol` double DEFAULT NULL,
  `BiayaParkir` double DEFAULT NULL,
  `BiayaBBM` double DEFAULT NULL,
  `IsBBMVoucher` int(1) DEFAULT '0',
  `Via` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `IsOnline` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_jadwal`
--

INSERT INTO `tbl_md_jadwal` (`KodeJadwal`, `IdJurusan`, `KodeCabangAsal`, `KodeCabangTujuan`, `JamBerangkat`, `IdLayout`, `JumlahKursi`, `FlagSubJadwal`, `KodeJadwalUtama`, `BiayaSopir1`, `BiayaSopir2`, `BiayaSopir3`, `IsBiayaSopirKumulatif`, `BiayaTol`, `BiayaParkir`, `BiayaBBM`, `IsBBMVoucher`, `Via`, `FlagAktif`, `IsOnline`) VALUES
('BAN-DEP05', 1, 'BAN', 'DEP', '05:00', '13', 13, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP07', 1, 'BAN', 'DEP', '07:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP09', 1, 'BAN', 'DEP', '09:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP11', 1, 'BAN', 'DEP', '11:00', '13', 13, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP13', 1, 'BAN', 'DEP', '13:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP15', 1, 'BAN', 'DEP', '15:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP17', 1, 'BAN', 'DEP', '17:00', '13', 13, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP19', 1, 'BAN', 'DEP', '19:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-DEP20', 1, 'BAN', 'DEP', '20:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-KAR06', 3, 'BAN', 'KAR', '06:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-KAR08', 3, 'BAN', 'KAR', '08:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-KAR10', 3, 'BAN', 'KAR', '10:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-KAR12', 3, 'BAN', 'KAR', '12:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-KAR14', 3, 'BAN', 'KAR', '14:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-KAR16', 3, 'BAN', 'KAR', '16:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-KAR18', 3, 'BAN', 'KAR', '18:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-KAR20', 3, 'BAN', 'KAR', '20:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0),
('BAN-PUR06', 4, 'BAN', 'PUR', '06:00', '14', 14, 1, 'BAN-KAR06', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-PUR08', 4, 'BAN', 'PUR', '08:00', '14', 14, 1, 'BAN-KAR08', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-PUR10', 4, 'BAN', 'PUR', '10:00', '14', 14, 1, 'BAN-KAR10', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-PUR12', 4, 'BAN', 'PUR', '12:00', '14', 14, 1, 'BAN-KAR12', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-PUR14', 4, 'BAN', 'PUR', '14:00', '14', 14, 1, 'BAN-KAR14', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-PUR16', 3, 'BAN', 'KAR', '16:00', '14', 14, 1, 'BAN-KAR16', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-PUR18', 3, 'BAN', 'KAR', '18:00', '14', 14, 1, 'BAN-KAR18', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('BAN-PUR20', 4, 'BAN', 'PUR', '20:00', '14', 14, 1, 'BAN-KAR20', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN011', 2, 'DEP', 'BAN', '11:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN013', 2, 'DEP', 'BAN', '13:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN015', 2, 'DEP', 'BAN', '15:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN017', 2, 'DEP', 'BAN', '17:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN019', 2, 'DEP', 'BAN', '19:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN020', 2, 'DEP', 'BAN', '20:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN05', 2, 'DEP', 'BAN', '05:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN07', 2, 'DEP', 'BAN', '07:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0),
('DEP-BAN09', 2, 'DEP', 'BAN', '09:00', '14', 14, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_jurusan`
--

CREATE TABLE IF NOT EXISTS `tbl_md_jurusan` (
  `IdJurusan` int(11) NOT NULL,
  `KodeJurusan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `HargaTiket` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaTiketTuslah` double DEFAULT NULL,
  `FlagTiketTuslah` int(1) DEFAULT '0',
  `KodeAkunPendapatanPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunPendapatanPaket` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunCharge` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunBiayaSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaSopir` double DEFAULT '0',
  `KodeAkunBiayaTol` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaTol` double DEFAULT '0',
  `KodeAkunBiayaParkir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaParkir` double DEFAULT '0',
  `KodeAkunBiayaBBM` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaBBM` double DEFAULT NULL,
  `LiterBBM` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPenumpangSopir` double DEFAULT '0',
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `KodeAkunKomisiPaketSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketSopir` double DEFAULT '0',
  `KodeAkunKomisiPaketCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketCSO` double DEFAULT '0',
  `FlagAktif` int(1) DEFAULT '1',
  `FlagLuarKota` int(1) DEFAULT '1',
  `HargaPaket1KiloPertama` double DEFAULT '0',
  `HargaPaket1KiloBerikut` double DEFAULT '0',
  `HargaPaket2KiloPertama` double DEFAULT '0',
  `HargaPaket2KiloBerikut` double DEFAULT '0',
  `HargaPaket3KiloPertama` double DEFAULT '0',
  `HargaPaket3KiloBerikut` double DEFAULT '0',
  `HargaPaket4KiloPertama` double DEFAULT '0',
  `HargaPaket4KiloBerikut` double DEFAULT '0',
  `HargaPaket5KiloPertama` double DEFAULT '0',
  `HargaPaket5KiloBerikut` double DEFAULT '0',
  `HargaPaket6KiloPertama` double NOT NULL DEFAULT '0',
  `HargaPaket6KiloBerikut` double NOT NULL DEFAULT '0',
  `FlagOperasionalJurusan` int(1) unsigned DEFAULT '0' COMMENT '0=reguler; 1=paket; 2=paket dan reguler;3=non-reguler;4=charter',
  `IsBiayaSopirKumulatif` int(1) unsigned DEFAULT '0',
  `IsVoucherBBM` int(1) NOT NULL DEFAULT '0' COMMENT '0=cash; 1=voucher BBM',
  `IsOnline` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_jurusan`
--

INSERT INTO `tbl_md_jurusan` (`IdJurusan`, `KodeJurusan`, `KodeCabangAsal`, `KodeCabangTujuan`, `HargaTiket`, `HargaTiketTuslah`, `FlagTiketTuslah`, `KodeAkunPendapatanPenumpang`, `KodeAkunPendapatanPaket`, `KodeAkunCharge`, `KodeAkunBiayaSopir`, `BiayaSopir`, `KodeAkunBiayaTol`, `BiayaTol`, `KodeAkunBiayaParkir`, `BiayaParkir`, `KodeAkunBiayaBBM`, `BiayaBBM`, `LiterBBM`, `KodeAkunKomisiPenumpangSopir`, `KomisiPenumpangSopir`, `KodeAkunKomisiPenumpangCSO`, `KomisiPenumpangCSO`, `KodeAkunKomisiPaketSopir`, `KomisiPaketSopir`, `KodeAkunKomisiPaketCSO`, `KomisiPaketCSO`, `FlagAktif`, `FlagLuarKota`, `HargaPaket1KiloPertama`, `HargaPaket1KiloBerikut`, `HargaPaket2KiloPertama`, `HargaPaket2KiloBerikut`, `HargaPaket3KiloPertama`, `HargaPaket3KiloBerikut`, `HargaPaket4KiloPertama`, `HargaPaket4KiloBerikut`, `HargaPaket5KiloPertama`, `HargaPaket5KiloBerikut`, `HargaPaket6KiloPertama`, `HargaPaket6KiloBerikut`, `FlagOperasionalJurusan`, `IsBiayaSopirKumulatif`, `IsVoucherBBM`, `IsOnline`) VALUES
(1, 'BAN-DEP', 'BAN', 'DEP', '13=80000;14=80000;', 13, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '13=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 'DEP-BAN', 'DEP', 'BAN', '13=80000;14=80000;', 13, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '13=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 'BAN-KAR', 'BAN', 'KAR', '13=60000;14=60000;', 13, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '13=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 'BAN-PUR', 'BAN', 'PUR', '13=30000;14=30000;', 13, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, '13=;14=;', NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_kendaraan`
--

CREATE TABLE IF NOT EXISTS `tbl_md_kendaraan` (
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Jenis` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Merek` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Tahun` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Warna` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL,
  `KodeSopir1` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeSopir2` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoSTNK` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoBPKB` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoRangka` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoMesin` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KilometerAkhir` double DEFAULT '0',
  `Remark` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `IsCargo` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_kendaraan`
--

INSERT INTO `tbl_md_kendaraan` (`KodeKendaraan`, `KodeCabang`, `NoPolisi`, `Jenis`, `Merek`, `Tahun`, `Warna`, `IdLayout`, `JumlahKursi`, `KodeSopir1`, `KodeSopir2`, `NoSTNK`, `NoBPKB`, `NoRangka`, `NoMesin`, `KilometerAkhir`, `Remark`, `FlagAktif`, `IsCargo`) VALUES
('hai-001', 'BAN', 'D7656AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP2', 'SOP2', '', '', '', '', 0, NULL, 1, 0),
('hai-0010', 'BAN', 'D7702AH', 'ELF', 'ISUZU', '2010', '', '14', 14, 'SOP2', 'SOP2', '', '', '', '', 0, NULL, 1, 0),
('hai-0011', 'BAN', 'D7988AN', 'ELF', 'ISUZU', '2010', '', '14', 14, 'SOP4', 'SOP4', '', '', '', '', 0, NULL, 1, 0),
('hai-0012', 'BAN', 'D7704AO', 'ELF', 'ISUZU', '2010', '', '14', 14, 'SOP5', 'SOP5', '', '', '', '', 0, NULL, 1, 0),
('hai-0013', 'BAN', 'D7705AO', 'ELF', 'ISUZU', '2010', '', '14', 14, 'SOP5', 'SOP5', '', '', '', '', 0, NULL, 1, 0),
('hai-002', 'BAN', 'D7657AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP1', 'SOP1', '', '', '', '', 0, NULL, 1, 0),
('hai-003', 'BAN', 'D7658AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP3', 'SOP3', '', '', '', '', 0, NULL, 1, 0),
('hai-004', 'BAN', 'D7659AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP1', 'SOP1', '', '', '', '', 0, NULL, 1, 0),
('hai-005', 'BAN', 'D7694AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP2', 'SOP2', '', '', '', '', 0, NULL, 1, 0),
('hai-006', 'BAN', 'D7696AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP4', 'SOP4', '', '', '', '', 0, NULL, 1, 0),
('hai-007', 'BAN', 'D7697AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP4', 'SOP4', '', '', '', '', 0, NULL, 1, 0),
('hai-008', 'BAN', 'D7698AH', 'ELF', 'ISUZU', '2010', '', '13', 13, 'SOP2', 'SOP2', '', '', '', '', 0, NULL, 1, 0),
('hai-009', 'BAN', 'D7701AH', 'ELF', 'ISUZU', '2010', '', '14', 14, 'SOP1', 'SOP1', '', '', '', '', 0, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_kendaraan_layout`
--

CREATE TABLE IF NOT EXISTS `tbl_md_kendaraan_layout` (
  `Id` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `LayoutBody` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_kendaraan_layout`
--

INSERT INTO `tbl_md_kendaraan_layout` (`Id`, `LayoutBody`) VALUES
('13', 13),
('14', 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_kota`
--

CREATE TABLE IF NOT EXISTS `tbl_md_kota` (
  `KodeKota` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `NamaKota` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_kota`
--

INSERT INTO `tbl_md_kota` (`KodeKota`, `NamaKota`) VALUES
('BDG', 'BANDUNG'),
('DPK', 'DEPOK'),
('KRW', 'KARAWANG'),
('PRW', 'PURWAKARTA');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_libur`
--

CREATE TABLE IF NOT EXISTS `tbl_md_libur` (
  `Id` int(11) NOT NULL,
  `TglLibur` date DEFAULT NULL,
  `KeteranganLibur` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `DiubahOleh` text COLLATE latin1_general_ci,
  `WaktuUbah` text COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_member`
--

CREATE TABLE IF NOT EXISTS `tbl_md_member` (
  `IdMember` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JenisKelamin` int(1) NOT NULL DEFAULT '0',
  `KategoriMember` int(1) DEFAULT NULL,
  `TempatLahir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `NoKTP` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglRegistrasi` date DEFAULT NULL,
  `Alamat` text COLLATE latin1_general_ci,
  `Kota` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodePos` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Handphone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `Pekerjaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Point` double DEFAULT NULL,
  `ExpiredDate` date DEFAULT NULL,
  `WaktuTransaksiTerakhir` datetime DEFAULT NULL,
  `IdKartu` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSeriKartu` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KataSandi` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) DEFAULT '1',
  `CabangDaftar` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `SaldoDeposit` double DEFAULT '0',
  `Signature` varchar(100) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_paket_daftar_layanan`
--

CREATE TABLE IF NOT EXISTS `tbl_md_paket_daftar_layanan` (
  `Id` int(10) unsigned NOT NULL,
  `KodeLayanan` char(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaLayanan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaKiloPertama` double DEFAULT NULL,
  `HargaKiloBerikutnya` double DEFAULT NULL,
  `FlagAktif` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_plan_asuransi`
--

CREATE TABLE IF NOT EXISTS `tbl_md_plan_asuransi` (
  `IdPlanAsuransi` int(10) unsigned NOT NULL,
  `NamaPlan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `BesarPremi` double DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_promo`
--

CREATE TABLE IF NOT EXISTS `tbl_md_promo` (
  `KodePromo` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `LevelPromo` int(2) NOT NULL,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `BerlakuMula` date NOT NULL,
  `BerlakuAkhir` date NOT NULL,
  `JumlahPenumpangMinimum` int(2) DEFAULT NULL,
  `JumlahPoint` double NOT NULL,
  `JumlahDiscount` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `FlagDiscount` int(1) NOT NULL DEFAULT '0',
  `FlagAktif` int(1) DEFAULT NULL,
  `FlagTargetPromo` tinyint(3) unsigned DEFAULT NULL,
  `JamMulai` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JamAkhir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPromo` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisMobil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NomorKursi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `BerlakuHari` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `IsPromoPP` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_promo`
--

INSERT INTO `tbl_md_promo` (`KodePromo`, `LevelPromo`, `KodeCabangAsal`, `KodeCabangTujuan`, `KodeJadwal`, `BerlakuMula`, `BerlakuAkhir`, `JumlahPenumpangMinimum`, `JumlahPoint`, `JumlahDiscount`, `FlagDiscount`, `FlagAktif`, `FlagTargetPromo`, `JamMulai`, `JamAkhir`, `NamaPromo`, `JenisMobil`, `NomorKursi`, `BerlakuHari`, `IsPromoPP`) VALUES
('ssyy', 1, 'BAN', 'DEP', '', '2016-02-24', '2016-02-29', 0, 0, '0,5', 0, 1, 0, '07:00', '23:00', 'ssyyt', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_sopir`
--

CREATE TABLE IF NOT EXISTS `tbl_md_sopir` (
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `HP` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Alamat` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoSIM` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `FlagAktif` int(1) DEFAULT '1',
  `IsCargo` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_md_sopir`
--

INSERT INTO `tbl_md_sopir` (`KodeSopir`, `Nama`, `HP`, `Alamat`, `NoSIM`, `FlagAktif`, `IsCargo`) VALUES
('SOP1', 'RENDI RARDANI', '', '', '', 1, 0),
('SOP2', 'MUHAMMAD RIDWAN', '', '', '', 1, 0),
('SOP3', 'RIKI', '', '', '', 1, 0),
('SOP4', 'YUDI KUSMIADI', '', '', '', 1, 0),
('SOP5', 'SOPYAN', '', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_deposit_topup_log`
--

CREATE TABLE IF NOT EXISTS `tbl_member_deposit_topup_log` (
  `ID` int(10) unsigned NOT NULL,
  `IdMember` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeReferensi` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `Jumlah` double NOT NULL DEFAULT '0',
  `PetugasTopUp` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_deposit_trx_log`
--

CREATE TABLE IF NOT EXISTS `tbl_member_deposit_trx_log` (
  `ID` int(10) unsigned NOT NULL,
  `IdMember` int(11) DEFAULT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `IsDebit` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'debit=pengurangan saldo, kredit=penambahan saldo',
  `Keterangan` text COLLATE latin1_general_ci,
  `Jumlah` double NOT NULL DEFAULT '0',
  `Saldo` double NOT NULL DEFAULT '0',
  `Signature` varchar(100) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_frekwensi_berangkat`
--

CREATE TABLE IF NOT EXISTS `tbl_member_frekwensi_berangkat` (
  `IdFrekwensi` int(10) unsigned NOT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `FrekwensiBerangkat` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_mutasi_point`
--

CREATE TABLE IF NOT EXISTS `tbl_member_mutasi_point` (
  `IdMutasiPoint` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Referensi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `JenisMutasi` int(1) DEFAULT NULL,
  `JumlahPoint` double DEFAULT NULL,
  `Operator` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_promo_point`
--

CREATE TABLE IF NOT EXISTS `tbl_member_promo_point` (
  `KodeProduk` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NamaProduk` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JumlahPointTukar` double NOT NULL,
  `FlagAktif` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket`
--

CREATE TABLE IF NOT EXISTS `tbl_paket` (
  `NoTiket` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPengirim` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPengirim` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengirim` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NamaPenerima` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPenerima` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenerima` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPaket` double DEFAULT NULL,
  `Diskon` double NOT NULL DEFAULT '0',
  `TotalBayar` double NOT NULL DEFAULT '0',
  `Dimensi` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKoli` double DEFAULT NULL,
  `Berat` double DEFAULT NULL,
  `KeteranganPaket` text COLLATE latin1_general_ci,
  `InstruksiKhusus` text COLLATE latin1_general_ci,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPenjual` int(11) DEFAULT NULL,
  `KomisiPaketSopir` double DEFAULT NULL,
  `KodeAkunKomisiPaketSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketCSO` double DEFAULT NULL,
  `KodeAkunKomisiPaketCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `FlagBatal` int(1) DEFAULT '0',
  `JenisPembayaran` int(1) DEFAULT NULL,
  `CaraPembayaran` int(1) DEFAULT NULL,
  `CetakTiket` int(1) DEFAULT '0',
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `PetugasPembatalan` int(10) unsigned DEFAULT NULL,
  `StatusDiambil` int(1) DEFAULT '0',
  `NamaPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoKTPPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPengambilan` datetime DEFAULT NULL,
  `PetugasPemberi` int(10) unsigned DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(10) unsigned DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisBarang` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisLayanan` char(4) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMutasi` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuMutasi` text COLLATE latin1_general_ci NOT NULL,
  `UserPemutasi` text COLLATE latin1_general_ci NOT NULL,
  `IsSampai` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuSampai` datetime DEFAULT NULL,
  `UserCheckIn` int(11) DEFAULT NULL,
  `KodePelanggan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCheck` date DEFAULT NULL,
  `IdChecker` int(11) DEFAULT NULL,
  `IsCheck` int(1) DEFAULT '0',
  `LogMutasi` text COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket_cargo`
--

CREATE TABLE IF NOT EXISTS `tbl_paket_cargo` (
  `Id` int(11) NOT NULL,
  `NoAWB` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPengirim` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengirim` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPengirim` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenerima` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenerima` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPenerima` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDiterima` date DEFAULT NULL,
  `KodeAsal` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `Asal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeTujuan` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `Tujuan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisBarang` varchar(3) COLLATE latin1_general_ci DEFAULT 'PRC' COMMENT 'PRC=PARCEL; DOK=DOKUMEN',
  `Via` varchar(3) COLLATE latin1_general_ci DEFAULT 'DRT' COMMENT 'DRT=Darat; UDR=Udara; LUT=Laut',
  `Layanan` varchar(3) COLLATE latin1_general_ci DEFAULT 'REG' COMMENT 'URG="Urgent"; REG="Reguler"',
  `Koli` int(2) DEFAULT '0' COMMENT 'Pax',
  `IsVolumetric` int(1) DEFAULT '0' COMMENT '0=perhitungan berdasarkan berat aktual; 1= perhitungan berdasarkan berat voume metric',
  `DimensiPanjang` double DEFAULT '0' COMMENT 'dalam satuan centimeter',
  `DimensiLebar` double DEFAULT '0' COMMENT 'satuan dalam centimeter',
  `DimensiTinggi` double DEFAULT '0' COMMENT 'dalam satuan centimeter',
  `Berat` double DEFAULT '0' COMMENT 'dalam kilogram',
  `LeadTime` varchar(10) COLLATE latin1_general_ci DEFAULT '' COMMENT 'string',
  `BiayaKirim` double DEFAULT '0',
  `BiayaTambahan` double DEFAULT '0',
  `BiayaPacking` double DEFAULT '0',
  `IsAsuransi` int(1) DEFAULT '0' COMMENT '0=tidak pake asuransi; 1=pake asuransi (besarnya asuransi adalah 2 permil dari harga pengakuan)',
  `HargaDiakui` double DEFAULT '0' COMMENT 'harga yang diakui pelanggan utk menghitung asuransi',
  `BiayaAsuransi` double DEFAULT '0',
  `TotalBiaya` double DEFAULT '0',
  `IsTunai` int(1) DEFAULT '1' COMMENT '1=Tunai;  0=kredit',
  `DeskripsiBarang` text COLLATE latin1_general_ci,
  `WaktuCatat` datetime DEFAULT NULL,
  `DicatatOleh` int(11) DEFAULT NULL,
  `NamaPencatat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakAWB` datetime DEFAULT NULL,
  `DicetakOleh` int(11) DEFAULT NULL,
  `NamaPencetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `PoinPickedUp` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUp` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IsPickedUp` int(1) DEFAULT '0' COMMENT '0=belum di pickup; 1=sudah di pickup',
  `WaktuPickedUp` datetime DEFAULT NULL,
  `PetugasPemberi` int(11) DEFAULT NULL,
  `NamaPetugasPemberi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPenerima` int(11) DEFAULT NULL,
  `NamaPetugasPenerima` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `IsBatal` int(1) DEFAULT '0',
  `PetugasBatal` int(11) DEFAULT NULL,
  `NamaPetugasBatal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBatal` datetime DEFAULT NULL,
  `IsMutasi` int(1) DEFAULT '0',
  `TglDiterimaLama` date DEFAULT NULL,
  `PoinPickedUpLama` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUpLama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `PetugasMutasi` int(11) DEFAULT NULL,
  `NamaPetugasMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `LogMutasi` text COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket_cargo_md_harga`
--

CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_md_harga` (
  `Id` int(11) NOT NULL,
  `KodeAsal` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeTujuan` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPerKg` double DEFAULT NULL,
  `LeadTime` varchar(10) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket_cargo_md_kota`
--

CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_md_kota` (
  `KodeKota` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `NamaKota` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeProvinsi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket_cargo_md_provinsi`
--

CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_md_provinsi` (
  `Id` int(11) NOT NULL,
  `NamaProvinsi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paket_cargo_spj`
--

CREATE TABLE IF NOT EXISTS `tbl_paket_cargo_spj` (
  `Id` int(11) NOT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDiterima` date DEFAULT NULL,
  `PoinPickedUp` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUp` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPickedUp` datetime DEFAULT NULL,
  `PetugasPemberi` int(11) DEFAULT NULL,
  `NamaPetugasPemberi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `PetugasCetak` int(11) DEFAULT NULL,
  `NamaPetugasCetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `BodyUnit` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDriver` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahTransaksi` int(3) DEFAULT NULL,
  `JumlahKoli` int(3) DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalBerat` double DEFAULT NULL,
  `PetugasPickedUp` int(11) DEFAULT NULL,
  `NamaPetugasPickedUp` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `LogCetak` text COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pelanggan`
--

CREATE TABLE IF NOT EXISTS `tbl_pelanggan` (
  `IdPelanggan` int(10) unsigned NOT NULL,
  `NoHP` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoTelp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglPertamaTransaksi` date DEFAULT NULL,
  `TglTerakhirTransaksi` date DEFAULT NULL,
  `CSOTerakhir` int(10) unsigned DEFAULT NULL,
  `KodeJurusanTerakhir` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `FrekwensiPergi` double DEFAULT '0',
  `FlagMember` int(1) DEFAULT '0',
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengaturan_parameter`
--

CREATE TABLE IF NOT EXISTS `tbl_pengaturan_parameter` (
  `NamaParameter` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `NilaiParameter` text COLLATE latin1_general_ci NOT NULL,
  `Deskripsi` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_pengaturan_parameter`
--

INSERT INTO `tbl_pengaturan_parameter` (`NamaParameter`, `NilaiParameter`, `Deskripsi`) VALUES
('BBM_SOLAR', '7500', 'harga solar per liter'),
('BBM_SPBU_57', 'SBPU 57', ''),
('BBM_SPBU_BDO', 'SBPU BDO', ''),
('DEPOSIT_MIN', '2000000', 'saldo minimum yang harus tersimpan'),
('DEPOSIT_SALDO', '10000000', 'Saldo Deposit Tiketux'),
('FEE_TIKET', '1500', 'Fee tiket pertama & kedua'),
('KOMISITTX1', '10000', 'Besar Komisi untuk tiketux'),
('PERUSH_ALAMAT', 'alamat', 'Alamat perusahaan'),
('PERUSH_EMAIL', 'email', 'email perusahaan'),
('PERUSH_NAMA', 'HELDA TRAVEL', 'nama perusahaan penyedia jasa transportasi'),
('PERUSH_TELP', '022', 'telp perusahaan'),
('PERUSH_WEB', 'web', 'web perusahaan'),
('PESAN_DITIKET', '', 'Menampilkan pesan di tiket'),
('POLIS_ASURANSI', '1233232323', 'nomor polis induk asuransi'),
('TGL_AKHIR_TUSLAH1', '2013-08-12 ', 'Tanggal Akhir Tuslah Luar Kota'),
('TGL_AKHIR_TUSLAH2', '2013-08-11 ', 'tgl berakhirnya tuslah dalam kota'),
('TGL_MULAI_TUSLAH1', '2013-08-02 ', 'Tanggal Mulai Tuslah Luar Kota'),
('TGL_MULAI_TUSLAH2', '2013-08-05 ', 'tgl mulai tuslah dalam kota');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengaturan_umum`
--

CREATE TABLE IF NOT EXISTS `tbl_pengaturan_umum` (
  `IdPengaturanUmum` int(10) unsigned NOT NULL,
  `PesanSambutan` text COLLATE latin1_general_ci,
  `PesanDiTiket` text COLLATE latin1_general_ci,
  `FeeTiket` double DEFAULT NULL,
  `FeePaket` double DEFAULT NULL,
  `FeeSMS` double DEFAULT NULL,
  `NamaPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `EmailPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WebSitePerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPaketMinimum` double DEFAULT NULL,
  `TglMulaiTuslah` date DEFAULT NULL,
  `TglAkhirTuslah` date DEFAULT NULL,
  `FlagBlokir` int(1) NOT NULL DEFAULT '0',
  `PemblokirUnblokir` int(11) NOT NULL,
  `WaktuBlokirUnblokir` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_pengaturan_umum`
--

INSERT INTO `tbl_pengaturan_umum` (`IdPengaturanUmum`, `PesanSambutan`, `PesanDiTiket`, `FeeTiket`, `FeePaket`, `FeeSMS`, `NamaPerusahaan`, `AlamatPerusahaan`, `TelpPerusahaan`, `EmailPerusahaan`, `WebSitePerusahaan`, `HargaPaketMinimum`, `TglMulaiTuslah`, `TglAkhirTuslah`, `FlagBlokir`, `PemblokirUnblokir`, `WaktuBlokirUnblokir`) VALUES
(1, '', '', 5000, 0, 100, 'HELDA TRAVEL', '', '', '', 'www.tiketux.com', 10000, '2011-08-23', '2011-08-31', 0, 25, '2013-09-03 15:04:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengumuman`
--

CREATE TABLE IF NOT EXISTS `tbl_pengumuman` (
  `IdPengumuman` int(10) unsigned NOT NULL,
  `KodePengumuman` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JudulPengumuman` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Pengumuman` text COLLATE latin1_general_ci,
  `Kota` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PembuatPengumuman` int(10) unsigned DEFAULT NULL,
  `WaktuPembuatanPengumuman` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penjadwalan_kendaraan`
--

CREATE TABLE IF NOT EXISTS `tbl_penjadwalan_kendaraan` (
  `IdPenjadwalan` int(10) unsigned NOT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeKendaraan` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `LayoutKursi` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) DEFAULT NULL,
  `KodeDriver` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDriver` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `StatusAktif` int(1) DEFAULT '0',
  `Remark` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penjadwalan_kendaraan_ba_penutupan`
--

CREATE TABLE IF NOT EXISTS `tbl_penjadwalan_kendaraan_ba_penutupan` (
  `Id` int(11) NOT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `Remark` text COLLATE latin1_general_ci,
  `PetugasPenutup` int(11) DEFAULT NULL,
  `NamaPetugasPenutup` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPenutupan` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_posisi`
--

CREATE TABLE IF NOT EXISTS `tbl_posisi` (
  `ID` int(10) unsigned NOT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `TglBerangkat` date NOT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JumlahKursi` int(2) NOT NULL DEFAULT '0',
  `SisaKursi` int(2) NOT NULL DEFAULT '0',
  `TglCetakSPJ` datetime DEFAULT NULL,
  `PetugasCetakSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Memo` text COLLATE latin1_general_ci,
  `PembuatMemo` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuatMemo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMemo` int(1) DEFAULT '0',
  `FlagOperasionalJurusan` int(1) DEFAULT '0' COMMENT '0=reguler; 1=paket; 2=paket dan reguler;3=non-reguler;4=charter'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_posisi`
--

INSERT INTO `tbl_posisi` (`ID`, `NoSPJ`, `KodeJadwal`, `IdJurusan`, `TglBerangkat`, `JamBerangkat`, `KodeKendaraan`, `KodeSopir`, `JumlahKursi`, `SisaKursi`, `TglCetakSPJ`, `PetugasCetakSPJ`, `Memo`, `PembuatMemo`, `WaktuBuatMemo`, `FlagMemo`, `FlagOperasionalJurusan`) VALUES
(1, NULL, 'BAN-DEP05', NULL, '2016-02-23', '05:00:00', '', '', 13, 13, NULL, NULL, NULL, NULL, NULL, 0, 0),
(2, NULL, 'BAN-KAR06', NULL, '2016-02-23', '06:00:00', '', '', 14, 14, NULL, NULL, NULL, NULL, NULL, 0, 0),
(3, NULL, 'BAN-KAR18', NULL, '2016-02-23', '18:00:00', '', '', 14, 14, NULL, NULL, NULL, NULL, NULL, 0, 0),
(4, NULL, 'BAN-KAR10', NULL, '2016-02-23', '10:00:00', '', '', 14, 14, NULL, NULL, NULL, NULL, NULL, 0, 0),
(5, NULL, 'DEP-BAN011', NULL, '2016-02-24', '11:00:00', '', '', 14, 14, NULL, NULL, NULL, NULL, NULL, 0, 0),
(6, NULL, 'DEP-BAN09', NULL, '2016-02-24', '09:00:00', '', '', 14, 14, NULL, NULL, NULL, NULL, NULL, 0, 0),
(7, NULL, 'BAN-DEP07', NULL, '2016-02-23', '07:00:00', '', '', 14, 14, NULL, NULL, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_posisi_backup`
--

CREATE TABLE IF NOT EXISTS `tbl_posisi_backup` (
  `ID` int(10) unsigned NOT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date NOT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL DEFAULT '0',
  `SisaKursi` int(2) NOT NULL DEFAULT '0',
  `TglCetakSPJ` datetime DEFAULT NULL,
  `PetugasCetakSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Memo` text COLLATE latin1_general_ci,
  `PembuatMemo` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuatMemo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMemo` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_posisi_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_posisi_detail` (
  `NomorKursi` int(2) NOT NULL,
  `ID` int(10) unsigned DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `JamBerangkat` time DEFAULT NULL,
  `IsSubJadwal` int(1) DEFAULT '0',
  `KodeJadwalUtama` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangAsal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date NOT NULL DEFAULT '0000-00-00',
  `StatusKursi` int(1) DEFAULT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Session` int(10) DEFAULT NULL,
  `StatusBayar` int(1) DEFAULT '0',
  `SessionTime` datetime DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_posisi_detail`
--

INSERT INTO `tbl_posisi_detail` (`NomorKursi`, `ID`, `KodeJadwal`, `JamBerangkat`, `IsSubJadwal`, `KodeJadwalUtama`, `KodeCabangAsal`, `KodeCabangTujuan`, `TglBerangkat`, `StatusKursi`, `NoTiket`, `Nama`, `Session`, `StatusBayar`, `SessionTime`, `KodeBooking`) VALUES
(1, NULL, 'BAN-DEP05', '05:00:00', 0, 'BAN-DEP05', 'BAN', 'DEP', '2016-02-23', 1, NULL, NULL, 1, 0, '2016-02-23 14:37:04', NULL),
(1, NULL, 'BAN-DEP07', '07:00:00', 0, 'BAN-DEP07', 'BAN', 'DEP', '2016-02-23', 1, NULL, NULL, 1, 0, '2016-02-23 15:04:59', NULL),
(1, NULL, 'BAN-KAR10', '10:00:00', 0, 'BAN-KAR10', 'BAN', 'KAR', '2016-02-23', 1, NULL, NULL, 1, 0, '2016-02-23 09:40:35', NULL),
(1, NULL, 'BAN-KAR18', '18:00:00', 0, 'BAN-KAR18', 'BAN', 'KAR', '2016-02-23', 1, NULL, NULL, 1, 0, '2016-02-23 09:39:59', NULL),
(2, NULL, 'BAN-KAR06', '06:00:00', 0, 'BAN-KAR06', 'BAN', 'KAR', '2016-02-23', 1, NULL, NULL, 1, 0, '2016-02-23 08:55:15', NULL),
(2, NULL, 'BAN-PUR06', '06:00:00', 1, 'BAN-KAR06', 'BAN', 'PUR', '2016-02-23', 1, NULL, NULL, 8, 0, '2016-02-23 15:07:51', NULL),
(4, NULL, 'DEP-BAN011', '11:00:00', 0, 'DEP-BAN011', 'DEP', 'BAN', '2016-02-24', 1, NULL, NULL, 1, 0, '2016-02-23 14:23:59', NULL),
(4, NULL, 'DEP-BAN09', '09:00:00', 0, 'DEP-BAN09', 'DEP', 'BAN', '2016-02-24', 1, NULL, NULL, 1, 0, '2016-02-23 15:03:31', NULL),
(5, NULL, 'DEP-BAN011', '11:00:00', 0, 'DEP-BAN011', 'DEP', 'BAN', '2016-02-24', 1, NULL, NULL, 1, 0, '2016-02-23 14:24:06', NULL),
(5, NULL, 'DEP-BAN09', '09:00:00', 0, 'DEP-BAN09', 'DEP', 'BAN', '2016-02-24', 1, NULL, NULL, 1, 0, '2016-02-23 15:03:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_posisi_detail_backup`
--

CREATE TABLE IF NOT EXISTS `tbl_posisi_detail_backup` (
  `NomorKursi` int(2) NOT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `TglBerangkat` date NOT NULL,
  `ID` int(10) unsigned NOT NULL,
  `StatusKursi` int(1) DEFAULT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Session` int(10) unsigned DEFAULT NULL,
  `StatusBayar` int(1) DEFAULT '0',
  `SessionTime` datetime DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rekon_data`
--

CREATE TABLE IF NOT EXISTS `tbl_rekon_data` (
  `IdRekonData` int(11) NOT NULL,
  `TglRekonData` date DEFAULT NULL,
  `PetugasRekonData` int(10) unsigned DEFAULT NULL,
  `JumlahSPJ` double DEFAULT NULL,
  `JumlahPenumpang` double DEFAULT NULL,
  `JumlahTiketOnline` double DEFAULT NULL,
  `JumlahTiketBatal` double DEFAULT NULL,
  `JumlahFeeTiket` double DEFAULT NULL,
  `JumlahPaket` double DEFAULT NULL,
  `JumlahFeePaket` double DEFAULT NULL,
  `JumlahSMS` double DEFAULT NULL,
  `JumlahFeeSMS` double DEFAULT NULL,
  `TotalFee` double DEFAULT NULL,
  `TotalDiskonFee` double DEFAULT '0',
  `TotalBayarFee` double DEFAULT '0',
  `FlagDibayar` int(1) DEFAULT NULL,
  `WaktuCatatBayar` datetime DEFAULT NULL,
  `PetugasCatatBayar` int(10) unsigned DEFAULT NULL,
  `FeePerTiket` double NOT NULL,
  `FeePerPaket` double NOT NULL,
  `FeePerSMS` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservasi`
--

CREATE TABLE IF NOT EXISTS `tbl_reservasi` (
  `NoTiket` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PointMember` int(3) DEFAULT '0',
  `Nama` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HP` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `HargaTiket` double DEFAULT '0',
  `Charge` double DEFAULT '0',
  `SubTotal` double DEFAULT '0',
  `Discount` double DEFAULT '0',
  `PPN` double DEFAULT '0',
  `Total` double DEFAULT '0',
  `PetugasPenjual` int(11) DEFAULT NULL,
  `FlagPesanan` int(1) DEFAULT '0',
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `CetakTiket` int(1) DEFAULT '0',
  `PetugasCetakTiket` int(11) DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `FlagSetor` int(1) DEFAULT '0',
  `PetugasCetakSPJ` int(11) DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscount` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(11) DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagRekonData` int(1) DEFAULT '0',
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagBatal` int(1) DEFAULT '0',
  `PetugasPembatalan` int(11) DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpang` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCode` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCodeExpiredTime` datetime DEFAULT NULL,
  `Signature` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Pemutasi` int(10) unsigned DEFAULT NULL,
  `MutasiDari` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `OTP` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `OTPUsed` int(1) NOT NULL DEFAULT '0',
  `IsSettlement` int(1) NOT NULL DEFAULT '1',
  `Komisi` double NOT NULL DEFAULT '0',
  `HargaTiketux` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reservasi_olap`
--

CREATE TABLE IF NOT EXISTS `tbl_reservasi_olap` (
  `NoTiket` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PointMember` int(3) DEFAULT '0',
  `Nama` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HP` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `HargaTiket` double DEFAULT '0',
  `Charge` double DEFAULT '0',
  `SubTotal` double DEFAULT '0',
  `Discount` double DEFAULT '0',
  `PPN` double DEFAULT '0',
  `Total` double DEFAULT '0',
  `PetugasPenjual` int(11) DEFAULT NULL,
  `FlagPesanan` int(1) DEFAULT '0',
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `CetakTiket` int(1) DEFAULT '0',
  `PetugasCetakTiket` int(11) DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `FlagSetor` int(1) DEFAULT '0',
  `PetugasCetakSPJ` int(11) DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscount` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(11) DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagRekonData` int(1) DEFAULT '0',
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagBatal` int(1) DEFAULT '0',
  `PetugasPembatalan` int(11) DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpang` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCode` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Pemutasi` int(10) unsigned DEFAULT NULL,
  `MutasiDari` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `OTP` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `OTPUsed` int(1) NOT NULL DEFAULT '0',
  `IsSettlement` int(1) NOT NULL DEFAULT '1',
  `Komisi` double NOT NULL DEFAULT '0',
  `HargaTiketux` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sessions`
--

CREATE TABLE IF NOT EXISTS `tbl_sessions` (
  `session_id` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `session_user_id` float NOT NULL,
  `session_start` float NOT NULL,
  `session_time` float NOT NULL,
  `session_ip` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `session_page` float NOT NULL,
  `session_logged_in` float NOT NULL,
  `session_admin` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_sessions`
--

INSERT INTO `tbl_sessions` (`session_id`, `session_user_id`, `session_start`, `session_time`, `session_ip`, `session_page`, `session_logged_in`, `session_admin`) VALUES
('0f059305de3788f3b83448390b98107d', 1, 1456210000, 1456220000, 'c0a80364', 201, 1, 0),
('89db248a0e6dc57be0b5995860be4b92', 8, 1456210000, 1456220000, '00000000', 201, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_spj`
--

CREATE TABLE IF NOT EXISTS `tbl_spj` (
  `NoSPJ` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `TglSPJ` datetime DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursiDisediakan` int(2) DEFAULT NULL,
  `JumlahPenumpang` int(2) DEFAULT '0',
  `JumlahPaket` int(3) DEFAULT NULL,
  `JumlahPaxPaket` int(3) DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CSO` int(10) unsigned DEFAULT NULL,
  `CSOPaket` int(10) DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalOmzetPaket` double DEFAULT NULL,
  `FlagAmbilBiayaOP` int(1) DEFAULT NULL,
  `TglTransaksi` datetime DEFAULT NULL,
  `CabangBayarOP` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `InsentifSopir` double DEFAULT '0',
  `IsEkspedisi` int(1) DEFAULT '0' COMMENT '0=travel; 1=paket',
  `IsCetakVoucherBBM` int(1) DEFAULT '0',
  `IsSubJadwal` int(1) DEFAULT '0' COMMENT '0=not verified; 1=verified',
  `IsCheck` int(1) DEFAULT '0',
  `IdChecker` int(11) DEFAULT NULL,
  `NamaChecker` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCheck` datetime DEFAULT NULL,
  `JumlahPenumpangCheck` int(2) DEFAULT NULL,
  `PathFoto` text COLLATE latin1_general_ci,
  `JumlahPaketCheck` int(2) DEFAULT '0',
  `JumlahPenumpangTambahanCheck` int(2) DEFAULT '0',
  `JumlahPenumpangTanpaTiketCheck` int(2) DEFAULT '0',
  `CatatanTambahanCheck` text COLLATE latin1_general_ci,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surat_jalan`
--

CREATE TABLE IF NOT EXISTS `tbl_surat_jalan` (
  `NoSJ` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `TglSJ` date NOT NULL,
  `WaktuSJ` varchar(6) COLLATE latin1_general_ci NOT NULL,
  `WaktuCetak` datetime NOT NULL,
  `IdPool` int(10) unsigned NOT NULL,
  `IdPetugas` int(10) unsigned NOT NULL,
  `NamaPetugas` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `NoBody` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NamaSopir` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `JumlahTrip` int(2) unsigned NOT NULL DEFAULT '0',
  `IsBatal` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuBatal` datetime NOT NULL,
  `Pembatal` int(10) unsigned NOT NULL,
  `NamaPembatal` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surat_jalan_biaya`
--

CREATE TABLE IF NOT EXISTS `tbl_surat_jalan_biaya` (
  `IdBiaya` int(10) unsigned NOT NULL,
  `JenisBiaya` int(1) NOT NULL,
  `COA` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoSJ` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(11) NOT NULL,
  `KodeJurusan` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `NamaBiaya` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `Jumlah` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `IsRealized` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `user_active` float NOT NULL,
  `username` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_password` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_session_time` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_session_page` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_lastvisit` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_level` decimal(3,1) NOT NULL,
  `user_timezone` float NOT NULL,
  `berlaku` datetime NOT NULL,
  `NRP` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `birthday` datetime NOT NULL,
  `telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `hp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `address` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `status_online` int(1) NOT NULL,
  `waktu_update_terakhir` datetime DEFAULT NULL,
  `waktu_login` datetime NOT NULL,
  `waktu_logout` datetime NOT NULL,
  `path_foto` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `jumlah_pengumuman_baru` int(11) DEFAULT '0',
  `cetak_bbm` int(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `KodeCabang`, `user_active`, `username`, `user_password`, `user_session_time`, `user_session_page`, `user_lastvisit`, `user_level`, `user_timezone`, `berlaku`, `NRP`, `nama`, `birthday`, `telp`, `hp`, `email`, `address`, `status_online`, `waktu_update_terakhir`, `waktu_login`, `waktu_logout`, `path_foto`, `jumlah_pengumuman_baru`, `cetak_bbm`) VALUES
(1, '', 1, 'demo', '202cb962ac59075b964b07152d234b70', '1456215015', '201', '1456213074', '0.0', 0, '2099-04-23 15:18:29', 'TBK007', 'DEMO', '0000-00-00 00:00:00', '', '', '', '', 1, '2016-02-23 15:28:39', '2016-02-23 14:38:42', '0000-00-00 00:00:00', NULL, 4, 0),
(2, 'BAN', 1, 'rahmat', '202cb962ac59075b964b07152d234b70', '', '', '', '2.0', 0, '2017-02-23 00:00:00', '001', 'Rahmat Hidayat', '0000-00-00 00:00:00', '', '', '', '', 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 4, 0),
(3, 'BAN', 1, 'khoerudin', '202cb962ac59075b964b07152d234b70', '', '', '', '2.0', 0, '2017-02-23 00:00:00', '002', 'Khoerudin Firmansyah', '0000-00-00 00:00:00', '', '', '', '', 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 4, 0),
(4, 'BAN', 1, 'ipah', '202cb962ac59075b964b07152d234b70', '1456192735', '900', '1456192663', '2.0', 0, '2017-02-23 00:00:00', '003', 'Ipah Nuraeni', '0000-00-00 00:00:00', '', '', '', '', 0, '2016-02-23 08:58:55', '2016-02-23 08:57:43', '2016-02-23 08:58:55', NULL, 4, 0),
(5, 'BAN', 1, 'sylvian', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', 0, '2017-02-23 00:00:00', '004', 'Sylvian Septiani', '0000-00-00 00:00:00', '', '', '', '', 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 4, 0),
(6, 'BAN', 1, 'whidi', '202cb962ac59075b964b07152d234b70', '', '', '', '2.0', 0, '2017-02-23 00:00:00', '005', 'Whidi Eka Syakti', '0000-00-00 00:00:00', '', '', '', '', 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 4, 0),
(7, 'BAN', 1, 'herman', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', 0, '2017-02-23 00:00:00', '006', 'Herman JF Frawira', '0000-00-00 00:00:00', '', '', '', '', 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 4, 0),
(8, 'BAN', 1, 'andryan', '202cb962ac59075b964b07152d234b70', '1456214980', '201', '1456192896', '1.0', 0, '2017-02-23 00:00:00', '007', 'Andryan Suanto', '0000-00-00 00:00:00', '', '', '', '', 1, '2016-02-23 15:30:50', '2016-02-23 15:07:33', '0000-00-00 00:00:00', NULL, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_baca_pengumuman`
--

CREATE TABLE IF NOT EXISTS `tbl_user_baca_pengumuman` (
  `user_id` int(11) NOT NULL,
  `IdPengumuman` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_setoran`
--

CREATE TABLE IF NOT EXISTS `tbl_user_setoran` (
  `IdSetoran` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `WaktuSetoran` datetime DEFAULT NULL,
  `IdUser` int(11) DEFAULT NULL,
  `NamaUser` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahTiketUmum` double DEFAULT '0',
  `JumlahTiketDiskon` double DEFAULT '0',
  `OmzetPenumpangTunai` double DEFAULT '0',
  `OmzetPenumpangDebit` double DEFAULT '0',
  `OmzetPenumpangKredit` double DEFAULT '0',
  `TotalDiskon` double DEFAULT '0',
  `TotalPaket` double DEFAULT '0',
  `OmzetPaket` double DEFAULT '0',
  `TotalBiaya` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voucher`
--

CREATE TABLE IF NOT EXISTS `tbl_voucher` (
  `KodeVoucher` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `CabangBerangkat` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `CabangTujuan` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPencetak` int(10) unsigned DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `ExpiredDate` date DEFAULT NULL,
  `WaktuDigunakan` datetime DEFAULT NULL,
  `PetugasPengguna` int(10) unsigned DEFAULT NULL,
  `NilaiVoucher` double NOT NULL,
  `IsHargaTetap` int(1) NOT NULL DEFAULT '0' COMMENT 'jika voucher nilainya adalah sebagai harga tertera, maka nilainya = 1',
  `IsBolehWeekEnd` int(1) NOT NULL DEFAULT '0',
  `IsReturn` int(1) NOT NULL DEFAULT '0',
  `IdJurusanBerangkat` int(11) DEFAULT NULL,
  `KodeJadwalBerangkat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NoTiketBerangkat` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `IsBatal` int(1) DEFAULT '0',
  `DibatalkanOleh` int(11) DEFAULT NULL,
  `WaktuBatal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voucher_bbm`
--

CREATE TABLE IF NOT EXISTS `tbl_voucher_bbm` (
  `Id` int(10) unsigned NOT NULL,
  `KodeVoucher` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `NoBody` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Kilometer` double DEFAULT NULL,
  `JenisBBM` int(1) DEFAULT NULL,
  `JumlahLiter` int(3) DEFAULT NULL,
  `JumlahBiaya` double DEFAULT NULL,
  `IdPetugas` int(10) unsigned DEFAULT NULL,
  `NamaPetugas` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDicatat` date DEFAULT NULL,
  `KodeSPBU` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSPBU` varchar(30) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_list_jurusan_by_kota_sangatta`
--
CREATE TABLE IF NOT EXISTS `view_list_jurusan_by_kota_sangatta` (
`IdJurusan` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `view_list_jurusan_by_kota_sangatta`
--
DROP TABLE IF EXISTS `view_list_jurusan_by_kota_sangatta`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_list_jurusan_by_kota_sangatta` AS select `tmj`.`IdJurusan` AS `IdJurusan` from (`tbl_md_jurusan` `tmj` join `tbl_md_cabang` `tmc` on((`tmj`.`KodeCabangAsal` = `tmc`.`KodeCabang`))) where (`tmc`.`Kota` = 'SANGATTA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_ba_bop`
--
ALTER TABLE `tbl_ba_bop`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_biaya_insentif_sopir`
--
ALTER TABLE `tbl_biaya_insentif_sopir`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IDX_1` (`TglBerangkat`) USING BTREE,
  ADD KEY `IDX_2` (`IdJurusan`) USING BTREE,
  ADD KEY `FK_5` (`IdPenjadwalan`) USING BTREE,
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  ADD KEY `KodeKendaraan` (`KodeKendaraan`) USING BTREE,
  ADD KEY `KodeSopir` (`KodeSopir`) USING BTREE;

--
-- Indexes for table `tbl_biaya_op`
--
ALTER TABLE `tbl_biaya_op`
  ADD PRIMARY KEY (`IDBiayaOP`),
  ADD KEY `FK_1` (`NoSPJ`) USING BTREE,
  ADD KEY `FK_2` (`IdJurusan`) USING BTREE,
  ADD KEY `FK_3` (`TglTransaksi`) USING BTREE,
  ADD KEY `FK_4` (`KodeSopir`) USING BTREE,
  ADD KEY `FK_5` (`NoPolisi`) USING BTREE;

--
-- Indexes for table `tbl_deposit_log_topup`
--
ALTER TABLE `tbl_deposit_log_topup`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_deposit_log_trx`
--
ALTER TABLE `tbl_deposit_log_trx`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_jenis_discount`
--
ALTER TABLE `tbl_jenis_discount`
  ADD PRIMARY KEY (`IdDiscount`);

--
-- Indexes for table `tbl_kasbon_sopir`
--
ALTER TABLE `tbl_kasbon_sopir`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_kendaraan_administratif`
--
ALTER TABLE `tbl_kendaraan_administratif`
  ADD PRIMARY KEY (`id_kendaraan_administratif`);

--
-- Indexes for table `tbl_log_cetakulang_voucher_bbm`
--
ALTER TABLE `tbl_log_cetakulang_voucher_bbm`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_log_cetak_manifest`
--
ALTER TABLE `tbl_log_cetak_manifest`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE;

--
-- Indexes for table `tbl_log_cetak_tiket`
--
ALTER TABLE `tbl_log_cetak_tiket`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE;

--
-- Indexes for table `tbl_log_finpay`
--
ALTER TABLE `tbl_log_finpay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_log_koreksi_disc`
--
ALTER TABLE `tbl_log_koreksi_disc`
  ADD PRIMARY KEY (`IdLog`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE;

--
-- Indexes for table `tbl_log_mutasi`
--
ALTER TABLE `tbl_log_mutasi`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `KodeJadwalSebelumnya` (`KodeJadwalSebelumnya`) USING BTREE,
  ADD KEY `FK2` (`KodeJadwalMutasi`) USING BTREE;

--
-- Indexes for table `tbl_log_sms`
--
ALTER TABLE `tbl_log_sms`
  ADD PRIMARY KEY (`IdLogSms`),
  ADD KEY `IDX_WAKTUKIRIM` (`WaktuKirim`) USING BTREE;

--
-- Indexes for table `tbl_log_token_sms`
--
ALTER TABLE `tbl_log_token_sms`
  ADD PRIMARY KEY (`IdLog`);

--
-- Indexes for table `tbl_log_user`
--
ALTER TABLE `tbl_log_user`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `tbl_mac_address`
--
ALTER TABLE `tbl_mac_address`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_md_cabang`
--
ALTER TABLE `tbl_md_cabang`
  ADD PRIMARY KEY (`KodeCabang`),
  ADD KEY `TbMDCabang_index4992` (`Nama`,`Alamat`,`Kota`) USING BTREE,
  ADD KEY `TbMDCabang_index4994` (`Telp`) USING BTREE,
  ADD KEY `TbMDCabang_index4995` (`FlagAgen`) USING BTREE;

--
-- Indexes for table `tbl_md_harga_promo`
--
ALTER TABLE `tbl_md_harga_promo`
  ADD PRIMARY KEY (`KodeHargaPromo`);

--
-- Indexes for table `tbl_md_jadwal`
--
ALTER TABLE `tbl_md_jadwal`
  ADD PRIMARY KEY (`KodeJadwal`),
  ADD KEY `TbMDJadwal_index5011` (`IdJurusan`,`JamBerangkat`,`FlagAktif`) USING BTREE,
  ADD KEY `TbMDJadwal_index5013` (`IdJurusan`) USING BTREE;

--
-- Indexes for table `tbl_md_jurusan`
--
ALTER TABLE `tbl_md_jurusan`
  ADD PRIMARY KEY (`IdJurusan`),
  ADD KEY `TbMDJurusan_index4998` (`BiayaSopir`) USING BTREE,
  ADD KEY `TbMDJurusan_index4999` (`KodeJurusan`) USING BTREE,
  ADD KEY `fk1mdjur` (`KodeCabangAsal`) USING BTREE,
  ADD KEY `fk2mdjur` (`KodeCabangTujuan`) USING BTREE;

--
-- Indexes for table `tbl_md_kendaraan`
--
ALTER TABLE `tbl_md_kendaraan`
  ADD PRIMARY KEY (`KodeKendaraan`),
  ADD KEY `TbMDKendaraan_index5022` (`NoPolisi`) USING BTREE;

--
-- Indexes for table `tbl_md_kendaraan_layout`
--
ALTER TABLE `tbl_md_kendaraan_layout`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_md_kota`
--
ALTER TABLE `tbl_md_kota`
  ADD PRIMARY KEY (`KodeKota`);

--
-- Indexes for table `tbl_md_libur`
--
ALTER TABLE `tbl_md_libur`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IDX_1` (`TglLibur`) USING BTREE;

--
-- Indexes for table `tbl_md_member`
--
ALTER TABLE `tbl_md_member`
  ADD PRIMARY KEY (`IdMember`),
  ADD KEY `TbMDMember_index5245` (`IdKartu`) USING BTREE,
  ADD KEY `TbMDMember_index5246` (`NoSeriKartu`) USING BTREE;

--
-- Indexes for table `tbl_md_paket_daftar_layanan`
--
ALTER TABLE `tbl_md_paket_daftar_layanan`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_md_plan_asuransi`
--
ALTER TABLE `tbl_md_plan_asuransi`
  ADD PRIMARY KEY (`IdPlanAsuransi`);

--
-- Indexes for table `tbl_md_promo`
--
ALTER TABLE `tbl_md_promo`
  ADD PRIMARY KEY (`KodePromo`);

--
-- Indexes for table `tbl_md_sopir`
--
ALTER TABLE `tbl_md_sopir`
  ADD PRIMARY KEY (`KodeSopir`);

--
-- Indexes for table `tbl_member_deposit_topup_log`
--
ALTER TABLE `tbl_member_deposit_topup_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_member_deposit_trx_log`
--
ALTER TABLE `tbl_member_deposit_trx_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_member_frekwensi_berangkat`
--
ALTER TABLE `tbl_member_frekwensi_berangkat`
  ADD PRIMARY KEY (`IdFrekwensi`);

--
-- Indexes for table `tbl_member_mutasi_point`
--
ALTER TABLE `tbl_member_mutasi_point`
  ADD PRIMARY KEY (`IdMutasiPoint`);

--
-- Indexes for table `tbl_member_promo_point`
--
ALTER TABLE `tbl_member_promo_point`
  ADD PRIMARY KEY (`KodeProduk`);

--
-- Indexes for table `tbl_paket`
--
ALTER TABLE `tbl_paket`
  ADD PRIMARY KEY (`NoTiket`),
  ADD KEY `tbl_paket_index5692` (`TelpPengirim`,`AlamatPenerima`) USING BTREE;

--
-- Indexes for table `tbl_paket_cargo`
--
ALTER TABLE `tbl_paket_cargo`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_paket_cargo_md_harga`
--
ALTER TABLE `tbl_paket_cargo_md_harga`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_paket_cargo_md_kota`
--
ALTER TABLE `tbl_paket_cargo_md_kota`
  ADD PRIMARY KEY (`KodeKota`);

--
-- Indexes for table `tbl_paket_cargo_md_provinsi`
--
ALTER TABLE `tbl_paket_cargo_md_provinsi`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_paket_cargo_spj`
--
ALTER TABLE `tbl_paket_cargo_spj`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  ADD PRIMARY KEY (`IdPelanggan`),
  ADD KEY `IDX_01` (`NoHP`) USING BTREE,
  ADD KEY `IDX_02` (`NoTelp`) USING BTREE,
  ADD KEY `IDX_03` (`FrekwensiPergi`) USING BTREE;

--
-- Indexes for table `tbl_pengaturan_parameter`
--
ALTER TABLE `tbl_pengaturan_parameter`
  ADD PRIMARY KEY (`NamaParameter`);

--
-- Indexes for table `tbl_pengaturan_umum`
--
ALTER TABLE `tbl_pengaturan_umum`
  ADD PRIMARY KEY (`IdPengaturanUmum`);

--
-- Indexes for table `tbl_pengumuman`
--
ALTER TABLE `tbl_pengumuman`
  ADD PRIMARY KEY (`IdPengumuman`);

--
-- Indexes for table `tbl_penjadwalan_kendaraan`
--
ALTER TABLE `tbl_penjadwalan_kendaraan`
  ADD PRIMARY KEY (`IdPenjadwalan`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE;

--
-- Indexes for table `tbl_penjadwalan_kendaraan_ba_penutupan`
--
ALTER TABLE `tbl_penjadwalan_kendaraan_ba_penutupan`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FX_1` (`TglBerangkat`) USING BTREE,
  ADD KEY `FX_2` (`TglBerangkat`,`KodeJadwal`) USING BTREE,
  ADD KEY `FX_3` (`IdJurusan`) USING BTREE,
  ADD KEY `FX_4` (`PetugasPenutup`) USING BTREE,
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE;

--
-- Indexes for table `tbl_posisi`
--
ALTER TABLE `tbl_posisi`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `TbPosisi_index5116` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbPosisi_index5117` (`NoSPJ`) USING BTREE;

--
-- Indexes for table `tbl_posisi_backup`
--
ALTER TABLE `tbl_posisi_backup`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `TbPosisi_index5116` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbPosisi_index5117` (`NoSPJ`) USING BTREE;

--
-- Indexes for table `tbl_posisi_detail`
--
ALTER TABLE `tbl_posisi_detail`
  ADD PRIMARY KEY (`NomorKursi`,`KodeJadwal`,`TglBerangkat`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  ADD KEY `idx2` (`KodeJadwal`,`TglBerangkat`) USING BTREE;

--
-- Indexes for table `tbl_posisi_detail_backup`
--
ALTER TABLE `tbl_posisi_detail_backup`
  ADD PRIMARY KEY (`NomorKursi`,`KodeJadwal`,`TglBerangkat`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE;

--
-- Indexes for table `tbl_rekon_data`
--
ALTER TABLE `tbl_rekon_data`
  ADD PRIMARY KEY (`IdRekonData`);

--
-- Indexes for table `tbl_reservasi`
--
ALTER TABLE `tbl_reservasi`
  ADD PRIMARY KEY (`NoTiket`),
  ADD KEY `TbReservasi_index5119` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5120` (`IdJurusan`) USING BTREE,
  ADD KEY `TbReservasi_index5121` (`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5123` (`KodeCabang`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5124` (`CetakTiket`,`TglBerangkat`,`KodeJadwal`) USING BTREE,
  ADD KEY `TbReservasi_index5125` (`KodeCabang`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5126` (`IdJurusan`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5127` (`KodeKendaraan`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5128` (`TglBerangkat`,`KodeKendaraan`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5129` (`PetugasPenjual`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5130` (`TglBerangkat`,`PetugasCetakTiket`,`CetakTiket`) USING BTREE;

--
-- Indexes for table `tbl_reservasi_olap`
--
ALTER TABLE `tbl_reservasi_olap`
  ADD PRIMARY KEY (`NoTiket`),
  ADD KEY `TbReservasi_index5119` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5120` (`IdJurusan`) USING BTREE,
  ADD KEY `TbReservasi_index5121` (`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5123` (`KodeCabang`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5124` (`CetakTiket`,`TglBerangkat`,`KodeJadwal`) USING BTREE,
  ADD KEY `TbReservasi_index5125` (`KodeCabang`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5126` (`IdJurusan`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5127` (`KodeKendaraan`,`TglBerangkat`) USING BTREE,
  ADD KEY `TbReservasi_index5128` (`TglBerangkat`,`KodeKendaraan`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5129` (`PetugasPenjual`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  ADD KEY `TbReservasi_index5130` (`TglBerangkat`,`PetugasCetakTiket`,`CetakTiket`) USING BTREE;

--
-- Indexes for table `tbl_spj`
--
ALTER TABLE `tbl_spj`
  ADD PRIMARY KEY (`NoSPJ`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  ADD KEY `IDX_2` (`NoPolisi`) USING BTREE,
  ADD KEY `IDX_3` (`TglSPJ`) USING BTREE,
  ADD KEY `IDX_4` (`TglSPJ`,`NoPolisi`) USING BTREE;

--
-- Indexes for table `tbl_surat_jalan`
--
ALTER TABLE `tbl_surat_jalan`
  ADD PRIMARY KEY (`NoSJ`);

--
-- Indexes for table `tbl_surat_jalan_biaya`
--
ALTER TABLE `tbl_surat_jalan_biaya`
  ADD PRIMARY KEY (`IdBiaya`),
  ADD KEY `Index_2` (`NoSJ`) USING BTREE;

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_user_baca_pengumuman`
--
ALTER TABLE `tbl_user_baca_pengumuman`
  ADD PRIMARY KEY (`user_id`,`IdPengumuman`);

--
-- Indexes for table `tbl_user_setoran`
--
ALTER TABLE `tbl_user_setoran`
  ADD PRIMARY KEY (`IdSetoran`);

--
-- Indexes for table `tbl_voucher`
--
ALTER TABLE `tbl_voucher`
  ADD PRIMARY KEY (`KodeVoucher`),
  ADD KEY `KodeJadwal` (`KodeJadwal`) USING BTREE;

--
-- Indexes for table `tbl_voucher_bbm`
--
ALTER TABLE `tbl_voucher_bbm`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ba_bop`
--
ALTER TABLE `tbl_ba_bop`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_biaya_insentif_sopir`
--
ALTER TABLE `tbl_biaya_insentif_sopir`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_biaya_op`
--
ALTER TABLE `tbl_biaya_op`
  MODIFY `IDBiayaOP` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_deposit_log_topup`
--
ALTER TABLE `tbl_deposit_log_topup`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_deposit_log_trx`
--
ALTER TABLE `tbl_deposit_log_trx`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_jenis_discount`
--
ALTER TABLE `tbl_jenis_discount`
  MODIFY `IdDiscount` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_kasbon_sopir`
--
ALTER TABLE `tbl_kasbon_sopir`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_kendaraan_administratif`
--
ALTER TABLE `tbl_kendaraan_administratif`
  MODIFY `id_kendaraan_administratif` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_cetakulang_voucher_bbm`
--
ALTER TABLE `tbl_log_cetakulang_voucher_bbm`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_cetak_manifest`
--
ALTER TABLE `tbl_log_cetak_manifest`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_cetak_tiket`
--
ALTER TABLE `tbl_log_cetak_tiket`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_finpay`
--
ALTER TABLE `tbl_log_finpay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_koreksi_disc`
--
ALTER TABLE `tbl_log_koreksi_disc`
  MODIFY `IdLog` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_mutasi`
--
ALTER TABLE `tbl_log_mutasi`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_sms`
--
ALTER TABLE `tbl_log_sms`
  MODIFY `IdLogSms` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_token_sms`
--
ALTER TABLE `tbl_log_token_sms`
  MODIFY `IdLog` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_log_user`
--
ALTER TABLE `tbl_log_user`
  MODIFY `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT for table `tbl_mac_address`
--
ALTER TABLE `tbl_mac_address`
  MODIFY `Id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_md_harga_promo`
--
ALTER TABLE `tbl_md_harga_promo`
  MODIFY `KodeHargaPromo` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_md_jurusan`
--
ALTER TABLE `tbl_md_jurusan`
  MODIFY `IdJurusan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_md_libur`
--
ALTER TABLE `tbl_md_libur`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_md_paket_daftar_layanan`
--
ALTER TABLE `tbl_md_paket_daftar_layanan`
  MODIFY `Id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_md_plan_asuransi`
--
ALTER TABLE `tbl_md_plan_asuransi`
  MODIFY `IdPlanAsuransi` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_member_deposit_topup_log`
--
ALTER TABLE `tbl_member_deposit_topup_log`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_member_deposit_trx_log`
--
ALTER TABLE `tbl_member_deposit_trx_log`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_member_frekwensi_berangkat`
--
ALTER TABLE `tbl_member_frekwensi_berangkat`
  MODIFY `IdFrekwensi` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_paket_cargo`
--
ALTER TABLE `tbl_paket_cargo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_paket_cargo_md_harga`
--
ALTER TABLE `tbl_paket_cargo_md_harga`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_paket_cargo_md_provinsi`
--
ALTER TABLE `tbl_paket_cargo_md_provinsi`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_paket_cargo_spj`
--
ALTER TABLE `tbl_paket_cargo_spj`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  MODIFY `IdPelanggan` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_pengaturan_umum`
--
ALTER TABLE `tbl_pengaturan_umum`
  MODIFY `IdPengaturanUmum` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_pengumuman`
--
ALTER TABLE `tbl_pengumuman`
  MODIFY `IdPengumuman` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_penjadwalan_kendaraan`
--
ALTER TABLE `tbl_penjadwalan_kendaraan`
  MODIFY `IdPenjadwalan` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_penjadwalan_kendaraan_ba_penutupan`
--
ALTER TABLE `tbl_penjadwalan_kendaraan_ba_penutupan`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_posisi`
--
ALTER TABLE `tbl_posisi`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_posisi_backup`
--
ALTER TABLE `tbl_posisi_backup`
  MODIFY `ID` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_rekon_data`
--
ALTER TABLE `tbl_rekon_data`
  MODIFY `IdRekonData` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_surat_jalan_biaya`
--
ALTER TABLE `tbl_surat_jalan_biaya`
  MODIFY `IdBiaya` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_voucher_bbm`
--
ALTER TABLE `tbl_voucher_bbm`
  MODIFY `Id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
