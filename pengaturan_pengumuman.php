<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengumuman.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["SCHEDULER"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Pengumuman	= new Pengumuman();

	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'pengumuman/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx).'">Pengumuman</a> | <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx."?mode=add").'">Tambah Pengumuman</a> ',
		 'JUDUL'		=>'Tambah Data Pengumuman',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '0',
		 'PESAN'						=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_PENGUMUMAN_ADD_ACT'	=> append_sid('pengaturan_pengumuman.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah pengumuman
		$id_pengumuman = $HTTP_POST_VARS['id_pengumuman'];
		$kode  						= str_replace(" ","",$HTTP_POST_VARS['kode']);
		$kode_old					= str_replace(" ","",$HTTP_POST_VARS['kode_old']);
		$judul_pengumuman	= $HTTP_POST_VARS['judul'];
		$pengumuman   		= $HTTP_POST_VARS['pengumuman'];
		
		$terjadi_error=false;
		
		if($Pengumuman->periksaDuplikasi($kode) && $kode!=$kode_old){
			$pesan="<font color='white' size=3>Kode pengumuman yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Pengumuman";
				$path	='<a href="'.append_sid('pengaturan_pengumuman.'.$phpEx."?mode=add").'">Tambah Pengumuman</a> ';
				
				if($Pengumuman->tambah($kode,$judul_pengumuman,$pengumuman,$kota,$userdata['user_id'])){
						
					redirect(append_sid('pengaturan_pengumuman.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data pengumuman Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
				}
			}
			else{
				
				$judul="Ubah Data Pengumuman";
				$path	='<a href="'.append_sid('pengaturan_pengumuman.'.$phpEx."?mode=edit&id=$id_pengumuman").'">Ubah Pengumuman</a> ';
				
				if($Pengumuman->ubah($id_pengumuman,$kode_old,$kode,$judul_pengumuman,$pengumuman,$kota,$userdata['user_id'])){
						
						//redirect(append_sid('pengaturan_pengumuman.'.$phpEx.'?mode=add',true));
						//die_message('<h2>Data pengumuman Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx.'?mode=edit&id='.$kode).'">Sini</a> Untuk Melanjutkan','');
						$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
						$bgcolor_pesan="98e46f";
				}
			}
			
			//exit;
			
		}
		
		$template->set_filenames(array('body' => 'pengumuman/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx).'">Pengumuman</a> | '.$path,
		 'JUDUL'    => $judul,
		 'MODE'   	=> 'save',
		 'SUB'    	=> $submode,
		 'ID_PENGUMUMAN' => $id_pengumuman,
		 'KODE_OLD' => $kode_old,
		 'KODE'    	=> $kode,
		 'JUDUL_PENGUMUMAN'=>$judul_pengumuman,
		 'PENGUMUMAN'=> $pengumuman,
		 'PEMBUAT'		=> $userdata['nama'],
		 'WAKTU_BUAT'=> "",
		 'PESAN'		 => $pesan,
		 'BGCOLOR_PESAN'=> $bgcolor_pesan,
		 'U_PENGUMUMAN_ADD_ACT'=>append_sid('pengaturan_pengumuman.'.$phpEx)
		 )
		);
		
		
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Pengumuman->ambilDataDetail($id);
		
		$template->set_filenames(array('body' => 'pengumuman/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx).'">Pengumuman</a> | <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx."?mode=edit&id=$id").'">Ubah Pengumuman</a> ',
			 'JUDUL'		=>'Ubah Data Pengumuman',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'ID_PENGUMUMAN' => $row['IdPengumuman'],
			 'KODE_OLD' => $row['KodePengumuman'],
			 'KODE'    	=> $row['KodePengumuman'],
			 'JUDUL_PENGUMUMAN'=> $row['JudulPengumuman'],
			 'PENGUMUMAN'=> $row['Pengumuman'],
			 'PEMBUAT'	=> $row['NamaPembuat'],
			 'WAKTU_BUAT'=> FormatMySQLDateToTgl($row['WaktuPembuatanPengumuman']),
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_PENGUMUMAN_ADD_ACT'=>append_sid('pengaturan_pengumuman.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus pengumuman
		$list_pengumuman = str_replace("\'","'",$HTTP_GET_VARS['list_pengumuman']);
		//echo($list_pengumuman. " asli :".$HTTP_GET_VARS['list_pengumuman']);
		$Pengumuman->hapus($list_pengumuman);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'pengumuman/pengumuman_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE KodePengumuman LIKE '%$cari%' 
				OR JudulPengumuman LIKE '%$cari%' 
				OR Pengumuman LIKE '%$cari%'";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdPengumuman","tbl_pengumuman","",$kondisi,"pengaturan_pengumuman.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT 
				IdPengumuman,KodePengumuman,JudulPengumuman,
				LEFT(Pengumuman,100) AS Pengumuman,WaktuPembuatanPengumuman,
				f_user_get_nama_by_userid(PembuatPengumuman) AS NamaPembuat
			FROM tbl_pengumuman $kondisi 
			ORDER BY WaktuPembuatanPengumuman DESC,KodePengumuman ASC LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_pengumuman.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'		=>$odd,
							'check'	=>$check,
							'no'		=>$i,
							'tanggal'=>FormatMySQLDateToTgl($row['WaktuPembuatanPengumuman']),
							'kode'	=>$row['KodePengumuman'],
							'judul'	=>$row['JudulPengumuman'],
							'pengumuman'=>$row['Pengumuman']."...",
							'pembuat'	=>$row['NamaPembuat'],
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load pengumuman',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_pengumuman.'.$phpEx).'">Pengumuman</a>',
			'U_ADD'		=> append_sid('pengaturan_pengumuman.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_pengumuman.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>