<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']>$LEVEL_MANAJEMEN){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################



// ACTION
$act = $HTTP_GET_VARS['act'];
if ($act == 'delete' ) {
	$id = $HTTP_GET_VARS['id'];
	$sql = "DELETE FROM tbcomplain WHERE id = '$id'";
	if (!$result = $db->sql_query($sql))
		{
		  die_error('Cannot Load complain',__FILE__,__LINE__,$sql);
		}
	redirect(append_sid('complain.'.$phpEx,true));
	die_message('Data Complain Telah Terhapus','Click Di <a href="'.append_sid('complain.'.$phpEx).'">Sini</a> Untuk Melanjutkan','');

}
else
if ($act == 'add' ) {
	$comment = nl2br($HTTP_POST_VARS['complain']);
	$userid = $userdata['user_id'];
		$sql = "INSERT INTO tbcomplain (userid,komentar,cdate) values ('$userid','$comment',CONVERT(datetime,getdate(),20))";
	if (!$result = $db->sql_query($sql))
		{
		  die_error('Cannot Load complain',__FILE__,__LINE__,$sql);
		}
	redirect(append_sid('complain.'.$phpEx,true));
	die_message('Data Complain Telah diposting','Click Di <a href="'.append_sid('complain.'.$phpEx).'">Sini</a> Untuk Melanjutkan','');

}
else {
// LIST
	
	$sql = "SELECT id,userid,komentar,CONVERT(CHAR(20),cdate,20),status FROM tbcomplain";
	
	if (!$result = $db->sql_query($sql)){
		die_error('Cannot Load complain',__FILE__,__LINE__,$sql);
	} 
	else{
		$i = 1;
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
			 
			$sqlu = "SELECT username from TbUser where user_id = '$row[1]'";
			
			if (!$resultu = $db->sql_query($sqlu)){
				die_error('Cannot Load username',__FILE__,__LINE__,$sqlu);
			}
			else{
				$rowu = $db->sql_fetchrow($resultu);
			}			 
			
			$act = "<a onclick='return confirm(\"Yakin Akan Menghapus ?\");' href='".append_sid('complain.'.$phpEx.'?act=delete&id='.$row[0])."'>Delete</a>";
			
			$template->assign_block_vars('ROW',array('odd'=>$odd,'no'=>$i,'complain'=>$row[2].$rowr[2],'tanggal'=>$row[3],'user'=>$rowu[0],'action'=>$act));
			$i++;
		}
	}
}
$template->assign_vars  (array(
	'BCRUMP' =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('complain.'.$phpEx).'">Complain</a>',
	'U_COMPLAIN_ADD'=>append_sid('complain.'.$phpEx.'?act=add')));

$template->set_filenames(array('body' => 'complain.tpl')); 
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
