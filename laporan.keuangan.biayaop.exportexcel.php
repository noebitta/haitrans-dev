<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,305);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$filbiaya1  = isset($HTTP_GET_VARS['filbiaya1'])? $HTTP_GET_VARS['filbiaya1'] : $HTTP_POST_VARS['filbiaya1'];
$filbiaya2  = isset($HTTP_GET_VARS['filbiaya2'])? $HTTP_GET_VARS['filbiaya2'] : $HTTP_POST_VARS['filbiaya2'];
$filbiaya3  = isset($HTTP_GET_VARS['filbiaya3'])? $HTTP_GET_VARS['filbiaya3'] : $HTTP_POST_VARS['filbiaya3'];
$filbiaya4  = isset($HTTP_GET_VARS['filbiaya4'])? $HTTP_GET_VARS['filbiaya4'] : $HTTP_POST_VARS['filbiaya4'];
$filbiaya5  = isset($HTTP_GET_VARS['filbiaya5'])? $HTTP_GET_VARS['filbiaya5'] : $HTTP_POST_VARS['filbiaya5'];
$filbiaya7  = isset($HTTP_GET_VARS['filbiaya7'])? $HTTP_GET_VARS['filbiaya7'] : $HTTP_POST_VARS['filbiaya7'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//VIEW MODE=========================================================================================

$list_filter_jenis_biaya = "";
	
if($filbiaya1=="on"){$chkbiaya1="checked";$list_filter_jenis_biaya .="1,";}else{$chkbiaya1="";$list_filter_jenis_biaya .="";}
if($filbiaya2=="on"){$chkbiaya2="checked";$list_filter_jenis_biaya .="2,";}else{$chkbiaya2="";$list_filter_jenis_biaya .="";}
if($filbiaya3=="on"){$chkbiaya3="checked";$list_filter_jenis_biaya .="3,";}else{$chkbiaya3="";$list_filter_jenis_biaya .="";}
if($filbiaya4=="on"){$chkbiaya4="checked";$list_filter_jenis_biaya .="4,";}else{$chkbiaya4="";$list_filter_jenis_biaya .="";}
if($filbiaya5=="on"){$chkbiaya5="checked";$list_filter_jenis_biaya .="5,";}else{$chkbiaya5="";$list_filter_jenis_biaya .="";}
if($filbiaya7=="on"){$chkbiaya7="checked";$list_filter_jenis_biaya .="7,";}else{$chkbiaya7="";$list_filter_jenis_biaya .="";}
	
$list_filter_jenis_biaya	= $list_filter_jenis_biaya!=""?substr($list_filter_jenis_biaya,0,-1):"1,2,3,4,5,7";
$kondisi_filter_biaya	= " AND FlagJenisBiaya IN(".$list_filter_jenis_biaya.")";
	
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

// LIST
$kondisi =($cari=="")?
	"":
	" AND (f_sopir_get_nama_by_id(tbo.KodeSopir) LIKE '$cari%'
					OR f_kendaraan_ambil_nopol_by_kode(tbo.NoPolisi) LIKE '%$cari%'
					OR tbo.NoSPJ LIKE '%$cari%'
					OR f_user_get_nama_by_userid(IdPetugas) LIKE '%$cari%'
					OR CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) LIKE  '%$cari%'
				)";

$kondisi .= $kota==""?"":"AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan))='$kota'";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)='$asal'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan)='$tujuan'":"";

$sql	=
	"SELECT
		tbo.TglTransaksi,
		CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) AS Jurusan,
		ts.JamBerangkat,
		tbo.NoSPJ,
		tbo.NoPolisi,
		f_sopir_get_nama_by_id(tbo.KodeSopir) AS NamaSopir,
		Jumlah,FlagJenisBiaya,
		f_user_get_nama_by_userid(IdPetugas) AS Pencetak,
		tbo.WaktuCatat
	FROM (tbl_biaya_op tbo LEFT JOIN tbl_spj ts ON tbo.NoSPJ=ts.NoSPJ)
	WHERE
		(tbo.TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		$kondisi
		$kondisi_filter_biaya
	ORDER BY $sort_by $order";

//EXPORT KE MS-EXCEL
	
if ($result = $db->sql_query($sql)){
		
	$i=1;
	
	$objPHPExcel = new PHPExcel();          
  $objPHPExcel->setActiveSheetIndex(0);  
  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
  
	//HEADER
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Keuangan Biaya Operasional per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
  $objPHPExcel->getActiveSheet()->setCellValue('A4', 'No');
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Tanggal');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Jurusan');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Jam');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Jenis Biaya');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('F4', 'No.SPJ');
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Kendaraan');
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Sopir');
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Jumlah');
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('J4', 'Releaser');
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Waktu Release');
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	
	$idx=0;
	
	while ($row = $db->sql_fetchrow($result)){
		$idx++;
		$idx_row=$idx+4;
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['TglTransaksi']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['Jurusan']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['JamBerangkat']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $LIST_JENIS_BIAYA[$row['FlagJenisBiaya']]);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NoSPJ']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NoPolisi']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NamaSopir']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Jumlah']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['Pencetak']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['WaktuCatat']);
		
		
	}
	$temp_idx=$idx_row;
	
	$idx_row++;		
	
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':H'.$idx_row);
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
		
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
  
	if ($idx>0){
		header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Keuangan Biaya Operasional per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output'); 
	}
}
else{
	die_error('Err:',__LINE__);
}   
 
  
?>
