<?php

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in']){  
  exit;
}

class Asuransi{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Asuransi(){
		$this->ID_FILE="C-ASR";
	}
	
	//BODY
	
	function tambah($nama_plan,$besar_premi,$keterangan){
	  
		/*
		IS	: data asuransi belum ada dalam database
		FS	:Data asuransi baru telah disimpan dalam database 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $LEVEL_KEUANGAN;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER && $user_level!=$LEVEL_KEUANGAN){
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_md_plan_asuransi (NamaPlan, BesarPremi, Keterangan)
			VALUES('$nama_plan','$besar_premi','$keterangan')";
				
		if (!$db->sql_query($sql)){
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah($id_plan_asuransi,$nama_plan,$besar_premi,$keterangan){
	  
		/*
		IS	: data asuransi sudah ada dalam database
		FS	:Data asuransi diubah 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $LEVEL_KEUANGAN;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER && $user_level!=$LEVEL_KEUANGAN){
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_md_plan_asuransi SET
				NamaPlan='$nama_plan', 
				BesarPremi='$besar_premi', 
				Keterangan='$keterangan'
			WHERE IdPlanAsuransi='$id_plan_asuransi'";
				
		if (!$db->sql_query($sql)){
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_id_plan){
	  
		/*
		IS	: data asuransi sudah ada dalam database
		FS	:Data asuransi dihapus
		*/
		
		//kamus
		global $db;
		global $userdata;
		global $LEVEL_MANAJER;
		global $LEVEL_KEUANGAN;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER && $user_level!=$LEVEL_KEUANGAN){
			return 0;
		}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENGHAPUS DATA DALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_plan_asuransi
			WHERE IdPlanAsuransi IN($list_id_plan)";
								
		if (!$db->sql_query($sql)){
			return 3;
			die_error("ERR: $this->ID_FILE".__LINE__);
		}
		
		return 1;
	}//end hapus
	
	function ambilDataDetail($id_plan_asuransi){
		
		/*
		ID	:007
		Desc	:Mengembalikan data promo sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_plan_asuransi
			WHERE IdPlanAsuransi='$id_plan_asuransi'";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilDataDetail
	
	function ambilData($order_by="",$asc=""){
		
		/*
		Desc	:Mengembalikan data asuransi sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_md_plan_asuransi
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function tambahAsuransi(
		$tgl_berangkat, $id_jurusan, $kode_jadwal, 
		$jam_berangkat, $no_tiket, $nama, 
		$tgl_lahir, $telp, $hp, 
		$plan_asuransi, $besar_premi, $petugas_transaksi, 
		$waktu_transaksi, $cabang_transaksi, $flag_batal){
	  
		/*
		IS	: data plan asuransi belum ada dalam database
		FS	:Data plan asuransi baru telah disimpan dalam database 
		*/
		
		//kamus
		global $userdata;
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_asuransi (
				TglBerangkat, IdJurusan, KodeJadwal, 
				JamBerangkat, NoTiket, Nama, 
				TglLahir, Telp, HP, 
				PlanAsuransi, BesarPremi, PetugasTransaksi, 
				WaktuTransaksi, CabangTransaksi, FlagBatal)
			VALUES(
				'$tgl_berangkat', '$id_jurusan', '$kode_jadwal', 
				'$jam_berangkat', '$no_tiket', '$nama', 
				'$tgl_lahir', '$telp', '$hp', 
				'$plan_asuransi', '$besar_premi', '$petugas_transaksi', 
				'$waktu_transaksi', '$cabang_transaksi', '$flag_batal'
			)";
				
		if (!$db->sql_query($sql)){
			die_error("Error $this->ID_FILE ".__LINE__);
		}
		
		return true;
	}
	
	function ubahAsuransi(
		$id_asuransi, $tgl_berangkat, $id_jurusan, 
		$kode_jadwal, $jam_berangkat, $no_tiket, 
		$nama, $tgl_lahir, $telp, 
		$hp, $plan_asuransi, $besar_premi, 
		$petugas_transaksi, $waktu_transaksi, $cabang_transaksi, 
		$flag_batal){
	  
		/*
		IS	: data plan asuransi sudah ada dalam database
		FS	:Data plan asuransi baru telah diubah dalam database 
		*/
		
		//kamus
		global $userdata;
		global $db;
		global $LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR;
		
		//ASPEK KEAMANAN ==========================================================================================
		if (!in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR))){
			$kondisi_pengubahan_cso = " AND FlagBatal=1";
		}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_asuransi SET
				TglBerangkat='$tgl_berangkat', IdJurusan='$id_jurusan', KodeJadwal='$kode_jadwal', 
				JamBerangkat='$jam_berangkat', NoTiket='$no_tiket', Nama='$nama', 
				TglLahir='$tgl_lahir', Telp='$telp', HP='$hp', 
				PlanAsuransi='$plan_asuransi', BesarPremi='$besar_premi', PetugasTransaksi='$petugas_transaksi', 
				WaktuTransaksi='$waktu_transaksi', CabangTransaksi='$cabang_transaksi', FlagBatal='$flag_batal'
			WHERE IdAsuransi='$id_asuransi' $kondisi_pengubahan_cso";
				
		if (!$db->sql_query($sql)){
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
		return $db->sql_affectedrows();
	}
	
	function ubahFlagBatalAsuransi($list_no_tiket, $flag_batal, $id_petugas){
	  
		/*
		IS	: data plan asuransi sudah ada dalam database
		FS	:Data plan asuransi baru telah diubah dalam database 
		*/
		
		//kamus
		global $userdata;
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_asuransi SET FlagBatal='$flag_batal', WaktuTransaksi=NOW(),PetugasTransaksi=$id_petugas
			WHERE NoTiket IN($list_no_tiket) AND FlagBatal=1";
				
		if (!$db->sql_query($sql)){
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapusAsuransi($no_tiket){
	  
		/*
		IS	: data asuransi sudah ada dalam database
		FS	:Data asuransi dihapus
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//MENGHAPUS DATA DALAM DATABASE
		$sql =
			"DELETE FROM tbl_asuransi
			WHERE NoTiket IN('$no_tiket')";
								
		if (!$db->sql_query($sql)){
			return 3;
			die_error("ERR: $this->ID_FILE".__LINE__);
		}
		
		return 1;
	}//end hapus
	
	function ambilDataDetailByNoTiket($no_tiket){
		
		/*
		Desc	:Mengembalikan data asuransi sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				IdAsuransi, TglBerangkat, IdJurusan, 
				KodeJadwal, JamBerangkat, NoTiket, 
				Nama, TglLahir, Telp, 
				HP, PlanAsuransi, BesarPremi, 
				PetugasTransaksi, WaktuTransaksi, CabangTransaksi, 
				FlagBatal
			FROM tbl_asuransi
			WHERE NoTiket='$no_tiket'";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		} 
		else{
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
		$data_asuransi['IdAsuransi']			= $row['IdAsuransi'];
		$data_asuransi['TglBerangkat']		= $row['TglBerangkat'];
		$data_asuransi['IdJurusan']				= $row['IdJurusan'];
		$data_asuransi['KodeJadwal']			= $row['KodeJadwal'];
		$data_asuransi['JamBerangkat']		= $row['JamBerangkat'];
		$data_asuransi['NoTiket']					= $row['NoTiket'];
		$data_asuransi['Nama']						= $row['Nama'];
		$data_asuransi['TglLahir']				= $row['TglLahir'];
		$data_asuransi['Telp']						= $row['Telp'];
		$data_asuransi['HP']							= $row['HP'];
		$data_asuransi['PlanAsuransi']		= $row['PlanAsuransi'];
		$data_asuransi['BesarPremi']			= $row['BesarPremi'];
		$data_asuransi['PetugasTransaksi']= $row['PetugasTransaksi'];
		$data_asuransi['WaktuTransaksi']	= $row['WaktuTransaksi'];
		$data_asuransi['CabangTransaksi']	= $row['CabangTransaksi'];
		$data_asuransi['FlagBatal']				= $row['FlagBatal'];
		
		$sql = "SELECT NamaPlan FROM tbl_md_plan_asuransi WHERE IdPlanAsuransi='$data_asuransi[PlanAsuransi]'";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		} 
		else{
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
		$data_asuransi['NamaPlanAsuransi']	= $row['NamaPlan'];
		
		return	$data_asuransi;
		
	}//  END ambilDataDetailByNoTiket
	
	function ambilTglLahirTerakhir($no_telp){
		
		/*
		Desc	:Mengembalikan data asuransi sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT TglLahir
			FROM tbl_asuransi
			WHERE HP='$no_telp' OR Telp='$no_telp' ORDER BY TglBerangkat DESC LIMIT 0,1";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row['TglLahir'];
		} 
		else{
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilTglLahirTerakhir
	
	function ambilTotalPendapatanAsuransi($tgl_awal,$tgl_akhir,$petugas_transaksi=""){
		
		/*
		Desc	:Mengembalikan data asuransi sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$kondisi_petugas	=($petugas_transaksi=="")?"":" AND PetugasTransaksi=$petugas_transaksi";
			
		
		$sql = 
			"SELECT IS_NULL(SUM(BesarPremi),0)
			FROM tbl_asuransi
			WHERE (WaktuTransaksi BETWEEN '$tgl_awal 00:00:00' AND '$tgl_akhir 23:59:59') AND FlagBatal!=1 $kondisi_petugas";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			die_error("Error $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilTotalPendapatanAsuransi
}
?>