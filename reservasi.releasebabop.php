<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraBOP.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassKota.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$expired_time = 120; //menit
// LIST
$template->set_filenames(array('body' => 'beritaacara.bop/release.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$BeritaAcaraBOP = new BeritaAcaraBOP();

$mode	= $mode==""?"explore":$mode;

switch($mode){
	case 'explore':
				$kondisi =	$cari==""?"":
			" AND (KodeJadwal LIKE '$cari%'
				OR KodeSopir LIKE '$cari%' 
				OR NamaSopir LIKE '%$cari%'
				OR Keterangan LIKE '%$cari%' 
				OR NamaPembuat LIKE '%$cari%'
				OR NamaReleaser LIKE '%$cari%')";
		
		$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan))='$kota'":"";
		$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan)='$asal'":"";
		$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tpk.IdJurusan)='$tujuan'":"";
		
		$order	=($order=='')?"DESC":$order;
			
		$sort_by =($sort_by=='')?"IsRelease ASC,WaktuTransaksi":$sort_by;
		
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging		= pagingData($idx_page,"1","tbl_ba_bop",
		"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
		"WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"beritaacara.bop.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql	=
			"SELECT *,IF(TIME_TO_SEC(TIMEDIFF(WaktuTransaksi,NOW()))/60>=-$expired_time OR WaktuTransaksi IS NULL,0,1) AS IsExpired
			FROM tbl_ba_bop
			WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
				AND ((TIME_TO_SEC(TIMEDIFF(WaktuRelease,NOW()))/60>=-120 AND IsRelease=1) OR WaktuRelease IS NULL)
			$kondisi
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";
		
			
		if(!$result = $db->sql_query($sql)){
			die_error("Gagal eksekusi query!",__LINE__,"Error Code","");
		}
		
		$i=1;
		
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
				
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$show_header	= ($i%$config['repeatshowheader']!=0 && $i!=1)?"none":"";
			
			if($row['IsRelease']==1){
				$odd="green";
				$act= "<span class='b_print' onClick=\"Start('".append_sid("reservasi.releasebabop.cetak.php?id=".$row['Id'])."');\" title='cetak' >";
			}
			elseif($row['IsExpired']==0){
				$act= "<span class='b_ok' onClick=\"bayarBO('".$row['Id']."');\" title='bayar' >";
			}
			else{
				$odd="red";
				$act= "EXPIRED";
			}
			
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'no'=>$i+$idx_page*$VIEW_PER_PAGE,
						'tglberangkat'=>dateparse(FormatMySQLDateToTglWithTime($row['TglBerangkat'])),
						'kodebody'=>$row['KodeKendaraan'],
						'namasopir'=>$row['NamaSopir'],
						'kodejadwal'=>$row['KodeJadwal'],
						'jamberangkat'=>substr($row['JamBerangkat'],0,5),
						'nospj'=>$row['NoSPJ'],
						'waktubuat'=>dateparse(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])),
						'dibuatoleh'=>$row['NamaPembuat'],
						'jenisbiaya'=>$LIST_JENIS_BIAYA[$row['JenisBiaya']],
						'jumlah'=>"Rp.".number_format($row['Jumlah'],0,",","."),
						'keterangan'=>$row['Keterangan'],
						'releaser'=>$row['NamaReleaser']==""?"Belum Released":$row['NamaReleaser'],
						'waktureleased'=>$row['WaktuRelease']==""?"Belum Released":dateparse(FormatMySQLDateToTglWithTime($row['WaktuRelease'])),
						'act'=>$act,
					)
				);
			$i++;
		}
		
		if($i-1<=0){
			$no_data	=	"<div style='width:100%;' class='yellow' align='center'><font size=3><b>data tidak ditemukan</b></font></div>";
		}

    $Kota = new Kota();

		$page_title	= "Berita Acara Biaya Operasional";
		
		$template->assign_vars(array(
			'ACTION_CARI'		=> append_sid('reservasi.releasebabop.'.$phpEx),
			'PAGING'				=> $paging,
			'TGL_AWAL'			=> $tanggal_mulai,
			'TGL_AKHIR'			=> $tanggal_akhir,
			'OPT_KOTA'			=> $Kota->setComboKota($kota),
			'TXT_CARI'			=> $cari,
			'KOTA'					=> $kota,
			'ASAL'					=> $asal,
			'TUJUAN'				=> $tujuan,
			'NO_DATA'				=> $no_data,
			)
		);
						
		
		include($adp_root_path . 'includes/page_header.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
		
		exit;
	case 'getasal':
		$Cabang	= new Cabang();
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		$Cabang	= new Cabang();
		
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;
	
	case 'bayar':
		$BeritaAcaraBOP= new BeritaAcaraBOP();
		$BiayaOperasional = new BiayaOperasional();
		
		//MENYIMPAN BERITA ACARA
		$id	= $HTTP_POST_VARS["id"];
		
		if($BeritaAcaraBOP->bayar($id,$userdata['user_id'],$userdata['nama'])){
			$data_ba = $BeritaAcaraBOP->ambilDetail($id);
			
			if($data_ba["IsRelease"]==1){
				$BiayaOperasional->tambah($data_ba['NoSPJ'],"",$data_ba['JenisBiaya'],$data_ba["KodeKendaraan"],$data_ba["KodeSopir"],$data_ba["Jumlah"],$userdata["user_id"],$data_ba["KodeJadwal"],$userdata['KodeCabang'],$data_ba['Keterangan']);
			}
			echo("proses_ok=true;window.location.reload();Start('reservasi.releasebabop.cetak.php?sid=$userdata[session_id]&id=$id');");
		}
		else{
			echo("proses_ok=true;alert('Data tidak ditemukan!');");
		}
		
	exit;
}
?>