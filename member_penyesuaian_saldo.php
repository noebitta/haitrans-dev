<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassMemberTransaksi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']!=$LEVEL_ADMIN){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode	= $HTTP_GET_VARS['mode'];
$mode	=($mode=='')?"blank":$mode;

$data_member;
$body		= "";
$valid	= true;

$TransaksiMember= new TransaksiMember();
$Member					= new Member();

switch ($mode){
	case 'blank':
		
		$id_mutasi	= $HTTP_GET_VARS['id_mutasi'];
		
		$body=
			"
			<table width='80%'>
				<tr>
					<td colspan=3 align='center'>Silahkan masukkan koreksi jumlah uang</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input' name='frm_input' action=\"".append_sid('member_penyesuaian_saldo.'.$phpEx)."&mode=konfirmasi_proses\" method='post'>
							<input type='hidden' id='hdn_id_mutasi' name='hdn_id_mutasi' value='$id_mutasi' />
							Rp. <input type='text' id='jumlah_uang' name='jumlah_uang' /> <input type='submit' value='&nbsp;&nbsp;OK&nbsp;&nbsp;' />
						</form>
					</td>
				</tr>
			</table>
			<input type='button' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;' onClick='window.close();'/>";
		
	break;
	
	case 'konfirmasi_proses':
		
		$id_mutasi		= trim($HTTP_POST_VARS['hdn_id_mutasi']);
		$jumlah_uang	= trim($HTTP_POST_VARS['jumlah_uang']);
		
		$data_transaksi	=$TransaksiMember->ambilDetailMutasi($id_mutasi);
		$jenis_mutasi		=($data_transaksi['jenis_transaksi']==0)?"Mutasi debet":",mutasi kredit";
		
		$body=
			"
			<table width='80%'>
				<tr>
					<td colspan=3 align='left' valign='top'><h3>Data transaksi yang anda pilih dan ubah adalah</h3></td>
				</tr>
				<tr>
					<td valign='top' >ID Mutasi</td><td valign='top'>:</td><td>$data_transaksi[id_mutasi]</td>
				</tr>
				<tr>
					<td valign='top'>Waktu transaksi</td valign='top'><td>:</td><td>$data_transaksi[waktu_transaksi]</td>
				</tr>
				<tr>
					<td valign='top'>Keterangan</td><td valign='top'>:</td><td>$data_transaksi[keterangan]</td>
				</tr>
				<tr>
					<td valign='top'>Jenis mutasi</td><td valign='top'>:</td><td>$jenis_mutasi</td>
				</tr>
				<tr>
					<td valign='top'>Jumlah</td><td valign='top'>:</td><td>Rp. ".number_format($data_transaksi['jumlah_mutasi'],0,",",".")."</td>
				</tr>
				<tr>
					<td valign='top'>Jumlah Koreksi</td><td valign='top'>:</td><td><strong>Rp. ".number_format($jumlah_uang,0,",",".")."</strong></td>
				</tr>
			</table>
			<a href='#' onClick='javascript:history.back();'><< Kembali</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type='button' value='&nbsp;&nbsp;Lakukan proses koreksi&nbsp;&nbsp;' onClick='konfirmPassword();'/>";
			
			$parameter="
				<input type='hidden' id='id_mutasi' name='id_mutasi' value='$id_mutasi' />
				<input type='hidden' id='id_member' name='id_member' value='$data_transaksi[id_member]' />
				<input type='hidden' id='jumlah_uang' name='jumlah_uang' value='$jumlah_uang' />";
			
	break;
	
	//TRANSAKSI  =============================================================================================
	case 'proses_transaksi':
				
		$id_mutasi		= trim($HTTP_POST_VARS['id_mutasi']);		
		$id_member		= trim($HTTP_POST_VARS['id_member']);		
		$jumlah_uang	= trim($HTTP_POST_VARS['jumlah_uang']);		
		$password			= trim($HTTP_POST_VARS['password']);		
		
		$useraktif	= $userdata['username'];
		
		//memeriksa otorisasi password
		$sql = 
			"SELECT user_password FROM tbl_user WHERE username='$useraktif';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$password_user=$row['user_password'];
		} 
		else{
			die_error("Gagal melakukan otorisasi password ");
		}
		
		if($password_user==md5($password)){// MEMERIKSA OTORISASI USER
			//JIKA PASSWORD BENAR
			
			// koreksi deposit
			$return=$TransaksiMember->koreksiMutasiAccount($id_member,$id_mutasi,$jumlah_uang);
			if($return!="false") {
				$body	=	
					"<h2>KOREKSI SALDO BERHASIL DILAKUKAN! $return</h2>
					<a href='javascript:window.close()'>Tutup window</a>";
			}
			else{
				$body	=	
					"<h2><font color='red'>Gagal melakukan proses koreksi</font></h2><br>
					<a href='javascript:history.back()'>Kembali</a><br>";
			}
		}
		else{
			$body	=	
				"<h2><font color='red'>Password anda tidak benar!</font></h2><br>
				<a href='javascript:history.back()'>Kembali</a><br>";
		}
		


	break;
}

include($adp_root_path . 'includes/page_header.php');
$template->set_filenames(array('body' => 'member_penyesuaian_saldo.tpl')); 
$template->assign_vars (
	array(
		'BODY'			=> $body,
		'PARAMETER'	=> $parameter
	)
);
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>