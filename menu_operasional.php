<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
//var_dump($userdata);exit;
$userdata = session_pagestart($user_ip,100); 
//init_userprefs($userdata);


// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################
// HEADER

$page_title	= "Menu Operasional";
$interface_menu_utama=true;

// TEMPLATE
switch($userdata['user_level']){
	case $USER_LEVEL_INDEX['CSO']:
    $height_wrapper	= 120;
	redirect(append_sid('reservasi.'.$phpEx.''),true);
	/*
	$template->set_filenames(array('body' => 'menu_operasional_body.tpl'));
    //menu Operasional
    $template->assign_block_vars('menu_operasional',array());

    //sub menu
    $template->assign_block_vars('menu_rsv_reg',array());
    $template->assign_block_vars('menu_rsv_nonreg',array());
	*/
	break;
	
	case $USER_LEVEL_INDEX['CSO_PAKET']:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX['ADMIN'] :
		$height_wrapper	= 420;
		
		$template->set_filenames(array('body' => 'menu_operasional_body.tpl')); 
		//menu Operasional
		$template->assign_block_vars('menu_operasional',array());
		//sub menu
		
		$template->assign_block_vars('menu_segmen1',array());
		
		$template->assign_block_vars('menu_rsv_reg',array());
		//$template->assign_block_vars('menu_rsv_nonreg',array());
		$template->assign_block_vars('menu_penjadwalan',array());
		$template->assign_block_vars('menu_pengumuman',array());

    	$template->assign_block_vars('menu_jenis_discount',array('br'=>'100px'));
		$template->assign_block_vars('menu_status_online',array());
		$template->assign_block_vars('menu_promo',array());
    	$template->assign_block_vars('daftar_manifest',array());

		$template->assign_block_vars('menu_pembatalan',array('br'=>'100px'));
		$template->assign_block_vars('menu_laporan_pembatalan',array());
		$template->assign_block_vars('menu_laporan_koreksi_disc',array());
    	$template->assign_block_vars('menu_dashboard',array());

		$template->assign_block_vars('menu_general_total',array('br'=>'100px'));
		$template->assign_block_vars('menu_berita_acara_insentif_sopir',array());
		$template->assign_block_vars('menu_berita_acara_biayaoperasional',array());
		//END menu Operasional
		
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX['MANAJEMEN'] :
		$height_wrapper	= 420;
		
		$template->set_filenames(array('body' => 'menu_operasional_body.tpl')); 
		//menu Operasional
		$template->assign_block_vars('menu_operasional',array());
		//sub menu
		$template->assign_block_vars('menu_rsv_reg',array());
    	//$template->assign_block_vars('menu_rsv_nonreg',array());
		$template->assign_block_vars('menu_penjadwalan',array());
		$template->assign_block_vars('menu_pengumuman',array());

    	$template->assign_block_vars('menu_jenis_discount',array('br'=>'100px'));
		$template->assign_block_vars('menu_status_online',array());
		$template->assign_block_vars('menu_promo',array());
		$template->assign_block_vars('daftar_manifest',array());

		$template->assign_block_vars('menu_pembatalan',array('br'=>'100px'));
		$template->assign_block_vars('menu_laporan_pembatalan',array());
		$template->assign_block_vars('menu_laporan_koreksi_disc',array());
		$template->assign_block_vars('menu_dashboard',array());

		$template->assign_block_vars('menu_general_total',array('br'=>'100px'));
		$template->assign_block_vars('menu_berita_acara_insentif_sopir',array());
		$template->assign_block_vars('menu_berita_acara_biayaoperasional',array());
		//END menu Operasional

		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_tiketux',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX['MANAJER'] :
		$height_wrapper	= 420;
		
		$template->set_filenames(array('body' => 'menu_operasional_body.tpl')); 
		//menu Operasional
		$template->assign_block_vars('menu_operasional',array());
		//sub menu
		$template->assign_block_vars('menu_rsv_reg',array());
    	//$template->assign_block_vars('menu_rsv_nonreg',array());
		$template->assign_block_vars('menu_penjadwalan',array());
		$template->assign_block_vars('menu_pengumuman',array());

		$template->assign_block_vars('menu_jenis_discount',array('br'=>'100px'));
		$template->assign_block_vars('menu_status_online',array());
		$template->assign_block_vars('menu_promo',array());
		$template->assign_block_vars('daftar_manifest',array());

		$template->assign_block_vars('menu_pembatalan',array('br'=>'100px'));
		$template->assign_block_vars('menu_laporan_pembatalan',array());
		$template->assign_block_vars('menu_laporan_koreksi_disc',array());
		$template->assign_block_vars('menu_dashboard',array());
		//$template->assign_block_vars('menu_kasbon_sopir',array());
		//END menu Operasional
		
		
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_pengelolaan_member',array());
		$template->assign_block_vars('menu_pengaturan',array());
	break;
	
	case $USER_LEVEL_INDEX['SPV_RESERVASI'] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_operasional_body.tpl')); 
		//menu Operasional
		$template->assign_block_vars('menu_operasional',array());
		//sub menu
		$template->assign_block_vars('menu_rsv_reg',array());
    	//$template->assign_block_vars('menu_rsv_nonreg',array());
    	$template->assign_block_vars('menu_penjadwalan',array());
		$template->assign_block_vars('menu_pengumuman',array());

		$template->assign_block_vars('menu_pembatalan',array('br'=>'100px'));
		$template->assign_block_vars('menu_laporan_pembatalan',array());
		//END menu Operasional
		
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
	break;
	
	case $USER_LEVEL_INDEX['SPV_OPERASIONAL'] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_operasional_body.tpl')); 
		//menu Operasional
		$template->assign_block_vars('menu_operasional',array());
		//sub menu
		$template->assign_block_vars('menu_rsv_reg',array());
    	//$template->assign_block_vars('menu_rsv_nonreg',array());
		$template->assign_block_vars('menu_penjadwalan',array());
		$template->assign_block_vars('menu_pengumuman',array());

		$template->assign_block_vars('menu_pembatalan',array('br'=>'100px'));
		$template->assign_block_vars('menu_dashboard',array());
		$template->assign_block_vars('menu_berita_acara_insentif_sopir',array());
		$template->assign_block_vars('menu_berita_acara_biayaoperasional',array());
		
		//MENU UTAMA
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_paket',array());
		$template->assign_block_vars('menu_pengaturan',array());
		
	break;
	
	case $USER_LEVEL_INDEX['SCHEDULER'] :
		$height_wrapper	= 220;
		
		$template->set_filenames(array('body' => 'menu_operasional_body.tpl')); 
		//menu Operasional
		$template->assign_block_vars('menu_operasional',array());
		//sub menu
		$template->assign_block_vars('menu_rsv_reg',array());
    	//$template->assign_block_vars('menu_rsv_nonreg',array());
		$template->assign_block_vars('menu_penjadwalan',array());
		$template->assign_block_vars('menu_pengumuman',array());

		$template->assign_block_vars('menu_dashboard',array('br'=>'100px'));
		//END menu Operasional
	break;
	
	case $USER_LEVEL_INDEX['KEUANGAN'] :
		$height_wrapper	= 120;
		
		$template->set_filenames(array('body' => 'menu_operasional_body.tpl')); 
		
		//menu Operasional
		$template->assign_block_vars('menu_operasional',array());
		//sub menu
		$template->assign_block_vars('daftar_manifest',array());
		//$template->assign_block_vars('menu_kasbon_sopir',array());
		//END menu Operasional
		
		
		$template->assign_block_vars('menu_laporan_reservasi',array());
		$template->assign_block_vars('menu_laporan_keuangan',array());
		$template->assign_block_vars('menu_tiketux',array());
	break;
	
	case $USER_LEVEL_INDEX['CUSTOMER_CARE'] :
		$template->set_filenames(array('body' => 'menu_body_6.tpl')); 
	break;
	
}

$template->assign_vars(array
  ( 'BCRUMP'    				=>'<a href="'.append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional') .'">Home',
    'U_RSV_REG'   		  =>append_sid('reservasi.'.$phpEx.''),
    'U_RSV_NONREG'   		=>append_sid('reservasi.nonreguler.'.$phpEx.''),
		'U_PENJADWALAN' 		=>append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx.''),
		'U_PENGUMUMAN'  		=>append_sid('pengaturan_pengumuman.'.$phpEx.''),
		'U_JENIS_DISCOUNT'	=>append_sid('jenis_discount.'.$phpEx),
		'U_USER_LOGIN'			=>append_sid('pengaturan_user_list_login.'.$phpEx),
		'U_DAFTAR_MANIFEST'	=>append_sid('daftar_manifest.'.$phpEx.''),
		'U_PROMO'						=>append_sid('pengaturan_promo.'.$phpEx),
		'U_MEMO'						=>append_sid('pengaturan_memo.'.$phpEx),
		'U_BATAL' 					=>append_sid('pembatalan.'.$phpEx.''),
    'U_LOG_BATAL'				=>append_sid('laporan_pembatalan.'.$phpEx),
		'U_LOG_KOREKSI_DISC'=>append_sid('laporan_koreksi_disc.'.$phpEx),
		'U_SURAT_JALAN'			=>append_sid('surat_jalan.'.$phpEx),
    'U_DASHBOARD'				=>append_sid('dashboardops.'.$phpEx),
		'U_GT'							=>append_sid('generaltotal.'.$phpEx),
		'U_BERITA_ACARA'		=>append_sid('beritaacara.insentifsopir.'.$phpEx),
		'U_BERITA_ACARA_BOP'=>append_sid('beritaacara.bop.'.$phpEx),
		'HEIGHT_WRAPPER'		=>$height_wrapper
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>