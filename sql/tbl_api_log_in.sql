/*
Navicat MySQL Data Transfer

Source Server         : MySQL - Local
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : arnes

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-02-23 19:13:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_api_log_in
-- ----------------------------
DROP TABLE IF EXISTS `tbl_api_log_in`;
CREATE TABLE `tbl_api_log_in` (
  `in_id` int(11) NOT NULL AUTO_INCREMENT,
  `in_request_uri` varchar(100) NOT NULL,
  `in_request_query` text NOT NULL,
  `in_request_method` varchar(5) NOT NULL,
  `in_user_id` int(11) NOT NULL,
  `in_access_token` varchar(150) NOT NULL,
  `in_client_type` varchar(30) NOT NULL,
  `in_client_id` varchar(150) NOT NULL,
  `in_hh_manufacturer` varchar(50) NOT NULL,
  `in_hh_model` varchar(50) NOT NULL,
  `in_hh_os` varchar(50) NOT NULL,
  `in_hh_sdk` smallint(6) NOT NULL,
  `in_hh_screen` varchar(12) NOT NULL,
  `in_hh_imei` varchar(50) NOT NULL,
  `in_network_operator` varchar(50) NOT NULL,
  `in_subscriber_id` varchar(50) NOT NULL,
  `in_msisdn` varchar(50) NOT NULL,
  `in_app_version_code` smallint(6) NOT NULL,
  `in_app_version_name` varchar(10) NOT NULL,
  `in_ip_address` varchar(25) NOT NULL,
  `in_user_agent` text NOT NULL,
  `in_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `in_response` text NOT NULL,
  PRIMARY KEY (`in_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1784 DEFAULT CHARSET=latin1;
