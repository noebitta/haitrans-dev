/*
Navicat MySQL Data Transfer

Source Server         : MySQL - Local
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : arnes

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-02-23 19:13:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_auth_client
-- ----------------------------
DROP TABLE IF EXISTS `tbl_auth_client`;
CREATE TABLE `tbl_auth_client` (
  `client_id` varchar(150) NOT NULL,
  `client_secret` varchar(150) NOT NULL,
  `client_name` varchar(50) NOT NULL,
  `client_email` varchar(100) NOT NULL,
  `client_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_auth_client
-- ----------------------------
INSERT INTO `tbl_auth_client` VALUES ('ANDROID', 'b014dcc2a3a984ebaea090dd96e6d0fc', 'Daytans Android', 'android@daytrans.tiketux.com', '2015-08-11 06:12:27');
