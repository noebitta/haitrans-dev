/*
Navicat MySQL Data Transfer

Source Server         : MySQL - Local
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : tiketux_baraya

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-02-23 22:14:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_otp_manifest
-- ----------------------------
DROP TABLE IF EXISTS `tbl_otp_manifest`;
CREATE TABLE `tbl_otp_manifest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdJurusan` int(10) DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `OTP` int(6) DEFAULT NULL,
  `OTPUsed` int(1) DEFAULT NULL,
  `PetugasRequest` int(10) DEFAULT NULL,
  `WaktuRequest` datetime DEFAULT NULL,
  `UsedBy` int(10) DEFAULT NULL,
  `WaktuDigunakan` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
