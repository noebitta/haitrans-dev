<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraBOP.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$expired_time = 120; //menit

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$kondisi =	$cari==""?"":
	" AND (KodeJadwal LIKE '$cari%'
		OR KodeSopir LIKE '$cari%' 
		OR NamaSopir LIKE '%$cari%'
		OR Keterangan LIKE '%$cari%' 
		OR NamaPembuat LIKE '%$cari%'
		OR NamaReleaser LIKE '%$cari%')";

$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan))='".strClean($kota)."'":"";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tpk.IdJurusan)='".strClean($asal)."'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tpk.IdJurusan)='".strClean($tujuan)."'":"";

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"WaktuTransaksi":$sort_by;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Berita Acara Biaya Operasional -filter- Cari:'.$cari.' Kota:'.$kota.',Asal:'.$asal.',Tujuan:'.$tujuan.' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Tgl.Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Kode Body');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Sopir');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Kode Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jam');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'No.Manifest');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Waktu BA');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'BA Oleh');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Jenis Biaya');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Jumlah');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Releaser');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Waktu Released');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

$sql	=
	"SELECT *,IF(TIME_TO_SEC(TIMEDIFF(WaktuTransaksi,NOW()))/60>=-$expired_time OR WaktuTransaksi IS NULL,0,1) AS IsExpired
	FROM tbl_ba_bop
	WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	ORDER BY $sort_by $order;";

	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+3;
	
	
	if($row['IsRelease']==1){
		$status= "RELEASED";
	}
	elseif($row['IsExpired']==0){
		$status= "BELUM RELEASED";
	}
	else{
		$status= "EXPIRED";
	}
	
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeKendaraan']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['NamaSopir']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, substr($row['JamBerangkat'],0,5));
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NoSPJ']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, dateparse(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])));	
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['NamaPembuat']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $LIST_JENIS_BIAYA[$row['JenisBiaya']]);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Jumlah']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['Keterangan']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $row['NamaReleaser']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['WaktuRelease']==""?"":dateparse(FormatMySQLDateToTglWithTime($row['WaktuRelease'])));
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $status);
	
}
$temp_idx=$idx_row;

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Berita Acara Biaya Operasional '.$tanggal_mulai.' s/d '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}

  
?>
