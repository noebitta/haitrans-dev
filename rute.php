<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']>=$LEVEL_MANAJER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act    = $HTTP_GET_VARS['act'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

switch($mode){

//TAMPILKAN RUTE BARU ==========================================================================================================
case 'tampilkan_rute':
	
	$cari		= $HTTP_GET_VARS['txt_cari'];
	$asal   = $HTTP_GET_VARS['rute_asal'];
	$tujuan = $HTTP_GET_VARS['rute_tujuan'];
	
		
	$kondisi_pencarian='';
	
	if($cari!=''){
		$kondisi_pencarian .= " AND ((kode LIKE '%$cari%')";
		$kondisi_pencarian .= " OR (asal LIKE '%$cari%')";
		$kondisi_pencarian .= " OR (tujuan LIKE '$cari')";
		$kondisi_pencarian .= " OR (jenis LIKE '$cari')";
		$kondisi_pencarian .= " OR (nopol LIKE '$cari'))";
	}
	
	$kondisi_asal			= "asal LIKE '$asal%'";
	$kondisi_tujuan		= "tujuan LIKE '$tujuan%'";
	
	
	$kondisi	= 'WHERE '.$kondisi_asal." AND ".$kondisi_tujuan.$kondisi_pencarian;
	
	$sql = "SELECT 
						kode, asal, tujuan,CONVERT(VARCHAR(10),jam,114) as jam, 
						jenis, harga,kursi,nopol,aktif
					FROM	TbMDJadwal ".$kondisi." ORDER BY kode";
			
	if (!$result = $db->sql_query($sql)){
		die_error('GAGAL mengambil data',__FILE__,__LINE__,$sql);
	}
	else {
		
		$hasil=			
			"<table width='100%' class='border'>
		    <tr>
		      <th>#</th>
					<th>Kode</th>
					<th>Asal</th>
					<th>Tujuan</th>
					<th>Jam</th>
					<th>Harga tiket</th>
					<th>No Polisi</th>
					<th>Jenis</th>
					<th>Kapasitas kursi</th>
					<th>Status</th>
					<th>Aksi</th>
		    </tr>";
		
		while ($row=$db->sql_fetchrow($result)){   
			$i++;
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			//memberikan warna merah untuk jadwal yang tidak aktif
			$odd	=($row['aktif']==1)? $odd:'red';
			
			$id = $row['kode'];
									
			$status=($row['aktif']==1)?"<a onclick=\"UbahStatusAktif('$row[kode]');\" href='##'>Aktif</a>":"<a onclick=\"UbahStatusAktif('$row[kode]');\" href='##'>Nonaktif</a>";
			$action = "<a href='".append_sid('rute_detail.'.$phpEx)."&kode=$id&mode=ambil_data_rute'>Edit</a>+<a onclick=\"TanyaHapus('$id');\" href='##'>Delete</a>";
			
			$hasil .=
		    "<tr bgcolor='D0D0D0'>
		       <td class='$odd'><div align='right'>$i</div></td>
		       <td class='$odd'><div align='left'>$row[kode]</div></td>
		       <td class='$odd'><div align='left'>$row[asal]</div></td>
		       <td class='$odd'><div align='left'>$row[tujuan]</div></td>
		       <td class='$odd'><div align='left'>$row[jam]</div></td>
		       <td class='$odd'><div align='right'>".number_format($row['harga'],0,",",".")."</div></td>
		       <td class='$odd'><div align='left'>$row[nopol]</div></td>
		       <td class='$odd'><div align='left'>$row[jenis]</div></td>
					 <td class='$odd'><div align='right'>$row[kursi]</div></td>
					 <td class='$odd'><div align='center'>$status</div></td>
					 <td class='$odd'><div align='center'>$action</div></td>
		     </tr>";
			
		}
		
		//jika tidak ditemukan data pada database
		if($i==0){
			$hasil .=
				"<table width='100%' class='border'>
					<tr><td align='center' bgcolor='EFEFEF'>
						<font color='red'><strong>Data tidak ditemukan!</strong></font>
					</td></tr>
				</table><br><br>";
		}
		
		$hasil .="</table>";
	}
	
	echo($hasil);
exit;

//HAPUS RUTE BARU ==========================================================================================================
case 'hapus_rute':
	$kode_rute    = $HTTP_GET_VARS['kode_rute'];  
	
	$sql =
		"DELETE FROM TbMDJadwal 
		WHERE (kode='$kode_rute')";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('GAGAL menghapus data anggota',__FILE__,__LINE__,$sql);
		die_error('GAGAL menghapus data');
	}

exit;
}//switch mode

$template->set_filenames(array('body' => 'rute.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  =>$userdata['username'],
   	'BCRUMP'    =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('rute.'.$phpEx).'">Rute</a>',
   	'U_ADD' 		=>'<a href="'.append_sid('rute_detail.'.$phpEx) .'">Tambah Rute</a>',
		'CARI'	=>$cari,
		'SID'=>$userdata['sid'],
		'ASCENDING'=>$ascending,
		'PESAN'	=>$pesan,
		'CEK_KODE_RUTE'=>$cek_kode_rute, 					
		'CEK_POINT_ASAL'=>$cek_point_asal, 							
		'CEK_JAM_BERANGKAT'=>$cek_jam_berangkat, 				
		'CEK_POINT_TUJUAN'=>$cek_point_tujuan, 					
		'OPT_JENIS_RUTE'=>$opt_jenis_rute,
		'INDEX_HALAMAN'=>$index_halaman,
		'HALAMAN_SKRG'=>$idx_halaman_sekarang,
		'JUMLAH_DATA'=>number_format($jumlah_data,0,",",".")
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>