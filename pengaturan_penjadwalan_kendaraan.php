<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassPenjadwalanKendaraan.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tanggal		= isset($HTTP_GET_VARS['tanggal'])? $HTTP_GET_VARS['tanggal'] : $HTTP_POST_VARS['tanggal'];
$cabang_asal= isset($HTTP_GET_VARS['cabangasal'])? $HTTP_GET_VARS['cabangasal'] : $HTTP_POST_VARS['cabangasal'];
$id_jurusan	= isset($HTTP_GET_VARS['idjurusan'])? $HTTP_GET_VARS['idjurusan'] : $HTTP_POST_VARS['idjurusan'];

if($tanggal==""){
	$tanggal = date("d-m-Y");
	$tombol_cari=false;
}
else{
	$tanggal;
	$tombol_cari=true;
}

$tanggal_mysql	= FormatMySQLDateToTgl($tanggal);

$Jurusan= new Jurusan();
$Sopir	= new Sopir();
$Mobil	= new Mobil();
$Cabang	= new Cabang();
$PenjadwalanKendaraan = new PenjadwalanKendaraan();

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;
	global $Mobil;
			
	$result=$Mobil->ambilDataForComboBox();
	$selected_default	= ($kode_kendaraan!="")?"":"selected";

	$opt_mobil="<option value='' $selected_default>- silahkan pilih kendaraan  -</option>";
	
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
			$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[JumlahKursi] Seat $row[Merek] $row[Jenis]</option>";
		}
	}
	else{
		echo("err :".__LINE__);exit;
	}		
	
	return $opt_mobil;
	//END SET COMBO MOBIL
}

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	global $Sopir;
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	
	$selected_default	= ($kode_sopir_dipilih!="")?"":"selected";
	$opt_sopir="<option value='' $selected_default>- silahkan pilih sopir  -</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
	
}

function setComboCabangAsal($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;
			
	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal," FlagOperasionalJurusan IN (0,2,3)");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

//OPERATION SELECTOR
$mode=$mode==""?"exp":$mode;

switch($mode){
  case "exp":  /*MODE EXPLORE*******************************************************************************************/

    if($tombol_cari){

      $data_jurusan = $Jurusan->ambilDataDetail($id_jurusan);

      $kolom_status = "Status";

      /*MEMERIKSA JENIS PENGOPERASIAN JURUSAN, JIKA NON REGULER, AKAN MEMUNCULKAN TOMBOL TAMBAH JADWAL*/
      if($data_jurusan["FlagOperasionalJurusan"]!=3){
        /*JIKA JENIS OPERASIONAL JADWAL ADALAH REGULER*/
        $kondisi	= ($id_jurusan!='')?" WHERE tmj.IdJurusan LIKE $id_jurusan ":"";

        //PAGING======================================================
        $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
        $paging		= pagingData($idx_page,"KodeJadwal","tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'",
          "&tanggal=$tanggal&cabangasal=$cabang_asal&idjurusan=$id_jurusan",
          $kondisi,"pengaturan_penjadwalan_kendaraan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
        //END PAGING======================================================

        $sql=
          "SELECT tmj.KodeJadwal,tmj.IdJurusan,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan)) as Asal,
					f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tmj.IdJurusan)) as Tujuan,
					tmj.JamBerangkat,IdPenjadwalan,tpk.StatusAktif,NoPolisi,KodeKendaraan,
					IF(tpk.LayoutKursi IS NULL,tmj.JumlahKursi,tpk.LayoutKursi) AS JumlahKursi,KodeDriver,NamaDriver,
					FlagAktif	AS FlagAktifDefault,tpk.StatusAktif AS FlagAktif,tpk.Remark
				FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'
				$kondisi
				ORDER BY tmj.KodeJadwal,tmj.JamBerangkat LIMIT $idx_awal_record,$VIEW_PER_PAGE";

      }
      else{
        /*JIKA JENIS OPERASIONAL JADWAL ADALAH NON-REGULER*/
        $template->assign_block_vars("TOMBOLACTION",array());
        $kolom_status = "Action";
        $is_non_reguler = true;

        $kondisi	= "WHERE IdJurusan LIKE $id_jurusan AND TglBerangkat='$tanggal_mysql'";

        //PAGING======================================================
        $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
        $paging		= pagingData($idx_page,"KodeJadwal","tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'",
          "&tanggal=$tanggal&cabangasal=$cabang_asal&idjurusan=$id_jurusan",
          $kondisi,"pengaturan_penjadwalan_kendaraan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
        //END PAGING======================================================

        $sql=
          "SELECT
            KodeJadwal,
            f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) as Asal,
					  f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) as Tujuan,
					  JamBerangkat,IdPenjadwalan,StatusAktif,NoPolisi,KodeKendaraan,
					  LayoutKursi AS JumlahKursi,KodeDriver,NamaDriver,
					  StatusAktif AS FlagAktif, StatusAktif AS FlagAktifDefault,Remark
				FROM tbl_penjadwalan_kendaraan tpk
				$kondisi
				ORDER BY JamBerangkat LIMIT $idx_awal_record,$VIEW_PER_PAGE";
      }

      $idx_check=0;

      if (!$result = $db->sql_query($sql)){
        echo("Err:".__LINE__);exit;
      }

      $i = $idx_page*$VIEW_PER_PAGE+1;

      while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
          $odd = 'even';
        }

        if($row['FlagAktif']==1 || ($row['FlagAktif']=='' && $row['FlagAktifDefault']==1)){
          $status="<a href='' onClick='return ubahStatus(\"$row[IdPenjadwalan]\")'>Aktif</a>";
        }
        else{
          $odd	= "red";
          $status="<a href='' onClick='return ubahStatus(\"$row[IdPenjadwalan]\")'>Nonaktif</a>";
        }

        //JADWAL BELUM PERNAH DIBUAT
        if($row['IdPenjadwalan']==''){
          $flag_status_invert	= 1-$row['FlagAktifDefault'];

          if($row['FlagAktifDefault']==1){
            $label_status	= "Aktif";
          }
          else{
            $odd	= "red";
            $label_status	= "Nonaktif";
          }

          $status="<a href='#' onClick='simpan(\"$tanggal\",\"$row[KodeJadwal]\",$flag_status_invert); return false;'>$label_status</a>";
        }

        if($row['NoPolisi']=='' && $row['FlagAktif']==1 && !$is_non_reguler){
          $odd	= "pink";
        }

        $opt_kendaraan	= "<span style='width: 100px;margin-left: 5px;'>".($row['KodeKendaraan']==""?"<font color='".($odd!='red'?"red":"white")."'>BELUM DIATUR</font></span>":"<b>$row[KodeKendaraan]</b> | $row[NoPolisi]</span>")." <input style='font-size: 12px; width: 25px;margin-left: 5px;position: static;' type='button' value='...' onclick='setDaftarMobil(\"$row[KodeKendaraan]\",\"$row[KodeJadwal]\",\"$tanggal_mysql\",1);'/>";

        $opt_sopir			= "<span style='width: 100px;margin-left: 5px;'>".($row['KodeDriver']==""?"<font color='".($odd!='red'?"red":"white")."'>BELUM DIATUR</font></span>":"<b>$row[NamaDriver]</b> | $row[KodeDriver]</span>")." <input style='font-size: 12px; width: 25px;margin-left: 5px;position: static;' type='button' value='...' onclick='setDaftarSopir(\"$row[KodeDriver]\",\"$row[KodeJadwal]\",\"$tanggal_mysql\",1);'/>";

        if($data_jurusan["FlagOperasionalJurusan"]==3){
          /*JIKA JURUSAN ADALAH JURUSAN NON REGULER, JADWAL DAPAT DIHAPUS*/
          $status="<input type='button' onClick='hapus(\"$row[IdPenjadwalan]\");' value='Hapus' />";
        }

        $template->
          assign_block_vars(
            'ROW',
            array(
              'odd'=>$odd,
              'check'=>$check,
              'no'=>$i,
              'kode'=>$row['KodeJadwal'],
              'jurusan'=>$row['Asal']."-".$row['Tujuan'],
              'jam'=>substr($row['JamBerangkat'],0,5),
              'nopol'=>$opt_kendaraan,
              'kode_sopir'=>$opt_sopir,
              'kursi'=>$row['JumlahKursi'],
              'status'=>$status,
              'remark'=>$row['Remark']
            )
          );

        $i++;
      }



      if($i-1<=0){
        $no_data	= "<tr><td colspan='20' style='background-color: yellow;font-size:16px; text-align: center;'>tidak ada jadwal ditemukan</td></tr>";
      }
    }
    else{
      $no_data	= "<tr><td colspan='20' style='background-color: yellow;font-size:16px; text-align: center;'>Silahkan pilih asal dan tujuan lalu klik tombol CARI</td></tr>";
    }

    $page_title = "Penjadwalan";
    $template->set_filenames(array('body' => 'penjadwalan_kendaraan/jadwal_body.tpl'));
    $template->assign_vars(array(
        'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx).'">Jadwal Kendaraan</a>',
        'U_ADD'	        => append_sid('pengaturan_penjadwalan_kendaraan.php?mode=add&tanggal='.$tanggal.'&cabangasal='.$cabang_asal."&idjurusan=".$id_jurusan),
        'ACTION_CARI'		=> append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx),
        'ASAL'					=> $cabang_asal,
        'ID_JURUSAN'		=> $id_jurusan,
        'CABANG_ASAL'		=> $Cabang->setInterfaceComboCabang($cabang_asal),
        'CABANG_TUJUAN'	=> setComboCabangTujuan($cabang_asal,$id_jurusan),
        'TANGGAL'				=> $tanggal,
        'PAGING'				=> $paging,
        'FIRST_LOAD'		=> !$tombol_cari,
        'NO_DATA'				=> $no_data,
        'KOLOM_STATUS'  => $kolom_status
      )
    );

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');

  exit; /*=============================================================================================================*/

  case 'add': /*MODE ADD************************************************************************************************/
    $kode_jadwal    = $HTTP_POST_VARS['kodejadwal']==""?$HTTP_GET_VARS['kodejadwal']:$HTTP_POST_VARS['kodejadwal'];

    $pesan = $HTTP_GET_VARS['pesan'];

    if($pesan==1){
      $pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
      $bgcolor_pesan="98e46f";
    }

    /*MEMBUAT ARRAY LAYOUT KENDARAAN*/
    $data_layout	= $Mobil->getArrayLayout();

    $data_jurusan       = $Jurusan->ambilDataDetail($id_jurusan);
    $data_cabang_asal   = $Cabang->ambilDataDetail($data_jurusan["KodeCabangAsal"]);
    $data_cabang_tujuan = $Cabang->ambilDataDetail($data_jurusan["KodeCabangTujuan"]);

    $kode_jadwal        = $kode_jadwal==""?$PenjadwalanKendaraan->generateKodeJadwalNonReguler($tanggal_mysql,$id_jurusan):$kode_jadwal;

    $page_title = "Tambah Jadwal Non-Reguler";
    $template->set_filenames(array('body' => 'penjadwalan_kendaraan/add_body.tpl'));
    $template->assign_vars(array(
        'BCRUMP'		      => '<a href="'.append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional').'">Home</a> | <a href="'.append_sid('pengaturan_penjadwalan_kendaraan.php?tanggal='.$tanggal.'&cabangasal='.$cabang_asal.'&idjurusan='.$id_jurusan).'">Jadwal Kendaraan</a> | <a href="'.append_sid("pengaturan_penjadwalan_kendaraan.php?mode=add&tanggal=$tanggal&kodejadwal=$kode_jadwal&idjurusan=$id_jurusan").'">Tambah Jadwal Non-Reguler</a> ',
        'JUDUL'		        => 'Tambah Jadwal Non-Reguler',
        'MODE'   	        => 'save',
        'TANGGAL'   	    => dateparse($tanggal),
        'PESAN'				    => $pesan,
        'BGCOLOR_PESAN'   => $bgcolor_pesan,
        'OPT_KURSI'       => $Mobil->setComboLayoutKursi($data_layout,""),
        'TGL_JADWAL'      => $tanggal,
        'KODE_JADWAL'     => $kode_jadwal,
        'ID_JURUSAN'      => $id_jurusan,
        'KODE_CABANG_ASAL'=> $cabang_asal,
        'CABANG_ASAL'     => $data_cabang_asal["Nama"]."(".$data_cabang_asal["KodeCabang"].")",
        'CABANG_TUJUAN'   => $data_cabang_tujuan["Nama"]."(".$data_cabang_tujuan["KodeCabang"].")",
        'U_ADD'	          => append_sid('pengaturan_penjadwalan_kendaraan.php'),
        'U_BACK'	        => append_sid('pengaturan_penjadwalan_kendaraan.php?tanggal='.$tanggal.'&cabangasal='.$cabang_asal.'&idjurusan='.$id_jurusan)
      )
    );

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');

  exit; /*=============================================================================================================*/;

  case 'save': /*ACTION SIMPAN DATA*************************************************************************************/
    /*MENAMBAHKAN JADWAL NON REGULER KE DATABASE*/
    $kode_jadwal    = $HTTP_POST_VARS['kodejadwal'];
    $jam_berangkat  = $HTTP_POST_VARS['jam'].":".$HTTP_POST_VARS['menit'];
    $layout_kursi   = $HTTP_POST_VARS['layoutkursi'];

    $data_layout	= $Mobil->getArrayLayout();

    $data_jurusan       = $Jurusan->ambilDataDetail($id_jurusan);
    $data_cabang_asal   = $Cabang->ambilDataDetail($data_jurusan["KodeCabangAsal"]);
    $data_cabang_tujuan = $Cabang->ambilDataDetail($data_jurusan["KodeCabangTujuan"]);

    if($PenjadwalanKendaraan->isDuplikasi($tanggal_mysql,$kode_jadwal)){
      $pesan="<font color='white' size=3>Kode kota yang dimasukkan sudah terdaftar dalam sistem!</font>";
      $bgcolor_pesan="red";
    }
    else{
      $path	='<a href="'.append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx."?mode=add").'">Tambah Jadwal Non-Reguler</a> ';

      if($PenjadwalanKendaraan->tambahJadwalNonReguler($tanggal_mysql,$jam_berangkat,$kode_jadwal,
        $id_jurusan,$layout_kursi,$data_layout[$layout_kursi],"Ditambahkan oleh ".$userdata["nama"]. " Pada ".date("H:i d-m-y"))){

        redirect(append_sid("pengaturan_penjadwalan_kendaraan.php?mode=add&cabangasal=$cabang_asal&tanggal=$tanggal&idjurusan=$id_jurusan&pesan=1",true));
      }
      else{
        $pesan="<font color='white' size=3>Gagal menyimpan data!</font>";
        $bgcolor_pesan="red";
      }

    }

    $judul="Tambah Jadwal Non_reguler";
    $page_title = $judul;
    $template->set_filenames(array('body' => 'kota/add_body.tpl'));
    $template->assign_vars(array(
        'BCRUMP'		   => '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_kota.'.$phpEx).'">Kota</a> | '.$path,
        'JUDUL'		     => $judul,
        'MODE'         => 'save',
        'TANGGAL'   	 => dateparse($tanggal),
        'PESAN'		     => $pesan,
        'BGCOLOR_PESAN'=> $bgcolor_pesan,
        'OPT_KURSI'    => $Mobil->setComboLayoutKursi($data_layout,$layout_kursi),
        'KODE_JADWAL'  => $kode_jadwal,
        'CABANG_ASAL'  => $data_cabang_asal["Nama"]."(".$data_cabang_asal["KodeCabang"].")",
        'CABANG_TUJUAN'=> $data_cabang_tujuan["Nama"]."(".$data_cabang_tujuan["KodeCabang"].")",
        'U_ADD'	       => append_sid('pengaturan_penjadwalan_kendaraan.php'),
        'U_BACK'	     => append_sid('pengaturan_penjadwalan_kendaraan.php?tanggal='.$tanggal.'&cabangasal='.$cabang_asal.'&idjurusan='.$id_jurusan)
      )
    );

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');
  exit;

  case "update": /*ACTION UPDATE****************************************************************************************/
    $tgl					= $HTTP_GET_VARS['tgl'];
    $tgl_mysql		= FormatTglToMySQLDate($HTTP_GET_VARS['tgl']);
    $kode_jadwal	= $HTTP_GET_VARS['kode_jadwal'];
    $kode_kendaraaan = $HTTP_GET_VARS['kodekendaraaan'];
    $kode_sopir		= $HTTP_GET_VARS['kode_sopir'];
    $aktif				= $HTTP_GET_VARS['aktif'];
    $submode			= $HTTP_GET_VARS['submode'];

    //MEMERIKSA JIKA AKAN MENONAKTIFKAN JADWAL, AKAN DIPERIKSA TERLEBIH DAHULU APAKAH MASIH ADA PENUMPANG DI JADWAL TERSEBUT ATAU TIDAK
    if($aktif==0){
      $sql =
        "SELECT COUNT(1)
				FROM tbl_reservasi
				WHERE KodeJadwal='$kode_jadwal'
					AND TglBerangkat='$tgl_mysql'
					AND (FlagBatal!=1 OR FlagBatal IS NULL)";

      if (!$result = $db->sql_query($sql)){
        die_error("Err: $this->ID_FILE".__LINE__);
      }

      $data_penumpang=$db->sql_fetchrow($result);

      if($data_penumpang[0]>0){
        //MASIH ADA PENUMPANG DI JADWAL YANG AKAN DINONAKTIFKAN
        echo(0);
        exit;
      }
    }

    $sql =
      "SELECT IdPenjadwalan
			FROM tbl_penjadwalan_kendaraan
			WHERE TglBerangkat='$tgl_mysql'
				AND KodeJadwal='$kode_jadwal'";

    if ($result = $db->sql_query($sql)){
      $row = $db->sql_fetchrow($result);

      if($row[0]!=""){
        if($submode==0){
          //ubah kendaraan
          $PenjadwalanKendaraan->ubah($row[0],$kode_kendaraaan,"");
        }
        else{
          //ubah sopir
          $PenjadwalanKendaraan->ubah($row[0],"",$kode_sopir);
        }
      }
      else{
        $sql_jadwal	=
          "SELECT JamBerangkat,IdJurusan FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal'";

        if ($result = $db->sql_query($sql_jadwal)){
          $row_jadwal = $db->sql_fetchrow($result);
        }
        else{
          echo("Err:".__LINE__);
          exit;
        }

        $PenjadwalanKendaraan->tambah(
          $tgl_mysql,$row_jadwal['JamBerangkat'],
          $kode_jadwal,$row_jadwal['IdJurusan'],
          $kode_kendaraaan,$kode_sopir,$aktif);

      }
    }
    else{
      echo("Err:".__LINE__);
      exit;
    }

    echo(1);
  exit; /*=============================================================================================================*/

  case "ubahstatus": /*ACTION UBAH STATUS*******************************************************************************/
    $id = $HTTP_GET_VARS['id'];

    echo($PenjadwalanKendaraan->ubahStatus($id)?1:0);
  exit; /*=============================================================================================================*/

  case "hapus": /*ACTION UBAH STATUS*******************************************************************************/
    $id = $HTTP_GET_VARS['id'];

    if($PenjadwalanKendaraan->hapusJadwalNonReguler($id)){
      echo("status='OK';");
    }
    else{
      echo("status='ERROR';");
    }
  exit; /*=============================================================================================================*/

  case "get_tujuan": /*ACTION GET TUJUAN********************************************************************************/
    $cabang_asal		= $HTTP_GET_VARS['asal'];
    $id_jurusan			= $HTTP_GET_VARS['jurusan'];

    $opt_cabang_tujuan=
      "<select id='idjurusan' name='idjurusan'>".
      setComboCabangTujuan($cabang_asal,$id_jurusan)
      ."</select>";

    echo($opt_cabang_tujuan);

  exit; /*=============================================================================================================*/

  case "setdaftarmobil": /*ACTION SET LIST MOBIL************************************************************************/
    $mobil_dipilih	= $HTTP_POST_VARS['mobil'];
    $kode_jadwal		= $HTTP_POST_VARS['kodejadwal'];
    $tanggal				= $HTTP_POST_VARS['tanggal'];
    $aktif					= $HTTP_POST_VARS['aktif'];

    $return	=
      "<select name='dlgcbomobil' id='dlgcbomobil'>"
      .setComboMobil($mobil_dipilih).
      "</select>";

    echo($return);

  exit; /*=============================================================================================================*/

  case "setdaftarsopir": /*ACTION SET LIST SOPIR************************************************************************/
    $sopir_dipilih	= $HTTP_POST_VARS['sopir'];
    $kode_jadwal		= $HTTP_POST_VARS['kodejadwal'];
    $tanggal				= $HTTP_POST_VARS['tanggal'];
    $aktif					= $HTTP_POST_VARS['aktif'];

    $return			=
      "<select name='dlgcbosopir' id='dlgcbosopir'>"
      .setComboSopir($sopir_dipilih).
      "</select>";

    echo($return);

  exit; /*=============================================================================================================*/

}
?>