<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$kota  					= $HTTP_GET_VARS['kota'];
$asal  					= $HTTP_GET_VARS['asal'];
$tujuan  				= $HTTP_GET_VARS['tujuan'];
$tanggal_mulai	= $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir	= $HTTP_GET_VARS['tanggal_akhir'];

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi	= "";

$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan))='$kota'":"";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'":"";

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"TglBerangkat,Jamberangkat":$sort_by;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Daftar Manifest Kota:'.$kota.',Asal:'.$asal.',Tujuan:'.$tujuan.' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Kode Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Keterlambatan');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', '#Manifest');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Sopir');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Mobil');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Jumlah Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

$sql	=
	"SELECT 
		KodeDriver,Driver,
		NoSPJ,TglSPJ,KodeJadwal,
		TglBerangkat,JamBerangkat,JumlahKursiDisediakan,
		JumlahPenumpang,NoPolisi,
		TIMEDIFF(TglSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) AS Keterlambatan,
		IF(TIMEDIFF(TglSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) >'00:30:00',1,0) IsTerlambat
	FROM tbl_spj ts
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	ORDER BY $sort_by $order";

	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+3;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, dateparse(FormatMySQLDateToTglWithTime($row['TglSPJ'])));
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['Keterlambatan']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NoSPJ']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Driver']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NoPolisi']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['JumlahPenumpang']);
	
}
$temp_idx=$idx_row;

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Daftar Manifest Kota:'.$kota.',Asal:'.$asal.',Tujuan:'.$tujuan.' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}

  
?>
