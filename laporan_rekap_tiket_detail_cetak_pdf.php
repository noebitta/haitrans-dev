<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['p0'])? $HTTP_GET_VARS['p0'] : $HTTP_POST_VARS['p0'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$pencari				= isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$jenis_laporan	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

if($jenis_laporan==0){
	//laporan omzet cso
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND PetugasPenjual=$pencari";
	$judul						= "Rekap Tiket Omzet CSO";
}
elseif($jenis_laporan==1){
	//laporan omzet cabang	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND KodeCabang=$pencari";
	$judul						= "Laporan Omzet Cabang";
}
elseif($jenis_laporan==2){
	//laporan omzet Jurusan	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND IdJurusan=$pencari";
	$judul						= "Laporan Omzet Jurusan";
}
elseif($jenis_laporan==3){
	//laporan rekap uang user	
	$kondisi_waktu		= "WaktuCetakTiket";
	$kondisi_laporan	= "AND PetugasCetakTiket=$pencari";
	
	$kondisi_waktu_paket	= "WaktuPesan";
	$kondisi_laporan_paket= "AND PetugasPenjual=$pencari";
}
elseif($jenis_laporan==4){
	//laporan rekap uang user	
	$kondisi_waktu		= "WaktuCetakTiket";
	$kondisi_laporan	= "AND KodeCabang=$pencari";
	
	$kondisi_waktu_paket	= "WaktuPesan";
	$kondisi_laporan_paket= "AND KodeCabang=$pencari";
}

$kondisi	= 
	"WHERE (DATE($kondisi_waktu) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 $kondisi_laporan";

$kondisi_cari	=($cari=="")?"":
	" AND NoTiket LIKE '$cari'";
	
$kondisi	= $kondisi.$kondisi_cari;

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"WaktuPesan":$sort_by;

//QUERY
//DATA TIKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,Discount,Total,JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM 
		tbl_reservasi
	$kondisi
	ORDER BY $sort_by $order";	

//QUERY PAKET
$sql_paket=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,NamaPengirim,
		NamaPenerima,HargaPaket,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO
	FROM 
		tbl_paket
	$kondisi
	ORDER BY $sort_by $order";	
	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,80);
$pdf->Ln(25);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,$judul,'',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',12);
$pdf->Cell(60,5,"PEROLEHAN TIKET",'',0,'L');$pdf->Ln();

$pdf->SetFont('courier','B',8);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(25,5,'W.Pesan','B',0,'C',1);
$pdf->Cell(20,5,'No.Tiket','B',0,'C',1);
$pdf->Cell(25,5,'W.Brgkt','B',0,'C',1);
$pdf->Cell(20,5,'Kode','B',0,'C',1);
$pdf->Cell(25,5,'Nama','B',0,'C',1);
$pdf->Cell(15,5,'Seat','B',0,'C',1);
$pdf->Cell(20,5,'Harga','B',0,'C',1);
$pdf->Cell(20,5,'Disc.','B',0,'C',1);
$pdf->Cell(20,5,'Total','B',0,'C',1);
$pdf->Cell(25,5,'Tipe.Disc','B',0,'C',1);
$pdf->Cell(20,5,'CSO','B',0,'C',1);
$pdf->Cell(20,5,'Status','B',0,'C',1);
$pdf->Cell(25,5,'Ket','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',8);
$pdf->SetTextColor(0);
//CONTENT TIKET

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		
		if($row['FlagBatal']!=1){
			if($row['CetakTiket']!=1){
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			$keterangan="";
		}
		else{
			$status	="BATAL";
			$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
			$sum_tiket_batal++;
			$sum_uang_tiket_batal	+= $row['HargaTiket'];
			$sum_discount_batal		+= $row['TotalDiscount'];
			$sum_total_batal			+= $row['Total'];
		}
		
		$sum_tiket_all++;
		$sum_uang_tiket_all	+= $row['HargaTiket'];
		$sum_discount_all		+= $row['TotalDiscount'];
		$sum_total_all			+= $row['Total'];
		
		$pdf->Cell(5,5,$i,'',0,'C');
		$pdf->MultiCell2(25,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),'','L');
		$pdf->Cell(20,5,$row['NoTiket'],'',0,'L');
		$pdf->MultiCell2(25,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),'','L');
		$pdf->Cell(20,5,$row['KodeJadwal'],'',0,'L');
		$pdf->MultiCell2(25,5,$row['Nama'],'',0,'L');
		$pdf->Cell(15,5,number_format($row['NomorKursi'],0,",","."),'',0,'R');
		$pdf->Cell(20,5,number_format($row['HargaTiket'],0,",","."),'',0,'R');
		$pdf->Cell(20,5,number_format($row['Discount'],0,",","."),'',0,'R');
		$pdf->Cell(20,5,number_format($row['Total'],0,",","."),'',0,'R');
		$pdf->Cell(20,5,number_format($row['TipeDiscount'],0,",","."),'',0,'L');
		$pdf->MultiCell2(20,5,$row['NamaCSO'],'',0,'L');
		$pdf->MultiCell2(20,5,$status,'',0,'L');
		$pdf->MultiCell2(25,5,$keterangan,'',0,'L');
		$pdf->Ln(0);
		$pdf->Cell(285,1,'','B',0,'');
		$pdf->Ln();
		$i++;
  }
	
	$pdf->SetFont('courier','',10);
	
	$pdf->Ln();
	$pdf->Cell(0,5,'KETERANGAN:','',0,'L');$pdf->Ln();
	$pdf->Cell(50,5,'Total Tiket Dicetak:','',0,'L');$pdf->Cell(15,5,number_format($sum_tiket_all,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(50,5,'Total Tiket Batal:','',0,'L');$pdf->Cell(15,5,number_format($sum_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(65,1,'','B',0,'');$pdf->Ln();
	$pdf->Cell(50,5,'Total Tiket OK:','',0,'L');$pdf->Cell(15,5,number_format($sum_tiket_all-$sum_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	
	$pdf->Ln();$pdf->Ln();
	$pdf->Cell(50,5,'Perolehan Tiket Dicetak:','',0,'L');$pdf->Cell(30,5,number_format($sum_uang_tiket_all,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(50,5,'Perolehan Tiket Batal:','',0,'L');$pdf->Cell(30,5,number_format($sum_uang_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(80,1,'','B',0,'');$pdf->Ln();
	$pdf->Cell(50,5,'Perolehan Tiket OK:','',0,'L');$pdf->Cell(30,5,number_format($sum_uang_tiket_all-$sum_uang_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(50,5,'Discount:','',0,'L');$pdf->Cell(30,5,number_format($sum_discount_all-$sum_discount_batal,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(80,1,'','B',0,'');$pdf->Ln();
	$pdf->Cell(50,5,'Perolehan Tiket Akhir:','',0,'L');$pdf->Cell(30,5,number_format($sum_uang_tiket_all-$sum_uang_tiket_batal-$sum_discount_all-$sum_discount_batal,0,",","."),'',0,'R');$pdf->Ln();
	
} 
else{
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
} 

$pdf->Ln(4);
$pdf->SetFont('courier','B',12);
$pdf->Cell(60,5,"PEROLEHAN PAKET",'',0,'L');$pdf->Ln();

$pdf->SetFont('courier','B',8);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(30,5,'W.Brgkt','B',0,'C',1);
$pdf->Cell(30,5,'No.Tiket','B',0,'C',1);
$pdf->Cell(30,5,'Kode','B',0,'C',1);
$pdf->Cell(30,5,'Dari','B',0,'C',1);
$pdf->Cell(30,5,'Untuk','B',0,'C',1);
$pdf->Cell(30,5,'Harga','B',0,'C',1);
$pdf->Cell(30,5,'CSO','B',0,'C',1);
$pdf->Cell(30,5,'Status','B',0,'C',1);
$pdf->Cell(30,5,'Ket','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',8);
$pdf->SetTextColor(0);
//CONTENT PAKET

$sum_tiket_batal			= 0;
$sum_uang_tiket_batal	= 0;
$sum_tiket_all				= 0;
$sum_uang_tiket_all		= 0;


if ($result = $db->sql_query($sql_paket)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		
		if($row['FlagBatal']!=1){
			if($row['CetakTiket']!=1){
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			$keterangan="";
		}
		else{
			$status	="BATAL";
			$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
			$sum_tiket_batal++;
			$sum_uang_tiket_batal	+= $row['HargaPaket'];
		}
		
		$sum_tiket_all++;
		$sum_uang_tiket_all	+= $row['HargaPaket'];
		
		$pdf->Cell(5,5,$i,'',0,'C');
		$pdf->MultiCell2(30,5,dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),'','L');
		$pdf->Cell(30,5,$row['NoTiket'],'',0,'L');
		$pdf->Cell(30,5,$row['KodeJadwal'],'',0,'L');
		$pdf->MultiCell2(30,5,$row['NamaPengirim'],'',0,'L');
		$pdf->MultiCell2(30,5,$row['NamaPenerima'],'',0,'L');
		$pdf->Cell(30,5,number_format($row['HargaPaket'],0,",","."),'',0,'R');
		$pdf->MultiCell2(30,5,$row['NamaCSO'],'',0,'L');
		$pdf->MultiCell2(30,5,$status,'',0,'L');
		$pdf->MultiCell2(30,5,$keterangan,'',0,'L');
		$pdf->Ln(0);
		$pdf->Cell(275,1,'','B',0,'');
		$pdf->Ln();
		$i++;
  }
	
	$pdf->SetFont('courier','',10);
	
	$pdf->Ln();
	$pdf->Cell(0,5,'KETERANGAN:','',0,'L');$pdf->Ln();
	$pdf->Cell(50,5,'Total Paket Dicetak:','',0,'L');$pdf->Cell(15,5,number_format($sum_tiket_all,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(50,5,'Total Paket Batal:','',0,'L');$pdf->Cell(15,5,number_format($sum_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(65,1,'','B',0,'');$pdf->Ln();
	$pdf->Cell(50,5,'Total Paket OK:','',0,'L');$pdf->Cell(15,5,number_format($sum_tiket_all-$sum_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	
	$pdf->Ln();$pdf->Ln();
	$pdf->Cell(50,5,'Perolehan Paket Dicetak:','',0,'L');$pdf->Cell(30,5,number_format($sum_uang_tiket_all,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(50,5,'Perolehan Paket Batal:','',0,'L');$pdf->Cell(30,5,number_format($sum_uang_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	$pdf->Cell(80,1,'','B',0,'');$pdf->Ln();
	$pdf->Cell(50,5,'Perolehan Paket OK:','',0,'L');$pdf->Cell(30,5,number_format($sum_uang_tiket_all-$sum_uang_tiket_batal,0,",","."),'',0,'R');$pdf->Ln();
	
} 
else{
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
} 
						
$pdf->Output();
						
?>