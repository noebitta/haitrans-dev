<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$userdata = session_pagestart($user_ip,305);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$filbiaya1  = isset($HTTP_GET_VARS['filbiaya1'])? $HTTP_GET_VARS['filbiaya1'] : $HTTP_POST_VARS['filbiaya1'];
$filbiaya2  = isset($HTTP_GET_VARS['filbiaya2'])? $HTTP_GET_VARS['filbiaya2'] : $HTTP_POST_VARS['filbiaya2'];
$filbiaya3  = isset($HTTP_GET_VARS['filbiaya3'])? $HTTP_GET_VARS['filbiaya3'] : $HTTP_POST_VARS['filbiaya3'];
$filbiaya4  = isset($HTTP_GET_VARS['filbiaya4'])? $HTTP_GET_VARS['filbiaya4'] : $HTTP_POST_VARS['filbiaya4'];
$filbiaya5  = isset($HTTP_GET_VARS['filbiaya5'])? $HTTP_GET_VARS['filbiaya5'] : $HTTP_POST_VARS['filbiaya5'];
$filbiaya7  = isset($HTTP_GET_VARS['filbiaya7'])? $HTTP_GET_VARS['filbiaya7'] : $HTTP_POST_VARS['filbiaya7'];
$filbiaya8  = isset($HTTP_GET_VARS['filbiaya8'])? $HTTP_GET_VARS['filbiaya8'] : $HTTP_POST_VARS['filbiaya8'];
$filbiaya9  = isset($HTTP_GET_VARS['filbiaya9'])? $HTTP_GET_VARS['filbiaya9'] : $HTTP_POST_VARS['filbiaya9'];


$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//ACT MODE==========================================================================

switch($mode){
	case 'getasal':
		$Cabang	= new Cabang();
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		$Cabang	= new Cabang();
		
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;
	
}

//VIEW MODE=========================================================================================

if($tanggal_mulai==''){
	$chkbiaya1="checked";
	$chkbiaya2="checked";
	$chkbiaya3="checked";
	$chkbiaya4="checked";
	$chkbiaya5="checked";
	$chkbiaya7="checked";
	$chkbiaya8="checked";
	$chkbiaya9="checked";
	
	$kondisi_filter_biaya	= "";
}
else{
	$list_filter_jenis_biaya = "";
	
	if($filbiaya1=="on"){$chkbiaya1="checked";$list_filter_jenis_biaya .="1,";}else{$chkbiaya1="";$list_filter_jenis_biaya .="";}
	if($filbiaya2=="on"){$chkbiaya2="checked";$list_filter_jenis_biaya .="2,";}else{$chkbiaya2="";$list_filter_jenis_biaya .="";}
	if($filbiaya3=="on"){$chkbiaya3="checked";$list_filter_jenis_biaya .="3,";}else{$chkbiaya3="";$list_filter_jenis_biaya .="";}
	if($filbiaya4=="on"){$chkbiaya4="checked";$list_filter_jenis_biaya .="4,";}else{$chkbiaya4="";$list_filter_jenis_biaya .="";}
	if($filbiaya5=="on"){$chkbiaya5="checked";$list_filter_jenis_biaya .="5,";}else{$chkbiaya5="";$list_filter_jenis_biaya .="";}
	if($filbiaya7=="on"){$chkbiaya7="checked";$list_filter_jenis_biaya .="7,";}else{$chkbiaya7="";$list_filter_jenis_biaya .="";}
	if($filbiaya8=="on"){$chkbiaya8="checked";$list_filter_jenis_biaya .="8,";}else{$chkbiaya8="";$list_filter_jenis_biaya .="";}
	if($filbiaya9=="on"){$chkbiaya9="checked";$list_filter_jenis_biaya .="9,";}else{$chkbiaya9="";$list_filter_jenis_biaya .="";}

	$list_filter_jenis_biaya	= $list_filter_jenis_biaya!=""?substr($list_filter_jenis_biaya,0,-1):"1,2,3,4,5,7,8,9";
	$kondisi_filter_biaya	= " AND FlagJenisBiaya IN(".$list_filter_jenis_biaya.")";
	
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

// LIST
$template->set_filenames(array('body' => 'laporan.keuangan.biayaop/index.tpl')); 

$kondisi =($cari=="")?
	"":
	" AND (f_sopir_get_nama_by_id(tbo.KodeSopir) LIKE '$cari%'
					OR f_kendaraan_ambil_nopol_by_kode(tbo.NoPolisi) LIKE '%$cari%'
					OR tbo.NoSPJ LIKE '%$cari%'
					OR tbo.Keterangan LIKE '%$cari%'
					OR f_user_get_nama_by_userid(IdPetugas) LIKE '%$cari%'
					OR CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) LIKE  '%$cari%'
				)";

$kondisi .= $kota==""?"":"AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan))='$kota'";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)='$asal'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan)='$tujuan'":"";

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"TglTransaksi":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tbo.IdBiayaOP","(tbl_biaya_op tbo LEFT JOIN tbl_spj ts ON tbo.NoSPJ=ts.NoSPJ)",
"&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota.
	"&asal=".$asal."&tujuan=".$tujuan."&filbiaya1=".$filbiaya1."&filbiaya2=".$filbiaya2."&filbiaya3=".$filbiaya3."&filbiaya4=".$filbiaya4."&filbiaya5=".$filbiaya5."&filbiaya7=".$filbiaya7.
	"&cari=".$cari."&sort_by=".$sort_by."&order=".$order,
"WHERE (tbo.TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND FlagJenisBiaya!='$FLAG_BIAYA_VOUCHER_BBM' $kondisi $kondisi_filter_biaya",
"laporan.keuangan.biayaop.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//mengambil query voucher BBM

$sql	=
	"SELECT
		tbo.TglTransaksi,
		CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) AS Jurusan,
		ts.JamBerangkat,
		tbo.NoSPJ,
		tbo.NoPolisi,
		f_sopir_get_nama_by_id(tbo.KodeSopir) AS NamaSopir,
		Jumlah,FlagJenisBiaya,
		f_user_get_nama_by_userid(IdPetugas) AS Pencetak,
		tbo.WaktuCatat,Keterangan
	FROM (tbl_biaya_op tbo LEFT JOIN tbl_spj ts ON tbo.NoSPJ=ts.NoSPJ)
	WHERE
		(tbo.TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND FlagJenisBiaya!='$FLAG_BIAYA_VOUCHER_BBM'
		$kondisi
		$kondisi_filter_biaya
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$show_header	= ($i%$config['repeatshowheader']!=0 && $i!=1)?"none":"";
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'showheader'=>$show_header,
				'no'=>$i,
				'tanggal'=>dateparseD_Y_M(FormatMySQLDateToTgl($row['TglTransaksi'])),
				'jurusan'=>$row['Jurusan'],
				'jam'=>$row['JamBerangkat'],
				'jenisbiaya'=>$LIST_JENIS_BIAYA[$row['FlagJenisBiaya']],
				'no_spj'=>$row['NoSPJ'],
				'kendaraan'=>$row['NoPolisi'],
				'sopir'=>$row['NamaSopir'],
				'jumlah'=>"Rp.".number_format($row['Jumlah'],0,",","."),
				'remark'=>$row['Keterangan'],
				'releaser'=>$row['Pencetak'],
				'released'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCatat'])),
			)
		);
	
	$i++;
}

//TOTAL SUMMARY=============

$sql	=
	"SELECT SUM(Jumlah)
	FROM (tbl_biaya_op tbo LEFT JOIN tbl_spj ts ON tbo.NoSPJ=ts.NoSPJ)
	WHERE
		(tbo.TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND FlagJenisBiaya!='$FLAG_BIAYA_VOUCHER_BBM'
		$kondisi
		$kondisi_filter_biaya";
	
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$summary	=
	"<b>Total Biaya	= Rp. ".number_format($row[0],0,",",".")."</b>";
//-END TOTAL SUMMARY===========

if($i-1<=0){
	$template->assign_block_vars('ROW',array('showheader'=>''));
	$no_data	=	"<div style='width:100%;' class='yellow' align='center'><font size=3><b>data tidak ditemukan</b></font></div>";
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	=
	"&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota.
	"&asal=".$asal."&tujuan=".$tujuan."&filbiaya1=".$filbiaya1."&filbiaya2=".$filbiaya2."&filbiaya3=".$filbiaya3."&filbiaya4=".$filbiaya4."&filbiaya5=".$filbiaya5."&filbiaya7=".$filbiaya7.
	"&cari=".$cari."&sort_by=".$sort_by."&order=".$order;
												
$script_cetak_excel="Start('laporan.keuangan.biayaop.exportexcel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//BEGIN KOMPONEN-KOMPONEN SORTING================================
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';

$parameter_sorting	= 
	"&page=".$idx_page."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota.
	"&asal=".$asal."&tujuan=".$tujuan."&filbiaya1=".$filbiaya1."&filbiaya2=".$filbiaya2."&filbiaya3=".$filbiaya3."&filbiaya4=".$filbiaya4."&filbiaya5=".$filbiaya5."&filbiaya7=".$filbiaya7.
	"&cari=".$cari."&order=".$order_invert;

$array_sort	= 
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=TglTransaksi'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=Jurusan'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=JamBerangkat'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=FlagJenisBiaya'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=NoSPJ'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=NoPolisi'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=NamaSopir'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=Jumlah'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=Pencetak'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=WaktuCatat'.$parameter_sorting)."',".
	"'".append_sid('laporan.keuangan.biayaop.php?sort_by=Keterangan'.$parameter_sorting)."'";

//END KOMPONEN-KOMPONEN SORTING================================

include($adp_root_path . 'ClassKota.php');
$Kota = new Kota();

$page_title	= "Laporan Biya Operasional";
	
$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('menu_lap_keuangan.'.$phpEx.'?top_menu_dipilih=top_menu_lap_keuangan') .'">Home</a> | <a href="'.append_sid('laporan.keuangan.biayaop.'.$phpEx).'">Laporan Keuangan Biaya Operasional</a>',
	'ACTION_CARI'		=> append_sid('laporan.keuangan.biayaop.'.$phpEx),
	'OPT_KOTA'			=> $Kota->setComboKota($kota),
	'TXT_CARI'			=> $cari,
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'SUMMARY'				=> $summary,
	'ARRAY_SORT'		=> $array_sort,
	'NO_DATA'				=> $no_data,
	'CHKBIAYA1'			=> $chkbiaya1,
	'CHKBIAYA2'			=> $chkbiaya2,
	'CHKBIAYA3'			=> $chkbiaya3,
	'CHKBIAYA4'			=> $chkbiaya4,
	'CHKBIAYA5'			=> $chkbiaya5,
	'CHKBIAYA7'			=> $chkbiaya7,
	'CHKBIAYA8'			=> $chkbiaya8,
	'CHKBIAYA9'			=> $chkbiaya9,
	)
);
	      
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>